jQuery(document).ready(function(){
	
	
	
	
	var chromeBrowser = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()); 

	if(!chromeBrowser){
	 
		alert("Use chrome browser and try again");
	
		window.location.replace("http://180.179.67.207/login");
	}
	else
	{
		//jQuery(".content").hide();
		jQuery("#main-menu").hide();

		jQuery(".breadcrumb").hide();

		jQuery("#triptych-wrapper").hide();

		jQuery(".title").hide();

		jQuery("#footer-wrapper").hide();
		
		
		
	
		
		//Login pop up.
		jQuery(function() {
		    jQuery( "#loginScreenPopUp" ).dialog({
		      autoOpen: true,
		      minWidth: 400,
		      modal:true,
		      show: {
		        effect: "clip",
		        duration: 200
		      },
		      hide: {
		        effect: "clip",
		        duration: 200
		      }
		   });
		});
		
		
		jQuery("#txtNewPassword").focus();
		
		/*searchWHLocation();
		
		function searchWHLocation()
		{
			showMessage("loading", "Loading data...", "");
			
			jQuery.ajax({
				url:Drupal.settings.basePath + 'LoginCallback',
				data:
				{
					id:"searchWHLocation",
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var WHLocationData = jQuery.parseJSON(data);
					
					if(WHLocationData.Status == 1)
					{
						
						jQuery("#loginLocation").empty();
						
						jsonForLocations=data;
						var whlocations = WHLocationData.Result;
						WHLocation =  WHLocationData.Result;
						
						jQuery.each(whlocations,function(key,value)
						{	
							jQuery("#loginLocation").append("<option id=\""+whlocations[key]['LocationCode']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
			
						});
					}
					else
					{
						showMessage("red", WHLocationData.Description, '');
					}
					
				}
			
			});
		}*/
		
		
		
		
		//jQuery( "#distributorHistoryPopUp" ).dialog( "open" );
		
		
		/*jQuery("#loginLocation").keydown(function(e){

			if(e.which == 115) 
			{
				var currentLocationId = this.id;

				jQuery.ajax({
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"LocationSearchLookUp",
					},
					success:function(data)
					{

						//alert(data);
						jQuery("#divLookUp").html(data);

						locationSearchGrid(currentLocationId); //orderHistory.js
						jQuery("#divLookUp" ).dialog( "open" );

						compositeItemLookUpData();

						----------Dialog for item search is there ----------------


					}

				});
			}


		});*/
		
		/*jQuery("#username").keyup(function(event){

			if(event.keyCode == 13){
				
				var username = jQuery("#username").val();

				jQuery.ajax({
					url:Drupal.settings.basePath + 'LoginCallback',
					data:
					{
						id:"fetchUserLocation",
						username:username,
					},
					success:function(data)
					{
					
						jQuery("#loginLocation").empty();
						//alert(data);
						
						jQuery(".ajaxLoading").hide();
						
						var userLocationsData = jQuery.parseJSON(data);
						
						if(userLocationsData.Status == 1)
						{

							var oUserLocationsData = userLocationsData.Result;
							
							if(oUserLocationsData.length == 0)
							{
								
							}
							else
							{
								jQuery.each(oUserLocationsData,function(key,value){
									
									jQuery("#loginLocation").append("<option id=\""+oUserLocationsData[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['LocationName']+"</option>");
									
								});
							}

							//storeValuesInSession(autheticatedInformation[0]['UserId'],autheticatedInformation[0]['Username'],autheticatedInformation[0]['LocationId']);

							jQuery("#loginScreenPopUp").dialog("close");


						}
						
						else
						{
							jQuery("#errorMsg").html("<h4 style='color:red'>"+authenticatedUserData.Description+"</h4>");
							//jQuery("#errorMsg").html("<h3 style='background-color:red'>Authentication failed</h3>");
						}
						
					}
				
				});


			}
		});*/
		
		
		jQuery("#txtNewPassword").focus();
		
		
		jQuery("#txtNewPassword").focusout(function(){
			
			if(jQuery("#txtNewPassword").val().length < 6 && jQuery("#txtNewPassword").val().length != '') 
			{
				jQuery("#errorMsg").html("<h4 style='color:red'>Password should be of minimum 6 Characters</h4>");
				
				jQuery("#txtNewPassword").focus();
				
				jQuery("#txtNewPassword").select();
			}
			else
			{
				jQuery("#errorMsg").html("<h4 style='color:red'></h4>");
			}
			
		});
		
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:
			{
				id:"getLoggedInUserInfo",
			},
			success:function(data)
			{
				//jQuery(".ajaxLoading").hide();
				
				//alert(data);
				var loginUserData = jQuery.parseJSON(data);
				
				if(loginUserData.Status == 1)
				{
					

					//jQuery("#loggedInUserName").html(authenticatedUserData.Result.UserName);
					jQuery("#loggedInUserName").html(loginUserData.Result.UserFullName);

					jQuery("#loggedInLocation").html(loginUserData.Result.LocationName);

					var m_names = new Array("Jan", "Feb", "Mar", 
							"Apr", "May", "Jun", "Jul", "Aug", "Sep", 
							"Oct", "Nov", "Dec");


					var d = new Date();
					var curr_date = d.getDate();
					var curr_month = d.getMonth();
					var curr_year = d.getFullYear();
					var todayDate = curr_date + "-" + m_names[curr_month] + "-" + curr_year;

					jQuery("#loggedInUserTime").html(todayDate);		

					//storeValuesInSession(autheticatedInformation[0]['UserId'],autheticatedInformation[0]['Username'],autheticatedInformation[0]['LocationId']);

					//jQuery("#loginScreenPopUp").dialog("close");
					
				}
				else
				{
					showMessage("red", loginUserData.Description, "");
					
					
				}
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
		
		
		jQuery("#txtReEnterPassword").keyup(function(event){
			
			if(event.keyCode == 13){
		    	
				var newPassword = jQuery("#txtNewPassword").val();
		    	
				var reEnteredPassword = jQuery("#txtReEnterPassword").val();
				
				
				var passwordToBeChangedOrNot = matchPasswords(newPassword,reEnteredPassword)
				
				if(passwordToBeChangedOrNot == -1)
				{
					jQuery("#errorMsg").html("<h4 style='color:red'>Password should be of minimum 6 Characters.</h4>");
					
					
					
					//jQuery("#errorMsg").html("<h4 style='color:green'>Passwords Changed.</h4>");
				}
				else if(passwordToBeChangedOrNot == 0)
				{
					jQuery("#errorMsg").html("<h4 style='color:red'>Passwords not matched ! Try again.</h4>");
				}
				else
				{
					changePassword(newPassword);
				}
				
				
		    }
		});
		
		
		
		jQuery("#btnChangePassword").click(function(){
			
			var newPassword = jQuery("#txtNewPassword").val();
	    	
			var reEnteredPassword = jQuery("#txtReEnterPassword").val();
			
			
			var passwordToBeChangedOrNot = matchPasswords(newPassword,reEnteredPassword)
			
			if(passwordToBeChangedOrNot == -1)
			{
				jQuery("#errorMsg").html("<h4 style='color:red'>Password should be of minimum 6 Characters.</h4>");
				
				
				
				//jQuery("#errorMsg").html("<h4 style='color:green'>Passwords Changed.</h4>");
			}
			else if(passwordToBeChangedOrNot == 0)
			{
				jQuery("#errorMsg").html("<h4 style='color:red'>Passwords not matched ! Try again.</h4>");
			}
			else
			{
				changePassword(newPassword);
			}
			
			
		});
		
		
		function matchPasswords(newPassword,reEnteredPassword)
		{
			var passwordMatched = 0;
			
			if(newPassword.length < 6)
			{
				passwordMatched = -1;
			}
			else
			{
				if(newPassword != reEnteredPassword)
				{
					passwordMatched = 0;
					
						
				}
				else
				{
					passwordMatched = 1;
					
						
				}
			}
			
			
			
			return passwordMatched;
		}
		
		
		function changePassword(newPassword)
		{
			showMessage("loading", "Changing Password...", "");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'CommonActionCallback',
				data:
				{
					id:"changePassword",
					newPassword:newPassword,
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					//alert(data);
					var changePasswordData = jQuery.parseJSON(data);
					
					if(changePasswordData.Status == 1)
					{
						jQuery("#errorMsg").html("<h4 style='color:green'>Password Changed.</h4>");
						
						jQuery("#loginScreenPopUp").dialog("close");
						
						showMessage("green", "Password Changed Successfully", "");
						
					}
					else
					{
						showMessage("red", changePasswordData.Description, "");
						
						jQuery("#errorMsg").html("<h4 style='color:red'>"+authenticatedDistributorData.Description+"</h4>");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		jQuery("#login").click(function(){
			
			var locationId = jQuery("#loginLocation").val();
			
			//var typeOfPOSSelection = jQuery("#")
			var typeOfPOSSelection = jQuery('input[name=TypeOfPOS]:checked').val();
			
			authenticateUser(locationId,typeOfPOSSelection);
			
		});
		
		function validateDistributorOrPUC(username)
		{
			var password = jQuery("#password").val();
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LoginCallback',
				data:
				{
					id:"authenticateUser",
					username:username,
					password:password,
				},
				success:function(data)
				{
					//alert(data);
					var authenticatedDistributorData = jQuery.parseJSON(data);
					
					if(authenticatedDistributorData.Status == 1)
					{
						jQuery("#loginScreenPopUp").dialog("close");
						
					}
					else
					{
						showMessage("red", authenticatedDistributorData.Description, "");
						
						jQuery("#errorMsg").html("<h4 style='color:red'>"+authenticatedDistributorData.Description+"</h4>");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
			
		}
		
		function fetchUserLocations()
		{
			var username = jQuery("#username").val();
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LoginCallback',
				data:
				{
					id:"fetchUserLocation",
					username:username,
				},
				success:function(data)
				{
				
					jQuery("#loginLocation").empty();
					//alert(data);
					
					jQuery(".ajaxLoading").hide();
					
					jQuery("#locationOption").hide();
					
					var userLocationsData = jQuery.parseJSON(data);
					
					if(userLocationsData.Status == 1)
					{

						var oUserLocationsData = userLocationsData.Result;
						
						if(oUserLocationsData.length == 0)
						{
							jQuery("#locationOption").hide();
							
							jQuery("#errorMsg").html("<h4 style='color:red'>"+"No online location privileges."+"</h4>");
						}
						else if(oUserLocationsData.length == 1)
						{
							var locationId = oUserLocationsData[0]['LocationId'];
							
							var typeOfPOSSelection = jQuery('input[name=TypeOfPOS]:checked').val();
							
							authenticateUser(locationId,typeOfPOSSelection);
						}
						else
						{
							jQuery("#ok").hide();
							
							jQuery("#login").show();
							
							jQuery("#locationOption").show();
							
							jQuery("#errorMsg").html("<h4 style='color:green'>"+"Select any Location and proceed"+"</h4>");
							
							jQuery.each(oUserLocationsData,function(key,value){
								
								jQuery("#loginLocation").append("<option id=\""+oUserLocationsData[key]['LocationId']+"\""+" value=\""+oUserLocationsData[key]['LocationId']+"\""+">"+oUserLocationsData[key]['LocationName']+"</option>");
								
							});
						}

						//storeValuesInSession(autheticatedInformation[0]['UserId'],autheticatedInformation[0]['Username'],autheticatedInformation[0]['LocationId']);

					}
					
					else
					{
						jQuery("#errorMsg").html("<h4 style='color:red'>"+userLocationsData.Description+"</h4>");
						//jQuery("#errorMsg").html("<h3 style='background-color:red'>Authentication failed</h3>");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		
		
		function authenticateUser(locationId,authenticateUser)
		{
			
			//var locationId = jQuery("#loginLocation").val();
			
			if(locationId == -1)
			{
				locationId = '';
			}
			
			showMessage("loading", "Authenticating...", "");
			
			var username = jQuery("#username").val();
			var password = jQuery("#password").val();
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LoginCallback',
				data:
				{
					id:"authenticateUser",
					username:username,
					password:password,
					locationId:locationId,
					typeOfPOS:authenticateUser,
					
				},
				success:function(data)
				{
					
					//alert(data);
					
					jQuery(".ajaxLoading").hide();
					
					var authenticatedUserData = jQuery.parseJSON(data);
					if(authenticatedUserData.Status == 1)
					{

						var AuthenticatedUserUserId = authenticatedUserData.Result.UserId;

						//jQuery("#loggedInUserName").html(authenticatedUserData.Result.UserName);
						jQuery("#loggedInUserName").html(authenticatedUserData.Result.UserFullName);

						jQuery("#loggedInLocation").html(authenticatedUserData.Result.LocationName);

						var m_names = new Array("Jan", "Feb", "Mar", 
								"Apr", "May", "Jun", "Jul", "Aug", "Sep", 
								"Oct", "Nov", "Dec");


						var d = new Date();
						var curr_date = d.getDate();
						var curr_month = d.getMonth();
						var curr_year = d.getFullYear();
						var todayDate = curr_date + "-" + m_names[curr_month] + "-" + curr_year;

						jQuery("#loggedInUserTime").html(todayDate);		

						//storeValuesInSession(autheticatedInformation[0]['UserId'],autheticatedInformation[0]['Username'],autheticatedInformation[0]['LocationId']);

						jQuery("#loginScreenPopUp").dialog("close");


					}
					
					else
					{
						jQuery("#errorMsg").html("<h4 style='color:red'>"+authenticatedUserData.Description+"</h4>");
						//jQuery("#errorMsg").html("<h3 style='background-color:red'>Authentication failed</h3>");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
			
		}
		
		function storeValuesInSession(userId,userName,locationId)
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LoginCallback',
				data:
				{
					id:"storeValuesInSession",
					userId:userId,
					userName:userName,
					locationId:locationId,
					
				},
				success:function(data)
				{
					if(data.trim() == "enabled")
					{
						
						//Session has been stored for current location id.
					}
					else
					{
						showMessage("red", "Server side application error", "");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
		}
	}
	

});
