
	
	
	
	/*
	 * Create grid dynamically called from vestige-pos.js for creating item lookup grid.
	 */


	function myAccountSearchGrid()
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#myAccountSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Pay ?', 'Cancel Order','Order Number','Amount','Card Amount','Order Date','Log No.','Order Mode','Distributor Id','DistributorName','Courier Detail','Order Status'],
			colModel: [{ name: 'Select', index: 'Select', width:50 },
			           {name:'action',index:'action',width: 120,align: 'center', formatter: cancelOrderbutton},
			           { name: 'OrderNumber', index: 'OrderNumber', width:150,summaryTpl:'<b>Total({0})</b>'},
			           { name: 'OrderAmount', index: 'OrderAmount', width: 70,summaryType:'sum'},
			           { name: 'CardAmount', index: 'CardAmount', width: 70,summaryType:'sum'},
			           { name: 'OrderDate', index: 'OrderDate', width: 100},
					   { name: 'LogNo', index: 'LogNo', width: 150},
					   { name: 'OrderMode', index: 'OrderMode', width: 70},
					   { name: 'DistributorId', index: 'DistributorId', width: 70},
					   { name: 'DistributorName', index: 'DistributorName', width:100},
					   { name: 'CourierDetail', index: 'CourierDetail', width: 150,hidden:true},
					   { name: 'OrderStatus', index: 'OrderStatus', width: 100}],	
			           pager: jQuery('#PJmap_myAccountSearchGrid'),
			           width:750,
			           height:300,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
					   grouping:true,
			           groupingView : {
			          		groupField : ['LogNo'],
			          		groupSummary : [true],
			          		groupColumnShow : [true],
			          		groupText : ['<input type="checkbox" data-logno="{0}" value = "1" class="groupHeader"/> <b>  {0}  </b>'],
			          		groupCollapse : false,
			          		groupOrder: ['desc']
			           },
			           ondblClickRow: function (id) {
	                		var selectedLookUpCode = jQuery("#myAccountSearchGrid").jqGrid('getCell', id, 'OrderNumber');

	                		showOrderReport(selectedLookUpCode);
	                		/*jQuery("#"+fieldIdForItemSearch).val(selectedLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();

	                		jQuery("#divLookUp1" ).dialog("close");*/
	                		
	                 
	                   } 
			          
		});

		jQuery("#myAccountSearchGrid").jqGrid('navGrid','#PJmap_myAccountSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_myAccountSearchGrid").hide();
		
		/*-----------------------------------------------------------------------------------------------------------*/
	}
	function cancelOrderbutton(cellvalue, options, rowObject)
    {
        var edit = "<input class='button' type='button' value='Cancel' onclick=\"cancelHistoryOrder(this,'" + options.rowId + "',this.value);\"  />";
        return edit;
    }
	
	function cancelHistoryOrder(button,id,val){
		
		var rowData = jQuery("#myAccountSearchGrid").getRowData(id);
		var postData = JSON.stringify(rowData);
				var customerOrderNumber = rowData.OrderNumber;
				//var LogNo = rowData.LogNo;
				//var OrderAmount = rowData.OrderAmount;
				var OrderStatus = rowData.OrderStatus;				
				if(OrderStatus == "Invoiced"){
					showMessage("red", "Order Already Invoiced. Cancellation not allowed.", "");
					jQuery("#btnOrderCancel").attr('disabled',false);
					return;
				}
				
				if(OrderStatus == "Cancelled"){
					showMessage("red", "Order Already Cancelled.", "");
					jQuery("#btnOrderCancel").attr('disabled',false);
					return;
				}
				var r = confirm("Do you want to cancel order-"+customerOrderNumber+" ?");
				if(r == true)
				{
				cancelCustomerOrder(customerOrderNumber,postData,id);
				}
	}
	
function cancelCustomerOrder(customerOrderNumber,postData,id)
		{
			var orderNumber = customerOrderNumber;
			var FormData = postData;
			var rowId=id;
			showMessage("loading", "Cancelling order...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSClient',
				//data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
				data:
				{
					id:"pos_cancel_order",
					moduleCode:"POS01",
					functionName:"Cancel",
					//form_data:jQuery("#pos_form_submit").serialize(),
					form_data:FormData,
					orderNumber:orderNumber,
					createdBy:createdBy,

				},
				success: function(data)
				{

					jQuery(".ajaxLoading").hide();
					
					var cancelOrderData = jQuery.parseJSON(data);
					
					if(cancelOrderData.Status == 1)
					{
						//controlOrderActionButton(5); //cancel order state on screen.
						
						var oCancelOrder = cancelOrderData.Result; // show response from the php script.
						
						if(oCancelOrder.length == 0)
						{
							showMessage("", "Unknown Order Cancellation Status from Server", "");
						}
						else
						{
							jQuery.each(oCancelOrder,function(key,value){
								var orderNo = oCancelOrder[key]['CustomerOrderNo'];
								var orderStatus = oCancelOrder[key]['Status'];
								var orderStatusId =  oCancelOrder[key]['StatusId'];
								showMessage("green", orderNo+":Order Cancelled Successfully", "");
								//controlOrderActions(orderNo, orderStatusId);
								
								//var rowId=jQuery("#myAccountSearchGrid").jqGrid('getGridParam','selrow'); 
								//var rowData = jQuery("#myAccountSearchGrid").getRowData(rowId);
								//rowData.OrderStatus= orderStatus;
								jQuery("#myAccountSearchGrid").jqGrid("setCell",rowId,"OrderStatus",orderStatus+"");
								jQuery("#btnOrderCancel").attr('disabled',false);

							});
						}
						
					}
					else
					{
						showMessage("red", cancelOrderData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
			
		}	
	
	function CODPaymentGrid()
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#CODSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Order No','Invoice No','Payment Amount','Payment Received By','Invoice Date'],
			colModel: [{ name: 'CustomerOrderNo', index: 'CustomerOrderNo', width:150 },
			           { name: 'InvoiceNo', index: 'InvoiceNo', width: 70},
			           { name: 'PaymentAmount', index: 'PaymentAmount', width: 100},
					   { name: 'PaymentReceivedBy', index: 'PaymentReceivedBy', width: 70},
					   { name: 'InvoiceDate', index: 'InvoiceDate', width: 70}],	
			           pager: jQuery('#PJmap_CODSearchGrid'),
			           width:750,
			           height:300,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           ondblClickRow: function (id) {
	                		/*var selectedLookUpCode = jQuery("#CODSearchGrid").jqGrid('getCell', id, 'p');

	                		showOrderReport(selectedLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).val(selectedLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();

	                		jQuery("#divLookUp1" ).dialog("close");*/
			        	   
			        	   return false;
	                		
	                 
	                   },
	                   onSelectRow: function (id) {
			        	   
			        	   jQuery("#btnReceivePayment").attr('disabled',false);
		               }
			          
		});

		jQuery("#CODSearchGrid").jqGrid('navGrid','#PJmap_CODSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_CODSearchGrid").hide();
		
		/*-----------------------------------------------------------------------------------------------------------*/
	}
	
	function showReceivePaymentGrid(){
		
		jQuery("#receivePaymentGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Order No','Cash','Card','Bonus','Forex','Cheque','Bank','Unpaid Amount','SuccessfulStatus','LogNo'],
			colModel: [{ name: 'CustomerOrderNo', index: 'CustomerOrderNo', width:100,summaryType:'count',summaryTpl:'<b>Total({0})</b>'},
			           { name: 'Cash', index: 'Cash', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'Card', index: 'Card', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'Bonus', index: 'Bonus', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'Forex', index: 'Forex', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'Cheque', index: 'Cheque', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'Bank', index: 'Bank', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'UnpaidAmount', index: 'UnpaidAmount', width:100,sorttype:"float", formatter:"number",summaryType:'sum'},
			           { name: 'SuccessfulStatus', index: 'SuccessfulStatus', width:150,hidden:true },
			           { name: 'LogNo', index: 'LogNo', width:150,hidden:true}
			           
			          ],	
			           pager: jQuery('#PJmap_receivePaymentGrid'),
			           width:590,
			           height:300,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           viewrecords: true,
			           sortorder: "asc",
			           hoverrows: true,
			           grouping:true,
			           groupingView : {
			          		groupField : ['LogNo'],
			          		groupSummary : [true],
			          		groupColumnShow : [true],
			          		groupText : ['<b>{0}</b>'],
			          		groupCollapse : false,
			          		groupOrder: ['asc']
			           }
			          
		});

		jQuery("#receivePaymentGrid").jqGrid('navGrid','#PJmap_receivePaymentGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_receivePaymentGrid").hide();
			
	}
	
	function showReceivePaymentData(orderType,searchParameter){
		
		showMessage("loading", "Loading payment modes", "");
		jQuery.ajax({
			
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"receivePaymentAtBranch",
				orderType:orderType,
				searchParameter:searchParameter
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var oResult = jQuery.parseJSON(data.trim());
				if(oResult.Status == 1)
				{
					jQuery("#receivePaymentGrid").jqGrid('clearGridData');
					var oLookUpData = oResult.Result;
					var tempRowId = 0;
					jQuery.each(oLookUpData,function(key,value){
						
						var unpaidAmount = 0;
						var totalPaidAmount = 0;
						var totalAmount = parseFloat(oLookUpData[key]['CashAmount']) + parseFloat(oLookUpData[key]['CardAmount']) + parseFloat(oLookUpData[key]['BonusAmount'] + parseFloat(oLookUpData[key]['OrderShippingCharges']));
						totalPaidAmount += parseFloat(oLookUpData[key]['BonusAmount']);
						if(oLookUpData[key]['SuccessfulStatus'] == 1){
							totalPaidAmount += oLookUpData[key]['CardAmount'];
						}
						if(oLookUpData[key]['PaidOrder'] == 1){ //if order paid means have to minus all amount.
							totalPaidAmount = parseFloat(oLookUpData[key]['CashAmount']) + parseFloat(oLookUpData[key]['CardAmount']) + parseFloat(oLookUpData[key]['BonusAmount'] + parseFloat(oLookUpData[key]['OrderShippingCharges']));
							//totalPaidAmount += oLookUpData[key]['CardAmount'];
						}
						unpaidAmount = totalAmount - totalPaidAmount;
						tempRowId = tempRowId + 1;
						
						var lookUpGridData = [{"CustomerOrderNo":oLookUpData[key]['CustomerOrderNo'],"Cash":formatFloatNumbers(parseFloat(oLookUpData[key]['CashAmount'])),"Card":formatFloatNumbers(parseFloat(oLookUpData[key]['CardAmount'])),
							"Bonus":formatFloatNumbers(parseFloat(oLookUpData[key]['BonusAmount'])), "Forex":formatFloatNumbers(parseFloat(oLookUpData[key]['ChequeAmount'])),
							"Cheque":formatFloatNumbers(parseFloat(oLookUpData[key]['BankAmount'])),"Bank":formatFloatNumbers(parseFloat(oLookUpData[key]['BankAmount'])),"UnpaidAmount":formatFloatNumbers(unpaidAmount),
							"LogNo":oLookUpData[key]['LogNo']}];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#receivePaymentGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}
						
					});
					
					jQuery('#receivePaymentGrid').trigger('reloadGrid');
					
					//showPaymentOnGrid();
					
					//selectAnyBranch();
				}
				else
				{
					jQuery(".ajaxLoading").hide();
					showMessage("red", oResult.Description, '');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}  
		
		});
		
	}
	
	function showPaymentOnGrid() {
        var $grid = jQuery('#receivePaymentGrid');
        var colSum = $grid.jqGrid('getCol', 'UnpaidAmount', false, 'sum');
        $grid.jqGrid('footerData', 'set', { UnpaidAmount: colSum });
   }
	
	function showOrderReport(orderNo)
	{
		//jQuery("#actionName").val('Print');
		
		showMessage("loading", "Opening Order Preview...", "");

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"OrderPrint",
				orderNo:orderNo,
				functionName:"PrintOrder",
				moduleCode:"POS01",
				originalOrDuplicate:1
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var resultsData = jQuery.parseJSON(data);
				if(resultsData.Status==1){
					var results=resultsData.Result;
					jQuery.each(results,function(key,value){
						var arr = {'locationId':results[key]['locationId'],'orderID':results[key]['orderNo'],'isDuplicate':results[key]['isDuplicate'],'GeneratedBy':loggedInUserFullName}; //invocie copy parameter need to watch

						showReport(results[key]['baseUrl'],results[key]['Url'],arr,0); //last parameter to define report called using php java bridge library.


					});
				}
				else{

					showMessage('red',resultsData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	/*
	 * My account data fill up in grid.
	 */
	/*-------------------Item look up search button click--------------------*/
	function myAccountLookUpData()
	{
		
		jQuery("#btnLookUpSearch").click(function(){
		
			
			myAccountLookUpGridDataPopulation();
			
			
		});
		
		jQuery("#itemLookUpItemName").keydown(function(e){
			
			if(e.which == 13)
			{
				myAccountLookUpGridDataPopulation();
			}
			
		});
		
		jQuery("#btnLookUpReset").click(function(){
			
			showCalender();
			
			jQuery("#myAccountSearchGrid").jqGrid('clearGridData');
			
		});
		
		jQuery("#OrderPrint").click(function(){
			var id =jQuery("#myAccountSearchGrid").getGridParam('selrow');
		var selectedLookUpCode = jQuery("#myAccountSearchGrid").jqGrid('getCell', id, 'OrderNumber');
		if(selectedLookUpCode!=''){
			showOrderReport(selectedLookUpCode);
		}
		else{
			showReport("yellow","Please select Order no",'');
			
		}
		});
		
	}
	

	/*function loadMyAccountHistoryOrder(toLoadOrNot)
	{
		if(toLoadOrNot == 0)
		{
			//Nothing
		}
		else
		{
			TOCheck();
		}
	}*/

	
	
	//alert("Yes We Can");
	function myAccountLookUpGridDataPopulation()
	{
		showMessage("loading", "Searching...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"myAccountLookUpData",
				logNo:'',
				formData:jQuery("#myAccountLookUpform").serialize(),
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var tempRowId = 0;
			
				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					jQuery("#myAccountSearchGrid").jqGrid('clearGridData');
					//alert(data);
					var oLookUpData = lookUpData.Result;
					
					
					if(oLookUpData.length == 0)
					{
						showMessage("", "No result found", "");
					}
					
					if(oLookUpData.length > 200)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(oLookUpData,function(key,value){
						
						if(key > 200) //index is starting from 0.//hence 49.
						{
							return false;
						}

						tempRowId = tempRowId + 1;
						var checkBoxId = "check"+tempRowId;
						if(oLookUpData[key]['TenderType'] == 2 && oLookUpData[key]['CardAmount'] != 0 && (oLookUpData[key]['TransactionId'] == null || oLookUpData[key]['TransactionId'] ==0) && oLookUpData[key]['LoggedInUserType'] == 'D' && oLookUpData[key]['Status'] == 3){
							//var checkBox = "<input type='checkbox' class='myAccountCheckBoxes' value='1' id='"+checkBoxId+"' />";
							var checkBox = "";
						}
						else{
							var checkBox = "";
						}
						
						var lookUpGridData = [{"Select":checkBox,"OrderNumber":oLookUpData[key]['CustomerOrderNo'], "OrderAmount":formatFloatNumbers(parseFloat(oLookUpData[key]['PaymentAmount']))
						,"CardAmount":parseFloat(oLookUpData[key]['CardAmount']),"OrderDate":displayDate(oLookUpData[key]['CreatedDate']),"LogNo":oLookUpData[key]['LogNo'],
						"OrderMode":oLookUpData[key]['OrderModeName'],"DistributorId":oLookUpData[key]['DistributorId'],
						"DistributorName":oLookUpData[key]['DistributorName'],"CourierDetail":'',"OrderStatus":oLookUpData[key]['StatusName']}];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#myAccountSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}
						
					});
					
					//notToAllowMultipleCheckBoxToSelect();
					notToAllowMultipleLogToSelect();
					jQuery('#myAccountSearchGrid').trigger('reloadGrid');
					//notToAllowMultipleCheckBoxToSelect();
					notToAllowMultipleLogToSelect();
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
				
				
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	
	/*--------------COD Payment Grid-------------------*/
	
	function CODPaymentLookUpData()
	{

		jQuery("#btnCODSearch").click(function(){


			CODLookUpGridDataPopulation();


		});

		jQuery(".clsPOPEnterField").keydown(function(e){

			if(e.which == 13)
			{
				CODLookUpGridDataPopulation();
			}

		});

		jQuery("#btnLookUpReset").click(function(){

			//showCalender();

			jQuery(".clsPOPEnterField").val('');

			jQuery("#CODSearchGrid").jqGrid('clearGridData');

		});
		jQuery("#btnReceivePayment").click(function(){

			//showCalender();
			
			var CODSearchGridRow = jQuery('#CODSearchGrid').jqGrid('getGridParam','selrow');
			
			if(CODSearchGridRow){
				
				var selectedRowDataObj = jQuery("#CODSearchGrid").jqGrid('getRowData',CODSearchGridRow);
				
				receiveCODPayment(selectedRowDataObj);
				
			}
			else{
				showMessage("", "Select any row to receive payment", "");
			}

			

		});
		



	}
	

	/*function loadMyAccountHistoryOrder(toLoadOrNot)
	{
		if(toLoadOrNot == 0)
		{
			//Nothing
		}
		else
		{
			TOCheck();
		}
	}*/

	function receiveCODPayment(selectedRowDataObj){
		
		var CODPaymentInvoiceNo = selectedRowDataObj.InvoiceNo;
		
		showMessage("loading", "Receiving Payment ...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"receiveCODPayment",
				moduleCode:"POS01",
				functionName:"CODPayment",
				CODPaymentInvoiceNo:CODPaymentInvoiceNo,
				CODReceiveComment:jQuery("#txtCODReceivePaymentComment").val()
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
			
				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					if(lookUpData.Result[0].UpdateStatus == 1){
						showMessage("green", "COD Payment Received Successfully", "");
					}
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	//alert("Yes We Can");
	function CODLookUpGridDataPopulation()
	{
		showMessage("loading", "Searching...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"CODLookUpData",
				formData:jQuery("#CODPaymentLookUpForm").serialize()
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var tempRowId = 0;
			
				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					jQuery("#CODSearchGrid").jqGrid('clearGridData');
					//alert(data);
					var oLookUpData = lookUpData.Result;
					
					
					if(oLookUpData.length == 0)
					{
						showMessage("", "No result found", "");
					}
					
					if(oLookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(oLookUpData,function(key,value){
						
						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}

						tempRowId = tempRowId + 1;
						
						var lookUpGridData = [{"CustomerOrderNo":oLookUpData[key]['CustomerOrderNo'], "InvoiceNo":oLookUpData[key]['InvoiceNo']
						,"PaymentAmount":formatFloatNumbers(oLookUpData[key]['PaymentAmount']),"PaymentReceivedBy":oLookUpData[key]['PaymentReceivedBy'],
						"InvoiceDate":displayDate(oLookUpData[key]['InvoiceDate'])}];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#CODSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}
						
					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
				
				
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	

	function itemSearchGrid(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#itemSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','Distributor Price','Promotion Participation'],
			colModel: [{ name: 'ItemCode', index: 'ItemCode', width:100 },
			           { name: 'ItemName', index: 'ItemName', width: 150},
					   { name: 'DistributorPrice', index: 'DistributorPrice', width: 150},
					   { name: 'PromotionParticipation', index: 'PromotionParticipation', width: 150}],	
			           pager: jQuery('#PJmap_ItemSearchGrid'),
			           width:560,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,


			           afterInsertRow : function(ids)
			           {
//			        	   alert("ids is" + ids);
			        	   var index1 = jQuery("#ShortInventoryGrid").jqGrid('getCol','Name',false);
//			        	   alert("after insert row" + index1);
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	   jQuery("tr.jqgrow").css("background", "#DDDDDC");
			           },
			           onSelectRow: function (id) {
	                		var selectedItemLookUpCode = jQuery("#itemSearchGrid").jqGrid('getCell', id, 'ItemCode');
	                		jQuery("#barcode_value").val(selectedItemLookUpCode);
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");	                 
	                   } 
			          
		});

		jQuery("#itemSearchGrid").jqGrid('navGrid','#PJmap_ItemSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_ItemSearchGrid").hide();
		
		/*-----------------------------------------------------------------------------------------------------------*/
	}
	
	

	
	/*-------------------Item look up search button click--------------------*/
	function itemLookUpData()
	{
		
		jQuery("#btnItemSearchLookUpSearch").click(function(){
		
			
			itemLookUpGridDataPopulation();
			
			
		});
		
		jQuery("#itemLookUpItemName").keydown(function(e){
			
			if(e.which == 13)
			{
				itemLookUpGridDataPopulation();
			}
			
		});
		
		jQuery("#btnItemSearchLookUpReset").click(function(){
			
			jQuery("#itemLookUpItemCode").val('');
			jQuery("#itemLookUpItemName").val('');
			jQuery("#itemSearchGrid").jqGrid('clearGridData');
			
		});
		
	}
	
	
	//alert("Yes We Can");
	function itemLookUpGridDataPopulation()
	{
		var itemName = jQuery("#itemLookUpItemName").val();
		var ItemCode = jQuery("#itemLookUpItemCode").val();
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"itemLookUpData",
				itemName:itemName,
				itemCode:ItemCode
			},
			success:function(data)
			{
				var tempRowId = 0;
			
				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					jQuery("#itemSearchGrid").jqGrid('clearGridData');
					//alert(data);
					var oItemLookUpData = lookUpData.Result;
					
					if(oItemLookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(oItemLookUpData,function(key,value){
						
						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}

						tempRowId = tempRowId + 1;
						
						var itemLookUpGridData = [{"ItemCode":oItemLookUpData[key]['ItemCode'], "ItemName":oItemLookUpData[key]['ItemName']
						,"DistributorPrice":parseFloat(oItemLookUpData[key]['DistributorPrice']),"PromotionParticipation":oItemLookUpData[key]['PromotionParticipation']}];
						for (var i=0;i<itemLookUpGridData.length;i++) {
							jQuery("#itemSearchGrid").jqGrid('addRowData', tempRowId, itemLookUpGridData[itemLookUpGridData.length-i-1], "last");
						}
						
					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
				
				
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	
	/**
	 * function used to provide purchase order search grid.
	 * @param fieldIdForItemSearch
	 */
	
	function POSearchGrid(fieldIdForPOSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#POSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['PO No','PO Date','PO Type','PO Status',' Vendor Code'],
			colModel: [{ name: 'PONo', index: 'PONo', width:150 },
			           { name: 'PODate', index: 'PODate', width: 150},
					   { name: 'POType', index: 'POType', width: 150 ,hidden:true },
					   { name: 'POStatus', index: 'POStatus', width: 150},
					   { name: 'VendorCode', index: 'VendorCode', width: 150}],	
			           pager: jQuery('#PJmap_POSearchGrid'),
			           width:560,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           ondblClickRow: function (id) {
	                		var selectedValue = jQuery("#POSearchGrid").jqGrid('getCell', id, 'PONo');
	                		
	                		jQuery("#"+fieldIdForPOSearch).val(selectedValue);
	                		jQuery("#"+fieldIdForPOSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#POSearchGrid").jqGrid('navGrid','#PJmap_POSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_POSearchGrid").hide();
		
		/*-----------------------------------------------------------------------------------------------------------*/
	}
	
	
	/*-------------------PO search button click--------------------*/
	function POLookUpData()
	{
		
		jQuery("#btnPOLookUpSearch").click(function(){
		
			
			POLookUpGridDataPopulation();
			
			
		});
		
		jQuery("#txtPONumber").keydown(function(e){
			
			if(e.which == 13)
			{
				POLookUpGridDataPopulation();
			}
			
		});
		
		jQuery("#btnPOLookUpReset").click(function(){
			
			jQuery("#txtPONumber").val('');
			jQuery("#txtVendorCode").val('');
			jQuery("#POSearchGrid").jqGrid('clearGridData');
			
		});
		
	}
	
	
	//alert("Yes We Can");
	function POLookUpGridDataPopulation()
	{
		var PONumber= jQuery("#txtPONumber").val();
		var VendorCode = jQuery("#txtVendorCode").val();
		showMessage("loading", "Searching availble POnumber...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"POLookUpData",
				PONumber:PONumber,
				VendorCode:VendorCode
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var tempRowId = 0;
			
				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					jQuery("#POSearchGrid").jqGrid('clearGridData');
					//alert(data);
					var olookUpData = lookUpData.Result;
					
					if(olookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(olookUpData,function(key,value){
						
						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}

						tempRowId = tempRowId + 1;
						var lookUpGridData = [{"PONo":olookUpData[key]['PONo'], "PODate":displayDate(olookUpData[key]['PODate'])
						,"POType":olookUpData[key]['POType'],"POStatus":olookUpData[key]['POStatus'],"VendorCode":olookUpData[key]['VendorCode']}];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#POSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}
						
					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
				
				
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	
	
	/*------------GRN Lookup for vendorReturn---------------*/
	
		function GRNSearchLookupGridForVR(fieldIdForGRNSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#GRNLookupSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['GRN No','GRN Date','GRN Type','GRN Status',' Vendor Code'],
			colModel: [{ name: 'GRNNo', index: 'GRNNo', width:200 },
			           { name: 'GRNDate', index: 'GRNDate', width: 150},
			           { name: 'GRNType', index: 'POType', width: 150 ,hidden:true },
					   { name: 'GRNStatus', index: 'GRNStatus', width: 100},
					   { name: 'VendorCode', index: 'VendorCode', width: 150}],	
			           pager: jQuery('#PJmap_POSearchGrid'),
			           width:560,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           ondblClickRow: function (id) {
	                		var selectedValue = jQuery("#GRNLookupSearchGrid").jqGrid('getCell', id, 'GRNNo');
	                		
	                		jQuery("#"+fieldIdForGRNSearch).val(selectedValue);
	                		jQuery("#"+fieldIdForGRNSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#GRNLookupSearchGrid").jqGrid('navGrid','#PJmap_GRNLookupSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNLookupSearchGrid").hide();
		
		/*-----------------------------------------------------------------------------------------------------------*/
	}
	
	
	
	
	function GRNLookUpDataForVR()
	{
		
		jQuery("#btnGRNLookUpSearch").click(function(){
		
			
			GRNLookUpGridDataPopulationForVR();
			
			
		});
		
		jQuery("#txtGRNNumber").keydown(function(e){
			
			if(e.which == 13)
			{
				GRNLookUpGridDataPopulationForVR();
			}
			
		});
		
		jQuery("#btnGRNLookUpReset").click(function(){
			
			jQuery("#txtGRNNumber").val('');
			jQuery("#txtVendorCode").val('');
			jQuery("#GRNLookupSearchGrid").jqGrid('clearGridData');
			
		});
		
	}
	
	
	//-------------Method to population of lookUpGridData for vendor return--------------------*/
	
	
	function GRNLookUpGridDataPopulationForVR()
	{
		var GRNNumber= jQuery("#txtGRNNumber").val();
		var VendorCode = jQuery("#txtVendorCode").val();
		showMessage("loading", "Searching available GRN Number...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"GRNLookUpDataForVR",
				GRNNumber:GRNNumber,
				VendorCode:VendorCode
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var tempRowId = 0;
			
				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					jQuery("#GRNLookupSearchGrid").jqGrid('clearGridData');
					//alert(data);
					var olookUpData = lookUpData.Result;
					
					if(olookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(olookUpData,function(key,value){
						
						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}

						tempRowId = tempRowId + 1;
						var lookUpGridData = [{
							"GRNNo":olookUpData[key]['GRNNo'], 
							"GRNDate":displayDate(olookUpData[key]['GRNDate']),
							//"POType":olookUpData[key]['POType'],
						    "GRNStatus":olookUpData[key]['Status'],
						    "VendorCode":olookUpData[key]['VendorCode']}];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#GRNLookupSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}
						
					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
				
				
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	
	
	
	
	/*--------------------RTGS Code Look upGrid---------------------------*/
	function distributorRTGSlookUpGrid(fieldIdForDistributorSearch,currentRTGSCodeValue)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*distributor search grid. 
		 * provide only grid to show. Without any data in that grid.

		 */
		jQuery("#distributorRTGSSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['RTGS/IFSC Code','Branch Name','Branch Name'],
			colModel: [{ name: 'RTGSCode', index: 'RTGSCode', width:100 },
			           { name: 'BranchName', index: 'BranchName', width:250 },
			           { name: 'BankName', index: 'BankName', width:250}
			          ],
			           pager: jQuery('#PJmap_distributorRTGSSearchGrid'),
			           width:600,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
	                		var selectedDistributorRTGSCode = jQuery("#distributorRTGSSearchGrid").jqGrid('getCell', id, 'RTGSCode');
	                		
	                		
	                		jQuery("#"+fieldIdForDistributorSearch).val(selectedDistributorRTGSCode);
	                		jQuery("#"+fieldIdForDistributorSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                		
	                		
	                		//getBankAccountFixedLength(selectedBankId); //to define length for account number. //func in distributor registration .js
	                 
	                   } 
			          
		});

		jQuery("#distributorRTGSSearchGrid").jqGrid('navGrid','#PJmap_distributorRTGSSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_distributorRTGSSearchGrid").hide();
	}
	
	
	/*--------------------RTGS look up data-----------------------------*/
	
	function distributorRTGSLookUpData(currentRTGSCodeValue)
	{
		jQuery("#txtRTGSCode").val(currentRTGSCodeValue);
		
		
		if(currentRTGSCodeValue != '')
		{
			searchRTGSCode(currentRTGSCodeValue);
		}
		
		
		
		jQuery("#btnDistributorRTGSLookUpSearch").click(function(){ // button made in look up data html screen in ui/lookupScreensUI
			
			var distributorRTGSValue = jQuery("#txtRTGSCode").val();
			
			searchRTGSCode(distributorRTGSValue);
			
		});
		
		jQuery("#btnDistributorLookUpReset").click(function(){
			
			jQuery("#distributorRTGSSearchGrid").jqGrid("clearGridData");
			
			jQuery("#txtRTGSCode").val('');
			
		});
		
		jQuery("#txtRTGSCode").click(function(e){
			
			if(e.which == 13)
			{
				var distributorRTGSValue = jQuery("#txtRTGSCode").val();
				
				searchRTGSCode(distributorRTGSValue);
			}
			
		});
	}
	
	/*------------different approah for searching when value is present in field.-------------*/
	function searchRTGSCode(currentRTGSCodeValue)
	{
		
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"distributorRTGSSearchLookUpData",
				distributorRTGSCode:jQuery("#txtRTGSCode").val(),
				
			},
			success:function(data)
			{
				
				//alert(data);
				var tempRowId = 0; 

				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					jQuery("#distributorRTGSSearchGrid").jqGrid("clearGridData");
					
					var oLookUpData = lookUpData.Result;
					
					if(oLookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(oLookUpData,function(key,value){

						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}
						
						tempRowId = tempRowId + 1;
						
						
						var lookUpGridData = [{"RTGSCode":oLookUpData[key]['BankBranckCode']
						, "BranchName":oLookUpData[key]['BranchName'],"BankName":oLookUpData[key]['BankName']
						,"BankId":oLookUpData[key]['BankId'],"FixedLength":oLookUpData[key]['FixedLength']}];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#distributorRTGSSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}
						
					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
				
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	/*-------------------Distributor Look up data to show in grid.--------------------*/
	function distributorLookUpData()
	{
		jQuery("#btnDistributorLookUpSearch").click(function(){ // button made in look up data html screen in ui/lookupScreensUI
			
			distributorLookUpSearchResultPopulation();
			
		});
		
		jQuery("#txtDistributorName").keydown(function(e){
			
			if(e.which == 13)
			{
				distributorLookUpSearchResultPopulation();
			}
			
		});
		
		jQuery("#btnDistributorLookUpReset").click(function(){
			
			jQuery("#distributorSearchGrid").jqGrid("clearGridData");
			
			jQuery("#txtDistributorName").val('');
			
		});
	}
	
	
	function distributorLookUpSearchResultPopulation()
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"distributorSearchLookUpData",
				formData:jQuery("#distributorSearchLookUpform").serialize(),
				
			},
			success:function(data)
			{
				
				//alert(data);
				var tempRowId = 0; 

				var lookUpData = jQuery.parseJSON(data);
				
				if(lookUpData.Status == 1)
				{
					
					jQuery("#distributorSearchGrid").jqGrid("clearGridData");
					
					var oLookUpData = lookUpData.Result;
					
					if(oLookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}
					
					jQuery.each(oLookUpData,function(key,value){

						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}
						
						tempRowId = tempRowId + 1;
						
						var itemLookUpGridData = [{"DistributorId":oLookUpData[key]['DistributorId']
						, "DistributorFirstName":oLookUpData[key]['DistributorFirstName'],
						"City":oLookUpData[key]['City'],
						"State":oLookUpData[key]['State']
						,"Status":oLookUpData[key]['Status']}];
						for (var i=0;i<itemLookUpGridData.length;i++) {
							jQuery("#distributorSearchGrid").jqGrid('addRowData', tempRowId, itemLookUpGridData[itemLookUpGridData.length-i-1], "last");
						}
						
					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}
			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	/*---------------------------------------*/
	function locationsLookUpData() //s for because composite item look up name has been assigned by someone vishal to location look up data.
	{
			jQuery("#btnLocationLookupSearch").click(function(){ 
				showMessage("loading", "Searching locations...", "");	
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:"LocationLookUpData",
					locationCode:jQuery("#locationCode").val(),
					statecode:jQuery("#statecode").val(),
					locationName:jQuery("#locatioName").val(),
					
				},
				success:function(data)
				{
					
					jQuery(".ajaxLoading").hide();
					//alert(data);
					var tempRowId = 0; 
	
					var lookUpData = jQuery.parseJSON(data);
					
					if(lookUpData.Status == 1)
					{
						
						jQuery("#locationSearchGrid").jqGrid("clearGridData");
						
						var oLookUpData = lookUpData.Result;
						
						if(oLookUpData.length > 50)
						{
							showMessage("", "More result available refine your search", "");
						}
						
						jQuery.each(oLookUpData,function(key,value){

							tempRowId = tempRowId + 1;
							
							var lookUpGridData = [{"LocationName":oLookUpData[key]['Name'],"StateName":oLookUpData[key]['StateName'],
							"StateCode":oLookUpData[key]['StateCode'], "LocationCode":oLookUpData[key]['LocationCode'],
							"LocationId":oLookUpData[key]['LocationId']}];
							for (var i=0;i<lookUpGridData.length;i++) {
								jQuery("#locationSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
							}
							
						});
					}
					else
					{
						showMessage("red", lookUpData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
			
		});
		jQuery("#btnLocationLookUpReset").click(function(){ 
		
			jQuery('#locationCode').val('');
			jQuery("#statecode").val('');
			jQuery("#locatioName").val('');
		});
		
	}
	
	/*-------------------TOI search button click--------------------*/
	function TOILookUpData(fieldIdForItemSearch)
	{
		showCalender();
		jQuery("#btnTOILookUpSearch").click(function(){
			
			toiGridDataPopulation();
			
			
		});
		
		jQuery("#TOILookUpTOINumber").keydown(function(e){
			
			if(e.which == 13)
			{
				toiGridDataPopulation();
			}
			
		});
		
	}
	
	function toiGridDataPopulation()
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"TOILookUpData",
				formData:jQuery("#TOILookUpform").serialize(),
			},
			success:function(data)
			{
				var sTOISearchData = jQuery.parseJSON(data);
				
				var tempRowId = 0;
				
				jQuery("#TOISearchGrid").jqGrid("clearGridData");
		if(sTOISearchData.Status==1){
			sTOISearch=sTOISearchData.Result;
			if(sTOISearch.length>0){
				jQuery.each(sTOISearch,function(key,value){	
					
					tempRowId = tempRowId + 1;
					
					var newData = [{"TOINumber":sTOISearch[key]['TOINumber'],
									"DestinationAddress":fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId', sTOISearch[key]['DestinationLocationId'],'DisplayName'),
									"SourceAddress":fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId', sTOISearch[key]['SourceLocationId'],'DisplayName'),
									"TOICreationDate":sTOISearch[key]['CreationDate'],
									"TOIDate":sTOISearch[key]['TOIDate'],
									
									"TotalTOIQuantity":parseFloat(sTOISearch[key]['TotalTOIQuantity'],10).toFixed(2),
									
									"TotalTOIAmount":parseFloat(sTOISearch[key]['TotalTOIAmount'],10).toFixed(2),
							
								  }];
						
					for (var i=0;i<newData.length;i++) {
						
					jQuery("#TOISearchGrid").jqGrid('addRowData',tempRowId, newData[newData.length-i-1], "first");
					
					}
				});
			}
			else{
				showMessage('red',"No record found",'');
			}
				
		}
		else{
			showMessage('red',sTOISearchData.Description,'');
		}
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	/*-------------------TO search button click--------------------*/
	function TOLookUpData()
	{

		function TOLookUpReset()
		{
			
			showCalender();
			jQuery("#TOLookUpTONumber").val('');
			
			jQuery("#TOLookUpIndentised").attr("checked",false);
		}
		
		
		jQuery("#btnTOLookUpSearch").click(function(){	
			
			toGridDataPopulation();
			
		});
		
		jQuery("#btnTOLookUpReset").click(function(){

			TOLookUpReset(); //Reset TO Look Up.
			
		});
	
		showCalender();
		/*jQuery("#TOLookUpTONumber").keyDown(function(e){
			
			if(e.which == 13)
			{
				toGridDataPopulation();
			}
			
		});*/
		
	}
	function TOLookUpData2()
	{

		function TOLookUpReset2()
		{
			
			showCalender();
			jQuery("#TOLookUpTONumber").val('');
			
			jQuery("#TOLookUpIndentised").attr("checked",false);
		}
		
		
		jQuery("#btnTOLookUpSearch").click(function(){	
			
			toGridDataPopulation2();
			
		});
		
		jQuery("#btnTOLookUpReset").click(function(){

			TOLookUpReset2(); //Reset TO Look Up.
			
		});
		showCalender();
		
		/*jQuery("#TOLookUpTONumber").keyDown(function(e){
			
			if(e.which == 13)
			{
				toGridDataPopulation2();
			}
			
		});*/
		
	}
	function toGridDataPopulation2()
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"TOLookUpData2",
				formData:jQuery("#TOLookUpform").serialize(),
			},
			success:function(data)
			{
				
				var sTOSearchData = jQuery.parseJSON(data);
				var tempRowId = 0;
				jQuery("#TOSearchGrid").jqGrid("clearGridData");
			if(sTOSearchData.Status==1){
				oTOSearchData=sTOSearchData.Result;
				if(oTOSearchData.length>0){
				jQuery.each(oTOSearchData,function(key,value){	
					
					tempRowId = tempRowId + 1;
					
					var newData = [{"TOINumber":oTOSearchData[key]['TOINumber'],
									"TONumber":oTOSearchData[key]['TONumber'],	
									"DestinationAddress":fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId', oTOSearchData[key]['DestinationLocationId'],'DisplayName'),
									"SourceAddress":fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId', oTOSearchData[key]['SourceLocationId'],'DisplayName'),
									"TOCreationDate":oTOSearchData[key]['CreationDate'],
									"TOIDate":oTOSearchData[key]['TOIDate'],
									
									"TotalTOIQuantity":parseFloat(oTOSearchData[key]['TotalTOQuantity'],10).toFixed(2),
									
									"TotalTOIAmount":parseFloat(oTOSearchData[key]['TotalTOAmount'],10).toFixed(2),
									
									"Status":oTOSearchData[key]['StatusName'],
							
								  }];
						
					for (var i=0;i<newData.length;i++) {
						
					jQuery("#TOSearchGrid").jqGrid('addRowData',tempRowId, newData[newData.length-i-1], "first");
					
					}
				});
				}
				else{
					showMessage('red','No record found','');
				}
			}
			else{
				showMessage('red',oTOSearchData.Description,'');
			}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	function toGridDataPopulation()
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"TOLookUpData",
				formData:jQuery("#TOLookUpform").serialize(),
			},
			success:function(data)
			{
				
				var sTOSearchData = jQuery.parseJSON(data);
				var tempRowId = 0;
				jQuery("#TOSearchGrid").jqGrid("clearGridData");
			if(sTOSearchData.Status==1){
				oTOSearchData=sTOSearchData.Result;
				if(oTOSearchData.length>0){
				jQuery.each(oTOSearchData,function(key,value){	
					
					tempRowId = tempRowId + 1;
					
					var newData = [{"TOINumber":oTOSearchData[key]['TOINumber'],
									"TONumber":oTOSearchData[key]['TONumber'],	
									"DestinationAddress":fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId', oTOSearchData[key]['DestinationLocationId'],'DisplayName'),
									"SourceAddress":fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId', oTOSearchData[key]['SourceLocationId'],'DisplayName'),
									"TOCreationDate":oTOSearchData[key]['CreationDate'],
									"TOIDate":oTOSearchData[key]['TOIDate'],
									
									"TotalTOIQuantity":parseFloat(oTOSearchData[key]['TotalTOQuantity'],10).toFixed(2),
									
									"TotalTOIAmount":parseFloat(oTOSearchData[key]['TotalTOAmount'],10).toFixed(2),
									
									"Status":oTOSearchData[key]['StatusName'],
							
								  }];
						
					for (var i=0;i<newData.length;i++) {
						
					jQuery("#TOSearchGrid").jqGrid('addRowData',tempRowId, newData[newData.length-i-1], "first");
					
					}
				});
				}
				else{
					showMessage('red','No record found','');
				}
			}
			else{
				showMessage('red',oTOSearchData.Description,'');
			}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	/*-------------------TO search button click--------------------*/
	function compositeItemLookUpData()
	{
		
		jQuery("#btnCompositeItemLookUpSearch").click(function(){
			
			compositeItemGridDataPopulation();
			
			
		});
		
		jQuery("#packUnpackCreateItemCode").keydown(function(e){
			
			if(e.which == 13)
			{
				compositeItemGridDataPopulation();
			}
				
		});
		
	
	}
	
	
	function compositeItemGridDataPopulation()
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"CompositeItemLookUpData",
				formData:jQuery("#CompositeItemLookUpform").serialize(),
			},
			success:function(data)
			{
				
				var oCompositeItemSearchData = jQuery.parseJSON(data);
				
				var tempRowId = 0;
				
				jQuery("#compositeItemSearchGrid").jqGrid("clearGridData");
				
				jQuery.each(oCompositeItemSearchData,function(key,value){	
					
					tempRowId = tempRowId + 1;
					
					var newData = [{"Code":oCompositeItemSearchData[key]['ItemCode'],
									"Name":oCompositeItemSearchData[key]['ItemName'],	
									"ShortName":oCompositeItemSearchData[key]['ShortName'],
									"DisplayName":oCompositeItemSearchData[key]['DisplayName'],
									"ParentHeirarchy":oCompositeItemSearchData[key]['Name'],
								  }];
						
					for (var i=0;i<newData.length;i++) {
						
					jQuery("#compositeItemSearchGrid").jqGrid('addRowData',tempRowId, newData[newData.length-i-1], "first");
					
					}
				});
				

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	function manufactureBatchLookUpCR(searchScreen,itemCode)
	 {
	  jQuery("#btnManufactureBatchLookUpSearch").click(function(){

	   searchBatchUsingCodeCR(searchScreen,itemCode);
	   
	  });
	  
	  jQuery("#manufactureBatchNo").keydown(function(e){
	   
	   if(e.which == 13)
	   {
		   searchBatchUsingCodeCR(searchScreen,itemCode);
	   }
	   
	  });
	  
	 }
	 function searchBatchUsingCodeCR(searchScreen,itemCode){

	  var itemCodeOrItemId;

	  if(searchScreen == 'CR')
	  {
	   locationId = null;
	   itemid = itemCode; //Instead of item id there is item code that's why itemCodeOrItemId flag is 1.
	   bucketId = 5 ; //For customer return to search in bucket of on-hand only.
	   SACreateFromBatchNo=jQuery("#manufactureBatchNo").val() ;
	   var itemCodeOrItemId = 1;

	  }
	  else
	  {
	   var locationId=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationId');
	   var itemId=fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId');
	   bucketId=  jQuery("#SACreateFromBucket").val() ;
	   SACreateFromBatchNo=jQuery("#manufactureBatchNo").val() ;
	   var itemCodeOrItemId = 0;

	  }
	  var rowid=0;
	  showMessage("loading", "Search for Batch No...", "");
	  jQuery.ajax({
	   type:'POST',
	   url:Drupal.settings.basePath + 'StockAdjustmentCallback',
	   data:
	   {
	    id:"searchBatchUsingCR",
	    bNo:SACreateFromBatchNo,
	    itemId:itemCode,
	    searchScreen:searchScreen,
	    itemCodeOrItemId:itemCodeOrItemId,
	    bucketId:bucketId
	   },
	   success:function(data)
	   {

	    rowid = 0;
	    
	    jQuery(".ajaxLoading").hide();
	    jQuery("#SACreateFromBatchNo").css("background","white");
	    resultArrData = jQuery.parseJSON(data);
	    if(resultArrData.Status==1){
	     bucketBatchLocWiseQty=resultArrData.Result;
	     resultArr=resultArrData.Result;
	     
	     if(resultArr.length == 0)
	     {
	      showMessage("", "No result found", "");
	     }

	     if(resultArr.length > 50)
	     {
	      showMessage("", "More result available refine your search", "");
	     }
	     
	     
	     if(resultArr.length!=0){
	      jQuery.each(resultArr,function(key,value){
	       
	       if(key > 49) //index is starting from 0.//hence 49.
	       {
	        return false;
	       }

	       rowid=rowid+1;
	       
	       
	       var newData = [{
	        "ManufacturingCode":resultArr[key]['ManufacturingCode'],
	        "ItemName":resultArr[key]['ItemName'],
	        "ItemCode":resultArr[key]['ItemCode'],
	        "MRP":parseFloat(resultArr[key]['MRP'],10).toFixed(2),
	        "Quantity":(resultArr[key]['Quantity'] != null)?parseFloat(resultArr[key]['Quantity'],10).toFixed(0):'-',
	        "Weight":(resultArr[key]['Weight'] != null)?parseFloat(resultArr[key]['Weight'],10).toFixed(2):'-',
	        "MfgDate":displayDate(resultArr[key]['MfgDate']),
	        "ExpDate":displayDate(resultArr[key]['ExpDate']),
	        "UOMId":resultArr[key]['UOMId'],
	        "UOMName":resultArr[key]['UOMName'],
	        "BatchNo":resultArr[key]['BatchNo'],

	       }];

	       for (var i=0;i<newData.length;i++) {
	        jQuery("#manufactureBatchSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
	        
	       }

	        });
	     }
	     else {

	      showMessage("red","NO Record found .","");
	     }
	    }
	    else{
	     showMessage('red',resultArrData.Description,'');
	    }

	   },
	   error: function(XMLHttpRequest, textStatus, errorThrown) { 

	    jQuery(".ajaxLoading").hide();
	    var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
	    showMessage("", defaultMessage, "");
	   }

	  });




	 }
	
	/*-------------------manufacture batch look up grid data population--------------------*/
	//function manufactureBatchLookUpData(searchScreen,itemCode)
	function manufactureBatchLookUpData(typeForBatchSearch)
	{
		jQuery("#btnManufactureBatchLookUpSearch").click(function(){

			//searchBatchUsingCode(searchScreen,itemCode);
			searchBatchUsingCode();
			
		});
		searchBatchUsingCode();
		
		jQuery("#manufactureBatchNo").keydown(function(e){
			
			if(e.which == 13)
			{
				//searchBatchUsingCode(searchScreen,itemCode);
				searchBatchUsingCode();
			}
			
		});
		
	}
	

	 function searchBatchUsingCode( ){
		  
		  var locationId=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationId');
	    	var itemId=fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId');
	    	bucketId=  jQuery("#SACreateFromBucket").val() ;
	    	jQuery("#manufactureBatchSearchGrid").jqGrid("clearGridData");
	    	SACreateFromBatchNo=jQuery("#manufactureBatchNo").val() ;
	    	var rowid=0;
		  showMessage("loading", "Search for Batch No...", "");
		   jQuery.ajax({
			   type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchBatchUsingCode",
					bNo:SACreateFromBatchNo,
					locationId:locationId,
					itemId:itemId,
					bucketId:bucketId
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					jQuery("#SACreateFromBatchNo").css("background","white");
					 resultArrData = jQuery.parseJSON(data);
			if(resultArrData.Status==1){
				bucketBatchLocWiseQty=resultArrData.Result;
				resultArr=resultArrData.Result;
				if(resultArr.length!=0){


							var rowid= 0;
							jQuery.each(resultArr,function(key,value)							{
								var newData = [{
									"ManufacturingCode":resultArr[key]['ManufacturingCode'],
									"ItemName":resultArr[key]['ItemName'],
									"ItemCode":resultArr[key]['ItemCode'],
									"MRP":parseFloat(resultArr[key]['MRP'],10).toFixed(2),
									"Quantity":parseFloat(resultArr[key]['Quantity'],10).toFixed(0),
									"Weight":parseFloat(resultArr[key]['Weight'],10).toFixed(2),
									"MfgDate":displayDate(resultArr[key]['MfgDate']),
									"ExpDate":displayDate(resultArr[key]['ExpDate']),
									"UOMId":resultArr[key]['UOMId'],
									"UOMName":resultArr[key]['UOMName'],
									"BatchNo":resultArr[key]['BatchNo'],
											
								}];
								rowid += 1;
								for (var i=0;i<newData.length;i++) {

								jQuery("#manufactureBatchSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "last");
									
								}
							 
						});
				   }
				else {
					
			        showMessage("red","NO Record found .","");
			        		}
			}
			else{
						showMessage('red',resultArrData.Description,'');
									
				}
			},
								  error: function(XMLHttpRequest, textStatus, errorThrown) { 

									  jQuery(".ajaxLoading").hide();
										var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
										showMessage("", defaultMessage, "");
									}
								
								});
							} 


	
	
	/*-------------------TO search button click--------------------*/
/*	function locationLookUpData()
	{
		jQuery("#btnLocationLookupSearch").click(function(){
			
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:"LocationLookUpData",
					formData:jQuery("#CompositeItemLookUpform").serialize(),
				},
				success:function(data)
				{
					
					var oCompositeItemSearchData = jQuery.parseJSON(data);
					
					var tempRowId = 0;
					
					jQuery("#compositeItemSearchGrid").jqGrid("clearGridData");
					
					jQuery.each(oCompositeItemSearchData,function(key,value){	
						
						tempRowId = tempRowId + 1;
						
						var newData = [{"Code":oCompositeItemSearchData[key]['ItemCode'],
										"Name":oCompositeItemSearchData[key]['ItemName'],	
										"ShortName":oCompositeItemSearchData[key]['ShortName'],
										"DisplayName":oCompositeItemSearchData[key]['DisplayName'],
										"ParentHeirarchy":oCompositeItemSearchData[key]['Name'],
									  }];
							
						for (var i=0;i<newData.length;i++) {
							
						jQuery("#compositeItemSearchGrid").jqGrid('addRowData',tempRowId, newData[newData.length-i-1], "first");
						
						}
					});
					
	
				}
			
			});
			
		});
	}
	
*/
	/*
	 * TOI LookUp grid create dynamically called from TransferOut.js
	 */
	function TOISearchGrid(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#TOISearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['TOI Number','Source Location','Destination Location','TOI CreationDate','TOI Date','Total TOI Quantity','Total TOI Amount'],
			colModel: [{ name: 'TOINumber', index: 'TOINumber', width:100 },
			           { name: 'SourceAddress', index: 'SourceAddress', width: 150},
			           { name: 'DestinationAddress', index: 'DestinationAddress', width: 150},
			           { name: 'TOICreationDate', index: 'TOICreationDate', width: 150},
			           { name: 'TOShipDate', index: 'TOIDate', width: 150,hidden:true},
			           { name: 'TotalTOIQuantity', index: 'TotalTOIQuantity', width: 150},
			           { name: 'TotalTOIAmount', index: 'TotalTOIAmount', width: 150}],
			           pager: jQuery('#PJmap_TOISearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,


			           afterInsertRow : function(ids)
			           {
		        	   
			        	   var index1 = jQuery("#ShortInventoryGrid").jqGrid('getCol','Name',false);

			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

			           },
			           ondblClickRow: function (id) {
			        	   
	                		var selectedItemLookUpCode = jQuery("#TOISearchGrid").jqGrid('getCell', id, 'TOINumber');
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#TOISearchGrid").jqGrid('navGrid','#PJmap_TOISearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_TOISearchGrid").hide();
		
		
	}
	
	
	/*
	 * TO LookUp grid create dynamically called from TransferIn.js
	 */
	function TOSearchGrid(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#TOSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['TOI Number','To Number','Source Location','Destination Location','TO CreationDate','TO Ship Date','Total TOI Quantity','Total TOI Amount','Status'],
			colModel: [{ name: 'TOINumber', index: 'TOINumber', width:100 },
			           { name: 'TONumber', index: 'TONumber', width:100 },
			           { name: 'SourceAddress', index: 'SourceAddress', width: 150},
			           { name: 'DestinationAddress', index: 'DestinationAddress', width: 150},
			           { name: 'TOCreationDate', index: 'TOICreationDate', width: 150},
			           { name: 'TOShipDate', index: 'TOIDate', width: 150,hidden:true},
			           { name: 'TotalTOIQuantity', index: 'TotalTOIQuantity', width: 150},
			           { name: 'TotalTOIAmount', index: 'TotalTOIAmount', width: 150},
			           { name: 'Status', index: 'Status', width: 150}],
			           pager: jQuery('#PJmap_TOSearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,


			           afterInsertRow : function(ids)
			           {
		        	   
			        	   var index1 = jQuery("#ShortInventoryGrid").jqGrid('getCol','Name',false);

			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

			           },
			           ondblClickRow: function (id) {
			        	   
	                		var selectedItemLookUpCode = jQuery("#TOSearchGrid").jqGrid('getCell', id, 'TONumber');
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#TOSearchGrid").jqGrid('navGrid','#PJmap_TOSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_TOSearchGrid").hide();
		
		
	}
	function TOSearchGrid2(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#TOSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['TOI Number','To Number','Source Location','Destination Location','TO CreationDate','TO Ship Date','Total TOI Quantity','Total TOI Amount','Status'],
			colModel: [{ name: 'TOINumber', index: 'TOINumber', width:100 },
			           { name: 'TONumber', index: 'TONumber', width:100 },
			           { name: 'SourceAddress', index: 'SourceAddress', width: 150},
			           { name: 'DestinationAddress', index: 'DestinationAddress', width: 150},
			           { name: 'TOCreationDate', index: 'TOICreationDate', width: 150},
			           { name: 'TOShipDate', index: 'TOIDate', width: 150,hidden:true},
			           { name: 'TotalTOIQuantity', index: 'TotalTOIQuantity', width: 150},
			           { name: 'TotalTOIAmount', index: 'TotalTOIAmount', width: 150},
			           { name: 'Status', index: 'Status', width: 150}],
			           pager: jQuery('#PJmap_TOSearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,


			           afterInsertRow : function(ids)
			           {
		        	   
			        	   var index1 = jQuery("#ShortInventoryGrid").jqGrid('getCol','Name',false);

			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

			           },
			           ondblClickRow: function (id) {
			        	   
	                		var selectedItemLookUpCode = jQuery("#TOSearchGrid").jqGrid('getCell', id, 'TONumber');
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#TOSearchGrid").jqGrid('navGrid','#PJmap_TOSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_TOSearchGrid").hide();
		
		
	}
	
	/*
	 * TO LookUp grid create dynamically called from TransferIn.js
	 */
	function CompositeItemSearchGrid(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#compositeItemSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Code','Name','Short Name','Display Name','Parent Heirarchy'],
			colModel: [{ name: 'Code', index: 'Code', width:100 },
			           { name: 'Name', index: 'Name', width:100 },
			           { name: 'ShortName', index: 'ShortName', width:100 },
			           { name: 'DisplayName', index: 'DisplayName', width: 150},
			           { name: 'ParentHeirarchy', index: 'ParentHeirarchy', width: 150}
			          ],
			           pager: jQuery('#PJmap_compositeItemSearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,


			           afterInsertRow : function(ids)
			           {
		        	   
			        	   var index1 = jQuery("#ShortInventoryGrid").jqGrid('getCol','Name',false);

			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

			           },
			           ondblClickRow: function (id) {
			        	   
	                		var selectedItemLookUpCode = jQuery("#compositeItemSearchGrid").jqGrid('getCell', id, 'Code');
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#compositeItemSearchGrid").jqGrid('navGrid','#PJmap_compositeItemSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_compositeItemSearchGrid").hide();
		
		
	}
	
	// Location Search Grid
	
	/*
	 * TO LookUp grid create dynamically called from TransferIn.js
	 */
	function manufactureBatchSearchGrid(fieldIdForItemSearch,typeForBatchSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 *  BatchId: "BAT1/WHDL2/00064"

		 */
		jQuery("#manufactureBatchSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','MRP','MfgBatchNo','Quantity','Weight','MfgDate','ExpDate','UOMId','UOMName','BatchNo'],
			colModel: [
			           { name: 'ItemCode', index: 'ShortName', width:70 },
			           { name: 'ItemName', index: 'Name', width:200 },		           
			           { name: 'MRP', index: 'MRP', width: 50,hidden : true},
			           { name: 'ManufacturingCode', index: 'Code', width:100 },
			           { name: 'Quantity', index: 'Quantity', width: 50},
			           { name: 'Weight', index: 'Weight', width: 50,hidden : true},
			           { name: 'MfgDate', index: 'MfgDate', width: 70},
			           { name: 'ExpDate', index: 'ExpDate', width: 70},
			           { name: 'UOMId', index: 'UOMId',hidden:true},
			           { name: 'UOMName', index: 'UOMName',hidden:true},
			           { name: 'BatchNo', index: 'BatchNo', width: 1,hidden:true}
			          ],
			           pager: jQuery('#PJmap_manufactureBatchSearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
			        	   if(typeForBatchSearch==2){
			        		   var selectedItemLookUpCode = jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'ManufacturingCode');
			        		   jQuery("#SACreateToBatchNo").val(selectedItemLookUpCode); 
			        		   toBatchNo=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'BatchNo');
			        		   jQuery("#SACreateToWeight").val(jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'Quantity'));
			        		   jQuery("#SACreateAllocatedQty").focus();
			        	   }
			        	   else{
	                		var selectedItemLookUpCode = jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'ManufacturingCode');
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                		jQuery("#SACreateAvailableQty").val(jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'Quantity'));
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		fromBatchNo=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'BatchNo');
	                		jQuery("#SACreateToBatchNo").focus();
	                		
			        	   }
			        	   jQuery("#divLookUp" ).dialog("close");
	                 
	                   } 
			          
		});

		jQuery("#manufactureBatchSearchGrid").jqGrid('navGrid','#PJmap_manufactureBatchSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_manufactureBatchSearchGrid").hide();
		
		
	}
	function manufactureBatchLookUpData2(itemId,locationId,bucketId,type)
	{
		jQuery("#btnManufactureBatchLookUpSearch").click(function(){

			searchBatchUsingCode2(itemId,locationId,bucketId,type);
			
		});
		searchBatchUsingCode2(itemId,locationId,bucketId,type);
	}
	function manufactureBatchLookUpDataForNewGrid(itemId,locationId,bucketId,type)
	{
		jQuery("#btnManufactureBatchLookUpSearch").click(function(){

			searchBatchUsingCodeForNewGrid(itemId,locationId,bucketId,type);
			
		});
		
			searchBatchUsingCodeForNewGrid(itemId,locationId,bucketId,type);
			
		
	}
	function searchBatchUsingCode2(itemId,locationId,bucketId,type){
	    	SACreateFromBatchNo=jQuery("#manufactureBatchNo").val() ;
	    	var rowid=0;
		  showMessage("loading", "Search for Batch No...", "");
		   jQuery.ajax({
			   type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchBatchUsingCode",
					bNo:SACreateFromBatchNo,
					locationId:locationId,
					itemId:itemId,
					bucketId:bucketId,
					type:type 
				},
				success:function(data)
				{
					jQuery("#manufactureBatchSearchGrid").jqGrid("clearGridData");
					jQuery(".ajaxLoading").hide();
					jQuery("#SACreateFromBatchNo").css("background","white");
					 resultArrData = jQuery.parseJSON(data);
			if(resultArrData.Status==1){
				bucketBatchLocWiseQty=resultArrData.Result;
				resultArr=resultArrData.Result;
				if(resultArr.length!=0){
							var rowid =0 ;
							jQuery.each(resultArr,function(key,value)
							{
								var newData = [{
									"ManufacturingCode":resultArr[key]['ManufacturingCode'],
									"ItemName":resultArr[key]['ItemName'],
									"ItemCode":resultArr[key]['ItemCode'],
									"MRP":parseFloat(resultArr[key]['MRP'],10).toFixed(2),
									"Quantity":parseFloat(resultArr[key]['Quantity'],10).toFixed(0),
									"Weight":parseFloat(resultArr[key]['Weight'],10).toFixed(2),
									"MfgDate":displayDate(resultArr[key]['MfgDate']),
									"ExpDate":displayDate(resultArr[key]['ExpDate']),
									"UOMId":resultArr[key]['UOMId'],
									"UOMName":resultArr[key]['UOMName'],
									"BatchNo":resultArr[key]['BatchNo'],
											
								}];
								rowid=rowid+1;
								
								for (var i=0;i<newData.length;i++) {
								jQuery("#manufactureBatchSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
								
								}
							 
							});
				   }
				else {
					
			        showMessage("red","NO Record found .","");
				}
			}
			else{
				showMessage('red',resultArrData.Description,'');
			}
				
			  },
			  
			
			});
		} 
	function searchBatchUsingCodeForNewGrid(itemId,locationId,bucketId,type){
    	SACreateFromBatchNo=jQuery("#manufactureBatchNo").val() ;
    	var rowid=0;
	  showMessage("loading", "Search for Batch No...", "");
	   jQuery.ajax({
		   type:'POST',
			url:Drupal.settings.basePath + 'StockAdjustmentCallback',
			data:
			{
				id:"searchBatchUsingCode",
				bNo:SACreateFromBatchNo,
				locationId:locationId,
				itemId:itemId,
				bucketId:bucketId,
				type:type
			},
			success:function(data)
			{
				jQuery("#manufactureBatchSearchGrid").jqGrid("clearGridData");
				jQuery(".ajaxLoading").hide();
				jQuery("#SACreateFromBatchNo").css("background","white");
				 resultArrData = jQuery.parseJSON(data);
		if(resultArrData.Status==1){
			bucketBatchLocWiseQty=resultArrData.Result;
			resultArr=resultArrData.Result;
			if(resultArr.length!=0){
			
						var rowid = 0;
						jQuery.each(resultArr,function(key,value)
						{
							var newData = [{
								"ManufacturingCode":resultArr[key]['ManufacturingCode'],
								"ItemName":resultArr[key]['ItemName'],
								"ItemCode":resultArr[key]['ItemCode'],
								"MRP":parseFloat(resultArr[key]['MRP'],10).toFixed(2),
								"Quantity":parseFloat(resultArr[key]['Quantity'],10).toFixed(0),
								"Weight":parseFloat(resultArr[key]['Weight'],10).toFixed(2),
								"MfgDate":displayDate(resultArr[key]['MfgDate']),
								"ExpDate":displayDate(resultArr[key]['ExpDate']),
								"UOMId":resultArr[key]['UOMId'],
								"UOMName":resultArr[key]['UOMName'],
								"BatchNo":resultArr[key]['BatchNo'],
										
							}];
							
							rowid=rowid+1;
							
							for (var i=0;i<newData.length;i++) {
							jQuery("#manufactureBatchSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							
							}
						 
						});
			   }
			else {
				
		        showMessage("red","NO Record found .","");
			}
		}
		else{
			showMessage('red',resultArrData.Description,'');
		}
			
		  },
		  
		
		});
	} 

	function manufactureBatchSearchGrid2(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 *  BatchId: "BAT1/WHDL2/00064"

		 */
		jQuery("#manufactureBatchSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','MRP','MfgBatchNo','Quantity','Weight','MfgDate','ExpDate','UOMId','UOMName','BatchNo'],
			colModel: [
			           { name: 'ItemCode', index: 'ShortName', width:70 },
			           { name: 'ItemName', index: 'Name', width:200 },		           
			           { name: 'MRP', index: 'MRP', width: 50,hidden : true},
			           { name: 'ManufacturingCode', index: 'Code', width:100 },
			           { name: 'Quantity', index: 'Quantity', width: 50},
			           { name: 'Weight', index: 'Weight', width: 50,hidden : true},
			           { name: 'MfgDate', index: 'MfgDate', width: 70},
			           { name: 'ExpDate', index: 'ExpDate', width: 70},
			           { name: 'UOMId', index: 'UOMId',hidden:true},
			           { name: 'UOMName', index: 'UOMName',hidden:true},
			           { name: 'BatchNo', index: 'BatchNo', width: 1,hidden:true}
			          ],
			           pager: jQuery('#PJmap_manufactureBatchSearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 200,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
	                		var selectedItemLookUpCode = jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'ManufacturingCode');
	                		
	                		jQuery("#"+fieldIdForItemSearch).val(selectedItemLookUpCode);
	                	
	                		jQuery("#"+fieldIdForItemSearch).focus();
	                		batchnoForMultipleSameMfgNo=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'BatchNo');
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#manufactureBatchSearchGrid").jqGrid('navGrid','#PJmap_manufactureBatchSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_manufactureBatchSearchGrid").hide();
		
		
	}
	function manufactureBatchSearchGridForNewGrid(fieldIdForItemSearch,rowId)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 *  BatchId: "BAT1/WHDL2/00064"

		 */
		jQuery("#manufactureBatchSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','MRP','MfgBatchNo','Quantity','Weight','MfgDate','ExpDate','UOMId','UOMName','BatchNo'],
			colModel: [
			           { name: 'ItemCode', index: 'ShortName', width:70 },
			           { name: 'ItemName', index: 'Name', width:200 },		           
			           { name: 'MRP', index: 'MRP', width: 50,hidden : true},
			           { name: 'ManufacturingCode', index: 'Code', width:100 },
			           { name: 'Quantity', index: 'Quantity', width: 50},
			           { name: 'Weight', index: 'Weight', width: 50,hidden : true},
			           { name: 'MfgDate', index: 'MfgDate', width: 70},
			           { name: 'ExpDate', index: 'ExpDate', width: 70},
			           { name: 'UOMId', index: 'UOMId',hidden:true},
			           { name: 'UOMName', index: 'UOMName',hidden:true},
			           { name: 'BatchNo', index: 'BatchNo', width: 1,hidden:true}
			          ],
			           pager: jQuery('#PJmap_manufactureBatchSearchGrid'),
			           width:590,
			           height:200,
			           rowNum: 50,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
			        	   maxRowId=GetLastInsertedId();
	                		var selectedItemLookUpCode = jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'ManufacturingCode');
	                		batchnoForMultipleSameMfgNo=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'BatchNo');
	                		availQuantityInBatch=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'Quantity');
	                		jQuery("#TOCreateAvailableQty").val(availQuantityInBatch);
	                		jQuery("#TOCreateItemBatchNo").val(selectedItemLookUpCode);
							currentBatchNo=batchnoForMultipleSameMfgNo;
							jQuery("#TOCreateTransferQty").focus();
	               if(parseInt(availQuantityInBatch)>=parseInt(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'Adjust'))   )
	               {
	            	   if(id!=maxRowId){
	            		   var r = confirm("FIFO Omit");{
			                	if(vallidateInsertedBatchExistInGrid(batchnoForMultipleSameMfgNo) && r ==true){
			                		jQuery("#TOItemGrid").jqGrid('setCell', rowId, 'ManufactureBatchNo',selectedItemLookUpCode);
			                		jQuery("#TOItemGrid").jqGrid('setCell', rowId, 'BatchNo',batchnoForMultipleSameMfgNo);
			                		jQuery("#TOItemGrid").jqGrid('setCell', rowId, 'MfgBatchNo',selectedItemLookUpCode);
			                		jQuery("#TOItemGrid").jqGrid('setCell',rowId,"MfgBatchNo","", { 'background-color': 'white' });
			                		jQuery("#divLookUp" ).dialog("close");
			                	}
	            		   }
	            	   }
	            	   else {
	            		   if(vallidateInsertedBatchExistInGrid(batchnoForMultipleSameMfgNo)){
		                		jQuery("#TOItemGrid").jqGrid('setCell', rowId, 'ManufactureBatchNo',selectedItemLookUpCode);
		                		jQuery("#TOItemGrid").jqGrid('setCell', rowId, 'BatchNo',batchnoForMultipleSameMfgNo);
		                		jQuery("#TOItemGrid").jqGrid('setCell', rowId, 'MfgBatchNo',selectedItemLookUpCode);
		                		jQuery("#TOItemGrid").jqGrid('setCell',rowId,"MfgBatchNo","", { 'background-color': 'white' });
		                		jQuery("#divLookUp" ).dialog("close");
		                	}
	            	   }
	            	  
			           }
	               
	               else{
	                			showMessage('yellow','This Batch have no suffient quantity for transfer.Delete this row and add manually.','');
	                	}
	                		
	                 
	                   } 
			          
		});

		jQuery("#manufactureBatchSearchGrid").jqGrid('navGrid','#PJmap_manufactureBatchSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_manufactureBatchSearchGrid").hide();
		
		
	}
	
	 function GetLastInsertedId(){
		    var allRows = jQuery('#manufactureBatchSearchGrid').jqGrid('getDataIDs');
		    	
		    	return allRows[0];
	 }
	function vallidateInsertedBatchExistInGrid(batchNo){
		//jQuery("#TOItemGrid").jqGrid('setGridParam',{ page: 1 }).trigger("reloadGrid");
		var gridRowsForColumn2 = jQuery("#TOItemGrid").jqGrid('getCol','BatchNo',false);
		
		for(var i=0;i<gridRowsForColumn2.length;i++)
		{
			
	
			if( gridRowsForColumn2[i]==batchNo){
				
				showMessage('yellow',"Duplicate Batch can not be inserted",'');
				return 0;
			}
		}
		
			
		return 1;
		
	}
	function distributorSearchGrid(fieldIdForDistributorSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*distributor search grid.
		 * provide only grid to show. Without any data in that grid.

		 */
		jQuery("#distributorSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Distributor Id','Distributor Name','City','State','Status'],
			colModel: [{ name: 'DistributorId', index: 'DistributorId', width:100 },
			           { name: 'DistributorFirstName', index: 'DistributorName', width:200 },
			           { name: 'City', index: 'City', width:100 },
			           { name: 'State', index: 'State', width:100 },
			           { name: 'Status', index: 'Status', width:60 },
			          ],
			           pager: jQuery('#PJmap_distributorSearchGrid'),
			           width:560,
			           height:200,
			           rowNum: 100,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
	                		var selectedDistributorId = jQuery("#distributorSearchGrid").jqGrid('getCell', id, 'DistributorId');
	                		
	                		jQuery("#"+fieldIdForDistributorSearch).val(selectedDistributorId);
	                		jQuery("#"+fieldIdForDistributorSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		
	                 
	                   } 
			          
		});

		jQuery("#distributorSearchGrid").jqGrid('navGrid','#PJmap_distributorSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_distributorSearchGrid").hide();
	}
	
	
	function locationSearchGrid(fieldIdForSearch,type)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*distributor search grid.
		 * provide only grid to show. Without any data in that grid.

		 */
		jQuery("#locationSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Location Name','State Name','State Code','Location Code','LocationId'],
			colModel: [{ name: 'LocationName', index: 'LocationName', width:200 },
			           { name: 'StateName', index: 'StateName', width:100 },
			           { name: 'StateCode', index: 'StateCode', width:100 },
			           { name: 'LocationCode', index: 'LocationCode', width:100 },
			           { name: 'LocationId', index: 'LocationId', width:100, hidden:true },
			          ],
			           pager: jQuery('#PJmap_locationsSearchGrid'),
			           width:560,
			           height:200,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
	                		var LocationName = jQuery("#locationSearchGrid").jqGrid('getCell', id, 'LocationName');
	                		var LocationCode = jQuery("#locationSearchGrid").jqGrid('getCell', id, 'LocationCode');
	                		jQuery("#"+fieldIdForSearch).val(LocationName+'-'+LocationCode);
	                		jQuery("#"+fieldIdForSearch).focus();
	                		jQuery("#divLookUp" ).dialog("close");
	                		//fireFunctionOnSelectLocation(type);
	                		if(type==1){
	                			jQuery("#createSourceLocation").val(jQuery("#locationSearchGrid").jqGrid('getCell',jQuery("#locationSearchGrid").getGridParam('selrow'), 'LocationId'))
	    						
	                		}
	                		if(type==2){
	                			jQuery("#createDestinationLocation").val(jQuery("#locationSearchGrid").jqGrid('getCell',jQuery("#locationSearchGrid").getGridParam('selrow'), 'LocationId'))
	    						
	                		}
	                 
	                   } 
			          
		});

		jQuery("#locationSearchGrid").jqGrid('navGrid','#PJmap_locationSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_locationSearchGrid").hide();
	}
	
	
	/// --- 16_05_2014 changes made by harshvardhan.
	
	/*----------LookUp for item detail---------------*/
	function BlockItemSearchGrid(fieldIdForItemSearch)
	{
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 *  Message grid 
		 */
		jQuery("#BlockItemLookupSearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','Status'],
			colModel: [{ name: 'ItemCode', index: 'ItemCode', width:200 },
			           { name: 'ItemName', index: 'ItemName', width: 200},
			           { name: 'Status', index: 'Status', width: 150}],	
			           pager: jQuery('#PJmap_BlockItemLookupSearchGrid'),
			           width:560,
			           height:200,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           ondblClickRow: function (id) {
			        	   var selectedValue = jQuery("#BlockItemLookupSearchGrid").jqGrid('getCell', id, 'ItemCode');

			        	   jQuery("#"+fieldIdForItemSearch).val(selectedValue);
			        	   jQuery("#"+fieldIdForItemSearch).focus();
			        	   jQuery("#divLookUp" ).dialog("close");


			           } 

		});

		jQuery("#BlockItemLookupSearchGrid").jqGrid('navGrid','#BlockItemLookupSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_BlockItemLookupSearchGrid").hide();

		/*-----------------------------------------------------------------------------------------------------------*/
	}	





	function BlockItemLookUpData()
	{

		jQuery("#btnBlockItemLookUpSearch").click(function(){


			BlockItemLookUpGridDataPopulation();


		});

		jQuery("#txtBlockItemCode").keydown(function(e){

			if(e.which == 13)
			{
				BlockItemLookUpGridDataPopulation();
			}

		});

		jQuery("#btnBlockItemLookUpReset").click(function(){

			jQuery("#txtBlockItemCode").val('');
			jQuery("#txtBlockItemName").val('');
			jQuery("#BlockItemLookupSearchGrid").jqGrid('clearGridData');

		});

	}
	
	
	
	function BlockItemLookUpGridDataPopulation()
	{
		var ItemCode= jQuery("#txtBlockItemCode").val();
		var ItemName = jQuery("#txtBlockItemName").val();
		showMessage("loading", "Searching available Item...", "");

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"BlockItemLookUpData",
				ItemCode:ItemCode,
				ItemName:ItemName
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var tempRowId = 0;

				var lookUpData = jQuery.parseJSON(data);

				if(lookUpData.Status == 1)
				{
					jQuery("#BlockItemLookupSearchGrid").jqGrid('clearGridData');
					//alert(data);
					var olookUpData = lookUpData.Result;

					if(olookUpData.length > 50)
					{
						showMessage("", "More result available refine your search", "");
					}

					jQuery.each(olookUpData,function(key,value){
						if(key > 49) //index is starting from 0.//hence 49.
						{
							return false;
						}
						tempRowId = tempRowId + 1;
						var lookUpGridData = [{
							"ItemName":olookUpData[key]['ItemName'], 
							"ItemCode":olookUpData[key]['ItemCode'],
							"Status":olookUpData[key]['Status'] }];
						for (var i=0;i<lookUpGridData.length;i++) {
							jQuery("#BlockItemLookupSearchGrid").jqGrid('addRowData', tempRowId, lookUpGridData[lookUpGridData.length-i-1], "last");
						}

					});
				}
				else
				{
					showMessage("red", lookUpData.Description, "");
				}




			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}
	
	
	
	
