
var validationStatus = 1;

var currentLocationId ;

/*---------------------------------------------------------------------------------------------------------------------*/
/**
 * Function for uploading files of any format.
 */
function getCurrentLocation(){
	var locationId ;
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:
		{
			id:"getCurrentLocation"
		},
		success:function(data)
		{
			//alert(data);	
			var currentLocationIdData = jQuery.parseJSON(data);

			if(currentLocationIdData.Status == 1)
			{

				currentLocationKey  = currentLocationIdData.Result;
				jQuery.each(currentLocationKey,function(key,value)
						{	
					
					currentLocationId=currentLocationKey[key]['locationId'];
						});
				
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}

	});

}


/**
 * 
 * @param documentBody - When error generated then it will generate an screenshot of document and upload on server.
 */
function generateImageOfApplicationError(documentBody,msg)
{
	html2canvas(documentBody, {
	    onrendered: function(canvas) {
	        // canvas is the actual canvas element,
	          // to append it to the page call for example
	    	
	    	//document.getElementById("canvasImage").src = canvas.toDataURL();
	    	
	    	
	    	var dataURL = canvas.toDataURL();
	    	
	          // document.body.appendChild( canvas );
	           
	           var blobBin = atob(dataURL.split(',')[1]);
	           var array = [];
	           for(var i = 0; i < blobBin.length; i++) {
	               array.push(blobBin.charCodeAt(i));
	           }
	           var file=new Blob([new Uint8Array(array)], {type: 'image/png'});


	           var formdata = new FormData();
	           formdata.append("myNewFileName", file);
	           //formdata.append("id", "Parveen");
	           jQuery.ajax({
	             url: "/sites/all/modules/VestigePOS/VestigePOS/Business/UploadFile.php",
	             //url: "/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/Business/UploadFile.php", //For application with document root with c:/Wamp/
	        	  //url:Drupal.settings.basePath + 'CommonActionCallback',
	              type: "POST",
	              data: formdata,
	              processData: false,
	              contentType: false,
	           }).done(function(respond){
	        	   
	        	   //alert(respond);
	        	   showMessage("green", "Reported Successfully", "");
	        	    
	        	   sendEmailOnReportErrorStatus(respond,msg);
	        	   
	             //alert(respond);
	           });   
	           
	        
	    }
	});
}


function sendEmailOnReportErrorStatus(respond,msg)
{
	   jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:
			{
				id:"sendEmailOnErrorReporting",
				response:respond,
				msg:msg
			},
			success:function(data)
			{
				//alert(data);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
}

/**
 * Author - Parveen Verma
 * function used to upload file.
 * @param fileInputId - select file option id to disable it later.
 * @param fileIndex - coming 0(Zero).
 * @param current_checkbox_id - checkbox id correspoding to file browse id.
 * @param distributorId - distributorid.
 */
function upload(fileInputId, fileIndex, current_checkbox_id,distributorId,userid)
{
	// take the file from the input
	var file = document.getElementById(fileInputId).files[fileIndex]; 
	var userId=userid;
	var reader = new FileReader();
	reader.readAsBinaryString(file); // alternatively you can use readAsDataURL
	reader.onloadend  = function(evt)
	{
		// create XHR instance
		xhr = new XMLHttpRequest();
		
		//var basePath = "http://localhost"+Drupal.settings.basePath;
		
		//var basePath = "/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/";
		
		var modifiedFileName = distributorId+fileInputId+file.name;
		var distributorid=distributorId;
		var checkboxid=current_checkbox_id;
		var userid=userId;
		

		// send the file through POST
		xhr.open("POST", '/sites/all/modules/VestigePOS/VestigePOS/Business/FileUpload.php', true);
		
		xhr.setRequestHeader("X_FILE_NAME", modifiedFileName);
		xhr.setRequestHeader("X_DISTRIBUTORID", distributorid);
		xhr.setRequestHeader("X_CHECKBOXID", checkboxid);
		xhr.setRequestHeader("X_USERID", userid);
		
		//xhr.setRequestHeader("X_BASE_PATH", basePath);

		// make sure we have the sendAsBinary method on all browsers
		XMLHttpRequest.prototype.mySendAsBinary = function(text){
			var data = new ArrayBuffer(text.length);
			var ui8a = new Uint8Array(data, 0);
			for (var i = 0; i < text.length; i++) ui8a[i] = (text.charCodeAt(i) & 0xff);

			if(typeof window.Blob == "function")
			{
				var blob = new Blob([data]);
			}else{
				var bb = new (window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder)();
				bb.append(data);
				var blob = bb.getBlob();
			}

			this.send(blob);
		}

		// let's track upload progress
		var eventSource = xhr.upload || xhr;
		eventSource.addEventListener("progress", function(e) {
			// get percentage of how much of the current file has been sent
			var position = e.position || e.loaded;
			var total = e.totalSize || e.total;
			var percentage = Math.round((position/total)*100);

			// here you should write your own code how you wish to proces this
		});

		// state change observer - we need to know when and if the file was successfully uploaded
		xhr.onreadystatechange = function()
		{
			if(xhr.readyState == 4)
			{
				if(xhr.status == 200)
				{

					//process status
					//var result = xhr.responseText.evalJSON();

					// alert the message
					//jQuery("#"+fileInputId).hide(); - if not work
					try
					{
						jQuery("#"+fileInputId).attr("disabled",true);

						//alert(xhr.responseText);
						var filename1 = "/imp/mine.tmp"
						var tmpfile_uploaded_info = jQuery.parseJSON(xhr.responseText);
						var tmpfile_location = tmpfile_uploaded_info.uploadedfile;
						jQuery("#"+current_checkbox_id).val(tmpfile_location);
						current_hidden_field = current_checkbox_id + "1";
						//jQuery(".form_hidden_field").append("<input type='text' name="+tmpfile_location+" value="+tmpfile_location+">");
						//jQuery("#"+current_hidden_field).val(tmpfile_location);
						//jQuery("#"+current_checkbox_id).attr("disabled", true);
						jQuery("#"+current_hidden_field).val(tmpfile_location);
						//alert("check box latest value:"+jQuery("#"+current_hidden_field).val());
						showMessage("green", "Document Uploaded", "");
					}
					catch(err)
					{
						showMessage("red", "Error " + xhr.responseText, "");
					}
					
					//alert("Status : success");
				}else{
					// process error
					//alert("Status : failure");
					showMessage("red", "Problem in uploading file", "");
				}
			}
		};

		// start sending
		xhr.mySendAsBinary(evt.target.result);
	};
}
	
/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to round off floating values upto two decimal points
 * 
 */
function formatFloatNumbers(floatNumber)
{
	var floatNumber = parseFloat(floatNumber);
	
	return floatNumber.toFixed(2);
}

/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to validate mobile number
 * 
 */
function validateMobileNumber(mobileNumber)
{
	var regex = /^[0-9]{10}$/;

	if (regex.test(mobileNumber)) {
		// Valid international phone number
		return 1;
	} else {
		// Invalid international phone number
		return 0;
	}
}

function validateMobileNumberForDIS(mobileNumber){
	var regex = /^[7-9]{1}[0-9]{9}$/;
	var validater = 0;
	if (regex.test(mobileNumber)) {
		// Valid international phone number
		validater = 1;
	} else {
		// Invalid international phone number
		validater = 0;
	}
	
	if(validater == 1 && mobileNumber != "7777777777" && mobileNumber != "8888888888" && mobileNumber != "9999999999"){
		validater = 1;
	}
	else{
		validater = 0;
	}
	
	return validater;
}

function backGroundColorForDisableField(){
	var kids = jQuery("Body").find('input, textarea, select ');
	for (var i=0;i<=kids.length;i++){
		
		if(jQuery(kids[i]).is(":disabled")){
			jQuery('#'+kids[i].id).css("background","#dddddd");
		}
	
	
		
	}
}
/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to validate Email Address
 * 
 */
function validateEmailAddress(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var emailValidationFlag = regex.test(email);
	if(emailValidationFlag )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to validate Email Address
 * 
 */
function validatePinCode(pin) {
	
	var regex = /^[0-9]{6}$/;

	if (regex.test(pin)) {
		// Valid international phone number
		return 1;
	} else {
		// Invalid international phone number
		return 0;
	}
}


function showErrorReportingMessage(messageCode,defaultMessage,messageParam){

	
	
	if(messageCode == "green")
	 {
	   var msg = defaultMessage;
	      jQuery( "<div style='z-index:1005;'>" )
	        .appendTo( document.body )
	        .text( msg )
	        .addClass( "notificationGreen " )
	        .position({
	          my: "center top",
	          at: "center top",
	          of: window
	        })
	        .show({
	          effect: "blind",
	        })
	        .delay( 5000 )
	        .hide({
	          effect: "blind",
	          duration: "slow"
	        }, function() {
	          jQuery( this ).remove();
	        });
	 }
}	

function showMessage(messageCode,defaultMessage,messageParam){

	jQuery(".ajaxLoading").hide();
	
	if(messageCode == "green")
	 {
	   var msg = defaultMessage;
	      jQuery( "<div style='z-index:10005;'>" )
	        .appendTo( document.body )
	        .text( msg )
	        .addClass( "notificationGreen " )
	        .position({
	          my: "center top",
	          at: "center top",
	          of: window
	        })
	        .show({
	          effect: "blind",
	        })
	        .delay( 5000 )
	        .hide({
	          effect: "blind",
	          duration: "slow"
	        }, function() {
	          jQuery( this ).remove();
	        });
	 }
	 
	 else if(messageCode == "red")
	 {
		 
		var numberOfTimesGenerateErrorImageTOBeCalled = 1; 
		 
	   var msg = defaultMessage;
	   
	   
	  // alert(msg);
	   
	      jQuery( "<div style='z-index:10005;'>" )
	        .appendTo( document.body )
	        .html("<div id = 'errorStatus' style='text-align:center';>"+ msg +"<br><button type='button' id='reportError' class='errorReportButtonId' value='CanvasImageId'>Report to Vestige Help Desk</button></br></div>")
	        .addClass( "notificationRed " )
	        .position({
	          my: "center top",
	          at: "center top",
	          of: window
	        })
	        .show({
	          effect: "blind",
	          
	        },
	        function(){
	        	
	        	
	        		jQuery(".errorReportButtonId").click(function(){
	   	   	    	 
	  	  	    	  generateImageOfApplicationError(document.body,msg);
	  	  	    	  
	  	  	      });
	        	
	        	
	        	
	        	
	        })
	        .delay( 5000 )
	        .hide({
	          effect: "blind",
	          duration: "slow"
	        }, function() {
	          jQuery( this ).remove();
	          
	          //alert(this);
	          
	        });
	       
	        
	      
	      
	      
	      /*jQuery(document.body).on('click', '#reportError' ,function(){
	    	  	
	    	  
	    	
	    	  if(numberOfTimesGenerateErrorImageTOBeCalled == 0)
	    	  {
	    		  generateImageOfApplicationError(document.body,msg);
	    	  }
	    	
	    	  numberOfTimesGenerateErrorImageTOBeCalled  += 1;
	    	  
	    	   
	      });*/
	      
	      /*jQuery(".errorReportButtonId :last").click(function(){
	    	 
	    	  generateImageOfApplicationError(document.body);
	    	  
	      });*/
	      jQuery(".errorReportButtonId :last").click(function(){
    	    	 
  	    	  generateImageOfApplicationError(document.body,msg);
  	    	  
  	      });
	      
	      
	      
	 }
	
	 else if(messageCode == "loading")
	 {
		 var msg = defaultMessage;
	      jQuery( "<div class='ajaxLoading' style='border:5px solid;z-index:20000'>" )
	        .appendTo( document.body )
	        .html("<img style='float:left' id='ajaxCallLoadingImage' src='/sites/all/modules/VestigePOS/VestigePOS/images/485.gif' width='50' height='50'><div style='float:left;margin: 10px;'>"+msg+"</div>")
	        .addClass( "notificationLoading " )
	        .position({
	          my: "center top",
	          at: "center top",
	          of: window
	        })
	        .show({
	          effect: "blind",
	        });
	        
	 }
	
	 else
	 {
	   var msg = defaultMessage;
	      jQuery( "<div style='z-index:10005'>" )
	        .appendTo( document.body )
	        .html( msg )
	        .addClass( "notification " )
	        .position({
	          my: "center top",
	          at: "center top",
	          of: window
	        })
	        .show({
	          effect: "blind",
	        })
	        .delay( 5000 )
	        .hide({
	          effect: "blind",
	          duration: "slow"
	        }, function() {
	          jQuery( this ).remove();
	        });
	 }
	
	
}


  // Module for DateTime set
function displayDate(GMTdateToFormatForDisplay){

			var parsedDate = jQuery.datepicker.parseDate('yy-mm-dd', GMTdateToFormatForDisplay);
			return jQuery.datepicker.formatDate('d-M-yy', parsedDate);
}

function formatDateForDisplay(GMTdateToFormatForDisplay){

	var parsedDate = jQuery.datepicker.parseDate('dd-mm-yy', GMTdateToFormatForDisplay);
	return jQuery.datepicker.formatDate('d-M-yy', parsedDate);
}

 function getDate(displayDateFormatInGMT)
 {
	 	var parsedDate = jQuery.datepicker.parseDate('d-M-yy', displayDateFormatInGMT);
		return jQuery.datepicker.formatDate('yy-mm-dd', parsedDate);
 }
 
 function dateForCompare(displayDateFormatInGMT)
 {
	 	var parsedDate = jQuery.datepicker.parseDate('d-M-yy', displayDateFormatInGMT);
		return jQuery.datepicker.formatDate('dd-mm-yy', parsedDate);
 }
 function IsDateGreater(DateValue1, DateValue2)
 {

 var DaysDiff;
 Date1 = new Date(DateValue1);
 Date2 = new Date(DateValue2);
 DaysDiff = Math.floor((Date1.getTime() - Date2.getTime())/(1000*60*60*24));
 if(DaysDiff > 0)
 return true;
 else
 return false;
 }
 
 function showCalender() {
		jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0', onSelect:function(){
            
        	jQuery(this).focus();
      
           }});
		jQuery( ".showCalender" ).datepicker("setDate",new Date());
		
		//jQuery('.showCalender').attr('readonly', true);
	}
 
 function showCalender2() {
		jQuery( ".showCalender2" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0',onSelect:function(){
            
			jQuery(this).focus();
      
           }});
		//jQuery('.showCalender').attr('readonly', true);
	}
 

function showReport(reportBaseURL,reportUrl,reportParam,calledUsingPhpJavaBridge,progressCount){

	
	if(calledUsingPhpJavaBridge == 1)
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"getInvoiceReport",
				reportUrl:reportBaseURL,
				reportParam:reportParam
					
			},
			success:function(data)
			{

				var invoiceReportWin = window.open("","Customer Invoice","height=600,width=800");
				
				invoiceReportWin.document.write(data);
				
				
				
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}
	else
	{
		
		
		var form = document.createElement("form");
		
		form.setAttribute("method", "POST");

		form.setAttribute("action", reportBaseURL+reportUrl);


		form.setAttribute("target", "formresult");
		//var reportName = document.createElement("input");   
		//reportName.setAttribute("name","__report");
		//reportName.setAttribute("value",reportUrl);
		//reportName.setAttribute("type",'hidden');
		//form.appendChild(reportName);

		//var params = new Array();

		for(keys in reportParam){
			var key = keys;
			var value = reportParam[keys];
			var hiddenField = document.createElement('input');
			hiddenField.setAttribute("name",key);
			hiddenField.setAttribute("value",value);
			hiddenField.setAttribute("type",'hidden');
			form.appendChild(hiddenField);
		}
		
		var hiddenField = document.createElement('input');
		hiddenField.setAttribute("name","GeneratedBy");
		hiddenField.setAttribute("value",loggedInUserFullName);
		hiddenField.setAttribute("type",'hidden');
		form.appendChild(hiddenField);
		
		document.body.appendChild(form);
		
		/*htmlToAppendToWindowOpen = "<html><head><script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>" +
				"<script src='http://localhost/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/JS/progressBarScript.js'></script></head><body>" +
				"<div id='dialog'><div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div></div></body></html>";*/

		// creating the 'formresult' window with custom features prior to submitting the form
		var result = window.open('', 'formresult', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');

		if(result){
			
			/*var divElement = result.document.createElement("div");
			
			divElement.setAttribute("id", "dialog");
			
			result.document.body.appendChild(divElement);
			
			jQuery("#dialog").append("<div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div>");*/
			
			//result.document.write("<div id='dialog'><div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div></div>");
			
			//result.document.write(htmlToAppendToWindowOpen);
			
			//showProgressBarForLogReport(progressCount);
			
			form.submit();
		}
		else{
			alert("Error in submitting to url");
		}
	}

	

}


function showProgressBar()
{

	var progressbar = $( "#progressbar" ),
	progressLabel = $( ".progress-label" );

	progressbar.progressbar({
		value: false,
		change: function() {
			progressLabel.text( progressbar.progressbar( "value" ) + "%" );
		},
		complete: function() {
			progressLabel.text( "Complete!" );
		}
	});

	function progress() {
		var val = progressbar.progressbar( "value" ) || 0;

		progressbar.progressbar( "value", val + 1 );



		if ( val < 29 ) {
			setTimeout( progress, 7000 );
		}
	}

	setTimeout( progress, 1000 );

}

function fetchvaluesFromJsonOnSelectRow(jsonString,inputKey,inputKeyValue,OutPutKey)
{
	var ret;
	ret='';
	itemDesc = jsonString;
	
if(itemDesc.length!=0)
{


      jQuery.each(itemDesc,function(key,value)
	   {	
		 var itemValue = itemDesc[key][inputKey]	;

        if(itemValue==inputKeyValue){
         	
         	ret= itemDesc[key][OutPutKey];
    	return ret;
          }
		 
      });
	
  }
return ret;
	
}

function matchItemsInJQGrid(gridName,columnName,columnValue){
	var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
   var found=0;
	for(var i=0;i<gridRowsForColumn.length;i++)
	{

		if(columnValue == gridRowsForColumn[i])
		{
			found = 1;
			break;
		}
		else 
			{
			
			found=0;
			}
//		pv_sum+= parseFloat(PV[i]);
	}
	return found;
}
function matchItemsInJQGridUsingTOColumn(gridName,columnName,columnValue,columnName2,columnValue2){
	var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
	var gridRowsForColumn2 = jQuery("#"+gridName).jqGrid('getCol',columnName2,false);
   var found=0;
	for(var i=0;i<gridRowsForColumn.length;i++)
	{

		if(columnValue == gridRowsForColumn[i] && gridRowsForColumn2[i]==columnValue2)
		{
			found = 1;
			break;
		}
		else 
			{
			
			found=0;
			}
//		pv_sum+= parseFloat(PV[i]);
	}
	return found;
}
// it is for sum of row of one column of jqgrid ....
function sumOfColumnValueJQGrid(gridName,columnName){
	var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
	
   var columnValue=0;
	for(var i=0;i<gridRowsForColumn.length;i++)
	{

		columnValue=0+columnValue+parseInt(gridRowsForColumn[i])+0;
//		pv_sum+= parseFloat(PV[i]);
	}
	return columnValue;
}

function sumOfColumnValueJQGrid2(gridName,columnName){
	var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
	
   var columnValue=0;
	for(var i=0;i<gridRowsForColumn.length;i++)
	{

		columnValue=0+columnValue+parseFloat(gridRowsForColumn[i])+0;
//		pv_sum+= parseFloat(PV[i]);
	}
	return columnValue;
}


//function used to validate age //take two parameter one DOB//another year by which to validate.

function validateAge(selectedDOB,ageToValidate)
{
	var validateAgeFlag = 0;
	
	if(selectedDOB.indexOf('/') == -1)
	{
		validateAgeFlag = 0;
	}
	else
	{
		var ddmmyyyy = selectedDOB.split('/');

		if(ddmmyyyy[0] && ddmmyyyy[1] && ddmmyyyy[2])
		{
			var mmddyyyy = ddmmyyyy[1] + '/' + ddmmyyyy[0] + '/' + ddmmyyyy[2];
			
			validateAgeFlag = returnAge(mmddyyyy,ageToValidate);
		}
		else
		{
			validateAgeFlag = 0;
		}
	}

	return validateAgeFlag;  
}

function returnAge(dateForWhichToReturnAge,ageToValidate){ 	 	
	 	
		try { 	 	
			var validateAgeFlag = 0; 	 	
			selectedDOB = new Date(dateForWhichToReturnAge); //convert string to date type. 	 	
			if ( isNaN(selectedDOB.getTime() ) ){ 	 	
				validateAgeFlag = 0; 	 	
				throw "invalidDate"; 	 	
			} 	 	
			var today = new Date();       	 	
			var age = today.getFullYear() - selectedDOB.getFullYear(); 	 	
			var m = today.getMonth() - selectedDOB.getMonth(); 	 	
			if (m < 0 || (m === 0 && today.getDate() < selectedDOB.getDate())) { 	 	
				age--; 	 	
			}	   	 	
	 	 	
	 	 	
			if (age >= ageToValidate) { 	 	
				validateAgeFlag = 1; 	 	
			} else { 	 	
				validateAgeFlag = 0; //0 otherwise //which is already set. 	 	
			} 	 	
		} catch (e) { 	 	
			// TODO: handle exception 	 	
			validateAgeFlag = 0; 	 	
		} 	 	
		 	 	
		return validateAgeFlag; 	 	
	} 

//function to be implemented remained to watch that hwo to return from ajax call in jquery.

/*function fetchTitles()
{
	showMessage("loading", "Loading titles...", "");
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'POSDistributorRegistration',
		data:
		{
			id:"titles",
			
		},
		success:function(data)
		{
			
			return data;
		} //end success
		
	});
}
*/


function checkIntegerOrNot(valueToCheck)
{
	var validatedIntegerOrNot = 0;
	
	var intRegex = /^\d+$/;
   
	if(intRegex.test(valueToCheck))
	{
		validatedIntegerOrNot = 1;
	}
	else
	{
		validatedIntegerOrNot = 0;
	}
	
	return validatedIntegerOrNot;
}


function panNumberValidation(valueToCheck)
{
	var validatedPanNumberOrNot = 0;
	
	valueToCheck = valueToCheck.toUpperCase();

	var intRegex = /[A-Z]{3}[PHFATC]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}/;

	if(intRegex.test(valueToCheck))
	{
		validatedPanNumberOrNot = 1;
	}
	else
	{
		validatedPanNumberOrNot = 0;
	}

	return validatedPanNumberOrNot;
}

function sendDBR(){
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:
		{
			id:"sendDBRReport"
		},
		success:function(data)
		{
			//alert(data);	
			//var oResponse = jQuery.parseJSON(data);

			/*if(oResponse.Status == 1)
			{
				oResult = oResponse.Result;
			}*/
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}

	});
	
}

function convertArrayToJson(data) {
	var json = {};
	jQuery.each(data, function(){
		json[this.name] = this.value || '';
	});

	return json;
}


Array.prototype.contains = function(v) {
	for(var i = 0; i < this.length; i++) {
		if(this[i] === v) return true;
	}
	return false;
};
