var  jsonForLocations='';
var jsonForGRNSearch='';
var todayDate='';
var loggedInUserPrivileges;
jQuery(document).ready(function(){

	
	
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	});
	
	
	getCurrentLocation();
	var moduleCode = jQuery("#moduleCode").val();
	 todayDate=jQuery("#todayDate").val();
	 
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
			data:
			{
				id:"StockCountModuleFuncHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

				if(loggedInUserPrivilegesForStockCountData.Status == 1)
				{

					loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

					if(loggedInUserPrivileges.length == 0)
					{
						jQuery(".GRNCreateActionButtons").attr('disabled',true);
					}
					else
					{
						disableFieldOnStartup();
						 enableFieldOnStartup();
						GRNSearchStatus();
						searchWHLocation();
						vendoreCode();
						showCalender();
						showCalender2();
						showCalender3();
						disableButtons(0);
					}


				}
				else
				{
					showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});

		function showCalender3() {
			jQuery( ".showCalender3" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0',maxDate:0,onSelect:function(){
	         
				jQuery(this).focus();
				if (diffrenInDays(jQuery(this).val(),jQuery("#GRNCreatePODate").val())<0){
					showMessage('yellow','Invoice or Challan Date  cannot be less than PO Date','');
	
				}
	   
	        }});
			//jQuery('.showCalender3').attr('readonly', true);
		}
		function diffrenInDays(DateValue1, DateValue2)
		 {

		 var DaysDiff;
		 Date1 = new Date(DateValue1);
		 Date2 = new Date(DateValue2);
		 DaysDiff = Math.floor((Date1.getTime() - Date2.getTime())/(1000*60*60*24));
            return DaysDiff ;
		 }
		 
		/**
		 * Purchase order look up screen.
		 */
		jQuery("#GRNCreatePONumber").keydown(function(e){
			if(e.which == 115) 
			{
				var currentSearchId = this.id;
				//alert(currentItemSearchId);
				showMessage("loading", "Please wait...", "");

				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"POLookUp",
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();

						//alert(data);
						jQuery("#divLookUp").html(data);

						POSearchGrid(currentSearchId);
						jQuery("#divLookUp" ).dialog( "open");
						
						POLookUpData();

						/*----------Dialog for item search is there ----------------*/


					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}

				});



			}

		});
		
		function actionButtonsStauts()
		{
			jQuery(".GRNCreateActionButtons").each(function(){

				var buttonid = this.id;
				var extractedButtonId = buttonid.replace("btn","");

				if(loggedInUserPrivileges[extractedButtonId] == 1)
				{

					//jQuery("#"+buttonid).attr('disabled',false);

				}
				else
				{
					jQuery("#"+buttonid).attr('disabled',true);
				}

				jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.

			});
		}

	function searchWHLocation()
	{
		showMessage("loading", "Loading information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"searchWHLocation",
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			
			var whlocationsData = jQuery.parseJSON(data);
			if(whlocationsData.Status==1){
				jsonForLocations=whlocationsData.Result;
				whlocations=whlocationsData.Result;
				
				jQuery.each(whlocations,function(key,value)
				{	
					jQuery("#GRNDestinationLocaiton").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");

				});
				jQuery('#GRNDestinationLocaiton').val(currentLocationId);
				jQuery('#GRNDestinationLocaiton').attr('disabled',true);
				backGroundColorForDisableField();
			}
			else{
				showMessage('red',whlocationsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	function GRNSearchStatus()
	{
		showMessage("loading", "Loading information", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'GRNCallback',
			data:
			{
				id:"GRNSearchStatus",
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var GRNStatusData = jQuery.parseJSON(data);
			if(GRNStatusData.Status==1){
				GRNStatus=GRNStatusData.Result;
				jQuery.each(GRNStatus,function(key,value)
				{	
					
					jQuery("#GRNSearchStatus").append("<option id=\""+GRNStatus[key]['keycode1']+"\""+" value=\""+GRNStatus[key]['keycode1']+"\""+">"+GRNStatus[key]['keyvalue1']+"</option>");
				});
			}
			else{
				showMessage('red',GRNStatusData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	function vendoreCode()
	{
		showMessage("loading", "Loading information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'GRNCallback',
			data:
			{
				id:"vendoreCode",
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var vendoreCodeData = jQuery.parseJSON(data);
				if(vendoreCodeData.Status==1){
					vendoreCode=vendoreCodeData.Result;
				
				jQuery.each(vendoreCode,function(key,value)
				{	
					jQuery("#GRNVendorCode").append("<option id=\""+vendoreCode[key]['VendorId']+"\""+" value=\""+vendoreCode[key]['VendorId']+"\""+">"+vendoreCode[key]['VendorName']+"</option>");
				});
			}
				else{
					showMessage('red',vendoreCodeData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', yearRange: '1950:2010',minDate: +1, maxDate: "100D"});
	/*jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
	jQuery( ".showCalender" ).datepicker("setDate",new Date());
*/	
	
	jQuery(function() {
			//alert("dfadf");
			jQuery( "#divGoodReceiptNoteTab" ).tabs(); // Create tabs using jquery ui.
		});
    jQuery("#divGoodReceiptNoteTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
		
		var tabIndex = tab.index; 
		if(tabIndex == 0)
		{
			 disableFieldOnStartup();
			 enableFieldOnStartup();
			 jQuery("#GRNBatchDetailsGrid").jqGrid('clearGridData');
				jQuery("#GRNViewGrid").jqGrid("clearGridData");
				jQuery("#GRNCreatePONumber").val('');
				disableButtons(0);
		}
		else if(tabIndex == 1){
			
		}
		});
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN Search grid.
		 */
		jQuery("#GRNSearchGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['View','GRN No','GRN Date','PO No','PO Date','Challen No','Invoice No.','Vendor Code','Status','Location Code',
			'AmendmentNo','ChallanDate','GrossWeight','NoOfBoxes','InvoiceDate','InvoiceTaxAmount','InvoiceAmount','ShippingDetails','VehicleNo','ReceivedBy',
			'StatusId','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','VendorName','D_Address1','D_Address2','D_Address3','D_Address4','D_City',
			'D_State','D_Country','VendorAddress'],
			colModel: [{ name: 'View', index: 'View', width:40 },
			           { name: 'GRNNo', index: 'GRNNo', width: 120},
			           { name: 'GRNDate', index: 'GRNDate', width:70 },
			           { name: 'PONo', index: 'PONo', width: 100},
			           { name: 'PODate', index: 'PODate', width: 100},
			           { name: 'ChallenNo', index: 'ChallenNo', width: 100, hidden:true},
			           { name: 'InvoiceNo', index: 'InvoiceNo', width: 70, hidden:true},
			           { name: 'VendorCode', index: 'VendorCode', width: 120},
			           { name: 'Status', index: 'Status', width: 70},
			           { name: 'LocationCode', index: 'LocationCode', width: 100},
			       
			           { name: 'AmendmentNo', index: 'AmendmentNo', width: 70, hidden:true},
			           { name: 'ChallanDate', index: 'ChallanDate', width: 70, hidden:true},
			           { name: 'GrossWeight', index: 'GrossWeight', width: 70, hidden:true},
			           { name: 'NoOfBoxes', index: 'NoOfBoxes', width: 70, hidden:true},
			           { name: 'InvoiceDate', index: 'InvoiceDate', width: 70, hidden:true},
			           { name: 'InvoiceTaxAmount', index: 'InvoiceTaxAmount', width: 70, hidden:true},
			           { name: 'InvoiceAmount', index: 'InvoiceAmount', width: 70, hidden:true},
			           { name: 'ShippingDetails', index: 'ShippingDetails', width: 70, hidden:true},
			           { name: 'VehicleNo', index: 'VehicleNo', width: 70, hidden:true},
			           { name: 'ReceivedBy', index: 'ReceivedBy', width: 70, hidden:true},
			           { name: 'StatusId', index: 'StatusId', width: 70, hidden:true},

			           { name: 'CreatedBy', index: 'CreatedBy', width: 70, hidden:true},
			           { name: 'CreatedDate', index: 'CreatedDate', width: 70, hidden:true},
			           { name: 'ModifiedBy', index: 'ModifiedBy', width: 70, hidden:true},
			           { name: 'ModifiedDate', index: 'ModifiedDate', width: 70, hidden:true},
			           { name: 'VendorName', index: 'VendorName', width: 70, hidden:true},
			           { name: 'D_Address1', index: 'D_Address1', width: 70, hidden:true},
			           { name: 'D_Address2', index: 'D_Address2', width: 70, hidden:true},
			           { name: 'D_Address3', index: 'D_Address3', width: 70, hidden:true},
			           { name: 'D_Address4', index: 'D_Address4', width: 70, hidden:true},
			           { name: 'D_City', index: 'D_City', width: 70, hidden:true},
			           { name: 'D_State', index: 'D_State', width: 70, hidden:true},
			           { name: 'D_Country', index: 'D_Country', width: 70, hidden:true},
			           { name: 'VendorAddress', index: 'VendorAddress', width:200 , hidden:true}
			           
			        
			          ],
			           
			           
			           pager: jQuery('#PJmap_GRNSearchGrid'),
			           width:1040,
			           height:470,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
                       shrinkToFit:true,

			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	  
			           },

			           loadComplete: function() {
//			        	   jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//			        	   alert("laodign complete");
			           },
			           onSelectRow: function(rowId) {

			        	   jQuery("#GRNCreatePONumber").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'PONo'));
			        	   jQuery("#GRNCreateAmendmentNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'AmendmentNo'));
			        	   jQuery("#GRNCreatePODate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'PODate')));
			        	   jQuery("#GRNNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GRNNo'));
			        	   jQuery("#GRNDate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GRNDate')));
			        	   jQuery("#GRNStatus").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'Status'));
			        	   jQuery("#GRNCreateChallanNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ChallenNo'));
			        	   jQuery("#GRNCreateChallanDate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ChallanDate')));
			        	   jQuery("#GRNCreateVendorCode").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VendorCode'));
			        	   jQuery("#GRNCreateInvoiceNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceNo'));
			        	   jQuery("#GRNCreateInvoiceDate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceDate')));
			        	   jQuery("#GRNCreateVendorName").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VendorName'));
			        	   jQuery("#GRNViewInvoiceTax").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceTaxAmount'));
			        	   jQuery("#GRNViewShippingDetails").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ShippingDetails'));
			        	   
			        	   jQuery("#GRNViewDestinationLocation").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VendorAddress'));
			        	   jQuery("#GRNViewInvoiceAmtInclTax").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceAmount'));
			        	   jQuery("#GRNViewGrossWeight").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GrossWeight'));
			        	   jQuery("#GRNViewVehicleNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VehicleNo'));
			        	   jQuery("#GRNViewReceivedBy").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ReceivedBy'));
			        	   jQuery("#GRNViewNoOfBoxes").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'NoOfBoxes'));
			        	   if(jQuery("#GRNCreateChallanNo").val()==''){
			        		   jQuery("#GRNCreateChallanDate").val('');
			        	   }
			        	   if(   jQuery("#GRNCreateInvoiceNo").val()==''){
			        		   jQuery("#GRNCreateInvoiceDate").val('');
			        	   }
			        	
			       		jQuery("#divGoodReceiptNoteTab").tabs( "select", "DivCreate" );
			       		searchGRNItemDetails(jQuery("#GRNCreatePONumber").val(),jQuery("#GRNNo").val());
			       		searchBatchDetail( jQuery("#GRNNo").val());
			       	  disableButtons( jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'StatusId'));
			       				           }
			         
		});

		jQuery("#GRNSearchGrid").jqGrid('navGrid','#PJmap_GRNSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNSearchGrid").hide();
		
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN View grid.
		 */
		 myDelOptions2 = {
					onclickSubmit: function (rp_ge, rowid) {
	               showMessage('','Cannot delete row','');
	          
	            return true;
	        },
	        processing: true
	        };
		jQuery("#GRNViewGrid").jqGrid({
	
			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','PO Qty','Already Received Qty','Already Invoiced Qty','Balance Qty','Received Qty','Invoice Qty','ItemId',
			           'SerialNo','AmendmentNo','PONumber','GRNNo','BatchNumber','PurchaseUOM','MaxQty','Add Batches'],
			colModel: [
                       
			           { name: 'ItemCode', index: 'ItemCode', width: 70},
			           { name: 'ItemName', index: 'ItemName', width:150 },
			           { name: 'POQty', index: 'POQty', width: 70},
			           { name: 'AlreadyReceivedQty', index: 'AlreadyReceivedQty', width: 150},
			           { name: 'AlreadyInvoicedQty', index: 'AlreadyInvoicedQty', width: 150},
			           { name: 'BalanceQty', index: 'BalanceQty', width: 100},
			           { name: 'ReceivedQty', index: 'ReceivedQty', width: 100},
			           { name: 'InvoiceQty', index: 'InvoiceQty', width: 100
							 },
			           { name: 'ItemId', index: 'ItemId', width: 1,hidden:true},
			           { name: 'SerialNo', index: 'SerialNo', width: 1,hidden:true},
			           { name: 'AmendmentNo', index: 'AmendmentNo', width: 1,hidden:true},
			           { name: 'PONumber', index: 'PONumber', width: 1,hidden:true},
			           { name: 'GRNNo', index: 'GRNNo', width: 1,hidden:true},
			           { name: 'BatchNumber', index: 'BatchNumber', width: 1,hidden:true},
			           { name: 'PurchaseUOM', index: 'PurchaseUOM', width: 1,hidden:true},
			           { name: 'MaxQty', index: 'MaxQty', width: 1,hidden:true},
			           { name: 'AddBatches', index: 'AddBatches', width: 100}
			           ],
			           pager: jQuery('#PJmap_GRNViewGrid'),
			           width:1040,
			           height:150,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           
			           editurl:'clientArray',
			           shrinkToFit: false,
			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	  
			           },

			           loadComplete: function() {
//			        	   jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//			        	   alert("laodign complete");
			           }	
			           ,
			           
			           onSelectRow: function(rowId){
			        	 var itemCode=  jQuery('#GRNViewGrid').jqGrid('getCell',rowId, 'ItemCode');
			        	 hideRowsONSelectItemCode(itemCode);
			        	// showAllRowsONSelectItemCode();
			           },
			           ondblClickRow: function(rowid)
			           {
			        	 //  showAllRowsONSelectItemCode();
			        	 //  saveRowOnNewRow();
			        	   
			        	
			           }
		});

		function rowIseditableOrNot(rowId){
			var ind=jQuery("#GRNBatchDetailsGrid").getInd(rowId,true);if(ind != false){
			    edited = jQuery(ind).attr("editable");
			}

			if (edited === "1"){
			 return  true ;
			} else{
			   return false ;
			}
		}
		function saveRowOnNewRow(){
			if(jQuery("#GRNStatus").val()!='Closed' ){
			  batchRow= jQuery("#GRNBatchDetailsGrid").getGridParam('selrow');
	        	if(jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')==null){
	        		addRow();
	        		
	        	}
	        	else if(rowIseditableOrNot(batchRow) || jQuery("tr#"+batchRow).attr("editable")==1){
	        		
	        	 if(  jQuery("#GRNBatchDetailsGrid").jqGrid('saveRow',batchRow)==true){
	        	   checkSave(batchRow,'');
	        	   if(vallidateQtyOfPOItemsONSave()==1){
	        	   addRow();
	        	   }
	        	   else{
	        	   jQuery("#GRNBatchDetailsGrid").jqGrid('editRow',batchRow);
	        	   }
	        	 }
	        	}
	        	else{
	        		
	        		addRow();
	        	}
	    	}
			jQuery('.ui-icon-pencil').hide();
		}

    	function checkSave(rowid,type){	
		
			var savedRowItemCode = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ItemCode');
			var temReciveQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
			sumOfQtytobeEdit=getBatchReciveItemSum(savedRowItemCode);
			if(type=='delete'){
	    	sumOfQtytobeEdit=sumOfQtytobeEdit-temReciveQty;
			}
			
	    	var viewGridRowId=selectRowIdFromItemCode(savedRowItemCode);
	    	if(viewGridRowId!=-1 && vallidateOnAddBatch(rowid,viewGridRowId)!=0){
	            
		            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
		            
		            	updateQuantityInGrid(sumOfQtytobeEdit,viewGridRowId);
		            	
	    	}
	    	else if(viewGridRowId==-1){
	    		jQuery("#GRNBatchDetailsGrid").jqGrid('editRow',rowid);
	    		showMessage("yellow",'Invalid Item Code','');
	    	}
		}
		jQuery("#GRNViewGrid").jqGrid('navGrid','#PJmap_GRNViewGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNViewGrid").hide();
		
		function addRow(){
			 jQuery("#GRNBatchDetailsGrid").addRow(1, {
       	      rowID : "new_row",
       	      initdata : {},
       	      position :"first",
       	      useDefValues : false,
       	      useFormatter : false,
       	      addRowParams : {extraparam:{}}
       	  });
			 vallidationForReciveQty();
		}
		
	
		jQuery("#GRNCreatePONumber").keydown(function(e){
			
		    if(e.which==9 || e.which == 13){
		    	e.preventDefault(); 
		    	var PONumber = jQuery("#GRNCreatePONumber").val();
		    	searchItemUsingCode(PONumber);        
		    }

		});
		

		function searchItemUsingCode(PONumber){
			showMessage("loading", "Searching available purchase order number...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'GRNCallback',
				data:
				{
					id:"searchPONumber",
					PONumber:PONumber
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					jsonForGRNSearch=data;
					itemDescData = jQuery.parseJSON(data);
				if(itemDescData.Status==1)
				{
					
					itemDesc=itemDescData.Result;
					if(itemDesc.length!=0)
				
					{
					 jQuery.each(itemDesc,function(key,value)
					 {	

						jQuery("#GRNCreateAmendmentNo").val(itemDesc[key]['AmendmentNo']);
						jQuery("#GRNStatus").val('New');
						
						jQuery("#GRNCreateChallanNo").val(itemDesc[key]['ChallanNo']);
						jQuery("#GRNCreateChallanNo").focus();
						
						jQuery("#GRNCreateVendorCode").val(itemDesc[key]['VendorCode']);
						jQuery("#GRNCreateInvoiceNo").val('');
						
						
						jQuery("#GRNCreatePODate").val(displayDate(itemDesc[key]['PODate']));
						jQuery("#GRNCreateVendorName").val(itemDesc[key]['VendorName']);
						jQuery("#GRNViewInvoiceTax").val(formatFloatNumbers(parseFloat(itemDesc[key]['TotalTaxAmount'])));
						
						jQuery("#GRNViewShippingDetails").val(itemDesc[key]['ShippingDetails']);
						jQuery("#GRNViewDestinationLocation").val(itemDesc[key]['V_Address1']+' '+itemDesc[key]['V_Address2']+ '  '+itemDesc[key]['V_Address3']+ ' '+itemDesc[key]['V_Address4']+' '+itemDesc[key]['V_CityName']+' '+itemDesc[key]['V_StateName']);
						jQuery("#GRNViewInvoiceAmtInclTax").val(0);
						jQuery("#GRNViewGrossWeight").val('');
						
						jQuery("#GRNViewVehicleNo").val('');
						jQuery("#GRNViewReceivedBy").val('');
						jQuery("#GRNViewNoOfBoxes").val('');
						jQuery("#GRNCreatePONumber").css("background","white");
					
						searchGRNItemDetails(PONumber,'');
						jQuery("#GRNCreatePONumber").attr('disabled',true);
						
					  });
					
				
				    }
					else {
						jQuery("#GRNCreatePONumber").css("background","#FF9999");
				        showMessage("yellow","Enter Valid PO Number .","");
				        jQuery("#GRNCreatePONumber").focus();
						return;
					}
				}
				else{
					showMessage('red',itemDescData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
			
		}
		
		jQuery("#btnSearchGRNReset").unbind("click").click(function(){
			jQuery("#GRNumber").val('');
			jQuery("#GRNPONumber").val('');
			jQuery("#GRNReceivedBy").val('');
			jQuery("#GRNVendorCode").val(-1);
		   jQuery("#GRNSearchStatus").val(-1);
		   jQuery("#GRNSearchGrid").jqGrid("clearGridData");
			showCalender();
		});	
		jQuery("#btnRCV01Search").unbind("click").click(function(){
			searchGRN();
		});		
		function searchGRN(){
			jQuery("#actionName").val('Search');
			showMessage("loading", "Searching for GRNs...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'GRNCallback',
				data:
				{
					id:"searchGRN",
					GRNNo:jQuery("#GRNumber").val(),
					PONumber:jQuery("#GRNPONumber").val(),
					ReceivedBy:jQuery("#GRNReceivedBy").val(),
					VendorID :jQuery("#GRNVendorCode").val(),
					Status:jQuery("#GRNSearchStatus").val(),
					LocationID :jQuery("#GRNDestinationLocaiton").val(),
					FromGrnDate:getDate(jQuery("#GRNFromGRNDate").val()),
					ToGrnDate :getDate(jQuery("#GRNTOGRNDate").val()),
					FromPODate:getDate(jQuery("#GRNFromPODate").val()),
					ToPODate:getDate(jQuery("#GRNToPODate").val()),
					functionName:jQuery("#actionName").val(),
					moduleCode:"RCV01"
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
	if(itemDescData.Status==1){
		itemDesc=itemDescData.Result;
	
		if(itemDesc.length!=0)
	
				{
				jQuery("#GRNSearchGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
						var newData = [{"View":'<Button type="button" id="">Open</button>',
							"GRNNo":itemDesc[key]['GRNNo'],
							"GRNDate":displayDate(itemDesc[key]['GRNDate']),
							"PONo":itemDesc[key]['PONumber'],
							"PODate":displayDate(itemDesc[key]['PODate']),
							"ChallenNo":itemDesc[key]['ChallanNo'],
							"InvoiceNo":itemDesc[key]['InvoiceNo'],
							"Status":itemDesc[key]['StatusName'],
					      	"LocationCode":itemDesc[key]['LocationCode'] ,
						"VendorCode":itemDesc[key]['VendorCode'],
						"AmendmentNo":itemDesc[key]['AmendmentNo'],
				      	"ChallanDate":displayDate(itemDesc[key]['ChallanDate']),
				      	"GrossWeight":itemDesc[key]['GrossWeight'],
						"NoOfBoxes":itemDesc[key]['NoOfBoxes'],
				      	"InvoiceDate":displayDate(itemDesc[key]['InvoiceDate']),
				      	"InvoiceTaxAmount":formatFloatNumbers(parseFloat(itemDesc[key]['InvoiceTaxAmount'])),
				      	"InvoiceAmount":formatFloatNumbers(parseFloat(itemDesc[key]['InvoiceAmount'])),
				      	"ShippingDetails":itemDesc[key]['ShippingDetails'],
				      	"VehicleNo":itemDesc[key]['VehicleNo'],
						"ReceivedBy":itemDesc[key]['ReceivedBy'],
				      	"StatusId":itemDesc[key]['Status'],
				      	"CreatedBy":itemDesc[key]['CreatedBy'],
				      	"CreatedDate":displayDate(itemDesc[key]['CreatedDate']),
					 	"ModifiedBy":itemDesc[key]['ModifiedBy'],
				      	"ModifiedDate":displayDate(itemDesc[key]['ModifiedDate']),
						"VendorName":itemDesc[key]['VendorName'],
				   
				      	"D_Address1":itemDesc[key]['D_Address1'],
				      	"D_Address2":itemDesc[key]['D_Address2'],
				      	"D_Address3":itemDesc[key]['D_Address3'],
				      	"D_Address4":itemDesc[key]['D_Address4'],
				      	"D_City":itemDesc[key]['D_City'],
				      	"D_State":itemDesc[key]['D_State'],
				      	"VendorAddress":itemDesc[key]['VendorAddress'],
				      	"D_Country":itemDesc[key]['D_Country']
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#GRNSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						
					 });
					
				    }
			else 	{
						
					        showMessage("yellow","No Record Found","");
							return;
					}
				}
	else{
		showMessage('red',itemDescData.Description,'');
	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
		}
		
		
		function searchGRNItemDetails(PONumber,$GRNNo){
			showMessage("loading", "Searching for item details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'GRNCallback',
				data:
				{
					id:"searchGRNItemDetails",
					PONumber:PONumber,
					GRNNo:$GRNNo
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();	
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
		if(itemDescData.Status==1){
			
			itemDesc=itemDescData.Result;
			if(itemDesc.length!=0)
					{
				
				jQuery("#GRNViewGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
					var invoiceQty='';
					if(itemDesc[key]['InvoiceQty']!=''){
							invoiceQty=parseInt(parseFloat(itemDesc[key]['InvoiceQty'],10).toFixed(2));
					}
					else{
						invoiceQty="''";
					}
						var newData = [{
							"ItemCode":itemDesc[key]['ItemCode'],
							"ItemName":itemDesc[key]['ItemDescription'],
							"POQty":parseInt(itemDesc[key]['POQty']),
							"AlreadyReceivedQty":parseInt(parseFloat( itemDesc[key]['AlreadyReceivedQty'],10).toFixed(2)),
							"AlreadyInvoicedQty":parseInt(parseFloat( itemDesc[key]['AlreadyInvoicedQty'],10).toFixed(2)),
							"BalanceQty":parseInt(parseFloat(itemDesc[key]['BalanceQty'],10).toFixed(2)),
							"ReceivedQty":parseInt(parseFloat(itemDesc[key]['ReceivedQty'],10).toFixed(2)),
							"InvoiceQty":"<input type='text'  name='claimQty' style='width:100px;' value="
								+invoiceQty+" class='CLaimQuantity' id="
								+itemDesc[key]['ItemCode']+">",
						"ItemId":itemDesc[key]['ItemId'],
				      	"SerialNo":itemDesc[key]['SerialNo'],
				      	"AmendmentNo":itemDesc[key]['AmendmentNo'],
						"PONumber":itemDesc[key]['PONumber'],
				      	"GRNNo":itemDesc[key]['GRNNo'],
				      	"BatchNumber":itemDesc[key]['BatchNumber'],
				      	"PurchaseUOM":itemDesc[key]['PurchaseUOM'],
				      	"MaxQty":parseFloat(itemDesc[key]['MaxQty'],10).toFixed(2),
				      	"AddBatches":'<Button type="button" class="AddBatches" id='+rowid+'>Add Batch</button>',
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#GRNViewGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						channgeinClaimQtyChange();
						AddBatches();
					 });
					
				    }
			else 	{
							jQuery("#txtItemCode").css("background","#FF9999");
					        showMessage("yellow","Enter Valid Item Code.","");
					        jQuery("#txtItemCode").focus();
							return;
					}
				}
		else{
			showMessage('yellow',itemDescData.Description,'');
		}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
			
			
		}
		
		function AddBatches(){
			jQuery(".AddBatches").unbind("click").click(function(e){
				e.preventDefault();
					var rowId=this.id;
					jQuery('#GRNViewGrid').jqGrid('setSelection',rowId);
					 saveRowOnNewRow();
					
				});
			
		}
		
		function channgeinClaimQtyChange(){
			
			jQuery('.CLaimQuantity').keyup(function(){
				
				if(vallidateForInvoiceQtyNotshorterThenReciveQty()==1){
					jQuery('.AddBatches').attr('disabled',false);
	
				}
				else{
					jQuery('.AddBatches').attr('disabled',true);
				}
			});
		}
		
		function vallidationForReciveQty(){
			jQuery('.editable').keyup(function(){
			  value=jQuery(this).val();	
			
			  if(isNaN(value)==false && this.id==jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_'+'ReceivedQty'){
				 if( vallidateQtyOfPOItemsONSave(value,  jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', jQuery("#GRNBatchDetailsGrid").getGridParam('selrow'), 'ItemCode'))==0){
					 jQuery('.AddBatches').attr('disabled',true); 
				 }
				 else{
					 jQuery('.AddBatches').attr('disabled',false);
				 }
			  }
			
			});
		}
		function searchBatchDetail(GRNNo){
			showMessage("loading", "Searching for item batch details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'GRNCallback',
				data:
				{
					id:"searchBatchDetail",
					GRNNo:GRNNo
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();	
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
		if(itemDescData.Status==1){
			
			itemDesc=itemDescData.Result;
			if(itemDesc.length!=0)
					{
			
				jQuery("#GRNBatchDetailsGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
							
						var newData = [{
							"ItemCode":itemDesc[key]['ItemCode'],
							"ReceivedQty":parseInt(itemDesc[key]['ReceivedQty']),
							"MRP":parseFloat(itemDesc[key]['MRP'],10).toFixed(2),
							"ManufacturerBatch":itemDesc[key]['ManufacturerBatchNo'],
							"ManufacturingDate":displayDate(itemDesc[key]['ManufacturingDate']),
							"ExpiryDate":displayDate(itemDesc[key]['ExpiryDate'])
					      	
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#GRNBatchDetailsGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						
					 });
				jQuery('.ui-icon-pencil').hide();
				    }
			else 	{
							jQuery("#txtItemCode").css("background","#FF9999");
					        showMessage("yellow","Enter Valid Item Code.","");
					        jQuery("#txtItemCode").focus();
							return;
					}
				}
							else{
								showMessage('red',itemDescData.Description,'');
							}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
				});	
			
	   }
		function disableFieldOnStartup(){
			jQuery("#GRNCreateAmendmentNo").attr('disabled',true);
			jQuery("#GRNCreateAmendmentNo").val('');
			jQuery("#GRNCreatePODate").attr('disabled',true);
			jQuery("#GRNCreatePODate").val('');
			jQuery("#GRNNo").attr('disabled',true);
			jQuery("#GRNNo").val('');
       		jQuery("#GRNDate").attr('disabled',true);
       		jQuery("#GRNDate").val('');
       		jQuery("#GRNStatus").attr('disabled',true);
       		jQuery("#GRNStatus").val('');

       		jQuery("#GRNCreateChallanNo").attr('disabled',true);
       		jQuery("#GRNCreateChallanNo").val('');
       		
       		jQuery("#GRNCreateChallanDate").attr('disabled',true);
       		
       		jQuery("#GRNCreateVendorCode").attr('disabled',true);
       		jQuery("#GRNCreateVendorCode").val('');

       		jQuery("#GRNCreateInvoiceNo").attr('disabled',true);	
       		jQuery("#GRNCreateInvoiceNo").val('');
       		jQuery("#GRNCreateInvoiceDate").attr('disabled',true);	
       
			jQuery("#GRNCreateVendorName").attr('disabled',true);
			jQuery("#GRNCreateVendorName").val('');
			jQuery("#GRNViewInvoiceTax").attr('disabled',true);
			jQuery("#GRNViewInvoiceTax").val('');
			jQuery("#GRNViewShippingDetails").attr('disabled',true);
			jQuery("#GRNViewShippingDetails").val('');
			jQuery("#GRNViewDestinationLocation").attr('disabled',true);
			jQuery("#GRNViewDestinationLocation").val('');
			jQuery("#GRNViewInvoiceAmtInclTax").attr('disabled',true);
			jQuery("#GRNViewInvoiceAmtInclTax").val('');
            jQuery("#GRNViewGrossWeight").attr('disabled',true);
            jQuery("#GRNViewGrossWeight").val('');
            jQuery("#GRNViewVehicleNo").attr('disabled',true);	
            jQuery("#GRNViewVehicleNo").val('');
            
            jQuery("#GRNViewReceivedBy").attr('disabled',true);
            jQuery("#GRNViewReceivedBy").val('');
			jQuery("#GRNViewNoOfBoxes").attr('disabled',true);
			jQuery("#GRNViewNoOfBoxes").val('');
			jQuery("#GRNViewTransporterName").val('');
			jQuery("#GRNTransportNo").val('');
			jQuery("#GRNGatePassNo").val('');
			
			jQuery("#CRCreateItemCode2").hide();
			jQuery("#GRNCreateChallanDate").val('');	
			jQuery("#GRNCreateInvoiceDate").val('');
		}

		function enableFieldOnStartup(){
			jQuery("#GRNCreateChallanNo").attr('disabled',false);
			jQuery("#GRNCreateChallanDate").attr('disabled',false);

			jQuery("#GRNCreateInvoiceNo").attr('disabled',false);	
			jQuery("#GRNCreateInvoiceDate").attr('disabled',false);	

			jQuery("#GRNCreatePONumber").attr('disabled',false);
		
			jQuery("#GRNViewInvoiceAmtInclTax").attr('disabled',false);

            jQuery("#GRNViewVehicleNo").attr('disabled',false);	
            
            jQuery("#GRNViewReceivedBy").attr('disabled',false);
			jQuery("#GRNViewNoOfBoxes").attr('disabled',false);
		}

		
		var field1, field2,
	    myCustomCheck = function (value, colname) {
	        if (colname == "Remove") {
	            if(value == 12)
	            {
	            	alert("12 entered");
	            	return [true];
	            }
	            else
	            {
	            	return [false, "some error text"];
	            }
	        } 

	        /*if (field1 !== undefined && field2 !== undefined) {
	            // validate the fields here
	            return [false, "some error text"];
	        } else {
	            return [true];
	        }*/
	        
	      
	    };
	    
	    function vallidateQtyOfPOItemsONSave(){
	    	var  value ;
	    	var allRowIds=jQuery('#GRNViewGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	       
	    		   value=	parseInt(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'MaxQty'))-(
	  			    	parseInt(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'ReceivedQty'))+
	  			    	parseInt(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'AlreadyReceivedQty')))
	  			   if(value<0)
	  				   {
	  				     showMessage('yellow','Received Qty for Item Code- '+jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index],'ItemCode')+' exceeded limit by '+ (-value),'');
	  				     return 0;
	  				     
	  				   }
	    		  
			    	
	    	}
	    	return 1 ;
	    }
	    function vallidateQtyOfPOItemsONSave(values,ItemCode){
	    	var  value ;
	    	var allRowIds=jQuery('#GRNViewGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	       if(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'ItemCode')==ItemCode){
		    		   value=	parseInt(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'MaxQty'))-(
		  			    	parseInt(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'ReceivedQty'))+
		  			    	parseInt(jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index], 'AlreadyReceivedQty'))+parseFloat(values));
		  			   if(value<0)
		  				   {
		  				     showMessage('yellow','Received Qty for Item Code '+jQuery('#GRNViewGrid').jqGrid('getCell', allRowIds[index],'ItemCode')+' exceeded limit by '+ (-value),'');
		  				     return 0;
		  				     
		  				   }
	    	       }
	    		  
			    	
	    	}
	    	return 1 ;
	    }
	    
	    function vallidateForInvoiceQtyNotshorterThenReciveQty(){
	    	var  value ;
	    	var arrayFromGrid=getJsonFromCreateGrid();
	    
	    	for(var index=0;index<arrayFromGrid.length;index++){
	    		if(arrayFromGrid[index].InvoiceQty==''){
	    			arrayFromGrid[index].InvoiceQty=0;
	    		}
	    		if(isNaN(arrayFromGrid[index].InvoiceQty)==true ){
	    			 showMessage('yellow','Invoice Quantity should be numeric','');
	    			 jQuery(this).focus();
	    			return 0;
	    		}
	    		  
	  			   if(parseFloat(arrayFromGrid[index].InvoiceQty)+parseFloat(arrayFromGrid[index].AlreadyInvoicedQty)<parseFloat(arrayFromGrid[index].ReceivedQty)+parseFloat(arrayFromGrid[index].AlreadyReceivedQty))
	  				   {
	  				      showMessage('yellow',"Received Quantity can't be more than Invoice Quantity. Item Code - "+arrayFromGrid[index].ItemCode,'');
	  				    jQuery(this).focus();
		    			return 0; 
	  				   }
	  			 if(parseFloat(arrayFromGrid[index].InvoiceQty)+parseFloat(arrayFromGrid[index].AlreadyInvoicedQty)>parseFloat(arrayFromGrid[index].MaxQty))
				   {
				      showMessage('yellow',"Invoice Quantity Out of Acceptable limit of PO. Item Code - "+arrayFromGrid[index].ItemCode,'');
				    jQuery(this).focus();
	    			return 0; 
				   }
	    	}
	    	return 1 ;
	    }
	    
	    
	    function showAllRowsONSelectItemCode(){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	       		jQuery("#"+allRowIds[index],"#GRNBatchDetailsGrid").show();
	    	}
	    }
	    
	    
	    
	    function hideRowsONSelectItemCode(itemCode){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	var batchItemCode=	jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'ItemCode');
			    	if(batchItemCode==itemCode){
			    		jQuery("#"+allRowIds[index],"#GRNBatchDetailsGrid").show();
			    		
			    	}
			    	else{
			    		jQuery("#"+allRowIds[index],"#GRNBatchDetailsGrid").hide();
			    	}
			    	
	    	}
	    	
	    }
	    function getBatchReciveItemSum(itemCode){
	    	var sumOfItems=0 ;
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	var batchItemCode=	jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'ItemCode');
			    	if(batchItemCode==itemCode){
			    		
			    		sumOfItems=sumOfItems+parseInt(parseFloat(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'ReceivedQty'),10).toFixed(0));
			    		
			    	}  	
			    	
	    	}
	    	return sumOfItems ;
	    }
	    
	    function getJsonFromCreateGrid (){
			var itemInformation = jQuery("#GRNViewGrid").jqGrid('getRowData');

			var sItemInformation = JSON.stringify(itemInformation);

			jQuery(".CLaimQuantity").each(function(){

				var claimCellid = this.id;
				var claimQty = jQuery("#"+claimCellid).val();



				jQuery.each(itemInformation,function(key,value){

					if(claimCellid == itemInformation[key]['ItemCode'])
					{
						itemInformation[key]['InvoiceQty'] = claimQty;
						itemInformation[key]['AddBatches'] = 0;
					}
					else
					{

					}

				});


			});
			return itemInformation;
		}

	    function updateQuantityInGrid(savedRowItemReceivedQty,rowId){
	    	
        
        	var AlreadyReceivedQtyReceivedQty = jQuery('#GRNViewGrid').jqGrid('getCell',
        			rowId, 'AlreadyReceivedQty');
        	var POQty = jQuery('#GRNViewGrid').jqGrid('getCell',
        			rowId, 'POQty');
        	var intBQty = parseInt(POQty)-parseInt(AlreadyReceivedQtyReceivedQty);
        	jQuery("#GRNViewGrid").jqGrid('setCell',rowId,'BalanceQty',
        			parseInt(intBQty)-parseInt(savedRowItemReceivedQty));
        	jQuery("#GRNViewGrid").jqGrid('setCell',rowId,
        			'ReceivedQty',parseInt(savedRowItemReceivedQty));
        /*	jQuery("#GRNViewGrid").jqGrid('setCell',rowId,
        			'InvoiceQty',parseInt(savedRowItemReceivedQty));*/
	    } 
	    
	    function selectRowIdFromItemCode(itemCode){
	    var allRows = jQuery('#GRNViewGrid').jqGrid('getDataIDs');
	    	for(var i=0; i<allRows.length ; i++){
	    		if(jQuery('#GRNViewGrid').jqGrid('getRowData', allRows[i]).ItemCode==itemCode){
	    			return allRows[i] ;
	    		}
	     		
	    	}
	    	return -1;
	    }
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN Batch Details Grid.
		 */
	    function currencyFmatter (cellvalue, options, rowObject)
	    {
	       // do something here
	    	if(jQuery("#GRNViewGrid").getGridParam('selrow')!=undefined){
	    		
	    	return	   jQuery('#GRNViewGrid').jqGrid('getCell',jQuery("#GRNViewGrid").getGridParam('selrow'), 'ItemCode');
	    	}
	    	if(cellvalue!=undefined){
	    		return cellvalue ;
	    	}
	    		
	    		return '';
	    }
	  /*  function currencyFmatter2 (cellvalue, options, rowObject)
	    {
	    	jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_ExpiryDate').focus();
	    	return '';
	    }*/
	    
	    
	    
	    /**
	     * edit param function for jqgrid to edit row data.
	     */
	    function vallidateOnAddBatch(rowid,viewGridRowId){
	    	if(IsDateGreater(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ManufacturingDate'),jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ExpiryDate') ))
	    	{
	    		jQuery('#GRNBatchDetailsGrid').editRow(rowid, true); 
        		showMessage("yellow","Mfg Date can't be greater then Exp Date ",'');
        		return 0;
	    	}
	    	
	    	return 1;
	    }
	  
	    myDelOptions = {
				onclickSubmit: function (rp_ge, rowid) {
            rp_ge.processing = true;
         //   if(isNaN(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',rowid, 'ReceivedQty'))==false){
	        	if(jQuery("tr#"+rowid).attr("editable")==0){
	        	checkSave(rowid,'delete');
	        	}
            	jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
            
                jQuery("#delmod" + jQuery("#GRNBatchDetailsGrid")[0].id).hide();
 
                if (jQuery("#GRNBatchDetailsGrid")[0].p.lastpage > 1) {
                	jQuery("#GRNBatchDetailsGrid").trigger("reloadGrid", [{ page: jQuery("#TOItemGrid")[0].p.page}]);
                }
          
            return true;
        },
        processing: true
        };
		
	    
	 /*  var myEditOptions = {
	            keys: true,
	            oneditfunc: function (rowid) {
	               // alert("row with rowid=" + rowid + " is editing.");
	            },
	            aftersavefunc: function (rowid, response, options) {
	            	var savedRowItemCode = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ItemCode');
	            	sumOfQtytobeEdit=getBatchReciveItemSum(savedRowItemCode);
	            	var viewGridRowId=selectRowIdFromItemCode(savedRowItemCode);
	            	if(viewGridRowId!=-1 && vallidateOnAddBatch(rowid,viewGridRowId)!=0){
                        
				            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
				            
				            	updateQuantityInGrid(sumOfQtytobeEdit,viewGridRowId);
				            	
	            	}
	            	else if(viewGridRowId==-1){
	            		//jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
	            		showMessage("red",'Invalid Item Code','');
	            	}
	            }
	      };*/
	    
	/*  var  editparameters = {
	    		"keys" : false,
	    		"oneditfunc" : null,
	    		"successfunc" : null,
	    		"url" : 'clientArray',
	    	        "extraparam" : {},
	    		"aftersavefunc" : null,
	    		"errorfunc": null,
	    		"afterrestorefunc" : null,
	    		"restoreAfterError" : true,
	    		"mtype" : "POST"
	    	};
	    */
		jQuery("#GRNBatchDetailsGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['Edit','Item Code','Received Qty','Manufacturer Batch','Manufacturing Date','Expiry Date'],
			colModel: [
{ name: 'myac', width:100, fixed:true, sortable:false, resize:false, formatter:'actions',
	formatoptions:{
		keys:true,
		onEdit:function(rowid) {

			saveditableRows();


		},
		onSuccess:function(jqXHR) {

			alert("in onSuccess used only for remote editing:"+
					"\nresponseText="+jqXHR.responseText+
					"\n\nWe can verify the server response and return false in case of"+
			" error response. return true confirm that the response is successful");
			return true;
		},
		onError:function(rowid, jqXHR, textStatus) {
			alert("in onError used only for remote editing:"+
					"\nresponseText="+jqXHR.responseText+
					"\nstatus="+jqXHR.status+
					"\nstatusText"+jqXHR.statusText+
			"\n\nWe don't need return anything");
		},
		afterSave:function(rowid) {
			checkSave(rowid,'');
		},
		onEdit:function(rowid){
			saveditableRows();

		},
		afterRestore:function(rowid) {
			//alert("in afterRestore (Cancel): rowid="+rowid+"\nWe don't need return anything");

		},
		delOptions:myDelOptions
	}
},

{ name: 'ItemCode', index: 'ItemCode', width: 150,editable:false,sorttype:'string',formatter:currencyFmatter }

, { name: 'ReceivedQty', index: 'ReceivedQty', width: 150,editable:true,
	"editrules":{"required":true, "integer":true,"minValue":0} },

	{ name: 'ManufacturerBatch', index: 'ManufacturerBatch', width:200,editable:true 
		,"editrules":{"required":true} },
		{ name: 'ManufacturingDate', index: 'ManufacturingDate', 
			width: 200,editable:true,"editrules":{"required":true},
			editoptions:{size:25,dataInit:function(el){ 
				jQuery(el).datepicker({
					
					beforeShowDay: function (date) {

						if (date.getDate() <= 31) {
							return [false, ''];
						}
						return [true, ''];
					},
					
					changeMonth: true, changeYear: true,dateFormat:'M-yy', yearRange: '1950:2060'/*,minDate: '-2000D', maxDate: "0D"  */, 
					showButtonPanel: true,/*onSelect:function(){

	                        	jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_ManufacturingDate').focus();

	                           },*/
					
					onClose: function(dateText, inst) { 
						var month = jQuery("#ui-datepicker-div .ui-datepicker-month :selected").val();
						var year = jQuery("#ui-datepicker-div .ui-datepicker-year :selected").val();
						jQuery(this).datepicker('setDate', new Date(year, month, 1));

						jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_ManufacturingDate').focus();
					}}); 

			}
			},formatoptions: {disabled : false} },
			{ name: 'ExpiryDate', index: 'ExpiryDate', width: 200,editable:true,
				"editrules":{"required":true},
				editoptions:{size:25,dataInit:function(el){ 
					jQuery(el).datepicker({

						beforeShowDay: function (date) {

							if (date.getDate() <= 31) {
								return [false, ''];
							}
							return [true, ''];
						},

						changeMonth: true, changeYear: true,dateFormat:'M-yy', yearRange: '1950:2060'/*,minDate: +1*/,
						showButtonPanel: true,/*onSelect:function(){

	                        	//jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_MRP').focus();

	                           },*/
						onClose: function(dateText, inst) { 
							var month = jQuery("#ui-datepicker-div .ui-datepicker-month :selected").val();
							var year = jQuery("#ui-datepicker-div .ui-datepicker-year :selected").val();
							jQuery(this).datepicker('setDate', new Date(year, month, 28));

							jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_MRP').focus();
						}
					});   
				} 
				},formatoptions: {disabled : false} }

			],
			          
			           pager: jQuery('#PJmap_GRNBatchDetailsGrid'),

			           width:1040,
			           height:150,
			           rowNum: 500,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           editurl:'clientArray',
			           shrinkToFit:false,
			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
			        	  
			           },
			           ondblClickRow: function(rowId) {	
			        	  if( saveditableRows()==1){
			        	   jQuery("#GRNBatchDetailsGrid").jqGrid('editRow',rowId);
			        	  }
			           	}

			          
		});
		jQuery("#GRNBatchDetailsGrid").jqGrid('navGrid','#PJmap_GRNBatchDetailsGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNBatchDetailsGrid").hide();
		jQuery('.ui-icon-pencil').hide();
		
		function saveditableRows(){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    		if(jQuery("tr#"+allRowIds[index]).attr("editable")==1 || rowIseditableOrNot(allRowIds[index]))
	       		{
	    			 if(  jQuery("#GRNBatchDetailsGrid").jqGrid('saveRow',allRowIds[index])==true){
			        	   checkSave(allRowIds[index],'');
			        	 }
	    			 else{
	    				 return 0;
	    			 }
	       		}
	    	}
	    	 return 1;
	    }
		jQuery("#GRNCreateChallanNo").change(function(){
			
			jQuery("#GRNCreateChallanNo").css("background","white");
		});
		jQuery("#GRNCreateInvoiceNo").change(function(){
			
			jQuery("#GRNCreateInvoiceNo").css("background","white");
		});
		
		
		function validationOnSaveButton (){
			
			showAllRowsONSelectItemCode();
				if(	jQuery("#GRNCreatePONumber").val()==''){
				jQuery("#GRNCreatePONumber").css("background","#FF9999");
				showMessage('yellow','Enter Valid PO Number .','');
				jQuery("#GRNCreatePONumber").focus();
				return 0;
				}
				else{
					jQuery("#GRNCreatePONumber").css("background","white");
				}
				if(	jQuery("#GRNCreatePODate").val()==''){
					showMessage('yellow','Enter Valid PO Number .','');
					jQuery("#GRNCreatePONumber").focus();
					return 0;
					}
		
				if(	jQuery("#GRNCreateChallanNo").val()==''&& jQuery("#GRNCreateInvoiceNo").val()==''){
					jQuery("#GRNCreateChallanNo").css("background","#FF9999");
					showMessage('yellow','Enter either Challan No or Invoice No .','');
					jQuery("#GRNCreateInvoiceNo").focus();
					return 0;
					}
				else
					{
					
					jQuery("#GRNCreateChallanNo").css("background","white");
					jQuery("#GRNCreateInvoiceNo").css("background","white");
					}
				if(jQuery("#GRNCreateInvoiceNo").val()!=''){
						if(jQuery("#GRNViewInvoiceAmtInclTax").val()==''||parseFloat(jQuery("#GRNViewInvoiceAmtInclTax").val(),10).toFixed(0)==0){
							jQuery("#GRNViewInvoiceAmtInclTax").css("background","#FF9999");
							showMessage('yellow','Enter invoice amount .','');
							jQuery("#GRNViewInvoiceAmtInclTax").focus();
							return 0;
						}
						else{
							jQuery("#GRNViewInvoiceAmtInclTax").css("background","white");
						}
						if(jQuery("#GRNCreateInvoiceDate").val()==''){
							jQuery("#GRNCreateInvoiceDate").css("background","#FF9999");
							showMessage('yellow','Enter Invoice date .','');
							jQuery("#GRNCreateInvoiceDate").focus();
							return 0;
						}
						else{
						
							jQuery("#GRNCreateInvoiceDate").css("background","white");
						}
						if (diffrenInDays(jQuery("#GRNCreateInvoiceDate").val(),jQuery("#GRNCreatePODate").val())<0){
							showMessage('yellow','Invoice date  cannot be lesser then PO Date','');
							jQuery("#GRNCreateInvoiceDate").focus();
							jQuery("#GRNCreateInvoiceDate").select();
							return 0;
						}
						if(isNaN(jQuery("#GRNViewInvoiceAmtInclTax").val())==true){
							jQuery("#GRNViewInvoiceAmtInclTax").css("background","#FF9999");
							showMessage('yellow','Enter Valid Invoice amount .','');
							jQuery("#GRNViewInvoiceAmtInclTax").focus();
							return 0;
						}
						else{
							jQuery("#GRNViewInvoiceAmtInclTax").css("background","white");
						}
						
				 }
				if(jQuery("#GRNCreateChallanNo").val()!=''){
					if(jQuery("#GRNCreateChallanDate").val()==''){
						
						jQuery("#GRNCreateChallanDate").css("background","#FF9999");
						showMessage('yellow','Enter challan date .','');
						jQuery("#GRNCreateChallanDate").focus();
						return 0;
					}
					else{
			
						jQuery("#GRNCreateChallanDate").css("background","white");
					}
					if (diffrenInDays(jQuery("#GRNCreateChallanDate").val(),jQuery("#GRNCreatePODate").val())<0){
						showMessage('yellow','Challan date cannot be lesser then PO Date','');
						jQuery("#GRNCreateChallanDate").focus();

						return 0;
					}
				}
				if(jQuery("#GRNViewInvoiceAmtInclTax").val!=''){
					jQuery("#GRNViewInvoiceAmtInclTax").css("background","white");
					jQuery("#GRNViewInvoiceAmtInclTax").focus();
				}
			
					var rowId=jQuery("#GRNBatchDetailsGrid").getGridParam('selrow');
					saveditableRows();
					if(jQuery("tr#"+rowId).attr("editable")==1 ){
						showAllRowsONSelectItemCode();
						
						
						showMessage('yellow','Save Editable Row from Batch Detail List. Use Save in Row.','');
						return 0;
					}
					if(isNaN(jQuery("#GRNViewNoOfBoxes").val())==true)
					{
						showMessage('yellow','Enter Valid No of Boxes','');
						jQuery("#GRNViewNoOfBoxes").focus();
						return 0;
						
					}
					if(jQuery("#GRNViewNoOfBoxes").val()=='')
					{
						showMessage('yellow','Enter Valid No of Boxes','');
						jQuery("#GRNViewNoOfBoxes").focus();
						return 0;
					}
					
					/*if(isNaN(jQuery("#GRNViewGrossWeight").val())==true)
					{
						showMessage('red','Enter Valid Weight','');
						jQuery("#GRNViewGrossWeight").focus();
						return 0;
					}
					if(jQuery("#GRNViewGrossWeight").val()=='')
					{
						showMessage('red','Enter Valid Weight','');
						jQuery("#GRNViewGrossWeight").focus();
						return 0;
					}*/
				if(	vallidateQtyOfPOItemsONSave()==0){
					return 0;
				}
				
				if(	vallidateForInvoiceQtyNotshorterThenReciveQty()==0){
					return 0;
				}
				return 1;
		}
		
		
		/**
		 * function used to return date formatted BatchDetailGrid which is initially be of kind Jun-2014 and after that would be of 01-Jun-2014.
		 */
		function getDateFormattedBatchDetailGrid(batchDetailGrid)
		{
			
			var monthsArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			
			jQuery.each(batchDetailGrid,function(key,value){
				
				var ManufacturingDate = new Date(batchDetailGrid[key]['ManufacturingDate']);
				var ExpiryDate = new Date(batchDetailGrid[key]['ExpiryDate']);
			
				var getManufacturingDay = ManufacturingDate.getDate();
				var getManufacturingMonth = monthsArray[ManufacturingDate.getMonth()];
				var getManufacturingYear = ManufacturingDate.getFullYear();
				
				var getExpiryDateDay = ExpiryDate.getDate();
				var getExpiryDateMonth = monthsArray[ExpiryDate.getMonth()];
				var getExpiryDateYear = ExpiryDate.getFullYear();
				
				batchDetailGrid[key]['ManufacturingDate'] = getManufacturingDay+'-'+getManufacturingMonth+'-'+getManufacturingYear;
				batchDetailGrid[key]['ExpiryDate'] = 28+'-'+getExpiryDateMonth+'-'+getExpiryDateYear;
				
			});
			
			
			return batchDetailGrid; 
		}
		

		
		
		
		jQuery("#btnShowAllbatch").click(function(){
			showAllRowsONSelectItemCode();
		});
		
	jQuery("#btnRCV01Save").unbind("click").click(function(){
		jQuery("#actionName").val('Save');

		if(validationOnSaveButton()==1){
			var batchDetailsGrid = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
			if(batchDetailsGrid.length==0){
				showMessage('yellow','Select atleast one Item in Batch List','');
				return;
			}
			
			batchDetailsGrid = JSON.stringify(getDateFormattedBatchDetailGrid(batchDetailsGrid));
			jsonForViewDetail=JSON.stringify(getJsonFromCreateGrid());
			
				var r =confirm("Are you sure you want to save data ?");
	if(r==true)
		{
		  showMessage("loading", "Saving GRN...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'GRNCallback',
				data:
				{
					id : 'saveGRN',
					jsonForBatchDetail:batchDetailsGrid,
					jsonForViewDetail:jsonForViewDetail,
					PONumber:jQuery("#GRNCreatePONumber").val(),
					ambendMentNO:jQuery("#GRNCreateAmendmentNo").val(),
					Status:1,
					GRNNo:jQuery("#GRNNo").val(),
					challanNo:jQuery("#GRNCreateChallanNo").val(),
					challanDate:getDate(jQuery("#GRNCreateChallanDate").val()),
					invoiveNo:jQuery("#GRNCreateInvoiceNo").val(),
					invoiceDate:getDate(jQuery("#GRNCreateInvoiceDate").val()),
					invoiceTax:jQuery("#GRNViewInvoiceTax").val(),
					invoiceAmt:jQuery("#GRNViewInvoiceAmtInclTax").val(),
					vehicleNo:jQuery("#GRNViewVehicleNo").val(),
					receiveBy:jQuery("#GRNViewReceivedBy").val(),
					noOfBox:jQuery("#GRNViewNoOfBoxes").val(),
					GrossWeight:jQuery("#GRNViewGrossWeight").val(),
					TransporterName:jQuery("#GRNViewTransporterName").val(),
					TransportNo:jQuery("#GRNTransportNo").val(),
					GatePassNo:jQuery("#GRNGatePassNo").val(),
					moduleCode:"RCV01",
					functionName:jQuery("#actionName").val()
				
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var keypairData = jQuery.parseJSON(data);
				if(keypairData.Status==1){
					keypair=keypairData.Result;
					if(keypair.length>=0){
						
					jQuery.each(keypair,function(key,value)
					{	
						
						if(keypair[key]['GRNNo']!=undefined){
							showMessage("green","Record Created Successfully "+keypair[key]['GRNNo'],"");
							jQuery("#GRNNo").val(keypair[key]['GRNNo']);
							jQuery("#GRNStatus").val('Created');
							disableButtons(1);
						}
						else
							{
							  if(keypair[key]['OutParam']=='INF0066'){
								  showMessage("yellow","All Previous GRN should be closed to Save GRN changes.","");
							  }
							  else{
								  showMessage("yellow","Unable to Save GRN Details. Error " + keypairData,"");
							  }
							
							}
					
						  /*enableDisableOnStatus(1);*/
						
					});
				 }
					else{
						showMessage("","GRN Details not Saved. Error " + keypairData,"");
					}
			    
				}
				else{
					showMessage('red',keypairData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
				});
			}
		
		   }
			else{
				return ;
			}
		
			
		});
	jQuery("#btnRCV01Cancel").unbind("click").click(function(){
		jQuery("#actionName").val('Cancel');
		if(validationOnSaveButton()==1){
		var batchDetailsGrid = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
		batchDetailsGrid = JSON.stringify(getDateFormattedBatchDetailGrid(batchDetailsGrid));
		jsonForViewDetail=JSON.stringify(getJsonFromCreateGrid());
			var r =confirm("Are you sure you want to cancel data ?");
if(r==true)
	{
	  showMessage("loading", "Cancelling GRN...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'GRNCallback',
			data:
			{
				id : 'saveGRN',
				jsonForBatchDetail:batchDetailsGrid,
				jsonForViewDetail:jsonForViewDetail,
				PONumber:jQuery("#GRNCreatePONumber").val(),
				ambendMentNO:jQuery("#GRNCreateAmendmentNo").val(),
				Status:2,
				GRNNo:jQuery("#GRNNo").val(),
				challanNo:jQuery("#GRNCreateChallanNo").val(),
				challanDate:getDate(jQuery("#GRNCreateChallanDate").val()),
				invoiveNo:jQuery("#GRNCreateInvoiceNo").val(),
				invoiceDate:getDate(jQuery("#GRNCreateInvoiceDate").val()),
				invoiceTax:jQuery("#GRNViewInvoiceTax").val(),
				invoiceAmt:jQuery("#GRNViewInvoiceAmtInclTax").val(),
				vehicleNo:jQuery("#GRNViewVehicleNo").val(),
				receiveBy:jQuery("#GRNViewReceivedBy").val(),
				noOfBox:jQuery("#GRNViewNoOfBoxes").val(),
				GrossWeight:jQuery("#GRNViewGrossWeight").val(),
				TransporterName:jQuery("#GRNViewTransporterName").val(),
				TransportNo:jQuery("#GRNTransportNo").val(),
				GatePassNo:jQuery("#GRNGatePassNo").val(),
				moduleCode:"RCV01",
				functionName:jQuery("#actionName").val()
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var keypairData = jQuery.parseJSON(data);
			if(keypairData.Status==1){
				keypair=keypairData.Result;
				if(keypair.length>=0){
					
				jQuery.each(keypair,function(key,value)
				{	
					showMessage("green","Record Cancelled Successfully "+keypair[key]['GRNNo'],"");
					jQuery("#GRNNo").val(keypair[key]['GRNNo']);
					jQuery("#GRNStatus").val('Canceled');
					  /*enableDisableOnStatus(1);*/
					disableButtons(2);
				
				});
			 }
				else{
					showMessage("","GRN Not Cancelled. Error " + keypairData,"");
				}
		    
			}
			else{
				showMessage('red',keypairData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
			});
		}
	
	   }
		else{
			return ;
		}
	
		
	});
		
	
	jQuery("#btnRCV01Close").unbind("click").click(function(){
		jQuery("#actionName").val('Close');
		if(validationOnSaveButton()==1)
		{
			
			
		var batchDetailsGrid = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
		batchDetailsGrid = JSON.stringify(getDateFormattedBatchDetailGrid(batchDetailsGrid));
		jsonForViewDetail=JSON.stringify(getJsonFromCreateGrid());
		if(batchDetailsGrid.length==0){
			showMessage('yellow','Select at least one item in Batch List','');
			return;
		}
			var r =confirm("Are you sure you want to confirm  data ?");
				if(r==true)
					{
					  showMessage("loading", "Closing GRN...", "");
						jQuery.ajax({
							type:'POST',
							url:Drupal.settings.basePath + 'GRNCallback',
							data:
							{
								id : 'saveGRN',
								jsonForBatchDetail:batchDetailsGrid,
								jsonForViewDetail:jsonForViewDetail,
								PONumber:jQuery("#GRNCreatePONumber").val(),
								ambendMentNO:jQuery("#GRNCreateAmendmentNo").val(),
								Status:3,
								GRNNo:jQuery("#GRNNo").val(),
								challanNo:jQuery("#GRNCreateChallanNo").val(),
								challanDate:getDate(jQuery("#GRNCreateChallanDate").val()),
								invoiveNo:jQuery("#GRNCreateInvoiceNo").val(),
								invoiceDate:getDate(jQuery("#GRNCreateInvoiceDate").val()),
								invoiceTax:jQuery("#GRNViewInvoiceTax").val(),
							    invoiceAmt:jQuery("#GRNViewInvoiceAmtInclTax").val(),
								vehicleNo:jQuery("#GRNViewVehicleNo").val(),
								receiveBy:jQuery("#GRNViewReceivedBy").val(),
								noOfBox:jQuery("#GRNViewNoOfBoxes").val(),
								GrossWeight:jQuery("#GRNViewGrossWeight").val(),
								TransporterName:jQuery("#GRNViewTransporterName").val(),
								TransportNo:jQuery("#GRNTransportNo").val(),
								GatePassNo:jQuery("#GRNGatePassNo").val(),
								moduleCode:"RCV01",
								functionName:jQuery("#actionName").val()
							
							},
							success:function(data)
							{
								jQuery(".ajaxLoading").hide();
							var keypairData = jQuery.parseJSON(data);
							if(keypairData.Status==1){
								keypair=keypairData.Result;
								if(keypair.length>=0){
									
								jQuery.each(keypair,function(key,value)
								{	
									showMessage("green","GRN Closed Successfully "+keypair[key]['GRNNo'],"");
									jQuery("#GRNNo").val(keypair[key]['GRNNo']);
									jQuery("#GRNStatus").val('Closed');
									disableButtons(3);
									  /*enableDisableOnStatus(1);*/
									
								});
							 }
								else{
									showMessage("yellow","GRN Not Closed. Error " + keypairDate,"");
								}
						    
							}
							else{
								showMessage('red',keypairData.Description,'');
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
							});
					}
	
	   }
		else{
			return ;
		}
	
		
	});
	
		 /*jQuery("#btnRCV01Print").click(function(){
			 	moduleCode="RCV01" ;
				functionName=jQuery("#actionName").val() ;
				var arr = {'GRNNo':jQuery("#GRNNo").val(),'PONumber':jQuery("#GRNCreatePONumber").val()};
				showReport('reports/GRNInvoice.rptdesign',arr); 
			
		 });*/
		 
		 jQuery("#btnRCV01Print").unbind("click").click(function(){
				jQuery("#actionName").val('Print');
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'GRNCallback',
					data:
					{
						id:"printGRN",
						GRNNo:jQuery("#GRNNo").val(),
						PONumber:jQuery("#GRNCreatePONumber").val(),
						functionName:jQuery("#actionName").val(),
						moduleCode:"RCV01"
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
						 jQuery.each(results,function(key,value)
								   {
							 			var arr = {'PONumber':results[key]['PONumber'],'GRNNo':results[key]['GRNNo'],'locationId':results[key]['locationId']};
							 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
									 
							      });
					}
					else{
						
						showMessage('red',resultsData.Description,'');
					}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});


			
		 });

		 jQuery("#btnReset").click(function(){
			 disableFieldOnStartup();
			 enableFieldOnStartup();
			 jQuery("#GRNBatchDetailsGrid").jqGrid('clearGridData');
				jQuery("#GRNViewGrid").jqGrid("clearGridData");
				jQuery("#GRNCreatePONumber").val('');
				disableButtons(0);
				jQuery("#GRNCreatePONumber").css("background","white");
				jQuery("#GRNCreateChallanNo").css("background","white");
				jQuery("#GRNCreateInvoiceNo").css("background","white");
				jQuery("#GRNCreateInvoiceDate").css("background","white");
				jQuery("#GRNCreateChallanDate").css("background","white");
		 });
		 
		 function disableButtons(statusId){
				     //onStartup
				 if(statusId==0){
						jQuery("#btnRCV01Print").attr('disabled',true);	
						jQuery("#btnRCV01Cancel").attr('disabled',true);	
						jQuery("#btnRCV01Close").attr('disabled',false);	
						jQuery("#btnRCV01Save").attr('disabled',false);
						jQuery("#btnRCV01Search").attr('disabled',false);
						jQuery("#GRNBatchDetailsGrid").showCol('myac');
						jQuery("#GRNViewGrid").showCol('AddBatches');
						disableEnableFieldOnStatus(0);
						
					}
				//CREATED
				if(statusId==1){
					
					jQuery("#btnRCV01Print").attr('disabled',true);	
					jQuery("#btnRCV01Cancel").attr('disabled',false);	
					jQuery("#btnRCV01Close").attr('disabled',false);	
					jQuery("#btnRCV01Save").attr('disabled',false);
					jQuery("#btnRCV01Search").attr('disabled',false);
					jQuery("#GRNBatchDetailsGrid").showCol('myac');
					jQuery("#GRNViewGrid").showCol('AddBatches');
					disableEnableFieldOnStatus(0);
				}
				//cancel
				else if(statusId==2){
					jQuery("#btnRCV01Print").attr('disabled',true);	
					jQuery("#btnRCV01Cancel").attr('disabled',true);	
					jQuery("#btnRCV01Close").attr('disabled',true);	
					jQuery("#btnRCV01Save").attr('disabled',true);
					jQuery("#btnRCV01Search").attr('disabled',false);
					jQuery("#GRNBatchDetailsGrid").hideCol('myac');
					jQuery("#GRNViewGrid").hideCol('AddBatches');
					disableEnableFieldOnStatus(1);
				}
				//closing
				else if(statusId==3){
					jQuery("#btnRCV01Print").attr('disabled',false);	
					jQuery("#btnRCV01Cancel").attr('disabled',true);	
					jQuery("#btnRCV01Close").attr('disabled',true);	
					jQuery("#btnRCV01Save").attr('disabled',true);
					jQuery("#btnRCV01Search").attr('disabled',false);	
					jQuery("#GRNBatchDetailsGrid").hideCol('myac');
					jQuery("#GRNViewGrid").hideCol('AddBatches');
					disableEnableFieldOnStatus(1);
				}
				actionButtonsStauts();
				
				
			}
	function disableEnableFieldOnStatus(IsDisable){
	if(IsDisable==1)
	{
		jQuery("#GRNCreateInvoiceDate").attr('disabled',true);	
		jQuery("#GRNCreateInvoiceNo").attr('disabled',true);	
		jQuery("#GRNCreateChallanNo").attr('disabled',true);	
		jQuery("#GRNCreateChallanDate").attr('disabled',true);
		
		jQuery("#GRNCreatePONumber").attr('disabled',true);
		jQuery("#GRNGatePassNo").attr('disabled',true);	
		jQuery("#GRNViewInvoiceAmtInclTax").attr('disabled',true);
		jQuery("#GRNViewNoOfBoxes").attr('disabled',true);	
		jQuery("#GRNViewVehicleNo").attr('disabled',true);	
		
		jQuery("#GRNViewGrossWeight").attr('disabled',true);	
		jQuery("#GRNGatePassNo").attr('disabled',true);	
		jQuery("#GRNTransportNo").attr('disabled',true);
		jQuery("#GRNViewTransporterName").attr('disabled',true);	
		jQuery(".CLaimQuantity").attr('disabled',true);	
		
	}		
	else{
		jQuery("#GRNCreateInvoiceDate").attr('disabled',false);	
		jQuery("#GRNCreateInvoiceNo").attr('disabled',false);	
		jQuery("#GRNCreateChallanNo").attr('disabled',false);	
		jQuery("#GRNCreateChallanDate").attr('disabled',false);
		jQuery(".CLaimQuantity").attr('disabled',false);	
		jQuery("#GRNCreatePONumber").attr('disabled',false);
		jQuery("#GRNGatePassNo").attr('disabled',false);	
		jQuery("#GRNViewInvoiceAmtInclTax").attr('disabled',false);
		jQuery("#GRNViewNoOfBoxes").attr('disabled',false);	
		jQuery("#GRNViewVehicleNo").attr('disabled',false);	
		jQuery("#GRNGatePassNo").attr('disabled',false);	
		jQuery("#GRNTransportNo").attr('disabled',false);
		jQuery("#GRNViewTransporterName").attr('disabled',false);
		jQuery("#GRNViewGrossWeight").attr('disabled',true);	
	}
		
	}		
});		
