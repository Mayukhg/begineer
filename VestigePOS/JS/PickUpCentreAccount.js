/*
 * While searching transaction then reversed trasction are shown 'RED' using function 'afterInsertRow' of jqgrid where color of row added is being made red.
 */


jQuery(document).ready(function(){
	
	
	
	jQuery("#header").hide();
	jQuery(".breadcrumb").hide();
	jQuery("#triptych-wrapper").hide();
	jQuery("#footer-wrapper").hide();
	
	var POSLocationId;
	var PUCId;
	var paymentMode;
	var date = new Date();
	var allPaymentMode;
	var totalDepositAmount = 0;
	var oPickUpCentreInfo;
	var day = date.getDate();
	var month = date.getUTCMonth()+1;
	var PUCAccDepositInformation;
	var loggedInUserPrivilegesForPUC;
	var year = date.getUTCFullYear();
	var PUCAccDepositGridId = 0;
	var PUCAccGridId = 0;
	
	
	 function showCalender3() {
			jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0',maxDate: new Date, onSelect:function(){
	            
	        	jQuery(this).focus();
	      
	           }});
			jQuery( ".showCalender" ).datepicker("setDate",new Date());
			
			//jQuery('.showCalender').attr('readonly', true);
		}
	
	jQuery(function() {
	    jQuery( "#divPUCRemarks" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      draggable: true,
	      modal:true,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	  });

	var moduleCode = jQuery("#moduleCode").val();
	
	
	/*--------Testing---------*/
	
	/*jQuery.ajax({
		type:'POST',
		url:"http://localhost:8080/VestOn/HelloWorld.Action",
		data:
		{
			name:"parveen"
		},
		success:function(data)
		{
			alert(data);
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
		
	});*/
	
	
	/*------------------------*/
	
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:
		{
			id:"moduleFunctionHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesPUC = jQuery.parseJSON(data);
			
			if(loggedInUserPrivilegesPUC.Status == 1)
			{
				
				loggedInUserPrivilegesForPUC  = loggedInUserPrivilegesPUC.Result;
				
				if(loggedInUserPrivilegesForPUC.length == 0)
				{
					//jQuery(".SCCreateActionButtons").attr('disabled',true); //all button coming as disabled already.
				}
				else
				{
					/*-------------calling initial state----------------*/
					//initialStateDIS();
					
					returnPaymentMode();
					btnSaveDisableFunction(1); //Disable save button
					btnClearDisableFunction(1); //Disable clear button
					
					initialState(); //initial state.
					
					showCalender3();
					
					//buttons are coming disabled previously.
					//initial state called here.
					//after that button that arte to be disabled from initial state are then called using function actionbuttonstatus()
					
					//enableOrShowElementByPrivilges();
					
					actionButtonsStauts();
					
					
				}
				
				
				
			}
			else
			{
				showMessage("red", loggedInUserPrivilegesPUC.Description, "");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
	
	
	function actionButtonsStauts()
	{
		jQuery(".PUCAccountAction").each(function(){
			
			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");
			
			if(loggedInUserPrivilegesForPUC[extractedButtonId] == 1)
			{
				
				jQuery("#"+buttonid).attr('disabled',false);
				
			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}
			
			
			jQuery("#btnPUCAccExit").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			jQuery("#btnPUCAccReset").attr('disabled',false); //temporary button for filling all fields.
			//jQuery("#btnPUC01Reverse").attr('disabled',false);
			
		});
	}
	
	
	jQuery("#transactionReverseRemarks").val('');
	
	myDelOptions = {
			
			
			
            // because I use "local" data I don't want to send the changes to the server
            // so I use "processing:true" setting and delete the row manually in onclickSubmit
            onclickSubmit: function (rp_ge, rowid) {
            	
            	 var removableOrNot = jQuery('#PUCAccDepositGrid').jqGrid('getCell',rowid, 'Removable');
            	
            		// reset the value of processing option to true to
                     // skip the ajax request to 'clientArray'.
                     rp_ge.processing = true;

                     // we can use onclickSubmit function as "onclick" on "Delete" button
                     // delete row
                     jQuery("#PUCAccDepositGrid").jqGrid('delRowData', rowid);
                     jQuery("#delmod" + jQuery("#PUCAccDepositGrid")[0].id).hide();
                     //calculateGridParamAfterUpdation();
                     if (jQuery("#PUCAccDepositGrid")[0].p.lastpage > 1) {
                         // reload grid to make the row from the next page visable.
                         // TODO: deleting the last row from the last page which number is higher as 1
                     	jQuery("#PUCAccDepositGrid").trigger("reloadGrid", [{ page: jQuery("#PUCAccDepositGrid")[0].p.page}]);
                     }

                     return true;
            	 
                
            },
            processing: true
        };
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * PUC Account Deposit
	 */
	jQuery("#PUCAccDepositGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Deposit Date','Deposit Mode','Transaction No','Deposit Amount','Remarks','Deposit Mode Id','Removable','PCID','ReverseTransStatus'],
		colModel: [/*{ name: 'Remove', width:80, fixed:true, sortable:false, resize:false, formatter:'actions',
			       	 formatoptions:{
			    		 keys:true,
			    		 onEdit:function(rowid) {
			    			 //var rowData = jQuery('#jqgridtable_left').jqGrid ('getRowData', rowid);
			                 //alert(rowData);
			                 //alert("in onEdit: rowid="+rowid+"\nWe don't need return anything");
			                 
			             },
			             onSuccess:function(jqXHR) {
			                 // the function will be used as "succesfunc" parameter of editRow function
			                 // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
			                 alert("in onSuccess used only for remote editing:"+
			                       "\nresponseText="+jqXHR.responseText+
			                       "\n\nWe can verify the server response and return false in case of"+
			                       " error response. return true confirm that the response is successful");
			                 // we can verify the server response and interpret it do as an error
			                 // in the case we should return false. In the case onError will be called
			                 return true;
			             },
			             onError:function(rowid, jqXHR, textStatus) {
			                 // the function will be used as "errorfunc" parameter of editRow function
			                 // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
			                 // and saveRow function
			                 // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#saverow)
			                 alert("in onError used only for remote editing:"+
			                       "\nresponseText="+jqXHR.responseText+
			                       "\nstatus="+jqXHR.status+
			                       "\nstatusText"+jqXHR.statusText+
			                       "\n\nWe don't need return anything");
			             },
			             afterSave:function(rowid) {
			                 //alert("in afterSave (Submit): rowid="+rowid+"\nWe don't need return anything");
			                 var rowData = jQuery('#jqgridtable_left').jqGrid ('getRowData', rowid);
			                 var itemDistributorPrice = rowData.DistributorPrice;
			                 var itemId = rowData.RowID;
			                 var itemQty = rowData.Qty;
			                 calculateGridParamAfterUpdate(itemDistributorPrice,itemQty,rowid,itemId);
			                 //alert(rowData);
			             },
			             afterRestore:function(rowid) {
			                 alert("in afterRestore (Cancel): rowid="+rowid+"\nWe don't need return anything");
			                
			             },
			             delOptions:myDelOptions
			    	 }
			       },*/
		           { name: 'DepositDate', index: 'DepositDate', width: 100},
		           { name: 'DepositMode', index: 'DepositMode', width: 100},
		           { name: 'TransactionNo', index: 'TransactionNo', width: 200},
		           { name: 'DepositAmount', index: 'DepositAmount', width: 100},
		           { name: 'Remarks', index: 'Remarks', width: 200},
		           { name: 'DepositModeId', index: 'DepositModeId', width: 100,hidden:true},
		           { name: 'Removable', index: 'Removable', width: 100,hidden:true},
		           { name: 'PCID', index: 'PCID', width: 100,hidden:true},
		           { name: 'ReverseTransStatus',index: 'ReverseTransStatus', width: 100,hidden:true}],
		           pager: jQuery('#pjmap_PUCAccDepositGrid'),
		           width:1040,
		           height:200,
		           rowNum: 20,
		           rowList: [500],
		           sortname: 'Label',
		           sortorder: "asc",
		           hoverrows: true,
		           	
		           onSelectRow: function (id) {
		        	   var transactionNo = jQuery('#PUCAccDepositGrid').jqGrid('getCell',id, 'TransactionNo');
		        	   var depositAmt = jQuery('#PUCAccDepositGrid').jqGrid('getCell',id, 'DepositAmount');
		        	   var depositModeId = jQuery('#PUCAccDepositGrid').jqGrid('getCell',id, 'DepositModeId');
		        	   var selectedPCId = jQuery('#PUCAccDepositGrid').jqGrid('getCell',id, 'PCID');
		        	   
		        	   jQuery("#PUCAccTransactionNo").val(transactionNo);
		        	   jQuery("#PUCAccAmount").val(depositAmt);
		        	   jQuery("#PUCAccPaymentMode").val(depositModeId);
		        	   jQuery("#PUCAccPickUpCentre").val(selectedPCId);
		        	   //getGiftVoucherItem(giftVoucherCode,giftVoucherSeriesNo);
	               },
	               afterInsertRow : function(rowid, rdata){
	            	   
	            	   var getReverseTrasnStatus = jQuery('#PUCAccDepositGrid').jqGrid('getCell',rowid, 'DepositAmount');
	            	   
	            	   if(getReverseTrasnStatus < 0) {
	            	    jQuery("#"+rowid,"#PUCAccDepositGrid").css({'background-color':'red', color: 'red'});
	            	   }
	            	   
	               }   



	});
		

	function reverseTransactionFormatter(cellvalue, options, row) 
	{
		//alert(cellvalue);
		
		if (cellvalue == 0) 
		{
			//return 'background-color:red';

		}
		if (cellvalue == 1) 
		{
			//return 'background-color:red';

		}
		
	}	
	

	jQuery("#PUCAccDepositGrid").jqGrid('navGrid','#pjmap_PUCAccDepositGrid',{edit:false,add:false,del:false});	
	jQuery("#pjmap_PUCAccDepositGrid").hide();
	
	
	
	function selectDepositForPC(PUCId)
	{
		
		jQuery('#PUCAccDepositGrid').jqGrid('clearGridData', true);
		
		
		var selectedPUCAccDepositGridId = 0;
		
		jQuery.each(PUCAccDepositInformation,function(key,value){
			
			if(PUCAccDepositInformation[key]['PCID'] == PUCId)
			{
				selectedPUCAccDepositGridId = selectedPUCAccDepositGridId + 1;
				
				var PUCAccDepositData = [{"DepositDate":PUCAccDepositInformation[key]['DEPOSITDATE'],"DepositMode":PUCAccDepositInformation[key]['DEPOSITTYPE'],"TransactionNo":PUCAccDepositInformation[key]['TRANSACTIONNO'],"DepositAmount":formatFloatNumbers(parseFloat(PUCAccDepositInformation[key]['DEPOSITAMOUNT'])),"DepositModeId":PUCAccDepositInformation[key]['DEPOSITMODE'],"Removable":0,"PCID":PUCAccDepositInformation[key]['PCID'],"ReverseTransStatus":0}];
				for (var i=0;i<PUCAccDepositData.length;i++) {
					jQuery("#PUCAccDepositGrid").jqGrid('addRowData', selectedPUCAccDepositGridId, PUCAccDepositData[PUCAccDepositData.length-i-1], "last");
				}
			}
		});
	}
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * PUC Account Grid 
	 */
	jQuery("#PUCAccGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['BO Location','PUC location','PUC Location Name','Deposit Amount','Used Amount'],
		colModel: [{ name: 'BOLocation', index: 'BOLocation', width:100 },
		           { name: 'PUCLocation', index: 'PUCLocation', width: 150,hidden:true},
		           { name: 'PUCLocationName', index: 'PUCLocationName', width: 150},
		           { name: 'DepositAmount', index: 'DepositAmount', width: 150},
		           { name: 'UsedAmount', index: 'UsedAmount', width: 100}],
		           pager: jQuery('#pjmap_PUCAccGrid'),
		           width:1040,
		           height:150,
		           rowNum: 20,
		           rowList: [500],
		           sortname: 'Label',
		           sortorder: "asc",
		           hoverrows: true,
		           	
		           onSelectRow: function (id) {
		        	   var PUCId = jQuery('#PUCAccGrid').jqGrid('getCell',id, 'PUCLocation');
		        	   //var giftVoucherSeriesNo = jQuery('#PUCAccGrid').jqGrid('getCell',id, 'VoucherSrNo');
		        	   selectDepositForPC(PUCId);
	               },


		           

		           loadComplete: function() {
//		        	   jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//		        	   alert("laodign complete");
		           }	
		           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

	});
		

		
	

	jQuery("#PUCAccGrid").jqGrid('navGrid','#pjmap_PUCAccGrid',{edit:false,add:false,del:false});	
	jQuery("#pjmap_PUCAccGrid").hide();
	
	
	/*---------------------------Getting location id from server-----------------------------*/
	
	showMessage("loading", "Loading PUC Transactions...", "");
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'PUCAccountCallback',
		data:
		{
			id:"locationId"

		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var POSLocationInfoData = jQuery.parseJSON(data);
			
			if(POSLocationInfoData.Status == 1)
			{
				POSLocationInfo = POSLocationInfoData.Result;
				
				var POSLocationName = POSLocationInfo[0]['Name'];
				var POSLocationCode = POSLocationInfo[0]['Locationcode'];
				
				POSLocationId= POSLocationInfo[0]['LocationId'];
				jQuery('#PUCAccLocationCode').val(POSLocationName+"-"+POSLocationCode);
			}
			else
			{
				showMessage("red", POSLocationInfoData.Description, "");
			}
			
			
			

		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}


	});
	
	
	loadAllPUC();
	/*----------------------Load all PUC---------------------------------*/
	
	/**
	 * funciton used to reset pick up centres.
	 */
	function resetingPUC()
	{
		//oPickUpCentreInfo = pickUpCentreData.Result;
		
		jQuery("#PUCAccPickUpCentre").empty();
		
		jQuery("#PUCAccPickUpCentre").append("<option name='state' value='0'>Select</option>");
		jQuery.each(oPickUpCentreInfo,function(key,value){
			
			
			if(oPickUpCentreInfo[key]['LocationConfigId'] == 3)
			{
				
			}
			else
			{
				jQuery("#PUCAccPickUpCentre").append("<option name='get_address_stock_loc_code' id=\""+oPickUpCentreInfo[key]['LocationId']+"\""+" value=\""+oPickUpCentreInfo[key]['LocationId']+"\""+">"+oPickUpCentreInfo[key]['LocationCode']+"</option>");
			}
				
		});
	}
	
	function loadAllPUC()
	{
		showMessage("loading", "Loading pick up centres...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"stock_delivery_info",

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var pickUpCentreData= jQuery.parseJSON(data);
				
				if(pickUpCentreData.Status == 1)
				{
					oPickUpCentreInfo = pickUpCentreData.Result;
					
					jQuery("#PUCAccPickUpCentre").empty();
					
					jQuery("#PUCAccPickUpCentre").append("<option name='state' value='0'>Select</option>");
					jQuery.each(oPickUpCentreInfo,function(key,value){
						
						
						if(oPickUpCentreInfo[key]['LocationConfigId'] == 3)
						{
							
						}
						else
						{
							jQuery("#PUCAccPickUpCentre").append("<option name='get_address_stock_loc_code' id=\""+oPickUpCentreInfo[key]['LocationId']+"\""+" value=\""+oPickUpCentreInfo[key]['LocationId']+"\""+">"+oPickUpCentreInfo[key]['LocationCode']+"</option>");
						}
							
					});
				
				}
				else
				{
					showMessage("red", pickUpCentreData.Description, "");
				}
				
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	
	
	/*----------------------------Date selection----------------------------*/
	
	 
	 /*jQuery( "#PUCAccDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy'});
	 
	 jQuery( "#PUCAccDate" ).datepicker("setDate",new Date());*/
	 
	 function stateAfterAdd()
		{
			jQuery(".requiredField").attr("disabled",true);
			jQuery(".requiredList").attr("disabled",true);
			jQuery("#PUCAccAmount").attr("disabled",true);
			jQuery("#PUCAccTransactionNo").attr("disabled",true);
			jQuery("#btnPUCAccClear").attr("disabled",false);
			jQuery("#btnPUCAccAdd").attr("disabled",true);
		}
		
	
	/*------------------------------------Add button click------------------------------*/
	jQuery("#btnPUCAccAdd").click(function(){
		
		var validForAddition  = validationToAddAmount();
		if(validForAddition == 1)
		{
			
			var selectedDepositId = jQuery("#PUCAccPaymentMode").val();
			//alert(selectedDepositId);
			//var depositMode = returnPaymentMode(selectedDepositId);
			//alert(allPaymentMode);
			jQuery.each(allPaymentMode,function(key,value){
				
				if(selectedDepositId == allPaymentMode[key]['Id'])
				{
					depositMode = allPaymentMode[key]['Description'];
					return false;
					
				}
			});
			var selectedDate = jQuery("#PUCAccDate").val();
			var transactionNumber = jQuery("#PUCAccTransactionNo").val();
			var depositAmount = jQuery("#PUCAccAmount").val();
			depositAmount = formatFloatNumbers(parseFloat(depositAmount));
			PUCAccDepositGridId = PUCAccDepositGridId + 1;
			var PUCId = jQuery("#PUCAccPickUpCentre").val();
			var PUCAccDepositData = [{"DepositDate":selectedDate,"DepositMode":depositMode,"TransactionNo":transactionNumber,"DepositAmount":depositAmount,'DepositModeId':selectedDepositId,"Removable":1,"ReverseTransStatus":0,"PCID":PUCId}];
			for (var i=0;i<PUCAccDepositData.length;i++) {
				jQuery("#PUCAccDepositGrid").jqGrid('addRowData', PUCAccDepositGridId, PUCAccDepositData[PUCAccDepositData.length-i-1], "first");
			}
			
			stateAfterAdd();
			
			
			
			btnSaveDisableflag = 0;
			btnSaveDisableFunction(btnSaveDisableflag);
			
			if(jQuery("#PUCAccAmount").val() == '')
			{
				tempValidationMsg = 0;
				jQuery("#PUCAccAmount").css("background","#FF9999");
			}
		}
		else
		{
			
		}
		
	});
	
	
	function btnSaveDisableFunction(btnSaveDisableflag)
	{
		if(btnSaveDisableflag == 0)
		{
			jQuery("#btnPUC01Save").attr("disabled",false);
		}
		else
		{
			jQuery("#btnPUC01Save").attr("disabled",true);
		}
	}
	
	function stateAfterClear()
	{
		jQuery("#PUCAccDate").attr("disabled",false);
		jQuery("#PUCAccPaymentMode").attr("disabled",false);
		jQuery("#PUCAccTransactionNo").attr("disabled",false);
		jQuery("#PUCAccTransactionNo").val('');
		jQuery("#PUCAccAmount").attr("disabled",false);
		jQuery("#PUCAccAmount").val('');
		jQuery("#btnPUCAccClear").attr("disabled",false);
		jQuery("#btnPUCAccReset").attr("disabled",false);
		//returnPaymentMode();
		jQuery("#btnPUCAccAdd").attr("disabled",false);
		jQuery("#btnPUC01Save").attr("disabled",true);
		
		actionButtonsStauts();
	}
	
	/*--------------------------clear button clicked---------------------------*/
	jQuery("#btnPUCAccClear").click(function(){
		
		stateAfterClear();
		
	});
	
	/*--------------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * function to validate necessary fields for adding amount for particular pick up centre.
	 */
	function validationToAddAmount()
	{
		var tempValidationMsg = 1;
		jQuery(".requiredField").each(function(){
			
			var currentId = this.id;
			if(jQuery("#"+currentId).val() == '')
			{
				jQuery("#"+currentId).css("background","#FF9999");
				tempValidationMsg = 0;
			}
			else
			{
				jQuery("#"+currentId).css("background","white");
			}
		});
		
		jQuery(".requiredList").each(function(){
			
			var currentListId = this.id;
			if(jQuery("#"+currentListId).val() == 0)
			{
				
				jQuery("#"+currentListId).css("background","#FF9999");
				tempValidationMsg = 0;
			}
			else
			{
				jQuery("#"+currentListId).css("background","white");
			}
		});
		
		if(jQuery("#PUCAccAmount").val() == '')
		{
			tempValidationMsg = 0;
			jQuery("#PUCAccAmount").css("background","#FF9999");
		}
		else
		{
			jQuery("#PUCAccAmount").css("background","white");
		}
		
		return tempValidationMsg;
	}
	
	/*-------------------------Select Payment Mode------------------------*/
	jQuery("#PUCAccPaymentMode").change(function(){
		paymentMode = this.value;
		
	});
	
	
	
	/*-----------------------------------------------------------------------------------------------------------------------------------*/
	function returnPaymentMode()
	{
		//var paymentMode;
		showMessage("loading", "Loading payment modes...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"payment_modes",

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				paymentModeData = jQuery.parseJSON(data);
				
				if(paymentModeData.Status == 1)
				{
					allPaymentMode = paymentModeData.Result;
				}
				else
				{
					showMessage("red", paymentModeData.Description, "");
				}
				
					
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
			
			//return paymentMode;
		});
		
		
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * function to validate necessary fields for searching PUC Account.
	 */
	function validationToSearchPUCAccDeposit()
	{
		var tempValidationMsg = 1;
		jQuery(".requiredField").each(function(){
			
			var currentId = this.id;
			if(jQuery("#"+currentId).val == '')
			{
				jQuery("#"+currentId).css("background","#FF9999");
				tempValidationMsg = 0;
			}
			else
			{
				jQuery("#"+currentId).css("background","white");
			}
		});
		
		jQuery(".requiredList").each(function(){
			
			var currentListId = this.id;
			if(jQuery("#"+currentListId).val() == 0)
			{
				
				jQuery("#"+currentListId).css("background","#FF9999");
				tempValidationMsg = 0;
			}
			else
			{
				jQuery("#"+currentListId).css("background","white");
			}
		});
		
		
		
		return tempValidationMsg;
	}
	
	/*-----------------------PUC Selection----------------------*/
	jQuery("#PUCAccPickUpCentre").change(function(){
		//var PUCId = this.value;
		var id = jQuery(this).val();//find(':selected')[0].id; //initially code written for id but now on value(which is pucid).
		//alert(PUCId);
		PUCId = id;
		//alert(id);
	});
	
	
	/*------------------------------Search PUC Acc ----------------------------------*/
	jQuery("#btnPUC01Search").unbind('click').click(function(){
		
		var PUCSelectedDate;
		
		showMessage("loading", "Searching deposits...", "");
		
		jQuery('#PUCAccDepositGrid').jqGrid('clearGridData');
		jQuery('#PUCAccGrid').jqGrid('clearGridData');
		
		jQuery("#btnPUCAccAdd").attr("disabled",true);
		var PUCSelectionCheck = jQuery("#PUCAccPickUpCentre").val();
		if(PUCSelectionCheck == 0)
		{
			PUCId = -1;
		}
		var PUCPaymentMode  = jQuery("#PUCAccPaymentMode").val();
		if(PUCPaymentMode == 0)
		{
			paymentMode = -1;
		}
		var PUCDate = getDate(jQuery("#PUCAccDate").val());
		
		if(PUCDate == '')
		{
			PUCSelectedDate = '1/1/1900';
		}
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'PUCAccountCallback',
			data:
			{
				id:"PUCAccDepositSearch",
				POSLocationId:POSLocationId,
				PUCId:PUCId,
				paymentMode:paymentMode,
				transactionNo:jQuery("#PUCAccTransactionNo").val(),
				date:PUCDate,
				mode:0,
				depmnt:'',
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				jQuery('#PUCAccDepositGrid').jqGrid('clearGridData');
				jQuery('#PUCAccGrid').jqGrid('clearGridData');
				var PUCAccDepositData = jQuery.parseJSON(data);
				
				if(PUCAccDepositData.Status == 1)
				{
					stateAfterSearch(); //Change screen state after search.
					
					PUCAccDepositInformation = PUCAccDepositData.Result;
					
					jQuery.each(PUCAccDepositInformation,function(key,value){
						
						PUCAccDepositGridId = PUCAccDepositGridId + 1;
						var PUCAccDepositData = [{"DepositDate":PUCAccDepositInformation[key]['DEPOSITDATE'],"DepositMode":PUCAccDepositInformation[key]['DEPOSITTYPE'],"TransactionNo":PUCAccDepositInformation[key]['TRANSACTIONNO'],"DepositAmount":formatFloatNumbers(parseFloat(PUCAccDepositInformation[key]['DEPOSITAMOUNT'])),"Remarks":PUCAccDepositInformation[key]['Remarks'],"DepositModeId":PUCAccDepositInformation[key]['DEPOSITMODE'],"Removable":0,"PCID":PUCAccDepositInformation[key]['PCID']}];
						for (var i=0;i<PUCAccDepositData.length;i++) {
							jQuery("#PUCAccDepositGrid").jqGrid('addRowData', PUCAccDepositGridId, PUCAccDepositData[PUCAccDepositData.length-i-1], "last");
						}
						
					});
					
					calculateAmountForAllTransaction(PUCAccDepositInformation);
				}
				else
				{
					showMessage("red", PUCAccDepositData.Description, "");
				}
	
			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}


		});
		
	});
	
	
	//define state on screen after search.
	function stateAfterSearch()
	{
		jQuery(".requiredField").attr("disabled",true);
		//jQuery(".requiredList").attr("disabled",true);
		jQuery("#PUCAccAmount").attr("disabled",true);
		jQuery("#PUCAccTransactionNo").attr("disabled",true);
		jQuery("#btnPUCAccClear").attr("disabled",false);
		jQuery("#btnPUCAccAdd").attr("disabled",true);
		jQuery("#btnPUCAccReset").attr("disabled",false);
		jQuery("#btnPUC01Save").attr("disabled",true);
		jQuery("#PUCAccTransactionNo").attr('disabled',false);
		//jQuery("#PUCAccAmount").attr('disabled',false);
		
		actionButtonsStauts();
	}
	
	
	/*--------------------------------------------------------------------------------------------------------------*/
	/**
	 * function calculate summation of all available deposit amount.
	 */
	function calculateAmountForAllTransaction(PUCAccDepositInformation)
	{
		
		var pucLocationName;
		var allPUC = jQuery("#PUCAccDepositGrid").jqGrid('getCol','PCID',false);
		var uniquePUC = GetUnique(allPUC);
		var BOLocationCode = jQuery("#PUCAccLocationCode").val();
		
		for(var i=0;i<uniquePUC.length;i++)
		{
			PUCAccGridId = PUCAccGridId + 1;
			totalDepositAmountAtPUC = 0.0;
			jQuery.each(PUCAccDepositInformation,function(key,value){
				
				if(uniquePUC[i] == PUCAccDepositInformation[key]['PCID'])
				{
					pucLocationName = PUCAccDepositInformation[key]['PUCLOCATION'];
					
					totalDepositAmountAtPUC = totalDepositAmountAtPUC + parseFloat(PUCAccDepositInformation[key]['DEPOSITAMOUNT']);
				}
			});
			
			var usedAmount = PUCAccDepositInformation[i]['UsedAmount'];
			
			
			
			var PUCAccData = [{"BOLocation":BOLocationCode,"PUCLocationName":pucLocationName,"PUCLocation":uniquePUC[i],"DepositAmount":totalDepositAmountAtPUC,"UsedAmount":parseFloat(usedAmount)}];
			
			for (var j=0;j<PUCAccData.length;j++) {
				jQuery("#PUCAccGrid").jqGrid('addRowData', PUCAccGridId, PUCAccData[PUCAccData.length-j-1], "last");
			}
			
		}
		/*var totalDepositAmount = 0.0;
		
		var allPaymentAmounts = jQuery("#PUCAccDepositGrid").jqGrid('getCol','DepositAmount',false);
		for(var i=0;i<allPaymentAmounts.length;i++)
		{
			totalDepositAmount+= parseFloat(allPaymentAmounts[i]);
			
		}
		//var BOLocationCode = jQuery("#PUCAccLocationCode").val();
		var PUCLocationId = jQuery("#PUCAccPickUpCentre").val();
		totalDepositAmount = formatFloatNumbers(totalDepositAmount);
		var PUCAccData = [{"BOLocation":BOLocationCode,"PUCLocation":PUCLocationId,"DepositAmount":totalDepositAmount,"UsedAmount":''}];
		
		for (var i=0;i<PUCAccData.length;i++) {
			jQuery("#PUCAccGrid").jqGrid('addRowData', PUCAccGridId, PUCAccData[PUCAccData.length-i-1], "last");
		}*/
	}
	
	
	function GetUnique(inputArray)
	{
	    var outputArray = [];
	    
	    for (var i = 0; i < inputArray.length; i++)
	    {
	        if ((jQuery.inArray(inputArray[i], outputArray)) == -1)
	        {
	            outputArray.push(inputArray[i]);
	        }
	    }
	   
	   
	    return outputArray;
	}
	
	function initialState()
	{
		jQuery(".requiredField").attr("disabled",false);
		jQuery("#PUCAccLocationCode").attr("disabled",true);
		jQuery(".requiredList").attr("disabled",false);
		jQuery(".addRequiredField").attr("disabled",false);
		jQuery("#PUCAccTransactionNo").attr("disabled",false);
		jQuery("#btnPUCAccClear").attr("disabled",false);
		jQuery("#btnPUCAccAdd").attr("disabled",false);
		jQuery("#btnPUCAccReset").attr("disabled",false);
		jQuery("#btnPUC01Save").attr("disabled",true);
		
		showCalender3();
		
		actionButtonsStauts();
		
	}
	
	/*----------------------------------------Reset state----------------------------------------*/
	jQuery("#btnPUCAccReset").click(function(){
		jQuery('#PUCAccDepositGrid').jqGrid('clearGridData', true);
		jQuery('#PUCAccGrid').jqGrid('clearGridData', true);
		//loadAllPUC();
		
		resetingPUC();
		
		initialState();
		
		jQuery("#PUCAccTransactionNo").val('');
		jQuery("#PUCAccAmount").val('');
	});
	
	/*--------------------------------------------------------------------------------------------------------------------*/
	function btnClearDisableFunction(disableClearButtonFlag)
	{
		if(disableClearButtonFlag == 0)
		{
			jQuery("#btnPUCAccClear").attr("disabled",false);
		}
		else
		{
			jQuery("#btnPUCAccClear").attr("disabled",true);
		}
	}
	
	
	function stateAfterSave()
	{
		
		jQuery(".requiredField").attr("disabled",true);
		jQuery(".requiredList").attr("disabled",true);
		jQuery("#PUCAccAmount").attr("disabled",true);
		jQuery("#PUCAccTransactionNo").attr("disabled",true);
		jQuery("#btnPUCAccClear").attr("disabled",false);
		jQuery("#btnPUCAccAdd").attr("disabled",true);
		jQuery("#btnPUCAccReset").attr("disabled",false);
		jQuery("#btnPUC01Save").attr("disabled",true);
		
		actionButtonsStauts();
	}
	
	
	/**
	 * function used to check multiple transactio with same transaction number exist or not.
	 */
	function checkingSameMultipleTransactions(PUCAccDepositGridData)
	{
		var multipleTransactionChecker1 = 0;
		
		jQuery.each(PUCAccDepositGridData,function(key,value){
			
			var multipleTransactionChecker = 0;
			
			var transactionNumber = PUCAccDepositGridData[key]['TransactionNo'];
			
			
			jQuery.each(PUCAccDepositGridData,function(key,value){
				
				if(transactionNumber == PUCAccDepositGridData[key]['TransactionNo'])
				{
					multipleTransactionChecker +=1;
				}
				
				
				
			});
			
			
			if(multipleTransactionChecker > 1)
			{
				multipleTransactionChecker1 = multipleTransactionChecker;
				
				return false;
			}
		
			
		});
		
		return multipleTransactionChecker1;
	}
	
	/*--------------------Save Deposit Amount-------------------*/
	
	jQuery("#btnPUC01Save").click(function(){
		
		showMessage("loading", "Saving PUC Amount...", "");
		
		var PUCAccDepositGridData = jQuery("#PUCAccDepositGrid").jqGrid('getRowData');
		
		if(checkingSameMultipleTransactions(PUCAccDepositGridData) == 0)
		{
			PUCAccDepositJson = JSON.stringify(PUCAccDepositGridData);
			var availableAmount = jQuery("#PUCAccAmount").val();
			
			var modifiedDate = getDate(jQuery("#PUCAccDate").val());
			
			
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'PUCAccountCallback',
				data:
				{
					id:"savePUCInfo",
					PUCAccDepositJson:PUCAccDepositJson,
					POSLocationId:POSLocationId,
					PUCId:PUCId,
					availableAmount:availableAmount,
					DBAvailAmt:availableAmount,
					pucAccountDepositStatus:1,
					modifiedDate:modifiedDate,
					

				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var pucAccountSaveData = jQuery.parseJSON(data);
					
					if(pucAccountSaveData.Status == 1)
					{
						oPucAccountSaveResult = pucAccountSaveData.Result;
						
						if(oPucAccountSaveResult[0]['outParam'] == 1)
						{
							
							showMessage("green", "PUC Account Updated Successfully", "");

							stateAfterSave();
							
							
						}
						else
						{
							showMessage("", "Transaction Number Already Exist. Choose Another and Try Again.", "");
							
						}
					}
					else
					{
						showMessage("red", pucAccountSaveData.Description, "");
					}
					
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}


			});
		}
		else
		{
			showMessage("", "Multiple transaction with same transation number exist. Contact System Administrator", "");
			
			jQuery(".ajaxLoading").hide();
		}
		
		
	});

	
	function reversePUCTransaction(sPUCTransactionData,PUCId,availableAmount,PUCAccountTransactionDetailsId,transactionNo,reverseTransactionRemarks)
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'PUCAccountCallback',
			data:
			{
				id:"reversePUCAccount",
				PUCAccDepositJson:sPUCTransactionData,
				POSLocationId:POSLocationId,
				PUCId:PUCId,
				availableAmount:availableAmount,
				DBAvailAmt:availableAmount,
				ReverseTransactionRemarks:reverseTransactionRemarks,
				pucAccountDepositStatus:2,
				modifiedDate:getDate(jQuery("#PUCAccDate").val()),
				

			},
			success:function(data)
			{
				
				var reverseTransationData = jQuery.parseJSON(data);

				if(reverseTransationData.Status == 1)
				{
					var results = reverseTransationData.Result;

					if(results[0]['outParam'] == 1)
					{
						jQuery("#divPUCRemarks").dialog("close");
						
						showMessage("green", "Transaction Reversed for PUC", "");
						
						notifyReverseTransactionInGrid(PUCAccountTransactionDetailsId,transactionNo);
					}
					else
					{
						showMessage("red", "Transaction Not Reversed. Error " + reverseTransationData, "");
					}
				}
				else
				{
					showMessage("red", reverseTransationData.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	jQuery("#btnPUC01Reverse").unbind('click').click(function(){
		
		jQuery("#transactionReverseRemarks").val('');
		
		jQuery( "#divPUCRemarks" ).dialog( "open" );
		
		jQuery("#btnPUCRemarks").unbind('click').click(function(){
			
			if(jQuery("#transactionReverseRemarks").val().trim() == '')
			{
				showMessage("", "Remarks Required for Reversal", "");
				jQuery("#transactionReverseRemarks").focus();
			}
			else
			{
				
				var PUCAccountTransactionDetailsId = jQuery('#PUCAccDepositGrid').jqGrid('getGridParam','selrow');
				if(PUCAccountTransactionDetailsId) //if row selected.
				{
					var PUCId = jQuery("#PUCAccPickUpCentre").val();

					var reverseTransactionRemarks = jQuery("#transactionReverseRemarks").val();

					var pucTransactionData = jQuery("#PUCAccDepositGrid").jqGrid ('getRowData', PUCAccountTransactionDetailsId);

					var PUCId = pucTransactionData.PCID;
					
					var transactionNo = pucTransactionData.TransactionNo;

					var transactionDepositAmount = parseInt(parseFloat(pucTransactionData.DepositAmount));
					
					var pucTransactionDataArray = new Array();
					
					pucTransactionDataArray[0] = pucTransactionData;

					var sPUCTransactionData = JSON.stringify(pucTransactionDataArray);

					var r=confirm("Do you want to reverse transaction for PUC?");
					if (r==true)
					{
						//reverseTransaction(PUCId,transactionNo,transactionDepositAmount,PUCAccountTransactionDetailsId);
						
						
						reversePUCTransaction(sPUCTransactionData,PUCId,transactionDepositAmount,PUCAccountTransactionDetailsId,transactionNo,reverseTransactionRemarks);
						
					}
					else
					{
						//nothing.
					}
				}
				else 
				{
					//alert("No selected row");
					showMessage("", "Select ONLY One Transaction To Reverse", "");
				}
				
			}
			
			
		});
		
	});
	
	jQuery("#btnPUCAccExit").click(function(){
		
		window.close();
		
	});
	
	
	//function used to reverse transaction.
	function reverseTransaction(PUCId,transactionNo,transactionDepositAmount,PUCAccountTransactionDetailsId)
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'PUCAccountCallback',
			data:
			{
				id:"reverseTransactionForPUC",
				PUCId:PUCId,
				transactionNo:transactionNo,
				transactionDepositAmount:transactionDepositAmount,
				remarks:jQuery("#transactionReverseRemarks").val(),

			},
			success:function(data)
			{
				//alert(data);
				
				var reverseTransationData = jQuery.parseJSON(data);
				
				if(reverseTransationData.Status == 1)
				{
					var results = reverseTransationData.Result;
					
					if(results.PUCDepositFlag == 1)
					{
						showMessage("green", "Transaction Reversed", "");
						
						notifyReverseTransactionInGrid(PUCAccountTransactionDetailsId,transactionNo);
					}
					else
					{
						showMessage("red", "Transaction Not Reversed. Error" + reverseTransationData, "");
					}
				}
				else
				{
					showMessage("red", reverseTransationData.Description, "");
				}
					
			}
			
			//return paymentMode;
		});
	}
	
	
	function notifyReverseTransactionInGrid(PUCAccountTransactionDetailsId,transactionNo)
	{
		//jQuery("#PUCAccDepositGrid").setCell(PUCAccountTransactionDetailsId,'TransactionNo',transactionNo,{'color':'#FF0000'});
		 jQuery("#"+PUCAccountTransactionDetailsId,"#PUCAccDepositGrid").css({'background-color':'red', color: 'red'});
		
		//"ReverseTransStatus":0
	}
	
	
});

