var jsonForLocations;
var loggedInUserPrivileges;
jQuery(document).ready(function(){

	
	
	jQuery("#main-menu").hide();

	jQuery(".breadcrumb").hide();

	jQuery("#triptych-wrapper").hide();

	var moduleCode = jQuery("#moduleCode").val();
	showCalender();
	showCalender2();
	getCurrentLocation();
	jQuery(function() {
		jQuery( "#divLookUp" ).dialog({
			autoOpen: false,
			minWidth: 625,
			show: {
				effect: "clip",
				duration: 200
			},
			hide: {
				effect: "clip",
				duration: 200
			}

		});
	});
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
		data:
		{
			id:"StockCountModuleFuncHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

			if(loggedInUserPrivilegesForStockCountData.Status == 1)
			{


				loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

				if(loggedInUserPrivileges.length == 0)
				{
					jQuery(".TOCreateActionButtons").attr('disabled',true);
				}
				else
				{
					searchWHLocation();
					TOStatus();
					disableFieldONStartup();
					enableField();
					enableDisableOnStatus(0);	
				}



			}
			else
			{
				showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
			}

		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}

	});
	function actionButtonsStauts()
	{
		jQuery(".TOCreateActionButtons").each(function(){

			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");

			if(loggedInUserPrivileges[extractedButtonId] == 1)
			{

				//jQuery("#"+buttonid).attr('disabled',false);

			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}

			jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.

		});
	}



	jQuery(function() {

		jQuery( "#divTransferInTab" ).tabs(); // Create tabs using jquery ui.
	});

	jQuery(".TOSearchInput").keydown(function(e){

		if(e.which == 115) 
		{
			var currentTOISearchId = this.id;

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:"TOLookUp",
				},
				success:function(data)
				{

					//alert(data);
					jQuery("#divLookUp").html(data);
					TOSearchGrid2(currentTOISearchId);
					jQuery("#divLookUp" ).dialog( "open" );

					TOLookUpData2();

					/*----------Dialog for item search is there ----------------*/


				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});
		}

		/*jQuery.ajax({
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:lookUpId,
				},
				success:function(data)
				{

					jQuery("#divLookUp").html(data);
					jQuery( "#divLookUp" ).dialog( "open" );
				}

			});*/

	});


	var today = new Date();
	jQuery( "#TIReceiveDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy', yearRange: '1950:2010',minDate: 0, maxDate: "0D"});
	jQuery( "#TIReceiveDate" ).datepicker("setDate",new Date());
	searchWHLocation();
	Status();
	disableFieldONStartup();
	enableDisableOnStatus(0);
	function searchWHLocation()
	{

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TICallback',
			data:
			{
				id:"searchWHLocation",
			},
			success:function(data)
			{
				//alert(data);

				var whlocationsData = jQuery.parseJSON(data);

				if(whlocationsData.Status==1){
					jsonForLocations=whlocationsData.Result;
					whlocations=whlocationsData.Result;
					jQuery.each(whlocations,function(key,value)
							{	
						jQuery("#TISourceLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#TIDestinationLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
							});
					jQuery('#TISourceLocation').val(currentLocationId);
					jQuery('#TISourceLocation').attr('disabled',true);
					backGroundColorForDisableField();
				}
				else{
					showMessage('red',whlocationsData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}

	function Status()
	{

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
			data:
			{
				id:"TIStatus",
			},
			success:function(data)
			{

				var toiData = jQuery.parseJSON(data);
				if(toiData.Status==1){
					toi=toiData.Result;
					jQuery.each(toi,function(key,value)
							{	

						jQuery("#TIStatus").append("<option id=\""+toi[key]['keycode1']+"\""+" value=\""+toi[key]['keycode1']+"\""+">"+toi[key]['keyvalue1']+"</option>");
							});
				}
				else{
					showMessage('red',toiData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}


	jQuery("#btnTSF06Search").click(function(){
		showMessage("loading", "Searching available transfer Ins...", "");
		jQuery("#fromTIShipDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#toTIShipDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#RecFromDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#recToDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#TISourceLocation").attr('disabled',false);
		var searchFormData = jQuery("#formSearchTI").serialize();
		jQuery("#TISourceLocation").attr('disabled',false);
			 jQuery("#fromTIShipDate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery("#toTIShipDate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery("#RecFromDate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery("#recToDate").datepicker("option", "dateFormat", "d-M-yy");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
			data:
			{
				functionName:"Search",
				moduleCode:"TSF06",
				id:"searchTI",
				formSearchTI:searchFormData,
			},
			success:function(data)
			{
				/*jQuery("#loadingImage").hide();*/
				jQuery(".ajaxLoading").hide();
				var rowid=0;
				var searchTOData = jQuery.parseJSON(data);
				if(searchTOData.Status==1){
					searchTO=searchTOData.Result;

					if(searchTO.length==0)
					{
						jQuery("#TISearchGrid").jqGrid("clearGridData");
						showMessage("","No Record Found","");
					}
					else
					{	

						jQuery("#TISearchGrid").jqGrid("clearGridData");
						jQuery.each(searchTO,function(key,value){			
							var newData = [{
								"CSNumber":searchTO[key]['CSNumber'],
								"TONumber":searchTO[key]['TONumber'],
								"TOINumber":searchTO[key]['TOINumber'],
								"TOCreationDate":searchTO[key]['TOShippingDate'],
								"TIShippingDate":searchTO[key]['TOShippingDate'],
								"ReceiveDate":searchTO[key]['ReceivedDate'],
								"SourceAddress":searchTO[key]['SourceAddress'],
								"DestinationAddress":searchTO[key]['DestinationAddress'],
								
								"TotalTOQuantity":parseInt(parseFloat(searchTO[key]['TotalTOQuantity'],10).toFixed(2)),
								"TotalTOAmount":parseFloat(searchTO[key]['TotalTOAmount'],10).toFixed(2),
								"TotalTIQuantity":parseInt(parseFloat(searchTO[key]['TotalTIQuantity'],10).toFixed(2)),
								"TotalTIAmount":parseFloat(searchTO[key]['TotalTIAmount'],10).toFixed(2),
								"Status":searchTO[key]['StatusName'],
								"StatusId":searchTO[key]['Status'],
								"ModifiedDate":searchTO[key]['ModifiedDate'],
								"SourceLocationId":searchTO[key]['SourceLocationId'],
								"PackSize":searchTO[key]['PackSize'],
								"ShippingDetails":searchTO[key]['ShippingDetails'],
								"DestinationLocationId":searchTO[key]['DestinationLocationId'],
								"ShippingWayBillNo":searchTO[key]['ShippingWayBillNo'],
								"ReceivedTime":searchTO[key]['ReceivedTime'],
								"GrossWeight":searchTO[key]['GrossWeight'],
							}];

							for (var i=0;i<newData.length;i++) {
								jQuery("#TISearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
								rowid=rowid+1;
							}
						});
					}

				}
				else{
					showMessage('',searchTOData.Description,'');
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	});
	jQuery("#TICreateTONumber").keydown(function(e){

		if(e.which==9 || e.which == 13){
			e.preventDefault(); 
			var TONumber = jQuery("#TICreateTONumber").val();

			searchItemUsingCode(TONumber);   

		}
		if(e.which==115){
			var currentTOISearchId = this.id;

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:"TOLookUp",
				},
				success:function(data)
				{

					//alert(data);
					jQuery("#divLookUp").html(data);



					TOSearchGrid2(currentTOISearchId);
					showCalender();
					jQuery("#divLookUp" ).dialog( "open" );

					TOLookUpData2();

					/*----------Dialog for item search is there ----------------*/


				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});
		}

	});
	function 	searchItemUsingCode (TONumber){
		var statusOfTOI =-1;
		showMessage("loading", "Searching transfer out number...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
			data:
			{
				id:"searchTO",
				TONumber:TONumber

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var keypairData = jQuery.parseJSON(data);
				var sourceId=-1;
				var destinationId=-1;

				if(keypairData.Status==1){
					keypair=keypairData.Result;
					if(keypair.length>0){
						jQuery.each(keypair,function(key,value)
								{TISourceLocation
							jQuery("#TICreateTINumber").css("background","white");
							sourceId=keypair[key]['SourceLocationId'] ;
							destinationId=keypair[key]['DestinationLocationId'];
							jQuery("#TICreateTISourceLocation").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',keypair[key]['SourceLocationId'],'DisplayName'));
							jQuery("#TICreateTIDestinationLocation").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',keypair[key]['DestinationLocationId'],'DisplayName'));    
							jQuery("#TICreateSourceAddress").val(keypair[key]['SourceAddress']);
							jQuery("#TICreateDestinationAddress").val(keypair[key]['DestinationAddress']);
							jQuery("#TICreateTotalTOQuantity").val(parseFloat(keypair[key]['TotalTOQuantity'],10).toFixed(0));
							jQuery("#TICreateGrossWeight").val(parseFloat(keypair[key]['GrossWeight'],10).toFixed(2));
							jQuery("#TICreateTotalTOAmount").val(parseFloat(keypair[key]['TotalTOAmount'],10).toFixed(2));
							jQuery("#TICreateShippingDetails").val(keypair[key]['ShippingDetails']);
							jQuery("#TICreateShippingWayBillNo").val(keypair[key]['ShippingWayBillNo']);
							jQuery("#TICreateRemarks").val(keypair[key]['Remarks']);
							jQuery("#TICreatePackSize").val(keypair[key]['PackSize']);
							jQuery("#TICreateStatus").val('');
							statusOfTOI=	 keypair[key]['Status'];
								});
						jQuery("#TICreateTONumber").css("background","white");

						if(statusOfTOI==2){
							ShowControlTaxInfor(sourceId,'s');
							ShowControlTaxInfor(destinationId,'d');
							adjustItemAndBatchForModified(TONumber);
						}
						else  if(statusOfTOI==1){
							showMessage ("","No Claims Allowed. Transfer Out Not Confirmed.","");	
						}
						else  if(statusOfTOI==3){
							showMessage ("","No Claims Allowed. Transfer In Completed.","");
						}
						
					}
					else {
						jQuery("#TICreateTONumber").css("background","#FF9999");

					}
				}
				else{
					showMessage('red',keypairData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});	

	}
	function ShowControlTaxInfor(locationId,locationType){
		showMessage("loading", "Searching for taxes...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TICallback',
			data:
			{
				id:"ShowControlTaxInfor",
				locationId:locationId

			},
			success:function(data)
			{

				jQuery(".ajaxLoading").hide();
				var keypairData = jQuery.parseJSON(data);

				if(keypairData.Status==1){
					keypair=keypairData.Result;
					if(keypair.length>0){
						jQuery.each(keypair,function(key,value)
								{
							if(locationType=='s'){
								jQuery("#TICreateSourceCSTNo").val(keypair[key]['cstNO']);
								jQuery("#TICreateSourceVATNo").val(keypair[key]['VatNo']);
								jQuery("#TICreateSourceTINNo").val(keypair[key]['tinNo']);

							}
							else {
								jQuery("#TICreateDestinationCSTNo").val(keypair[key]['cstNO']);
								jQuery("#TICreateDestinationVATNo").val(keypair[key]['VatNo']);
								jQuery("#TICreateDestinationTinNo").val(keypair[key]['tinNo']);

							}
								});
					}
				}
				else{
					sowMessage('red',keypairData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});	
	}


	function adjustItemAndBatchForModified(TONumber){
		showMessage("loading", "Searching for item details...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
			type:'POST',
			data:
			{
				id:"adjustItemAndBatch",
				TONumber:TONumber

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();

				var itemsJsonData = jQuery.parseJSON(data);
				if(itemsJsonData.Status==1){

					itemsJson=itemsJsonData.Result;
					jQuery("#TICreateGrid").jqGrid("clearGridData");
					var rowId=0;
					if(itemsJson.length>0){
						jQuery.each(itemsJson,function(key,value)
								{

							var ItemCode = itemsJson[key]['ItemCode']	;
							var ItemId=itemsJson[key]['ItemId']	;
							var ItemDescription = itemsJson[key]['ItemDescription'];    
							var UOMId = itemsJson[key]['UOMId']  ;
							var UOMName = itemsJson[key]['UOMName']  ;
							var UnitPrice =parseFloat( itemsJson[key]['TransferPrice'],10).toFixed(2);    
							var AvailableQty =parseFloat( itemsJson[key]['AvailableQty'],10).toFixed(2);         
							var TotalAmount =parseFloat( itemsJson[key]['TotalAmount'],10).toFixed(2); 
							var TOINumber =itemsJson[key]['TOINumber'] ; 
							var rowNo =itemsJson[key]['RowNo'] ;
							var RequestQty=itemsJson[key]['AfterAdjustQty'] ;
							var BatchNo =itemsJson[key]['BatchNo'] ; 
							var ManufactureBatchNo =itemsJson[key]['ManufactureBatchNo'] ;
							var MRP=itemsJson[key]['MRP'] ;
							var Weight =itemsJson[key]['Weight'] ;
							var ExpDuration=itemsJson[key]['ExpDuration'] ;
							var MerchHierarchyDetailId=itemsJson[key]['MerchHierarchyDetailId'] ;
							jQuery("#TICreateTINumber").val(itemsJson[key]['CSNumber']);
							var newData = [{"Items":ItemCode,"ItemId":itemsJson[key]['ItemId'],"ItemName":ItemDescription,"UnitPrice":UnitPrice,
								"BucketName":'',"MfgBatchNo":ManufactureBatchNo,
								"ClaimQty":"<input type='text'  name='claimQty' style='width:100px;' value="
									+parseInt(parseFloat(itemsJson[key]['ClaimQty'],10).toFixed(2))+" class='CLaimQuantity' id="
									+ItemCode+ManufactureBatchNo+">","TotalAmount":TotalAmount
									,"BatchNo":BatchNo,"TOINumber":TOINumber,
									"rowNo":rowNo,"RequestQty":parseInt(RequestQty),"TOINo":TOINumber,"ManufactureBatchNo":ManufactureBatchNo,"MRP":MRP,
									"Weight":Weight,
									"ExpDuration":ExpDuration,"MerchHierarchyDetailId":MerchHierarchyDetailId,
									"MfgDate":itemsJson[key]['MfgDate'],"ExpDate":itemsJson[key]['ExpDate'],
									"BucketId":itemsJson[key]['BucketId'],"UOMId":itemsJson[key]['UOMId']
							}];
							for (var i=0;i<newData.length;i++) {
								jQuery("#TICreateGrid").jqGrid('addRowData',rowId, newData[newData.length-i-1], "first");
								rowId=rowId+1;
							}

							enableDisableOnStatus(itemsJson[key]['Status']);
								});
												jQuery(".CLaimQuantity").focusout(function(){
													var itemInformation = jQuery("#TICreateGrid").jqGrid('getRowData');
													jQuery(".CLaimQuantity").each(function(){
													var claimCellid = this.id;
													
													var claimQty = jQuery("#"+claimCellid).val();
													var intRegex = /^\d+$/;
													jQuery.each(itemInformation,function(key,value){
						
														if(claimCellid == itemInformation[key]['Items']+itemInformation[key]['ManufactureBatchNo'])
														{
															if(parseInt(parseFloat(itemInformation[key]['RequestQty'],10).toFixed(2)) < parseInt(parseFloat(claimQty,10).toFixed(2))){
																showMessage('','Claim Quanity cannot be more than TO Quantity','');
																jQuery("#"+claimCellid).val('');
															}
															
						
															if(  !intRegex.test(claimQty)){
																jQuery("#"+claimCellid).val('');
																showMessage("","Enter Valid Claim Quantity.","");
															}
													
														}
														else
														{
						
														}
													});
							});


						});


						
					}
					else
					{
						showMessage("","Total quantity in TOI item(s) is greater than item(s) quantity at current location.","");
					}
				}
				else{
					showMessage('red',itemsJsonData.Description,'');
				}	
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}


		});	
	}




	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * TOI Create Grid 
	 */
	/* var myEditOptions = {
    		  keys: true,
	            oneditfunc: function (rowid) {
	               // alert("row with rowid=" + rowid + " is editing.");
	            },
	            aftersavefunc: function (rowid, response, options) {RequestQty
	            	var ClaimQty = jQuery('#TICreateGrid').jqGrid('getCell', rowid, 'ClaimQty');
	            var RequestQty = jQuery('#TICreateGrid').jqGrid('getCell', rowid, 'RequestQty');

	            	if(RequestQty<ClaimQty){

				            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('setCell', rowid, 'ClaimQty',0);
				            	showMessage('red','Claim Qty not  greater then TO qty','');

	            	}

	            }
      };*/

	function getJsonFromCreateGrid (){
		var itemInformation = jQuery("#TICreateGrid").jqGrid('getRowData');

		var sItemInformation = JSON.stringify(itemInformation);

		jQuery(".CLaimQuantity").each(function(){

			var claimCellid = this.id;
			var claimQty = jQuery("#"+claimCellid).val();



			jQuery.each(itemInformation,function(key,value){

				if(claimCellid == itemInformation[key]['Items']+itemInformation[key]['ManufactureBatchNo'])
				{
					itemInformation[key]['ClaimQty'] = claimQty;
				}
				else
				{

				}

			});


		});
		return itemInformation;
	}
	
		
	
	jQuery("#TICreateGrid").jqGrid({
		datatype: 'jsonstring',
		colNames: ['Items','ItemId','Item Description','Unit Price','Mfg Batch No','Transfer Out Qty'
		           ,'Claim Qty','Total Amount','BatchNo'
		           ,'rowNo','Bucket Name','TOINo','ManufactureBatchNo','MRP',
		           'Weight','ExpDuration','MerchHierarchyDetailId','MfgDate','ExpDate',
		           'BucketId','UOMId'],
		           colModel: [{ name: 'Items', index: 'Items', width:100 },
		                      { name: 'ItemId', index: 'ItemId', width: 70, hidden:true},
		                      { name: 'ItemName', index: 'ItemName', width:150 },
		                      { name: 'UnitPrice', index: 'DistributorId', width: 100},

		                      { name: 'MfgBatchNo', index: 'DiscountPercent', width: 100},
		                      { name: 'RequestQty', index: 'RequestQty', width: 70},
		                      { name: 'ClaimQty', index: 'ClaimQty', width: 100
		                      },
		                      { name: 'TotalAmount', index: 'PromotionId', width: 70,hidden:true},
		                      { name: 'BatchNo', index: 'PromoDescription', width: 100,hidden:true},
		                      { name: 'rowNo', index: 'rowNo', width: 70, hidden:true},
		                      { name: 'BucketName', index: 'FirstName', width: 100, hidden:true},
		                      { name: 'TOINo', index: 'TOINo', width: 70, hidden:true},
		                      { name: 'ManufactureBatchNo', index: 'ManufactureBatchNo', width: 70, hidden:true},
		                      { name: 'MRP', index: 'MRP', width: 70, hidden:true},
		                      { name: 'Weight', index: 'Weight', width: 70, hidden:true},
		                      { name: 'ExpDuration', index: 'ExpDuration', width: 70, hidden:true},
		                      { name: 'MerchHierarchyDetailId', index: 'MerchHierarchyDetailId', width: 70, hidden:true},
		                      { name: 'MfgDate', index: 'MfgDate', width: 70, hidden:true},
		                      { name: 'ExpDate', index: 'ExpDate', width: 70, hidden:true},
		                      { name: 'BucketId', index: 'BucketId', width: 70, hidden:true},
		                      { name: 'UOMId', index: 'UOMId', width: 70, hidden:true}
		                      ],
		                      //  pager: jQuery('#PJmap_TICreateGrid'),
		                      width:1040,
		                      height:250,
		                      rowNum: 20,
		                      rowList: [500],
		                      sortname: 'Label',
		                      sortorder: "asc",
		                      hoverrows: true,
		                      editurl:'clientArray',


		                      afterInsertRow : function(ids)
		                      {

		                    	  jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

		                      },

		                      loadComplete: function() {

		                      }	

	});

	//jQuery("#TICreateGrid").jqGrid('navGrid','#PJmap_TICreateGrid',{edit:false,add:false,del:false});	
	//jQuery("#PJmap_TICreateGrid").hide();


	/*jQuery("#TICreateGrid").jqGrid('navGrid','#PJmap_TICreateGrid',{add:false});

		jQuery("#TICreateGrid").jqGrid('inlineNav',"#PJmap_TICreateGrid",{

		    editParams: myEditOptions,

		    delbutton: true,
            editbutton: false,
		    editurl:'clientArray'
		});
		jQuery("#edit_TICreateGrid").hide();
		jQuery("#refresh_TICreateGrid").hide();
		jQuery("#del_TICreateGrid").hide();
		jQuery("#TICreateGrid_iladd").hide();
		jQuery("#search_TICreateGrid").hide();
	 */
	/*------------------------------add calender to fields --------------------------------*/



	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * TOI Search Grid 
	 */
	jQuery("#TISearchGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['CS Number','TO Number','TOI Number','Source Address','Destination Address','TO Creation Date',
		           'TI Shipping Date','Receive Date','Total TO Quantity','Total TO Amount','TotalTIQuantity','TotalTIAmount','Status','StatusId',
		           'ModifiedDate',
		           'SourceLocationId','PackSize','Remarks','ShippingDetails','DestinationLocationId',
		           'ShippingWayBillNo','ReceivedTime','GrossWeight',
		           ],
		           colModel: [
		                      { name: 'CSNumber', index: 'TONumber', width: 100},
		                      { name: 'TONumber', index: 'TONumber', width: 100},
		                      { name: 'TOINumber', index: 'TOINumber', width: 100},
		                      { name: 'SourceAddress', index: 'SourceAddress', width: 250},
		                      { name: 'DestinationAddress', index: 'DestinationAddress', width: 250},
		                      { name: 'TOCreationDate', index: 'DiscountAmount', width: 100, hidden:true},
		                      { name: 'TIShippingDate', index: 'TOIDate', width: 100, hidden:true},
		                      { name: 'ReceiveDate', index: 'ReceiveDate', width: 100},
		                      { name: 'TotalTOQuantity', index: 'TotalTOIQuantity', width: 100, hidden:true},
		                      { name: 'TotalTOAmount', index: 'TotalTOIAmount', width: 100, hidden:true},
		                      { name: 'TotalTIQuantity', index: 'TotalTIQuantity', width: 100, hidden:true},
		                      { name: 'TotalTIAmount', index: 'TotalTIAmount', width: 100, hidden:true},					       
		                      { name: 'Status', index: 'Status', width: 70},
		                      { name: 'StatusId', index: 'Status', width: 70, hidden:true},
		                      { name: 'ModifiedDate', index: 'ModifiedDate', width: 70, hidden:true},
		                      { name: 'SourceLocationId', index: 'SourceLocationId', width: 70, hidden:true},
		                      { name: 'PackSize', index: 'PackSize', width: 70, hidden:true},
		                      { name: 'Remarks', index: 'Remarks', width: 70, hidden:true},
		                      { name: 'ShippingDetails', index: 'ShippingDetails', width: 70, hidden:true},
		                      { name: 'DestinationLocationId', index: 'DestinationLocationId', width: 70, hidden:true},

		                      { name: 'ShippingWayBillNo', index: 'ShippingWayBillNo', width: 70, hidden:true},
		                      { name: 'ReceivedTime', index: 'ReceivedTime', width: 70, hidden:true},
		                      { name: 'GrossWeight', index: 'GrossWeight', width: 70, hidden:true}
		                      ],

		                      pager: jQuery('#PJmap_TOSearchGrid'),
		                      width:1040,
		                      height:500,
		                      rowNum: 20,
		                      rowList: [500],
		                      sortname: 'Label',
		                      sortorder: "asc",
		                      hoverrows: true,
		                      autowidth:true,
		                      scrollable:true,
		                      shrinkToFit:false,
		                      autoheight:true,


		                      afterInsertRow : function(ids)
		                      {

		                    	  jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//		                    	  jQuery("tr.jqgrow").css("background", "#DDDDDC");
		                      },
		                      ondblClickRow: function(rowId) {


		                    	  jQuery("#TICreateTISourceLocation").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations, 'LocationId', jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'SourceLocationId'), 'LocationName'));
		                    	  jQuery("#TICreateTIDestinationLocation").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations, 'LocationId', jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'DestinationLocationId'), 'LocationName'));
		                    	  jQuery("#TICreateTONumber").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'TONumber'));
		                    	  jQuery("#TICreateSourceAddress").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'SourceAddress'));

		                    	  jQuery("#TICreateDestinationAddress").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'DestinationAddress'));
		                    	  jQuery("#TICreatePackSize").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'PackSize'));
		                    	  jQuery("#TICreateBranchRemark").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'Remarks'));
		                    	  jQuery("#TICreateStatus").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'Status'));

		                    	  jQuery("#TICreateTINumber").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'TINumber'));
		                    	  sourceId=jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'SourceLocationId');

		                    	  jQuery("#TICreateTotalTOQuantity").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'TotalTIQuantity'));
		                    	  jQuery("#TICreateGrossWeight").val(parseFloat(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'GrossWeight'),10).toFixed(2));
		                    	  jQuery("#TICreateTotalTOAmount").val(parseFloat(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'TotalTOAmount'),10).toFixed(2));
		                    	  jQuery("#TICreateShippingDetails").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'ShippingDetails'));
		                    	  jQuery("#TICreateShippingWayBillNo").val(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'ShippingWayBillNo'));

		                    	  adjustItemAndBatchForModified(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'TONumber'));
		                    	  jQuery("#divTransferInTab").tabs( "select", "DivCreate" );
		                    	  enableDisableOnStatus(jQuery("#TISearchGrid").jqGrid('getCell', rowId, 'StatusId'));



		                      }
		                      //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked
	});

	jQuery("#TISearchGrid").jqGrid('navGrid','#PJmap_TISearchGrid',{edit:false,add:false,del:false});	
	jQuery("#PJmap_TISearchGrid").hide();

	jQuery("#TIReceiveDate").change(function(){

		jQuery("#TIReceiveDate").css("background","white");
	});

	function validationOnSaveButton(status){
		if(status==1){
			if(jQuery("#claimDate").val()==''){
				jQuery("#TIReceiveDate").css("background","#FF9999");
				return 0;
			}
			else{
				jQuery("#TIReceiveDate").css("background","white");
			}


			if(jQuery("#TICreateTONumber").val()==''){
				jQuery("#TICreateTONumber").css("background","#FF9999");
				return 0;
			}
			else{
				jQuery("#TICreateTONumber").css("background","white");
			}

		}
		else if(status==2){
			if(jQuery("#claimRemarks").val()==''){
				jQuery("#claimRemarks").css("background","#FF9999");
				return 0;

			}
			else{
				jQuery("#claimRemarks").css("background","white");
			}
		}
     return 1;
	}

	jQuery("#packUnpackCreatePackUnpackQty").keyup(function(event){
		var intRegex = /^\d+$/;

		if(  !intRegex.test(jQuery("#packUnpackCreatePackUnpackQty").val())  && parseInt(jQuery("#packUnpackCreatePackUnpackQty").val())>0){
			jQuery("#TOCreatePackSize").val('');
			showMessage("","Enter Valid Claim Quantity.","");
		}
		jQuery("#TOCreatePackSize").css("background","white");
	});
	jQuery("#btnTSF06Save").click(function(){
		jQuery("#actionName").val('Save');
		var oOrderJson =getJsonFromCreateGrid ();
		order_json_string = JSON.stringify(oOrderJson);
	
		var TONumber=jQuery("#TICreateTONumber").val();
		var TNumber=jQuery("#TICreateTINumber").val();
		var vallidationStatus=validationOnSaveButton(1);
		if(vallidationStatus==1){
			var r =confirm("Are you sure you want to save data ?");
			if(r==true)
			{
				showMessage("loading", "Saving Claim Details...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
					data:
					{
						id:"saveTI",
						jsonForItems:order_json_string,
						TONumber:TONumber,
						statusId:1,
						TNumber:TNumber,
						Remark:jQuery("#claimRemarks").val(),
						functionName:jQuery("#actionName").val(),
						moduleCode:"TSF06",

					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var keypairData = jQuery.parseJSON(data);
						if(keypairData.Status==1){
							keypair=keypairData.Result;
							if(keypair.length>=0){

								jQuery.each(keypair,function(key,value)
										{	
									showMessage("green","Claim Saved Successfully. "+keypair[key]['CSNumber'],"");
									jQuery("#TICreateTINumber").val(keypair[key]['CSNumber']);
									jQuery("#TICreateStatus").val('Claim Raised');
									enableDisableOnStatus(1);

										});
							}
							else{
								showMessage("red","Claim Not Saved. Error " + keypairData,"");
							}

						}
						else{
							showMessage('red',keypairData.Description,'');
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}

		}

	});

	jQuery("#btnTSF06Confirm").unbind("click").click(function(){
		var oOrderJson =getJsonFromCreateGrid ();
		order_json_string = JSON.stringify(oOrderJson);
		jQuery("#actionName").val('Confirm');
		var TONumber=jQuery("#TICreateTONumber").val();
		var TNumber=jQuery("#TICreateTINumber").val();
		var vallidationStatus=validationOnSaveButton(2);
		if(vallidationStatus==1){
			var r =confirm("Are you sure you want to save data ?");
			if(r==true)
			{
				showMessage("loading", "Saving Claim Settlement Details...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
					data:
					{
						id:"saveTI",
						jsonForItems:order_json_string,
						TONumber:TONumber,
						statusId:2,
						TNumber:TNumber,
						functionName:jQuery("#actionName").val(),
						moduleCode:"TSF06",
						Remark:jQuery("#claimRemarks").val(),
						

					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var keypairData = jQuery.parseJSON(data);
						if(keypairData.Status==1){
							keypair=keypairData.Result;
							if(keypair.length>=0){

								jQuery.each(keypair,function(key,value)
										{	
									showMessage("green","Claim Settlement Saved. "+keypair[key]['CSNumber'],"");
									jQuery("#TICreateTINumber").val(keypair[key]['CSNumber']);
									jQuery("#TICreateStatus").val('Claim Settled');
									enableDisableOnStatus(2);

										});
							}
							else{
								showMessage("red","Claim Not Settled. Error " + keypairData,"");
							}

						}
						else{
							showMessage('red',keypairData.Description,'');
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}

		}

	});




	function disableFieldONStartup(){
		jQuery("#TICreateTISourceLocation").attr('disabled',true);
		jQuery("#TICreateTIDestinationLocation").attr('disabled',true);    
		jQuery("#TICreateSourceAddress").attr('disabled',true);
		jQuery("#TICreateDestinationAddress").attr('disabled',true);
		jQuery("#TICreateTotalTOQuantity").attr('disabled',true);
		jQuery("#TICreateGrossWeight").attr('disabled',true);
		jQuery("#TICreateTotalTOAmount").attr('disabled',true);
		jQuery("#TICreateShippingDetails").attr('disabled',true);
		jQuery("#TICreateShippingWayBillNo").attr('disabled',true);
		jQuery("#TICreateRemarks").attr('disabled',true);
		jQuery("#TICreatePackSize").attr('disabled',true);
		jQuery("#TICreateStatus").attr('disabled',true);
		jQuery("#TICreateSourceCSTNo").attr('disabled',true);
		jQuery("#TICreateSourceVATNo").attr('disabled',true);
		jQuery("#TICreateSourceTINNo").attr('disabled',true);
		jQuery("#TICreateDestinationCSTNo").attr('disabled',true);
		jQuery("#TICreateDestinationVATNo").attr('disabled',true);
		jQuery("#TICreateDestinationTinNo").attr('disabled',true);

		jQuery("#TICreateTINumber").attr('disabled',true);
		jQuery("#claimRemarks").val('');
	}



	function disableButton(){
		jQuery("#btnPrint").attr('disabled',true);
		jQuery("#btnConfirm").attr('disabled',true);
		jQuery("#btnSave").attr('disabled',true);
	}

	function enableField(){
		jQuery("#TOCreateTOINumber").attr('disable',false);
		jQuery("#TOCreatePackSize").attr('disable',false);
		jQuery("#TOCreateRemarks").attr('disable',false);
		jQuery("#TOCreateExpectedDeliveryDate").attr('disable',false);
		jQuery("#TOCreateRefNo").attr('disable',false);
		jQuery("#TOCreateShippingWayBillNo").attr('disable',false);

	}

	function resetAllFields(){
		jQuery("#TICreateTONumber").val('');
		jQuery("#TICreateTISourceLocation").val('');
		jQuery("#TICreateTIDestinationLocation").val('');
		jQuery("#TICreateTINumber").val('');
		jQuery("#TICreateTISourceAddress").val('');
		jQuery("#TICreateDestinationAddress").val('');

		jQuery("#TICreatePackSize").val('');
		jQuery("#TIreateBranchRemark").val('');
		jQuery("#TICreateSourceCSTNo").val('');
		jQuery("#TICreateSourceVATNo").val('');
		jQuery("#TICreateSourceTINNo").val('');
		jQuery("#TICreateDestinationCSTNo").val('');
		jQuery("#TICreateDestinationVATNo").val('');
		jQuery("#TICreateDestinationTinNo").val('');

		jQuery("#TICreateExpectedDeliveryDate").val('');
		jQuery("#TICreateRefNo").val('');

		jQuery("#TICreateShippingWayBillNo").val('');
		jQuery("#TICreateTotalTOQuantity").val('');
		jQuery("#TICreateGrossWeight").val('');
		jQuery("#TICreateTotalTOAmount").val('');
		jQuery("#TICreateShippingDetails").val('');
		jQuery("#TICreateStatus").val('');
		jQuery("#TICreateDestinationCSTNo").val('');
		jQuery("#TICreateIsExported").val('');
		jQuery("#TICreateGrid").jqGrid("clearGridData");

	}

	jQuery("#btnCreateReset").click(function(){
		resetAllFields();
		enableDisableOnStatus(0);
		jQuery("#claimRemarks").val('');
		jQuery("#TICreateSourceAddress").val('');
		jQuery("#TICreateTONumber").css("background","white");
		jQuery("#claimRemarks").css("background","white");
	});




	jQuery("#TOCreateRemarks").change(function(){

		jQuery("#TOCreateRemarks").css("background","white");
	});
	jQuery("#TOCreateRefNo").change(function(){

		jQuery("#TOCreateRefNo").css("background","white");
	});
	jQuery("#TOCreateShippingDetails").change(function(){

		jQuery("#TOCreateShippingDetails").css("background","white");
	});
	jQuery("#TOCreateExpectedDeliveryDate").change(function(){
		jQuery("#TOCreateExpectedDeliveryDate").css("background","white");
	});
	jQuery("#TOCreateShippingWayBillNo").change(function(){
		jQuery("#TOCreateShippingWayBillNo").css("background","white");
	});
	jQuery("#TOCreatePackSize").change(function(){
		jQuery("#TOCreatePackSize").css("background","white");
	});

	/*  ALL BUTTONS       */	

	jQuery("#btnReset").click(function(){
		jQuery("#TIStatus").val(-1);
		jQuery("#TINumber").val('');
		jQuery("#TONumber").val('');
		jQuery("#TISourceLocation").val(-1);
		jQuery("#fromTIShipDate").val('');
		jQuery("#toTIShipDate").val('');
		
		jQuery("#claimRemarks").css("background","white");
		
		showCalender();
		
	});

	function enableDisableOnStatus(status){
		if(status==0){
			jQuery("#btnTSF06Confirm").attr('disabled',true);     /*on startup*/
			jQuery("#btnTSF06Save").attr('disabled',false);
			jQuery("#btnTSF06Print").attr('disabled',true);
		}
		if(status==1){   //raise claim status

			jQuery("#btnTSF06Confirm").attr('disabled',false);
			jQuery("#btnTSF06Save").attr('disabled',true);
			jQuery("#btnTSF06Print").attr('disabled',true);
			jQuery(".CLaimQuantity").attr('disabled',true);
		}
		if(status==2){  //claim settled

			jQuery("#btnTSF06Confirm").attr('disabled',true);
			jQuery("#btnTSF06Save").attr('disabled',true);
			jQuery(".CLaimQuantity").attr('disabled',true);
			jQuery("#btnTSF06Print").attr('disabled',false);
		}
	}
	jQuery("#btnItemReset").click(function(){
		jQuery("#TICreateGrid").jqGrid("clearGridData");
	});
	/*jQuery("#btnPrint").click(function(){
		var arr = {'locationId':10,'CSNumber':jQuery("#TICreateTINumber").val()};
		showReport('http://180.179.67.207/:8080/birt-viewer/frameset?__format=pdf','reports/ClaimInvoice.rptdesign',arr);
	});*/
	jQuery("#btnTSF06Print").unbind("click").click(function(){
		
		jQuery("#actionName").val('Print');
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'ClaimSettlementCallBack',
			data:
			{
				id:"printTI",
				TINumber:jQuery("#TICreateTINumber").val(),
				functionName:jQuery("#actionName").val(),
				moduleCode:"TSF06"
				
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				 jQuery.each(results,function(key,value)
						   {
					 			var arr = {'locationId':results[key]['locationId'],'CSNumber':results[key]['CSNumber']};
					 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
							 
					      });
			}
			else{
				
				showMessage('red',resultsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});

	});


});
