
var rowid = 0;
var historyOrderNo; 
var loggedInUserPrivileges;
var jsonForLocations ;
var expectedDate ;
var distributordata;
var avaliablesheet;
var DISTRIBUTOR_NO_VALIDATION = 0;

jQuery(document).ready(function(){
	jQuery(".breadcrumb").hide();
jQuery("#DistributorVoucherDeatailGrid").jqGrid({
    
    
    datatype: 'jsonstring',
    colNames: ['DistributorID','DistributorName','VoucherNo','CreatedBy','LocationId'
/*		                ,'DeliverToCityName',
               'DeliverToStateName','DeliverToCountryName','DeliverFromCityName','DeliverFromStateName','DeliverFromCountryName','IsPrintAllowed','InvoiceDate','ValidReportPrintDays',
               'OrderMode','UsedForRegistration','TerminalCode','TotalBV','TotalPV','DistributorAddress','OrderType','PCId','PCCode','BOId','DeliverFromAddress1','DelverFromAddress2',
               'DeliverFromAddress3','DeliverFromAddress4','DeliverFromCityId','DeliverFromPincode','DeliverFromStateId','DeliverFromCountryId','DeliverFromTelephone','DeliverFromMobile',
               'DeliverToMobile','TotalUnits','TotalWeight','OrderAmount','DiscountAmount','TaxAmount','PaymentAmount','ChangeAmount','CreatedBy','CreatedByName','CreatedDate',
               'DeliverToAddressLine1','DeliverTo','DeliverToAddressLine2','DeliverToAddressLine3','DeliverToAddressLine4','DeliverToCityId','DeliverToPincode','DeliverToStateId',
               'DeliverToCountryId','DeliverToTelephone'
               ,'LogValue'*/
               ],
    colModel: [

               { name: 'DistributorID', index: 'DistributorID', width:160},
               { name: 'Distributorname', index: 'Distributorname', width:200},
               { name: 'VoucherNo', index: 'VoucherNo', width:180 },
               { name: 'CreatedBy', index: 'CreatedBy', width:180},
               { name: 'LocationId', index: 'LocationId', width:180 },
             /*  { name: 'DeliverToCityName', index: 'DeliverToCityName', width:20 ,hidden:true},
               { name: 'DeliverToStateName', index: 'DeliverToStateName', width:20 ,hidden:true},
               { name: 'DeliverToCountryName', index: 'DeliverToCountryName', width:20 ,hidden:true},
               { name: 'DeliverFromCityName', index: 'DeliverFromCityName', width:20 ,hidden:true},
               { name: 'DeliverFromStateName', index: 'DeliverFromStateName', width:20 ,hidden:true},
               { name: 'DeliverFromCountryName', index: 'DeliverFromCountryName', width:20 ,hidden:true},
               { name: 'IsPrintAllowed', index: 'IsPrintAllowed', width:20 ,hidden:true},
               { name: 'InvoiceDate', index: 'InvoiceDate', width:20 ,hidden:true},
               { name: 'ValidReportPrintDays', index: 'ValidReportPrintDays', width:20 ,hidden:true},
               { name: 'OrderMode', index: 'OrderMode', width:20 ,hidden:true},
               { name: 'UsedForRegistration', index: 'UsedForRegistration', width:20 ,hidden:true},
               { name: 'TerminalCode', index: 'TerminalCode', width:20 ,hidden:true},
               { name: 'TotalBV', index: 'TotalBV', width:20 ,hidden:true},
               { name: 'TotalPV', index: 'TotalPV', width:20 ,hidden:true},
               { name: 'DistributorAddress', index: 'DistributorAddress', width:20 ,hidden:true},          
               { name: 'OrderType', index: 'OrderType', width:20 ,hidden:true},
               { name: 'PCId', index: 'PCId', width:20 ,hidden:true}, 
               { name: 'PCCode', index: 'PCCode', width:20 ,hidden:true},
               { name: 'BOId', index: 'BOId', width:20 ,hidden:true},
               

               { name: 'DeliverFromAddress1', index: 'DeliverFromAddress1', width:20 ,hidden:true},
               { name: 'DelverFromAddress2', index: 'DelverFromAddress2', width:20 ,hidden:true},
               { name: 'DeliverFromAddress3', index: 'DeliverFromAddress3', width:20 ,hidden:true},
               { name: 'DeliverFromAddress4', index: 'DeliverFromAddress4', width:20 ,hidden:true}, 
               { name: 'DeliverFromCityId', index: 'DeliverFromCityId', width:20 ,hidden:true},
               { name: 'DeliverFromPincode', index: 'DeliverFromPincode', width:20 ,hidden:true},
               { name: 'DeliverFromStateId', index: 'DeliverFromStateId', width:20 ,hidden:true},
               { name: 'DeliverFromCountryId', index: 'DeliverFromCountryId', width:20 ,hidden:true},
               { name: 'DeliverFromTelephone', index: 'DeliverFromTelephone', width:20 ,hidden:true},
               { name: 'DeliverFromMobile', index: 'DeliverFromMobile', width:20 ,hidden:true}, 
              
               { name: 'DeliverToMobile', index: 'DeliverToMobile', width:20 ,hidden:true},
               { name: 'TotalUnits', index: 'TotalUnits', width:20 ,hidden:true},
               { name: 'TotalWeight', index: 'TotalWeight', width:20 ,hidden:true},
               { name: 'OrderAmount', index: 'OrderAmount', width:20 ,hidden:true},
               { name: 'DiscountAmount', index: 'DiscountAmount', width:20 ,hidden:true},
               { name: 'TaxAmount', index: 'TaxAmount', width:20 ,hidden:true},
               { name: 'PaymentAmount', index: 'PaymentAmount', width:20 ,hidden:true},
               { name: 'ChangeAmount', index: 'ChangeAmount', width:20 ,hidden:true},
               { name: 'CreatedBy', index: 'CreatedBy', width:20 ,hidden:true},
               { name: 'CreatedByName', index: 'CreatedByName', width:20 ,hidden:true},
               { name: 'CreatedDate', index: 'CreatedDate', width:20 ,hidden:true},
               
               { name: 'DeliverToAddressLine1', index: 'DeliverToAddressLine1', width:20,hidden:true },
               { name: 'DeliverTo', index: 'DeliverTo', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine2', index: 'DeliverToAddressLine2', width:20,hidden:true },
               { name: 'DeliverToAddressLine3', index: 'DeliverToAddressLine3', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine4', index: 'DeliverToAddressLine4', width:20 ,hidden:true},
               { name: 'DeliverToCityId', index: 'DeliverToCityId', width:20 ,hidden:true},
               { name: 'DeliverToPincode', index: 'DeliverToPincode', width:20 ,hidden:true},
               { name: 'DeliverToStateId', index: 'DeliverToStateId', width:20 ,hidden:true},
               { name: 'DeliverToCountryId', index: 'DeliverToCountryId', width:20 ,hidden:true},
               { name: 'DeliverToTelephone', index: 'DeliverToTelephone', width:20 ,hidden:true},
             
               
           
            
        
               { name: 'LogValue', index: 'LogValue', width:20, hidden:true }
*/		              
               ],
                pager: jQuery('#GatewayGridPager'),
               width:926,
               height:300,
               rowNum: 1000,
               rowList: [500],
               sortable: true,
               sortorder: "asc",
               hoverrows: true,
               shrinkToFit: false,
               ondblClickRow: function (id)
               {
            	   dblclkAction(id);
               }
               		


   });
	
	jQuery("#DistributorVoucherDeatailGrid").jqGrid('navGrid','#GatewayGridPager',{edit:false,add:false,del:false}); 
	jQuery("#GatewayGridPager").hide();	
	
	
	
jQuery("#btnTB01Search").unbind("click").click(function(){
	
	
	if(jQuery("#TicketBookingLoc").val() == -1)
	{
	showMessage("Red", "Please select Location", "");
	jQuery("#TicketBookingLoc").focus();
	return;
	}
	
	
	
	jQuery("#actionName").val('');
	
	
	showMessage("loading", "Searching Tickets...", "");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TicketBookingPOSTCallBack',
			data:
			{
				
				id:"searchDisTicketBookingDetail",
				DistributorID:jQuery("#Distributorid").val(),
				moduleCode:"TB01",
				Location:jQuery("#TicketBookingLoc").val(),
				InvoiceNo:jQuery("#InvoiceNo").val(),
			    functionName:"Search",
			},
			success:function(data)
			{

				jQuery(".ajaxLoading").hide();

				var historyOrderData = jQuery.parseJSON(data);
				
				if(historyOrderData.Status == 1)
				{
					
					
					
					var historyrowid=0;
					var searchTO = historyOrderData.Result;
					if(searchTO.length==0)
					{
						jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 	
						showMessage("","No Record Found","");
					}

					else
					{	
						var message = "Total " + searchTO.length + " records found.";
						showMessage("", message,"");
						
						/*['Add To Log','Print','Invoice','Order No','Status','Log No'],*/
						jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 	
						jQuery .each(searchTO,function(key,value){	
							
							jQuery("#AvailableSeats").val(searchTO[key]['AvailSeats']);

							var newData = [{
								"DistributorID":searchTO[key]['DistributorId'],"Distributorname":searchTO[key]['DistributorName'],"VoucherNo":searchTO[key]['VoucherNo'],"CreatedBy":searchTO[key]['CreatedBy'],"Created":searchTO[key]['createddate'],"LocationId":searchTO[key]['Location'],"PaymantMode":searchTO[key]['paymentmode']
														/*		"DeliverToCityName":searchTO[key]['DeliverToCityName'],"DeliverToStateName":searchTO[key]['DeliverToStateName'],"DeliverToCountryName":searchTO[key]['DeliverToCountryName'],
								"DeliverFromCityName":searchTO[key]['DeliverFromCityName'],"DeliverFromCountryName":searchTO[key]['DeliverFromCountryName'],"IsPrintAllowed":searchTO[key]['IsPrintAllowed'],
								"InvoiceDate":searchTO[key]['InvoiceDate'],"ValidReportPrintDays":searchTO[key]['ValidReportPrintDays'],"OrderMode":searchTO[key]['OrderMode'],"UsedForRegistration":searchTO[key]['UsedForRegistration'],"TerminalCode":searchTO[key]['TerminalCode'],
								"TotalBV":searchTO[key]['TotalBV'],"TotalPV":searchTO[key]['TotalPV'],
								"DistributorAddress":searchTO[key]['DistributorAddress'],"OrderType":searchTO[key]['OrderType'],"PCId":searchTO[key]['PCId'],"PCCode":searchTO[key]['PCCode'],"BOId":searchTO[key]['BOId'],"DeliverFromAddress1":searchTO[key]['DeliverFromAddress1'],"DelverFromAddress2":searchTO[key]['DelverFromAddress2'],
								"DeliverFromAddress3":searchTO[key]['DeliverFromAddress3'],"DeliverFromAddress4":searchTO[key]['DeliverFromAddress4'],"DeliverFromCityId":searchTO[key]['DeliverFromCityId'],"DeliverFromPincode":searchTO[key]['DeliverFromPincode'],
								"DeliverFromStateId":searchTO[key]['DeliverFromStateId'],"DeliverFromCountryId":searchTO[key]['DeliverFromCountryId'],"DeliverFromTelephone":searchTO[key]['DeliverFromTelephone'],"DeliverFromMobile":searchTO[key]['DeliverFromMobile'],
								"DeliverToMobile":searchTO[key]['DeliverToMobile'],"TotalUnits":searchTO[key]['TotalUnits'],"TotalWeight":searchTO[key]['TotalWeight'],
								"OrderAmount":searchTO[key]['OrderAmount'],"DiscountAmount":searchTO[key]['DiscountAmount'],"TaxAmount":searchTO[key]['TaxAmount'],
								"PaymentAmount":searchTO[key]['PaymentAmount'],"ChangeAmount":searchTO[key]['ChangeAmount'],"CreatedBy":searchTO[key]['CreatedBy'],"CreatedByName":searchTO[key]['CreatedByName'],
								"CreatedDate":searchTO[key]['CreatedDate'],"DeliverToAddressLine1":searchTO[key]['DeliverToAddressLine1'],"DeliverTo":searchTO[key]['DeliverTo'],"DeliverToAddressLine2":searchTO[key]['DeliverToAddressLine2'],
								"DeliverToAddressLine3":searchTO[key]['DeliverToAddressLine3'],"DeliverToAddressLine4":searchTO[key]['DeliverToAddressLine4'],"DeliverToCityId":searchTO[key]['DeliverToCityId'],"DeliverToPincode":searchTO[key]['DeliverToPincode'],
								"DeliverToStateId":searchTO[key]['DeliverToStateId'],"DeliverToCountryId":searchTO[key]['DeliverToCountryId'],"DeliverToTelephone":searchTO[key]['DeliverToTelephone'],
								"LogValue":searchTO[key]['LogValue'],*/

							}];

							for (var i=0;i<newData.length;i++) {
								jQuery("#DistributorVoucherDeatailGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
								historyrowid=historyrowid+1;
							}
						});

					}
				}
				else
				{
					showMessage("red", historyOrderData.Description, "");
				}
			
			},// end success.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
		
	
	
});


showCalender();
jQuery("#btnReset").click(function(){ 
	 jQuery("#InvoiceNo").attr('disabled',false);
	 jQuery("#InvoiceNo").val('');
     jQuery("#Distributorname").val('');
		jQuery("#Distributorid").val('');
		 jQuery("#Distributorid").attr('disabled',true);
		jQuery("#AvailableSeats").val('');
		jQuery("#TicketBookingLoc").val('-1');
		
       		jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 
       		 DISTRIBUTOR_NO_VALIDATION = 0;
});



jQuery("#btnTB01Save").unbind("click").click(function(){
	
	jQuery("#actionName").val('');
	
	
	
	if(DISTRIBUTOR_NO_VALIDATION == 1)
	{
		
		if(jQuery("#TicketBookingLoc").val() == -1)
		{
		showMessage("Red", "Please select location", "");
		jQuery("#TicketBookingLoc").focus();
		return;
		}
		
		
		
		jQuery('#btnTB01Save').prop('disabled', true);
	
		
		jQuery("#PaymentStatus").attr('disabled',true);
   		jQuery("#Amount").attr('disabled',true);
	showMessage("loading", "Save Voucher Detail...", "");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TicketBookingPOSTCallBack',
			data:
			{
				
				id:"saveticketbooking",
				DistributorID:jQuery("#Distributorid").val(),
				moduleCode:"TB01",
				InvoiceNo:jQuery("#InvoiceNo").val(),
				Locationid:jQuery("#TicketBookingLoc").val(),
			    functionName:"Save",
			},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				
				jQuery("#btnTB01Save").prop('disabled', false);
				jQuery("#InvoiceNo").attr('disabled',false);
				 jQuery("#Distributorid").val('');
			     jQuery("#Distributorname").val('');
			     jQuery("#InvoiceNo").val('');
					jQuery("#AvailableSeats").val('');
					jQuery("#TicketBookingLoc").val('-1');
			       		jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 
			       		jQuery("#Distributorid").attr('disabled',false);
				
				var keypair=resultsData.Result;
				if(keypair.length>=0){

					jQuery.each(keypair,function(key,value)
							{
						
					if(value['SeqNo'] != ''  || value['SeqNo'] != null)
						showMessage("green","Created Successfully Ticket No "+value['SeqNo'],"");
							
							});
				}

					 
				}
		
						
					

			else{
				jQuery("#InvoiceNo").attr('disabled',false);
				 jQuery("#InvoiceNo").val('');
			     jQuery("#Distributorname").val('');
					jQuery("#Distributorid").val('');
					jQuery("#AvailableSeats").val('');
					jQuery("#TicketBookingLoc").val('-1');
				jQuery("#btnTB01Save").prop('disabled', false);
				showMessage('red',resultsData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
		
	});
	}
	else
		{
			showMessage("", "Please Validate Invoice No ", "");
		}
});





jQuery("#InvoiceNo").attr('maxlength','20');

jQuery("#InvoiceNo").keydown(function(e){

	if(e.which == 13  || e.which == 9)
	{
		
		

		if(jQuery("#InvoiceNo").val()==''){
			
			jQuery("#InvoiceNo").css("background", "#FF9999");
			
			showMessage("","Enter valid Invoice NO.","");
			jQuery("#InvoiceNo").focus();
		}
		
		else
		{
			var invoiceno = jQuery("#InvoiceNo").val(); //S - search distributor id field.
			
			if(invoiceno.length !=17)
			{
				jQuery("#InvoiceNo").css("background", "#FF9999");
				
				showMessage("", "Enter valid Invoice NO", "");
				jQuery("#InvoiceNo").focus();
			}
			else
			{
				checkValidDistributor(invoiceno); //function to validate distributor id.
			}
		}
		
		
		
	}
});
function checkValidDistributor(invoiceno)
{
	if(invoiceno == '')
	{
		jQuery("#InvoiceNo").css("background", "#FF9999");
		
		showMessage("", "Enter invoice NO ", "");
		jQuery("#InvoiceNo").focus();
		
		emptyFieldsData();
	}
	else
	{
		showMessage("loading", "Validating invoice details...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TicketBookingPOSTCallBack',
			data:
			{
				id:"checkinvoice",
				distributorId:invoiceno,

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var returnedDistibutorData = jQuery.parseJSON(data);
				
				if(returnedDistibutorData.Status == 1)
				{
					var results = returnedDistibutorData.Result;
					distributordata=returnedDistibutorData.Result;
					
					jQuery.each(distributordata,function(key,value){
						

						jQuery("#Distributorname").val( distributordata[key]['Distributorname']);
						jQuery("#Distributorid").val( distributordata[key]['DistributorId']);
						

					});
					
					if(results.length ==1)  //means not any distributor registered yetn on this id.
					{
						jQuery("#InvoiceNo").attr('disabled',true);
						jQuery("#Distributorid").attr('disabled',true);
						jQuery("#Distributorid").css("background", "white");
						
						jQuery("#distributorname").val(results.distributorname);
						
						
						
						
						//validatedDistributorId.validatedUplineDistributorId = 1;
						 DISTRIBUTOR_NO_VALIDATION =1;
						
						
						
						//emptyFieldsData();
					}
					
					
				}
				else
				{
					showMessage("red", returnedDistibutorData.Description, "");
				}
				
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
}




jQuery("#btnTB01Print").unbind("click").click(function(){
	
	batchRow= jQuery("#DistributorVoucherDeatailGrid").getGridParam('selrow');
	
	if(batchRow!= null){
		jQuery("#actionName").val('Print');
			var selRowId=jQuery("#DistributorVoucherDeatailGrid").jqGrid('getRowData',batchRow);
		
		
		var Distributorid=selRowId.DistributorID;
		var Voucherno=selRowId.VoucherNo;
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TicketBookingPOSTCallBack',
			data:
			{
				id:"printsheet",
				DistributoriD:Distributorid,
				VoucherNO:Voucherno,
				functionName:"Print",
				moduleCode:"TB01",
				
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			var printout = jQuery.parseJSON(data);
			if(printout.Status==1){
						var results=printout.Result;
						 jQuery.each(results,function(key,value)
								   {
							 			var arr = {'distributorid':results[key]['distributorid'],'voucherNo':results[key]['Voucherno']};
							 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
									 
							      });
					}
					else{
						
						showMessage('red',printout.Description,'');
					}

			
			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
	
});
		
	}
	else {
		showMessage("red","Please select one row From list" ,"");
	}
});

 TicketBookingLocationOnSearchTab();

function TicketBookingLocationOnSearchTab()
{
	showMessage("loading", "Loading data...", "");
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'TicketBookingPOSTCallBack',
		data:
		{
			id:"TicketBookingLoc",
		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var TicketBookingLocationData = jQuery.parseJSON(data);
			
			if(TicketBookingLocationData.Status == 1)
			{
				jsonForLocations=data;
				var TicketBookinglocations = TicketBookingLocationData.Result;
				TicketBookingLocation =  TicketBookingLocationData.Result;
				
				var options = '';
				options += '<option value=-1 selected="selected">Select</option>';
				jQuery.each(TicketBookinglocations,function(index, obj){
					
						options += '<option value="' + obj['LocationId'] + '">'
						 + obj['DisplayName'] + '</option>';
					
				});
				jQuery('#TicketBookingLoc').html(options);
				
			
			}
			else
			{
				showMessage("red", TicketBookingLocationData.Description, '');
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
}

jQuery("#TicketBookingLoc").change(function(){
	
	if(jQuery("#TicketBookingLoc").val() == -1){
		jQuery("#AvailableSeats").val('');

		
		
	}
	
	else{
		var multipleValues = jQuery("#TicketBookingLoc" ).val();
        
	

		getlocationshet(multipleValues);
		
	}
	
	function getlocationshet(multipleValues){
		jQuery.each(avaliablesheet,function(index, obj){
			if(multipleValues == obj['LocationId']){
				
				jQuery("#AvailableSeats").val(obj['Available_sheet']);
			}
		});
	}

});

sheet();
function sheet() {
	jQuery.ajax({
		type : 'POST',
		url : Drupal.settings.basePath + 'TicketBookingPOSTCallBack',
		data : {
			id : "searchticavb",
			
			
		},
		success : function(data) {
			var sheet = jQuery.parseJSON(data);
			if (sheet.Status == 1) {
				avaliablesheet = sheet.Result;
			} else {
				showMessage("red", avaliablesheet.Description, "");
				hideMessageAfterTime(2000);
			}
		}
	});
}






});
