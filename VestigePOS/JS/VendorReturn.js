var  jsonForLocations='';
var jsonForGRNSearch='';
var todayDate='';
var loggedInUserPrivileges;
var classId='cvteste';
jQuery(document).ready(function(){

	
	
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	});
	
	
	getCurrentLocation();
	var moduleCode = jQuery("#moduleCode").val();
	 todayDate=jQuery("#todayDate").val();
	 
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
			data:
			{
				id:"StockCountModuleFuncHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

				if(loggedInUserPrivilegesForStockCountData.Status == 1)
				{

					loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

					if(loggedInUserPrivileges.length == 0)
					{
						jQuery(".GRNCreateActionButtons").attr('disabled',true);
					}
					else
					{
						disableFieldOnStartup();
						 enableFieldOnStartup();
						VRSearchStatus();
						searchWHLocation();
						vendoreCode();
						showCalender();
						showCalender2();
						showCalender3();
						disableButtons(0);
					}


				}
				else
				{
					showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});

		function showCalender3() {
			jQuery( "#VRShipDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+20'});
			jQuery( "#VRShipDate" ).datepicker("setDate",new Date());
		}

		
		function diffrenInDays(DateValue1, DateValue2)
		 {

		 var DaysDiff;
		 Date1 = new Date(DateValue1);
		 Date2 = new Date(DateValue2);
		 DaysDiff = Math.floor((Date1.getTime() - Date2.getTime())/(1000*60*60*24));
            return DaysDiff ;
		 }
		 
		/**
		 * Purchase order look up screen.
		 */
		jQuery("#VrCreateGRNNumber").keydown(function(e){
			if(e.which == 115) 
			{
				var currentSearchId = this.id;
				
				showMessage("loading", "Please wait...", "");

				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"GRNLookUpForVR",
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();

						//alert(data);
						jQuery("#divLookUp").html(data);

						GRNSearchLookupGridForVR(currentSearchId);
						jQuery("#divLookUp" ).dialog( "open",'title','GRN Search');
						
						GRNLookUpDataForVR();
						GRNLookUpGridDataPopulationForVR();

						/*----------Dialog for item search is there ----------------*/


					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}

				});



			}

		});
		
		function actionButtonsStauts()
		{
			jQuery(".GRNCreateActionButtons").each(function(){

				var buttonid = this.id;
				var extractedButtonId = buttonid.replace("btn","");

				if(loggedInUserPrivileges[extractedButtonId] == 1)
				{

					//jQuery("#"+buttonid).attr('disabled',false);

				}
				else
				{
					jQuery("#"+buttonid).attr('disabled',true);
				}

				jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.

			});
		}

	function searchWHLocation()
	{
		showMessage("loading", "Loading information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"searchWHLocation",
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			
			var whlocationsData = jQuery.parseJSON(data);
			if(whlocationsData.Status==1){
				jsonForLocations=whlocationsData.Result;
				whlocations=whlocationsData.Result;
				
				jQuery.each(whlocations,function(key,value)
				{	
					jQuery("#GRNDestinationLocaiton").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");

				});
				jQuery('#GRNDestinationLocaiton').val(currentLocationId);
				jQuery('#GRNDestinationLocaiton').attr('disabled',true);
				backGroundColorForDisableField();
			}
			else{
				showMessage('red',whlocationsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	function VRSearchStatus()
	{
		showMessage("loading", "Loading information", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'VendorReturnCallBack',
			data:
			{
				id:"VRSearchStatus",
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var VRStatusData = jQuery.parseJSON(data);
			if(VRStatusData.Status==1){
				VRStatus=VRStatusData.Result;
				jQuery.each(VRStatus,function(key,value)
				{	
					
					jQuery("#VRSearchStatus").append("<option id=\""+VRStatus[key]['keycode1']+"\""+" value=\""+VRStatus[key]['keycode1']+"\""+">"+VRStatus[key]['keyvalue1']+"</option>");
				});
			}
			else{
				showMessage('red',GRNStatusData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	function vendoreCode()
	{
		showMessage("loading", "Loading information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'GRNCallback',
			data:
			{
				id:"vendoreCode",
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var vendoreCodeData = jQuery.parseJSON(data);
				if(vendoreCodeData.Status==1){
					vendoreCode=vendoreCodeData.Result;
				
				jQuery.each(vendoreCode,function(key,value)
				{	
					jQuery("#GRNVendorCode").append("<option id=\""+vendoreCode[key]['VendorId']+"\""+" value=\""+vendoreCode[key]['VendorId']+"\""+">"+vendoreCode[key]['VendorName']+"</option>");
				});
			}
				else{
					showMessage('red',vendoreCodeData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', yearRange: '1950:2010',minDate: +1, maxDate: "100D"});
	/*jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
	jQuery( ".showCalender" ).datepicker("setDate",new Date());
*/	
	
	jQuery(function() {
			//alert("dfadf");
			jQuery( "#divGoodReceiptNoteTab" ).tabs(); // Create tabs using jquery ui.
		});
    jQuery("#divGoodReceiptNoteTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
		
		var tabIndex = tab.index; 
		if(tabIndex == 0)
		{
			 disableFieldOnStartup();
			 enableFieldOnStartup();
			 jQuery("#GRNBatchDetailsGrid").jqGrid('clearGridData');
				jQuery("#GRNViewGrid").jqGrid("clearGridData");
				jQuery("#VrCreateGRNNumber").val('');				
				disableButtons(0);
		}
		else if(tabIndex == 1){
			
		}
		});
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN Search grid.
		 */
		jQuery("#GRNSearchGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['View','Return No','Return Date','GRN No','GRN Date','PO No','HandlingCharge','Vendor Code','Vendor Name','Status','Location Code',
			'Shipping Details','Shipment Date','Remarks','PODate','Debit Note Amount','InvoiceTaxAmount','Total Amount','ShippingDetails','Total Quantity','ReceivedBy',
			'StatusId','GRNStatus','CreatedDate','ModifiedBy','ModifiedDate','VendorName','D_Address1','D_Address2','D_Address3','D_Address4','D_City',
			'D_State','D_Country','VendorAddress','GrossWeight','NoOfBoxes'],
			colModel: [{ name: 'View', index: 'View', width:40 },
			           { name: 'VRNo', index: 'VRNo', width: 120},
			           { name: 'ReturnDate', index: 'ReturnDate', width:70 },
			           { name: 'GRNNo', index: 'GRNNo', width: 100},
			           { name: 'GRNDate', index: 'GRNDate', width: 100},
			           { name: 'PONo', index: 'PODate', width: 100, hidden:true},
			           { name: 'HandlingCharge', index: 'HandlingCharge', width: 70, hidden:true},
			           { name: 'VendorCode', index: 'VendorCode', width:1, hidden:true},
			           { name: 'VendorName', index: 'VendorName', width: 120},
			           { name: 'Status', index: 'Status', width: 70},
			           { name: 'LocationCode', index: 'LocationCode', width: 100},
			       
			           { name: 'ShippingDetails', index: 'ShippingDetails', width: 70, hidden:true},
			           { name: 'ShipmentDate', index: 'ShipmentDate', width: 70, hidden:true},
			           { name: 'Remarks', index: 'Remarks', width: 70, hidden:true},
			           { name: 'PODate', index: 'Podate', width: 70, hidden:true},
			           { name: 'DebitNoteAmt', index: 'DebitNoteAmt', width: 70, hidden:true},
			           { name: 'InvoiceTaxAmount', index: 'InvoiceTaxAmount', width: 70, hidden:true},
			           { name: 'TotalAmount', index: 'TotalAmount', width: 70, hidden:true},
			           { name: 'TransporterName', index: 'TransporterName', width: 70, hidden:true},
			           { name: 'TotalQty', index: 'TotalQty', width: 70, hidden:true},
			           { name: 'ReceivedBy', index: 'ReceivedBy', width: 70, hidden:true},
			           { name: 'StatusId', index: 'StatusId', width: 70, hidden:true},

			           { name: 'GRNStatus', index: 'GRNStatus', width: 70, hidden:true},
			           { name: 'CreatedDate', index: 'CreatedDate', width: 70, hidden:true},
			           { name: 'ModifiedBy', index: 'ModifiedBy', width: 70, hidden:true},
			           { name: 'ModifiedDate', index: 'ModifiedDate', width: 70, hidden:true},
			           { name: 'VendorName', index: 'VendorName', width: 70, hidden:true},
			           { name: 'D_Address1', index: 'D_Address1', width: 70, hidden:true},
			           { name: 'D_Address2', index: 'D_Address2', width: 70, hidden:true},
			           { name: 'D_Address3', index: 'D_Address3', width: 70, hidden:true},
			           { name: 'D_Address4', index: 'D_Address4', width: 70, hidden:true},
			           { name: 'D_City', index: 'D_City', width: 70, hidden:true},
			           { name: 'D_State', index: 'D_State', width: 70, hidden:true},
			           { name: 'D_Country', index: 'D_Country', width: 70, hidden:true},
			           { name: 'VendorAddress', index: 'VendorAddress', width:200 , hidden:true},
			           { name: 'GrossWeight', index: 'GrossWeight', width:1 , hidden:true},
			           { name: 'NoOfBoxes', index: 'NoOfBoxes', width:1 , hidden:true}
			           
			        
			          ],
			           
			           
			           pager: jQuery('#PJmap_GRNSearchGrid'),
			           width:1040,
			           height:470,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
                       shrinkToFit:true,

			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	  
			           },

			           loadComplete: function() {
//			        	   jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//			        	   alert("laodign complete");
			           },
			           onSelectRow: function(rowId) {

			        	   jQuery("#VrCreateGRNNumber").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GRNNo'));
			        	   jQuery("#VrReturnNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VRNo'));
			        	   jQuery("#GRNCreatePODate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GRNDate')));
			        	   jQuery("#PONo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'PONo'));
			        	   jQuery("#PODate").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'PODate'));
			        	   jQuery("#VrStatus").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'Status'));
			        	   jQuery("#GRNStatus").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GRNStatus'));
			        	   jQuery("#VrShipDetail").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ShippingDetails'));
			        	   jQuery("#VRShipDate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ReturnDate')));
			        	   jQuery("#GRNCreateVendorCode").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VendorCode'));
			        	   jQuery("#GRNCreateInvoiceNo").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceNo'));
			        	   jQuery("#GRNCreateInvoiceDate").val((jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceDate')));
			        	   jQuery("#GRNCreateVendorName").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VendorName'));
			        	   jQuery("#GRNViewInvoiceTax").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'InvoiceTaxAmount'));
			        	   jQuery("#VrShipDetail").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ShippingDetails'));
			        	   jQuery("#VrCreateTotalQty").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'TotalQty'));
			        	   jQuery("#GRNViewDestinationLocation").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'VendorAddress'));
			        	   jQuery("#VrTotalAmount").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'TotalAmount'));
			        	   jQuery("#PODate").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'PODate'));
			        	   jQuery("#HandlingCharge").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'HandlingCharge'));
			        	   jQuery("#GRNViewReceivedBy").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'ReceivedBy'));
			        	   jQuery("#VRCreateDebitAmt").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'DebitNoteAmt'));
			        	   jQuery("#VrRemarks").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'Remarks'));
			        	   jQuery("#NoOfBoxes").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'NoOfBoxes'));
			        	   jQuery("#ProductWeight").val(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'GrossWeight'));
			        	   
			        	   if(jQuery("#GRNCreateChallanNo").val()==''){
			        		   jQuery("#GRNCreateChallanDate").val('');
			        	   }
			        	   if(   jQuery("#GRNCreateInvoiceNo").val()==''){
			        		   jQuery("#GRNCreateInvoiceDate").val('');
			        	   }
			        	
			       		jQuery("#divGoodReceiptNoteTab").tabs( "select", "DivCreate" );
			       		searchBatchDetail( jQuery("#VrReturnNo").val(),jQuery("#VrCreateGRNNumber").val());
			       		disableButtons(jQuery("#GRNSearchGrid").jqGrid('getCell', rowId, 'StatusId'));
			       				           }
			         
		});

		jQuery("#GRNSearchGrid").jqGrid('navGrid','#PJmap_GRNSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNSearchGrid").hide();
		
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN View grid.
		 */
		 myDelOptions2 = {
					onclickSubmit: function (rp_ge, rowid) {
	               showMessage('','Cannot delete row','');
	          
	            return true;
	        },
	        processing: true
	        };
		jQuery("#GRNViewGrid").jqGrid({
	
			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','Batch No','Manufacturer Batch','GRN Qty','Already Returned Qty','Available Qty','Received Qty','Invoice Qty','ItemId',
			           'SerialNo','PONumber','GRNNo','BatchNumber','PurchaseUOM','MaxQty','Return Batches'],
			colModel: [
                       
			           { name: 'ItemCode', index: 'ItemCode', width: 100},
			           { name: 'ItemName', index: 'ItemName', width:170 },
			           { name: 'BatchNo', index: 'ManufacturingBatchNo', width: 170},
			           { name: 'ManufacturerBatch', index: 'ManufacturerBatch', width: 110},
			           { name: 'GRNQty', index: 'GRNQty', width: 100},
			           
			           { name: 'AlreadyReturnedQty', index: 'AlreadyReturnedQty',  width: 150},
			           { name: 'AvailableQty', index: 'AvailableQty',  width: 100},
			           { name: 'ReceivedQty', index: 'ReceivedQty',  width: 1,hidden:true},
			           { name: 'InvoiceQty', index: 'InvoiceQty',  width: 1,hidden:true },
			           { name: 'ItemId', index: 'ItemId', width: 1,hidden:true},
			           { name: 'SerialNo', index: 'SerialNo', width: 1,hidden:true},
			           
			           { name: 'PONumber', index: 'PONumber', width: 1,hidden:true},
			           { name: 'GRNNo', index: 'GRNNo', width: 1,hidden:true},
			           { name: 'BatchNumber', index: 'BatchNumber', width: 1,hidden:true},
			           { name: 'PurchaseUOM', index: 'PurchaseUOM', width: 1,hidden:true},
			           { name: 'MaxQty', index: 'MaxQty', width: 1,hidden:true},
			           { name: 'ADDBatches', index: 'AddBatches', width: 100}
			           ],
			           pager: jQuery('#PJmap_GRNViewGrid'),
			           width:1040,
			           height:150,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           
			           editurl:'clientArray',
			           shrinkToFit: false,
			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	  
			           },

			           loadComplete: function() {
//			        	   jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//			        	   alert("laodign complete");
			           }	
			           ,
			           
			           onSelectRow: function(rowId){
			        	 var itemCode=  jQuery('#GRNViewGrid').jqGrid('getCell',rowId, 'ItemCode');
			        	 //hideRowsONSelectItemCode(itemCode);
			        	// showAllRowsONSelectItemCode();
			           },
			           ondblClickRow: function(rowid)
			           {
			        	 //  showAllRowsONSelectItemCode();
			        	 //  saveRowOnNewRow();
			        	   
			        	
			           }
		});

		function rowIseditableOrNot(rowId){
			var ind=jQuery("#GRNBatchDetailsGrid").getInd(rowId,true);if(ind != false){
			    edited = jQuery(ind).attr("editable");
			}

			if (edited === "1"){
			 return  true ;
			} else{
			   return false ;
			}
		}
		function saveRowOnNewRow(){
			var Status=jQuery("#GRNViewGrid").jqGrid('getCell', rowId, 'Status');
			showMessage("yellow",'status is:'+status,'');
			if(jQuery("#GRNStatus").val()!='Approved' )
			{
			  batchRow= jQuery("#GRNBatchDetailsGrid").getGridParam('selrow');		  
			  
			  
	        	if(jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')==null)
	        	{
	        		addRow();
	        		
	        	}
	        	else if(rowIseditableOrNot(batchRow) || jQuery("tr#"+batchRow).attr("editable")==1)
	        	{
	        		
	        	     if(  jQuery("#GRNBatchDetailsGrid").jqGrid('saveRow',batchRow)==true)
	        	        {
	        	          checkSave(batchRow,'');
	        	          if(vallidateReturnQtyONSave()==1)
	        	          {
	        	             addRow();
	        	          }
	        	          else
	        	          {
	        	             jQuery("#GRNBatchDetailsGrid").jqGrid('editRow',batchRow);
	        	          }
	        	        }
	        	}
	        	else{
	        		
	        		addRow();
	        	}
	    	}
			jQuery('.ui-icon-pencil').hide();
		}

    	function checkSave(rowid,type){	
		
			var savedRowItemCode = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ItemCode');
			var temReciveQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
			sumOfQtytobeEdit=getBatchReciveItemSum(savedRowItemCode);
			if(type=='delete'){
	    	sumOfQtytobeEdit=sumOfQtytobeEdit-temReciveQty;
			}
			
	    	var viewGridRowId=selectRowIdFromItemCode(savedRowItemCode);
	    	if(viewGridRowId!=-1 && vallidateOnAddBatch(rowid,viewGridRowId)!=0){
	            
		            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
		            
		            	updateQuantityInGrid(sumOfQtytobeEdit,viewGridRowId);
		            	
	    	}
	    	else if(viewGridRowId==-1){
	    		jQuery("#GRNBatchDetailsGrid").jqGrid('editRow',rowid);
	    		showMessage("yellow",'Invalid Item Code','');
	    	}
		}
		jQuery("#GRNViewGrid").jqGrid('navGrid','#PJmap_GRNViewGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNViewGrid").hide();
		
		function addRow(){
			 jQuery("#GRNBatchDetailsGrid").addRow(1, {
       	      rowID : "new_row",
       	      initdata : {},
       	      position :"first",
       	      useDefValues : false,
       	      useFormatter : false,
       	      addRowParams : {extraparam:{}}
       	  });
			 vallidationForReciveQty();
		}
		
	
		jQuery("#VrCreateGRNNumber").keydown(function(e){
			
		    if(e.which==9 || e.which == 13){
		    	e.preventDefault(); 
		    	var GRNNumber = jQuery("#VrCreateGRNNumber").val(); 
		    	searchItemUsingCode(GRNNumber);
		    }

		});
		function searchItemUsingCode(GRNNumber){
			showMessage("loading", "Searching available GRN number...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VendorReturnCallback',
				data:
				{
					id:"searchGRNNumber",
					GRNNumber:GRNNumber
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					jsonForGRNSearch=data;
					itemDescData = jQuery.parseJSON(data);
				if(itemDescData.Status==1)
				{
					
					itemDesc=itemDescData.Result;
					if(itemDesc.length!=0)
				
					{
					 jQuery.each(itemDesc,function(key,value)
					 {	
						 if(itemDesc[key]['Status']=='Closed')
							{
						jQuery("#GRNCreateAmendmentNo").val(itemDesc[key]['AmendmentNo']);
						jQuery("#GRNStatus").val(itemDesc[key]['Status']);
						jQuery("#PONo").val(itemDesc[key]['PONumber']);
						jQuery("#PODate").val(displayDate(itemDesc[key]['PODate']));
						jQuery("#VrCreateTotalQty").val(0);
						jQuery("#VrTotalAmount").val(0);
						jQuery("#VRCreateDebitAmt").val(0);
						jQuery("#GRNCreateVendorCode").val(itemDesc[key]['VendorCode']);						
						jQuery("#GRNCreatePODate").val(displayDate(itemDesc[key]['GRNDate']));
						jQuery("#GRNCreateVendorName").val(itemDesc[key]['VendorName']);
						jQuery("#GRNViewDestinationLocation").val(itemDesc[key]['V_Address1']+' '+itemDesc[key]['V_Address2']+ '  '+itemDesc[key]['V_Address3']+ ' '+itemDesc[key]['V_Address4']+' '+itemDesc[key]['CityName']+' '+itemDesc[key]['StateName']+' '+itemDesc[key]['CountryName']+' ');
						
						
						
						jQuery("#VrCreateGRNNumber").css("background","white");
						
						
						createBatchDetailRow(GRNNumber);
							
						jQuery("#VrCreateGRNNumber").attr('disabled',true);
							}
						 else
							 {
							 showMessage("yellow","Enter Valid GRN Number .","");
							 }
					  });
					
				
				    }
					else {
						jQuery("#VrCreateGRNNumber").css("background","#FF9999");
				        showMessage("yellow","Enter Valid GRN Number .","");
				        jQuery("#VrCreateGRNNumber").focus();
						return;
					}
				}
				else{
					showMessage('red',itemDescData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
			
		}

		
		
		jQuery("#btnSearchGRNReset").unbind("click").click(function(){
			jQuery("#GRNumber").val('');
			jQuery("#GRNPONumber").val('');
			jQuery("#GRNReceivedBy").val('');
			jQuery("#GRNVendorCode").val(-1);
		   jQuery("#GRNSearchStatus").val(-1);
		   jQuery("#GRNSearchGrid").jqGrid("clearGridData");
			showCalender();
		});	
		
		
		
		jQuery("#btnRET01Search").unbind("click").click(function(){
			searchVR();
		});	
		
		
		function searchVR(){
			jQuery("#actionName").val('Search');
			showMessage("loading", "Searching for Vendor Returns...", "");
			var searchFormData = jQuery("#formSearchTO").serialize();
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VendorReturnCallback',
				data:
				{
					id:"searchVR",
					TOFormData:searchFormData,
					VRNumber:jQuery("#VRNumber").val(),
					VRGRNNumber:jQuery("#VRGRNNumber").val(),
					VRCreatedBy:jQuery("#VRCreatedBy").val(),
					VendorID :jQuery("#GRNVendorCode").val(),
					Status:jQuery("#VRSearchStatus").val(),
					LocationID :jQuery("#GRNDestinationLocaiton").val(),
					FromVRDate:getDate(jQuery("#VRFromVRDate").val()),
					ToVRDate :getDate(jQuery("#VRTOVRDate").val()),
					FromGRNDate:getDate(jQuery("#VRFromGRNDate").val()),
					ToGRNDate:getDate(jQuery("#VRToGRNDate").val()),
					functionName:jQuery("#actionName").val(),
					moduleCode:"RET01"
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
	if(itemDescData.Status==1){
		itemDesc=itemDescData.Result;
	
		if(itemDesc.length!=0)
	
				{
				jQuery("#GRNSearchGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){
					var Quantity='';
					if(itemDesc[key]['Quantity']!=''){
						Quantity=parseInt(parseFloat(itemDesc[key]['Quantity'],10).toFixed(2));
					}
						var newData = [{"View":'<Button type="button" id="">Open</button>',
							"VRNo":itemDesc[key]['ReturnNo'],
							"ReturnDate":displayDate(itemDesc[key]['RetVendorDate']),
							"GRNNo":itemDesc[key]['GRNNo'],
							"GRNDate":displayDate(itemDesc[key]['grnDate']),
							"ShippingDetails":itemDesc[key]['ShippingDetails'],
							"InvoiceNo":itemDesc[key]['InvoiceNo'],
							"Status":itemDesc[key]['Status'],
					      	"LocationCode":itemDesc[key]['LocationCode'] ,
					      	
						"VendorCode":itemDesc[key]['VendorCode'],
						"PONo":itemDesc[key]['PONumber'],
						"PODate":displayDate(itemDesc[key]['PODate']),
				      	"ShipmentDate":displayDate(itemDesc[key]['ShipmentDate']),
				      	"VendorName":itemDesc[key]['VendorName'],
						"TotalAmount":formatFloatNumbers(parseFloat(itemDesc[key]['TotalAmount'])),
				      	"DebitNoteAmt":formatFloatNumbers(parseFloat(itemDesc[key]['DebitNoteAmount'])),
				      	"HandlingCharge":formatFloatNumbers(parseFloat(itemDesc[key]['HandlingCharge'],10).toFixed(2)),
				      	"InvoiceAmount":formatFloatNumbers(parseFloat(itemDesc[key]['InvoiceAmount'])),				      	
				      	"TotalQty":Quantity,
						"ReceivedBy":itemDesc[key]['ReceivedBy'],
				      	"StatusId":itemDesc[key]['StatusId'],
				      	"GRNStatus":itemDesc[key]['GRNStatus'],
				      	"CreatedDate":itemDesc[key]['CreatedDate'],
					 	"ModifiedBy":itemDesc[key]['ModifiedBy'],
				      	"ModifiedDate":itemDesc[key]['ModifiedDate'],
						"VendorName":itemDesc[key]['VendorName'],
				   
				      	"D_Address1":itemDesc[key]['D_Address1'],
				      	"D_Address2":itemDesc[key]['D_Address2'],
				      	"D_Address3":itemDesc[key]['D_Address3'],
				      	"D_Address4":itemDesc[key]['D_Address4'],
				      	"D_City":itemDesc[key]['D_City'],
				      	"D_State":itemDesc[key]['D_State'],
				      	"VendorAddress":itemDesc[key]['address'],
				      	"D_Country":itemDesc[key]['D_Country'],
				      	"Remarks":itemDesc[key]['Remarks'],
				      	"GrossWeight":formatFloatNumbers(parseFloat(itemDesc[key]['GrossWeight'])),
				      	"NoOfBoxes":parseInt(parseFloat(itemDesc[key]['NoOfBoxes']))
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#GRNSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						
					 });
					
				    }
			else 	{
						
					        showMessage("yellow","No Record Found","");
					        jQuery("#GRNSearchGrid").jqGrid("clearGridData");
							return;
					}
				}
	else{
		showMessage('red',itemDescData.Description,'');
	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
}
		
	
		function removeSpecialCharacter(mystring)
		{
			
			//mystring = mystring.replace('/','');
			mystring=mystring.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-').replace(/\//g, "-");
			return mystring;
			
		}
		
		function createBatchDetailRow(GRNNumber)
		{
		showMessage("loading", "Searching for item batch details...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'VendorReturnCallBack',
			data:
			{
				id:"searchVRItemDetails",
				//PONumber:PONumber,
				GRNNumber:GRNNumber
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();	
				var rowid =0;
				itemDescData = jQuery.parseJSON(data);
	if(itemDescData.Status==1){
		
		itemDesc=itemDescData.Result;
		if(itemDesc.length!=0)
				{
			
			jQuery("#GRNBatchDetailsGrid").jqGrid("clearGridData");
			var incrementer = 0;
			jQuery.each(itemDesc,function(key,value){	
				var invoiceQty='';
				if(itemDesc[key]['InvoiceQty']!=''){
						invoiceQty=parseInt(parseFloat(itemDesc[key]['InvoiceQty'],10).toFixed(2));
				}
				else{
					invoiceQty="''";
				}
				
				incrementer += 1;
				
				var elementId = "test"+incrementer;
					
					var BatchNo=removeSpecialCharacter(itemDesc[key]['BatchNumber']);
					var newData = [{
						"ItemCode":itemDesc[key]['ItemCode'],
						"ItemName":itemDesc[key]['ItemDescription'],
						"BatchNo":itemDesc[key]['BatchNumber'],
						"IsItemVendorExcised":itemDesc[key]['IsItemVendorExcised'],
						"ManufacturerBatch":itemDesc[key]['ManufacturerBatchNo'],
						"ManufacturingDate":displayDate(itemDesc[key]['ManufacturingDate']),
						"ExpiryDate":displayDate(itemDesc[key]['ExpiryDate']),
						"InStockQty":parseInt(parseFloat(itemDesc[key]['InStockQuantity'],10).toFixed(2)),
						"GRNQty":parseInt(parseFloat(itemDesc[key]['ReceivedQty'],10).toFixed(2)),
						"AlreadyReturnedQty":parseInt(parseFloat(itemDesc[key]['ReturnedQty'],10).toFixed(2)),
						"PerUnitPrice":parseFloat(itemDesc[key]['PerUnitPrice'],10).toFixed(2),
						"UnitPriceWithTax":itemDesc[key]['UnitPriceWithTax'],
						"PerUnitTax":parseFloat(itemDesc[key]['UnitTax'],10).toFixed(2),
						"AvailableQty":parseInt(parseFloat(itemDesc[key]['ReceivedQty'],10).toFixed(2))-parseInt(parseFloat(itemDesc[key]['ReturnedQty'],10).toFixed(2)),
						"ReturnQty":"<input type='text' placeHolder='0' name='ReturnQty' style='width:70px;' class='ReturnQty' id="
									+BatchNo+">"
					}] ;

					for (var i=0;i<newData.length;i++) {
						jQuery("#GRNBatchDetailsGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
						rowid=rowid+1;
						}
					
					   // channgeinClaimQtyChange();
					    //AddBatches();'UnitPriceWithTax','PerUnitTax'
				 });
			
			
			
			jQuery(".ReturnQty").keyup(function(e) {
				 var Cellid = this.id;
				 vallidateReturnQtyONSave(Cellid);
				//jQuery("#"+Cellid).css("background","#FF9999");				
				var itemInformation = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
				jQuery(".ReturnQty").each(function(){
				var returnCellid = this.id;
				var retQty = jQuery("#"+returnCellid).val();
				var intRegex =/^[\d]+$/;
				
				jQuery.each(itemInformation,function(key,value){

					
					

						if(  !intRegex.test(retQty)){
					
							jQuery("#"+returnCellid).val("");
							
						}
						
						
						else{
							
							//jQuery("#"+returnCellid).css("background","white");
							}
						
						updateQuantityAndAmount();
						updateTotalAmount();
						
					
					
				});
});


});
				
			    }
		else 	{
						jQuery("#txtItemCode").css("background","#FF9999");
				        showMessage("yellow","Enter Valid Item Code.","");
				        jQuery("#txtItemCode").focus();
						return;
				}
			}
	else{
		showMessage('yellow',itemDescData.Description,'');
	}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});	
		
		
	}
		
		
		 function getJsonFromBatchGrid (){
				var itemInformation = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');

				var sItemInformation = JSON.stringify(itemInformation);

				jQuery(".ReturnQty").each(function(){

					var claimCellid = this.id;
					var retQty = jQuery("#"+claimCellid).val();
					if(jQuery("#"+claimCellid).val()=='')
						{
						retQty=0;
						}



					jQuery.each(itemInformation,function(key,value){


						if(claimCellid == removeSpecialCharacter(itemInformation[key]['BatchNo']))
						{
							itemInformation[key]['ReturnQty'] = retQty;
						}
						else
						{

						}

					});


				});
				return itemInformation;
			}
		    
		
		function vallidationForReciveQty(){
			jQuery('.editable').keyup(function(){
			  value=jQuery(this).val();	
			
			  if(isNaN(value)==false && this.id==jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_'+'ReturnQty'){
				 if( vallidateQtyOfPOItemsONSave(value,  jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', jQuery("#GRNBatchDetailsGrid").getGridParam('selrow'), 'BatchNo'))==0){
					 jQuery('.AddBatches').attr('disabled',true); 
				 }
				 else{
					 jQuery('.AddBatches').attr('disabled',false);
				 }
			  }
			
			});
		}
		function searchBatchDetail(ReturnNo,GRNNo){
			showMessage("loading", "Searching for item batch details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VendorReturnCallback',
				data:
				{
					id:"searchVRBatchDetail",
					ReturnNo:ReturnNo,
					GRNNo:GRNNo
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();	
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
		if(itemDescData.Status==1){
			
			itemDesc=itemDescData.Result;
			if(itemDesc.length!=0)
					{
			
				jQuery("#GRNBatchDetailsGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
				
					var BatchNo=removeSpecialCharacter(itemDesc[key]['BatchNumber']);
					var newData = [{
						"ItemCode":itemDesc[key]['ItemCode'],
						"ItemName":itemDesc[key]['ItemDescription'],
						"BatchNo":itemDesc[key]['BatchNumber'],
						"IsItemVendorExcised":itemDesc[key]['IsItemVendorExcised'],
						"ManufacturerBatch":itemDesc[key]['ManufacturerBatchNo'],
						"ManufacturingDate":displayDate(itemDesc[key]['ManufacturingDate']),
						"ExpiryDate":displayDate(itemDesc[key]['ExpiryDate']),
						"GRNQty":parseInt(parseFloat(itemDesc[key]['ReceivedQty'],10).toFixed(2)),
						"InStockQty":parseInt(parseFloat(itemDesc[key]['InStockQuantity'],10).toFixed(2)),
						"AlreadyReturnedQty":parseInt(parseFloat(itemDesc[key]['ReturnedQty'],10).toFixed(2)),
						"PerUnitPrice":parseFloat(itemDesc[key]['PerUnitPrice'],10).toFixed(2),
						"TotalAmount":formatFloatNumbers(parseFloat(itemDesc[key]['PerUnitPrice'],10).toFixed(2)*parseFloat(itemDesc[key]['ReturnQty'],10).toFixed(2)),
						"UnitPriceWithTax":itemDesc[key]['UnitPriceWithTax'],
						"PerUnitTax":parseFloat(itemDesc[key]['UnitTax'],10).toFixed(2),
						"AvailableQty":parseInt(parseFloat(itemDesc[key]['ReceivedQty'],10).toFixed(2))-parseInt(parseFloat(itemDesc[key]['ReturnedQty'],10).toFixed(2)),
						"ReturnQty":"<input type='text'  name='returnQty' style='width:70px;' value="
							+parseInt(parseFloat(itemDesc[key]['ReturnQty'],10).toFixed(2))+" class='ReturnQty' id="
									+BatchNo+">"
					}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#GRNBatchDetailsGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						
					 });
				jQuery(".ReturnQty").keyup(function(e) {
					 var Cellid = this.id;
					 vallidateReturnQtyONSave(Cellid);
					//jQuery("#"+Cellid).css("background","#FF9999");				
					var itemInformation = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
					jQuery(".ReturnQty").each(function(){
					var returnCellid = this.id;
					var retQty = jQuery("#"+returnCellid).val();
					var intRegex =/^[\d]+$/;
					
					jQuery.each(itemInformation,function(key,value){

						
						

							if(  !intRegex.test(retQty)){
						
								jQuery("#"+returnCellid).val("");
								
							}
							
							
							else{
								
								//jQuery("#"+returnCellid).css("background","white");
								}
							
							updateQuantityAndAmount();
							updateTotalAmount();
							
						
						
					});
	});


	});		
				
				jQuery('.ui-icon-pencil').hide();
				    }
			else 	{
							jQuery("#txtItemCode").css("background","#FF9999");
					        showMessage("yellow","Enter Valid Item Code.","");
					        jQuery("#txtItemCode").focus();
							return;
					}
				}
							else{
								showMessage('red',itemDescData.Description,'');
							}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
				});	
			
			
	   }
		
		
		
		function disableFieldOnStartup(){
			jQuery("#GRNCreatePODate").attr('disabled',true);
			jQuery("#GRNCreatePODate").val('');
			jQuery("#PONo").attr('disabled',true);
			jQuery("#PONo").val('');
			jQuery("#VrReturnNo").attr('disabled',true);
			jQuery("#VrReturnNo").val('');
       		jQuery("#PODate").attr('disabled',true);
       		jQuery("#PODate").val('');
       		jQuery("#GRNStatus").attr('disabled',true);
       		jQuery("#GRNStatus").val(''); 
       		jQuery("#VrStatus").attr('disabled',true);
       		jQuery("#VrStatus").val(''); 
       		jQuery("#GRNCreateVendorCode").attr('disabled',true);
       		jQuery("#GRNCreateVendorCode").val('');
       		jQuery("#VRCreateDebitNo").attr('disabled',true);	
       		jQuery("#VRCreateDebitNo").val('');
			jQuery("#GRNCreateVendorName").attr('disabled',true);
			jQuery("#GRNCreateVendorName").val('');
			jQuery("#VrCreateTotalQty").attr('disabled',true);
			jQuery("#VrCreateTotalQty").val('');
			jQuery("#VRCreateDebitNo").attr('disabled',true);
			jQuery("#VRCreateDebitAmt").attr('disabled',true);
			jQuery("#VRCreateDebitNo").val('');
			jQuery("#VRCreateDebitAmt").val('');
			jQuery("#GRNViewShippingDetails").attr('disabled',true);
			jQuery("#GRNViewShippingDetails").val('');
			jQuery("#GRNViewDestinationLocation").attr('disabled',true);
			jQuery("#GRNViewDestinationLocation").val('');
			jQuery("#VrTotalAmount").attr('disabled',true);
			jQuery("#VrTotalAmount").val('');
            jQuery("#GRNCreateInvoiceNo").attr('disabled',true);
            jQuery("#GRNCreateInvoiceNo").val('');
            jQuery("#GRNViewVehicleNo").attr('disabled',true);	
            jQuery("#GRNViewVehicleNo").val('');            
            jQuery("#GRNViewReceivedBy").attr('disabled',true);
            jQuery("#GRNViewReceivedBy").val('');
			jQuery("#VrRemarks").attr('disabled',true);
			jQuery("#VrRemarks").val('');
			jQuery("#HandlingCharge").attr('disabled',true);
			jQuery("#HandlingCharge").val('');
			jQuery("#GRNViewTransporterName").attr('disabled',true);
			jQuery("#GRNViewTransporterName").val('');
			jQuery("#GRNViewVehicleNo").attr('disabled',true);
			jQuery("#GRNViewVehicleNo").val('');
			jQuery("#GRNTransportNo").val('');
			jQuery("#VRShipDate").attr('disabled',true);
			jQuery("#VrShipDetail").val('');
			jQuery("#NoOfBoxes").val('');
			jQuery("#ProductWeight").val('');
			jQuery("#CRCreateItemCode2").hide();
			showCalender();
		}

		function enableFieldOnStartup(){
	

			jQuery("#VrCreateGRNNumber").attr('disabled',false);

		}

		
		var field1, field2,
	    myCustomCheck = function (value, colname) {
	        if (colname == "Remove") {
	            if(value == 12)
	            {
	            	alert("12 entered");
	            	return [true];
	            }
	            else
	            {
	            	return [false, "some error text"];
	            }
	        } 

	        /*if (field1 !== undefined && field2 !== undefined) {
	            // validate the fields here
	            return [false, "some error text"];
	        } else {
	            return [true];
	        }*/
	        
	      
	    };
	    
	    function vallidateReturnQtyONSave(cellId){
/*	    	var  value,receivedQty,returnQty,alreadyReturnedQty ;
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	
	    	for(var index=0;index<allRowIds.length;index++){
	    		receivedQty=parseInt(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index],'GRNQty'));
		    	
		    	 returnQty=parseInt(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'ReturnQty'));
		    	alreadyReturnedQty=parseInt(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'AlreadyReturnedQty'));
	    		   value=receivedQty - ( returnQty	+ alreadyReturnedQty);
	  			    	
	  			   if(value<0)
	  				   {
	  				     showMessage('yellow','You can return maximum:'+ (receivedQty-alreadyReturnedQty)+' for BatchNumber- '+jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index],'BatchNo'));
	  				     return 0;
	  				     
	  				   }
	    		  
			    	
	    	}
	    	return 1 ;*/
	    		var breakOut; var breakOut2;
				var itemInformation =getJsonFromBatchGrid();




					jQuery.each(itemInformation,function(key,value){


						if(cellId == removeSpecialCharacter(itemInformation[key]['BatchNo']))
						{
							value=parseInt(itemInformation[key]['InStockQty']) -(parseInt(itemInformation[key]['ReturnQty']));
							value1=parseInt(itemInformation[key]['GRNQty']) -(parseInt(itemInformation[key]['AlreadyReturnedQty'])+ parseInt(itemInformation[key]['ReturnQty']));
							 
							   if(value1<0)
			  				   {
								 jQuery("#"+cellId).css("background","#FF9999").focus();
			  				     showMessage('yellow','You can return maximum:'+ (parseInt(itemInformation[key]['GRNQty'])-parseInt(itemInformation[key]['AlreadyReturnedQty'])));
			  				   jQuery("#"+cellId).val(parseInt(itemInformation[key]['GRNQty'])-parseInt(itemInformation[key]['AlreadyReturnedQty']));
			  				     breakOut = true;
			  				     breakOut2=true;
			  				     return false;			  				     
			  				   }
							   else
								   {
								   jQuery("#"+cellId).css("background","white");
								   }   
							if(value<0)
			  				   {
								 jQuery("#"+cellId).css("background","#FF9999").focus();
			  				     showMessage('yellow','You can return maximum:'+ (parseInt(itemInformation[key]['InStockQty'])));
			  				     jQuery("#"+cellId).val(parseInt(itemInformation[key]['InStockQty']));
			  				     breakOut = true;
			  				     breakOut2=true;
			  				     return false;			  				     
			  				   }
							   else
								   {
								   jQuery("#"+cellId).css("background","white");
								   }
						}

					});
					if(breakOut) {
					    breakOut = false;
					    return false;
					}


				if(breakOut2=true)
				return 0;
				else 
				return 1;
			
	    	
	    }
	    

	    
	    function vallidateQtyOfPOItemsONSave(values,BatchNo){
	    	var  value ;
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	       if(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'BatchNo')==BatchNo){
	    	    	   value=	parseInt(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'ReceivedQty'))-
		    		   (   	parseInt(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'ReturnQty'))+
		    				   parseInt(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'AlreadyReturnedQty'))+
		  			    	parseFloat(values));
		  			   if(value<0)
		  				   {
		  				     showMessage('yellow','Return Qty for Batch Number '+jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index],'BatchNo')+' exceeded limit by '+ (-value),'');
		  				     return 0;
		  				     
		  				   }
	    	       }
	    		  
			    	
	    	}
	    	return 1 ;//
	    }
	    function updateTotalAmount(){
	    	var totalAmount=0;var retQty=0;
	    	var arrayFromGrid=getJsonFromBatchGrid();
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    		retQty=arrayFromGrid[index].ReturnQty;
	    		var amount=parseFloat(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'PerUnitPrice'));
	    		totalAmount=formatFloatNumbers(parseFloat(retQty)*amount);
	    		jQuery("#GRNBatchDetailsGrid").jqGrid('setCell',allRowIds[index],'TotalAmount',totalAmount);
			    	
	    	}
	    
	    }
	    
	    function vallidateForReturnQtyNotgreaterThenAvlQty(){
	    	var  value ;
	    	var arrayFromGrid=getJsonFromBatchGrid();
	    
	    	for(var index=0;index<arrayFromGrid.length;index++){
	    		if(arrayFromGrid[index].ReturnQty==''){
	    			arrayFromGrid[index].ReturnQty=0;
	    		}
	    		if(isNaN(arrayFromGrid[index].ReturnQty)){
	    			 showMessage('yellow','Return Quantity should be numeric.','');
	    			 jQuery(this).focus();
	    			return 0;
	    		}
	    		  
	  			   if(parseFloat(arrayFromGrid[index].InvoiceQty)+parseFloat(arrayFromGrid[index].AlreadyInvoicedQty)<parseFloat(arrayFromGrid[index].ReceivedQty)+parseFloat(arrayFromGrid[index].AlreadyReceivedQty))
	  				   {
	  				      showMessage('yellow',"Received Quantity can't be more than Invoice Quantity. Item Code - "+arrayFromGrid[index].ItemCode,'');
	  				    jQuery(this).focus();
		    			return 0; 
	  				   }
	  			 if(parseFloat(arrayFromGrid[index].ReturnQty)>parseFloat(arrayFromGrid[index].AvailableQty))
				   {
				      showMessage('yellow',"Return Quantity Out of Return limit of GRN. Item Code - "+arrayFromGrid[index].ItemCode,'');
				    jQuery(this).focus();
	    			return 0; 
				   }
	    	}
	    	return 1 ;
	    }
	    
	    
	    
	    
	    function showAllRowsONSelectItemCode(){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	       		jQuery("#"+allRowIds[index],"#GRNBatchDetailsGrid").show();
	    	}
	    }
	    
	    
	    
	    function hideRowsONSelectItemCode(itemCode){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	var batchItemCode=	jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'ItemCode');
			    	if(batchItemCode==itemCode){
			    		jQuery("#"+allRowIds[index],"#GRNBatchDetailsGrid").show();
			    		
			    	}
			    	else{
			    		jQuery("#"+allRowIds[index],"#GRNBatchDetailsGrid").hide();
			    	}
			    	
	    	}
	    	
	    }
	    function getBatchReciveItemSum(itemCode){
	    	var sumOfItems=0 ;
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    	var batchItemCode=	jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'ItemCode');
			    	if(batchItemCode==itemCode){
			    		
			    		sumOfItems=sumOfItems+parseInt(parseFloat(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',allRowIds[index], 'ReceivedQty'),10).toFixed(0));
			    		
			    	}  	
			    	
	    	}
	    	return sumOfItems ;
	    }
	    

	    
	    function selectRowIdFromItemCode(itemCode){
	    var allRows = jQuery('#GRNViewGrid').jqGrid('getDataIDs');
	    	for(var i=0; i<allRows.length ; i++){
	    		if(jQuery('#GRNViewGrid').jqGrid('getRowData', allRows[i]).ItemCode==itemCode){
	    			return allRows[i] ;
	    		}
	     		
	    	}
	    	return -1;
	    }
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN Batch Details Grid.
		 */
	    function currencyFmatter (cellvalue, options, rowObject)
	    {
	       // do something here
	    	if(jQuery("#GRNViewGrid").getGridParam('selrow')!=undefined){
	    		
	    	return	   jQuery('#GRNViewGrid').jqGrid('getCell',jQuery("#GRNViewGrid").getGridParam('selrow'), 'ItemCode');
	    	}
	    	if(cellvalue!=undefined){
	    		return cellvalue ;
	    	}
	    		
	    		return '';
	    }
	  /*  function currencyFmatter2 (cellvalue, options, rowObject)
	    {
	    	jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_ExpiryDate').focus();
	    	return '';
	    }*/
	    
	    
	    
	    /**
	     * edit param function for jqgrid to edit row data.
	     */
	
	  
	    myDelOptions = {
				onclickSubmit: function (rp_ge, rowid) {
            rp_ge.processing = true;
         //   if(isNaN(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',rowid, 'ReceivedQty'))==false){
	        	if(jQuery("tr#"+rowid).attr("editable")==0){
	        	checkSave(rowid,'delete');
	        	}
            	jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
            
                jQuery("#delmod" + jQuery("#GRNBatchDetailsGrid")[0].id).hide();
 
                if (jQuery("#GRNBatchDetailsGrid")[0].p.lastpage > 1) {
                	jQuery("#GRNBatchDetailsGrid").trigger("reloadGrid", [{ page: jQuery("#TOItemGrid")[0].p.page}]);
                }
          
            return true;
        },
        processing: true
        };
		
	    
	 /*  var myEditOptions = {
	            keys: true,
	            oneditfunc: function (rowid) {
	               // alert("row with rowid=" + rowid + " is editing.");
	            },
	            aftersavefunc: function (rowid, response, options) {
	            	var savedRowItemCode = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ItemCode');
	            	sumOfQtytobeEdit=getBatchReciveItemSum(savedRowItemCode);
	            	var viewGridRowId=selectRowIdFromItemCode(savedRowItemCode);
	            	if(viewGridRowId!=-1 && vallidateOnAddBatch(rowid,viewGridRowId)!=0){
                        
				            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
				            
				            	updateQuantityInGrid(sumOfQtytobeEdit,viewGridRowId);
				            	
	            	}
	            	else if(viewGridRowId==-1){
	            		//jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
	            		showMessage("red",'Invalid Item Code','');
	            	}
	            }
	      };*/
	    
	/*  var  editparameters = {
	    		"keys" : false,
	    		"oneditfunc" : null,
	    		"successfunc" : null,
	    		"url" : 'clientArray',
	    	        "extraparam" : {},
	    		"aftersavefunc" : null,
	    		"errorfunc": null,
	    		"afterrestorefunc" : null,
	    		"restoreAfterError" : true,
	    		"mtype" : "POST"
	    	};
	    */
		jQuery("#GRNBatchDetailsGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','Batch No','Mfg Batch','Mfg Date','Expiry Date','GRN Qty','Already Ret. Qty','GRN Avl. Qty','In Stock Qty','Received Qty','Invoice Qty','ItemId',
			           'SerialNo','PONumber','GRNNo','UnitPriceWithTax','PerUnitTax','BatchNumber','IsItemVendorexcised','UnitPrice','Return Qty','TotalAmount'],
			colModel: [          
      
			           { name: 'ItemCode', index: 'ItemCode', width: 58,editable:false,sorttype:'string',formatter:currencyFmatter },
			           { name: 'ItemName', index: 'ItemName', width:160,editable:false,sorttype:'string'},
			           { name: 'BatchNo', index: 'BatchNo', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'ManufacturerBatch', index: 'ManufacturerBatch', width: 75,editable:false,sorttype:'string'},
			           { name: 'ManufacturingDate', index: 'ManufacturingDate', width: 80,editable:false,sorttype:'string'},
			           { name: 'ExpiryDate', index: 'ExpiryDate', width: 80,editable:false,sorttype:'string'},
			           { name: 'GRNQty', index: 'GRNQty', width: 78,editable:false,sorttype:'string'},
			           { name: 'AlreadyReturnedQty', index: 'AlreadyReturnedQty',  width: 90,editable:false,sorttype:'string'},
			           { name: 'AvailableQty', index: 'AvailableQty',  width: 78,editable:false,sorttype:'string'},
			           { name: 'InStockQty', index: 'InStockQty',  width: 78,editable:false,sorttype:'string'},
			           { name: 'ReceivedQty', index: 'ReceivedQty',  width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'InvoiceQty', index: 'InvoiceQty',  width: 1,hidden:true,editable:false,sorttype:'string' },
			           { name: 'ItemId', index: 'ItemId', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'SerialNo', index: 'SerialNo', width: 1,hidden:true,editable:false,sorttype:'string'},			         
			           { name: 'PONumber', index: 'PONumber', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'GRNNo', index: 'GRNNo', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'UnitPriceWithTax', index: 'UnitPriceWithTax', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'PerUnitTax', index: 'PerUnitTax', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'BatchNumber', index: 'BatchNumber', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'IsItemVendorExcised', index: 'IsItemVendorExcised', width: 1,hidden:true,editable:false,sorttype:'string'},
			           { name: 'PerUnitPrice', index: 'PerUnitPrice', width:60,hidden:false,editable:false,sorttype:'string'},			           
			           { name: 'ReturnQty', index: 'ReturnQty', width:78,editable:false,edittype:"text" 	},
			           { name: 'TotalAmount', index: 'TotalAmount', width: 70,editable:false,edittype:"text" 	}
			            //custom:true,custom_func: validationForReturnQty
					
			        	
			          ],
			         
			           pager: jQuery('#PJmap_GRNBatchDetailsGrid'),

			           width:1040,
			           height:300,
			           rowNum: 500,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           editurl:'clientArray',
			           shrinkToFit:false,
			           gridComplete: function()
			           {	 
			        	   disableBatchGridTextBox();
			           },
			           
			           
			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
			        	  
			           }
			          
		});
		
		function updateQuantityAndAmount()
		{
			var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
			var totalamount=0;var totalTaxtAmount=0;var amount=0;var qty=0;var rqty=0;
			var DetailsGrid = getJsonFromBatchGrid();
		/*	for(var index=0;index<allRowIds.length;index++){    	
				 rqty=jQuery('#DetailsGrid').jqGrid('getCell', allRowIds[index], 'ReturnQty');
				if(!isNaN(rqty))
					{ 
					if(rqty=="")
						{
						rqty=0;
						}
					qty=qty+parseInt(rqty);
					totalamount=totalamount + (parseInt(rqty) * formatFloatNumbers(jQuery('#DetailsGrid').jqGrid('getCell', allRowIds[index], 'PerUnitPrice')));
					}
				
			}*/// for closed
			jQuery.each(DetailsGrid,function(key,value)
			{
				var retQty=DetailsGrid[key]['ReturnQty'];
				if(retQty=="")
					{
					retQty=0;
					}
				retQty=parseInt(retQty);
				qty=qty+retQty;
				totalamount=totalamount + (parseInt(retQty) * DetailsGrid[key]['UnitPriceWithTax']);
				totalTaxtAmount=totalTaxtAmount+(parseInt(retQty) * formatFloatNumbers(DetailsGrid[key]['PerUnitTax']));
			});
			
			jQuery("#VrCreateTotalQty").val(parseInt(qty));
			jQuery("#VrTotalAmount").val(formatFloatNumbers(parseFloat(totalamount).toFixed(2)));
			jQuery("#VRCreateDebitAmt").val(formatFloatNumbers(parseFloat(totalamount).toFixed(2)));
			updateDebitAmount();
		}
		
		jQuery("#GRNBatchDetailsGrid").jqGrid('navGrid','#PJmap_GRNBatchDetailsGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_GRNBatchDetailsGrid").hide();
		jQuery('.ui-icon-pencil').hide();
		
		function saveditableRows(){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    		if(jQuery("tr#"+allRowIds[index]).attr("editable")==1 || rowIseditableOrNot(allRowIds[index]))
	       		{
	    			 if(  jQuery("#GRNBatchDetailsGrid").jqGrid('saveRow',allRowIds[index])==true){
			        	   //checkSave(allRowIds[index],'');
			        	 }
	    			 else{
	    				 return 0;
	    			 }
	       		}
	    	}
	    	 return 1;
	    }
		jQuery("#GRNCreateChallanNo").change(function(){
			
			jQuery("#GRNCreateChallanNo").css("background","white");
		});
		jQuery("#GRNCreateInvoiceNo").change(function(){
			
			jQuery("#GRNCreateInvoiceNo").css("background","white");
		});
		

		


function validationForExcise()
{
	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	
	for(var index=0;index<allRowIds.length;index++){    	
    	 var IsItemVendorExcised=jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'IsItemVendorExcised');
    		if(IsItemVendorExcised==1)
    		{
    			var res=confirm('Excise applies to ItemCode'+jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'ItemCode')+'\n'
    					+'Have you prepared all required documents for excise return?\n'
    					
    					+'Press "OK" to proceed.' );
    			if(res==false)
    				{
    				
    				return 0;
    				}
    		
    		}
    	}
	return 1;
	}


		
		
function validationOnSaveButton (){
			
			showAllRowsONSelectItemCode();
				if(	jQuery("#VrCreateGRNNumber").val()==''){
				jQuery("#VrCreateGRNNumber").css("background","#FF9999");
				showMessage('yellow','Enter Valid GRN Number .','');
				jQuery("#VrCreateGRNNumber").focus();
				return 0;
				}
				else{
					jQuery("#VrCreateGRNNumber").css("background","white");
				}
				if(	jQuery("#PONo").val()==''){
					showMessage('yellow','PO Number is not valid .','');
					return 0;
					}				
/*					if (diffrenInDays(jQuery("#VRShipDate").val(),jQuery("#GRNCreatePODate").val())<0){
						showMessage('yellow','Shipment date  cannot be lesser then GRN Date','');
						jQuery("#VRShipDate").focus();
						jQuery("#VRShipDate").select();
						return 0;
					}*/
					
					if(jQuery("#VrCreateTotalQty").val()==''||parseFloat(jQuery("#VrCreateTotalQty").val(),10).toFixed(0)==0){
						showMessage('yellow','Enter return quantity of atleast one item in item batch detail .','');
						return 0;
					}
					if(jQuery("#VRCreateDebitAmt").val()==''){
						jQuery("#VRCreateDebitAmt").css("background","#FF9999");
						showMessage('yellow','Enter debit note amount .','');
						jQuery("#VRCreateDebitAmt").focus();
						return 0;
					}
					else{
						jQuery("#VRCreateDebitAmt").css("background","white");
					}
				
					if(isNaN(jQuery("#VRCreateDebitAmt").val())==true||parseFloat(jQuery("#VRCreateDebitAmt").val(),10).toFixed(0)<=0){
						jQuery("#VRCreateDebitAmt").css("background","#FF9999");
						showMessage('yellow','Enter Valid Debit Note amount .','');
						jQuery("#VRCreateDebitAmt").focus();
						return 0;
					}
					else{
						jQuery("#VRCreateDebitAmt").css("background","white");
					}
					
					if(jQuery("#VrShipDetail").val()==''){
						
						jQuery("#VrShipDetail").css("background","#FF9999");
						showMessage('yellow','Enter Transporter Name .','');
						jQuery("#VrShipDetail").focus();
						return 0;
					}
						else{
							jQuery("#VrShipDetail").css("background","white");
						}
					
					
					if(jQuery("#VrRemarks").val()==''){
						
						jQuery("#VrRemarks").css("background","#FF9999");
						showMessage('yellow','Enter Remarks .','');
						jQuery("#VrRemarks").focus();
						return 0;
					}
						else{
							jQuery("#VrRemarks").css("background","white");
						}
					
					if(jQuery("#NoOfBoxes").val()==''){
						
						jQuery("#NoOfBoxes").css("background","#FF9999");
						showMessage('yellow','Enter No Of Boxes .','');
						jQuery("#NoOfBoxes").focus();
						return 0;
					}
						else{
							jQuery("#NoOfBoxes").css("background","white");
						}
					if(isNaN(jQuery("#NoOfBoxes").val())==true||parseFloat(jQuery("#NoOfBoxes").val(),10).toFixed(0)<=0){
						jQuery("#NoOfBoxes").css("background","#FF9999");
						showMessage('yellow','Enter Valid No Of Boxes .','');
						jQuery("#NoOfBoxes").focus();
						return 0;
					}
					else{
						jQuery("#NoOfBoxes").css("background","white");
					}
					if(isNaN(jQuery("#ProductWeight").val())==true||parseFloat(jQuery("#ProductWeight").val(),10).toFixed(0)<0){
						jQuery("#ProductWeight").css("background","#FF9999");
						showMessage('yellow','Enter Valid gross weight .','');
						jQuery("#ProductWeight").focus();
						return 0;
					}
					else{
						jQuery("#ProductWeight").css("background","white");
					}
		    	
					var rowId=jQuery("#GRNBatchDetailsGrid").getGridParam('selrow');
					saveditableRows();
/*					if(jQuery("tr#"+rowId).attr("editable")==1 ){
						showAllRowsONSelectItemCode();
						
						
						showMessage('yellow','Save Editable Row from Batch Detail List. Use Save in Row.','');
						return 0;
					}*/
					
					
					/*if(validationForReturnQty()==0)
						{
						return 0;
						
						}*/
/*					if(	vallidateReturnQtyONSave()==0){
						return 0;
					}*/

					
					
					return 1;
		}
		
jQuery(".HandlingCharge").keyup(function(e) {
	 var Cellid = this.id;
	 var charges = jQuery("#"+Cellid).val();
	 var intRegex =/^[+]?[\d.]+$/g;//  /^[\d]+$/;	
		

			if(  !intRegex.test(charges)){
		
				jQuery("#"+Cellid).val('');
				showMessage('yellow','Enter valid handling charge.','');
				updateDebitAmount();
			}
			
			
			else{					
				updateDebitAmount();			
				}
			
});

function updateDebitAmount()
{	
	HandlingCharge=jQuery("#HandlingCharge").val();
	TotalAmount=jQuery("#VrTotalAmount").val();
	if(HandlingCharge==''||isNaN(HandlingCharge)==true)
	{
		HandlingCharge=0;
	}
	if(TotalAmount==''||isNaN(TotalAmount)==true)
	{
			TotalAmount=0;
	}
	
	jQuery("#VRCreateDebitAmt").val(formatFloatNumbers(parseFloat(TotalAmount)+parseFloat(HandlingCharge)));
}
		

		
	jQuery("#btnRET01Save").unbind("click").click(function(){
		jQuery("#actionName").val('Save');
		
			
		if(validationOnSaveButton()==1){
			var batchDetailsGrid = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
			if(batchDetailsGrid.length==0){
				showMessage('yellow','Select atleast one Item in Batch List','');
				return;
			}
			if(validationForExcise()==0)
			{
			return;
			}
			batchDetailsGrid = JSON.stringify(getJsonFromBatchGrid());
			jsonForViewDetail=JSON.stringify(getJsonFromBatchGrid());
			
				var r =confirm("Are you sure you want to save data ?");
	if(r==true)
		{
		  showMessage("loading", "Saving Vendor Return Details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VendorReturnCallBack',
				data:
				{
					id : 'saveVR',
					jsonForBatchDetail:batchDetailsGrid,
					jsonForViewDetail:jsonForViewDetail,
					GRNNumber:jQuery("#VrCreateGRNNumber").val(),
					VRNumber:jQuery("#VrReturnNo").val(),
					Status:1,
					PONo:jQuery("#PONo").val(),
					ShippingDetail:jQuery("#VrShipDetail").val(),
					ShipmentDate:getDate(jQuery("#VRShipDate").val()),
					Remarks:jQuery("#VrRemarks").val(),
					TotalQuantity:jQuery("#VrCreateTotalQty").val(),
					TotalAmount:formatFloatNumbers(jQuery("#VrTotalAmount").val()),
					HandlingCharge:jQuery("#HandlingCharge").val(),
					DebitAmount:jQuery("#VRCreateDebitAmt").val(),
					receiveBy:jQuery("#GRNViewReceivedBy").val(),
					VendorCode:jQuery("#GRNCreateVendorCode").val(),
					NoOfBoxes:parseInt(jQuery("#NoOfBoxes").val()),
					GrossWeight:jQuery("#ProductWeight").val(),
					moduleCode:"RET01",
					functionName:jQuery("#actionName").val()
				
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var keypairData = jQuery.parseJSON(data);
				if(keypairData.Status==1){
					keypair=keypairData.Result;
					if(keypair.length>=0){
						
					jQuery.each(keypair,function(key,value)
					{	
						
						if(keypair[0]['ReturnNo']!=undefined){
						
							showMessage("green","Record Created Successfully "+keypair[key]['ReturnNo'],"");
							jQuery("#VrReturnNo").val(keypair[key]['ReturnNo']);
							jQuery("#HandlingCharge").val(formatFloatNumbers(keypair[key]['HandlingCharge']));
							jQuery("#VrStatus").val('Created');
							jQuery("#VrShipDetail").val(keypair[key]['ShippingDetails']);
							jQuery("#VRShipDate").val(displayDate(keypair[key]['retVendorDate']));
							jQuery("#VRCreateDebitAmt").val(formatFloatNumbers(keypair[key]['DebitNoteAmount']));
							jQuery("#VrCreateTotalQty").val(parseInt(keypair[key]['RetQuantity']));
							jQuery("#VrTotalAmount").val(formatFloatNumbers(keypair[key]['TotalAmount']));
							jQuery("#VrRemarks").val(keypair[key]['Remarks']);
							jQuery("#NoOfBoxes").val(keypair[key]['NoOfBoxes']);
							jQuery("#ProductWeight").val(parseFloat(keypair[key]['GrossWeight']));
							disableButtons(1);
							//searchVR();
						}
						else
							  {
							  if(keypair[key]['OutParam']=='INF0066')
							      {
								  showMessage("yellow","All Previous GRN should be closed to Save GRN changes.","");
							      }
							  else
							      {
								  showMessage("yellow","Unable to Save GRN Details. Error " + keypairData,"");
							      }
							
							}
					
						  /*enableDisableOnStatus(1);*/
						
					});
				 }
				else{
					showMessage("","VR Details not Saved. Error " + keypairData,"");
				}
			    
				}
				else{
					showMessage('red',keypairData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
				});
			}
		
		   }
			else{
				return ;
			}
		
			
		});
	jQuery("#btnRET01Cancel").unbind("click").click(function(){
		jQuery("#actionName").val('Cancel');
		if(validationOnSaveButton()==1){
		var batchDetailsGrid = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
		batchDetailsGrid = JSON.stringify(getJsonFromBatchGrid());
		jsonForViewDetail=JSON.stringify(getJsonFromBatchGrid());
			var r =confirm("Are you sure you want to cancel data ?");
			if(r==true)
			{
			  showMessage("loading", "cancelling return...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'VendorReturnCallBack',
					data:
					{
						id : 'saveVR',
						jsonForBatchDetail:batchDetailsGrid,
						jsonForViewDetail:jsonForViewDetail,
						GRNNumber:jQuery("#VrCreateGRNNumber").val(),
						VRNumber:jQuery("#VrReturnNo").val(),
						Status:2,
						PONo:jQuery("#PONo").val(),
						ShippingDetail:jQuery("#VrShipDetail").val(),
						ShipmentDate:getDate(jQuery("#VRShipDate").val()),
						Remarks:jQuery("#VrRemarks").val(),
						TotalQuantity:jQuery("#VrCreateTotalQty").val(),
						TotalAmount:jQuery("#VrTotalAmount").val(),
						HandlingCharge:jQuery("#HandlingCharge").val(),
						DebitAmount:jQuery("#VRCreateDebitAmt").val(),
						receiveBy:jQuery("#GRNViewReceivedBy").val(),
						TransporterName:jQuery("#GRNViewTransporterName").val(),
						VendorCode:jQuery("#GRNCreateVendorCode").val(),
						NoOfBoxes:parseInt(jQuery("#NoOfBoxes").val()),
						GrossWeight:jQuery("#ProductWeight").val(),
						moduleCode:"RET01",
						functionName:jQuery("#actionName").val()
					
					},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var keypairData = jQuery.parseJSON(data);
			if(keypairData.Status==1){
				keypair=keypairData.Result;
				if(keypair.length>=0){
					jQuery.each(keypair,function(key,value)
							{	
								
								if(keypair[0]['ReturnNo']!=undefined){
								
									showMessage("green","Vendor Return Closed Successfully "+keypair[key]['ReturnNo'],"");
									jQuery("#VrReturnNo").val(keypair[key]['ReturnNo']);
									jQuery("#HandlingCharge").val(formatFloatNumbers(keypair[key]['HandlingCharge']));
									jQuery("#VrStatus").val('Cancelled');
									jQuery("#VrShipDetail").val(keypair[key]['ShippingDetails']);
									jQuery("#VRShipDate").val(displayDate(keypair[key]['retVendorDate']));
									jQuery("#VRCreateDebitAmt").val(formatFloatNumbers(keypair[key]['DebitNoteAmount']));
									jQuery("#VrCreateTotalQty").val(parseInt(keypair[key]['RetQuantity']));
									jQuery("#VrTotalAmount").val(formatFloatNumbers(keypair[key]['TotalAmount']));
									jQuery("#VrRemarks").val(keypair[key]['Remarks']);
									jQuery("#NoOfBoxes").val(keypair[key]['NoOfBoxes']);
									jQuery("#ProductWeight").val(parseFloat(keypair[key]['GrossWeight']));
									disableButtons(2);
									disableBatchGridTextBox();
								}
								else
									  {
									  if(keypair[key]['OutParam']=='INF090')
									      {
										  showMessage("yellow","This Return number already closed.","");
									      }
									  else
									      {
										  showMessage("yellow","This VendorReturn can not be modified. Error " + keypairData,"");
									      }
									
									}
							
								  /*enableDisableOnStatus(1);*/
								
							});	
				jQuery.each(keypair,function(key,value)
				{	
					showMessage("green","Record Cancelled Successfully "+keypair[key]['ReturnNo'],"");
					jQuery("#VrReturnNo").val(keypair[key]['ReturnNo']);
					jQuery("#VrStatus").val('Cancelled');
					  /*enableDisableOnStatus(1);*/
					disableButtons(2);
				
				});
			 }
				else{
					showMessage("","GRN Not Cancelled. Error " + keypairData,"");
				}
		    
			}
			else{
				showMessage('red',keypairData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
			});
		}
	
	   }
		else{
			return ;
		}
	
		
	});
		
	
	jQuery("#btnRET01Close").unbind("click").click(function(){
		jQuery("#actionName").val('Close');
		if(validationOnSaveButton()==1)
		{
			
			
		var batchDetailsGrid = jQuery("#GRNBatchDetailsGrid").jqGrid('getRowData');
		batchDetailsGrid = JSON.stringify(getJsonFromBatchGrid());
		jsonForViewDetail=JSON.stringify(getJsonFromBatchGrid());
		if(batchDetailsGrid.length==0){
			showMessage('yellow','Select at least one item in Batch List','');
			return;
		}
		if(validationForExcise()==0)
		{
		return;
		}
			var r =confirm("Are you sure you want to confirm  data ?");
				if(r==true)
					{
					  showMessage("loading", "Confirming Return...", "");
						jQuery.ajax({
							type:'POST',
							url:Drupal.settings.basePath + 'VendorReturnCallBack',
							data:
							{
								id : 'saveVR',
								jsonForBatchDetail:batchDetailsGrid,
								jsonForViewDetail:jsonForViewDetail,
								GRNNumber:jQuery("#VrCreateGRNNumber").val(),
								VRNumber:jQuery("#VrReturnNo").val(),
								Status:4,
								PONo:jQuery("#PONo").val(),
								ShippingDetail:jQuery("#VrShipDetail").val(),
								ShipmentDate:getDate(jQuery("#VRShipDate").val()),
								Remarks:jQuery("#VrRemarks").val(),
								TotalQuantity:jQuery("#VrCreateTotalQty").val(),
								TotalAmount:jQuery("#VrTotalAmount").val(),
								HandlingCharge:jQuery("#HandlingCharge").val(),
								DebitAmount:jQuery("#VRCreateDebitAmt").val(),
								receiveBy:jQuery("#GRNViewReceivedBy").val(),
								TransporterName:jQuery("#GRNViewTransporterName").val(),
								VendorCode:jQuery("#GRNCreateVendorCode").val(),
								NoOfBoxes:parseInt(jQuery("#NoOfBoxes").val()),
								GrossWeight:jQuery("#ProductWeight").val(),
								moduleCode:"RET01",
								functionName:jQuery("#actionName").val()
							
							},
							success:function(data)
							{
								jQuery(".ajaxLoading").hide();
							var keypairData = jQuery.parseJSON(data);
							if(keypairData.Status==1){
								keypair=keypairData.Result;
								if(keypair.length>=0){
									
						
								jQuery.each(keypair,function(key,value)
										{	
											
											if(keypair[0]['ReturnNo']!=undefined){
											
												showMessage("green","Vendor Return Closed Successfully "+keypair[key]['ReturnNo'],"");
												jQuery("#VrReturnNo").val(keypair[key]['ReturnNo']);
												jQuery("#HandlingCharge").val(formatFloatNumbers(keypair[key]['HandlingCharge']));
												jQuery("#VrStatus").val('Approved');
												jQuery("#VrShipDetail").val(keypair[key]['ShippingDetails']);
												jQuery("#VRShipDate").val(displayDate(keypair[key]['retVendorDate']));
												jQuery("#VRCreateDebitAmt").val(formatFloatNumbers(keypair[key]['DebitNoteAmount']));
												jQuery("#VrCreateTotalQty").val(parseInt(keypair[key]['RetQuantity']));
												jQuery("#VrTotalAmount").val(formatFloatNumbers(keypair[key]['TotalAmount']));
												jQuery("#VrRemarks").val(keypair[key]['Remarks']);
												jQuery("#NoOfBoxes").val(keypair[key]['NoOfBoxes']);
												jQuery("#ProductWeight").val(parseFloat(keypair[key]['GrossWeight']));
												disableButtons(4);
												disableBatchGridTextBox();
											}
											else
												  {
												  if(keypair[key]['OutParam']=='INF090')
												      {
													  showMessage("yellow","This Return Number already closed.","");
												      }
												  else
												      {
													  showMessage("yellow","This VR cannt be modified. Error " + keypairData,"");
												      }
												
												}
										
											  /*enableDisableOnStatus(1);*/
											
										});
							 }
								else{
									showMessage("yellow","VR Not Closed. Error " + keypairDate,"");
								}
						    
							}
							else{
								showMessage('red',keypairData.Description,'');
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
							});
					}
	
	   }
		else{
			return ;
		}
	
		
	});
    function disableBatchGridTextBox()
    {		
	 	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	 	var returnStatus=jQuery("#VrStatus").val();
	 	if(returnStatus=='Approved'||returnStatus=='Cancelled'){
    	for(var index=0;index<allRowIds.length;index++){
    		var textBoxId=removeSpecialCharacter(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', allRowIds[index], 'BatchNo'));
    		jQuery("#"+textBoxId).attr('disabled',true);		    	
    	}
	 	}
    	
    	
    }
    jQuery("#btnRET01Print").unbind("click").click(function(){
		jQuery("#actionName").val('Print');
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'VendorReturnCallBack',
			data:
			{
				id:"printVR",
				ReturnNo:jQuery("#VrReturnNo").val(),
				GRNNo:jQuery("#VrCreateGRNNumber").val(),
				functionName:jQuery("#actionName").val(),
				moduleCode:"RET01"
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				 jQuery.each(results,function(key,value)
						   {
					 			var arr = {'ReturnNo':results[key]['ReturnNo'],'GRNNo':results[key]['GRNNo'],'locationId':results[key]['locationId'],'GeneratedBy':results[key]['GeneratedBy']};
					 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
							 
					      });
			}
			else{
				
				showMessage('red',resultsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});


	
 });

		 jQuery("#btnReset").click(function(){
			 disableFieldOnStartup();
			 enableFieldOnStartup();
			 jQuery("#GRNBatchDetailsGrid").jqGrid('clearGridData');
				jQuery("#GRNViewGrid").jqGrid("clearGridData");
				jQuery("#VrCreateGRNNumber").val('');
				jQuery("#VrShipDetail").val('');
				jQuery("#VrTotalAmount").val('');
				jQuery("#VrRemarks").val('');
				jQuery("#NoOfBoxes").val('');
				jQuery("#ProductWeight").val('');
				jQuery("#PONo").val('');
				jQuery("#PODate").val('');
				disableButtons(0);
				jQuery("#VrCreateGRNNumber").css("background","white");
				jQuery("#GRNCreateChallanNo").css("background","white");
				jQuery("#GRNCreateInvoiceNo").css("background","white");
				jQuery("#GRNCreateInvoiceDate").css("background","white");
				jQuery("#GRNCreateChallanDate").css("background","white");
				showCalender();
		 });
		 
		 function disableButtons(statusId){
				     //onStartup
				 if(statusId==0){
						jQuery("#btnRET01Print").attr('disabled',true);	
						jQuery("#btnRET01Cancel").attr('disabled',true);	
						jQuery("#btnRET01Close").attr('disabled',false);	
						jQuery("#btnRET01Save").attr('disabled',false);
						jQuery("#btnRET01Search").attr('disabled',false);
						jQuery("#GRNBatchDetailsGrid").showCol('myac');
						jQuery("#GRNViewGrid").showCol('AddBatches');
						disableEnableFieldOnStatus(0);
						
					}
				//CREATED
				if(statusId==1){
					
					jQuery("#btnRET01Print").attr('disabled',true);	
					jQuery("#btnRET01Cancel").attr('disabled',false);	
					jQuery("#btnRET01Close").attr('disabled',false);	
					jQuery("#btnRET01Save").attr('disabled',false);
					jQuery("#btnRET01Search").attr('disabled',false);
					jQuery("#GRNBatchDetailsGrid").showCol('myac');
					jQuery("#GRNViewGrid").showCol('AddBatches');
					
					disableEnableFieldOnStatus(4);
				}
				//cancel
				else if(statusId==2){
					jQuery("#btnRET01Print").attr('disabled',true);	
					jQuery("#btnRET01Cancel").attr('disabled',true);	
					jQuery("#btnRET01Close").attr('disabled',true);	
					jQuery("#btnRET01Save").attr('disabled',true);
					jQuery("#btnRET01Search").attr('disabled',false);
					jQuery("#GRNBatchDetailsGrid").hideCol('myac');
					jQuery("#GRNViewGrid").hideCol('AddBatches');
					//disableBatchGridTextBox();
					disableEnableFieldOnStatus(1);
				}
				//closing
				else if(statusId==4){
					jQuery("#btnRET01Print").attr('disabled',false);	
					jQuery("#btnRET01Cancel").attr('disabled',true);	
					jQuery("#btnRET01Close").attr('disabled',true);	
					jQuery("#btnRET01Save").attr('disabled',true);
					jQuery("#btnRET01Search").attr('disabled',false);	
					jQuery("#GRNBatchDetailsGrid").hideCol('myac');
					jQuery("#GRNViewGrid").hideCol('AddBatches');
					//disableBatchGridTextBox();
					disableEnableFieldOnStatus(1);
				}
				//actionButtonsStauts();
				
				
			}
	function disableEnableFieldOnStatus(IsDisable){
	if(IsDisable==1)
	{
		jQuery("#GRNCreateInvoiceDate").attr('disabled',true);		
		jQuery("#VrCreateGRNNumber").attr('disabled',true);		
		jQuery("#VRCreateDebitNo").attr('disabled',true);	
		jQuery("#VRCreateDebitAmt").attr('disabled',true);				
		jQuery("#VrRemarks").attr('disabled',true);
		jQuery("#HandlingCharge").attr('disabled',true);
		jQuery("#NoOfBoxes").attr('disabled',true);
		jQuery("#ProductWeight").attr('disabled',true);
		jQuery("#VrShipDetail").attr('disabled',true);	
		jQuery(".CLaimQuantity").attr('disabled',true);	
		
	}	
	else if(IsDisable==4)
	{
		
		jQuery(".CLaimQuantity").attr('disabled',false);	
		jQuery("#VrCreateGRNNumber").attr('disabled',true);		
		jQuery("#VRCreateDebitAmt").attr('disabled',true);		
		jQuery("#VrRemarks").attr('disabled',false);
		jQuery("#HandlingCharge").attr('disabled',false);
		jQuery("#NoOfBoxes").attr('disabled',false);
		jQuery("#ProductWeight").attr('disabled',false);
		jQuery("#VrShipDetail").attr('disabled',false);	
	}
	else{
		
		jQuery(".CLaimQuantity").attr('disabled',false);	
		jQuery("#VrCreateGRNNumber").attr('disabled',false);		
		jQuery("#VRCreateDebitAmt").attr('disabled',true);		
		jQuery("#VrRemarks").attr('disabled',false);
		jQuery("#HandlingCharge").attr('disabled',false);
		jQuery("#NoOfBoxes").attr('disabled',false);
		jQuery("#ProductWeight").attr('disabled',false);
		jQuery("#VrShipDetail").attr('disabled',false);	
	}
		
	}		
});		
