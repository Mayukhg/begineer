var  jsonForLocations='';
var loggedInUserPrivileges;
var sourceId=0;
var destinationId=0;
var arrayOfSelectedRowFromTOIItem ;
var currentBatchNo ;
var exactCalculationFlag=1;
var ItemCodeArray=new Array();
var JsonFromArray ;
var gridrowId=1000;
var batchnoForMultipleSameMfgNo  ;
 
var itemWeigth ;
jQuery(document).ready(function(){


	
	
	showCalender();
	showCalender2();
	jQuery("#main-menu").hide();

	jQuery(".breadcrumb").hide();

	jQuery("#triptych-wrapper").hide();

	

	jQuery("#footer-wrapper").hide();

	var moduleCode = jQuery("#moduleCode").val();
	
	jQuery(function() {

		jQuery( "#divLookUp" ).dialog({
			autoOpen: false,
			minWidth: 625,
			show: {
				effect: "clip",
				duration: 200
			},
			hide: {
				effect: "clip",
				duration: 200

			}

		});
	});


	jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy', yearRange: '1950:2010',minDate: +0, maxDate: "500D",onSelect:function(){
        
		jQuery(this).focus();
  
       }});

	jQuery(function() {
		//alert("dfadf");
		jQuery( "#divTransferOutTab" ).tabs(); // Create tabs using jquery ui.
	});


	jQuery(".TOINumberSearch").keydown(function(e){

		if(e.which == 115) 
		{
			var currentTOISearchId = this.id;
			forOpenMFGdialogue(currentTOISearchId);
					/*----------Dialog for item search is there ----------------*/


			
		}

	});
	
	
	function forOpenMFGdialogue (currentTOISearchId){
		
	

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"TOILookUp",
			},
			success:function(data)
			{

				//alert(data);
				jQuery("#divLookUp").html(data);

				TOISearchGrid(currentTOISearchId);
				jQuery("#divLookUp" ).dialog( "open" );

				TOILookUpData();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});

	}

	getCurrentLocation();

	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
		data:
		{
			id:"StockCountModuleFuncHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

			if(loggedInUserPrivilegesForStockCountData.Status == 1)
			{

				loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

				if(loggedInUserPrivileges.length == 0)
				{
					jQuery(".TOCreateActionButtons").attr('disabled',true);
				}
				else
				{
					callOnStarup();
				}


			}
			else
			{
				showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
			}

		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}

	});
	function actionButtonsStauts()
	{
		jQuery(".TOCreateActionButtons").each(function(){

			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");

			if(loggedInUserPrivileges[extractedButtonId] == 1)
			{

				//jQuery("#"+buttonid).attr('disabled',false);

			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}

			jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.

		});
	}

	jQuery("#btnReset").click(function(){
		jQuery("#TOINumber").val('');
		jQuery("#TONumber").val('');
		jQuery("#TORefNumber").val('');
		//jQuery("#TODestinationLocation").val(-1);
		jQuery("#indentised").val(-1);
		jQuery("#TOStatus").val(-1);
	});
	
	
	function callOnStarup(){
	
		searchWHLocation();
		TOStatus();
		disableField();
		disableItemField();
		EnableDisableOnStatus(0);
		
	}	


	function searchWHLocation()
	{

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"searchWHLocation",
			},
			success:function(data)
			{
				//alert(data);
				var WHLocationData = jQuery.parseJSON(data);

				if(WHLocationData.Status == 1)
				{

					jsonForLocations = WHLocationData.Result;

					var whlocations =  WHLocationData.Result;
					//jsonForLocations=jQuery.parseJSON(data);
					//var whlocations = jQuery.parseJSON(data);
					jQuery.each(whlocations,function(key,value)
							{	
						jQuery("#TOSourceLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#TODestinationLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");

						jQuery("#createSourceLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#createDestinationLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#createSourceLocation").find("option[value='-1']").remove();
						jQuery("#createDestinationLocation").find("option[value='-1']").remove();


							});

				}
				else
				{
					showMessage("red", WHLocationData.Description, "");
				}
				jQuery('#TOSourceLocation').val(currentLocationId);
				jQuery('#TOSourceLocation').attr('disabled',true);

				backGroundColorForDisableField();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}


		});

	}

	function TOStatus()
	{

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"TOStatus",
			},
			success:function(data)
			{

				var toiData = jQuery.parseJSON(data);
				if(toiData.Status==1){


					var toi =  toiData.Result;

					jQuery.each(toi,function(key,value)
							{	

						jQuery("#TOStatus").append("<option id=\""+toi[key]['keycode1']+"\""+" value=\""+toi[key]['keycode1']+"\""+">"+toi[key]['keyvalue1']+"</option>");
							});
				}
				else

				{
					showMessage("red", toiData.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}






	jQuery("#btnTSF02Search").unbind("click").click(function(){
		jQuery("#actionName").val('Search');
		showMessage("loading", "Searching transfer outs...", "");
		jQuery("#fromTODate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#toTODate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#fromTOShipDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery("#toTOShipDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		jQuery('#TOSourceLocation').attr('disabled',false);
		var searchFormData = jQuery("#formSearchTO").serialize();
			 jQuery("#fromTODate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery("#toTODate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery("#fromTOShipDate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery("#toTOShipDate").datepicker("option", "dateFormat", "d-M-yy");
			 jQuery('#TOSourceLocation').attr('disabled',true);

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"searchTO",
				functionName:jQuery("#actionName").val(),
				moduleCode:"TSF02",
				TOFormData:searchFormData,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var searchToData=jQuery.parseJSON(data);
				if(searchToData.Status==1){
					var rowid=0;
					var searchTO = searchToData.Result ;
					if(searchTO.length==0)
					{
						jQuery("#TOSearchGrid").jqGrid("clearGridData");
						showMessage("yellow","No Record Found");

					}
					else
					{	


						jQuery("#TOSearchGrid").jqGrid("clearGridData");
						jQuery.each(searchTO,function(key,value){			
							var newData = [{"Edit":'',
								"TONumber":searchTO[key]['TONumber'],
								"TOINumber":searchTO[key]['TOINumber'],
								"TOCreationDate":searchTO[key]['TOCreationDate'],
								"SourceAddress":searchTO[key]['SourceAddress'],
								"DestinationAddress":searchTO[key]['DestinationAddress'],
								"TOShipDate":searchTO[key]['ShipDate'],
								"Quantity":parseFloat(searchTO[key]['TotalTOQuantity'],10).toFixed(2),
								"TotalTOIAmount":parseFloat(searchTO[key]['TotalTOAmount'],10).toFixed(2),
								"Status":searchTO[key]['StatusName'],"PackSize":searchTO[key]['PackSize'],"SourceLocationId":searchTO[key]['SourceLocationId'],
								"ExpectedDeliveryDate":searchTO[key]['ExpectedDeliveryDate'],"Remarks":searchTO[key]['Remarks'],"ShippingDetails":searchTO[key]['ShippingDetails'],
								"DestinationLocationId":searchTO[key]['DestinationLocationId'],"StatusName":searchTO[key]['StatusName'],"ShippingWayBillNo":searchTO[key]['ShippingWayBillNo'],
								"RefNumber":searchTO[key]['RefNumber'],"GrossWeight":searchTO[key]['GrossWeight'],"ShipDate":searchTO[key]['ShipDate'],
								"StateId":searchTO[key]['StateId'],"SourcePhone":searchTO[key]['SourcePhone'],"ModifiedByName":searchTO[key]['ModifiedByName'],
								"EmailId1":searchTO[key]['EmailId1'],"SourceCity":searchTO[key]['SourceCity'],"DestinationCity":searchTO[key]['DestinationCity'],
								"IECCode":searchTO[key]['IECCode'],"ExporterRef":searchTO[key]['ExporterRef'],"OtherRef":searchTO[key]['OtherRef'],
								"BuyerOtherthanConsignee":searchTO[key]['BuyerOtherthanConsignee'],"PreCarriage":searchTO[key]['PreCarriage'],"PlaceofReceiptbyPreCarrier":searchTO[key]['PlaceofReceiptbyPreCarrier'],
								"CountryOfOrigin":searchTO[key]['CountryOfOrigin'],"CountryOfDestination":searchTO[key]['CountryOfDestination'],"VesselflightNo":searchTO[key]['VesselflightNo'],
								"PortofLoading":searchTO[key]['PortofLoading'],"PortofDischarge":searchTO[key]['PortofDischarge'],"PortofDestination":searchTO[key]['PortofDestination'],
								"BuyerOrderNo":searchTO[key]['BuyerOrderNo'],"BuyerOrderDate":searchTO[key]['BuyerOrderDate'],"TermsofDelivery":searchTO[key]['TermsofDelivery'],
								"DELIVERY":searchTO[key]['DELIVERY'],"PAYMENT":searchTO[key]['PAYMENT']

							}];

							for (var i=0;i<newData.length;i++) {
								jQuery("#TOSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
								rowid=rowid+1;
							}
						});

					}

				}	
				else
				{

					showMessage('red', searchToData.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});

	});
	jQuery("#TOCreateTOINumber").keydown(function(e){

		if(e.which==9 || e.which == 13){
			e.preventDefault(); 
			var TOINumber = jQuery("#TOCreateTOINumber").val();
	      if(jQuery("#TOCreateTOINumber").val()!=''){
				searchItemUsingCode(TOINumber);   
	      }

		}

	});
	
	
	
	function 	searchItemUsingCode (TOINumber){
		var statusOfTOI =-1;
		showMessage("loading", "Searching transfer out instructions...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"searchTOI",
				TOINumber:TOINumber

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				searchTOIData=jQuery.parseJSON(data);
				if(searchTOIData.Status==1)
				{

					var searchTOI = searchTOIData.Result ;
					
					if( searchTOI.length>0){

						jQuery.each(searchTOI,function(key,value){
							jQuery("#TOCreateTOINumber").attr('disabled',true);
							sourceId=searchTOI[key]['SourceLocationId'] ;
							destinationId=searchTOI[key]['DestinationLocationId'];
							jQuery("#TOCreateTOISourceLocation").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',searchTOI[key]['SourceLocationId'],'DisplayName'));
							jQuery("#TOCreateTOIDestinationLocation").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',searchTOI[key]['DestinationLocationId'],'DisplayName'));
							jQuery("#TOCreateTOSourceLocation").val(searchTOI[key]['SourceAddress']);
							jQuery("#TOCreateDestinationAddress").val(searchTOI[key]['DestinationAddress']);
							jQuery("#TOCreateTotalTOQuantity").val((parseFloat(searchTOI[key]['TotalTOIQuantity'],10).toFixed(2)));
							jQuery("#TOCreateGrossWeight").val((parseFloat(searchTOI[key]['GrossWeight'],10).toFixed(2)));
							jQuery("#TOCreateTotalTOAmount").val(parseFloat(searchTOI[key]['TotalTOIAmount'],10).toFixed(2));
							statusOfTOI=	 searchTOI[key]['Status'];
						});
						if(statusOfTOI==2){
							jQuery("#TOCreateTOINumber").attr('disabled',true);
							ShowControlTaxInfor(sourceId,'s');
							ShowControlTaxInfor(destinationId,'d');
							adjustItemAndBatch(sourceId,TOINumber);
						
							jQuery("#TOCreateTOINumber").css("background","white");
							
							jQuery("#TOCreateExpectedDeliveryDate").focus();
							jQuery("#TOCreateRequestedQty").css("background","#00FF00");
						}
						else {
							showMessage ("yellow","Transfer Instructions Number should be confirmed ","");
							jQuery("#TOCreateTOINumber").focus();
							jQuery("#TOCreateTOINumber").select();
		
						}

					}
					else{
						jQuery("#TOCreateTOINumber").css("background","#FF9999");
						showMessage ("yellow","Enter valid Transfer Instructions Number.","");
						jQuery("#TOCreateTOINumber").focus();
						jQuery("#TOCreateTOINumber").select();
					}
				}

				else

				{

					showMessage("red", toiData.Description, "");
					jQuery("#TOCreateTOINumber").focus();
					jQuery("#TOCreateTOINumber").select();

				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}


		});	


	}
	
	function getjsonFromArray(array){
		JsonFromArray={};
			for(var i=0 ;i<array.length;i++){
				JsonFromArray[array[i].Items]=array[i].RequestQty  ;
			}
			
	}
	
	function ShowControlTaxInfor(locationId,locationType){
		showMessage("loading", "Searching tax related information...", "");

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"ShowControlTaxInfor",
				locationId:locationId

			},
			success:function(data)
			{

				jQuery(".ajaxLoading").hide();
				var TaxData =jQuery.parseJSON(data);
				if(TaxData.Status==1){

					var keypair = TaxData.Result ;
					if(keypair.length>0){
						jQuery.each(keypair,function(key,value)
								{
							if(locationType=='s'){
								jQuery("#TOCreateSourceCSTNo").val(keypair[key]['cstNO']);
								jQuery("#TOCreateSourceVATNo").val(keypair[key]['VatNo']);
								jQuery("#TOCreateSourceTINNo").val(keypair[key]['tinNo']);
								jQuery("#TOCreateSourceTINNo").val(keypair[key]['tinNo']);
							}
							else {
								jQuery("#TOCreateDestinationCSTNo").val(keypair[key]['cstNO']);
								jQuery("#TOCreateDestinationVATNo").val(keypair[key]['VatNo']);
								jQuery("#TOCreateDestinationTinNo").val(keypair[key]['tinNo']);
								jQuery("#TOCreateDestinationCSTNo").val(keypair[key]['cstNO']);
							}
								});
					}
				}
				else
				{
					showMessage("red", TaxData.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});	
	}
	function adjustItemAndBatch(sourceId,TOINumber){
		showMessage("loading", "Searching for adjustment of item and batch...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"adjustItemAndBatch1",
				sourceId:sourceId,
				TOINumber:TOINumber

			},
			success:function(data)
			{
				var isItemCodeInsertOrNot =0;
				jQuery(".ajaxLoading").hide();
				itemsJsonData = jQuery.parseJSON(data);
				if(itemsJsonData.Status==1){

					itemsJson = itemsJsonData.Result ;
					jQuery("#TOItemGrid").jqGrid("clearGridData");
					var rowId=0;
					if(itemsJson.length>0){
						jQuery("#TOCreateTOINumber").attr('disabled',true);
					
							jQuery.each(itemsJson,function(key,value)
									{
								/*					['Items','Item Name','Unit Price','Bucket Name','Mfg Batch No','Adjust','Total Amount','BatchNo']*/		
						if(itemsJson[key]['OutParam']==undefined) {	
													var ItemCode = itemsJson[key]['ItemCode']	;
													var RequestQty=parseFloat(itemsJson[key]['RequestQty'],10).toFixed(0) ;
						
													var ItemId=itemsJson[key]['ItemId']	;
													var ItemDescription = itemsJson[key]['ItemDescription'];    
													var UOMId = itemsJson[key]['UOMId']  ;
													var UOMName = itemsJson[key]['UOMName']  ;
													var UnitPrice =parseFloat( itemsJson[key]['TransferPrice'],10).toFixed(2);    
													var Bucketid = itemsJson[key]['BucketId'];
													var Bucket=itemsJson[key]['BucketName'];
													var AvailableQty =parseFloat( itemsJson[key]['AvailableQty'],10).toFixed(0);     
													var AfterAdjustQt =parseFloat( itemsJson[key]['AfterAdjustQty'],10).toFixed(0);           
													var TotalAmount =parseFloat( itemsJson[key]['TotalAmount'],10).toFixed(2); 
													var TOINumber =itemsJson[key]['TOINumber'] ; 
													var rowNo =itemsJson[key]['RowNo'] ;
													
													var BatchNo =itemsJson[key]['BatchNo'] ; 
					
													var ManufactureBatchNo =itemsJson[key]['ManufactureBatchNo'] ;
													var MRP=itemsJson[key]['MRP'] ;
					
													var Weight =itemsJson[key]['Weight'] ;
													var EachCartonQty=itemsJson[key]['EachCartonQty'] ;
					
													var ItemPackSize =itemsJson[key]['ItemPackSize'] ;
													var ItemDesc=itemsJson[key]['ItemDesc'] ;
					
													var DateFormatDesc =itemsJson[key]['DateFormatDesc'] ;
													var ExpDuration=itemsJson[key]['ExpDuration'] ;
													var MerchHierarchyDetailId=itemsJson[key]['MerchHierarchyDetailId'] ;
													/*if(rowId==0){
														ItemCodeArray[rowId] = {"ItemCode":ItemCode,"RequestQty":RequestQty};
													}
													for (var index=0 ;index<ItemCodeArray.length;index++){
														if(ItemCodeArray[index].Items==ItemCode){
															//do nothing
															isItemCodeInsertOrNot=1;
															break ;
														}
														
													}
													if(isItemCodeInsertOrNot!=1 && rowId!=0 ){
														ItemCodeArray[ItemCodeArray.length+1] = {"Items":ItemCode,"RequestQty":RequestQty};
													}*/
													ItemCodeArray[rowId] = {"Items":ItemCode,"ItemId":itemsJson[key]['ItemId'],"ItemName":ItemDescription,"UnitPrice":UnitPrice,
															"BucketName":Bucket,"MfgBatchNo":ManufactureBatchNo,
															"Adjust":AfterAdjustQt,"TotalAmount":TotalAmount
															,"BatchNo":BatchNo,"TOINumber":TOINumber,
															"rowNo":rowNo,"RequestQty":RequestQty,"TOINo":TOINumber,"ManufactureBatchNo":ManufactureBatchNo,"MRP":MRP,
															"Weight":Weight,"EachCartonQty":EachCartonQty,"ItemPackSize":ItemPackSize,
															"ItemDesc":ItemDesc,"DateFormatDesc":DateFormatDesc,"ExpDuration":ExpDuration,"MerchHierarchyDetailId":MerchHierarchyDetailId,
															"MfgDate":itemsJson[key]['MfgDate'],"ExpDate":itemsJson[key]['ExpDate'],
															"BucketId":itemsJson[key]['BucketId'],"UOMId":itemsJson[key]['UOMId']
														};
													
													
													var newData = [{"Items":ItemCode,"ItemId":itemsJson[key]['ItemId'],"ItemName":ItemDescription,"UnitPrice":UnitPrice,
														"BucketName":Bucket,"MfgBatchNo":'',
														"Adjust":0,"TotalAmount":TotalAmount
														,"BatchNo":'',"TOINumber":TOINumber,
														"rowNo":rowNo,"RequestQty":RequestQty,"TOINo":TOINumber,"ManufactureBatchNo":'',"MRP":MRP,
														"Weight":Weight,"EachCartonQty":EachCartonQty,"ItemPackSize":ItemPackSize,
														"ItemDesc":ItemDesc,"DateFormatDesc":DateFormatDesc,"ExpDuration":ExpDuration,"MerchHierarchyDetailId":MerchHierarchyDetailId,
														"MfgDate":itemsJson[key]['MfgDate'],"ExpDate":itemsJson[key]['ExpDate'],
														"BucketId":itemsJson[key]['BucketId'],"UOMId":itemsJson[key]['UOMId']
													}];
													if(!matchItemCodeInJQGrid(ItemCode)){
														for (var i=0;i<newData.length;i++) {
															jQuery("#TOItemGrid").jqGrid('addRowData',rowId, newData[newData.length-i-1], "first");
															jQuery("#TOItemGrid").jqGrid('setCell',rowId,"MfgBatchNo","", { 'background-color': '#FF9999' });            
															rowId=rowId+1;
														}
													}
													
						}
								

							});
  
							getjsonFromArray(ItemCodeArray);
							populateItemCodeIncombo();
						
					}
					
				}
				else{
					showMessage("red", itemsJsonData.Description, "");

				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}



		});	
		}
	
	jQuery("#clear").click(function(){
		clearItemBox();
	});
	
	function populateItemCodeIncombo(){
		//jQuery("#TOCreateItemCode").remove();
		jQuery("#TOCreateItemCode").empty();
		jQuery("#TOCreateItemCode").append("<option id=\""+0+"\""+" value=\""+0+"\""+">"+'select'+"</option>");
		jQuery.each(JsonFromArray,function(key,value)
				{
	          	jQuery("#TOCreateItemCode").append("<option id=\""+key+"\""+" value=\""+key+"\""+">"+key+"</option>");
				});
		}
	
	function resetItemCombo(){
		jQuery("#TOCreateItemCode").empty();
		jQuery("#TOCreateItemCode").append("<option id=\""+0+"\""+" value=\""+0+"\""+">"+'select'+"</option>");
	}
	
	jQuery("#TOCreateItemCode").change(function(){
		if(jQuery("#TOCreateItemCode").val()!=0){
			getRowJsonFromSelectedItemCode(jQuery("#TOCreateItemCode").val());	
			 jQuery("#TOCreateUOM").val(arrayOfSelectedRowFromTOIItem.UOMId);
       	  jQuery("#TOCreateWeight").val(parseFloat(arrayOfSelectedRowFromTOIItem.Weight,10).toFixed(2));
       	  jQuery("#TOCreateRequestedQty").val(parseInt(arrayOfSelectedRowFromTOIItem.RequestQty));
       	  jQuery("#TOIIteBucketName").val(arrayOfSelectedRowFromTOIItem.BucketName);
       	  jQuery("#TOCreateAvailableQty").val('');
       	  jQuery("#TOCreateTransferQty").val('');
       	  jQuery("#TOCreateTransferPrice").val(arrayOfSelectedRowFromTOIItem.UnitPrice);
       	  jQuery("#TOCreateItemDescription").val(arrayOfSelectedRowFromTOIItem.ItemName);
       	  jQuery("#TOCreateItemCode").val(arrayOfSelectedRowFromTOIItem.Items);
       	  jQuery("#TOCreateItemBatchNo").val('');
       	  jQuery("#TOCreateItemBatchNo").focus();
		}
		else{
			clearItemBox();
		}
	});

  function 	getRowJsonFromSelectedItemCode(ItemCode){
		for(var i=0 ;i<ItemCodeArray.length;i++)
			if(	ItemCodeArray[i].Items==ItemCode){
				arrayOfSelectedRowFromTOIItem=	ItemCodeArray[i];
				return arrayOfSelectedRowFromTOIItem;
			}
	}
	
	
	jQuery("#TOCreateItemBatchNo").keydown(function(e){
	    if((e.which==9 || e.which == 13) && jQuery("#TOCreateItemBatchNo").val()!=''){
	    	e.preventDefault(); 
	    	var SACreateFromBatchNo = jQuery("#TOCreateItemBatchNo").val(); //Batch number grid from TOI entered .
	    	var itemCode= jQuery("#TOCreateItemCode").val();
	    	searchBatchno(batchnoForMultipleSameMfgNo,sourceId,itemCode);     
	    }
	    if(e.which==115 ){
	    	lookupForSearchingBatches();
	    }

	});
	
	jQuery("#TOCreateItemBatchNo").focusout(function(e){
	if(	jQuery("#TOCreateItemBatchNo").val()!=''){
		  jQuery("#TOCreateAvailableQty").val('');
	    	e.preventDefault(); 
	    	var SACreateFromBatchNo = jQuery("#TOCreateItemBatchNo").val();
	    	var itemCode= jQuery("#TOCreateItemCode").val();
	    	searchBatchno(batchnoForMultipleSameMfgNo,sourceId,itemCode);  
	}

	});
	function lookupForSearchingBatches(){
		 
	    jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"ManufactureBaLookUp",
			},
			success:function(data)
			{

				//alert(data);
				jQuery("#divLookUp").html(data);

				
				jQuery("#manufactureBatchNo").focus(); //Updated to set focus so that user can directly press enter.
				
				manufactureBatchSearchGrid2('TOCreateItemBatchNo');
				jQuery("#divLookUp" ).dialog( "open" );
				jQuery("#manufactureBatchNo").val(jQuery("#SACreateFromBatchNo").val());
				var itemId=arrayOfSelectedRowFromTOIItem.ItemId ;
				manufactureBatchLookUpData2(itemId,sourceId,5,10);

				/*----------Dialog for item search is there ----------------*/


			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	    
 }
	function lookupForSearchingBatchesGrid(rowId){
		 
	    jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LookUpCallback',
			data:
			{
				id:"ManufactureBaLookUp",
			},
			success:function(data)
			{

				//alert(data);
				jQuery("#divLookUp").html(data);

				
				jQuery("#manufactureBatchNo").focus(); //Updated to set focus so that user can directly press enter.
				
				manufactureBatchSearchGridForNewGrid('TOCreateItemBatchNo',rowId);
				jQuery("#divLookUp" ).dialog( "open" );
				jQuery("#manufactureBatchNo").val(jQuery("#SACreateFromBatchNo").val());
				var rowid=jQuery("#TOItemGrid").jqGrid('getGridParam', 'selrow')  ;
				var itemId=jQuery("#TOItemGrid").jqGrid('getRowData',rowid).ItemId  ;
				manufactureBatchLookUpDataForNewGrid(itemId,sourceId,5,10);

				/*----------Dialog for item search is there ----------------*/


			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	    
 }
	
	function searchBatchno(SACreateFromBatchNo,locationId,itemCode)
		{
		 showMessage("loading", "Searching Batch No...", "");
		   jQuery.ajax({
			   type:'POST',
				url:Drupal.settings.basePath + 'TOCallback',
				data:
				{
					id:"searchBatchUsingCode",
					bNo:SACreateFromBatchNo,
					locationId:locationId,
					itemCode:itemCode,

				},
				success:function(data)
				{

					jQuery(".ajaxLoading").hide();
					jQuery("#SACreateFromBatchNo").css("background","white");
					resultArrData = jQuery.parseJSON(data);
					if(resultArrData.Status==1){
						
						bucketBatchLocWiseQty=resultArrData.Result;
						
						resultArr=resultArrData.Result;
						if(resultArr.length==1){

						}
						if(resultArr.length>0){
							jQuery.each(resultArr,function(key,value){
								jQuery("#TOCreateAvailableQty").val(parseFloat(resultArr[key]['Quantity'],10).toFixed(0));
								currentBatchNo=resultArr[key]['BatchNo'];
							});
							jQuery("#TOCreateTransferQty").focus();
						}
						else {

							showMessage("yellow","No Record Found.","");
						}
					}
					else{
						showMessage('red',resultArrData.Description,'');
					}

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

		   });
		}	
	

	
	
	function sumOfQTYWeight(gridName,columnName,columnName2){
		var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
		var gridRowsForColumn2=jQuery("#"+gridName).jqGrid('getCol',columnName2,false);
		var columnValue=0;
		var columnValue2=0;
		var sum=0;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{

			columnValue=parseFloat(gridRowsForColumn[i],10).toFixed(2);
			columnValue2=parseFloat(gridRowsForColumn2[i],10).toFixed(2);

			sum=sum + (columnValue*columnValue2);
//			pv_sum+= parseFloat(PV[i]);
		}
		return parseFloat(sum,10).toFixed(2);
	}
		
	myDelOptions = {
            onclickSubmit: function (rp_ge, rowid) {
                rp_ge.processing = true;
                var ItemCode=jQuery("#TOItemGrid").jqGrid('getRowData',rowid).Items ;
               matchResult= matchItemInJQGrid(ItemCode) ;
             if(matchResult>=-1){
                	jQuery("#TOItemGrid").jqGrid('delRowData', rowid);
             }
           
	                jQuery("#delmod" + jQuery("#TOItemGrid")[0].id).hide();
	                
	             
	                
	                if (jQuery("#TOItemGrid")[0].p.lastpage > 1) {
	                	jQuery("#TOItemGrid").trigger("reloadGrid", [{ page: jQuery("#TOItemGrid")[0].p.page}]);
	                }

                return true;
            },
            processing: true
        };
	
	  function selectRowIdFromBatchCode(BatchNo){
		    var allRows = jQuery('#TOItemGrid').jqGrid('getDataIDs');
		    	for(var i=0; i<allRows.length ; i++){
		    		if(jQuery('#TOItemGrid').jqGrid('getRowData', allRows[i]).BatchNo==BatchNo){
		    			return allRows[i] ;
		    		}
		     		
		    	}
		    	return -1;
		    }
	function matchBatchInJQGrid(columnValue){
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','BatchNo',false);
	   var found=0;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{

			if(columnValue == gridRowsForColumn[i])
			{
				found = found+1;
				
			}
//			pv_sum+= parseFloat(PV[i]);
		}
		return found;
	}

	function matchItemCodeInJQGrid(columnValue){
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','Items',false);
	   var found=0;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{

			if(columnValue == gridRowsForColumn[i])
			{
				found = 1;
				
			}
//			pv_sum+= parseFloat(PV[i]);
		}
		return found;
	}

	function matchItemInJQGrid(columnValue){
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','Items',false);
	   var found=0;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{

			if(columnValue == gridRowsForColumn[i])
			{
				found = found+1;
				
			}
//			pv_sum+= parseFloat(PV[i]);
		}
		return found;
	}
	
	

	function getRowIdfrommatchingBatchNo(BatchNo){
		
				rowId=selectRowIdFromBatchCode(BatchNo);
			
			return rowId;
	}
	
	function sumOfJQGridadjustQty(columnValue,transferQty){
		var isFoundItemCode =0;
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','Items',false);
		var gridRowsForColumn2 = jQuery("#TOItemGrid").jqGrid('getCol','Adjust',false);
		var gridRowsForColumn3 = jQuery("#TOItemGrid").jqGrid('getCol','RequestQty',false);
	   var sumOfReqQty =0;
	   var sumOfQty=0 ;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{
			if(columnValue == gridRowsForColumn[i]){
			sumOfQty= sumOfQty+parseInt(gridRowsForColumn2[i]) ;
			sumOfReqQty=gridRowsForColumn3[i];
			isFoundItemCode=1 ;
			}
		}
			if(isFoundItemCode==1){   //this fir when item exist in grid
				return sumOfReqQty-(sumOfQty+transferQty);
			}
			else{   //when item not ecxist so we can not calculate qty.
				showMessage('','Provide Transfer Quantity for Item Code '+ columnValue+' ');
				jQuery("#TOCreateItemCode").focus();
			
				return -10;
			}
	}
	/*
	function vallidateSaveTOQty(){
		jQuery("#TOItemGrid").jqGrid('setGridParam',{ page: 1 }).trigger("reloadGrid");
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','Items',false);
		var gridRowsForColumn2 = jQuery("#TOItemGrid").jqGrid('getCol','Adjust',false);
		var gridRowsForColumn3 = jQuery("#TOItemGrid").jqGrid('getCol','RequestQty',false);
	   var sumOfReqQty =0;
	   var sumOfQty=0 ;
	   var previousCode=0;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{
		
		if(i>0 && i!=gridRowsForColumn.length-1){
			if(previousCode != gridRowsForColumn[i]){
				previousCode=gridRowsForColumn[i];
			sumOfQty= sumOfQty+parseInt(gridRowsForColumn2[i]) ;
			parseInt(gridRowsForColumn3[i])
			}
			else{
				parseInt(gridRowsForColumn3[i]);
				if(sumOfQty!=sumOfReqQty){
					showMessage('',previousCode+' Item code qty not Equal to TOI Qty','');
					return 0
				}
				else{
					sumOfQty=parseInt(gridRowsForColumn2[i]);
					previousCode=gridRowsForColumn[i];
				}
				
			}
		}
		else if(i==0 && i!=gridRowsForColumn.length-1)   //first case when loop starts
		{
			sumOfQty=parseInt(gridRowsForColumn2[i]);
			previousCode=gridRowsForColumn[i];
			sumOfReqQty=parseInt(gridRowsForColumn3[i]);
		}
		else   //last item
		{
			parseInt(gridRowsForColumn3[i]);
			sumOfQty= sumOfQty+parseInt(gridRowsForColumn2[i]) ;
					if(sumOfQty!=sumOfReqQty){
						showMessage('',gridRowsForColumn[i]+' Item code qty not Equal to TOI Qty','');
						return 0
					}
		}
		}
		return 1;
		
	}*/
	
	function vallidateSaveTOQty(){
		jQuery("#TOItemGrid").jqGrid('setGridParam',{ page: 1 }).trigger("reloadGrid");
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','Items',false);
		var gridRowsForColumn2 = jQuery("#TOItemGrid").jqGrid('getCol','Adjust',false);
		var gridRowsForColumn3 = jQuery("#TOItemGrid").jqGrid('getCol','RequestQty',false);
	   var sumOfReqQty =0;
	   var sumOfQty=0 ;
	   var previousCode=0;
		for(var i=0;i<gridRowsForColumn.length;i++)
		{
			sumOfQty=sumOfQty+parseInt(gridRowsForColumn2[i]);
	
		}
		
			if(	sumOfQty!=jQuery("#TOCreateTotalTOQuantity").val()){
				return 0;
			}
		return 1;
		
	}
	function vallidateSaveForAllBatchAreInserted(){
		//jQuery("#TOItemGrid").jqGrid('setGridParam',{ page: 1 }).trigger("reloadGrid");
		var gridRowsForColumn = jQuery("#TOItemGrid").jqGrid('getCol','ManufactureBatchNo',false);
		var gridRowsForColumn2 = jQuery("#TOItemGrid").jqGrid('getCol','BatchNo',false);
		var gridRowsForColumn3 = jQuery("#TOItemGrid").jqGrid('getCol','MfgBatchNo',false);
		var gridRowsForColumn4 = jQuery("#TOItemGrid").jqGrid('getCol','Items',false);
		for(var i=0;i<gridRowsForColumn.length;i++)
		{
			
	
			if(gridRowsForColumn[i]=='' || gridRowsForColumn2[i]=='' || gridRowsForColumn3[i]==''){
				
				showMessage('yellow','Insert Batch For '+gridRowsForColumn4[i],'');
				return 0;
			}
		}
		
			
		return 1;
		
	}
	
	
	
	jQuery("#TOItemGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['','Items','ItemId','Item Name','Unit Price','Bucket Name','Mfg Batch No','Transfer Qty','Total Amount','BatchNo'
		           ,'rowNo','RequestQty','TOINo','ManufactureBatchNo','MRP',
		           'Weight','EachCartonQty','ItemPackSize','ItemDesc','DateFormatDesc','ExpDuration','MerchHierarchyDetailId','MfgDate','ExpDate',
		           'BucketId','UOMId'],
		           colModel: [
{ name: 'myac', width:60, fixed:true, sortable:false, resize:false, formatter:'actions',
	formatoptions:{
		keys:true,
		onEdit:function(rowid) {

			var thisid = this.id;

			updateOnEditableValue = jQuery(".editable").val();

			var itemQuantity = parseInt(jQuery(".editable").val());

			var rowEditableOrNot = parseInt(jQuery('#jqgridtable_left').jqGrid('getCell',rowid, 'IsEditableOrNot'));

			if(rowEditableOrNot == 0)
			{
				//alert(rowEditableOrNot);
				showMessage("yellow", "Promotional items quantity can not be changed", "");

				jQuery("#jqgridtable_left").jqGrid('setCell',rowid,'Qty',itemQuantity);

			}
			else
			{
				jQuery("#jqgridtable_left").jqGrid('setColProp', 'Qty', {editable:true});
			}


		},
		onSuccess:function(jqXHR) {
	
			alert("in onSuccess used only for remote editing:"+
					"\nresponseText="+jqXHR.responseText+
					"\n\nWe can verify the server response and return false in case of"+
			" error response. return true confirm that the response is successful");
			return true;
		},
		onError:function(rowid, jqXHR, textStatus) {
			alert("in onError used only for remote editing:"+
					"\nresponseText="+jqXHR.responseText+
					"\nstatus="+jqXHR.status+
					"\nstatusText"+jqXHR.statusText+
			"\n\nWe don't need return anything");
		},
		afterSave:function(rowid) {
			//alert("in afterSave (Submit): rowid="+rowid+"\nWe don't need return anything");
			var rowData = jQuery('#jqgridtable_left').jqGrid ('getRowData', rowid);
			var itemDistributorPrice = rowData.Price;
			var itemId = rowData.RowID;
			var itemQty = rowData.Qty;
			if(itemQty <= 0)
			{
				var intUpdateOnEditableValue = parseInt(updateOnEditableValue);

				showMessage("yellow", "Quantity cannot be negative or zero", "");

				jQuery("#jqgridtable_left").jqGrid('setCell',rowid,'Qty',intUpdateOnEditableValue);

			}
			else
			{
				
			}


			//alert(rowData);
		},
		afterRestore:function(rowid) {
			//alert("in afterRestore (Cancel): rowid="+rowid+"\nWe don't need return anything");

		},
		delOptions:myDelOptions
	}
},
		                      { name: 'Items', index: 'Items', width:100 },
		                      { name: 'ItemId', index: 'ItemId', width: 70, hidden:true},
		                      { name: 'ItemName', index: 'ItemName', width:230 },
		                      { name: 'UnitPrice', index: 'DistributorId', width: 70},
		                      { name: 'BucketName', index: 'FirstName', width: 100},
		                      { name: 'MfgBatchNo', index:'MfgBatchNo', width: 100},
		                      { name: 'Adjust', index: 'Adjust', width: 100},// should be editable to allow changes.
		                      { name: 'TotalAmount', index: 'PromotionId', width: 100},
		                      { name: 'BatchNo', index: 'PromoDescription', width: 120},
		                      { name: 'rowNo', index: 'rowNo', width: 70, hidden:true},
		                      { name: 'RequestQty', index: 'RequestQty', width: 70, hidden:true},
		                      { name: 'TOINo', index: 'TOINo', width: 70, hidden:true},
		                      { name: 'ManufactureBatchNo', index: 'ManufactureBatchNo', width: 70, hidden:true},
		                      { name: 'MRP', index: 'MRP', width: 70, hidden:true},
		                      { name: 'Weight', index: 'Weight', width: 70, hidden:false},
		                      { name: 'EachCartonQty', index: 'EachCartonQty', width: 70, hidden:true},
		                      { name: 'ItemPackSize', index: 'ItemPackSize', width: 70, hidden:true},
		                      { name: 'ItemDesc', index: 'ItemDesc', width: 70, hidden:true},
		                      { name: 'DateFormatDesc', index: 'DateFormatDesc', width: 70, hidden:true},
		                      { name: 'ExpDuration', index: 'ExpDuration', width: 70, hidden:true},
		                      { name: 'MerchHierarchyDetailId', index: 'MerchHierarchyDetailId', width: 70, hidden:true},
		                      { name: 'MfgDate', index: 'MfgDate', width: 70, hidden:true},
		                      { name: 'ExpDate', index: 'ExpDate', width: 70, hidden:true},
		                      { name: 'BucketId', index: 'BucketId', width: 70, hidden:true},
		                      { name: 'UOMId', index: 'UOMId', width: 70, hidden:true}
		                      ],
		                     /* pager: jQuery('#PJmap_TOItemGrid'),*/
		                    
		                      width:1040,
		                      height:150,
		                      rowNum: 500,
		                      rowList: [500],
		                      sortname: 'Label',
		                      sortorder: "asc",
		                      hoverrows: true,
		                      autowidth:true,
		                      scrollable:true,
		                      shrinkToFit:false,
		                      autoheight:true,
					          editurl:'clientArray',
		                      afterInsertRow : function(ids)
		                      {

		                    	  jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

		                      },

		                      onSelectRow: function(rowId) {

		                    	

		                      },
		                      ondblClickRow: function(rowId) {
		                    	  jQuery("#TOCreateUOM").val(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'UOMId'));
		                    	  jQuery("#TOCreateWeight").val(parseFloat(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'Weight'),10).toFixed(2));
		                    	  jQuery("#TOCreateRequestedQty").val(parseFloat(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'RequestQty'),10).toFixed(0));
		                    	  jQuery("#TOIIteBucketName").val(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'BucketName'));
		                    	  jQuery("#TOCreateAvailableQty").val('');
		                    	  jQuery("#TOCreateTransferQty").val('');
		                    	  batchnoForMultipleSameMfgNo=jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'BatchNo');
		                    	  jQuery("#TOCreateTransferPrice").val(parseFloat(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'UnitPrice'),10).toFixed(2));
		                    	  jQuery("#TOCreateItemDescription").val(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'ItemName'));
		                    	  jQuery("#TOCreateItemCode").val(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'Items'));
		                    	  jQuery("#TOCreateItemBatchNo").val(jQuery("#TOItemGrid").jqGrid('getCell', rowId, 'ManufactureBatchNo'));
		                    	  arrayOfSelectedRowFromTOIItem=jQuery("#TOItemGrid").jqGrid('getRowData', rowId) ;
		                    	   jQuery("#TOCreateItemBatchNo").focus();

		                    //  jQuery("#"+iRow+"_MfgBatchNo").attr('disabled',true);
	                  			//forOpenMFGdialogue("#"+iRow+"_MfgBatchNo");
	                  			lookupForSearchingBatchesGrid(rowId);
		                      }
	});

	jQuery("#TOItemGridTOItemGrid").jqGrid('navGrid','#PJmap_TOItemGrid',{edit:false,add:false,del:false});	
	

	/*------------------------------add calender to fields --------------------------------*/
	jQuery(function() {



		jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
		jQuery( ".showCalender" ).datepicker("setDate",new Date());
	});

	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * TOI Search Grid 
	 */
	function vallidationOnAdd (columnvalue){
		var itemCOde=columnvalue;
		if(jQuery("#TOCreateItemCode").val()==''){
			jQuery("#TOCreateItemCode").css("background","#FF9999");
			showMessage("yellow","Enter Valid Item Code","");
			jQuery("#TOCreateItemCode").focus();
			return 0;
		}
		else{
			
			jQuery("#TOCreateItemCode").css("background","white");
			
		}
		if(jQuery("#TOCreateTransferQty").val()==''){
			jQuery("#TOCreateTransferQty").css("background","#FF9999");
			showMessage("yellow","Enter Valid Transfer Quantity.","");
			jQuery("#TOCreateItemCode").focus();

			return 0;
		}
		else{
			
			jQuery("#TOCreateTransferQty").css("background","white");
			
		}
		if(jQuery("#TOCreateItemBatchNo").val()==''){
			jQuery("#TOCreateItemBatchNo").css("background","#FF9999");
			showMessage("yellow","Enter Valid Batch No.","");
			jQuery("#TOCreateItemBatchNo").focus();

			return 0;
		}
		else{
			
			jQuery("#TOCreateItemBatchNo").css("background","white");
			
		}
		if(jQuery("#TOCreateAvailableQty").val()==''){
			jQuery("#TOCreateAvailableQty").css("background","#FF9999");
			jQuery("#TOCreateItemBatchNo").focus();
			showMessage("yellow","Enter valid Item Code and Batch No for Available Qty. Press Tab.","");
		
			return 0;
		}
		else{
			
			jQuery("#TOCreateAvailableQty").css("background","white");
			
		}
		
	
		if(parseInt(parseFloat(jQuery("#TOCreateAvailableQty").val(),10).toFixed(2))<parseInt(parseFloat(jQuery("#TOCreateTransferQty").val(),10).toFixed(2))){
	
			showMessage("yellow","Transfer Quantity cannot be greater than Available Quantity",'');	
			return 0;
		}
		if(parseInt(parseFloat(jQuery("#TOCreateAvailableQty").val(),10).toFixed(2))>parseInt(parseFloat(jQuery("#TOCreateTransferQty").val(),10).toFixed(2)) && parseInt(parseFloat(jQuery("#TOCreateTransferQty").val(),10).toFixed(2))!=parseInt(parseFloat(jQuery("#TOCreateRequestedQty").val(),10).toFixed(2))){
			var r =confirm("Available batch quantity   is " +parseFloat(jQuery("#TOCreateAvailableQty").val(),10).toFixed(0)+" . Are you sure you want to proceed ?");
			if(r==false)
			{
				/*jQuery("#TOItemGrid").jqGrid('setCell',rowId,"MfgBatchNo","", { 'background-color': '#FF9999' });
				jQuery("#TOItemGrid").jqGrid('setCell',rowId,"MfgBatchNo","");
				jQuery("#TOItemGrid").jqGrid('setCell',rowId,"BatchNo","");
				jQuery("#TOItemGrid").jqGrid('setCell',rowId,"ManufactureBatchNo","");
				jQuery("#TOItemGrid").jqGrid('setCell',rowId,"ManufactureBatchNo","0");*/
			return 0 ; 	
			}
			
		}
		
		
		
		return 1;
}

	jQuery("#btnAdd").click(function(){
		
		updateRowId=getRowIdfrommatchingBatchNo(currentBatchNo);
		
		if(vallidationOnAdd(arrayOfSelectedRowFromTOIItem.Items)==1){
			
			if(updateRowId==-1){
			 var newData = [{"Items":arrayOfSelectedRowFromTOIItem.Items,"ItemId":arrayOfSelectedRowFromTOIItem.ItemId,"ItemName":arrayOfSelectedRowFromTOIItem.ItemName,"UnitPrice":arrayOfSelectedRowFromTOIItem.UnitPrice,
				  "BucketName":arrayOfSelectedRowFromTOIItem.BucketName,"MfgBatchNo":jQuery("#TOCreateItemBatchNo").val(),
				  "Adjust":parseFloat(jQuery("#TOCreateTransferQty").val(),10).toFixed(0),"TotalAmount":arrayOfSelectedRowFromTOIItem.TotalAmount
				  ,"BatchNo":currentBatchNo,"TOINumber":arrayOfSelectedRowFromTOIItem.TOINo,
				  "rowNo":arrayOfSelectedRowFromTOIItem.rowNo,"RequestQty":arrayOfSelectedRowFromTOIItem.RequestQty,"TOINo":arrayOfSelectedRowFromTOIItem.TOINo,"ManufactureBatchNo":arrayOfSelectedRowFromTOIItem.ManufactureBatchNo,"MRP":arrayOfSelectedRowFromTOIItem.MRP,
				  "Weight":arrayOfSelectedRowFromTOIItem.Weight,"EachCartonQty":arrayOfSelectedRowFromTOIItem.EachCartonQty,"ItemPackSize":arrayOfSelectedRowFromTOIItem.ItemPackSize,
				  "ItemDesc":arrayOfSelectedRowFromTOIItem.ItemDesc,"DateFormatDesc":arrayOfSelectedRowFromTOIItem.DateFormatDesc,"ExpDuration":arrayOfSelectedRowFromTOIItem.ExpDuration,"MerchHierarchyDetailId":arrayOfSelectedRowFromTOIItem.MerchHierarchyDetailId,
				  "MfgDate":arrayOfSelectedRowFromTOIItem.MfgDate,"ExpDate":arrayOfSelectedRowFromTOIItem.ExpDate,
				  "BucketId":arrayOfSelectedRowFromTOIItem.BucketId,"UOMId":arrayOfSelectedRowFromTOIItem.UOMId
			  }];
			
			  for (var i=0;i<newData.length;i++) {
				  jQuery("#TOItemGrid").jqGrid('addRowData',gridrowId, newData[newData.length-i-1], "first");
				  gridrowId=gridrowId+1;
				  clearItemBox();
				  
			  }
			  
			}
			else{
				updateQuantityInGrid(jQuery("#TOCreateTransferQty").val(),updateRowId);
				clearItemBox();
			}
			var currentSum=sumOfJQGridadjustQty(arrayOfSelectedRowFromTOIItem.Items,0);
			if(currentSum>0){
				showMessage('',currentSum+'Transfer Quantity less than TOI Quantity for Item Code '+arrayOfSelectedRowFromTOIItem.Items,'');
				jQuery("#"+arrayOfSelectedRowFromTOIItem.Items).css("background","white");
				jQuery("#TOCreateItemCode").focus();
			}
			else if(currentSum<0){
				showMessage('',(currentSum)+'Transfer Quantity is more than TOI Quantity. Make it equal.','');
				jQuery("#TOCreateItemCode").focus();
				//jQuery("#TOItemGrid").jqGrid('delRowData',gridrowId-1);
				jQuery("#"+arrayOfSelectedRowFromTOIItem.Items).css("background","white");
				return 0;
			}
			else{
				//jQuery("#"+arrayOfSelectedRowFromTOIItem.Items).css("background","#00FFFF");	
			}
		}
		
		//clearItemBox();
	});
	
	function MatchToiQtyAfersave(){
		//jQuery("#TOCreateItemCode").remove();
	   var arrayForItems=jQuery('option', '#TOCreateItemCode').map(function() {return jQuery(this).text();}).get();
		for(var i=1 ;i<arrayForItems.length;i++)
		{ 
			var currentSum=sumOfJQGridadjustQty(arrayForItems[i],0);
			if(currentSum>0){
				showMessage('',currentSum+'Transfer Quantity is less than TOI Quantity. Item Code '+arrayForItems[i],'');
				jQuery("#TOCreateItemCode").focus();
				return 0;
			}
			else if(currentSum==-10){
				return 0;
			}
			else if(currentSum<0){
				showMessage('',(currentSum)+'Transfer Quantity is more than TOI Quantity. Remove Transfer Quantity. Item Code '+arrayForItems[i]+'.','');
				return 0;
			}
				}
		   return 1;
		}
	
	
	function updateQuantityInGrid(transferQty,rowId){
    	
        
    	var AlreadyReceivedQtyReceivedQty = jQuery('#TOItemGrid').jqGrid('getCell',
    			rowId, 'Adjust');
    	jQuery("#TOItemGrid").jqGrid('setCell',rowId,
    			'Adjust',(parseInt(transferQty)));
    } 
	
	jQuery("#TOSearchGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['TO Number','TOI Number','Source Address','Destination Address','TO Creation Date',
		           'TO Ship Date','Quantity','Total TOI Amount','Status','PackSize',
		           'SourceLocationId','ExpectedDeliveryDate','Remarks','ShippingDetails','DestinationLocationId',
		           'StatusName','ShippingWayBillNo','RefNumber','GrossWeight','ShipDate','StateId','SourcePhone',
		           'ModifiedByName','EmailId1','SourceCity','DestinationCity','IECCode',
		           'ExporterRef','OtherRef','BuyerOtherthanConsignee','PreCarriage','PlaceofReceiptbyPreCarrier',
		           'CountryOfOrigin','CountryOfDestination','VesselflightNo','PortofLoading','PortofDischarge',
		           'PortofDestination','BuyerOrderNo','BuyerOrderDate','TermsofDelivery','DELIVERY','PAYMENT'
		           ],
		           colModel: [
		                      { name: 'TONumber', index: 'TONumber', width: 100},
		                      { name: 'TOINumber', index: 'TOINumber', width: 100},
		                      { name: 'SourceAddress', index: 'SourceAddress', width: 250},
		                      { name: 'DestinationAddress', index: 'DestinationAddress', width: 250},
		                      { name: 'TOCreationDate', index: 'DiscountAmount', width: 110},
		                      { name: 'TOShipDate', index: 'TOIDate', width: 90},
		                      { name: 'Quantity', index: 'TotalTOIQuantity', width: 90},
		                      { name: 'TotalTOIAmount', index: 'TotalTOIAmount', width: 100},
		                      { name: 'Status', index: 'Status', width: 70},
		                      { name: 'PackSize', index: 'PackSize', width: 70, hidden:true},
		                      { name: 'SourceLocationId', index: 'SourceLocationId', width: 70, hidden:true},
		                      { name: 'ExpectedDeliveryDate', index: 'ExpectedDeliveryDate', width: 70, hidden:true},
		                      { name: 'Remarks', index: 'Remarks', width: 70, hidden:true},
		                      { name: 'ShippingDetails', index: 'ShippingDetails', width: 70, hidden:true},
		                      { name: 'DestinationLocationId', index: 'DestinationLocationId', width: 70, hidden:true},
		                      { name: 'StatusName', index: 'StatusName', width: 70, hidden:true},
		                      { name: 'ShippingWayBillNo', index: 'ShippingWayBillNo', width: 70, hidden:true},
		                      { name: 'RefNumber', index: 'RefNumber', width: 70, hidden:true},
		                      { name: 'GrossWeight', index: 'GrossWeight', width: 70, hidden:true},
		                      { name: 'ShipDate', index: 'ShipDate', width: 70, hidden:true},
		                      { name: 'StateId', index: 'StateId', width: 70, hidden:true},
		                      { name: 'SourcePhone', index: 'SourcePhone', width: 70, hidden:true},
		                      { name: 'ModifiedByName', index: 'ModifiedByName', width: 70, hidden:true},
		                      { name: 'EmailId1', index: 'EmailId1', width: 70, hidden:true},
		                      { name: 'SourceCity', index: 'SourceCity', width: 70, hidden:true},
		                      { name: 'DestinationCity', index: 'DestinationCity', width: 70, hidden:true},
		                      { name: 'IECCode', index: 'IECCode', width: 70, hidden:true},
		                      { name: 'ExporterRef', index: 'ExporterRef', width: 70, hidden:true},
		                      { name: 'OtherRef', index: 'OtherRef', width: 70, hidden:true},
		                      { name: 'BuyerOtherthanConsignee', index: 'BuyerOtherthanConsignee', width: 70, hidden:true},
		                      { name: 'PreCarriage', index: 'PreCarriage', width: 70, hidden:true},
		                      { name: 'PlaceofReceiptbyPreCarrier', index: 'PlaceofReceiptbyPreCarrier', width: 70, hidden:true},
		                      { name: 'CountryOfOrigin', index: 'CountryOfOrigin', width: 70, hidden:true},
		                      { name: 'CountryOfDestination', index: 'CountryOfDestination', width: 70, hidden:true},
		                      { name: 'VesselflightNo', index: 'VesselflightNo', width: 70, hidden:true},
		                      { name: 'PortofLoading', index: 'PortofLoading', width: 70, hidden:true},
		                      { name: 'PortofDischarge', index: 'PortofDischarge', width: 70, hidden:true},
		                      { name: 'PortofDestination', index: 'PortofDestination', width: 70, hidden:true},
		                      { name: 'BuyerOrderNo', index: 'BuyerOrderNo', width: 70, hidden:true},
		                      { name: 'BuyerOrderDate', index: 'BuyerOrderDate', width: 70, hidden:true},
		                      { name: 'TermsofDelivery', index: 'TermsofDelivery', width: 70, hidden:true},
		                      { name: 'DELIVERY', index: 'DELIVERY', width: 70, hidden:true},
		                      { name: 'PAYMENT', index: 'PAYMENT', width: 70, hidden:true}
		                      ],

		                      pager: jQuery('#PJmap_TOSearchGrid'),
		                      width:1040,

		                      height:420,
		                      rowNum: 500,
		                      rowList: [500],
		                      sortname: 'Label',
		                      sortorder: "asc",
		                      hoverrows: true,
		                      autowidth:true,
		                      scrollable:true,
		                      shrinkToFit:false,
		                      autoheight:true,


		                      afterInsertRow : function(ids)
		                      {

		                    	  jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//		                    	  jQuery("tr.jqgrow").css("background", "#DDDDDC");
		                      },

		                      loadComplete: function() {
//		                    	  jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//		                    	  alert("laodign complete");
		                      }	,
		                      ondblClickRow: function(rowId) {
		                    	  disableField();
		                    	  clearItemBox();
		                    	  jQuery("#TOCreateTOINumber").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'TOINumber'));
		                    	  var sourceId=jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'SourceLocationId');
		                    	  var TONumber=jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'TONumber');
                                 var  sounrcelocationName=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'SourceLocationId'),'LocationName');
		                    	var destinationName=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'DestinationLocationId'),'LocationName');
                                 jQuery("#TOCreateTOISourceLocation").val(sounrcelocationName+'-'+fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'SourceLocationId'),'LocationCode'));
		                    	  jQuery("#TOCreateTOIDestinationLocation").val(destinationName+'-'+fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'DestinationLocationId'),'LocationCode'));
		                    	  jQuery("#TOCreateTONumber").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'TONumber'));

		                    	  jQuery("#TOCreateTOSourceLocation").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'SourceAddress'));

		                    	  jQuery("#TOCreateDestinationAddress").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'DestinationAddress'));
		                    	  jQuery("#TOCreatePackSize").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'PackSize'));
		                    	  jQuery("#TOCreateBranchRemark").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'Remarks'));
		                    	  jQuery("#TOCreateSourceCSTNo").val('');
		                    	  jQuery("#TOCreateSourceVATNo").val('');

		                    	  jQuery("#TOCreateSourceTINNo").val('');
		                    	  jQuery("#TOCreateDestinationCSTNo").val('');
		                    	  jQuery("#TOCreateDestinationVATNo").val( '');
		                    	  jQuery("#TOCreateDestinationTinNo").val('');
		                    	  jQuery("#TOCreateExpectedDeliveryDate").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'ExpectedDeliveryDate'));

		                    	  jQuery("#TOCreateRefNo").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'RefNumber'));
		                    	  jQuery("#TOCreateShippingWayBillNo").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'ShippingWayBillNo'));
		                    	  jQuery("#TOCreateTotalTOQuantity").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'Quantity'));
		                    	  jQuery("#TOCreateGrossWeight").val(parseFloat((jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'GrossWeight')),10).toFixed(2));
		                    	  jQuery("#TOCreateTotalTOAmount").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'TotalTOIAmount'));

		                    	  jQuery("#TOCreateRemarks").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'Remarks'));

		                    	  jQuery("#TOCreateShippingDetails").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'ShippingDetails'));


		                    	  jQuery("#TOCreateStatus").val(jQuery("#TOSearchGrid").jqGrid('getCell', rowId, 'StatusName'));
		                    	  jQuery("#TOCreateIsExported").val('');
		                    	  jQuery("#divTransferOutTab").tabs( "select", "DivCreate" );
		                    	  EnableDisableOnStatus(2);
		                    	  showMessage("loading", "Searching for item details...", "");
		                    	  jQuery.ajax({
		                    		  type:'POST',
		                    		  url:Drupal.settings.basePath + 'TOCallback',
		                    		  data:
		                    		  {
		                    			  id:"searchTOItems",
		                    			  TONo:TONumber,
		                    			  sourceId:sourceId
		                    		  },
		                    		  success:function(data)  /*ajax call for populating payment grid*/
		                    		  {
		                    			  jQuery(".ajaxLoading").hide();	
		                    			  itemsJsonData = jQuery.parseJSON(data);
		                    			  if(itemsJsonData.Status==1){

		                    				  var itemsJson=itemsJsonData.Result ;
		                    				  jQuery("#TOItemGrid").jqGrid("clearGridData");
		                    				  jQuery.each(itemsJson,function(key,value)
		                    						  {
		                    					  /*					['Items','Item Name','Unit Price','Bucket Name','Mfg Batch No','Adjust','Total Amount','BatchNo']*/		
		                    					  var ItemCode = itemsJson[key]['ItemCode']	;
		                    					  var ItemId=itemsJson[key]['ItemId']	;
		                    					  var ItemDescription = itemsJson[key]['ItemDescription'];    
		                    					  var UOMId = itemsJson[key]['UOMId']  ;
		                    					  var UOMName = itemsJson[key]['UOMName']  ;
		                    					  var UnitPrice =parseFloat( itemsJson[key]['TransferPrice'],10).toFixed(2);    
		                    					  var Bucketid = itemsJson[key]['BucketId'];
		                    					  var Bucket=itemsJson[key]['BucketName'];
		                    					  var AvailableQty =parseFloat( itemsJson[key]['AvailableQty'],10).toFixed(2);     
		                    					  var AfterAdjustQt =parseFloat( itemsJson[key]['AfterAdjustQty'],10).toFixed(2);           
		                    					  var TotalAmount =parseFloat( itemsJson[key]['TotalAmount'],10).toFixed(2); 
		                    					  var TOINumber =itemsJson[key]['TOINumber'] ; 
		                    					  var rowNo =itemsJson[key]['RowNo'] ;
		                    					  var RequestQty=itemsJson[key]['RequestQty'] ;
		                    					  var BatchNo =itemsJson[key]['BatchNo'] ; 

		                    					  var ManufactureBatchNo =itemsJson[key]['ManufactureBatchNo'] ;
		                    					  var MRP=itemsJson[key]['MRP'] ;

		                    					  var Weight =itemsJson[key]['Weight'] ;
		                    					  var EachCartonQty=itemsJson[key]['EachCartonQty'] ;

		                    					  var ItemPackSize =itemsJson[key]['ItemPackSize'] ;
		                    					  var ItemDesc=itemsJson[key]['ItemDesc'] ;

		                    					  var DateFormatDesc =itemsJson[key]['DateFormatDesc'] ;
		                    					  var ExpDuration=itemsJson[key]['ExpDuration'] ;
		                    					  var MerchHierarchyDetailId=itemsJson[key]['MerchHierarchyDetailId'] ;

		                    					  var newData = [{"Items":ItemCode,"ItemId":itemsJson[key]['ItemId'],"ItemName":ItemDescription,"UnitPrice":UnitPrice,
		                    						  "BucketName":Bucket,"MfgBatchNo":ManufactureBatchNo,
		                    						  "Adjust":AfterAdjustQt,"TotalAmount":TotalAmount
		                    						  ,"BatchNo":BatchNo,"TOINumber":TOINumber,
		                    						  "rowNo":rowNo,"RequestQty":RequestQty,"TOINo":TOINumber,"ManufactureBatchNo":ManufactureBatchNo,"MRP":MRP,
		                    						  "Weight":Weight,"EachCartonQty":EachCartonQty,"ItemPackSize":ItemPackSize,
		                    						  "ItemDesc":ItemDesc,"DateFormatDesc":DateFormatDesc,"ExpDuration":ExpDuration,"MerchHierarchyDetailId":MerchHierarchyDetailId,
		                    						  "MfgDate":itemsJson[key]['MfgDate'],"ExpDate":itemsJson[key]['ExpDate'],
		                    						  "BucketId":itemsJson[key]['BucketId'],"UOMId":itemsJson[key]['UOMId']	
		                    					  }];
		                    					  for (var i=0;i<newData.length;i++) {
		                    						  jQuery("#TOItemGrid").jqGrid('addRowData',rowId, newData[newData.length-i-1], "first");
		                    						  rowId=rowId+1;
		                    					  }



		                    						  });
		                    			  }
		                    			  else{
		                    				  showMessage('red',itemsJsonData.Description,'');
		                    			  }

		                    		  },
		                    		  error: function(XMLHttpRequest, textStatus, errorThrown) { 

		                  				jQuery(".ajaxLoading").hide();
		                  				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
		                  				showMessage("", defaultMessage, "");
		                  			}
		                    	  });

		                      }

	});

	jQuery("#TOSearchGrid").jqGrid('navGrid','#PJmap_TOSearchGrid',{edit:false,add:false,del:false});	
	jQuery("#PJmap_TOSearchGrid").hide();


	/*  enable disABLE FIELS       */	


	function disableItemField(){
		jQuery("#TOCreateUOM").attr('disabled',true);
		jQuery("#TOCreateWeight").attr('disabled',true);
		jQuery("#TOCreateRequestedQty").attr('disabled',true);
		jQuery("#TOIIteBucketName").attr('disabled',true);
		jQuery("#TOCreateAvailableQty").attr('disabled',true);

		jQuery("#TOCreateItemDescription").attr('disabled',true);

		jQuery("#TOCreateItemAmount").attr('disabled',true);
		jQuery("#TOCreateTransferPrice").attr('disabled',true);

	}

	function clearItemBox()
	{
		jQuery("#TOCreateUOM").val('');
		jQuery("#TOCreateWeight").val('');
		jQuery("#TOCreateRequestedQty").val('');
		jQuery("#TOIIteBucketName").val('');
		jQuery("#TOCreateAvailableQty").val('');
		jQuery("#TOCreateTransferQty").val('');
		jQuery("#TOCreateTransferPrice").val('');
		jQuery("#TOCreateItemDescription").val('');
		jQuery("#TOCreateItemCode").val('');
		jQuery("#TOCreateItemBatchNo").val('');


	}		
	function disableField(){

		jQuery("#btnTSF02Confirm").attr('disabled',true);
		jQuery("#btnPrint").attr('disabled',true);
		jQuery("#btnAdd").attr('disabled',true);
		jQuery("#TOCreateTOINumber").attr('disabled',false);
		jQuery("#TOCreateTOISourceLocation").attr('disabled',true);
		jQuery("#TOCreateTOIDestinationLocation").attr('disabled',true);
		jQuery("#TOCreateTONumber").attr('disabled',true);
		jQuery("#TOCreateTOSourceLocation").attr('disabled',true);
		jQuery("#TOCreateDestinationAddress").attr('disabled',true);

		jQuery("#TOCreatePackSize").attr('disabled',false);
		jQuery("#TOCreateBranchRemark").attr('disabled',true);
		jQuery("#TOCreateSourceCSTNo").attr('disabled',true);
		jQuery("#TOCreateSourceVATNo").attr('disabled',true);
		jQuery("#TOCreateSourceTINNo").attr('disabled',true);
		jQuery("#TOCreateDestinationCSTNo").attr('disabled',true);
		jQuery("#TOCreateDestinationVATNo").attr('disabled',true);
		jQuery("#TOCreateDestinationTinNo").attr('disabled',true);

		jQuery("#TOCreateExpectedDeliveryDate").attr('disabled',false);
		jQuery("#TOCreateRefNo").attr('disabled',false);

		jQuery("#TOCreateShippingWayBillNo").attr('disabled',false);
		jQuery("#TOCreateTotalTOQuantity").attr('disabled',true);
		jQuery("#TOCreateGrossWeight").attr('disabled',true);
		jQuery("#TOCreateTotalTOAmount").attr('disabled',true);
		jQuery("#TOCreateStatus").attr('disabled',true);
		jQuery("#TOCreateDestinationCSTNo").attr('disabled',true);
		jQuery("#TOCreateIsExported").attr('disabled',true);

	}





	function enableField(){
		jQuery("#TOCreateTOINumber").attr('disable',false);
		jQuery("#TOCreatePackSize").attr('disable',false);
		jQuery("#TOCreateRemarks").attr('disable',false);
		jQuery("#TOCreateExpectedDeliveryDate").attr('disable',false);
		jQuery("#TOCreateRefNo").attr('disable',false);
		jQuery("#TOCreateShippingWayBillNo").attr('disable',false);

	}

	function resetAllFields(){
		jQuery("#TOCreateRemarks").css("background","white");
		jQuery("#TOCreateRefNo").css("background","white");
		jQuery("#TOCreateShippingDetails").css("background","white");
		jQuery("#TOCreateExpectedDeliveryDate").css("background","white");
		jQuery("#TOCreateShippingWayBillNo").css("background","white");
		jQuery("#TOCreatePackSize").css("background","white");
		jQuery("#TOCreateTOINumber").css("background","white");
		jQuery("#TOCreateItemBatchNo").css("background","white");
		
		jQuery("#TOCreateAvailableQty").css("background","white");
		jQuery("#TOCreateTransferQty").css("background","white");
		jQuery("#TOCreateTOINumber").val('');
		jQuery("#TOCreateTOISourceLocation").val('');
		jQuery("#TOCreateTOIDestinationLocation").val('');
		jQuery("#TOCreateTONumber").val('');
		jQuery("#TOCreateTOSourceLocation").val('');
		jQuery("#TOCreateDestinationAddress").val('');

		jQuery("#TOCreatePackSize").val('');
		jQuery("#TOCreateBranchRemark").val('');
		jQuery("#TOCreateSourceCSTNo").val('');
		jQuery("#TOCreateSourceVATNo").val('');
		jQuery("#TOCreateSourceTINNo").val('');
		jQuery("#TOCreateDestinationCSTNo").val('');
		jQuery("#TOCreateDestinationVATNo").val('');
		jQuery("#TOCreateDestinationTinNo").val('');
		jQuery("#TOCreateShippingDetails").val('');

		jQuery("#TOCreateRemarks").val('');

		jQuery("#TOCreateExpectedDeliveryDate").val('');
		jQuery("#TOCreateRefNo").val('');

		jQuery("#TOCreateShippingWayBillNo").val('');
		jQuery("#TOCreateTotalTOQuantity").val('');
		jQuery("#TOCreateGrossWeight").val('');
		jQuery("#TOCreateTotalTOAmount").val('');
		jQuery("#TOCreateStatus").val('');
		jQuery("#TOCreateDestinationCSTNo").val('');
		jQuery("#TOCreateIsExported").val('');
		jQuery("#TOItemGrid").jqGrid("clearGridData");
		EnableDisableOnStatus(0);
		backGroundColorForDisableField();
	}
	function	EnableDisableOnStatus(status){
		if(status==0){
			jQuery("#btnTSF02Confirm").attr('disabled',false);
			jQuery("#btnTSF02Search").attr('disabled',false);
			jQuery("#btnTSF02Print").attr('disabled',true);
			jQuery("#btnAdd").attr('disabled',false);
			disableEnableFieldOnStatus(0);
		}
		else if (status==2){
			jQuery("#btnTSF02Confirm").attr('disabled',true);
			jQuery("#btnTSF02Search").attr('disabled',false);
			jQuery("#btnTSF02Print").attr('disabled',false);
			jQuery("#btnAdd").attr('disabled',true);
			disableEnableFieldOnStatus(1);
		}
		actionButtonsStauts();
	}
	function disableEnableFieldOnStatus(IsDisable){
		if(IsDisable==1)
		{
			jQuery("#TOCreateTOINumber").attr('disabled',true);	
			jQuery("#TOCreatePackSize").attr('disabled',true);	
			jQuery("#TOCreateExpectedDeliveryDate").attr('disabled',true);	
			jQuery("#TOCreateRefNo").attr('disabled',true);
			jQuery("#TOCreateGrossWeight").attr('disabled',true);
			jQuery("#TOCreateShippingDetails").attr('disabled',true);
			jQuery("#TOCreateShippingWayBillNo").attr('disabled',true);	
			jQuery("#TOCreateRemarks").attr('disabled',true);

			jQuery("#TOCreateItemCode").attr('disabled',true);
			jQuery("#TOCreateItemBatchNo").attr('disabled',true);	
			jQuery("#TOCreateTransferQty").attr('disabled',true);
			
		}		
		else{
			jQuery("#TOCreateTOINumber").attr('disabled',false);	
			jQuery("#TOCreatePackSize").attr('disabled',false);	
			jQuery("#TOCreateExpectedDeliveryDate").attr('disabled',false);	
			jQuery("#TOCreateRefNo").attr('disabled',false);
			jQuery("#TOCreateGrossWeight").attr('disabled',true);
			jQuery("#TOCreateShippingDetails").attr('disabled',false);
			jQuery("#TOCreateShippingWayBillNo").attr('disabled',false);	
			jQuery("#TOCreateRemarks").attr('disabled',false);
			
			jQuery("#TOCreateItemCode").attr('disabled',false);
			jQuery("#TOCreateItemBatchNo").attr('disabled',false);	
			jQuery("#TOCreateTransferQty").attr('disabled',false);
		
		}
			
		}	
	
	

	jQuery("#btnCreateReset").click(function(){
		AllReset();
		JsonFromArray={};
		
		
	});

	jQuery("#btnCreateItemReset").click(function(){
	
		clearItemBox();

	});

function AllReset(){
	disableField();
	resetAllFields();
	clearItemBox();
	resetItemCombo();
	jQuery("#txtItemCode").css("background","white");
	jQuery("#TOCreatePackSize").css("background","white");
	jQuery("#TOCreateExpectedDeliveryDate").css("background","white");
	jQuery("#TOCreateShippingWayBillNo").css("background","white");
	jQuery("#TOCreateShippingDetails").css("background","white");
	jQuery("#TOCreateRefNo").css("background","white");
}
	jQuery("#btnTSF02Confirm").unbind("click").click(function(){
		jQuery("#actionName").val('Confirm');
		var oOrderJson = jQuery("#TOItemGrid").jqGrid('getRowData');
		order_json_string = JSON.stringify(oOrderJson);
		var vallidationStatus=validationOnSaveButton	();
		if(vallidationStatus==1){

			var r = confirm("Are you sure you want to save data ?");
			if(r==true)	{
				showMessage("loading", "Saving transfer out...", "");

				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'TOCallback',
					data:
					{
						id:"saveTO",
						jsonForItems:order_json_string,
						TOINumber:jQuery("#TOCreateTOINumber").val(),
						statusId:2,
						PackSize:jQuery("#TOCreatePackSize").val(),
						ExpectedDeliveryDate:getDate(jQuery("#TOCreateExpectedDeliveryDate").val()),
						ShippingBillNo:jQuery("#TOCreateShippingWayBillNo").val(),
						ShippingDetails:jQuery("#TOCreateShippingDetails").val(),
						RefNumber:jQuery("#TOCreateRefNo").val(),
						Remarks:jQuery("#TOCreateRemarks").val(),
						functionName:jQuery("#actionName").val(),
						moduleCode:"TSF02",
						GrossWeight:jQuery("#TOCreateGrossWeight").val()
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();

						var saveData = jQuery.parseJSON(data);
						if(saveData.Status==1){
							var keypair=saveData.Result;
							if(keypair.length>=0){

								jQuery.each(keypair,function(key,value)
										{

									showMessage("green","Record Confirmed Successfully "+keypair[key]['TNumber'],"");
									jQuery("#TOCreateTONumber").val(keypair[key]['TNumber']);
									jQuery("#TOCreateStatus").val('shipped');
									EnableDisableOnStatus(2);
										});


							}
							else{
								showMessage("red","Record not confirmed. Error " + keypair ,"");
							}
						}
						else {
							showMessage('red',saveData.Description,'');
						}

					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}

				});
			}
		}
		else{
			return ;
		}
	});



	function validationOnSaveButton	(){

		 if(	vallidateSaveForAllBatchAreInserted()==0){
			//	showMessage("yellow","Mismatched TO Quantity and TOI Quantity. Make them equal.",""); 
				return 0;
			}
		  if(MatchToiQtyAfersave()==0){
			  
			return 0;
		  }
		var TOINumber=jQuery("#TOCreateTOINumber").val();
		if(TOINumber==''){
			jQuery("#txtItemCode").css("background","#FF9999");
			showMessage("yellow","Enter valid TOI Number","");
			return 0;
		}
		else{
			jQuery("#txtItemCode").css("background","white");
		}
		var PackSize=jQuery("#TOCreatePackSize").val();
		if(PackSize==''){
			jQuery("#TOCreatePackSize").css("background","#FF9999");
			showMessage("yellow","Enter no of boxes","");  
			jQuery("#TOCreatePackSize").focus();
			return 0;
		}
		else{

			jQuery("#TOCreatePackSize").css("background","white");
		}

		var ExpectedDeliveryDate=jQuery("#TOCreateExpectedDeliveryDate").val();
		if(ExpectedDeliveryDate==''){
			jQuery("#TOCreateExpectedDeliveryDate").css("background","#FF9999");
			showMessage("yellow","Enter delivery date",""); 
			jQuery("#TOCreateExpectedDeliveryDate").focus();
			return 0;
		}
		else{
			
			jQuery("#TOCreateExpectedDeliveryDate").css("background","white");
		}
		/*var ShippingBillNo=jQuery("#TOCreateShippingWayBillNo").val();
		if(ShippingBillNo==''){
			jQuery("#TOCreateShippingWayBillNo").css("background","#FF9999");
			showMessage("red","Enter shipping bill No.",""); 
			return 0;
		}
		else{
			
			jQuery("#TOCreateShippingWayBillNo").css("background","white");
		}*/
		var ShippingDetails=jQuery("#TOCreateShippingDetails").val();
		if(ShippingDetails==''){
			jQuery("#TOCreateShippingDetails").css("background","#FF9999");
			showMessage("yellow","Enter shipping detail",""); 
			jQuery("#TOCreateShippingDetails").focus();
			return 0;
		}
		else{
			
			jQuery("#TOCreateShippingDetails").css("background","white");
		}
		var RefNumber=jQuery("#TOCreateRefNo").val();
		/*if(RefNumber==''){
			jQuery("#TOCreateRefNo").css("background","#FF9999");
			showMessage("red"," Enter RefNo ?",""); 
			return 0;
		}
		else{
			
			jQuery("#TOCreateRefNo").css("background","white");
		}*/
		/*if(isNaN(jQuery("#TOCreateGrossWeight").val())==true){
			showMessage('red','Enter valid weight','');
			jQuery("#TOCreateGrossWeight").focus();
			return 0;
		}
		if(sumOfQTYWeight('TOItemGrid','Weight','Adjust')>jQuery("#TOCreateGrossWeight").val()){
			showMessage('red','Gross weight cannot be less than total items weight','');
			jQuery("#TOCreateGrossWeight").focus();
			return 0;
		}*/
		
		if(	vallidateSaveTOQty()==0){
			showMessage("yellow","Mismatched TO Quantity and TOI Quantity. Make them equal.",""); 
			return 0;
		}
		
		
		return 1;
	}



	jQuery("#TOCreateRemarks").change(function(){

		jQuery("#TOCreateRemarks").css("background","white");
	});
	jQuery("#TOCreateRefNo").change(function(){

		jQuery("#TOCreateRefNo").css("background","white");
	});
	jQuery("#TOCreateShippingDetails").change(function(){

		jQuery("#TOCreateShippingDetails").css("background","white");
	});
	jQuery("#TOCreateExpectedDeliveryDate").change(function(){
		jQuery("#TOCreateExpectedDeliveryDate").css("background","white");
	});
	jQuery("#TOCreateShippingWayBillNo").change(function(){
		jQuery("#TOCreateShippingWayBillNo").css("background","white");
	});
	jQuery("#TOCreatePackSize").keyup(function(event){
		var intRegex = /^\d+$/;

		if(  !intRegex.test(jQuery("#TOCreatePackSize").val()) && jQuery("#TOCreatePackSize").val()!=''){
			jQuery("#TOCreatePackSize").val('');
			showMessage("yellow","Enter valid pack size .","");
			jQuery("#TOCreatePackSize").focus();
		}
		jQuery("#TOCreatePackSize").css("background","white");
	});
	jQuery("#TOCreateTransferQty").keyup(function(event){
		var intRegex = /^\d+$/;

		if(  !intRegex.test(jQuery("#TOCreateTransferQty").val())&& jQuery("#TOCreateTransferQty").val()!=''){
			jQuery("#TOCreateTransferQty").val('');
			showMessage("yellow","Enter valid Qty .","");
			jQuery("#TOCreateTransferQty").focus();
		}
		jQuery("#TOCreateTransferQty").css("background","white");
	});


	/*  ALL BUTTONS       */	


	jQuery("#btnTSF02Print").unbind("click").click(function(){
		jQuery("#actionName").val('Print');
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"printTO",
				TONumber:jQuery("#TOCreateTONumber").val(),
				type:2,
				functionName:jQuery("#actionName").val(),
				moduleCode:"TSF02"
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				 jQuery.each(results,function(key,value)
						   {
					 			var arr = {'locationId':results[key]['locationId'],'TONumber':results[key]['TONumber']};
					 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
							 
					      });
			}
			else{
				
				showMessage('red',resultsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});

	
	});


});
