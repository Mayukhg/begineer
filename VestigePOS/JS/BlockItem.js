var  jsonForLocations='';
var jsonForGRNSearch='';
var todayDate='';
var loggedInUserPrivileges;
jQuery(document).ready(function(){

	
	
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	});
	
	
	getCurrentLocation();
	var moduleCode = jQuery("#moduleCode").val();
	 todayDate=jQuery("#todayDate").val();
	 
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
			data:
			{
				id:"StockCountModuleFuncHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

				if(loggedInUserPrivilegesForStockCountData.Status == 1)
				{

					loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

					if(loggedInUserPrivileges.length == 0)
					{
						jQuery(".GRNCreateActionButtons").attr('disabled',true);
					}
					else
					{	
						BlockItemLevel();
						disableFieldOnStartup();
						enableFieldOnStartup();
						//GRNSearchStatus();
						//searchWHLocation();
						//vendoreCode();
						showCalender();
						showCalender2();
						showCalender3();
						disableButtons(0);
					}


				}
				else
				{
					showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});

		function showCalender3() {
			jQuery( ".showCalender3" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0',maxDate:0,onSelect:function(){
	         
				jQuery(this).focus();
				if (diffrenInDays(jQuery(this).val(),jQuery("#GRNCreatePODate").val())<0){
					showMessage('yellow','Invoice or Challan Date  cannot be less than PO Date','');
	
				}
	   
	        }});
			//jQuery('.showCalender3').attr('readonly', true);
		}
		function diffrenInDays(DateValue1, DateValue2)
		 {

		 var DaysDiff;
		 Date1 = new Date(DateValue1);
		 Date2 = new Date(DateValue2);
		 DaysDiff = Math.floor((Date1.getTime() - Date2.getTime())/(1000*60*60*24));
            return DaysDiff ;
		 }
		 
		/**
		 * Purchase order look up screen.
		 */
		jQuery("#BlockItemItemCode").keydown(function(e){
			if(e.which == 115) 
			{
				var currentSearchId = this.id;
				//alert(currentItemSearchId);
				showMessage("loading", "Please wait...", "");

				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"BlockItemLookUp",
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();

						//alert(data);
						jQuery("#divLookUp").html(data);

						BlockItemSearchGrid(currentSearchId);
						jQuery("#divLookUp" ).dialog( "open");
						
						BlockItemLookUpData();
						BlockItemLookUpGridDataPopulation();
						/*----------Dialog for item search is there ----------------*/


					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}

				});



			}

		});
		
		function actionButtonsStauts()
		{
			jQuery(".GRNCreateActionButtons").each(function(){

				var buttonid = this.id;
				var extractedButtonId = buttonid.replace("btn","");

				if(loggedInUserPrivileges[extractedButtonId] == 1)
				{

					//jQuery("#"+buttonid).attr('disabled',false);

				}
				else
				{
					jQuery("#"+buttonid).attr('disabled',true);
				}

				jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.

			});
		}

	function searchWHLocation()
	{
		showMessage("loading", "Loading information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOCallback',
			data:
			{
				id:"searchWHLocation",
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			
			var whlocationsData = jQuery.parseJSON(data);
			if(whlocationsData.Status==1){
				jsonForLocations=whlocationsData.Result;
				whlocations=whlocationsData.Result;
				
				jQuery.each(whlocations,function(key,value)
				{	
					jQuery("#GRNDestinationLocaiton").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");

				});
				jQuery('#GRNDestinationLocaiton').val(currentLocationId);
				jQuery('#GRNDestinationLocaiton').attr('disabled',true);
				backGroundColorForDisableField();
			}
			else{
				showMessage('red',whlocationsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}


	function BlockItemLevel()
	{
		showMessage("loading", "Loading information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'BlockItemCallBack',
			data:
			{
				id:"BlockItemLevels",
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var BlockItemLevelData = jQuery.parseJSON(data);
				if(BlockItemLevelData.Status==1){
					BlockItemLevel=BlockItemLevelData.Result;
				
				jQuery.each(BlockItemLevel,function(key,value)
				{	
					jQuery("#BlockItemForLevel").append("<option id=\""+BlockItemLevel[key]['KeyCode1']+"\""+" value=\""+BlockItemLevel[key]['KeyCode1']+"\""+">"+BlockItemLevel[key]['keyvalue1']+"</option>");
				});
			}
				else{
					showMessage('red',BlockItemLevelData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', yearRange: '1950:2010',minDate: +1, maxDate: "100D"});
	/*jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
	jQuery( ".showCalender" ).datepicker("setDate",new Date());
*/	
	
	
	
	
	
	jQuery(function() {
			//alert("dfadf");
			jQuery( "#divGoodReceiptNoteTab" ).tabs(); // Create tabs using jquery ui.
		});
    jQuery("#divGoodReceiptNoteTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
		
		var tabIndex = tab.index; 
		if(tabIndex == 0)
		{
			 disableFieldOnStartup();
			 enableFieldOnStartup();
			 jQuery("#GRNBatchDetailsGrid").jqGrid('clearGridData');
				jQuery("#GRNViewGrid").jqGrid("clearGridData");
				jQuery("#GRNCreatePONumber").val('');
				disableButtons(0);
		}
		else if(tabIndex == 1){
			
		}
		});
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN View grid.
		 */

	jQuery("#BlockItemLocationViewGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['View Items','LocationId','Location Code','Location Name','Location Type','Assigned Url'],
		colModel: [
		           { name: 'ViewItems', index: 'ViewItems', width: 120,search:false},
		           { name: 'LocationId', index: 'LocationId', width: 150,hidden:true},
		           { name: 'LocationCode', index: 'LocationCode', width: 150},
		           { name: 'LocationName', index: 'LocationName', width:150 },
		           { name: 'LocationType', index: 'LocationType', width:100,search:false },
		           { name: 'AssignedUrl', index: 'AssignedUrl', width: 150,search:false}
		           
		           
		           ],
		           pager: jQuery('#PJmap_BlockItemLocationViewGrid'),
		          
		           width:1040,
		           height:200,
		           rowNum: 500,
		           //loadonce:true,
		           rowList: [500],
		           sortname: 'Label',
		           viewrecords: true,
		           gridview: true,
		           sortorder: "asc",
		           hoverrows: true,
		           editurl:'clientArray',
		           ignoreCase: true,
		           shrinkToFit:false,
		           //caption: "Toolbar Searching",
		           afterInsertRow : function(ids)
		           {
	
		        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
	        	  
		           },
		           onSelectRow: function(rowId){
			        	 var LocationId=  jQuery('#BlockItemLocationViewGrid').jqGrid('getCell',rowId, 'LocationId');
			        	 AddBatches();
			        	
			           },
	});
	
	jQuery("#BlockItemLocationViewGrid").jqGrid('navGrid','#PJmap_BlockItemLocationViewGrid',{del:false,add:false,edit:false,search:false});
	jQuery("#BlockItemLocationViewGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});

		function rowIseditableOrNot(rowId){
			var ind=jQuery("#GRNBatchDetailsGrid").getInd(rowId,true);if(ind != false){
			    edited = jQuery(ind).attr("editable");
			}

			if (edited === "1"){
			 return  true ;
			} else{
			   return false ;
			}
		}
		

		

		
	
		jQuery("#BlockItemItemCode").keydown(function(e){
			
		    if(e.which == 13){
		    	e.preventDefault(); 
		    	var ItemCode = jQuery("#BlockItemItemCode").val();
		    	searchItemUsingCode(ItemCode);     
		    	
		    }

		});
		

		function searchItemUsingCode(ItemCode){
			showMessage("loading", "Searching Item Details...", "");
			jQuery("#BlockItemItemViewGrid").jqGrid('clearGridData');
			jQuery("#BlockItemLocationViewGrid").jqGrid('clearGridData');
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'BlockItemCallback',
				data:
				{
					id:"SearchBlockItemDetail",
					ItemCode:ItemCode
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					jsonForGRNSearch=data;
					itemDescData = jQuery.parseJSON(data);
				if(itemDescData.Status==1)
				{
					
					itemDesc=itemDescData.Result;
					if(itemDesc.length!=0)
				
					{
					 jQuery.each(itemDesc,function(key,value)
					 {	
						 jQuery("#BlockItemItemCode").css("background","white");
						jQuery("#BlockItemItemName").val(itemDesc[key]['ItemName']);						
						//jQuery("#BlockItemItemCode").attr('disabled',true);
						
						blockItemLocationDetails(ItemCode);
					
					  });
				    }
					else {
						jQuery("#BlockItemItemCode").css("background","#FF9999");
				        showMessage("yellow","Enter Valid Item Code .","");
				        jQuery("#BlockItemItemCode").focus();
				    	disableFieldOnStartup();
						return;
					}
				}
				else{
					showMessage('red',itemDescData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
			
		}
		
		jQuery("#btnSearchGRNReset").unbind("click").click(function(){
		
		   jQuery("#GRNSearchGrid").jqGrid("clearGridData");
			showCalender();
		});	
		
		
		
		
		
	
		
		
		jQuery(".BlockItemItemCode").focusout(function(e) {
			 var Cellid = this.id;
			 var itemCode = jQuery("#"+Cellid).val();					
			 searchItemDetail(itemCode);					
		});
		jQuery(".BlockItemLocationCode").focusout(function(e) {
			 var Cellid = this.id;
			 var locationCode = jQuery("#"+Cellid).val();					
			 searchLocationDetails(locationCode);					
		});
		
		
		
		
		
		
	function searchItemDetail(ItemCode)
		 {
			 showMessage("loading", "Searching Item Details...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'BlockItemCallback',
					data:
					{
						id:"SearchBlockItemDetail",
						ItemCode:ItemCode
						
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						jsonForGRNSearch=data;
						itemDescData = jQuery.parseJSON(data);
					if(itemDescData.Status==1)
					{
						
						itemDesc=itemDescData.Result;
						if(itemDesc.length!=0)
					
						{
						 jQuery.each(itemDesc,function(key,value)
						 {	
							 jQuery("#BlockItemItemCode").css("background","white");
							jQuery("#BlockItemItemName").val(itemDesc[key]['ItemName']);						
							//jQuery("#BlockItemItemCode").attr('disabled',true);						
						  });
					    }
						else {
							jQuery("#BlockItemItemCode").css("background","#FF9999");
					        showMessage("yellow","Enter Valid Item Code .","");
					        jQuery("#BlockItemItemCode").focus();
					    	disableFieldOnStartup();
							return;
						}
					}
					else{
						showMessage('red',itemDescData.Description,'');
					}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});	 
			 
		 }
		
		
		
		function searchLocationDetails(locationCode){
			showMessage("loading", "Searching for location details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'BlockItemCallback',
				data:
				{
					id:"searchLocationDetail",
					locationCode:locationCode
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();	
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
		if(itemDescData.Status==1){
			
			itemDesc=itemDescData.Result;
			if(itemDesc.length!=0)
					{
				jQuery("#BlockItemLocationCode").css("background","white");
				
				jQuery("#BlockItemLocationViewGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
				jQuery("#BlockItemLocationName").val(itemDesc[key]['Name']);
				jQuery("#BlockItemLocationUrl").val(itemDesc[key]['AssignedURL']);
				/*		var newData = [{
							"ViewItems":'<Button type="button" class="AddBatches" id='+rowid+'>ViewBlockedItem</button>',
							"LocationId":itemDesc[key]['LocationId'],
							"LocationCode":itemDesc[key]['LocationCode'],
							"LocationName":itemDesc[key]['Name'],
							"AssignedUrl":itemDesc[key]['AssignedURL'],
							"LocationType":itemDesc[key]['LocationType']
					
				      	
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#BlockItemLocationViewGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						AddBatches();  */
					 });
					
				    }
			else 	{
							jQuery("#BlockItemLocationCode").css("background","#FF9999");
							jQuery("#BlockItemLocationName").val('');
					        showMessage("yellow","Enter valid location code.","");
					        jQuery("#BlockItemLocationCode").focus();
							return;
					}
				}
		else{
			showMessage('yellow',itemDescData.Description,'');
		}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
			
			
		}
		
		
		
		
		
		jQuery("#btnBIT01Search").unbind("click").click(function(){
			searchBlockedLocations();
		});
		
		function searchBlockedLocations(){
			jQuery("#actionName").val('Search');
			showMessage("loading", "Searching for Locations...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'BlockItemCallback',
				data:
				{
					id:"seachBlockedItemLocation",
					ItemCode:jQuery("#BlockItemItemCode").val(),
					ItemName:jQuery("#BlockItemItemName").val(),
					ForLevel:jQuery("#BlockItemForLevel").val(),
					LocationCode:jQuery("#BlockItemLocationCode").val(),
					LocationName:jQuery("#BlockItemLocationName").val(),
					UserType:jQuery("#UserType").val(),
					//invoiceDate:getDate(jQuery("#GRNCreateInvoiceDate").val()),
					BlockUnblock:jQuery("input:radio[name=BlockItemIsBlocked]:checked").val(),
					Url:jQuery("#BlockItemLocationUrl").val(),
					moduleCode:"BIT01",
					functionName:jQuery("#actionName").val()
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);

	
					if(itemDescData.Status==1){
			
					itemDesc=itemDescData.Result;
					if(itemDesc.length!=0)
					{
				
				jQuery("#BlockItemLocationViewGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
					
						var newData = [{
							"ViewItems":'<Button type="button" class="AddBatches" id='+itemDesc[key]['LocationId']+'>View Items</button>',
							"LocationId":itemDesc[key]['LocationId'],
							"LocationCode":itemDesc[key]['LocationCode'],
							"LocationName":itemDesc[key]['Name'],
							"AssignedUrl":itemDesc[key]['AssignedURL'],
							"LocationType":itemDesc[key]['LocationType']
					
				      	
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#BlockItemLocationViewGrid").jqGrid('addRowData',itemDesc[key]['LocationId'], newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						AddBatches();
					 });
					
				    }
			else 	{
							jQuery("#BlockItemLocationViewGrid").jqGrid("clearGridData");
					        showMessage("yellow","No Record Found","");
							return;
					}
				}
	else{
		showMessage('red',itemDescData.Description,'');
	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
		}
		
		
		function blockItemLocationDetails(ItemCode){
			showMessage("loading", "Searching for location details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'BlockItemCallback',
				data:
				{
					id:"BlockItemLocations",
					ItemCode:ItemCode
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();	
					var rowid =0;
					itemDescData = jQuery.parseJSON(data);
		if(itemDescData.Status==1){
			
			itemDesc=itemDescData.Result;
			if(itemDesc.length!=0)
					{
				
				jQuery("#BlockItemLocationViewGrid").jqGrid("clearGridData");
				jQuery.each(itemDesc,function(key,value){	
					
						var newData = [{
							"ViewItems":'<Button type="button" class="AddBatches" id='+itemDesc[key]['LocationId']+'>View Items</button>',
							"LocationId":itemDesc[key]['LocationId'],
							"LocationCode":itemDesc[key]['LocationCode'],
							"LocationName":itemDesc[key]['Name'],
							"AssignedUrl":itemDesc[key]['AssignedURL'],
							"LocationType":itemDesc[key]['LocationType']
					
				      	
						}] ;

						for (var i=0;i<newData.length;i++) {
							jQuery("#BlockItemLocationViewGrid").jqGrid('addRowData',itemDesc[key]['LocationId'], newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						AddBatches();
					 });
					
				    }
			else 	{
							jQuery("#txtItemCode").css("background","#FF9999");
					        showMessage("yellow","This item is not blocked on any location.","");
					        jQuery("#txtItemCode").focus();
							return;
					}
				}
		else{
			showMessage('yellow',itemDescData.Description,'');
		}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
			
			
		}
		
		function AddBatches(){
			jQuery(".AddBatches").unbind("click").click(function(e){
				e.preventDefault();
					var rowId=this.id;
					var LocationId=jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'LocationId');
					jQuery("#BlockItemLocationCode").val(jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'LocationCode'));
					jQuery("#BlockItemLocationName").val(jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'LocationName'));
					jQuery("#BlockItemLocationUrl").val(jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'AssignedUrl'));
					jQuery('#BlockItemLocationViewGrid').jqGrid('setSelection',rowId);
					jQuery("#BlockItemItemViewGrid").jqGrid('clearGridData');
					getBlockedItemOnLocation(LocationId);
					 
					
				});
			
		}
/*		jQuery(".AddBatches").unbind("click").click(function(e){
			e.preventDefault();
				var rowId=this.id;
				var LocationId=jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'LocationId');
				jQuery("#BlockItemLocationCode").val(jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'LocationCode'));
				jQuery("#BlockItemLocationName").val(jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'LocationName'));
				jQuery("#BlockItemLocationUrl").val(jQuery("#BlockItemLocationViewGrid").jqGrid('getCell', rowId, 'AssignedUrl'));
				jQuery('#BlockItemLocationViewGrid').jqGrid('setSelection',rowId);
				 getBlockedItemOnLocation(LocationId);
				
			});*/
		
	function getBlockedItemOnLocation(LocationId)
		
{
		showMessage("loading", "Searching for item  details...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'BlockItemCallback',
			data:
			{
				id:"BlockedItemOnLocation",
				LocationId:LocationId
				
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();	
				var rowid =0;
				itemDescData = jQuery.parseJSON(data);
	if(itemDescData.Status==1){
		
		itemDesc=itemDescData.Result;
		if(itemDesc.length!=0)
				{
		
			jQuery("#BlockItemItemViewGrid").jqGrid("clearGridData");
			jQuery.each(itemDesc,function(key,value){	
				var qty=parseInt(itemDesc[key]['Quantity']);
				if(qty==''||isNaN(qty)==true)
					{
					qty=0;							
					}
					var newData = [{					
						"ViewLocations":'<Button type="button" class="ViewLocations" id='+rowid+'>View Locations</button>',
						"ItemCode":itemDesc[key]['ItemCode'],
						"ItemName":itemDesc[key]['ItemName'],
						"BlockFrom":displayDate(itemDesc[key]['blockFrom']),
						"BlockTill":displayDate(itemDesc[key]['blockTill']),
						"BlockedLevel":itemDesc[key]['BlockedLevel'],
						"QtyAtLocation":qty
				      	
					}] ;

					for (var i=0;i<newData.length;i++) {
						jQuery("#BlockItemItemViewGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
						rowid=rowid+1;
						}
					AddLocations();
				 });
			jQuery('.ui-icon-pencil').hide();
			    }
		else 	{
						jQuery("#txtItemCode").css("background","#FF9999");
				        showMessage("yellow","No item blocked on this location.","");
						return;
				}
			}
						else{
							showMessage('red',itemDescData.Description,'');
						}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
});			



}
	
	
	function AddLocations(){
		jQuery(".ViewLocations").unbind("click").click(function(e){
			e.preventDefault();
				var rowId=this.id;
				var ItemCode=jQuery("#BlockItemItemViewGrid").jqGrid('getCell', rowId, 'ItemCode');
				var ItemName=jQuery("#BlockItemItemViewGrid").jqGrid('getCell', rowId, 'ItemName');
				jQuery("#BlockItemItemCode").val(ItemCode);
				jQuery("#BlockItemItemName").val(ItemName);
				jQuery('#BlockItemItemViewGrid').jqGrid('setSelection',rowId);
				blockItemLocationDetails(ItemCode);
				
			});
		
	}
	
	
		jQuery( ".requiredList" ).click(function() {
		  if(jQuery("#BlockItemForLevel").val()==3)
			  {
			 jQuery("#UserType").attr('disabled',false); 
			 jQuery("#UserType").css("background","white");
			  }
		  else
			  {
			  jQuery("#UserType").attr('disabled',true);
			  }
		  if(jQuery("#BlockItemForLevel").val()==5)
		  	{
			  jQuery("#BlockItemItemCode").blur();
			  jQuery("#BlockItemItemCode").attr('disabled',true);
		  	}
		  else
		  	{			
			  jQuery("#BlockItemItemCode").attr('disabled',false); 
			  jQuery("#BlockItemItemCode").css("background","white");
		  	}
		});
	
	
		function disableFieldOnStartup(){
			jQuery("#BlockItemItemName").attr('disabled',true);
			jQuery("#BlockItemItemName").val('');
			jQuery("#BlockItemLocationName").attr('disabled',true);
			jQuery("#BlockItemLocationCode").val('');
			jQuery("#BlockItemLocationName").val('');
			jQuery("#BlockItemLocationUrl").val('');
		

		}

		function enableFieldOnStartup(){
			

			jQuery("#BlockItemItemCode").attr('disabled',false);
			jQuery("#BlockItemItemCode").val('');
	
		}

		
		var field1, field2,
	    myCustomCheck = function (value, colname) {
	        if (colname == "Remove") {
	            if(value == 12)
	            {
	            	alert("12 entered");
	            	return [true];
	            }
	            else
	            {
	            	return [false, "some error text"];
	            }
	        } 

	        /*if (field1 !== undefined && field2 !== undefined) {
	            // validate the fields here
	            return [false, "some error text"];
	        } else {
	            return [true];
	        }*/
	        
	      
	    };
	    


	    

	    
	    
	
	    
	    
	    


	    



		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN Batch Details Grid.
		 */
	    function currencyFmatter (cellvalue, options, rowObject)
	    {
	       // do something here
	    	if(jQuery("#GRNViewGrid").getGridParam('selrow')!=undefined){
	    		
	    	return	   jQuery('#GRNViewGrid').jqGrid('getCell',jQuery("#GRNViewGrid").getGridParam('selrow'), 'ItemCode');
	    	}
	    	if(cellvalue!=undefined){
	    		return cellvalue ;
	    	}
	    		
	    		return '';
	    }
	  /*  function currencyFmatter2 (cellvalue, options, rowObject)
	    {
	    	jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_ExpiryDate').focus();
	    	return '';
	    }*/
	    
	    
	    
	    /**
	     * edit param function for jqgrid to edit row data.
	     */
	    
	  
	    myDelOptions = {
				onclickSubmit: function (rp_ge, rowid) {
            rp_ge.processing = true;
         //   if(isNaN(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',rowid, 'ReceivedQty'))==false){
	        	if(jQuery("tr#"+rowid).attr("editable")==0){
	        	checkSave(rowid,'delete');
	        	}
            	jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
            
                jQuery("#delmod" + jQuery("#GRNBatchDetailsGrid")[0].id).hide();
 
                if (jQuery("#GRNBatchDetailsGrid")[0].p.lastpage > 1) {
                	jQuery("#GRNBatchDetailsGrid").trigger("reloadGrid", [{ page: jQuery("#TOItemGrid")[0].p.page}]);
                }
          
            return true;
        },
        processing: true
        };
		
	    
	 /*  var myEditOptions = {
	            keys: true,
	            oneditfunc: function (rowid) {
	               // alert("row with rowid=" + rowid + " is editing.");
	            },
	            aftersavefunc: function (rowid, response, options) {
	            	var savedRowItemCode = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ItemCode');
	            	sumOfQtytobeEdit=getBatchReciveItemSum(savedRowItemCode);
	            	var viewGridRowId=selectRowIdFromItemCode(savedRowItemCode);
	            	if(viewGridRowId!=-1 && vallidateOnAddBatch(rowid,viewGridRowId)!=0){
                        
				            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
				            
				            	updateQuantityInGrid(sumOfQtytobeEdit,viewGridRowId);
				            	
	            	}
	            	else if(viewGridRowId==-1){
	            		//jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
	            		showMessage("red",'Invalid Item Code','');
	            	}
	            }
	      };*/
	    
	/*  var  editparameters = {
	    		"keys" : false,
	    		"oneditfunc" : null,
	    		"successfunc" : null,
	    		"url" : 'clientArray',
	    	        "extraparam" : {},
	    		"aftersavefunc" : null,
	    		"errorfunc": null,
	    		"afterrestorefunc" : null,
	    		"restoreAfterError" : true,
	    		"mtype" : "POST"
	    	};
	    */
	    jQuery("#BlockItemItemViewGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['ViewLocations','Item Code','Item Name','Block From','Block Till','Blocked Level','Qty At Location'],
			colModel: [

			           { name: 'ViewLocations', index: 'ViewLocations', width: 150,editable:false,search:false },
			           { name: 'ItemCode', index: 'ItemCode', width: 150,editable:false,sorttype:'string'},
			
			           { name: 'ItemName', index: 'ItemName', width: 150,editable:false },
			         			           
			           { name: 'BlockFrom', index: 'BlockFrom', width:150,editable:false,search:false },
			           
			           { name: 'BlockTill', index: 'BlockTill', width:150,editable:false,search:false,hidden:true },
			           { name: 'BlockedLevel', index: 'BlockedLevel', width:150,editable:false,search:false },
			         
			           { name: 'QtyAtLocation', index: 'QtyAtLocation', width:150,editable:false,search:false },
			        	
			          ],
			         
			           pager: jQuery('#PJmap_BlockItemItemViewGrid'),

			           width:1040,
			           height:200,
			           rowNum: 500,
			           //loadonce:true,
			           rowList: [500],
			           sortname: 'Label',
			           viewrecords: true,
			           gridview: true,
			           sortorder: "asc",
			           hoverrows: true,
			           editurl:'clientArray',
			           shrinkToFit:false,
			           ignoreCase: true,
			           afterInsertRow : function(ids)
			           {
		
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
			        	  
			           }
			      

			          
		});
	    jQuery("#BlockItemItemViewGrid").jqGrid('navGrid','#PJmap_BlockItemItemViewGrid',{del:false,add:false,edit:false,search:false});
		jQuery("#BlockItemItemViewGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
				
		function saveditableRows(){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    		if(jQuery("tr#"+allRowIds[index]).attr("editable")==1 || rowIseditableOrNot(allRowIds[index]))
	       		{
	    			 if(  jQuery("#GRNBatchDetailsGrid").jqGrid('saveRow',allRowIds[index])==true){
			        	   checkSave(allRowIds[index],'');
			        	 }
	    			 else{
	    				 return 0;
	    			 }
	       		}
	    	}
	    	 return 1;
	    }
		jQuery("#GRNCreateChallanNo").change(function(){
			
			jQuery("#GRNCreateChallanNo").css("background","white");
		});
		jQuery("#GRNCreateInvoiceNo").change(function(){
			
			jQuery("#GRNCreateInvoiceNo").css("background","white");
		});
		
		function validationOnUpdateButton ()
		{
			if(	jQuery("#BlockItemLocationCode").val()==''){
				jQuery("#BlockItemLocationCode").css("background","#FF9999");
				showMessage('yellow','Enter Valid Location Code .','');
				jQuery("#BlockItemLocationCode").focus();
				return 0;
				}
				else{
					jQuery("#BlockItemLocationCode").css("background","white");
				}
			if(	jQuery("#BlockItemLocationUrl").val()==''){
				jQuery("#BlockItemLocationUrl").css("background","#FF9999");
				showMessage('yellow','Enter Valid Url .','');
				jQuery("#BlockItemLocationUrl").focus();
				return 0;
				}
				else{
					jQuery("#BlockItemLocationUrl").css("background","white");
				}
			return 1;
		}
		
		function validationOnSaveButton (){
			
			if(	jQuery("#BlockItemForLevel").val()!=5){
				if(	jQuery("#BlockItemItemCode").val()==''){
				jQuery("#BlockItemItemCode").css("background","#FF9999");
				showMessage('yellow','Enter Valid Item Code .','');
				jQuery("#BlockItemItemCode").focus();
				return 0;
				}
				else{
					jQuery("#BlockItemItemCode").css("background","white");
				}
			}
		
				
				
				
				
				
				if(	jQuery("#BlockItemForLevel").val()==-1){
					jQuery("#BlockItemForLevel").css("background","#FF9999");
					showMessage('yellow','Select Level .','');
					jQuery("#BlockItemForLevel").focus();
					return 0;
					}
					else{
						jQuery("#BlockItemForLevel").css("background","white");
					}
				if(	jQuery("#BlockItemLocationCode").val()==''){
					if(jQuery("#BlockItemForLevel").val()==3)
					  {
						  if(jQuery("#UserType").val()!='WebUser2')
							  {
								jQuery("#BlockItemLocationCode").css("background","#FF9999");
								showMessage('yellow','Enter valid location code .','');
								jQuery("#BlockItemLocationCode").focus();
								return 0;
							  	
							  }
						  else
							  {
							  jQuery("#BlockItemLocationCode").blur();
							  }
						 
						 
					  }
					else
						{
						jQuery("#BlockItemLocationCode").css("background","#FF9999");
						showMessage('yellow','Enter valid location code .','');
						jQuery("#BlockItemLocationCode").focus();
						return 0;						
						}
				
					}
					else{
						jQuery("#BlockItemLocationCode").css("background","white");
					}
				if(	jQuery("input:radio[name=BlockItemIsBlocked]:checked").val()==''||isNaN(jQuery("input:radio[name=BlockItemIsBlocked]:checked").val())==true){
					jQuery("#BlockItemIsBlocked").css("background","#FF9999");
					showMessage('yellow','Select Block/Unblock  .','');
					jQuery("#BlockItemIsBlocked").focus();
					return 0;
					}
					else{
						jQuery("#BlockItemIsBlocked").css("background","white");
					}
				  if(jQuery("#BlockItemForLevel").val()==3)
				  {
					  if(jQuery("#UserType").val()=='Select')
						  {
						  	showMessage('yellow','Select User  .','');
						  	jQuery("#UserType").css("background","#FF9999");
						  	jQuery("#UserType").focus();
						  	return 0;
						  }
					  else
						  {
						  jQuery("#UserType").css("background","white");
						  }
					 
				  }
				
			return 1;
		}
		
		
				
	jQuery("#btnBIT01Save").unbind("click").click(function(){
		jQuery("#actionName").val('Save');
		if(validationOnSaveButton ()==0)
			{
			
			return;
			}
		var r =confirm("Are you sure you want to save data.");
		if(r==true)
			{
			  showMessage("loading", "Saving details...", "");
				jQuery.ajax({
					type:'POST',
							url:Drupal.settings.basePath + 'BlockItemCallback',
							data:
							{
							id : 'saveBlockItem',
							ItemCode:jQuery("#BlockItemItemCode").val(),
							ItemName:jQuery("#BlockItemItemName").val(),
							ForLevel:jQuery("#BlockItemForLevel").val(),
							LocationCode:jQuery("#BlockItemLocationCode").val(),
							LocationName:jQuery("#BlockItemLocationName").val(),
							UserType:jQuery("#UserType").val(),
							//invoiceDate:getDate(jQuery("#GRNCreateInvoiceDate").val()),
							BlockUnblock:jQuery("input:radio[name=BlockItemIsBlocked]:checked").val(),
							Url:jQuery("#BlockItemLocationUrl").val(),
							moduleCode:"BIT01",
							functionName:jQuery("#actionName").val()
		
							},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var keypairData = jQuery.parseJSON(data);
						if(keypairData.Status==1){
						keypair=keypairData.Result;
						if(keypair.length>=0){
							
					jQuery.each(keypair,function(key,value)
						{	
							if(keypair[key]['BlockUnblock']==1) {
							showMessage("green","Item Code: "+keypair[key]['ItemCode']+" Blocked Successfully .","");
							  /*enableDisableOnStatus(1);*/
							}
							else
								{
							showMessage("green","Item Code: "+keypair[key]['ItemCode']+" Unblocked Successfully .","");
								}
							
						});
					 }
						else{
							showMessage("yellow","Details not saved. Error " + keypairData,"");
						}
				    
					}
					else{
						showMessage('red',keypairData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
					});
			}




});
	
		
	
	jQuery("#btnBIT01Update").unbind("click").click(function(){
		jQuery("#actionName").val('Update');
				if(validationOnUpdateButton ()==0)
					{
					return;
					}
				var r =confirm("Are you sure you want to update Url. ?");
				if(r==true)
					{
					  showMessage("loading", "Updating Url...", "");
						jQuery.ajax({
							type:'POST',
							url:Drupal.settings.basePath + 'BlockItemCallBack',
							data:
							{
								id : 'UpdateUrl',
								LocationCode:jQuery("#BlockItemLocationCode").val(),
								LocationName:jQuery("#BlockItemLocationName").val(),
								LocationUrl:jQuery("#BlockItemLocationUrl").val(),
								moduleCode:"BIT01",
								functionName:jQuery("#actionName").val()
							
							},
							success:function(data)
							{
								jQuery(".ajaxLoading").hide();
								var keypairData = jQuery.parseJSON(data);
								if(keypairData.Status==1){
								keypair=keypairData.Result;
								if(keypair.length>=0){
									
							jQuery.each(keypair,function(key,value)
								{	
									showMessage("green","URL Updated Successfully for location Code: "+keypair[key]['LocationCode'],"");
									  /*enableDisableOnStatus(1);*/
									
								});
							 }
								else{
									showMessage("yellow","Url not updated. Error " + keypairData,"");
								}
						    
							}
							else{
								showMessage('red',keypairData.Description,'');
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
							});
					}
	
	 
	
		
	});
	
		 /*jQuery("#btnBIT01Print").click(function(){
			 	moduleCode="BIT01" ;
				functionName=jQuery("#actionName").val() ;
				var arr = {'GRNNo':jQuery("#GRNNo").val(),'PONumber':jQuery("#GRNCreatePONumber").val()};
				showReport('reports/GRNInvoice.rptdesign',arr); 
			
		 });*/
		 
		 
		 jQuery("#btnReset").click(function(){
			 disableFieldOnStartup();
			 enableFieldOnStartup();
			 jQuery("#BlockItemItemViewGrid").jqGrid('clearGridData');
				jQuery("#BlockItemLocationViewGrid").jqGrid("clearGridData");
				jQuery("#BlockItemQuantity").val('');
				jQuery("#BlockItemForLevel").val(-1);
				disableButtons(0);
				jQuery("#GRNCreatePONumber").css("background","white");
				jQuery("#GRNCreateChallanNo").css("background","white");
				jQuery("#GRNCreateInvoiceNo").css("background","white");
				jQuery("#GRNCreateInvoiceDate").css("background","white");
				jQuery("#GRNCreateChallanDate").css("background","white");
				jQuery("ui-search-toolbar").hide();
		 });
		 
		 function disableButtons(statusId){
				     //onStartup
				 if(statusId==0){
						jQuery("#btnBIT01Print").attr('disabled',false);		
						jQuery("#btnBIT01Update").attr('disabled',false);	
						jQuery("#btnBIT01Save").attr('disabled',false);
						jQuery("#btnBIT01Search").attr('disabled',false);
						jQuery("#GRNBatchDetailsGrid").showCol('myac');
						jQuery("#GRNViewGrid").showCol('AddBatches');
						disableEnableFieldOnStatus(0);
						
					}
				//CREATED
				if(statusId==1){
					
					jQuery("#btnBIT01Print").attr('disabled',false);	
					jQuery("#btnBIT01Cancel").attr('disabled',false);	
					jQuery("#btnBIT01Update").attr('disabled',false);	
					jQuery("#btnBIT01Save").attr('disabled',false);
					jQuery("#btnBIT01Search").attr('disabled',false);
					jQuery("#GRNBatchDetailsGrid").showCol('myac');
					jQuery("#GRNViewGrid").showCol('AddBatches');
					disableEnableFieldOnStatus(0);
				}
				//cancel
				else if(statusId==2){
					jQuery("#btnBIT01Print").attr('disabled',false);	
					jQuery("#btnBIT01Cancel").attr('disabled',true);	
					jQuery("#btnBIT01Update").attr('disabled',true);	
					jQuery("#btnBIT01Save").attr('disabled',true);
					jQuery("#btnBIT01Search").attr('disabled',false);
					jQuery("#GRNBatchDetailsGrid").hideCol('myac');
					jQuery("#GRNViewGrid").hideCol('AddBatches');
					disableEnableFieldOnStatus(1);
				}
				//closing
				else if(statusId==3){
					jQuery("#btnBIT01Print").attr('disabled',false);	
					jQuery("#btnBIT01Cancel").attr('disabled',true);	
					jQuery("#btnBIT01Update").attr('disabled',true);	
					jQuery("#btnBIT01Save").attr('disabled',true);
					jQuery("#btnBIT01Search").attr('disabled',false);	
					jQuery("#GRNBatchDetailsGrid").hideCol('myac');
					jQuery("#GRNViewGrid").hideCol('AddBatches');
					disableEnableFieldOnStatus(1);
				}
				actionButtonsStauts();
				
				
			}
	function disableEnableFieldOnStatus(IsDisable){
	if(IsDisable==1)
	{
		//jQuery("#GRNCreateInvoiceDate").attr('disabled',true);	

		
	}		
	else{
		//jQuery("#GRNCreateInvoiceDate").attr('disabled',false);	
		
	}
		
	}		
});		
