jQuery(document).ready(function(){

		
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * Short Inventory Message grid 
		 */
		jQuery("#ShortInventoryGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Order No.','Item Name','Available Qty.','Required Qty.'],
			colModel: [{ name: 'OrderNo', index: 'OrderNo', width:100 },
			           { name: 'ItemName', index: 'ItemName', width: 150},
			           { name: 'AvailableQty', index: 'AvailableQty', width: 150},
			           { name: 'RequiredQty', index: 'RequiredQty', width: 100}],
			           pager: jQuery('#PJmap_ShortInventoryGrid'),
			           width:400,
			           height:300,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,


			           afterInsertRow : function(ids)
			           {
//			        	   alert("ids is" + ids);
			        	   var index1 = jQuery("#ShortInventoryGrid").jqGrid('getCol','Name',false);
//			        	   alert("after insert row" + index1);
			        	   jQuery(this).jqGrid('setRowData', rowid, false,{'background-color':'white'});
//			        	   jQuery("tr.jqgrow").css("background", "#DDDDDC");
			           },

			           loadComplete: function() {
//			        	   jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
//			        	   alert("laodign complete");
			           }	
			           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

		});

		jQuery("#ShortInventoryGrid").jqGrid('navGrid','#PJmap_ShortInventoryGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_ShortInventoryGrid").hide();
		
		
		function getShortInventoryData(data)
		{
			alert(data);
		}
		
		
		
});	