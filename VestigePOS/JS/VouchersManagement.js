var  jsonForLocations='';
var jsonForGRNSearch='';
var todayDate='';
var loggedInUserPrivileges;
jQuery(document).ready(function(){
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	});
	
	getCurrentLocation();
	var moduleCode = jQuery("#moduleCode").val();
	 todayDate=jQuery("#todayDate").val();
	 
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
			data:
			{
				id:"StockCountModuleFuncHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

				if(loggedInUserPrivilegesForStockCountData.Status == 1)
				{

					loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

					if(loggedInUserPrivileges.length == 0)
					{
						jQuery(".Voucher").attr('disabled',true);
					}
					else
					{	
					
					}


				}
				else
				{
					showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});

		function diffrenInDays(DateValue1, DateValue2)
		 {

		 var DaysDiff;
		 Date1 = new Date(DateValue1);
		 Date2 = new Date(DateValue2);
		 DaysDiff = Math.floor((Date1.getTime() - Date2.getTime())/(1000*60*60*24));
            return DaysDiff ;
		 }
		 
		/**
		 * Purchase order look up screen.
		 */

		
		jQuery("#DistributoridBonus").keydown(function(e){
			
			if(e.which == 13 || e.which == 9) 
			{
				Distributorid=jQuery('#DistributoridBonus').val();
				if(Distributorid=='')
					{
					showMessage('yellow','Enter DistributorId.','');
					}
				else
					{
					
				bonusid=this.id;
				ValidateDistributor(Distributorid,bonusid);
			}
			}
		});
		
jQuery("#Distributorid").keydown(function(e){
			
			if(e.which == 13 || e.which == 9) 
			{
				Distributorid=jQuery('#Distributorid').val();
				if(Distributorid=='')
					{
					showMessage('yellow','Enter DistributorId.','');
					}
				else
					{
					
				distid=this.id;
				ValidateDistributor(Distributorid,distid);
			}
			}
		});

jQuery("#Distributoridpass").keydown(function(e){
	
	if(e.which == 13 || e.which == 9) 
	{
		Distributorid=jQuery('#Distributoridpass').val();
		if(Distributorid=='')
			{
			showMessage('yellow','Enter DistributorId.','');
			}
		else
			{
			
		distid=this.id;
		ValidateDistributor(Distributorid,distid);
	}
	}
});
		
		jQuery("#DistributoridGift").keydown(function(e){
			
			if(e.which == 13 || e.which == 9) 
				
			{
				Distributorid=jQuery('#DistributoridGift').val();
				if(Distributorid=='')
				{
				showMessage('yellow','Enter DistributorId.','');
				}
			else
				{
				giftid=this.id;
				
				ValidateDistributor(Distributorid,giftid);
			}
			}
		});
		
	function ValidateDistributor(Distributorid,id)	
		{
			
			var flag = DistributorValidation2(Distributorid,id);
			if (flag==0)
				{
				showMessage('yellow','Enter Valid DistributorId.','');
				}
			else
				{
				jQuery("#"+id).css("background","#FFFFFF");
			//alert(currentItemSearchId);
			showMessage("loading", "Please wait...", "");

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VouchersManagementCallBack',
				data:
				{
					Distributorid: Distributorid,
					id:"ValidateDistributor",
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var keypairData = jQuery.parseJSON(data);
					if(keypairData.Status==1){
					keypair=keypairData.Result;
					if(keypair.length!=0){
						
				jQuery.each(keypair,function(key,value)
					{
					if (keypair[key]['distributorstatus']=='')
						{
						showMessage("red","Please Enter Distributorid.");
						}
					else if(keypair[key]['distributorstatus']==1 || keypair[key]['distributorstatus']==2)
					{
						jQuery('#MobNo').val(keypair[key]['distributormobnumber']);
						jQuery("#MobNo").attr('readonly',true);
						}
						else
							{
							showMessage("red","Distributor is Blocked.");
							jQuery("#"+id).css("background","#FF9999");
							jQuery("#"+id).focus();
							}
						
					});
				 }
					else{
						showMessage("red","Distributor Does not Exist.");
						jQuery("#"+id).css("background","#FF9999");
						jQuery("#"+id).focus();
					}
			    
				}
				else{
					showMessage('red',keypairData.Description,'');
				}
					


				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});

				}

		}
	

		
		function DistributorValidation2(Distributorid,id)
		{
			
			if(Distributorid!='')
				{
			if(Distributorid.length!=8)
				{
				jQuery('#'+id).css("background","#FF9999");
				
				return 0;
				}
			var intonly =new RegExp('^[0-9]*$');
			if(intonly.test(Distributorid))
				{
				return 1;
				}
			else 
				{
				jQuery('#'+id).css("background","#FF9999");
				
				return 0;
				}
				}
			return 1;
		}
		




	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'dd-MM-yy', yearRange: '1950:2010',minDate: +1, maxDate: "100D"});
	//jQuery( "#BonusExpiry" ).datepicker({changeMonth: true, changeYear: true,dateFormat: 'dd-MM-yy', yearRange: '1950:2099'});
	jQuery( "#BonusExpiry" ).datepicker("setDate",new Date());
	
	/*jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
	jQuery( ".showCalender" ).datepicker("setDate",new Date());
	 */	
	
	
	
	
	
	jQuery(function() {
			//alert("dfadf");
			jQuery( "#divGoodReceiptNoteTab" ).tabs(); // Create tabs using jquery ui.
		});
    jQuery("#divGoodReceiptNoteTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
		
		var tabIndex = tab.index; 
		if(tabIndex == 0)
		{
			 jQuery("#VouchersManagementGrid").jqGrid('clearGridData');
			    jQuery("#BonusAmount").attr('readonly',false);
				jQuery("#DistributoridBonus").attr('readonly',false);
				jQuery("#DistributoridBonus").val('');
				jQuery("#Bonus").val('');
				jQuery("#BonusExpiry").val('');
				jQuery("#BonusAmount").val('');
				
		}
		else if(tabIndex == 1){
			 jQuery("#GiftManagementGrid").jqGrid('clearGridData');
			    jQuery("#GiftAmount").attr('readonly',false);
				jQuery("#DistributoridGift").attr('readonly',false);
				jQuery("#DistributoridBonus").val('');
				jQuery("#GiftVoucherNo").val('');
				jQuery("#GiftExpiry").val('');
				jQuery("#GiftAmount").val('');
				
				jQuery( "#GiftExpiry" ).datepicker("setDate",new Date());
		}
		else if(tabIndex == 3){
			jQuery("#Distributoridpass").val('');
			jQuery("#MobNo").attr('readonly',false);
			jQuery("#MobNo").val('');
		}
		});
		
		function rowIseditableOrNot(rowId){
			var ind=jQuery("#GRNBatchDetailsGrid").getInd(rowId,true);if(ind != false){
			    edited = jQuery(ind).attr("editable");
			}

			if (edited === "1"){
			 return  true ;
			} else{
			   return false ;
			}
		}


				
		jQuery("#BonusSearch").unbind("click").click(function(){
			var Bonusid='DistributoridBonus';
			var Distributorid=jQuery('#DistributoridBonus').val();
			var Bonus=jQuery('#Bonus').val();
			if (Distributorid=='' && Bonus=='')
				{
				showMessage('yellow','Please Enter DistributorId/VoucherNo.','');
				}
			else
			searchVoucherDetails(Distributorid,Bonus,Bonusid);
		});
		
		jQuery("#GiftSearch").unbind("click").click(function(){
			var Giftid='DistributoridGift';
			var Distributorid=jQuery('#DistributoridGift').val();
			var Gift=jQuery('#GiftVoucherNo').val();
			if (Distributorid=='' && Gift=='')
				{
				showMessage('yellow','Please Enter DistributorId/VoucherNo.','');
				}
			else
				SearchGiftVoucherDetails(Distributorid,Gift,Giftid);
		});
		
		jQuery("#DistributorSearch").unbind("click").click(function(){
			var distid=this.id;
			var Distributorid=jQuery("#Distributorid").val();
			if (Distributorid=='')
			{
				showMessage('yellow','Please Enter DistributorId.','');
				}
			var flag = DistributorValidation2(Distributorid,distid);
			if (flag==0)
				{
				showMessage('yellow','Enter Valid DistributorId.','');
				}
			showMessage("loading", "Searching for DistributorDetails...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VouchersManagementCallBack',
				data:
				{
					id:"SearchDistributorDetails",
					Distributorid: Distributorid,
					moduleCode:"VOD01",
					functionName:jQuery("#actionName").val()
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();

					var historyOrderData = jQuery.parseJSON(data);

					jQuery("#OnlineOrdersGrid").jqGrid("clearGridData"); 
					if(historyOrderData.Status == 1)
					{
						var searchResult = historyOrderData.Result;
						if(searchResult.length==0)
						{ 	
							showMessage("","No Record Found","");
						}
						
						else
						{	

							jQuery.each(searchResult,function(key,value){	
								
								if (searchResult[key]['Distributorstatus']==2 && searchResult[key]['Firstordertaken']==1)
									{
									jQuery("#joining").attr('checked', true);
									}
								else 
									jQuery("#joining").attr('checked', false);
								
								jQuery('#DistributorName').val(searchResult[key]['Name']);
								jQuery("#DistributorName").attr('readonly',true);
								
								jQuery('#Contact').val(searchResult[key]['DistributorMobNumber']);
								jQuery("#Contact").attr('readonly',true);
								
								jQuery('#Joining').val(searchResult[key]['DateOfJoining']);
								jQuery("#Joining").attr('readonly',true);
								var historyrowid=0;
								var newData = [{
									"DistributorId":searchResult[key]['DistributorId'],"OrderStatus":searchResult[key]['KeyValue1'],"OrderNo":searchResult[key]['CustomerOrderNo'],"CreatedDate":searchResult[key]['Date'] }];

								for (var i=0;i<searchResult.length;i++) {
									if(searchResult[key]['CustomerOrderNo']!=''){
									jQuery("#OnlineOrdersGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
									historyrowid=historyrowid+1;
									}
								}
							
								
							});

						}
					}
					else
					{
						showMessage("red", historyOrderData.Description, "");
					}
			}
			,
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
			
		});
		
		function searchVoucherDetails(Distributorid,Bonus,Bonusid){
			jQuery("#actionName").val('Search');
			var flag = DistributorValidation2(Distributorid,Bonusid);
			if (flag==0)
				{
				showMessage('yellow','Enter Valid DistributorId .','');
				}
			showMessage("loading", "Searching for VoucherDetails...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VouchersManagementCallBack',
				data:
				{
					id:"SearchVoucherDetails",
					Distributorid: Distributorid,
					Bonus : Bonus,
					moduleCode:"VOD01",
					functionName:jQuery("#actionName").val()
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();

					var historyOrderData = jQuery.parseJSON(data);
					
					if(historyOrderData.Status == 1)
					{
						
						
						
						var historyrowid=0;
						var searchTO = historyOrderData.Result;
						if(searchTO.length==0)
						{
							jQuery("#VouchersManagementGrid").jqGrid("clearGridData"); 	
							showMessage("","No Record Found","");
						}

						else
						{	
							var message = "Total " + searchTO.length + " records found.";
							showMessage("", message,"");
							
							/*['Add To Log','Print','Invoice','Order No','Status','Log No'],*/
							jQuery("#VouchersManagementGrid").jqGrid("clearGridData"); 	
							jQuery .each(searchTO,function(key,value){	
								
								

								var newData = [{
									"DistributorId":searchTO[key]['Distributorid'],"VoucherNo":searchTO[key]['ChequeNo'],"ExpiryDate":searchTO[key]['ExpiryDate'],"Amount":searchTO[key]['Amount'],"CreatedDate":searchTO[key]['CreatedDate']
								,"MobileNo":searchTO[key]['DistributorMobNumber'],"InvoiceNo":searchTO[key]['InvoiceNo'],"status":searchTO[key]['status']
								}];

								for (var i=0;i<newData.length;i++) {
									jQuery("#VouchersManagementGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
									
									historyrowid=historyrowid+1;
								}
								
							});

						}
					}
					else
					{
						showMessage("red", historyOrderData.Description, "");
					}
			}
			,
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
		}
		
		
		function SearchGiftVoucherDetails(Distributorid,Gift,Giftid){
			//jQuery("#actionName").val('Search');
			var flag = DistributorValidation2(Distributorid,Giftid);
			if (flag==0)
				{
				showMessage('yellow','Enter Valid DistributorId .','');
				}
			showMessage("loading", "Searching for Gift Voucher Details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'VouchersManagementCallBack',
				data:
				{
					id:"SearchGiftDetails",
					Distributorid: Distributorid,
					Gift : Gift,
					moduleCode:"VOD01",
				    functionName:jQuery("#actionName").val()
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();

					var historyOrderData = jQuery.parseJSON(data);
					
					if(historyOrderData.Status == 1)
					{
						
						
						
						var historyrowid=0;
						var searchTO = historyOrderData.Result;
						if(searchTO.length==0)
						{
							jQuery("#GiftManagementGrid").jqGrid("clearGridData"); 	
							showMessage("","No Record Found","");
						}

						else
						{	
							var message = "Total " + searchTO.length + " records found.";
							showMessage("", message,"");
							
							/*['Add To Log','Print','Invoice','Order No','Status','Log No'],*/
							jQuery("#GiftManagementGrid").jqGrid("clearGridData"); 	
							jQuery .each(searchTO,function(key,value){	
								
								

								var newData = [{
									"DistributorId":searchTO[key]['IssuedTo'],"VoucherNo":searchTO[key]['VoucherSrNo'],"ExpiryDate":searchTO[key]['ApplicableTo'],"Amount":searchTO[key]['MinBuyAmount'],
									"CreatedDate":searchTO[key]['CreatedDate'],"InvoiceNo":searchTO[key]['InvoiceNo'],"Availed":searchTO[key]['Availed'],"MobileNo":searchTO[key]['DistributorMobNumber']  
								}];

								for (var i=0;i<newData.length;i++) {
									jQuery("#GiftManagementGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
									historyrowid=historyrowid+1;
								}
							});

						}
					}
					else
					{
						showMessage("red", historyOrderData.Description, "");
					}
			}
			,
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
		}
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * GRN Batch Details Grid.
		 */
	    function currencyFmatter (cellvalue, options, rowObject)
	    {
	       // do something here
	    	if(jQuery("#GRNViewGrid").getGridParam('selrow')!=undefined){
	    		
	    	return	   jQuery('#GRNViewGrid').jqGrid('getCell',jQuery("#GRNViewGrid").getGridParam('selrow'), 'ItemCode');
	    	}
	    	if(cellvalue!=undefined){
	    		return cellvalue ;
	    	}
	    		
	    		return '';
	    }
	  /*  function currencyFmatter2 (cellvalue, options, rowObject)
	    {
	    	jQuery("#"+jQuery("#GRNBatchDetailsGrid").getGridParam('selrow')+'_ExpiryDate').focus();
	    	return '';
	    }*/
	    
	    
	    
	    /**
	     * edit param function for jqgrid to edit row data.
	     */
	    
	  
	    myDelOptions = {
				onclickSubmit: function (rp_ge, rowid) {
            rp_ge.processing = true;
         //   if(isNaN(jQuery('#GRNBatchDetailsGrid').jqGrid('getCell',rowid, 'ReceivedQty'))==false){
	        	if(jQuery("tr#"+rowid).attr("editable")==0){
	        	checkSave(rowid,'delete');
	        	}
            	jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
            
                jQuery("#delmod" + jQuery("#GRNBatchDetailsGrid")[0].id).hide();
 
                if (jQuery("#GRNBatchDetailsGrid")[0].p.lastpage > 1) {
                	jQuery("#GRNBatchDetailsGrid").trigger("reloadGrid", [{ page: jQuery("#TOItemGrid")[0].p.page}]);
                }
          
            return true;
        },
        processing: true
        };
		
	    
	 /*  var myEditOptions = {
	            keys: true,
	            oneditfunc: function (rowid) {
	               // alert("row with rowid=" + rowid + " is editing.");
	            },
	            aftersavefunc: function (rowid, response, options) {
	            	var savedRowItemCode = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ItemCode');
	            	sumOfQtytobeEdit=getBatchReciveItemSum(savedRowItemCode);
	            	var viewGridRowId=selectRowIdFromItemCode(savedRowItemCode);
	            	if(viewGridRowId!=-1 && vallidateOnAddBatch(rowid,viewGridRowId)!=0){
                        
				            	var savedRowItemReceivedQty = jQuery('#GRNBatchDetailsGrid').jqGrid('getCell', rowid, 'ReceivedQty');
				            
				            	updateQuantityInGrid(sumOfQtytobeEdit,viewGridRowId);
				            	
	            	}
	            	else if(viewGridRowId==-1){
	            		//jQuery("#GRNBatchDetailsGrid").jqGrid('delRowData', rowid);
	            		showMessage("red",'Invalid Item Code','');
	            	}
	            }
	      };*/
	    
	/*  var  editparameters = {
	    		"keys" : false,
	    		"oneditfunc" : null,
	    		"successfunc" : null,
	    		"url" : 'clientArray',
	    	        "extraparam" : {},
	    		"aftersavefunc" : null,
	    		"errorfunc": null,
	    		"afterrestorefunc" : null,
	    		"restoreAfterError" : true,
	    		"mtype" : "POST"
	    	};
	    */
	    

	    jQuery("#VouchersManagementGrid").jqGrid({
	        
	    	
	        datatype: 'jsonstring',
	        colNames: ['DistributorId','VoucherNo','ExpiryDate','Amount','CreatedDate','InvoiceNo','status','MsgDate','MobileNo','SendMessage'],
	        colModel: [
	                   { name: 'DistributorId', index: 'DistributorId', width:100},
	                   { name: 'VoucherNo', index: 'VoucherNo', width:160 },
	                   { name: 'ExpiryDate', index: 'ExpiryDate', width:160},
	                   { name: 'Amount', index: 'Amount', width:100 },
	                   { name: 'CreatedDate', index: 'CreatedDate', width:100},
	                   { name: 'InvoiceNo', index: 'InvoiceNo', width:160},
	                   { name: 'status', index: 'status', width:160, hidden:true},
	                   { name: 'MsgDate', index: 'MsgDate', width:160, hidden:true},
	                   { name: 'MobileNo', index: 'MobileNo', width:160,hidden:true},
	                   { name: 'SendMessage', index: 'SendMessage', width:150, edittype:'checkbox',formatter: 'checkbox',editoptions: { value:"True:False"},classes:'SendAllBonus',editable:true,formatoptions: {disabled : false}},
	                  ],
	                   pager: jQuery('#VouchersManagementPager'),
	                   width:1000,
	                   height:300,
	                   rowNum: 1000,
	                   rowList: [500],
	                   sortable: true,
	                   sortorder: "asc",
	                   hoverrows: true,
	                   shrinkToFit: false,
	                   
	                
	                   
	                  /* rowattr: function (historyrowid){
	   	    			
	               		if (jQuery("#VouchersManagementGrid").jqGrid('getCell', historyrowid, 'status')!=1) {
	               			return {
	               	            "class": "ui-state-disabled ui-jqgrid-disablePointerEvents"
	               	        }
	               		  
	               		,*/
	                   afterInsertRow:function() {
	                	   var count=jQuery("#VouchersManagementGrid").jqGrid('getGridParam', 'records');
	                	   for(var i=0;i<count;i++)
	                		   {
	                		   if (jQuery("#VouchersManagementGrid").jqGrid('getCell', i, 'status')!=1) {
	                			   jQuery('#' + jQuery.jgrid.jqID(i)).addClass('ui-state-disabled ui-jqgrid-disablePointerEvents');
	                		   }
	                		   }
	                   },
	           	   
	                   		
	    onSelectRow: function (historyrowid)
        {
     	   jQuery("#DistributoridBonus").val(jQuery("#VouchersManagementGrid").jqGrid('getCell', historyrowid, 'DistributorId'));
     	   jQuery("#Bonus").val(jQuery("#VouchersManagementGrid").jqGrid('getCell', historyrowid, 'VoucherNo'));
     	   jQuery("#BonusExpiry").val(jQuery("#VouchersManagementGrid").jqGrid('getCell', historyrowid, 'ExpiryDate'));
     	   jQuery("#BonusAmount").val(jQuery("#VouchersManagementGrid").jqGrid('getCell', historyrowid, 'Amount'));
     	  jQuery("#BonusCreated").val(jQuery("#VouchersManagementGrid").jqGrid('getCell', historyrowid, 'CreatedDate'));
     	   jQuery("#BonusAmount").attr('readonly',true);
     	   jQuery("#DistributoridBonus").attr('readonly',true);
     	  jQuery("#BonusCreated").attr('readonly',true);
     	 jQuery("#BonusExpiry").attr('readonly',true);
        }


	       });
	    	
	    	jQuery("#VouchersManagementGrid").jqGrid('navGrid','#VouchersManagementPager',{edit:false,add:false,del:false}); 
	    	jQuery("#VouchersManagementPager").hide();	

	    	
	    	/*Grid structure For Gift Management  */
	    	
	    	jQuery("#GiftManagementGrid").jqGrid({
		        
		        datatype: 'jsonstring',
		        colNames: ['DistributorId','VoucherNo','ExpiryDate','Min Buy Amount','CreatedDate','InvoiceNo','Availed','giftDate','SendMessage','MobileNo'],
		        colModel: [
		                   { name: 'DistributorId', index: 'DistributorId', width:100},
		                   { name: 'VoucherNo', index: 'VoucherNo', width:160 },
		                   { name: 'ExpiryDate', index: 'ExpiryDate', width:110},
		                   { name: 'Amount', index: 'Amount', width:160 },
		                   { name: 'CreatedDate', index: 'CreatedDate', width:130 },
		                   { name: 'InvoiceNo', index: 'InvoiceNo', width:150},
		                   { name: 'Availed', index: 'Availed', width:150, hidden:true},
		                   {name: 'giftDate', index: 'giftDate', width:160, hidden:true},
		                   { name: 'SendMessage', index: 'SendMessage', width:150, edittype:'checkbox',formatter: 'checkbox',editoptions: { value:"True:False"},classes:'SendToAll',editable:true,formatoptions: {disabled : false}},
		                   { name: 'MobileNo', index: 'MobileNo', width:160,hidden:true },
		                  ],
		                   pager: jQuery('#GiftManagementPager'),
		                   width:1000,
		                   height:300,
		                   rowNum: 1000,
		                   rowList: [500],
		                   sortable: true,
		                   sortorder: "asc",
		                   hoverrows: true,
		                   shrinkToFit: false,
		                   afterInsertRow:function() {
		                	   var count=jQuery("#GiftManagementGrid").jqGrid('getGridParam', 'records');
		                	   for(var i=0;i<count;i++)
		                		   {
		                		   if (jQuery("#GiftManagementGrid").jqGrid('getCell', i, 'Availed')==1) {
		                			   jQuery('#' + jQuery.jgrid.jqID(i)).addClass('ui-state-disabled ui-jqgrid-disablePointerEvents');
		                		   }
		                		   }
		                   },
                          onSelectRow:function(historyrowid)
                          {
                        	  jQuery("#DistributoridGift").val(jQuery("#GiftManagementGrid").jqGrid('getCell', historyrowid, 'DistributorId'));
          	                 jQuery("#GiftVoucherNo").val(jQuery("#GiftManagementGrid").jqGrid('getCell', historyrowid, 'VoucherNo'));
          	                 jQuery("#GiftExpiry").val(jQuery("#GiftManagementGrid").jqGrid('getCell', historyrowid, 'ExpiryDate'));
          	                 jQuery("#GiftCreated").val(jQuery("#GiftManagementGrid").jqGrid('getCell', historyrowid, 'CreatedDate'));
          	                 jQuery("#GiftAmount").val(jQuery("#GiftManagementGrid").jqGrid('getCell', historyrowid, 'Amount'));
          	                 jQuery("#GiftAmount").attr('readonly',true);
          	                jQuery("#GiftCreated").attr('readonly',true);
          	                 jQuery("#DistributoridGift").attr('readonly',true);
          	               jQuery("#GiftExpiry").attr('readonly',true);
                          }


		       });
		    	
		    	jQuery("#GiftManagementGrid").jqGrid('navGrid','#GiftManagementPager',{edit:false,add:false,del:false}); 
		    	jQuery("#GiftManagementPager").hide();	
		    	
		    	/*jQuery("#SelectAll").change(function(){
					
					if (jQuery("#SelectAll").is(':checked')){
						
						jQuery(".SendToAll").find('input[type=checkbox]').attr('checked',true);  
						
					}
					else{
						
						jQuery(".SendToAll").find('input[type=checkbox]').attr('checked',false);  
					}
				});
		    	
                 jQuery("#SelectBonus").change(function(){
					
					if (jQuery("#SelectBonus").is(':checked')){
						
						jQuery(".SendAllBonus").find('input[type=checkbox]').attr('checked',true);  
						
					}
					else{
						
						jQuery(".SendAllBonus").find('input[type=checkbox]').attr('checked',false);  
					}
				});*/
				
		    	jQuery("#OnlineOrdersGrid").jqGrid({
			        
			        datatype: 'jsonstring',
			        colNames: ['DistributorId','OrderStatus','OrderNo','CreatedDate','Activate'],
			        colModel: [
			                   { name: 'DistributorId', index: 'DistributorId', width:200},
			                   { name: 'OrderStatus', index: 'OrderStatus', width:200},
			                   { name: 'OrderNo', index: 'OrderNo', width:200 },
			                   { name: 'CreatedDate', index: 'CreatedDate', width:200 },
			                   { name: 'Activate', index: 'Activate', width:200, edittype:'checkbox',formatter: 'checkbox',editoptions: { value:"True:False"},classes:'SendToAll',editable:true,formatoptions: {disabled : false}},
			                  ],
			                   pager: jQuery('#OnlineOrdersGridPager'),
			                   width:1000,
			                   height:300,
			                   rowNum: 1000,
			                   rowList: [500],
			                   sortable: true,
			                   sortorder: "asc",
			                   hoverrows: true,
			                   shrinkToFit: false

			       });
			    	
			    	jQuery("#OnlineOrdersGrid").jqGrid('navGrid','#OnlineOrdersGridPager',{edit:false,add:false,del:false}); 
			    	jQuery("#OnlineOrdersGridPager").hide();
		    	
		    	
		    	function saveditableRows(){
	    	var allRowIds=jQuery('#GRNBatchDetailsGrid').jqGrid('getDataIDs');
	    	for(var index=0;index<allRowIds.length;index++){
	    		if(jQuery("tr#"+allRowIds[index]).attr("editable")==1 || rowIseditableOrNot(allRowIds[index]))
	       		{
	    			 if(  jQuery("#GRNBatchDetailsGrid").jqGrid('saveRow',allRowIds[index])==true){
			        	   checkSave(allRowIds[index],'');
			        	 }
	    			 else{
	    				 return 0;
	    			 }
	       		}
	    	}
	    	 return 1;
	    }
		jQuery("#GRNCreateChallanNo").change(function(){
			
			jQuery("#GRNCreateChallanNo").css("background","white");
		});
		jQuery("#GRNCreateInvoiceNo").change(function(){
			
			jQuery("#GRNCreateInvoiceNo").css("background","white");
		});
		
		function validationOnUpdateButton ()
		{
			
				
			return 1;
		}
		
		
				
	jQuery("#BonusSave").unbind("click").click(function(){
		jQuery("#actionName").val('Save');
		if(jQuery("#DistributoridBonus").val()=='' || jQuery("#Bonus").val()=='' || jQuery("#BonusExpiry").val()=='')
			{
			showMessage("yellow",'Please select a Voucher from the list below.','');
			}
		else
		{
		var r =confirm("Are you sure you want to Extend the Expiry Date?");
		if(r==true)
			{
			  showMessage("loading", "Saving details...", "");
			  jQuery("#VouchersManagementGrid").jqGrid('clearGridData');
				jQuery.ajax({
					type:'POST',
							url:Drupal.settings.basePath + 'VouchersManagementCallBack',
							data:
							{
							id : 'SaveVoucher',
							Distributorid : jQuery("#DistributoridBonus").val(),
							Bonus: jQuery("#Bonus").val(),
							Expiry : jQuery("#BonusExpiry").val(),
							moduleCode:"VOD01",
							functionName:jQuery("#actionName").val()		
							},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						itemDescData = jQuery.parseJSON(data);
						if(itemDescData.Status==1)
						{
							showMessage("green","Voucher no: "+jQuery("#Bonus").val()+" Updated Successfully .","");
						}
						else
						{
							showMessage('red',itemDescData.Description,'');
						}

							
	
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
					});
			}



	}
});

	jQuery("#GiftSave").unbind("click").click(function(){
		jQuery("#actionName").val('Save');
		if(jQuery("#DistributoridGift").val()=='' || jQuery("#GiftVoucherNo").val()=='' || jQuery("#GiftExpiry").val()=='')
		{
		showMessage("yellow",'Please select a Voucher from the list below.','');
		}
	else
	{
		var r =confirm("Are you sure you want to Extend the Expiry Date?");
		if(r==true)
			{
			  showMessage("loading", "Saving details...", "");
			  jQuery("#GiftManagementGrid").jqGrid('clearGridData');
				jQuery.ajax({
					type:'POST',
							url:Drupal.settings.basePath + 'VouchersManagementCallBack',
							data:
							{
							id : 'SaveGiftVoucher',
							Distributorid : jQuery("#DistributoridGift").val(),
							Expiry : jQuery("#GiftExpiry").val(),
							Gift : jQuery("#GiftVoucherNo").val(),
							moduleCode:"VOD01",
							functionName:jQuery("#actionName").val()		
							},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						itemDescData = jQuery.parseJSON(data);
						if(itemDescData.Status==1)
						{
							showMessage("green","Voucher no: "+jQuery("#GiftVoucherNo").val()+" Updated Successfully .","");
						}
						else
						{
							showMessage('red',itemDescData.Description,'');
						}

							
	
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
					});
			}


	}

});
	
	jQuery("#DistributorSave").unbind("click").click(function(){
		jQuery("#actionName").val('Save');
		var flag =0;
		var Distributorid =jQuery("#Distributorid").val();
		var JoiningDate =jQuery("#Joining").val();
		var Repurchase=jQuery("#joining").is(':checked')? 1 : 0;
		flag=ValidateJoining(Distributorid,JoiningDate,flag,Repurchase);
		if(flag==0)
			{
		var r =confirm("Are you sure you want to Activate Joining.");
		if(r==true)
			{
			  showMessage("loading", "Saving details...", "");
			  
				jQuery.ajax({
					type:'POST',
							url:Drupal.settings.basePath + 'VouchersManagementCallBack',
							data:
							{
							id : 'SaveDistributordetails',
							Distributorid : jQuery("#Distributorid").val(),
							moduleCode : "VOD01",
							functionName:jQuery("#actionName").val()		
							},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var itemDescData = jQuery.parseJSON(data);
						if(itemDescData.Status==1)
						{

							var resuts=itemDescData.Result;
							jQuery.each(resuts,function(key,value){
								showMessage("green","Joining for DistributorId: "+resuts[key]['DistributorId']+' ' +"is activated Successfully .","");
							});
						}
						else
						{
							showMessage('red',itemDescData.Description,'');
						}


							
	
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
					});
			}


			}

});
	
	jQuery("#OrderActivate").unbind("click").click(function(){
		jQuery("#actionName").val('Save');

		var r =confirm("Are you sure you want to Activate Orders.");
		if(r==true)
			{
				 var count=jQuery("#OnlineOrdersGrid").jqGrid('getGridParam', 'records');
			     var flag=0;
			     for(var i=0; i<count; i++)
			    	 {
			    	 if(jQuery("#OnlineOrdersGrid").jqGrid('getCell', i, 'Activate')=='True')
			    		 {
			    		 flag++;
			    		 }
			    	 }
				 
				 if(flag==0)
					 {
					 showMessage("yellow",'Please select the checkbox to send Message.','');
					 }
				 else{
					 var MessageGrid= jQuery("#OnlineOrdersGrid").jqGrid('getRowData');
					 var myJsonString = JSON.stringify(MessageGrid);
					 ActivateOrders(myJsonString,count);
			         }
			}
});
	
	function ValidateJoining(Distributorid,JoiningDate,flag,Repurchase)
	
	{
		if(Distributorid=='')
			{
			showMessage("yellow",'Please Enter DistributorId.','');
			flag++;
			return flag;
			}
		
		if(JoiningDate=='')
		{
		showMessage("yellow",'Please Click on Search button Fisrt.','');
		flag++;
		return flag;
		}
		
		if(JoiningDate!=jQuery.datepicker.formatDate('dd M yy', new Date()))
		{
		showMessage("yellow",'Only todays Joining will be Activated.','');
		flag++;
		return flag;
		}
		
		if(Repurchase==1)
		{
		showMessage("yellow",'Repurchase Activation not allowed!','');
		flag++;
		return flag;
		}
		
		return flag;
	}
	
		 /*jQuery("#btnBIT02Print").click(function(){
			 	moduleCode="BIT02" ;
				functionName=jQuery("#actionName").val() ;
				var arr = {'GRNNo':jQuery("#GRNNo").val(),'PONumber':jQuery("#GRNCreatePONumber").val()};
				showReport('reports/GRNInvoice.rptdesign',arr); 
			
		 });*/
		 
		 
		 jQuery("#btnReset").click(function(){
			 jQuery("#VouchersManagementGrid").jqGrid('clearGridData');
			    jQuery("#BonusAmount").attr('readonly',false);
				jQuery("#DistributoridBonus").attr('readonly',false);
				jQuery("#BonusExpiry").attr('readonly',false);
				jQuery("#DistributoridBonus").val('');
				jQuery("#Bonus").val('');
				jQuery("#BonusExpiry").val('');
				jQuery("#BonusAmount").val('');
				jQuery("#BonusCreated").val('');
				jQuery("#DistributoridBonus").css("background","#FFFFFF");
				
		 });
		 
		 jQuery("#DistPassReset").click(function(){
				jQuery("#MobNo").attr('readonly',false);
				jQuery("#MobNo").val('');
				jQuery("#Distributoridpass").val('');
				
		 });
		 
		 jQuery("#btnResetgift").click(function(){
			 jQuery("#GiftManagementGrid").jqGrid('clearGridData');
			    jQuery("#GiftAmount").attr('readonly',false);
				jQuery("#DistributoridGift").attr('readonly',false);
				jQuery("#GiftExpiry").attr('readonly',false);
				jQuery("#DistributoridGift").val('');
				jQuery("#GiftVoucherNo").val('');
				jQuery("#GiftExpiry").val('');
				jQuery("#GiftAmount").val('');
				jQuery("#GiftCreated").val('');
				jQuery("#DistributoridGift").css("background","#FFFFFF");
				
		 });
	
		 jQuery("#DistReset").click(function(){
			    jQuery("#Distributorid").val('');
			    jQuery("#DistributorName").attr('readonly',false);
				jQuery("#DistributorName").val('');
				jQuery("#Contact").attr('readonly',false);
				jQuery("#Contact").val('');
				jQuery("#Joining").attr('readonly',false);
				jQuery("#Joining").val('');
				jQuery("#joining").attr('checked', false);
				jQuery("#Distributorid").css("background","#FFFFFF");
				jQuery("#OnlineOrdersGrid").jqGrid("clearGridData"); 
				
		 });
		 
		 jQuery("#BonusMessage").click(function(){
			 
	    if(jQuery("#DistributoridBonus").val()=='' || jQuery("#Bonus").val()=='' || jQuery("#BonusExpiry").val()=='')
				{
				showMessage("yellow",'Please select a Voucher from the list below.','');
				}
	    else{
	     var count=jQuery("#VouchersManagementGrid").jqGrid('getGridParam', 'records');
	     var flag=0;
	     for(var i=0; i<count; i++)
	    	 {
	    	 if(jQuery("#VouchersManagementGrid").jqGrid('getCell', i, 'SendMessage')=='True')
	    		 {
	    		 flag++;
	    		 }
	    	 }
		 
		 if(flag==0)
			 {
			 showMessage("yellow",'Please select the checkbox to send Message.','');
			 }
		 else{
			 var MessageGrid= jQuery("#VouchersManagementGrid").jqGrid('getRowData');
			 var myJsonString = JSON.stringify(MessageGrid);
		 sendmessage(myJsonString,count);
		 }
	    }
		 });
		
		 jQuery("#GiftMessage").click(function(){
			 if(jQuery("#DistributoridGift").val()=='' || jQuery("#GiftVoucherNo").val()=='' || jQuery("#GiftExpiry").val()=='')
				{
				showMessage("yellow",'Please select a Voucher from the list below.','');
				}
			 else{
		     var giftcount=jQuery("#GiftManagementGrid").jqGrid('getGridParam', 'records');
		     var flag=0;
		     for(var i=0; i<giftcount; i++)
		    	 {
		    	 if(jQuery("#GiftManagementGrid").jqGrid('getCell', i, 'SendMessage')=='True')
	    		 {
	    		 flag++;
	    		 }
	    	 }
		 
		 if(flag==0)
			 {
			 showMessage("yellow",'Please select the checkbox to send Message.','');
			 }
		 else{
			 var MessageGrid= jQuery("#GiftManagementGrid").jqGrid('getRowData');
			 var myJsonString = JSON.stringify(MessageGrid);
			 sendGiftmessage(myJsonString,giftcount);
		 }
			 }
			 });
		 
		 function sendmessage(myJsonString,count)  	
	    	{
			 
	    	   showMessage("loading", "Sending Message...", "");
	    	   
				  
					jQuery.ajax({
						type:'POST',
								url:Drupal.settings.basePath + 'VouchersManagementCallBack',
								data:
								{
								id : 'SendMessage',
								myJsonString : myJsonString,
								count : count
								},							
						success:function(data)
						{
							
							jQuery(".ajaxLoading").hide();
							itemDescData = jQuery.parseJSON(data);
							if(itemDescData.Status==1)
							{
								showMessage("green","Message Sent Successfully!","");
							}
							else
							{
								showMessage('red',itemDescData.Description,'');
							}
					},	
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
						});
					
				}
		 
		 function sendGiftmessage(myJsonString,giftcount)  	
	    	{
			 
	    	   showMessage("loading", "Sending Message...", "");
	    	   
				  
					jQuery.ajax({
						type:'POST',
								url:Drupal.settings.basePath + 'VouchersManagementCallBack',
								data:
								{
								id : 'SendGiftMessage',
								myJsonString : myJsonString,
								giftcount : giftcount
								},							
						success:function(data)
						{
							
							jQuery(".ajaxLoading").hide();
							itemDescData = jQuery.parseJSON(data);
							if(itemDescData.Status==1)
							{
								showMessage("green","Message Sent Successfully!","");
							}
							else
							{
								showMessage('red',itemDescData.Description,'');
							}
					},	
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
						});
					
				}
		 
		 function ActivateOrders(myJsonString,count)  	
	    	{
			 
	    	   showMessage("loading", "Activating Orders...", "");
	    	   
				  
					jQuery.ajax({
						type:'POST',
								url:Drupal.settings.basePath + 'VouchersManagementCallBack',
								data:
								{
								id : 'ActivateOrders',
								myJsonString : myJsonString,
								count : count,
								moduleCode : "VOD01",
								functionName:jQuery("#actionName").val()
								},							
						success:function(data)
						{
							
							jQuery(".ajaxLoading").hide();
							itemDescData = jQuery.parseJSON(data);
							if(itemDescData.Status==1)
							{
								showMessage("green","Orders are Activated Successfully!","");
							}
							else
							{
								showMessage('red',itemDescData.Description,'');
							}
					},	
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
						});
					
				}
		 

		 jQuery("#sendpass").unbind("click").click(function(){
			 var MobNo=jQuery("#MobNo").val();
			 if(MobNo=='')
				 {
				 showMessage('yellow','Validate DistributorId press TAB.','');
				 }
			 showMessage("loading", "Sending Message...", "");
	    	   
			  
				jQuery.ajax({
					type:'POST',
							url:Drupal.settings.basePath + 'VouchersManagementCallBack',
							data:
							{
							id : 'SendPassword',
							MobNo : MobNo,
							Type : 'PASSWORD'
							},							
					success:function(data)
					{
						
						jQuery(".ajaxLoading").hide();
						var result=jQuery.trim(data);
					    var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
					    if(numericReg.test(result))
					    	{
					    showMessage("green","Message Sent Successfully!","");
					    	}
					    else
					    	showMessage("red", result, "");	

				},	
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
					});
				
			 
		 });
		 

});		
