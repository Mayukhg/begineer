//SC - Stock Count.

var  jsonForLocations='';
var itemGridId = 0;
var batchGridId = 0;
var oItemBatchData;
var oBucket;
var tempBucket;
var oBucketJson;

var loggedInUserPrivilegesForStockCount;

var oitemData;

jQuery(document).ready(function(){
	

	
	var moduleCode = jQuery("#moduleCode").val();
	
	jQuery(function() {
		
		jQuery( "#CRConfirmedDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0'});
	});	
	
	
	
	
	
	jQuery("#getScreenshot").click(function(){
		
		html2canvas(document.body, {
		    onrendered: function(canvas) {
		        // canvas is the actual canvas element,
		          // to append it to the page call for example
		    	
		    	//document.getElementById("canvasImage").src = canvas.toDataURL();
		    	
		    	
		    	var dataURL = canvas.toDataURL();
		    	
		          // document.body.appendChild( canvas );
		           
		           var blobBin = atob(dataURL.split(',')[1]);
		           var array = [];
		           for(var i = 0; i < blobBin.length; i++) {
		               array.push(blobBin.charCodeAt(i));
		           }
		           var file=new Blob([new Uint8Array(array)], {type: 'image/png'});


		           var formdata = new FormData();
		           formdata.append("myNewFileName", file);
		          // formdata.append("moduleCode", moduleCode);
		           jQuery.ajax({
		              url: "/sites/all/modules/VestigePOS/VestigePOS/Business/UploadFile.php",
		              type: "POST",
		              data: formdata,
		              processData: false,
		              contentType: false,
		           }).done(function(respond){
		             alert(respond);
		           });   
		           
		        
		    }
		});
		
	});
	
	//Security Regarding to Stock Count.
	
	
	
	/*disableAllButtonOnScreen()saaS;
	
	function disableAllButtonOnScreen()
	{
		jQuery(".SCCreateActionButtons").attr('disabled',true);
	}*/
	
	getCurrentLocation();
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
		data:
		{
			id:"StockCountModuleFuncHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);
			
			if(loggedInUserPrivilegesForStockCountData.Status == 1)
			{
				
				loggedInUserPrivilegesForStockCount  = loggedInUserPrivilegesForStockCountData.Result;
				
				if(loggedInUserPrivilegesForStockCount.length == 0)
				{
					jQuery(".SCCreateActionButtons").attr('disabled',true);
				}
				else
				{
					actionButtonsStauts();
					
					checkingStatusForButton("btnINV02Initiate"); //function chekcing  status for one particular button.
				}
				
				
				
			}
			else
			{
				showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
	
	
	function actionButtonsStauts()
	{
		jQuery(".SCCreateActionButtons").each(function(){
			
			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");
			
			if(loggedInUserPrivilegesForStockCount[extractedButtonId] == 1)
			{
				
				//jQuery("#"+buttonid).attr('disabled',false);
				
			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}
			
			jQuery("#btnINV01Save").attr('disabled',false);
			jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			jQuery("#btnSCCreatePopulateFields").attr('disabled',false); //temporary button for filling all fields.
			
		});
	}
	
	
	var WHLocation;
	
	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery("#batchGridId").hide();
	
	/*disableScreen();
	
	function disableScreen()
	{
		jQuery(".divStockCountOuter *").children().attr("disabled","disabled");
	}*/
	
	jQuery(".SCCreateHiddenDiv").hide(); //hide item addition field in stock count.
	
	
	
	function checkingStatusForButton(buttonid)
	{
		
		var extractedButtonId = buttonid.replace("btn","");
		
		if(loggedInUserPrivilegesForStockCount[extractedButtonId] == 1)
		{
			statesOnScreen(4); //Initial state of screen.
		}
		else
		{
			jQuery(".SCCreateActionButtons").attr('disabled',true); //disable on event on screen.
		}
		
		
		jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
		jQuery("#btnSCCreatePopulateFields").attr('disabled',false); //temporary button for filling all fields.
		
	}
	
	//initialDisabledFields();
	
	function initialDisabledFields()
	{
		jQuery("#SCCreateInitiatedDate").attr('disabled',false);
		jQuery("#SCCreateLocationCode").attr('disabled',true);
		jQuery("#SCCreateStatus").attr('disabled',true);
		jQuery("#SCCreateInitiatedBy").attr('disabled',true);
		jQuery("#SCCreateConfirmedDate").attr('disabled',true);
		jQuery("#SCCreateConfirmedBy").attr('disabled',true);
		jQuery("#SCStcokCountNo").attr('disabled',true);
	}
	
	
	// //Initial state on screen.
	
	//jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', yearRange: '1950:2010',minDate: +1, maxDate: "100D"});
	
		jQuery(function() {
			
			jQuery( "#divStockCountTab" ).tabs(); // Create tabs using jquery ui.
		});
		
		
		/*initializingDates(); //Initialize dates on the screen.
		
		
		function initializingDates()
		{
			jQuery( ".showCalender" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
			jQuery( ".showCalender" ).datepicker("setDate",new Date());
		}*/
		
		
		
	//	searchWHLocation();
		//searchBucket();
	searchWHLocationOnSearchTab();
		SCStatus();
		showCalender();
		
		//CRStatus();
		//CRCustomerType();
		//CRApproveBy();
		function searchWHLocation()
		{
			showMessage("loading", "Loading data...", "");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
				data:
				{
					id:"searchWHLocation",
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var WHLocationData = jQuery.parseJSON(data);
					
					if(WHLocationData.Status == 1)
					{
						jsonForLocations=data;
						var whlocations = WHLocationData.Result;
						WHLocation =  WHLocationData.Result;
						
						jQuery.each(whlocations,function(key,value)
						{	
							jQuery("#SCCreateLocation").append("<option id=\""+whlocations[key]['LocationCode']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
			
						});
						
						jQuery('#SCCreateLocation').val(currentLocationId);
						jQuery('#SCCreateLocation').attr('disabled',true);
						
						jQuery.each(WHLocation,function(key,value){
							
							if(WHLocation[key]['LocationId'] == currentLocationId)
							{
								jQuery("#SCCreateLocationCode").val(WHLocation[key]['LocationCode']);
							}
							else
							{
								
							}
							
						});
						
						//jQuery("#SCCreateLocationCode").attr('disabled',true);
						
						
					}
					else
					{
						showMessage("red", WHLocationData.Description, '');
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		function searchWHLocationOnSearchTab()
		{
			showMessage("loading", "Loading data...", "");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
				data:
				{
					id:"searchWHLocation",
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var WHLocationData = jQuery.parseJSON(data);
					
					if(WHLocationData.Status == 1)
					{
						jsonForLocations=data;
						var whlocations = WHLocationData.Result;
						WHLocation =  WHLocationData.Result;
						
						jQuery.each(whlocations,function(key,value)
						{	
							jQuery("#SCSearchLocation").append("<option id=\""+whlocations[key]['LocationCode']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
			
						});
						jQuery('#SCSearchLocation').val(currentLocationId);
						
					}
					else
					{
						showMessage("red", WHLocationData.Description, '');
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		/**
		 * function used to get status.
		 */
		function SCStatus()
		{
			
			/*jQuery("#CRLocation").append("<option id=\""+-1+"\""+" value=\""+-1+"\""+">"+"Select"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+1+"\""+" value=\""+1+"\""+">"+"Created"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+2+"\""+" value=\""+2+"\""+">"+"Cancel"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+3+"\""+" value=\""+3+"\""+">"+"Initiate"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+4+"\""+" value=\""+4+"\""+">"+"Processed"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+5+"\""+" value=\""+5+"\""+">"+"Executed"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+6+"\""+" value=\""+6+"\""+">"+"Closed"+"</option>");
			
			jQuery.each(oStockCountStatusData,function(key,value)
			{	
				jQuery("#CRLocation").append("<option id=\""+oStockCountStatusData[key]['KeyCode1']+"\""+" value=\""+oStockCountStatusData[key]['KeyCode1']+"\""+">"+oStockCountStatusData[key]['KeyValue1']+"</option>");

			});
			
			jQuery("#CRLocation").append("<option id=\""+7+"\""+" value=\""+7+"\""+">"+"Saved"+"</option>");
			jQuery("#CRLocation").append("<option id=\""+8+"\""+" value=\""+8+"\""+">"+"Confirmed"+"</option>");
			*/
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"stockCountStatusInfo",
				},
				success:function(data)
				{
					var stockCountStatusData = jQuery.parseJSON(data);
					
					if(stockCountStatusData.Status == 1)
					{
						
						var oStockCountStatusData = stockCountStatusData.Result;
						
						/*jQuery("#CRLocation").append("<option id=\""+-1+"\""+" value=\""+-1+"\""+">"+"Select"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+1+"\""+" value=\""+1+"\""+">"+"Created"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+2+"\""+" value=\""+2+"\""+">"+"Cancel"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+3+"\""+" value=\""+3+"\""+">"+"Initiate"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+4+"\""+" value=\""+4+"\""+">"+"Processed"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+5+"\""+" value=\""+5+"\""+">"+"Executed"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+6+"\""+" value=\""+6+"\""+">"+"Closed"+"</option>");
						*/
						jQuery("#CRLocation").append("<option id=\""+-1+"\""+" value=\""+-1+"\""+">"+"Select"+"</option>");
						
						jQuery.each(oStockCountStatusData,function(key,value)
						{	
							jQuery("#CRLocation").append("<option id=\""+oStockCountStatusData[key]['KeyCode1']+"\""+" value=\""+oStockCountStatusData[key]['KeyCode1']+"\""+">"+oStockCountStatusData[key]['KeyValue1']+"</option>");
			
						});
						
						/*jQuery("#CRLocation").append("<option id=\""+7+"\""+" value=\""+7+"\""+">"+"Saved"+"</option>");
						jQuery("#CRLocation").append("<option id=\""+8+"\""+" value=\""+7+"\""+">"+"Confirmed"+"</option>");*/
					}
					else
					{
						showMessage("red", stockCountStatusData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		/**
		 * function used to get all bucket.
		 */
		/*function searchBucket()
		{
			
			jQuery.ajax({
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"searchBucket",
				},
				success:function(data)
				{
					var bucketData = jQuery.parseJSON(data);
					
					if(bucketData.Status == 1)
					{
						var bucket = bucketData.Result;
						
						tempBucket = bucket;
						
						try
						{
							oBucket = bucket;

							jQuery.each(oBucket,function(key,value){

								delete oBucket[key]['Sellable'];

							});
							
							var jsonLength = oBucket.length;
							
							var sBucketJson = "{";
							var i = 0;
							jQuery.each(oBucket,function(key,value){
								
								
								if(i == jsonLength-1)
								{
									sBucketJson = sBucketJson +"\""+oBucket[key]['BucketId']+"\"" +":"+ "\""+oBucket[key]['BucketName']+"\""+"}";
									
									oBucketJson = jQuery.parseJSON(sBucketJson);
									
									itemsjQgrid(oBucketJson); 

									return false;
								}
								else
								{
									i+=1;
									sBucketJson = sBucketJson +"\""+oBucket[key]['BucketId']+"\"" +":"+ "\""+oBucket[key]['BucketName']+"\""+",";
									
								}
								
							});
							
							//alert(jsonString);
						}
						catch(err)
						{
							
						}
						
						jQuery.each(bucket,function(key,value){	

							jQuery("#SCCreateBucket").append("<option id=\""+bucket[key]['BucketId']+"\""+" value=\""+bucket[key]['BucketId']+"\""+">"+bucket[key]['BucketName']+"</option>");
						});
					}
					else
					{
						
					}
					
					
				}
			
			});
		}*/
		
		
		
		
		/*Stock Count search grid*/
		jQuery("#SCSearchGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['Action','Stock Count No. ','Location Name','LocationId','Location Address','Initiated Date','Initiated By','Executed Date','Executed By','Status','StatusId'],
			colModel: [{ name: 'Action', index: 'Action', width: 50},
			           { name: 'StockCountNo', index: 'StockCountNo', width: 150},
			           { name: 'LocationName', index: 'LocationName', width:80 },
			           { name: 'LocationId', index: 'LocationId', width:80,hidden:true },
			           { name: 'LocationAddress', index: 'LocationAddress', width: 150},
			           { name: 'InitiatedDate', index: 'InitiatedDate', width: 150},
			           { name: 'InitiatedBy', index: 'InitiatedBy', width: 100},
			           { name: 'ExecutedDate', index: 'ExecutedDate', width: 70},
			           { name: 'ExecutedBy', index: 'ExecutedBy', width: 70},
			           { name: 'StatusName', index: 'StatusName', width: 100},
			           { name: 'Status', index: 'Status', width: 100,hidden:true}
			          ],
			           pager: jQuery('#PJmap_SCSearchGrid'),
			           width:1040,
			           height:460,
			           rowNum: 1000,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,


			       /*    ondblClickRow: function (id) {
			        	   
			        	   
			        	   	var rowData = jQuery('#SCSearchGrid').jqGrid ('getRowData', id);
			        	   	
			        	   	/*---------populate stock count fields---------------*/ 
			        	   	
			        	/*   	jQuery("#SCCreateStatus").val('');

			    			jQuery("#SCCreateInitiatedDate").val('');

			    			jQuery("#SCCreateInitiatedBy").val('');

			    			jQuery("#SCCreateConfirmedDate").val('');
			    			
			    			jQuery("#SCCreateConfirmedBy").val('');
			    			
			    			jQuery("#SCStcokCountNo").val('');
			        	   	
			        	   	
			        	   	jQuery("#SCCreateLocation").val(rowData.LocationId);
			        	    jQuery("#SCCreateStatus").val(rowData.StatusName);
			        	    
			        	 	var stockCountStatus = parseInt(rowData.Status);
			        	    
			        	   	jQuery("#SCCreateLocationCode").val(rowData.LocationName);
			        	   	
			        	   	jQuery("#SCCreateInitiatedDate").val(formatDateForDisplay(rowData.InitiatedDate));
			        	   	jQuery("#SCCreateInitiatedBy").val(rowData.InitiatedBy);
			        	   	jQuery("#SCCreateConfirmedDate").val(formatDateForDisplay(rowData.ExecutedDate));
			        	   	jQuery("#SCCreateConfirmedBy").val(rowData.ExecutedBy);
			        	   	jQuery("#SCStcokCountNo").val(rowData.StockCountNo);
			        	   	
			        	   	
			        	   	jQuery("#divStockCountTab").tabs( "select", "DivCreate" );
			        	   	
			        	   	if(stockCountStatus == 6)
			        	   	{
			        	   		showMessage("loading", "Loading stock count information...", "");
			        	   		
			        	   		statesOnScreen(6);
			        	   		
			        	   		jQuery(".ajaxLoading").hide();
			        	   	}
			        	   	
			        		if(stockCountStatus == 2)
			        	   	{
			        	   		showMessage("loading", "Loading stock count information...", "");
			        	   		
			        	   		statesOnScreen(6);
			        	   	
			        	   		jQuery(".ajaxLoading").hide();
			        	   	}
	                		
			        	   	if(stockCountStatus == 3)
			        	   	{
			        	   		statesOnScreen(2);
			        	   		
			        	   		showMessage("loading", "Loading stock count information...", "");
			        	   		
			        	   		jQuery.ajax({
			        	   			type:'POST',
			        				url:Drupal.settings.basePath + 'StockCountCallBack',
			        				data:
			        				{
			        					id:"getStockCountRelatedInformation",
			        					statusId:rowData.Status,
			        					stockCountNo:rowData.StockCountNo,
			        					
			        					
			        				},
			        				success:function(data)
			        				{
			        					jQuery(".ajaxLoading").hide();
			        					
			        					var initiatedStockCountData = jQuery.parseJSON(data);
			        					
			        					if(initiatedStockCountData.Status == 1)
			        					{
			        						var itemGridId = 0;
			        						
			        						jQuery('#SCCreateItemGrid').jqGrid('clearGridData', true);
			        						
			        						var oInitiatedStockCountData = initiatedStockCountData.Result;
			        						jQuery.each(oInitiatedStockCountData,function(key,value){
			        							
			        							itemGridId = itemGridId + 1;
			        							
			        							var itemGridData = [{"ItemCode":oInitiatedStockCountData[key]['ItemCode'],"ItemName":oInitiatedStockCountData[key]['ItemName'],"ItemId":oInitiatedStockCountData[key]['ItemId'],"OnHandBucket":"<input type='text' placeholder='OnHand Bucket Quantity' name='OnHandBucketQuantity' style='width:220px;' class='onHandBucketQuantity' id="+oInitiatedStockCountData[key]['ItemId']+"onHand"+ ">","DamagedBucket":"<input type='text' placeholder='Damaged Bucket Quantity' name='DamagedBucketQuantity' style='width:220px;' class='damagedBucketQuantity' id="+oInitiatedStockCountData[key]['ItemId']+"damaged"+ ">","Varience":'none',"VariencePrice":'none',"VD":0}];
			        							for (var i=0;i<itemGridData.length;i++) {
			        								jQuery("#SCCreateItemGrid").jqGrid('addRowData', itemGridId, itemGridData[itemGridData.length-i-1], "last");
			        							
			        							}
			        							
			        							
			        						});
			        						
			        						checkItemsInBucketOrNot();
			        						
			        						jQuery(".onHandBucketQuantity").focusout(function(){

			        							checkItemsInBucketOrNot();

			        						});
			        						jQuery(".damagedBucketQuantity").focusout(function(){


			        							checkItemsInBucketOrNot();

			        						});
			        						
			        						*//**
			        						 * checking items entered in bucket or not.
			        						 *//*
			        						function checkItemsInBucketOrNot()
			        						{
			        							jQuery(".onHandBucketQuantity").each(function(){
			        								
			        								var currentOnHandItemBucketId = this.id;
			        								
			        								if(jQuery("#"+currentOnHandItemBucketId).val() == '')
			        								{
			        									jQuery("#"+currentOnHandItemBucketId).css('background','#ff9999');
			        								}
			        								else
			        								{
			        									jQuery("#"+currentOnHandItemBucketId).css('background','white');
			        								}
			        								
			        							});
			        							
			        							jQuery(".damagedBucketQuantity").each(function(){

			        								var currentDamagedItemBucketId = this.id;

			        								if(jQuery("#"+currentDamagedItemBucketId).val() == '')
			        								{
			        									jQuery("#"+currentDamagedItemBucketId).css('background','#ff9999');
			        								}
			        								else
			        								{
			        									jQuery("#"+currentDamagedItemBucketId).css('background','white');
			        								}

			        							});
			        						}
			        						
			        					}
			        					else
			        					{
			        						showMessage("red", initiatedStockCountData.Description, "");
			        					}
			        					
			        				}, //end success.
			        				error: function(XMLHttpRequest, textStatus, errorThrown) { 

			        					jQuery(".ajaxLoading").hide();
			        					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			        					showMessage("", defaultMessage, "");
			        				}
			        			});
			        	   	}
			        	   	
			        	   	if(stockCountStatus == 1) //Saved stock count.
			        	   	{
			        	   		statesOnScreen(2);
			        	   		
			        	   		showMessage("loading", "Loading stock count information...", "");
			        	   		
			        	   		jQuery.ajax({
			        	   			type:'POST',
			        				url:Drupal.settings.basePath + 'StockCountCallBack',
			        				data:
			        				{
			        					id:"getStockCountRelatedInformation",
			        					statusId:rowData.Status,
			        					stockCountNo:rowData.StockCountNo,
			        					
			        					
			        				},
			        				success:function(data)
			        				{
			        					jQuery(".ajaxLoading").hide();
			        					
			        					var savedStockCountData = jQuery.parseJSON(data); 
			        					
			        					if(savedStockCountData.Status == 1)
			        					{
			        						var itemGridId = 0;

			        						jQuery('#SCCreateItemGrid').jqGrid('clearGridData', true);

			        						var oSavedStockCountData = savedStockCountData.Result;
			        						jQuery.each(oSavedStockCountData,function(key,value){

			        							itemGridId = itemGridId + 1;

			        							var itemGridData = [{"ItemCode":oSavedStockCountData[key]['ItemCode'],"ItemName":oSavedStockCountData[key]['ItemName'],
			        								"ItemId":oSavedStockCountData[key]['Itemid'],
			        								"OnHandBucket":"<input type='text' placeholder='OnHand Bucket Quantity' name='OnHandBucketQuantity' style='width:220px;' class='onHandBucketQuantity' Value="+oSavedStockCountData[key]['OnHandQty']+" id="+oSavedStockCountData[key]['Itemid']+"onHand"+ ">",
			        								"DamagedBucket":"<input type='text' placeholder='Damaged Bucket Quantity' name='DamagedBucketQuantity' style='width:220px;' class='damagedBucketQuantity' Value="+oSavedStockCountData[key]['DamagedQty']+" id="+oSavedStockCountData[key]['Itemid']+"damaged"+ ">","Varience":'none',"VariencePrice":'none',"VD":0}];
			        							for (var i=0;i<itemGridData.length;i++) {
			        								jQuery("#SCCreateItemGrid").jqGrid('addRowData', itemGridId, itemGridData[itemGridData.length-i-1], "last");

			        							}


			        						});
			        						
			        						
			        						checkItemsInBucketOrNot();
			        						
			        						jQuery(".onHandBucketQuantity").focusout(function(){

			        							checkItemsInBucketOrNot();

			        						});
			        						jQuery(".damagedBucketQuantity").focusout(function(){


			        							checkItemsInBucketOrNot();

			        						});
			        						
			        						*//**
			        						 * checking items entered in bucket or not.
			        						 *//*
			        						function checkItemsInBucketOrNot()
			        						{
			        							jQuery(".onHandBucketQuantity").each(function(){
			        								
			        								var currentOnHandItemBucketId = this.id;
			        								
			        								if(jQuery("#"+currentOnHandItemBucketId).val() == '')
			        								{
			        									jQuery("#"+currentOnHandItemBucketId).css('background','#ff9999');
			        								}
			        								else
			        								{
			        									jQuery("#"+currentOnHandItemBucketId).css('background','white');
			        								}
			        								
			        							});
			        							
			        							jQuery(".damagedBucketQuantity").each(function(){

			        								var currentDamagedItemBucketId = this.id;

			        								if(jQuery("#"+currentDamagedItemBucketId).val() == '')
			        								{
			        									jQuery("#"+currentDamagedItemBucketId).css('background','#ff9999');
			        								}
			        								else
			        								{
			        									jQuery("#"+currentDamagedItemBucketId).css('background','white');
			        								}

			        							});
			        						}
			        						
			        					}
			        					else
			        					{
			        						showMessage("red", savedStockCountData.Description, "");
			        					}
			        				}, //end success.
			        				error: function(XMLHttpRequest, textStatus, errorThrown) { 

			        					jQuery(".ajaxLoading").hide();
			        					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			        					showMessage("", defaultMessage, "");
			        				}
			        			});
			        	   	}
			        	   	
			        	   	if(stockCountStatus == 5) //Confirmed stock count.
			        	   	{
			        	   		statesOnScreen(3);
			        	   		
			        	   		showMessage("loading", "Loading stock count information...", "");
			        	   		
			        	   		jQuery.ajax({
			        	   			type:'POST',
			        				url:Drupal.settings.basePath + 'StockCountCallBack',
			        				data:
			        				{
			        					id:"getStockCountRelatedInformation",
			        					statusId:rowData.Status,
			        					stockCountNo:rowData.StockCountNo,
			        					
			        					
			        				},
			        				success:function(data)
			        				{
			        					jQuery(".ajaxLoading").hide();
			        					
			        					var confirmedStockCountData = jQuery.parseJSON(data);
			        					
			        					if(confirmedStockCountData.Status == 1)
			        					{
			        						var itemGridId = 0;

			        						jQuery('#SCCreateItemGrid').jqGrid('clearGridData', true);

			        						var oConfirmedStockCountData1 = confirmedStockCountData.Result;
			        						jQuery.each(oConfirmedStockCountData1,function(key,value){

			        							itemGridId = itemGridId + 1;

			        							var itemGridData = [{"ItemCode":oConfirmedStockCountData1[key]['ItemCode'],"ItemName":oConfirmedStockCountData1[key]['ItemName'],
			        								"ItemId":oConfirmedStockCountData1[key]['Itemid'],
			        								"OnHandBucket":oConfirmedStockCountData1[key]['OnHandQty'],
			        								"DamagedBucket":oConfirmedStockCountData1[key]['DamagedQty'],
			        								"OnHandBucketSys":parseInt(parseFloat(oConfirmedStockCountData1[key]['OnHandSystem'])),
			        								"DamagedBucketSys":parseInt(parseFloat(oConfirmedStockCountData1[key]['DamagedSystem'])), "Varience":parseInt(parseFloat(oConfirmedStockCountData1[key]['Varience'])),
			        								"VariencePrice":formatFloatNumbers(parseFloat(oConfirmedStockCountData1[key]['VariancePrice'])),"VD":1}];
			        							for (var i=0;i<itemGridData.length;i++) {
			        								jQuery("#SCCreateItemGrid").jqGrid('addRowData', itemGridId, itemGridData[itemGridData.length-i-1], "last");

			        							}


			        						});
			        					}
			        					else
			        					{
			        						showMessage("red", confirmedStockCountData.Description, "");
			        					}
			        					
			        				}, //end success.
			        				error: function(XMLHttpRequest, textStatus, errorThrown) { 

			        					jQuery(".ajaxLoading").hide();
			        					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			        					showMessage("", defaultMessage, "");
			        				}
			        			});
			        	   	}
		                	
		                	
		                	

		                }  */
			           

			          
			         
		});

		jQuery("#SCSearchGrid").jqGrid('navGrid','#PJmap_SCSearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_SCSearchGrid").hide();
		
		
		/*-----------------stock count batch grid.--------------------*/
	
		jQuery("#SCCreateBatchGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['BatchNo','Physical Qty','ManufactureBatchNo'],
			colModel: [
			           { name: 'BatchNo', index: 'BatchNo', width:100 },
			           { name: 'PhysicalQty', index: 'PhysicalQty', width: 150,editable:true},
			           { name: 'ManufactureBatchNo', index: 'ManufactureBatchNo', width: 150,hidden:true},
			          ],
			           pager: jQuery('#PJmap_SCCreateBatchGrid'),
			           width:1040,
			           height:200,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true
		});

		jQuery("#SCCreateBatchGrid").jqGrid('navGrid','#PJmap_SCCreateBatchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_SCCreateBatchGrid").hide();
		
		
		
		/*----------------Stock count Item grid.------------------*/
		
		function itemsjQgrid(oBucketJson)
		{
			
		}
		
		jQuery("#SCCreateItemGrid").jqGrid({
			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','ItemId','OnHand Bucket','Damaged Bucket','OnHand Bucket(SYS)','Damaged Bucket(SYS)','Variance Qty','Variance Amount','VD'],
			colModel: [
			           { name: 'ItemCode', index: 'ItemCode', width:50},
			           { name: 'ItemName', index: 'ItemName', width: 150,sortable:true},
			           { name: 'ItemId', index: 'ItemId', width: 150,hidden:true},
			           { name: 'OnHandBucket', index: 'OnHandBucket', width:60,sorttype:'number'},
			           { name: 'DamagedBucket', index: 'DamagedBucket', width:60,sorttype:'number'},
			           { name: 'OnHandBucketSys', index: 'OnHandBucketSys', width: 75,sorttype:'number'},
			           { name: 'DamagedBucketSys', index: 'DamagedBucketSys', width:75,sorttype:'number'},
			           { name: 'Varience', index: 'Varience', width:75,formatter:varienceFormatter,sorttype:'number'},
			           { name: 'VariencePrice', index: 'VariencePrice', width:75,formatter:variencePriceFormatter,sorttype:'float'},
			           { name: 'VD', index: 'VD', width:75,hidden:true},
			          ],
			           pager: jQuery('#PJmap_SCCreateItemGrid'),
			           width:1040,
			           height:350,
			           rowNum: 1000,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           cellEdit: true,
			           cellsubmit: 'clientArray',
			           
			           afterInsertRow :function(rowid, rowData, rowelem) 
			           {
			        	   if (rowData.VD == 1)
			        	   { 
			        		   if(rowData.OnHandBucketSys > rowData.OnHandBucket)
			        		   {
			        			   jQuery("#SCCreateItemGrid").setCell(rowid,'OnHandBucketSys',rowData.OnHandBucketSys,{'color':'#FF0000'});
			        		   }
			        		   if(rowData.OnHandBucketSys < rowData.OnHandBucket)
			        		   {
			        			   jQuery("#SCCreateItemGrid").setCell(rowid,'OnHandBucketSys',rowData.OnHandBucketSys,{'color':'#0000FF'});
			        		   }
			        		   if(rowData.OnHandBucketSys == rowData.OnHandBucket)
			        		   {
			        			   jQuery("#SCCreateItemGrid").setCell(rowid,'OnHandBucketSys',rowData.OnHandBucketSys,{'background-color':'#FFFFFF'});
			        		   }
			        		   
			        		   //For Damaged Bucket.
			        		   if(rowData.DamagedBucketSys > rowData.DamagedBucket)
			        		   {
			        			   jQuery("#SCCreateItemGrid").setCell(rowid,'DamagedBucketSys',rowData.DamagedBucketSys,{'color':'#FF0000'});
			        		   }
			        		   if(rowData.DamagedBucketSys < rowData.DamagedBucket)
			        		   {
			        			   jQuery("#SCCreateItemGrid").setCell(rowid,'DamagedBucketSys',rowData.DamagedBucketSys,{'color':'#0000FF'});
			        		   }
			        		   if(rowData.DamagedBucketSys == rowData.DamagedBucket)
			        		   {
			        			   jQuery("#SCCreateItemGrid").setCell(rowid,'DamagedBucketSys',rowData.DamagedBucketSys,{'background-color':'#FFFFFF'});
			        		   }
			        	   }
			        	   else
			        	   {
			        		   
			        	   }
			           }
			        	   
			        	   

		});
		
		function varienceFormatter(cellvalue, options, row) 
		{
			//alert(cellvalue);
			
			if (cellvalue == 0) 
			{
				return '<span class="cellWithoutBackground" style="background-color:white;">' + cellvalue + '</span>';

			}
			if (cellvalue > 1) 
			{
				return '<span class="cellWithoutBackground" style="color:#FF0000;">' + cellvalue + '</span>';

			}
			if (cellvalue < 1) 
			{
				return '<span class="cellWithoutBackground" style="color:#0000FF;">' + cellvalue + '</span>';

			}
			if (cellvalue == "none") 
			{
				
				return '';
			}

		}
		
		function variencePriceFormatter(cellvalue, options, row) 
		{
			//alert(cellvalue);
			
			if (cellvalue == 0) 
			{
				return '<span class="cellWithoutBackground" style="background-color:white;">' + cellvalue + '</span>';

			}
			if (cellvalue > 1) 
			{
				return '<span class="cellWithoutBackground" style="color:#FF0000;">' + cellvalue + '</span>';

			}
			if (cellvalue < 1) 
			{
				return '<span class="cellWithoutBackground" style="color:#0000FF;">' + cellvalue + '</span>';

			}
			if (cellvalue == "none") 
			{
				
				return '';
			}

		}
		
		jQuery("#SCCreateItemGrid").jqGrid('navGrid','#PJmap_SCCreateItemGrid',{edit:true,add:false,del:false});	
		//jQuery("#PJmap_SCCreateItemGrid").hide();
		
		
		/*----------------------------------------------------------------Events----------------------------------------------------------------------*/
		jQuery("#SCCreateLocation").change(function(){
			
			var selectedWHLocation = jQuery("#SCCreateLocation").val();
			
			if(selectedWHLocation == -1)
			{
				jQuery("#SCCreateLocationCode").val('');
			}
			else
			{
				jQuery.each(WHLocation,function(key,value){
					
					if(WHLocation[key]['LocationId'] == selectedWHLocation)
					{
						jQuery("#SCCreateLocationCode").val(WHLocation[key]['LocationCode']);
					}
					else
					{
						
					}
					
				});
			}
			
		});
		
		/*---------------Item Details entered----------------*/
		
		jQuery("#SCCreateItemCode").keydown(function(e) {	
			if(e.which == 13 || e.which == 9)
			{

				provideItemInformationFromItemCode(jQuery("#SCCreateItemCode").val());

			}
			else
			{

			}

		});
		
		function provideItemInformationFromItemCode(itemCode)
		{
			
			showMessage("loading", "Loading item information...", "");

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"itemInformation",
					itemCode:itemCode,

				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var itemData = jQuery.parseJSON(data);

					if(itemData.Status == 1)
					{
						oItemData = itemData.Result;
						
						jQuery("#SCCreateItemDescription").val(oItemData[0]['ItemName']);
					}
					else
					{
						showMessage("red", itemData.Description, "");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});

		}
		
		/*-----------------Enter batch no ----------------*/
		jQuery("#SCCreateBatchNo").keydown(function(e){
			
			if(e.which == 13 || e.which == 9)
			{
				 verifyBatchNumber(jQuery("#SCCreateBatchNo").val());
				
			}
			
		});
		
		/**
		 * fuction verify batch details
		 */
		function verifyBatchNumber(batchNo)
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"batchVerification",
					batchNo:batchNo,

				},
				success:function(data)
				{
					var batchData = jQuery.parseJSON(data);

					if(batchData.Status == 1)
					{
						var oBatchData = batchData.Result;
						if(oBatchData == '')
						{
							jQuery("#SCCreateBatchNo").css('background','red');
						}
						else
						{
							oItemBatchData = oBatchData;
							
						}
					}
					else
					{
						showMessage("red", batchData.Description, "");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		
		/*---------batch add button click-----------------*/
		jQuery("#btnSCCreateBatchAdd").click(function(){
		
			var checkBatchAddOrNot = 1;

			if(jQuery("#SCCreateItemCode").val() == '')
			{
				jQuery("#SCCreateItemCode").css('background','red');
				checkBatchAddOrNot = 0;
			}
			else
			{
				jQuery("#SCCreateItemCode").css('background','white');
				checkBatchAddOrNot = 1;
			}
			
			if(jQuery("#SCCreateBatchPhysicalQty").val() == '')
			{
				jQuery("#SCCreateBatchPhysicalQty").css('background','red');
				checkBatchAddOrNot = 0;
			}
			else
			{
				jQuery("#SCCreateBatchPhysicalQty").css('background','white');
				checkBatchAddOrNot = 1;
			}
			
			if(checkBatchAddOrNot == 1)
			{
				batchGridId += 1;
				
				var batchGridData = [{"BatchNo":oItemBatchData[0]['BatchNo'],"PhysicalQty":jQuery("#SCCreateBatchPhysicalQty").val(),
									  "ManufactureBatchNo":oItemBatchData[0]['ManufactureBatchNo']}];
				for (var i=0;i<batchGridData.length;i++) {
					jQuery("#SCCreateBatchGrid").jqGrid('addRowData', batchGridId, batchGridData[batchGridData.length-i-1], "last");
//					
				}
			}
			else
			{
				showMessage("", "Fill all required fields", "");
			}
			
		});
		
		
		
		/*---------------Item Add button click------------------*/
		jQuery("#btnSCCreateAdd").click(function(){
			
			var checkItemAddOrNot = 1;
			
			if(jQuery("#SCCreateItemCode").val() == '')
			{
				jQuery("#SCCreateItemCode").css('background','red');
				checkItemAddOrNot = 0;
			}
			else
			{
				jQuery("#SCCreateItemCode").css('background','white');
				checkItemAddOrNot = 1;
			}
			
			if(jQuery("#SCCreateBucket").val() == -1)
			{
				jQuery("#SCCreateBucket").css('background','red');
				checkItemAddOrNot = 0;
				
			}
			else
			{
				jQuery("#SCCreateBucket").css('background','white');
				checkItemAddOrNot = 1;
			}
			if(jQuery("#SCCreatePhysicalQuantity").val() == '')
			{
				jQuery("#SCCreatePhysicalQuantity").css('background','red');
				checkItemAddOrNot = 0;
			}
			else
			{
				jQuery("#SCCreatePhysicalQuantity").css('background','white');
				checkItemAddOrNot = 1;
			}
			
			if(checkItemAddOrNot == 1)
			{
				itemGridId += 1;
				
				//var itemGridData = [{"ItemCode":oItemData[0]['ItemCode'],"ItemName":oItemData[0]['ItemName'],"Bucket":jQuery("#SCCreateBucket").val(),"ItemId":oItemData[0]['ItemId'],'PhysicalQty':jQuery("#SCCreatePhysicalQuantity").val()}];
				
				var itemGridData = [{"ItemCode":oItemData[0]['ItemCode'],"ItemName":oItemData[0]['ItemName'],"ItemId":oItemData[0]['ItemId'],"Bucket":"<select name='AvailableBucket' class='availableBucket' id="+oItemData[0]['ItemCode']+ "></select>","PhysicalQty":jQuery("#SCCreatePhysicalQuantity").val()}];
				for (var i=0;i<itemGridData.length;i++) {
					jQuery("#SCCreateItemGrid").jqGrid('addRowData', itemGridId, itemGridData[itemGridData.length-i-1], "last");
				
				}
				
				jQuery(".availableBucket").empty();
				
				jQuery.each(tempBucket,function(key,value){
					
					jQuery(".availableBucket").append("<option id=\""+tempBucket[key]['BucketId']+"\""+" value=\""+tempBucket[key]['BucketId']+"\""+">"+tempBucket[key]['BucketName']+"</option>");
					
				});
				
				
			}
			else
			{
				showMessage("", "Fill all required fields", "");
			}
			
		});
		
		
		/*---------------save button click----------------*/
		
		jQuery("#btnSCCreateSave").click(function(){
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"generateStockCountNo",
					seqType:"SC",
					locationId:jQuery("#SCCreateLocation").val(),
					
				},
				success:function(data)
				{
					//alert(data);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
			
			
		});
		
		/*------------save button click----------------*/
		
		jQuery("#btnINV02Create").click(function(){
			
			
			statusId = 1;
			callStockCountProc(statusId,1,'');
			
			

		});
		
		
		function callStockCountProc(statusId,stringifyJson,itemBatchInformation)
		{
			if(stringifyJson == 1)
			{
				var itemInformation = jQuery("#SCCreateItemGrid").jqGrid('getRowData');
				
				var sItemInformation = JSON.stringify(itemInformation);

				jQuery(".onHandBucketQuantity").each(function(){

					var currentOnHandBucketId = this.id;
					var onHandBucketQuantity = jQuery("#"+currentOnHandBucketId).val();

					var onHandItemCode = currentOnHandBucketId .replace(/(^\d+)(.+$)/i,'$1'); 

					jQuery.each(itemInformation,function(key,value){

						if(onHandItemCode == itemInformation[key]['ItemId'])
						{
							itemInformation[key]['OnHandBucket'] = onHandBucketQuantity;
						}
						else
						{

						}

					});


				});
				
				
				jQuery(".damagedBucketQuantity").each(function(){

					var currentDamagedBucketId = this.id;
					var damagedBucketQuantity = jQuery("#"+currentDamagedBucketId).val();

					var damagedItemCode = currentDamagedBucketId .replace(/(^\d+)(.+$)/i,'$1'); 

					jQuery.each(itemInformation,function(key,value){

						if(damagedItemCode == itemInformation[key]['ItemId'])
						{
							itemInformation[key]['DamagedBucket'] = damagedBucketQuantity;
						}
						else
						{

						}

					});


				});
				
				
				sItemInformation = JSON.stringify(itemInformation);
			}
			
			else
			{
				sItemInformation = '';
			}
			
			if(statusId == 3)
			{
				
				showMessage("loading", "Initiating stock count...", "");
				
				var stockCountlocationId = jQuery("#SCCreateLocation").val();
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
					data:
					{
						id:"createStockCount",
						moduleCode:"INV02",
						functionName:"Initiate",	
						stockCountNo:jQuery("#SCStcokCountNo").val(),
						itemInformation:sItemInformation,
						itemBatchInformation:itemBatchInformation,
						locationId:jQuery("#SCCreateLocation").val(),
						stockCountDate:getDate(jQuery("#SCCreateInitiatedDate").val()),
						statusId:statusId,

					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						
						var stockSavingData = jQuery.parseJSON(data);
						
						if(stockSavingData.Status == 1)
						{
							if(stockSavingData.Result[0]['initializeFlag'] == 0)
							{
								showMessage("red", "Stock count for this location already initiated", "");
							}
							else if(stockSavingData.Result[0]['initializeFlag'] == 2)
							{
								showMessage("red", "Stock count not confirmed or closed after stock count initiation", "");
							}
							else
							{
								enableSessionForLocation();
								
								getSessionForLocation(3); //get required session variables.
								
								getAllItemsForLocation();
								
								statesOnScreen(1); // for initiated state.
								
								showMessage("green","Stock count for location"+ stockCountlocationId+" initiated. Stock count no.:"+stockSavingData.Result[0]['SeqNo'],"");
								
								jQuery("#SCCreateStatus").val("Initiated");
								
								var sInitiatedDateFromServer = stockSavingData.Result[0]['InitiatedDate'];
								
								var oInitiatedDateFromServer = new Date(sInitiatedDateFromServer);
								
								var oneDay = 24*60*60*1000;
								
								var todayDate = new Date();
								
								var oTodayDate = todayDate.getDate();
								
								//var firstDate = new Date(2013,10,10);
								//var secondDate = new Date(2013,10,27);

								var daysLeft = Math.round(Math.abs((oInitiatedDateFromServer.getTime() - todayDate.getTime())/(oneDay)));
								
								if(oInitiatedDateFromServer.getDate() != oTodayDate)
								{
									jQuery(".onHandBucketQuantity").each(function(){
										
										var currentOnHandBucketId = this.id;
										jQuery("#"+currentOnHandBucketId).attr('disabled',true);
										
									});
									
									jQuery(".damagedBucketQuantity").each(function(){

										var currentDamagedBucketId = this.id;
										jQuery("#"+currentDamagedBucketId).attr('disabled',true);

									});
									
									
									
									showMessage("", "Stock count for this location will be initiate after "+daysLeft+ " days.","");
									
								}
								else
								{
									
								}
								
								
								
								jQuery("#SCStcokCountNo").val(stockSavingData.Result[0]['SeqNo']);
							}
							
							
							
							
							
						}
						else
						{
							showMessage("red", stockSavingData.Description, "");
							
						}
						
						//alert(data);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
				
			}
			
			if(statusId == 1)
			{
				
				showMessage("loading", "Saving stock count information...", "");
				
				jQuery.ajax({
					type: "POST",
					url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
					data:
					{
						moduleCode:"INV02",
						functionName:"Create",
						id:"createStockCount",
						stockCountNo:jQuery("#SCStcokCountNo").val(),
						itemInformation:sItemInformation,
						itemBatchInformation:itemBatchInformation,
						locationId:jQuery("#SCCreateLocation").val(),
						stockCountDate:getDate(jQuery("#SCCreateInitiatedDate").val()),
						statusId:statusId,

					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						
						var stockSavingData = jQuery.parseJSON(data);
						
						if(stockSavingData.Status == 1)
						{
							
							
							/*if(stockSavingData.Result == "INF0022")
							{
								showMessage("red","Initiated and saving dates must be same ","");
								
								statesOnScreen(2);
							}
							else
							{
								showMessage("red", "Problem in saving your information", "");
								
								statesOnScreen(1);
							}*/
							
							var oSavingDataFlag = stockSavingData.Result;
							
							if(oSavingDataFlag[0]['saved'] == 1)
							{
								showMessage("green","Stock count information saved.","");
								
								jQuery("#SCCreateStatus").val("Created");
								
								statesOnScreen(2);
							}
							else
							{
								showMessage("red", "Problem in saving information. Error " + oSavingDataFlag, "");
								
								statesOnScreen(1);
							}
							
							
						}
						else
						{
							showMessage("red", stockSavingData.Description, "");
						}
						
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}
			if(statusId == 5)
			{
				showMessage("loading", "Confirming stock count ...", "");
				
				jQuery.ajax({
					type: "POST",
					url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
					data:
					{
						moduleCode:"INV02",
						functionName:"Execute",
						id:"createStockCount",
						stockCountNo:jQuery("#SCStcokCountNo").val(),
						itemInformation:sItemInformation,
						itemBatchInformation:itemBatchInformation,
						locationId:jQuery("#SCCreateLocation").val(),
						stockCountDate:getDate(jQuery("#SCCreateInitiatedDate").val()),
						statusId:statusId,

					},
					success:function(data)
					{
						
						
						//alert(data);
						
						jQuery(".ajaxLoading").hide();
						
						var itemGridId = 0;
						
						var confirmStockCountData = jQuery.parseJSON(data);
						
						if(confirmStockCountData.Status == 1)
						{
							
							statesOnScreen(3);
							
							oConfirmStockCountData = confirmStockCountData.Result;
							
							getSessionForLocation(8);
							
							jQuery("#SCCreateStatus").val("Executed");
							
							jQuery( "#SCCreateConfirmedDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy'});
							jQuery( "#SCCreateConfirmedDate" ).datepicker("setDate",new Date());
							
							jQuery('#SCCreateItemGrid').jqGrid('clearGridData', true);
							
							jQuery.each(oConfirmStockCountData,function(key,value){

								itemGridId = itemGridId + 1;


								var itemGridData = [{"ItemCode":oConfirmStockCountData[key]['ItemCode'],"ItemName":oConfirmStockCountData[key]['ItemName'],
									"ItemId":oConfirmStockCountData[key]['itemid'],"OnHandBucket":parseInt(parseFloat(oConfirmStockCountData[key]['OnHandBucket'])) ,
									"DamagedBucket":parseInt(parseFloat(oConfirmStockCountData[key]['DamagedBucket'])),"OnHandBucketSys":parseInt(parseFloat(oConfirmStockCountData[key]['OnHandBucketSYS'])),
									"DamagedBucketSys":parseInt(parseFloat(oConfirmStockCountData[key]['DamagedBucketSYS'])), "Varience":parseInt(parseFloat(oConfirmStockCountData[key]['Varience'])),"VD":1,
									"VariencePrice":formatFloatNumbers(parseFloat(oConfirmStockCountData[key]['VariancePrice']))}];
								
								for (var i=0;i<itemGridData.length;i++) {
									
									jQuery("#SCCreateItemGrid").jqGrid('addRowData', itemGridId, itemGridData[itemGridData.length-i-1], "last");

								}
	
								
							});

							//return '<span class="cellWithoutBackground" style="background-color:' +
						       //color + ';">' + cellvalue + '</span>';
							
						}
						else
						{
							showMessage("red", confirmStockCountData.Description, "");
							
							statesOnScreen(2);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}
			
			
			if(statusId == 6)
			{
				
				if(jQuery("#SCStcokCountNo").val() == '')
				{
					showMessage("","No stock count no available to close. Use Search.", "");
				}
				else
				{
					showMessage("loading", "Closing Stock Count...", "");
					
					jQuery.ajax({
						type: "POST",
						url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
						data:
						{
							moduleCode:"INV02",
							functionName:"Close",
							id:"createStockCount",
							stockCountNo:jQuery("#SCStcokCountNo").val(),
							itemInformation:sItemInformation,
							itemBatchInformation:itemBatchInformation,
							locationId:jQuery("#SCCreateLocation").val(),
							stockCountDate:getDate(jQuery("#SCCreateInitiatedDate").val()),
							statusId:statusId,

						},
						success:function(data)
						{
							
							jQuery(".ajaxLoading").hide();
							
							//var itemGridId = 0;
							
							var cancelStockCountData = jQuery.parseJSON(data);
							
							if(cancelStockCountData.Status == 1)
							{
								
								statesOnScreen(6); // State used to disable everything on screen.
								
								oCancelStockCountData = cancelStockCountData.Result;
								
								if(oCancelStockCountData[0]['status'] == 6)
								{
									showMessage("green", "Stock Count Closed", "");
									
									jQuery("#SCCreateStatus").val("Closed");
									
								}
								else if(oCancelStockCountData[0]['status'] == 2)
								{
									showMessage("green", "Stock Count Cancelled", "");
									
									jQuery("#SCCreateStatus").val("Cancelled");
								}
								else
								{
									showMessage("red", "Stock Count not Closed. Error " + cancelStockCountData , "");
									
								}
								
								getSessionForLocation(6); // Not written for this condition yet accor. to screen, no field for showing who  have cancelled.
							}
							else
							{
								showMessage("red", cancelStockCountData.Description, "");
								
								statesOnScreen(2);
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
				}
				
			}
			
			
		}
		
		
		/*------------------initiate button click-----------------*/
		
		jQuery("#btnINV02Initiate").click(function(){
			
			var stockCountInitiateFlag = 1;
			
			if(jQuery("#SCCreateLocation").val()==-1 || jQuery("#SCCreateInitiatedDate").val() == '' || jQuery("#SCCreateLocationCode").val() == '')
			{
				stockCountInitiateFlag = 0;
				
				jQuery("#SCCreateLocation").css('background','#FF9999');
			}
			else
			{
				jQuery("#SCCreateLocation").css('background','white');
			}
			
			if(stockCountInitiateFlag == 1)
			{
				
				var sSelectedInitiatedDate = jQuery("#SCCreateInitiatedDate").val();
				
				var oSelectedInitiatedDate  = new Date(sSelectedInitiatedDate);
				
				var initiatedDateDay = oSelectedInitiatedDate.getDate();
				
				var todayDate = new Date();
				
				var todayDateDay = todayDate.getDate();
				
				if(initiatedDateDay < todayDateDay)
				{
					showMessage("red", "Stock count cannot be Initiated for previous date", "");
				}
				else
				{
					statusId = 3; //For initiating Stock count.

					
					
					callStockCountProc(statusId,0,'');
					
					jQuery("#SCCreateLocation").attr('disabled',true);
				}
				
				
				
			}
			else
			{
				showMessage("red", "Please enter required fields", "");
			}
			
			
		});
		
		/*----------Confirm button click-------------*/
		
		jQuery("#btnINV02Execute").click(function(){


			var onHandBucketId;
			var onHandItemCount = 0;
			var damagedBucketId;
			var damagedItemCount = 0;

			jQuery(".onHandBucketQuantity").each(function(){

				if(jQuery("#"+this.id).val() == '')
				{
					onHandBucketId = this.id;

					onHandItemCount = onHandItemCount + 1;

					return false;
				}


			});


			jQuery(".damagedBucketQuantity").each(function(){

				if(jQuery("#"+this.id).val() == '')
				{
					damagedBucketId = this.id;
					damagedItemCount = damagedItemCount + 1;
					return false;

				}


			});

			if(onHandItemCount == 0 && damagedItemCount == 0)
			{
				jQuery(".onHandBucketQuantity").attr('disabled',true);

				jQuery(".damagedBucketQuantity").attr('disabled',true);

				statusId = 5;

				callStockCountProc(statusId,1,'');
			}
			else
			{

				if(damagedItemCount > onHandItemCount)
				{
					jQuery("#"+onHandBucketId).focus();
				}
				else
				{
					jQuery("#"+damagedBucketId).focus();
				}

				showMessage("", "Fill data corresponding to each bucket.", "");
			}

			/*statusId = 8;

			callStockCountProc(statusId,0,'');*/

		});
		
		
		/*jQuery("#btnINV02Execute").click(function(){
			
			var onHandValidationFlag = 0;
			
			jQuery(".onHandBucketQuantity").each(function(){
				
				onHandFieldId = this.id;
				
				
				if(jQuery("#"+onHandFieldId).val() == '')
				{
					onHandValidationFlag = onHandValidationFlag + 1;
				}
				
				
			});
			var damagedValidationFlag = 0;
			
			jQuery(".damagedBucketQuantity").each(function(){

				damagedFieldId = this.id;
				

				if(jQuery("#"+damagedFieldId).val() == '')
				{
					damagedValidationFlag = damagedValidationFlag + 1;
				}


			});
			
			if(damagedValidationFlag == 0 && onHandValidationFlag == 0)
			{
				jQuery(".onHandBucketQuantity").attr('disabled',true);
				
				jQuery(".damagedBucketQuantity").attr('disabled',true);
				
				statusId = 5;
				
				callStockCountProc(statusId,1,'');
			}
			else
			{
				showMessage("", "Enter data corresponding to each item.", "");
			}
			
			statusId = 8;
			
			callStockCountProc(statusId,0,'');
	
		});*/
		
		/**
		 * Get session variables which are required like logined user on HO.
		 */
		function getSessionForLocation(loginUserStatus)
		{
			var selectedLocationId = jQuery("#SCCreateLocation").val();
			
			if(selectedLocationId != -1)
			{

				//alert(selectedLocationId);
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'StockCountCallBack',
					data:
					{
						id:"getLoginedHOUserInfo",
						locationId:selectedLocationId,
					
					},
					success:function(data)
					{
						//alert(data);
						
						var loginUserInfo = data.trim();
						
						if(loginUserInfo == "disable")
						{
							
							//Session has been stored for current location id.
							showMessage("red", "Server side application error", "");
						}
						else
						{
							if(loginUserStatus == 3)
							{
								jQuery("#SCCreateInitiatedBy").val(loginUserInfo);
							}
							if(loginUserStatus == 8)
							{
								jQuery("#SCCreateConfirmedBy").val(loginUserInfo);
							}
							
							
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}
			else
			{
				
			}
		}
		
		
		/**
		 * Get all item available.
		 */
		function getAllItemsForLocation()
		{
			showMessage("loading", "Loading items...", "");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"getItemsForLocation",
					locationId:jQuery("#SCCreateLocation").val(),
				
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var locationItemsData = jQuery.parseJSON(data);

					if(locationItemsData.Status == 1)
					{
						var oLocationItemsData = locationItemsData.Result;

						jQuery.each(oLocationItemsData,function(key,value){	

							itemGridId += 1;
							
							//var itemGridData = [{"ItemCode":oItemData[0]['ItemCode'],"ItemName":oItemData[0]['ItemName'],"Bucket":jQuery("#SCCreateBucket").val(),"ItemId":oItemData[0]['ItemId'],'PhysicalQty':jQuery("#SCCreatePhysicalQuantity").val()}];
							
							
							
							var itemGridData = [{"ItemCode":oLocationItemsData[key]['ItemCode'],"ItemName":oLocationItemsData[key]['DisplayName'],"ItemId":oLocationItemsData[key]['Id'],"OnHandBucket":"<input type='text' placeholder='OnHand Bucket Quantity' name='OnHandBucketQuantity' style='width:220px;' class='onHandBucketQuantity' id="+oLocationItemsData[key]['Id']+"onHand"+ ">","DamagedBucket":"<input type='text' placeholder='Damaged Bucket Quantity' name='DamagedBucketQuantity' style='width:220px;' class='damagedBucketQuantity' id="+oLocationItemsData[key]['Id']+"damaged"+ ">","Varience":'none',"VariencePrice":'none',"VD":0}];
							for (var i=0;i<itemGridData.length;i++) {
								jQuery("#SCCreateItemGrid").jqGrid('addRowData', itemGridId, itemGridData[itemGridData.length-i-1], "last");
							
							}
							

						});
						
						checkItemsInBucketOrNot();
						
						jQuery(".onHandBucketQuantity").focusout(function(){

							checkItemsInBucketOrNot();

						});
						jQuery(".damagedBucketQuantity").focusout(function(){


							checkItemsInBucketOrNot();

						});
						
						/**
						 * checking items entered in bucket or not.
						 */
						function checkItemsInBucketOrNot()
						{
							jQuery(".onHandBucketQuantity").each(function(){
								
								var currentOnHandItemBucketId = this.id;
								
								if(jQuery("#"+currentOnHandItemBucketId).val() == '')
								{
									jQuery("#"+currentOnHandItemBucketId).css('background','#ff9999');
								}
								else
								{
									jQuery("#"+currentOnHandItemBucketId).css('background','white');
								}
								
							});
							
							jQuery(".damagedBucketQuantity").each(function(){

								var currentDamagedItemBucketId = this.id;

								if(jQuery("#"+currentDamagedItemBucketId).val() == '')
								{
									jQuery("#"+currentDamagedItemBucketId).css('background','#ff9999');
								}
								else
								{
									jQuery("#"+currentDamagedItemBucketId).css('background','white');
								}

							});
						}
						
							
					}
					else
					{
						showMessage("red", locationItemsData.Description, '');
					}

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		
		
		/**btnSCCreateAdd
		 * Enable session for current location
		 */
		function enableSessionForLocation()
		{
			var selectedLocationId = jQuery("#SCCreateLocation").val();
			
			if(selectedLocationId != -1)
			{

				//alert(selectedLocationId);
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'StockCountCallBack',
					data:
					{
						id:"stockCountInitiatedForLocation",
						locationId:selectedLocationId,
					
					},
					success:function(data)
					{
						//alert(data);
						
						if(data.trim() == "enabled")
						{
							
							//Session has been stored for current location id.
						}
						else
						{
							showMessage("red", "Server side application error", "");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}
			else
			{
				
			}
		}
		
		jQuery("#btnReset").click(function(){
			
			jQuery('#SCCreateItemGrid').jqGrid('clearGridData', true);
			
			statesOnScreen(4); //Reset state on screen.

			jQuery("#SCCreateLocation").empty();
			
			jQuery("#SCCreateLocationCode").val('');

			jQuery("#SCCreateStatus").val('');

			jQuery("#SCCreateInitiatedDate").val('');

			jQuery("#SCCreateInitiatedBy").val('');

			jQuery("#SCCreateConfirmedDate").val('');
			
			jQuery("#SCCreateConfirmedBy").val('');
			
			jQuery("#SCStcokCountNo").val('');
			
			showCalender();
			
			//condition for checking empty list.
			searchWHLocation();
			
			
			
		});
		
		/*------------------print stock count report-------------*/
		
		jQuery("#btnINV02Print").click(function(){

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
				data:
				{
					id:"printStockCount",
					stockCountNo:jQuery("#SCStcokCountNo").val(),
					locationId:jQuery("#SCCreateLocation").val(),
					functionName:"Print",
					moduleCode:"INV02"
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
						jQuery.each(results,function(key,value){
							var arr = {'LocationId':results[key]['locationId'],'stockCountNo':results[key]['stockCountNumber'],
									'Status':8,'GeneratedBy':loggedInUserFullName};
							showReport(results[key]['baseUrl'],results[key]['Url'],arr);

						});
					}
					else{

						showMessage('red',resultsData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
/*
			var arr = {'LocationId':jQuery("#SCCreateLocation").val(),'stockCountNo':jQuery("#SCStcokCountNo").val(),'Status':8};
			showReport('reports/StockCount.rptdesign',arr);*/

		});

		
		/*------------------------stock count SEARCH reset button event------------------------------*/
		
		jQuery("#btnSearchSCReset").click(function(){
			
			jQuery("#SCSearchLocation").empty();
			
			jQuery('#SCSearchGrid').jqGrid('clearGridData', true);
			
			jQuery("#CRLocation").empty();
			SCStatus();
			
			searchWHLocationOnSearchTab();
			jQuery("#SCSearchSeqNo").val('');
			jQuery("#SCInitiatedDate").val('');
			jQuery("#SCConfirmedBy").val('');
			jQuery("#CRConfirmedDate").val('');
			jQuery("#CRInitiatedBy").val('');
			showCalender();
			
		});
		
		
		/*--------------------------Stock count SEARCH search button event-----------------------------*/
		jQuery("#btnINV02Search").click(function(){
			
			var datebaseFormattedInitiatedDate = getDate(jQuery("#SCInitiatedDate").val());
			
			jQuery("#SCInitiatedDateDataBaseFormatted").val(datebaseFormattedInitiatedDate);
			
			var datebaseFormattedConfirmedDDate = getDate(jQuery("#CRConfirmedDate").val());
			
			jQuery("#CRConfirmedDateDatabaseFormatted").val(datebaseFormattedConfirmedDDate);
			
		
			showMessage("loading", "Searching stock count detail...", "");
			
			jQuery("#SCSearchLocation").attr('disabled',false);
			
			var stockCountFormData = jQuery("#stockCountSearchForm").serialize();
			
			
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountCallBack',
				data:
				{
					id:"searchStockCount",
					stockCountSearchFormData:stockCountFormData,
				
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					jQuery('#SCSearchGrid').jqGrid('clearGridData', true);
					
					var stockCountSearchData = jQuery.parseJSON(data);
					
					if(stockCountSearchData.Status == 1)
					{
						var searchGridId = 0;
						
						var oStockCountSearchData = stockCountSearchData.Result;
						
						if(oStockCountSearchData.length == 0)
						{
							showMessage("", "No record found", "");
						}
						else
						{
							jQuery.each(oStockCountSearchData,function(key,value){	
								
								searchGridId = searchGridId + 1;
								var status=oStockCountSearchData[key]['StatusName'];
								var itemGridData = [{
									"Action":"<input type='checkbox' class="+"grid_chkbox"+" id="+"chk"+searchGridId+">",
									"StockCountNo":oStockCountSearchData[key]['StockCountSeqNo'],
									"LocationName":oStockCountSearchData[key]['LocationName'],
									"LocationId":oStockCountSearchData[key]['LocationId'],
									"LocationAddress":oStockCountSearchData[key]['LocationAddress'],
									"InitiatedDate":oStockCountSearchData[key]['InitiatedDate'],
									"InitiatedBy":oStockCountSearchData[key]['InitiatedBy'],
									"ExecutedDate":oStockCountSearchData[key]['ExecutedDate'],
									"ExecutedBy":oStockCountSearchData[key]['ExecutedBy'],
									"StatusName":oStockCountSearchData[key]['StatusName'],
									"Status":oStockCountSearchData[key]['Status']}];
								for (var i=0;i<itemGridData.length;i++) {
									jQuery("#SCSearchGrid").jqGrid('addRowData', searchGridId, itemGridData[itemGridData.length-i-1], "last");
								
								}
								 checkedUnchecked(status,searchGridId);	
								 
								 
								 
								 
								 jQuery(".grid_chkbox").click(function(){
									

									 jQuery(this).is(":checked") ? this.value = 1 : this.value = 0 ;

									 });
								 
								 
								
							});
						}
						
						var searchGridId = 0;
						
					}
					else
					{
						showMessage("red", stockCountSearchData.Description, "");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
				
			
		});
		
		/*----------close button click---------------*/
		jQuery("#btnINV02Close").click(function(){
			
			statusId = 6;
			callStockCountProc(statusId,0,'');
			
		});
		
		
		
		jQuery("#btnSCCreatePopulateFields").click(function(){
			
			jQuery(".onHandBucketQuantity").val(2300);
			jQuery(".damagedBucketQuantity").val(100);
			
		});
		
		
		
		/*------------------------button enable disable options---------------------------------*/
		
		function statesOnScreen(disableWhat)
		{
			//Initiated state.
			if(disableWhat == 1)
			{
				jQuery("#SCCreateInitiatedDate").attr('disabled',true);
				jQuery("#SCCreateLocation").attr('disabled',true);
				jQuery("#btnINV02Initiate").attr('disabled',true);
				jQuery("#btnINV02Create").attr('disabled',false);
				jQuery("#btnINV02Execute").attr('disabled',false);
				jQuery("#btnINV02Print").attr('disabled',true);
				actionButtonsStauts();
			}
			
			//Saved state.
			if(disableWhat == 2)
			{
				jQuery("#SCCreateInitiatedDate").attr('disabled',true);
				jQuery("#SCCreateLocation").attr('disabled',true);
				jQuery("#btnINV02Initiate").attr('disabled',true);
				jQuery("#btnINV02Create").attr('disabled',false);
				jQuery("#btnINV02Execute").attr('disabled',false);
				jQuery("#btnINV02Print").attr('disabled',true);
				actionButtonsStauts();
			}
			
			//Confirmed state.
			if(disableWhat == 3)
			{
				jQuery("#SCCreateInitiatedDate").attr('disabled',true);
				jQuery("#SCCreateLocation").attr('disabled',true);
				jQuery("#btnINV02Initiate").attr('disabled',true);
				jQuery("#btnINV02Create").attr('disabled',true);
				jQuery("#btnINV02Execute").attr('disabled',true);
				jQuery("#btnINV02Print").attr('disabled',false);
				actionButtonsStauts();
			}
			
			//Reset state
			if(disableWhat == 4)
			{
				jQuery("#SCCreateInitiatedDate").attr('disabled',false);
				jQuery("#SCCreateLocation").attr('disabled',false);
				jQuery("#btnINV02Initiate").attr('disabled',false);
				jQuery("#btnINV02Create").attr('disabled',true);
				jQuery("#btnINV02Execute").attr('disabled',true);
				jQuery("#btnINV02Print").attr('disabled',true);
				actionButtonsStauts();
			}
			
			//If initializing date is not today date.
			if(disableWhat == 5)
			{
				jQuery("#SCCreateInitiatedDate").attr('disabled',true);
				jQuery("#SCCreateLocation").attr('disabled',true);
				jQuery("#btnINV02Initiate").attr('disabled',true);
				jQuery("#btnINV02Create").attr('disabled',true);
				jQuery("#btnINV02Execute").attr('disabled',true);
				jQuery("#btnINV02Print").attr('disabled',true);
				actionButtonsStauts();
			}
			
			//Closing or Cancelling state.
			if(disableWhat == 6)
			{
				jQuery("#SCCreateInitiatedDate").attr('disabled',true);
				jQuery("#SCCreateLocation").attr('disabled',true);
				jQuery("#btnINV02Initiate").attr('disabled',true);
				jQuery("#btnINV02Create").attr('disabled',true);
				jQuery("#btnINV02Execute").attr('disabled',true);
				jQuery("#btnINV02Print").attr('disabled',true);
				actionButtonsStauts();
			}
		} 
	
		
		jQuery("#selectAll").change(function(){
			
			if (jQuery("#selectAll").is(':checked')){
				
				jQuery(".grid_chkbox").attr('checked',true);  
				jQuery(".grid_chkbox").attr("value",'1');
				
			}
			else{
				
				jQuery(".grid_chkbox").attr('checked',false);  
				jQuery(".grid_chkbox").attr("value",'0');
			}
		});
		
	 function checkedUnchecked(status,chkBoxId)
	 {
		 
		 if (status=='Initiated')
			 {
			 jQuery('#chk'+chkBoxId).attr("value",'1');
			 jQuery('#chk'+chkBoxId).attr('checked', true);
			 }
		
		 else
			 {
			 jQuery('#chk'+chkBoxId).attr("value",'0');
			 jQuery('#chk'+chkBoxId).attr('checked', false);
			 }
	 }
	 
	 jQuery("#btnINV01Save").click(function(){
		 
		 if (jQuery("#SCSearchGrid").jqGrid("getGridParam", "data") != '')
		 {
		 SaveAction();
		 }
		 else
			 {
			 showMessage("", "No Data Found", "");
			 }
	
	   
	 });
	 
	 function  SaveAction()
	 {
		 showMessage("loading", "Saving data...", "");
		 jsonArray = new Array();
		 
		 var rowids = jQuery('#SCSearchGrid').jqGrid('getDataIDs');
		  for(var i=0;i < rowids.length;i++){ 
			 
			  var rowdata= jQuery('#SCSearchGrid').jqGrid ('getRowData',rowids[i]);
			  
			  rowdata.Action = jQuery("#chk"+rowids[i]).val();
			  
			  jsonArray.push(rowdata);
			  
			  
			  
		 }
		  
         var postData = JSON.stringify(jsonArray);
     	
         
         jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
				data:
				{
					id:"savestockcount",
					locationId:jQuery("#SCCreateLocation").val(),
					griddata:postData,
					
					functionName:"Create",
					moduleCode:"INV02"
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
						jQuery.each(results,function(key,value){
							var arr = {'LocationId':results[key]['locationId'],'stockCountNo':results[key]['stockCountNumber'],
									'Status':8,'GeneratedBy':loggedInUserFullName};
							showMessage('green',results[key]['OutParam'],'');
							jQuery('#SCSearchGrid').jqGrid('clearGridData', true);
							document.getElementById("btnINV02Search").click();
							showMessage('green',results[key]['OutParam'],'');
						});
					}
					else{

						showMessage('red',resultsData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
         
	 }
		
});		
