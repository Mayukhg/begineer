
var rowid = 0;
var historyOrderNo; 
var loggedInUserPrivileges;
var jsonForLocations ;
var expectedDate ;
jQuery(document).ready(function(){
	jQuery(".breadcrumb").hide();
jQuery("#orderMovementGrid").jqGrid({
    
    
    datatype: 'jsonstring',
    colNames: ['AddToLog','Status','Status Name','Log No', 'Distributor Id','Distributor Name','Log Date','Total Amount','OrderType'
/*		                ,'DeliverToCityName',
               'DeliverToStateName','DeliverToCountryName','DeliverFromCityName','DeliverFromStateName','DeliverFromCountryName','IsPrintAllowed','InvoiceDate','ValidReportPrintDays',
               'OrderMode','UsedForRegistration','TerminalCode','TotalBV','TotalPV','DistributorAddress','OrderType','PCId','PCCode','BOId','DeliverFromAddress1','DelverFromAddress2',
               'DeliverFromAddress3','DeliverFromAddress4','DeliverFromCityId','DeliverFromPincode','DeliverFromStateId','DeliverFromCountryId','DeliverFromTelephone','DeliverFromMobile',
               'DeliverToMobile','TotalUnits','TotalWeight','OrderAmount','DiscountAmount','TaxAmount','PaymentAmount','ChangeAmount','CreatedBy','CreatedByName','CreatedDate',
               'DeliverToAddressLine1','DeliverTo','DeliverToAddressLine2','DeliverToAddressLine3','DeliverToAddressLine4','DeliverToCityId','DeliverToPincode','DeliverToStateId',
               'DeliverToCountryId','DeliverToTelephone'
               ,'LogValue'*/
               ],
    colModel: [

               { name: 'AddToLog', index: 'AddToLog', width: 50,edittype:'checkbox',formatter: 'checkbox',editoptions: { value:"True:False"},classes:'AddToLogSelection',editable:true,formatoptions: {disabled : false},hidden:true},
               { name: 'Status', index: 'Status', width:100,hidden:true},
               { name: 'StatusName', index: 'StatusName', width:100},
               { name: 'LogNo', index: 'LogNo', width:200 },
            
               { name: 'DistributorId', index: 'DistributorId', width:100 },
               { name: 'DistributorName', index: 'DistributorName', width:150 },
               { name: 'OrderDate', index: 'LogDate', width:100 },
               { name: 'TotalAmount', index: 'TotalAmount', width:100 },
               { name: 'OrderType', index: 'OrderType', width:200,hidden:true},
               
             /*  { name: 'DeliverToCityName', index: 'DeliverToCityName', width:20 ,hidden:true},
               { name: 'DeliverToStateName', index: 'DeliverToStateName', width:20 ,hidden:true},
               { name: 'DeliverToCountryName', index: 'DeliverToCountryName', width:20 ,hidden:true},
               { name: 'DeliverFromCityName', index: 'DeliverFromCityName', width:20 ,hidden:true},
               { name: 'DeliverFromStateName', index: 'DeliverFromStateName', width:20 ,hidden:true},
               { name: 'DeliverFromCountryName', index: 'DeliverFromCountryName', width:20 ,hidden:true},
               { name: 'IsPrintAllowed', index: 'IsPrintAllowed', width:20 ,hidden:true},
               { name: 'InvoiceDate', index: 'InvoiceDate', width:20 ,hidden:true},
               { name: 'ValidReportPrintDays', index: 'ValidReportPrintDays', width:20 ,hidden:true},
               { name: 'OrderMode', index: 'OrderMode', width:20 ,hidden:true},
               { name: 'UsedForRegistration', index: 'UsedForRegistration', width:20 ,hidden:true},
               { name: 'TerminalCode', index: 'TerminalCode', width:20 ,hidden:true},
               { name: 'TotalBV', index: 'TotalBV', width:20 ,hidden:true},
               { name: 'TotalPV', index: 'TotalPV', width:20 ,hidden:true},
               { name: 'DistributorAddress', index: 'DistributorAddress', width:20 ,hidden:true},          
               { name: 'OrderType', index: 'OrderType', width:20 ,hidden:true},
               { name: 'PCId', index: 'PCId', width:20 ,hidden:true}, 
               { name: 'PCCode', index: 'PCCode', width:20 ,hidden:true},
               { name: 'BOId', index: 'BOId', width:20 ,hidden:true},
               

               { name: 'DeliverFromAddress1', index: 'DeliverFromAddress1', width:20 ,hidden:true},
               { name: 'DelverFromAddress2', index: 'DelverFromAddress2', width:20 ,hidden:true},
               { name: 'DeliverFromAddress3', index: 'DeliverFromAddress3', width:20 ,hidden:true},
               { name: 'DeliverFromAddress4', index: 'DeliverFromAddress4', width:20 ,hidden:true}, 
               { name: 'DeliverFromCityId', index: 'DeliverFromCityId', width:20 ,hidden:true},
               { name: 'DeliverFromPincode', index: 'DeliverFromPincode', width:20 ,hidden:true},
               { name: 'DeliverFromStateId', index: 'DeliverFromStateId', width:20 ,hidden:true},
               { name: 'DeliverFromCountryId', index: 'DeliverFromCountryId', width:20 ,hidden:true},
               { name: 'DeliverFromTelephone', index: 'DeliverFromTelephone', width:20 ,hidden:true},
               { name: 'DeliverFromMobile', index: 'DeliverFromMobile', width:20 ,hidden:true}, 
              
               { name: 'DeliverToMobile', index: 'DeliverToMobile', width:20 ,hidden:true},
               { name: 'TotalUnits', index: 'TotalUnits', width:20 ,hidden:true},
               { name: 'TotalWeight', index: 'TotalWeight', width:20 ,hidden:true},
               { name: 'OrderAmount', index: 'OrderAmount', width:20 ,hidden:true},
               { name: 'DiscountAmount', index: 'DiscountAmount', width:20 ,hidden:true},
               { name: 'TaxAmount', index: 'TaxAmount', width:20 ,hidden:true},
               { name: 'PaymentAmount', index: 'PaymentAmount', width:20 ,hidden:true},
               { name: 'ChangeAmount', index: 'ChangeAmount', width:20 ,hidden:true},
               { name: 'CreatedBy', index: 'CreatedBy', width:20 ,hidden:true},
               { name: 'CreatedByName', index: 'CreatedByName', width:20 ,hidden:true},
               { name: 'CreatedDate', index: 'CreatedDate', width:20 ,hidden:true},
               
               { name: 'DeliverToAddressLine1', index: 'DeliverToAddressLine1', width:20,hidden:true },
               { name: 'DeliverTo', index: 'DeliverTo', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine2', index: 'DeliverToAddressLine2', width:20,hidden:true },
               { name: 'DeliverToAddressLine3', index: 'DeliverToAddressLine3', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine4', index: 'DeliverToAddressLine4', width:20 ,hidden:true},
               { name: 'DeliverToCityId', index: 'DeliverToCityId', width:20 ,hidden:true},
               { name: 'DeliverToPincode', index: 'DeliverToPincode', width:20 ,hidden:true},
               { name: 'DeliverToStateId', index: 'DeliverToStateId', width:20 ,hidden:true},
               { name: 'DeliverToCountryId', index: 'DeliverToCountryId', width:20 ,hidden:true},
               { name: 'DeliverToTelephone', index: 'DeliverToTelephone', width:20 ,hidden:true},
             
               
           
            
        
               { name: 'LogValue', index: 'LogValue', width:20, hidden:true }
*/		              
               ],
               pager: jQuery('#historyGridPager'),
               width:925,
               height:300,
               rowNum: 1000,
               rowList: [500],
               sortable: true,
               sortorder: "asc",
               hoverrows: true,
               shrinkToFit: false,

               ondblClickRow: function (id) {
               		
               	historyOrderNo = jQuery("#orderMovementGrid").jqGrid('getCell', id, 'OrderNo');
               	
               	loadHistoryOrder(historyOrderNo);
               	
               	jQuery("#historyPopUP").dialog("close"); //close history pop up after double click.

               } //on double click row end. 

   });
	
	jQuery("#orderMovementGrid").jqGrid('navGrid','#historyGridPager',{edit:false,add:false,del:false}); 
	jQuery("#historyGridPager").hide();
	
	
jQuery("#btnSearch").unbind("click").click(function(){
	
	
	showMessage("loading", "Searching History Orders...", "");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'SearchOrderMovementCallBack',
			data:
			{
				id:"searchOrderMovement",
				HistoryOrderNo:jQuery("#historyOrderNo").val(),
				
				historyLogNo:jQuery("#historyLogNo").val(),
				HistoryFromDate:jQuery("#historyFromDate").val(),
				historyToDate:jQuery("#historyToDate").val(),
			    historyDistributorNo:jQuery("#historyDistributorNo").val(),
				
				HistoryStatus:jQuery("#HistoryStatus").val()
			},
			success:function(data)
			{

				jQuery(".ajaxLoading").hide();

				var historyOrderData = jQuery.parseJSON(data);
				
				if(historyOrderData.Status == 1)
				{
					
					
					
					var historyrowid=0;
					var searchTO = historyOrderData.Result;
					if(searchTO.length==0)
					{
						jQuery("#orderMovementGrid").jqGrid("clearGridData");
						showMessage("","No Record Found","");
					}

					else
					{	
						var message = "Total " + searchTO.length + " records found.";
						showMessage("", message,"");
						
						/*['Add To Log','Print','Invoice','Order No','Status','Log No'],*/
						jQuery("#orderMovementGrid").jqGrid("clearGridData");
						jQuery.each(searchTO,function(key,value){	
							var orderAmt=parseFloat(parseFloat(searchTO[key]['OrderAmount']).toFixed(2));
							var taxAmt=parseFloat(parseFloat(searchTO[key]['TaxAmount']).toFixed(2));
							var totalAmt =orderAmt+taxAmt;

							var newData = [{
								"InvoiceNo":searchTO[key]['InvoiceNo'],"OrderNo":searchTO[key]['CustomerOrderNo'],"StatusName":searchTO[key]['StatusName'],
								"Status":searchTO[key]['Status'],"LogNo":searchTO[key]['LogNo'],	"DistributorId":searchTO[key]['DistributorId'],"DistributorName":searchTO[key]['DistributorName'],
								"OrderDate":displayDate(searchTO[key]['CreatedDate']),"TotalAmount":orderAmt,"OrderType":searchTO[key]['OrderType'],
								"PC":searchTO[key]['PCCode'],
						/*		"DeliverToCityName":searchTO[key]['DeliverToCityName'],"DeliverToStateName":searchTO[key]['DeliverToStateName'],"DeliverToCountryName":searchTO[key]['DeliverToCountryName'],
								"DeliverFromCityName":searchTO[key]['DeliverFromCityName'],"DeliverFromCountryName":searchTO[key]['DeliverFromCountryName'],"IsPrintAllowed":searchTO[key]['IsPrintAllowed'],
								"InvoiceDate":searchTO[key]['InvoiceDate'],"ValidReportPrintDays":searchTO[key]['ValidReportPrintDays'],"OrderMode":searchTO[key]['OrderMode'],"UsedForRegistration":searchTO[key]['UsedForRegistration'],"TerminalCode":searchTO[key]['TerminalCode'],
								"TotalBV":searchTO[key]['TotalBV'],"TotalPV":searchTO[key]['TotalPV'],
								"DistributorAddress":searchTO[key]['DistributorAddress'],"OrderType":searchTO[key]['OrderType'],"PCId":searchTO[key]['PCId'],"PCCode":searchTO[key]['PCCode'],"BOId":searchTO[key]['BOId'],"DeliverFromAddress1":searchTO[key]['DeliverFromAddress1'],"DelverFromAddress2":searchTO[key]['DelverFromAddress2'],
								"DeliverFromAddress3":searchTO[key]['DeliverFromAddress3'],"DeliverFromAddress4":searchTO[key]['DeliverFromAddress4'],"DeliverFromCityId":searchTO[key]['DeliverFromCityId'],"DeliverFromPincode":searchTO[key]['DeliverFromPincode'],
								"DeliverFromStateId":searchTO[key]['DeliverFromStateId'],"DeliverFromCountryId":searchTO[key]['DeliverFromCountryId'],"DeliverFromTelephone":searchTO[key]['DeliverFromTelephone'],"DeliverFromMobile":searchTO[key]['DeliverFromMobile'],
								"DeliverToMobile":searchTO[key]['DeliverToMobile'],"TotalUnits":searchTO[key]['TotalUnits'],"TotalWeight":searchTO[key]['TotalWeight'],
								"OrderAmount":searchTO[key]['OrderAmount'],"DiscountAmount":searchTO[key]['DiscountAmount'],"TaxAmount":searchTO[key]['TaxAmount'],
								"PaymentAmount":searchTO[key]['PaymentAmount'],"ChangeAmount":searchTO[key]['ChangeAmount'],"CreatedBy":searchTO[key]['CreatedBy'],"CreatedByName":searchTO[key]['CreatedByName'],
								"CreatedDate":searchTO[key]['CreatedDate'],"DeliverToAddressLine1":searchTO[key]['DeliverToAddressLine1'],"DeliverTo":searchTO[key]['DeliverTo'],"DeliverToAddressLine2":searchTO[key]['DeliverToAddressLine2'],
								"DeliverToAddressLine3":searchTO[key]['DeliverToAddressLine3'],"DeliverToAddressLine4":searchTO[key]['DeliverToAddressLine4'],"DeliverToCityId":searchTO[key]['DeliverToCityId'],"DeliverToPincode":searchTO[key]['DeliverToPincode'],
								"DeliverToStateId":searchTO[key]['DeliverToStateId'],"DeliverToCountryId":searchTO[key]['DeliverToCountryId'],"DeliverToTelephone":searchTO[key]['DeliverToTelephone'],
								"LogValue":searchTO[key]['LogValue'],*/

							}];

							for (var i=0;i<newData.length;i++) {
								jQuery("#orderMovementGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
								historyrowid=historyrowid+1;
							}
						});

					}
				}
				else
				{
					showMessage("red", historyOrderData.Description, "");
				}
			
			},// end success.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
});
getCurrentLocation();

searchSourceLocation();
function searchSourceLocation()
{
	showMessage("loading", "Loading information...", "");
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'SearchOrderMovementCallBack',
		data:
		{
			id:"searchWHLocation",
		},
		success:function(data)
		{
		jQuery(".ajaxLoading").hide();
		
		var whlocationsData = jQuery.parseJSON(data);
		if(whlocationsData.Status==1){
			jsonForLocations=whlocationsData.Result;
			whlocations=whlocationsData.Result;
			
			jQuery.each(whlocations,function(key,value)
			{	
				jQuery("#SourceLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");

			});
			jQuery('#SourceLocation').val(currentLocationId);
			jQuery('#SourceLocation').attr('disabled',true);
			backGroundColorForDisableField();
		}
		else{
			showMessage('red',whlocationsData.Description,'');
		}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
}

searchDestinationLocation();
function searchDestinationLocation()
{
	showMessage("loading", "Loading information...", "");
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'SearchOrderMovementCallBack',
		data:
		{
			id:"searchDestinationLocation",
		},
		success:function(data)
		{
		jQuery(".ajaxLoading").hide();
		
		var whlocationsData = jQuery.parseJSON(data);
		if(whlocationsData.Status==1){
			jsonForLocations=whlocationsData.Result;
			whlocations=whlocationsData.Result;
			
			jQuery.each(whlocations,function(key,value)
			{	
				jQuery("#DestinationLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");

			});			
		}
		else{
			showMessage('red',whlocationsData.Description,'');
		}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
}

function loadHistoryOrder(historyOrderNo)
{
	 
	 showMessage("loading", "Loading history order...", "");
	 
	 jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'POSClient',
		data:
		{
			id:"getHistoryOrder",
			historyOrderNo:historyOrderNo,
			
		},
		success:function(data)
		{
			
			jQuery(".ajaxLoading").hide();
			
			//alert("returned result"+data);
			var historyOrderData = jQuery.parseJSON(data);
			
			if(historyOrderData.Status == 1)
			{
				var historyOrderInformation = historyOrderData.Result;//Previous  order history data storage.
					
					//Condition to check order is invoiced or not, 4 for invoiced order.
					if(historyOrderInformation[0][0]['Status'] == "Invoiced")
					{
						/*function historyOrderInvoicedState() //will be done later.
							
						}*/
						
						
						
						loadPreviousOrderInformation(historyOrderInformation,"Invoiced"); //Doubt
						
						invoiceStatus = 4;
						
						jQuery("#btnPOS01PrintOrder").attr('disabled',false).css( 'opacity', '1' );
						jQuery("#btnPOS01Confirm").attr('disabled',true).css( 'opacity', '.5' );
						
						invoiceOrderLoadScreenChanges(); //Function used to display order in view state only.
						
						jQuery("#orderStatus").css('background','#289925');
						
						jQuery('#jqgridtable_left').hideCol('myac'); //hiding column when order invoiced.
					}
					else if(historyOrderInformation[0][0]['Status'] == "Cancelled")
					{
						jQuery("#orderStatus").css('background','#FF9999');
						
						jQuery('#jqgridtable_left').hideCol('myac'); //hiding column when order cancel.
						
						jQuery("#POSRemarks").attr('disabled',true); //disable remarks for cancel order.
						//alert(historyOrderInformation[0][0]['Status']);
						
						jQuery(".pos_order_button").attr("disabled",true).css( 'opacity', '.5' );
						
						loadPreviousOrderInformation(historyOrderInformation,"CancelledOrders"); //Function used to load previous order.
						
						cancelledOrderLoadScreenChanges(); //Function used to display order in view state only.
					}
					else
					{
						jQuery("#orderStatus").css('background','#fcd22e');
						
						jQuery("#btnPOS01PrintOrder").attr('disabled',false).css( 'opacity', '1' );
						jQuery("#btnPOS01Confirm").attr('disabled',true).css( 'opacity', '.5' );
						jQuery("#btnPOS01ModifyOrder").attr('disabled',false).css( 'opacity', '1' );
						jQuery("#btnPOS01Cancel").attr('disabled',true).css( 'opacity', '.5' );
						
						loadPreviousOrderInformation(historyOrderInformation,"savedOrders"); //Function used to load previous order.
						
						enableModifyOrderButton();
			                                    			
						
					}
			}
			else
			{
				showMessage("red", historyOrderData.Description, "");
			}
			
			actionButtonsStauts(); //Buttons to be disable according to access privileges.
			
		}, //end success.
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	});
}
function vallidattionONAddLogBtn(){
	
	 //   if(jQuery("#historyList").val()==-1){
	//	showMessage("red","Please selet log From list","");
    //    return 0;	
	//}
	if (jQuery(".AddToLogSelection").find('input[type=checkbox]').attr('checked',true))
	{
		showMessage("red","Please select log From list","");
	}
}

jQuery("#btnOMV01move").unbind("click").click(function(){
	
	batchRow= jQuery("#orderMovementGrid").getGridParam('selrow');
	
	if(batchRow!= null){
		jQuery("#actionName").val('Save');
			var selRowId=jQuery("#orderMovementGrid").jqGrid('getRowData',batchRow);
		
		
		var LogNo=selRowId.LogNo;
		var DestinationLocation=jQuery('#DestinationLocation').val();
			
		var r = confirm("Are you sure you want to move log ?");
		if(r==true)	{	
			showMessage("loading", "Moving Log...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'SearchOrderMovementCallBack',
			data:
			{
				id:"OrderMove",
				LogNo:LogNo,
				DestinationLocation:DestinationLocation,
				functionName:jQuery("#actionName").val(),
				moduleCode:"OMV01"
				
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			var MovedData = jQuery.parseJSON(data);
			if(MovedData.Status==1){
				var keypair=MovedData.Result;
				if(keypair.length>=0){

					jQuery.each(keypair,function(key,value)
							{
						$('#orderMovementGrid').jqGrid('delRowData',batchRow);
						showMessage("green","Log Moved Successfully "+"");
							});


				}
				else{
					showMessage("red","Log not Moved. Error " + keypair ,"");
				}
			}
			else {
				showMessage('red',MovedData.Description,'');
			}

			
			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
	
});	}
		
	}
	else {
		showMessage("red","Please select log From list" ,"");
	}
});

showCalender();

jQuery("#btnReset").click(function(){
	 jQuery("#historyLogNo").val('');
		jQuery("#historyDistributorNo").val('');
			 
					
  	
      		jQuery("#orderMovementGrid").jqGrid("clearGridData"); 
      		
      		showCalender();
      		
      		
	
});
});
