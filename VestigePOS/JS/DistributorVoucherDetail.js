
var rowid = 0;
var historyOrderNo; 
var loggedInUserPrivileges;
var jsonForLocations ;
var expectedDate ;
var distributordata;
var DISTRIBUTOR_NO_VALIDATION = 0;
jQuery(document).ready(function(){
	jQuery(".breadcrumb").hide();
jQuery("#DistributorVoucherDeatailGrid").jqGrid({
    
    
    datatype: 'jsonstring',
    colNames: ['DistributorID','VoucherNo','CreatedBy','Amount','LocationId','Paymant Mode'
/*		                ,'DeliverToCityName',
               'DeliverToStateName','DeliverToCountryName','DeliverFromCityName','DeliverFromStateName','DeliverFromCountryName','IsPrintAllowed','InvoiceDate','ValidReportPrintDays',
               'OrderMode','UsedForRegistration','TerminalCode','TotalBV','TotalPV','DistributorAddress','OrderType','PCId','PCCode','BOId','DeliverFromAddress1','DelverFromAddress2',
               'DeliverFromAddress3','DeliverFromAddress4','DeliverFromCityId','DeliverFromPincode','DeliverFromStateId','DeliverFromCountryId','DeliverFromTelephone','DeliverFromMobile',
               'DeliverToMobile','TotalUnits','TotalWeight','OrderAmount','DiscountAmount','TaxAmount','PaymentAmount','ChangeAmount','CreatedBy','CreatedByName','CreatedDate',
               'DeliverToAddressLine1','DeliverTo','DeliverToAddressLine2','DeliverToAddressLine3','DeliverToAddressLine4','DeliverToCityId','DeliverToPincode','DeliverToStateId',
               'DeliverToCountryId','DeliverToTelephone'
               ,'LogValue'*/
               ],
    colModel: [

               { name: 'DistributorID', index: 'DistributorID', width:100},
               { name: 'VoucherNo', index: 'VoucherNo', width:160 },
               { name: 'CreatedBy', index: 'CreatedBy', width:128},
               { name: 'Amount', index: 'Amount', width:100 },
               { name: 'LocationId', index: 'LocationId', width:160 },
               { name: 'PaymantMode', index: 'PaymantMode', width:130 },
             /*  { name: 'DeliverToCityName', index: 'DeliverToCityName', width:20 ,hidden:true},
               { name: 'DeliverToStateName', index: 'DeliverToStateName', width:20 ,hidden:true},
               { name: 'DeliverToCountryName', index: 'DeliverToCountryName', width:20 ,hidden:true},
               { name: 'DeliverFromCityName', index: 'DeliverFromCityName', width:20 ,hidden:true},
               { name: 'DeliverFromStateName', index: 'DeliverFromStateName', width:20 ,hidden:true},
               { name: 'DeliverFromCountryName', index: 'DeliverFromCountryName', width:20 ,hidden:true},
               { name: 'IsPrintAllowed', index: 'IsPrintAllowed', width:20 ,hidden:true},
               { name: 'InvoiceDate', index: 'InvoiceDate', width:20 ,hidden:true},
               { name: 'ValidReportPrintDays', index: 'ValidReportPrintDays', width:20 ,hidden:true},
               { name: 'OrderMode', index: 'OrderMode', width:20 ,hidden:true},
               { name: 'UsedForRegistration', index: 'UsedForRegistration', width:20 ,hidden:true},
               { name: 'TerminalCode', index: 'TerminalCode', width:20 ,hidden:true},
               { name: 'TotalBV', index: 'TotalBV', width:20 ,hidden:true},
               { name: 'TotalPV', index: 'TotalPV', width:20 ,hidden:true},
               { name: 'DistributorAddress', index: 'DistributorAddress', width:20 ,hidden:true},          
               { name: 'OrderType', index: 'OrderType', width:20 ,hidden:true},
               { name: 'PCId', index: 'PCId', width:20 ,hidden:true}, 
               { name: 'PCCode', index: 'PCCode', width:20 ,hidden:true},
               { name: 'BOId', index: 'BOId', width:20 ,hidden:true},
               

               { name: 'DeliverFromAddress1', index: 'DeliverFromAddress1', width:20 ,hidden:true},
               { name: 'DelverFromAddress2', index: 'DelverFromAddress2', width:20 ,hidden:true},
               { name: 'DeliverFromAddress3', index: 'DeliverFromAddress3', width:20 ,hidden:true},
               { name: 'DeliverFromAddress4', index: 'DeliverFromAddress4', width:20 ,hidden:true}, 
               { name: 'DeliverFromCityId', index: 'DeliverFromCityId', width:20 ,hidden:true},
               { name: 'DeliverFromPincode', index: 'DeliverFromPincode', width:20 ,hidden:true},
               { name: 'DeliverFromStateId', index: 'DeliverFromStateId', width:20 ,hidden:true},
               { name: 'DeliverFromCountryId', index: 'DeliverFromCountryId', width:20 ,hidden:true},
               { name: 'DeliverFromTelephone', index: 'DeliverFromTelephone', width:20 ,hidden:true},
               { name: 'DeliverFromMobile', index: 'DeliverFromMobile', width:20 ,hidden:true}, 
              
               { name: 'DeliverToMobile', index: 'DeliverToMobile', width:20 ,hidden:true},
               { name: 'TotalUnits', index: 'TotalUnits', width:20 ,hidden:true},
               { name: 'TotalWeight', index: 'TotalWeight', width:20 ,hidden:true},
               { name: 'OrderAmount', index: 'OrderAmount', width:20 ,hidden:true},
               { name: 'DiscountAmount', index: 'DiscountAmount', width:20 ,hidden:true},
               { name: 'TaxAmount', index: 'TaxAmount', width:20 ,hidden:true},
               { name: 'PaymentAmount', index: 'PaymentAmount', width:20 ,hidden:true},
               { name: 'ChangeAmount', index: 'ChangeAmount', width:20 ,hidden:true},
               { name: 'CreatedBy', index: 'CreatedBy', width:20 ,hidden:true},
               { name: 'CreatedByName', index: 'CreatedByName', width:20 ,hidden:true},
               { name: 'CreatedDate', index: 'CreatedDate', width:20 ,hidden:true},
               
               { name: 'DeliverToAddressLine1', index: 'DeliverToAddressLine1', width:20,hidden:true },
               { name: 'DeliverTo', index: 'DeliverTo', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine2', index: 'DeliverToAddressLine2', width:20,hidden:true },
               { name: 'DeliverToAddressLine3', index: 'DeliverToAddressLine3', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine4', index: 'DeliverToAddressLine4', width:20 ,hidden:true},
               { name: 'DeliverToCityId', index: 'DeliverToCityId', width:20 ,hidden:true},
               { name: 'DeliverToPincode', index: 'DeliverToPincode', width:20 ,hidden:true},
               { name: 'DeliverToStateId', index: 'DeliverToStateId', width:20 ,hidden:true},
               { name: 'DeliverToCountryId', index: 'DeliverToCountryId', width:20 ,hidden:true},
               { name: 'DeliverToTelephone', index: 'DeliverToTelephone', width:20 ,hidden:true},
             
               
           
            
        
               { name: 'LogValue', index: 'LogValue', width:20, hidden:true }
*/		              
               ],
               pager: jQuery('#GatewayGridPager'),
               width:808,
               height:300,
               rowNum: 1000,
               rowList: [500],
               sortable: true,
               sortorder: "asc",
               hoverrows: true,
               shrinkToFit: false,
               ondblClickRow: function (id)
               {
            	   dblclkAction(id);
               }
               		


   });
	
	jQuery("#DistributorVoucherDeatailGrid").jqGrid('navGrid','#GatewayGridPager',{edit:false,add:false,del:false}); 
	jQuery("#GatewayGridPager").hide();	
	

	
jQuery("#btnPGR01Search").unbind("click").click(function(){
	
	
	
	jQuery("#actionName").val('');
	
	if(DISTRIBUTOR_NO_VALIDATION == 1)
		{
	showMessage("loading", "Searching Voucher...", "");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorVoucherDetailCallBack',
			data:
			{
				
				id:"searchDisVoucherDetail",
				DistributorID:jQuery("#Distributorid").val(),
				moduleCode:"VOCHR01",
				VoucherAmount:jQuery("#Amount").val(),
			    functionName:"Search",
			},
			success:function(data)
			{

				jQuery(".ajaxLoading").hide();

				var historyOrderData = jQuery.parseJSON(data);
				
				if(historyOrderData.Status == 1)
				{
					
					
					
					var historyrowid=0;
					var searchTO = historyOrderData.Result;
					if(searchTO.length==0)
					{
						jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 	
						showMessage("","No Record Found","");
					}

					else
					{	
						var message = "Total " + searchTO.length + " records found.";
						showMessage("", message,"");
						
						/*['Add To Log','Print','Invoice','Order No','Status','Log No'],*/
						jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 	
						jQuery .each(searchTO,function(key,value){	
							
							

							var newData = [{
								"DistributorID":searchTO[key]['DistributorID'],"VoucherNo":searchTO[key]['VoucherNO'],"CreatedBy":searchTO[key]['UserName'],"Amount":searchTO[key]['Amount'],"LocationId":searchTO[key]['Name'],"PaymantMode":searchTO[key]['paymentmode']
														/*		"DeliverToCityName":searchTO[key]['DeliverToCityName'],"DeliverToStateName":searchTO[key]['DeliverToStateName'],"DeliverToCountryName":searchTO[key]['DeliverToCountryName'],
								"DeliverFromCityName":searchTO[key]['DeliverFromCityName'],"DeliverFromCountryName":searchTO[key]['DeliverFromCountryName'],"IsPrintAllowed":searchTO[key]['IsPrintAllowed'],
								"InvoiceDate":searchTO[key]['InvoiceDate'],"ValidReportPrintDays":searchTO[key]['ValidReportPrintDays'],"OrderMode":searchTO[key]['OrderMode'],"UsedForRegistration":searchTO[key]['UsedForRegistration'],"TerminalCode":searchTO[key]['TerminalCode'],
								"TotalBV":searchTO[key]['TotalBV'],"TotalPV":searchTO[key]['TotalPV'],
								"DistributorAddress":searchTO[key]['DistributorAddress'],"OrderType":searchTO[key]['OrderType'],"PCId":searchTO[key]['PCId'],"PCCode":searchTO[key]['PCCode'],"BOId":searchTO[key]['BOId'],"DeliverFromAddress1":searchTO[key]['DeliverFromAddress1'],"DelverFromAddress2":searchTO[key]['DelverFromAddress2'],
								"DeliverFromAddress3":searchTO[key]['DeliverFromAddress3'],"DeliverFromAddress4":searchTO[key]['DeliverFromAddress4'],"DeliverFromCityId":searchTO[key]['DeliverFromCityId'],"DeliverFromPincode":searchTO[key]['DeliverFromPincode'],
								"DeliverFromStateId":searchTO[key]['DeliverFromStateId'],"DeliverFromCountryId":searchTO[key]['DeliverFromCountryId'],"DeliverFromTelephone":searchTO[key]['DeliverFromTelephone'],"DeliverFromMobile":searchTO[key]['DeliverFromMobile'],
								"DeliverToMobile":searchTO[key]['DeliverToMobile'],"TotalUnits":searchTO[key]['TotalUnits'],"TotalWeight":searchTO[key]['TotalWeight'],
								"OrderAmount":searchTO[key]['OrderAmount'],"DiscountAmount":searchTO[key]['DiscountAmount'],"TaxAmount":searchTO[key]['TaxAmount'],
								"PaymentAmount":searchTO[key]['PaymentAmount'],"ChangeAmount":searchTO[key]['ChangeAmount'],"CreatedBy":searchTO[key]['CreatedBy'],"CreatedByName":searchTO[key]['CreatedByName'],
								"CreatedDate":searchTO[key]['CreatedDate'],"DeliverToAddressLine1":searchTO[key]['DeliverToAddressLine1'],"DeliverTo":searchTO[key]['DeliverTo'],"DeliverToAddressLine2":searchTO[key]['DeliverToAddressLine2'],
								"DeliverToAddressLine3":searchTO[key]['DeliverToAddressLine3'],"DeliverToAddressLine4":searchTO[key]['DeliverToAddressLine4'],"DeliverToCityId":searchTO[key]['DeliverToCityId'],"DeliverToPincode":searchTO[key]['DeliverToPincode'],
								"DeliverToStateId":searchTO[key]['DeliverToStateId'],"DeliverToCountryId":searchTO[key]['DeliverToCountryId'],"DeliverToTelephone":searchTO[key]['DeliverToTelephone'],
								"LogValue":searchTO[key]['LogValue'],*/

							}];

							for (var i=0;i<newData.length;i++) {
								jQuery("#DistributorVoucherDeatailGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
								historyrowid=historyrowid+1;
							}
						});

					}
				}
				else
				{
					showMessage("red", historyOrderData.Description, "");
				}
			
			},// end success.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
		}
	else
		{
		showMessage("","Please validate distributor ID ","");
		}
	
});


showCalender();
jQuery("#btnReset").click(function(){ 
	 jQuery("#Distributorid").val('');
     jQuery("#Distributorname").val('');
		jQuery("#Amount").val('');
       		jQuery("#DistributorVoucherDeatailGrid").jqGrid("clearGridData"); 
       		jQuery("#Distributorid").attr('disabled',false);
       		jQuery("#Amount").attr('disabled',false);
       		jQuery("#PaymentStatus").attr('disabled',false);
       		jQuery("#btnVOCHR01Save").prop('disabled', false);
       		jQuery("#PaymentStatus").val('-1');
       		 DISTRIBUTOR_NO_VALIDATION = 0;
});



jQuery("#btnVOCHR01Save").unbind("click").click(function(){
	
	jQuery("#actionName").val('');
	
	
	
	if(DISTRIBUTOR_NO_VALIDATION == 1)
	{
		
		if(jQuery("#PaymentStatus").val() == -1)
		{
		showMessage("Red", "Please select Payment Mode", "");
		jQuery("#Amount").focus();
		return;
		}
		
		
		if(jQuery("#Amount").val() == '' || jQuery("#Amount").val() == null)
			{
			showMessage("Red", "Please Enter Amount", "");
			jQuery("#Amount").focus();
			return;
			}
		
		jQuery('#btnVOCHR01Save').prop('disabled', true);
	
		
		jQuery("#PaymentStatus").attr('disabled',true);
		jQuery("#Distributorid").attr('disabled',true);
   		jQuery("#Amount").attr('disabled',true);
	showMessage("loading", "Save Voucher Detail...", "");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorVoucherDetailCallBack',
			data:
			{
				
				id:"saveDisVoucherDetail",
				DistributorID:jQuery("#Distributorid").val(),
				moduleCode:"VOCHR01",
				VoucherAmount:jQuery("#Amount").val(),
				paymentmode:jQuery("#PaymentStatus").val(),
			    functionName:"Save",
			},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				
				jQuery("#btnVOCHR01Save").prop('disabled', false);
				
				var keypair=resultsData.Result;
				if(keypair.length>=0){

					jQuery.each(keypair,function(key,value)
							{
						
					if(value['SeqNo'] != ''  || value['SeqNo'] != null)
						showMessage("green","Created Successfully Voucher No "+value['SeqNo'],"");
							
							});
				}

					 
				}
		
						
					

			else{

				showMessage('red',resultsData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
		
	});
	}
	else
		{
			showMessage("", "Please Validate Distributor ID", "");
		}
});





jQuery("#Distributorid").attr('maxlength','8');

jQuery("#Distributorid").keydown(function(e){

	if(e.which == 13  || e.which == 9)
	{
		
		

		if(jQuery("#Distributorid").val()==''){
			
			jQuery("#Distributorid").css("background", "#FF9999");
			
			showMessage("","Enter valid distributor id.","");
			jQuery("#Distributorid").focus();
		}
		
		else
		{
			var distributorId = jQuery("#Distributorid").val(); //S - search distributor id field.
			
			if(distributorId.length != 8)
			{
				jQuery("#Distributorid").css("background", "#FF9999");
				
				showMessage("", "Enter valid distributor id", "");
				jQuery("#Distributorid").focus();
			}
			else
			{
				checkValidDistributor(distributorId); //function to validate distributor id.
			}
		}
		
		
		
	}
});
function checkValidDistributor(distributorId)
{
	if(distributorId == '')
	{
		jQuery("#Distributorid").css("background", "#FF9999");
		
		showMessage("", "Enter distributor id", "");
		jQuery("#Distributorid").focus();
		
		emptyFieldsData();
	}
	else
	{
		showMessage("loading", "Validating distributor details...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorVoucherDetailCallBack',
			data:
			{
				id:"checkDistributor",
				distributorId:distributorId,

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var returnedDistibutorData = jQuery.parseJSON(data);
				
				if(returnedDistibutorData.Status == 1)
				{
					var results = returnedDistibutorData.Result;
					distributordata=returnedDistibutorData.Result;
					
					jQuery.each(distributordata,function(key,value){
						

						jQuery("#Distributorname").val( distributordata[key]['Distributorname']);
						

					});
					
					if(results.length ==1)  //means not any distributor registered yetn on this id.
					{
						
						jQuery("#Distributorid").attr('disabled',true);
						jQuery("#Distributorid").css("background", "white");
						
						jQuery("#distributorname").val(results.distributorname);
						
						
						
						results.DistributorId = distributorId;	
						//validatedDistributorId.validatedUplineDistributorId = 1;
						 DISTRIBUTOR_NO_VALIDATION =1;
						
						
						
						//emptyFieldsData();
					}
					
					
				}
				else
				{
					showMessage("red", returnedDistibutorData.Description, "");
				}
				
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
}




jQuery("#btnVOCHR01Print").unbind("click").click(function(){
	
	batchRow= jQuery("#DistributorVoucherDeatailGrid").getGridParam('selrow');
	
	if(batchRow!= null){
		jQuery("#actionName").val('Print');
			var selRowId=jQuery("#DistributorVoucherDeatailGrid").jqGrid('getRowData',batchRow);
		
		
		var Distributorid=selRowId.DistributorID;
		var Voucherno=selRowId.VoucherNo;
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorVoucherDetailCallBack',
			data:
			{
				id:"printvoucher",
				DistributoriD:Distributorid,
				VoucherNO:Voucherno,
				functionName:"Print",
				moduleCode:"VOCHR01",
				
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			var printout = jQuery.parseJSON(data);
			if(printout.Status==1){
						var results=printout.Result;
						 jQuery.each(results,function(key,value)
								   {
							 			var arr = {'distributorid':results[key]['distributorid'],'voucherNo':results[key]['Voucherno']};
							 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
									 
							      });
					}
					else{
						
						showMessage('red',printout.Description,'');
					}

			
			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
	
});
		
	}
	else {
		showMessage("red","Please select one row From list" ,"");
	}
});


});
