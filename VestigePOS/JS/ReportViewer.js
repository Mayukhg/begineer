/*Custom exception to be thrown when field's are left blank*/
function FieldLeftBlankException(message){
	this.message = message;
}
var jsonForLocations ;

var STATUS_KEY = "Status";
var DESCRIPTION_KEY = "Description";
var RESULT_KEY = "Result";

var REPORT_PARAMETER = "service";
var REPORT_REQUIRED = "params";

function ResponseHandler(){
	this.onResponseReceived = function(response, reportData, serviceTag){
		hideShowingMessage();
		try {
			if(serviceTag === REPORT_PARAMETER){
				updateUIWithParams(response);
			}
			else if(serviceTag === REPORT_REQUIRED){
				var responseInJson = JSON.parse(response);
				
				if(responseInJson[STATUS_KEY] == 1){
					hideShowingMessage();
					var results = responseInJson[RESULT_KEY];
					if(results.length != 0){
						generatingRprtMsg();
						forwardToReportView(results);
					}
					else{
						showMessage("yellow", "Details of reports not found. Error.", "");
						hideMessageAfterTime(2000);
					}
				}
			}
			else{
				showMessage("yellow", "Unknown operation. Error " + JSON.parse(response), "");
				hideMessageAfterTime(2000);
			}
		} catch (e) {
			showMessage("red", "JSON Parser Error " + e.message, "");
			hideMessageAfterTime(2000);
		}
		
	};
}
var whlocations = null;
function forwardToReportView(results){
		var baseUrl = results['BaseUrl'];
		var url = results['Url'];
		var reportData = results['ReportData'];
		reportData['BoLocationId']= currentLocationId;
		reportData['BOLocationId'] = currentLocationId;
		reportData['locationId'] = currentLocationId;
		reportData["GeneratedBy"] = loggedInUserFullName;
		viewReport(baseUrl, url, reportData);
}

function updateUIWithParams(params){
	jQuery(".divReportParams").html(params);
	if(jQuery(".showCalender").length)
		showCalender();
	
	if(jQuery("select.CommonSelect").length)
		jQuery("select.CommonSelect").css("width","152");
	
	if(jQuery("#PCLocationId").length){
		if(whlocations == null){
			showMessage("loading", "loading pick up centers...", "");
			getPUCId();
		}
	
		else{
			updatePUCLocation();
			if(jQuery("#PCLocationId").length && jQuery("#UsernameId").length){
				showMessage("loading", "loading users...", "");
				var pcLocation = jQuery("#PCLocationId").val();
				if(pcLocation != "")
					getUsersAtThisPCLocation(pcLocation);
			}
		}
	}
	if(jQuery("#businessMonth").length){
		showMessage("loading", "loading business months centers...", "");
		getBusinessMonths();

	}
	
	
}
/*load reports parameter*/
function loadSearchParameter(whichReport){
	var handler = new ResponseHandler();
	jQuery.ajax({
		type:"POST",
		url:Drupal.settings.basePath + 'POSReportsCallback',
		data:{
			WhichReport:whichReport
		},
		success:function(response){
			handler.onResponseReceived(response, null, REPORT_PARAMETER);
		},
		error:function(XMLRequest, textResponse, error){
			hideShowingMessage();
			showMessage("red", textResponse+error, "");
			hideMessageAfterTime(2000);
		}
	});
}

/*function to convert serialized array data into json data*/
function convertArrayToJson(data) {
 var json = {};
 jQuery.each(data, function(){
  json[this.name] = this.value || '';
 });
 
 return json;
}
/*function to serialize (this->form reference passed as parameter into the function) form data*/
function serializeReportData(that){
	return jQuery(that).serializeArray();
}
/*submit report parameter*/
/*function to show report in div*/
function viewReport(reportBaseURL,reportUrl,reportParam){
	var form = document.createElement("form");
	form.setAttribute("method", "POST");

	form.setAttribute("action", reportBaseURL+reportUrl);


	form.setAttribute("target", "reportIframe");

	//var params = new Array();

	for(keyName in reportParam){
		var key = keyName;
		var value = reportParam[keyName];
		var hiddenField = document.createElement('input');
		hiddenField.setAttribute("name",key);
		hiddenField.setAttribute("value",value);
		hiddenField.setAttribute("type",'hidden');
		form.appendChild(hiddenField);
	}

	document.body.appendChild(form);
	
	hideShowingMessage();
	form.submit();
	hideShowingMessage();
}

/*function to clear the message after this much time*/
function hideMessageAfterTime(timeInSecs){
	setTimeout(function(){
		hideShowingMessage();
	}, timeInSecs);
}

/*To hide the screen message*/
function hideShowingMessage(){
	jQuery(".ajaxLoading").hide();
}

var logNos;
var whlocations;
function getPUCId()
{
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'ReportViewerCallback',
		data:
		{
			id:"getPUCId",
		},
		success:function(data)
		{
			//alert(data);
			
			jsonForLocations=data;
			whlocations = jQuery.parseJSON(data);
			hideShowingMessage();
			updatePUCLocation();
		},
		error:function(XMLRequest, textReponse, error){
			showMessage("red", ""+error, "");
			hideMessageAfterTime(2000);
		}
	
	});
}

function getBusinessMonths(){
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'ReportViewerCallback',
		data:
		{
			id:"getBusinessMonths",
		},
		success:function(data)
		{
			//alert(data);
			
			jsonForLocations=data;
			businessMonths = jQuery.parseJSON(data);
			hideShowingMessage();
			updateBusinessMonthLocation(businessMonths);
		},
		error:function(XMLRequest, textReponse, error){
			showMessage("red", ""+error, "");
			hideMessageAfterTime(2000);
		}
	
	});
}

function updateBusinessMonthLocation(businessMonths){
	jQuery("#businessMonth").empty();
	
	jQuery.each(businessMonths,function(key,value){	
		jQuery("#businessMonth").append("<option id=\""+businessMonths[key]['MonthCloseDate']+"\""+" value=\""+businessMonths[key]['MonthCloseDate']+"\""+">"+businessMonths[key]['MonthCloseDate']+"</option>");
	});
}

function updatePUCLocation(){
	jQuery.each(whlocations,function(key,value)
			{	
				jQuery("#PCLocationId").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['LocationCode']+"</option>");
			});
}
var usersResult;
/*function to get users at this pc locations*/
function getUsersAtThisPCLocation(PCLocation){
	//var handler = new ServerResponseHandler();
	jQuery.ajax({
		type:"POST",
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:{
			id:'getUserNameForThisPCLocation',
			PCLocationId:PCLocation
		},
		success:function(response){
			//handler.onResponseReceived(response, null, GET_USER_AT_PC_LOC);
			hideShowingMessage();
			
			var values = JSON.parse(response);
			if(values["Status"] == 1){
				var users = '';
				usersResult = JSON.parse(values["Result"]);
				
				if(usersResult.length){
					jQuery.each(usersResult, function(index, obj){
						
						users+='<option value="'+obj["UserId"]+'">'+obj["UserName"]+'</option>';
					});
					jQuery("#UsernameId").html(users);
				}
				else{
					showMessage("yellow", "No users", "");
					hideMessageAfterTime(2000);
				}
			}
			else{
				showMessage("yellow", ""+values["Description"], "");
				hideMessageAfterTime(2000);
			}
		},
		error:function(XMLRequest, textResponse, error){
			hideShowingMessage();
			showMessage("red", textResponse + "" + error, "");
			hideMessageAfterTime(2000);
		}
	});
}
function updateUsers(){
	if(usersResult.length){
		jQuery.each(usersResult, function(index, obj){
			
			users+='<option value="'+obj["UserId"]+'">'+obj["UserName"]+'</option>';
		});
		jQuery("#UsernameId").html(users);
	}
}

function submitFormData(){
	try {
		jQuery(".showCalender").datepicker("option","dateFormat", "yy-mm-dd");
		var reportData = serializeReportData(jQuery(".ReportForm"));
		jQuery(".showCalender").datepicker("option","dateFormat", "d-M-yy");
		checkIfFieldsAreLeftBlank(reportData);
		var reportJsonData = convertArrayToJson(reportData);
		submit(reportJsonData);
	} catch (e) {
		if(e instanceof FieldLeftBlankException){
			hideShowingMessage();
			showMessage("yellow", e.message, "");
			hideMessageAfterTime(2000);
		}
		else{
			hideShowingMessage();
			showMessage("red", e.message, "");
			hideMessageAfterTime(2000);
		}
	}
}

/*function to check if fields are left blank*/
function checkIfFieldsAreLeftBlank(reportData){
	/*jQuery.each(reportData, function(index, obj){
		if(obj.value === "")
			throw new FieldLeftBlankException("fill all fields");
	});*/
	if(jQuery(".isBlank").length){
		if(jQuery(".isBlank").val() === '')
			throw new FieldLeftBlankException("Enter PO Number");
	}
	if(jQuery(".isDistributor").length){
		if(jQuery(".isDistributor").val() === '')
			throw new FieldLeftBlankException("Enter Distributor Id");
	}
}

function submit(reportJsonData){
	var handler = new ResponseHandler();
	jQuery.ajax({
		type:"POST",
		url:Drupal.settings.basePath + 'POSReportsUrlCallback',
		data:{
			ReportData:reportJsonData
		},
		success:function(response){
			handler.onResponseReceived(response, null, REPORT_REQUIRED);
		},
		error:function(XMLRequest, textResponse, error){
			hideShowingMessage();
			showMessage("red", error+" "+textResponse, "");
			hideMessageAfterTime(2000);
		}
	});
}

function generatingRprtMsg(){
	showMessage("loading", "Retrieving report data...", "");
}

jQuery(document).ready(function(){ 
	
	
	
	jQuery("#header").hide();
	jQuery(".breadcrumb").hide();
	jQuery("#triptych-wrapper").hide();
	jQuery("#footer-wrapper").hide();
	jQuery(".title").hide();
	
	showMessage("loading", "Please wait...", "");
	getCurrentLocation();
	//getPUCId();
	
	function fillPUCIdFromLocalVariable(){
		
		var whlocations = jQuery.parseJSON(jsonForLocations);
		jQuery.each(whlocations,function(key,value)
		{	
			jQuery("#PCLocationId").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['LocationCode']+"</option>");

		});
	}
	
	
	toggleBtn();
	initialState();
	
	jQuery(".reportViewerAction").click(function(){
		var actionButtonId = this.id;
		jQuery("#reportIframe").attr("src","");
		
		if(actionButtonId === "stockSalesReport")
		{
			initialState();
		}
		if(actionButtonId === "salesCollectionReport")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("salesCollectionReport");
			jQuery("#view").click(function(){
				
				submitFormData();
			});
			jQuery("#close").click(function(){
				window.close();
			});
		}
		if(actionButtonId === "stockistSalesLog")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("stockistSalesLog");
			jQuery("#view").click(function(){
				generatingRprtMsg();
				submitFormData();
			});
			jQuery("#close").click(function(){
				window.close();
			});
		}
		if(actionButtonId === "distributorBusinessReport")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("distributorBusinessReport");
			jQuery("#view").unbind('click').click(function(){
				if(jQuery("#DistributorId").val() == ''){
					showMessage("", "Enter distributor id", "");
					jQuery("#DistributorId").focus();
					return;
				}
				generatingRprtMsg();
				submitFormData();
			});
			jQuery("#close").click(function(){
				window.close();
			});
		}
		if(actionButtonId === "distributorAddressLogReport")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("distributorAddressLogReport");
			jQuery("#view").click(function(){
				generatingRprtMsg();
				submitFormData();
			});
			jQuery("#close").click(function(){
				window.close();
			});
		}
		if(actionButtonId === "distributorSingleAddressReport")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("distributorSingleAddressReport");
			jQuery("#view").click(function(){
				generatingRprtMsg();
				submitFormData();
			});
			jQuery("#close").click(function(){
				window.close();
			});
		}
		
		if(actionButtonId === "logStatus")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("logStatus");
			bindUnbindBtnClick();
		}
		if(actionButtonId === "orderPrint")
		{
			showMessage("loading", "Please wait...", "");
			loadSearchParameter("orderPrint");
			bindUnbindBtnClick();
		}
		
	});
	
	
	function bindUnbindBtnClick(){
		jQuery("#view").click(function(){
			generatingRprtMsg();
			submitFormData();
		});
		jQuery("#close").click(function(){
			window.close();
		});
	}
	
	
	/*--------------------------initial State-------------------------*/
	function initialState()
	{
		disableStockistSaleBTN();
		showMessage("loading", "Please wait...", "");
		loadSearchParameter("stockSalesReport");
		jQuery("#view").click(function(){
			generatingRprtMsg();
			submitFormData();
		});
		jQuery("#close").click(function(){
			window.close();
		});
	}
	
	function disableStockistSaleBTN(){
		var kids = jQuery(".divReportViewerAction").children();
		jQuery(kids[0]).attr("disabled", true);
		jQuery(kids[0]).addClass("hilite");
	}
	
	function toggleBtn(){
		jQuery( ".divReportViewerAction" ).click(function ( event ) {
			  jQuery( "*" ).removeAttr("disabled");
			  jQuery("*").removeClass("hilite");
			  jQuery(event.target).attr("disabled",true);
			  jQuery(event.target).addClass("hilite");
			  event.preventDefault();
			});
	}
	
	
	

});
