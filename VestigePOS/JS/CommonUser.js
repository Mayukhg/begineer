function getLoggedInUserFullName(){
	jQuery.ajax({
		type:"Post",
		url:Drupal.settings.basePath + 'LoggedInUserCallback',
		success:function(response){
			try {
				var resp = JSON.parse(response);
				if(resp["Status"] == 1){
					var result = resp["Result"];
					
					result = jQuery.parseJSON(result);
					
					loggedInUserFullName = result["Username"];
					loggedInUserLocationName = result["UserLocation"];
					
					loggedInUserTime = result["UserLoginTime"];
					              ip=result["IP"];
					
					jQuery("#loggedInUserInfo").html(loggedInUserFullName+" Check in: "+loggedInUserLocationName +" at "+loggedInUserTime+" IP:  "+ip);
					
				}
				else{
					showMessage("yellow", "Exception in finding pos username "+resp["Description"], "");
					var result = resp["Result"];
					loggedInUserFullName = result["Username"];
				}
			} catch (e) {
				showMessage("red", ""+e.message, "");
			}
		},
		error:function(XMLRequest, textReponse, error){
			showMessage("yellow", "Could not get pos username "+error+" "+textReponse, "");
			hideShowingMessage();
		}
		
	});
	
}

var loggedInUserFullName = "";

var loggedInUserLocationName = "";

var loggedInUserTime = "";
var ip="";

jQuery("document").ready(function(){
	getLoggedInUserFullName();
});