
jQuery(document).ready(function(){
	
	uploadBonusExcelFile();
	
});


function uploadBonusExcelFile(){
	jQuery("#uploadDocument").click(function(){
		
		
		var bonusExcelFile = document.getElementById("uploadBusinessFileId").files[0];
		
		if(bonusExcelFile == undefined){
			showMessage("", "Please choose any file", "");
			return;
		}
		
		var formData = new FormData();
		
		formData.append("bonusExcelFile", bonusExcelFile);
		
		
		
			jQuery.ajax({
	            url:"/sites/all/modules/VestigePOS/VestigePOS/Business/UploadBonusFile.php",
	            type: "POST",
	            data: formData,
	            processData: false,
	            contentType: false,
	            success: function (msg) {
	                alert(msg);
	            },
	            error:function(msg){
	            	alert(msg);
	            }
	        });
			
			//e.preventDefault();
			
			document.log(formData);
		
	});
	
}