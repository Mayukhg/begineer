
var countries;
var states,cities;
var countryId,stateId,cityId;
var indicate = 0;
var loggedInUserPrivileges ;
var currentLogNo ;
jQuery(document).ready(function(){
	
	
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 625,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	  });
	showCalender();
	//backGroundColorForDisableField();
	jQuery("#header").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	
	jQuery("#footer-wrapper").hide();
	
	var moduleCode = jQuery("#moduleCode").val();
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
		data:
		{
			id:"StockCountModuleFuncHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);
			
			if(loggedInUserPrivilegesForStockCountData.Status == 1)
			{
				
				loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;
				
				if(loggedInUserPrivileges.length == 0)
				{
					jQuery(".LogTeamOrderButtons").attr('disabled',true);
				}
				else
				{
					searchLogTeamInfo();
					logTeamOrderPaymentInfo();
					disableFieldsOnStartUp();
					loadCountries();
					showCalender();
					disableButtons();
					jQuery("#LTOPickUpCenterId").attr('disabled',false);
					jQuery("#LTODistributorId").attr('disabled',true);
					emptyDistributorIdField();
					getPickUpCenter(); 
					actionButtonsStauts();
				}
				
				
			}
			else
			{
				showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
	function actionButtonsStauts()
	{
		jQuery(".LogTeamOrderButtons").each(function(){
			
			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");
			
			if(loggedInUserPrivileges[extractedButtonId] == 1)
			{
				
				//jQuery("#"+buttonid).attr('disabled',false);
				
			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}
			
		});
	}
	
	
	/* it is for short inventory DialogBOX */
	jQuery(function() {
	    jQuery( "#shortInventorydiv" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "blind",
	        duration: 1000
	      },
	      hide: {
	        effect: "explode",
	        duration: 1000
	      }
	    });
	    jQuery(function() {
	        jQuery( "#logTeamOrderInvoicePopUp" ).dialog({
	          autoOpen: false,
	          minWidth: 640,
	          show: {
	            effect: "clip",
	            duration: 200,
	          },
	          hide: {
	            effect: "clip",
	            duration: 200
	          }
	        });
	        jQuery( "#InvoiceStatusGrid" ).dialog({
	  	      autoOpen: false,
	  	      minWidth: 640,
	  	      show: {
	  	        effect: "blind",
	  	        duration: 1000
	  	      },
	  	      hide: {
	  	        effect: "explode",
	  	        duration: 1000
	  	      }
	  	    });
			
			jQuery( "#receivePayPopUp" ).dialog({
		  	      autoOpen: false,
		  	      minWidth: 770,
		  	      show: {
		  	        effect: "blind",
		  	        duration: 1000
		  	      },
		  	      hide: {
		  	        effect: "explode",
		  	        duration: 1000
		  	     }
		  	});

	    });
	 
	});
	
	
	/*--------------------------------------------------------------------------------------------------------------------------------*/
	
	function disableButtons()
	{
		jQuery("#btnPOS02Invoice").attr('disabled',false);
		jQuery("#btnPOS02Close").attr('disabled',true);
	}
	
	/**
	 * Function generating grid have information about Log/Team.
	 */
	
	function searchLogTeamInfo()
	{
		jQuery("#LogTeamOrderGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['LogNo','Distributor Id','LogType','Distributor Name','Pick Up Center','Status','Log Order Total','Log Invoice Total','Log Date','Address1','Address1',
			           'Address1','Address1','Log Inv Weight','Country Code','State Code','CityCode','Pin Code','Phone1','Phone2','Mobile1','Mobile2','Email1','Email2','Fax1','Fax2',
			           'CityName','State Name','CountryName','CreatedBy','ModifiedBy','ModifiedDate','Website','logCashTotal','LogCreditCardTotal','LogChequeTotal',
			           'LogBonusChequeTotal','LogBankTotal','LogValue','isChangeAddress','isZeroLog','UnpaidAmount'],
			           colModel: [{ name: 'LogNo', index: 'LogNo', width: 200},
			                      { name: 'DistributorId', index: 'DistributorId', width: 100},
			                      { name: 'LogType', index: 'LogType', width: 70},
			                      { name: 'DistributorName', index: 'DistributorName', width: 100},
			                      { name: 'PickUpCenter', index: 'PickUpCenter', width: 100},
			                      { name: 'Status', index: 'Status', width: 70},
			                      { name: 'LogOrderTotal', index: 'LogOrderTotal', width: 130,sorttype:'int'},
			                      { name: 'LogInvoiceTotal', index: 'LogInvoiceTotal', width: 130,sorttype:'int'},
			                      { name: 'LogDate', index: 'LogDate', width: 100},
			                      { name: 'Address1', index: 'Address1', width: 70, hidden:true},
			                      { name: 'Address2', index: 'Address2', width: 70, hidden:true},
			                      { name: 'Address3', index: 'Address3', width: 70, hidden:true},
			                      { name: 'Address4', index: 'Address4', width: 70, hidden:true},
			                      
			                      { name: 'LogInvoiceWeight', index: 'LogInvoiceWeight', width: 100},
			                      { name: 'CountryCode', index: 'CountryCode', width: 70, hidden:true},
			                      { name: 'StateCode', index: 'StateCode', width: 70, hidden:true},
			                      { name: 'CityCode', index: 'CityCode', width: 70, hidden:true},
			                      { name: 'PinCode', index: 'PinCode', width: 70, hidden:true},
			                      { name: 'Phone1', index: 'Phone1', width: 70, hidden:true},
			                      { name: 'Phone2', index: 'Phone2', width: 70, hidden:true},
			                      { name: 'Mobile1', index: 'Mobile1', width: 70, hidden:true},
			                      { name: 'Mobile2', index: 'Mobile2', width: 70, hidden:true},
			                      { name: 'Email1', index: 'Email1', width: 70, hidden:true},
			                      { name: 'Email2', index: 'Email2', width: 70, hidden:true},
			                      { name: 'Fax1', index: 'Fax1', width: 70, hidden:true},
			                      { name: 'Fax2', index: 'Fax2', width: 70, hidden:true},         
			                      { name: 'CityName', index: 'CityName', width: 70, hidden:true},
			                      { name: 'StateName', index: 'StateName', width: 70, hidden:true},
			                      { name: 'CountryName', index: 'CountryName', width: 70, hidden:true},
			                      { name: 'CreatedBy', index: 'CreatedBy', width: 70, hidden:true},
			                      { name: 'ModifiedBy', index: 'ModifiedBy', width: 70, hidden:true},
			                      { name: 'ModifiedDate', index: 'ModifiedDate', width: 70, hidden:true},
			                      { name: 'Website', index: 'Website', width: 70, hidden:true},
			                      { name: 'LogCashTotal', index: 'LogCashTotal', width: 70, hidden:true},
			                      { name: 'LogCreditCardTotal', index: 'LogCreditCardTotal', width: 70, hidden:true},
			                      { name: 'LogChequeTotal', index: 'LogChequeTotal', width: 70, hidden:true},
			                      { name: 'LogBonusChequeTotal', index: 'LogBonusChequeTotal', width: 70, hidden:true},
			                      { name: 'LogbankTotal', index: 'LogbankTotal', width: 70, hidden:true},
			                      { name: 'LogValue', index: 'LogValue', width: 70, hidden:true},
			                      { name: 'isChangeAddress', index: 'isChangeAddress', width: 70, hidden:true},
			                      { name: 'isZeroLog', index: 'isZeroLog', width: 70, hidden:true},
								  { name: 'UnpaidAmount', index: 'UnpaidAmount', width: 70, hidden:true}],
			                      pager: jQuery('#pjmap_logTeamOrderGrid'),
			                      width: 900,
			                      height:120,
			                      rowNum: 20,
			                      rowList: [500],
			                      sortname: 'Label',
			                      sortorder: "ASC",
			                      hoverrows: true,
			                      altRows:true,
			                      /* altclass:'AltRowClass'*/

			                      afterInsertRow : function(ids)
			                      {


			                      },

			                      loadComplete: function() {


			                      },

			                      onSelectRow: function(ids) {

			                    	  clearFields();
			                    	  jQuery("#btnPrintLog").attr('disabled',false);
			                    	  var rowId=jQuery('#LogTeamOrderGrid').jqGrid('getGridParam', 'selrow');
			                    	  var logNo = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogValue');
			                    	  jQuery("#LTODistributorId").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'DistributorId'));
			                    	  jQuery("#LTODistributorName").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'DistributorName'));
			                    	  jQuery("#addressLine1").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Address1'));
			                    	  jQuery("#addressLine2").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Address2'));
			                    	  jQuery("#addressLine3").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Address3'));
			                    	  var address4 = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Address4');
			                    	  if(address4 == null)
			                    	  {
			                    		  jQuery("#addressLine4").val('');
			                    	  }
			                    	  else
			                    	  {
			                    		  jQuery("#addressLine4").val(address4);
			                    	  }
			                    	  countryId =jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'CountryCode');

			                    	/*  stateId =jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'StateCode');
			                    	  loadStates(countryId,stateId); 
			                    	  cityId =jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'CityCode');
			                    	  loadCities(stateId,cityId);*/
			                    	  jQuery("#LTONumber").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogValue'));
			                    	 /* jQuery("#logStatus").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Status'));*/
			                    	  jQuery("#LTOPinCode").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'PinCode'));
			                    	  jQuery("#LTOPhone1").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Phone1'));
			                    	  jQuery("#LTOPhone2").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Phone2'));
			                    	  jQuery("#LTOMobile1").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Mobile1'));
			                    	  jQuery("#LTOMobile2").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Mobile2'));
			                    	  jQuery("#LTOEmail1").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Email1'));
			                    	  jQuery("#LTOEmail2").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Email2'));
			                    	  jQuery("#LTOFax1").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Fax1'));
			                    	  jQuery("#LTOFax2").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Fax2'));
			                    	  jQuery("#LTOWebsite").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Website'));
			                    	  jQuery("#LTOWebsite").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Website'));
			                    	  if (jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'isZeroLog')==1){
			                    			jQuery("#zeroLog").attr('checked',true); 
			                    	  }
			                    	  jQuery("#isChangeAddress").val(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'isChangeAddress'));
			                    	  getCourierDetails(jQuery("#LTONumber").val());
			                    	  showMessage("loading", "Loading Log Information...", "");
			                    	  if(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Status')=='Closed'){
			                    		  jQuery("#btnCourierDetails").attr('disabled',false);
			                    	  }
			                    	  else{
			                    		  jQuery("#btnCourierDetails").attr('disabled',true);
			                    	  }
			                    	  jQuery.ajax({
			                    		  type:'POST',
			                    		  url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			                    		  data:
			                    		  {
			                    			  id:"searchPaymentsForLogNo",
			                    			  LogNo:logNo,
			                    		  },
			                    		  success:function(data)  /*ajax call for populating payment grid*/
			                    		  {
			                    			  jQuery(".ajaxLoading").hide();
			                    			  var rowId=1;
			                    			  var logNumberParsedJsonData = jQuery.parseJSON(data);
			                    			  if(logNumberParsedJsonData.Status==1){
			                    				  logNumberParsedJson=logNumberParsedJsonData.Result;
			                    				  jQuery("#LTOPaymentDescGrid").jqGrid("clearGridData");
			                    				  jQuery.each(logNumberParsedJson,function(key,value)
			                    						  {
			                    					  var bank = parseFloat(logNumberParsedJson[key]['BANK'],10).toFixed(2);	
			                    					  var bonusCheck = parseFloat(logNumberParsedJson[key]['BONUSCHECK'],10).toFixed(2);    
			                    					  var cash = parseFloat(logNumberParsedJson[key]['CASH'],10).toFixed(2);    
			                    					  var cheque =parseFloat( logNumberParsedJson[key]['CHEQUE'],10).toFixed(2);    
			                    					  var COD = parseFloat(logNumberParsedJson[key]['COD'],10).toFixed(2); 
			                    					  var creditCard =parseFloat( logNumberParsedJson[key]['CREDITCARD'],10).toFixed(2);     
			                    					  var forex =parseFloat( logNumberParsedJson[key]['FOREX'],10).toFixed(2);     
			                    					  var onAccount = parseFloat(logNumberParsedJson[key]['ONACCOUNT'],10).toFixed(2);      
			                    					  var changeAmount =parseFloat( logNumberParsedJson[key]['CHANGEAMOUNT'],10).toFixed(2); 
			                    					  var newData = [{"Bank":bank,"BonusCheck":bonusCheck,"Cash":cash,"Cheque":cheque
			                    						  ,"COD":COD,"CreditCard":creditCard,"Forex":forex,"OnAccount":onAccount,"ChangeAmount":changeAmount}];
			                    					  for (i=0;i<newData.length;i++) {
			                    						  jQuery("#LTOPaymentDescGrid").jqGrid('addRowData',rowId, newData[newData.length-i-1], "first");
			                    						  rowId=rowId+1;
			                    					  }



			                    						  });
			                    			  }
			                    			  else{
			                    				  showMessage('red',logNumberParsedJsonData.Description,'');
			                    			  }
			                    		  },
			                    		  error: function(XMLHttpRequest, textStatus, errorThrown) { 

			                  				jQuery(".ajaxLoading").hide();
			                  				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			                  				showMessage("", defaultMessage, "");
			                  			}
			                    	  });  	
			                      },
			                      ondblClickRow: function (id) {
			                    	 
			                    	  var rowId=jQuery('#LogTeamOrderGrid').jqGrid('getGridParam', 'selrow');
			                    	   currentLogNo = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogValue');
			                    	  searchForOrders(currentLogNo);
				                		//function to call pack unpack report.
				                		
				                   }
		});



		jQuery("#LogTeamOrderGrid").jqGrid('navGrid','#pjmap_logTeamOrderGrid',{edit:false,add:false,del:false});	
		jQuery("#pjmap_logTeamOrderGrid").hide();
	}

	/*--------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function gererating grid have information about log/team order payment.
	 */
	function searchForOrders($logNo){
		showMessage("loading", "Loading Log Information...", "");
  	  jQuery.ajax({
  		  type:'POST',
  		  url:Drupal.settings.basePath + 'LogTeamOrderCallback',
  		  data:
  		  {
  			  id:"searchForLogOrders",
  			  LogNo:$logNo,
  		  },
  		  success:function(data)  /*ajax call for populating payment grid*/
  		  {
  			  jQuery(".ajaxLoading").hide();
  			  var rowId=1;
  			  var rowId2=1 ;
  			  var searchForLogOrdersData = jQuery.parseJSON(data);
  			  if(searchForLogOrdersData.Status==1){
  				searchForLogOrders=searchForLogOrdersData.Result;
  				  jQuery("#LOGInvoicedOrders").jqGrid("clearGridData");
  				  jQuery("#LOGPendingOrders").jqGrid("clearGridData");
  				  if(searchForLogOrders.length>0){
  					 jQuery( "#logTeamOrderInvoicePopUp" ).dialog( "open" );
  	  				  
			  				  jQuery.each(searchForLogOrders,function(key,value)
			  						  { 
			  					
						  					 if(searchForLogOrders[key]['InvoiceNo']!=''){
									  					  var newData1 =	[{"Invoice":searchForLogOrders[key]['InvoiceNo'],"OrderNo":searchForLogOrders[key]['CustomerOrderNo']
									  					  ,"DistributorId":searchForLogOrders[key]['DistributorId'],"OrderDate":displayDate(searchForLogOrders[key]['OrderDate']),"OrderRefNo":searchForLogOrders[key]['OrderRefNo']
									  						  ,"OrderAmount":parseFloat(searchForLogOrders[key]['OrderAmount'],10).toFixed(2)}];
									  					  for (var i=0;i<newData1.length;i++) {
									  						  jQuery("#LOGInvoicedOrders").jqGrid('addRowData',rowId, newData1[newData1.length-i-1], "first");
									  						  rowId=rowId+1;
									  					  	}
						  					  }
						  					 else{
						  						  var newData2 =	[{"Invoice":searchForLogOrders[key]['InvoiceNo'],"OrderNo":searchForLogOrders[key]['CustomerOrderNo']
							  					  ,"DistributorId":searchForLogOrders[key]['DistributorId'],"OrderDate":displayDate(searchForLogOrders[key]['OrderDate']),"OrderRefNo":searchForLogOrders[key]['OrderRefNo']
							  						  ,"OrderAmount":parseFloat(searchForLogOrders[key]['OrderAmount'],10).toFixed(2)}];
							  					  for (var j=0;j<newData2.length;j++) {
							  						  jQuery("#LOGPendingOrders").jqGrid('addRowData',rowId2, newData2[newData2.length-j-1], "first");
							  						rowId2=rowId2+1;
							  					  	}
						  					 }
							  					 
						  					 
			  						  });
  				  }
  			  }
  			  else{
  				  showMessage('red',searchForLogOrdersData.Description,'');
  			  }
  		  },
  		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
  	  });  	
	}
	
	jQuery("#btnPrintLog").click(function(){
		  var rowId=jQuery('#LogTeamOrderGrid').jqGrid('getGridParam', 'selrow');
   	   currentLogNo = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogValue');
   	
   	 var logstatus=jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Status');		   	
 	 if (logstatus == 'Closed')				
 	  {		
 		  printOrdersForLog(currentLogNo) ;		
 		  		
 	  }		
 	 else {		
 	  showMessage("", "Only logs with closed status can be printed", "");		
 	  }
	});
	function printOrdersForLog($logNo){
		showMessage("loading", "Loading Log Information...", "");
  	  jQuery.ajax({
  		  type:'POST',
  		  url:Drupal.settings.basePath + 'LogTeamOrderCallback',
  		  data:
  		  {
  			  id:"printOrdersForLog",
  			  LogNo:$logNo,
  		  },
  		  success:function(data)  /*ajax call for populating payment grid*/
  		  {
  			  jQuery(".ajaxLoading").hide();
  			  var rowId=1;
  			  var rowId2=1 ;
  			  var logOrdeCount = 0;
  			  
  			  var searchForLogOrdersData = jQuery.parseJSON(data);
  			  if(searchForLogOrdersData.Status==1){
  				searchForLogOrders=searchForLogOrdersData.Result;

  				
  				var logOrderCount = searchForLogOrders[0].AllOrders.split(',');

  				
  				showReport(searchForLogOrders[0]["reportUrl"],searchForLogOrders[0]["reportPath"],searchForLogOrders[0],'',logOrderCount.length);

  				
  			  }
  			  else{
  				  showMessage('red',searchForLogOrdersData.Description,'');
  			  }
  		  },
  		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
  	  });  	
	}
	function callJSPServerletForPrint(Orders,locationId,loggedINUserName){
		showMessage("loading", "Loading Log Information...", "");
  	  jQuery.ajax({
  		  type:'POST',
  		  url:'http://localhost:8080/ReportMerger/mergepdfs',
  		  data:
  		  {
  			  id:"printOrdersForLog",
  			Orders:Orders,
  			locationId:locationId,
  			Name:loggedINUserName
  		  },
  		  success:function(data)  /*ajax call for populating payment grid*/
  		  {
  			  jQuery(".ajaxLoading").hide();
  			  var rowId=1;
  			  var rowId2=1 ;
  			  var searchForLogOrdersData = jQuery.parseJSON(data);
  			  if(searchForLogOrdersData.Status==1){
  				searchForLogOrders=searchForLogOrdersData.Result;
  				 
  				
  			  }
  			  else{
  				  showMessage('red',searchForLogOrdersData.Description,'');
  			  }
  		  },
  		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
  	  });  	
	}

	function logTeamOrderPaymentInfo()
	{
		jQuery("#LTOPaymentDescGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Bank','Bonus Cheque','Cash','Cheque','COD','Credit Card','Forex','On Account','Change Amount'],
			colModel: [{ name: 'Bank', index: 'Bank', width: 100},
			           { name: 'BonusCheck', index: 'BonusCheck', width: 100},
			           { name: 'Cash', index: 'Cash', width: 70},
			           { name: 'Cheque', index: 'Cheque', width: 70},
			           { name: 'COD', index: 'COD', width: 70},
			           { name: 'CreditCard', index: 'CreditCard', width: 100},
			           { name: 'Forex', index: 'Forex', width: 70},
			           { name: 'OnAccount', index: 'OnAccount', width: 100},
			           { name: 'ChangeAmount', index: 'ChangeAmount', width: 100},
			           ],
			           pager: jQuery('#pjmap_LTOPaymentDescGrid'),
			           width: 700,
			           height:50,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,

			           loadComplete: function() {
			        	   //jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
			        	   //alert("laodign complete");
			           }	
			           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

		});

		/*
		 * function jqGridFormatter(cellvalue, options, row) {
		 * var index1 =
		 * jQuery("#jqgridtable_left").jqGrid('getCol','Name',false);
		 * alert(index1);
		 *  }
		 */

		jQuery("#LTOPaymentDescGrid").jqGrid('navGrid','#pjmap_LTOPaymentDescGrid',{edit:false,add:false,del:false});	
		jQuery("#pjmap_LTOPaymentDescGrid").hide();
	}
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Disable type,pickup center,distributorid, distributor name.
	 */
	function disableFieldsOnStartUp()
	{
		jQuery("#LTODistributorID").attr('disabled',true);
		jQuery("#LTODistributorId").attr('disabled',true);
		jQuery("#LTOPickUpCenterId").attr('disabled',false);
		jQuery("#LTODistributorName").attr('disabled',true);
		//backGroundColorForDisableField();
	}
	
	/*----------------------------------Select Log/Team Type-------------------------------------*/
	
	jQuery("#LTOtype").change(function(){
		var typeValue = jQuery(this).val();
	
		if(typeValue == 0)
		{
			disableFieldsOnStartUp();
			emptyPickUpCenterList();
			emptyDistributorIdField();
		}
		else
		{
			undoFieldsBackgroundColor(); // undo backgound color of fileds which on that form have been made of another color.
			controlOptionForType(typeValue);
		}
	});

	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function  disable pickup center or distributor no fields options.
	 */
	function controlOptionForType(typeValue)
	{
		if(typeValue == 1)
		{
			jQuery("#LTOPickUpCenterId").attr('disabled',false);
			jQuery("#LTODistributorId").attr('disabled',true);
			emptyDistributorIdField();
			//getPickUpCenter(); 
			
		}
		if(typeValue == 2)
		{
			
			jQuery("#LTODistributorId").attr('disabled',false);
			jQuery("#LTOPickUpCenterId").attr('disabled',true);
			emptyPickUpCenterList();
		}
	}
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Provide pick up centers available selecting type = log.
	 */
	function getPickUpCenter()
	{
		showMessage("loading", "Loading information...", "");
		
		jQuery("#LTOPickUpCenterId").append("<option value='0'>Select</option>");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"LTOPickUpCenters",
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var pickUpCentersData = jQuery.parseJSON(data);
			if(pickUpCentersData.Status==1){
				pickUpCenters=pickUpCentersData.Result;
				jQuery.each(pickUpCenters,function(key,value){
                    if(pickUpCenters[key]['LocationType']=='PC'){
						jQuery("#LTOPickUpCenterId").append("<option  id=\""+pickUpCenters[key]['LocationId']+"\""+" value=\""+pickUpCenters[key]['LocationId']+"\""+">"+pickUpCenters[key]['LocationCode']+"</option>");
					
                    }
				});
				jQuery("#LTOPickUpCenterId").find("option[value='-1']").remove();
			}
			else{
				showMessage('red',pickUpCentersData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Empty select list
	 */
	function emptyPickUpCenterList()
	{
		jQuery("#LTOPickUpCenterId").val(0);
	}
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Empty Distributor id field.
	 */
	function emptyDistributorIdField()
	{
		jQuery('#LTODistributorId').val('');
		
	}
	
	/*----------------------------------Select pick up center-------------------------------------*/
	
	jQuery("#LTOPickUpCenterId").change(function(){
		var selectedPickUpCenter = jQuery(this).val();
		
		//Selected option is select whose value is 0. Else particular pick up center option has been selected
		if(selectedPickUpCenter == 0)
		{
			
		}
		else
		{
			undoFieldsBackgroundColor();
			distributorFromPickUpCenter(selectedPickUpCenter);
		}
	});
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	function distributorFromPickUpCenter(selectedPickUpCenter)
	{
		showMessage("loading", "Searching for distributor information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"DistributorFromPickUpCenter",
				selectedPickUpCenter:selectedPickUpCenter,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				clearFields();
				var distributorInfoFromPickUpCenterData = jQuery.parseJSON(data); 
		if(distributorInfoFromPickUpCenterData.Status==1){
			distributorInfoFromPickUpCenter=distributorInfoFromPickUpCenterData.Result;
				jQuery.each(distributorInfoFromPickUpCenter,function(key,value){
					jQuery("#LTODistributorID").val(distributorInfoFromPickUpCenter[key]['DistributorId']);
					jQuery("#LTODistributorName").val(distributorInfoFromPickUpCenter[key]['DistributorName']);
					jQuery("#addressLine1").val(distributorInfoFromPickUpCenter[key]['Address1']);
					jQuery("#addressLine2").val(distributorInfoFromPickUpCenter[key]['Address2']);
					jQuery("#addressLine3").val(distributorInfoFromPickUpCenter[key]['Address3']);
					var address4 = distributorInfoFromPickUpCenter[key]['Address4'];
					if(address4 == null)
					{
						jQuery("#addressLine4").val('');
					}
					else
					{
						jQuery("#addressLine4").val(address4);
					}
					countryId = distributorInfoFromPickUpCenter[key]['CountryId'];
					//showSelectedCountry(countryId); //show particular country based on id .
					
					stateId = distributorInfoFromPickUpCenter[key]['StateId'];
					loadStates(countryId,stateId); //load states based on country id.
					//showSelectedState(stateId);
					//showSelectedState(stateId);
					
					
					cityId = distributorInfoFromPickUpCenter[key]['CityId'];
					loadCities(stateId,cityId);
					//showSelectedCity(cityId);
					jQuery("#LTOPinCode").val(distributorInfoFromPickUpCenter[key]['Pincode']);
					jQuery("#LTOPhone1").val(distributorInfoFromPickUpCenter[key]['Phone1']);
					jQuery("#LTOPhone2").val(distributorInfoFromPickUpCenter[key]['Phone2']);
					jQuery("#LTOMobile1").val(distributorInfoFromPickUpCenter[key]['Mobile1']);
					jQuery("#LTOMobile2").val(distributorInfoFromPickUpCenter[key]['Mobile2']);
					jQuery("#LTOEmail1").val(distributorInfoFromPickUpCenter[key]['EmailId1']);
					jQuery("#LTOEmail2").val(distributorInfoFromPickUpCenter[key]['EmailId2']);
					jQuery("#LTOFax1").val(distributorInfoFromPickUpCenter[key]['Fax1']);
					jQuery("#LTOFax2").val(distributorInfoFromPickUpCenter[key]['Fax2']);
					jQuery("#LTOWebsite").val(distributorInfoFromPickUpCenter[key]['WebSite']);
					//alert(stateId);
					//jQuery("#State").val(stateId);
					//jQuery("#City").val(cityId);
					indicate = 1;
					//checkLoadedStates(indicate);
					
					
				});
			}
		else{
			showMessage('red',distributorInfoFromPickUpCenterData.Description,'');
		}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
		
		
	}
	
	/*----------------------------------Enter distributor id and press enter-------------------------------------*/
	
	jQuery("#LTODistributorId").keydown(function(e){
		
	    if(e.which==9 || e.which == 13){
	    	var distributorId = jQuery("#LTODistributorId").val();
	    	
	    	distributorFromId(distributorId);
	    	
	        
	    }
	});
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	
	/**
	 * Function Provide information about user based on distributor id.
	 */
	function distributorFromId(distributorId)
	{
		showMessage("loading", "Searching for distributor Information...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"DistributorFromId",
				enteredDistributorId:distributorId,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var distributorInfoFromDistributorIdData = jQuery.parseJSON(data); 
		if(distributorInfoFromDistributorIdData.Status==1){
			distributorInfoFromDistributorId=distributorInfoFromDistributorIdData.Result;
			if(distributorInfoFromDistributorId.length==0){
				
				jQuery("#LTODistributorId").css("background","#FF9999");
				return;
			}
			clearFields();
			jQuery("#LTODistributorId").css("background","white");
				jQuery.each(distributorInfoFromDistributorId,function(key,value){
					jQuery("#LTODistributorId").val(distributorInfoFromDistributorId[key]['DistributorId']);
					jQuery("#LTODistributorID").val(distributorInfoFromDistributorId[key]['DistributorId']);
					jQuery("#LTODistributorName").val(distributorInfoFromDistributorId[key]['DistributorFirstName']);
					jQuery("#addressLine1").val(distributorInfoFromDistributorId[key]['DistributorAddress1']);
					jQuery("#addressLine2").val(distributorInfoFromDistributorId[key]['DistributorAddress2']);
					jQuery("#addressLine3").val(distributorInfoFromDistributorId[key]['DistributorAddress3']);
				     var countryId=distributorInfoFromDistributorId[key]['DistributorCountryCode'];
				     var stateId=distributorInfoFromDistributorId[key]['DistributorStateCode'];
					loadStates(countryId,stateId); 
					 cityId= distributorInfoFromDistributorId[key]['DistributorCityCode']
					loadCities(stateId,cityId)
					var address4 = distributorInfoFromDistributorId[key]['DistributorAddress4'];
					if(address4 == null)
					{
						jQuery("#addressLine4").val('');
					}
					else
					{
						jQuery("#addressLine4").val(address4);
					}
					jQuery("#LTOPinCode").val(distributorInfoFromDistributorId[key]['DistributorPinCode']);
					jQuery("#LTOPhone1").val(distributorInfoFromDistributorId[key]['DistributorTeleNumber']);
					jQuery("#LTOPhone2").val(distributorInfoFromDistributorId[key]['Phone2']);
					jQuery("#LTOMobile1").val(distributorInfoFromDistributorId[key]['DistributorMobNumber']);
					jQuery("#LTOMobile2").val(distributorInfoFromDistributorId[key]['Mobile2']);
					jQuery("#LTOEmail1").val(distributorInfoFromDistributorId[key]['DistributorEMailID']);
					jQuery("#LTOEmail2").val(distributorInfoFromDistributorId[key]['EmailId2']);
					jQuery("#LTOFax1").val(distributorInfoFromDistributorId[key]['DistributorFaxNumber']);
					jQuery("#LTOFax2").val(distributorInfoFromDistributorId[key]['Fax2']);
					jQuery("#LTOWebsite").val(distributorInfoFromDistributorId[key]['WebSite']);
				});
			}
		else{
			showMessage('red',distributorInfoFromDistributorIdData.Description,'');
		}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
		
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Funciton to load countries.
	 * 
	 */
	function loadCountries()
	{
		jQuery("#Country").empty();
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"loadCountries",
			},
			success:function(data)
			{
				countriesData = jQuery.parseJSON(data);
			if(countriesData.Status==1){
				countries=countriesData.Result;
				jQuery.each(countries,function(key,value)
				{	
					
					jQuery("#Country").append("<option id=\""+countries[key]['CountryId']+"\""+" value=\""+countries[key]['CountryId']+"\""+">"+countries[key]['CountryName']+"</option>");
				});
			}
			else{
				showMessage('red',countriesData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}

	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Funciton to load states.
	 * 
	 */
	jQuery("#Country").change(function(){
		var countryId = jQuery(this).val();
		jQuery(this).css("background","white");
		//Selected option is select whose value is 0. Else particular pick up center option has been selected
		loadStates(countryId,'');
	});
	jQuery("#State").change(function(){
		jQuery(this).css("background","white");
		var stateId = jQuery(this).val();
		
		//Selected option is select whose value is 0. Else particular pick up center option has been selected
		loadCities(stateId,'');
	});
	jQuery("#City").change(function(){
		jQuery(this).css("background","white");
		
	});
	
	
	function loadStates(countryId,stateId)
	{
		jQuery("#State").empty();
		jQuery("#State").append("<option value='0'>Select</option>");
		showMessage("loading", "Searching for States...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"loadStates",
				countryId:countryId,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				statesData = jQuery.parseJSON(data);
		  if(statesData.Status==1){
			  states=statesData.Result;
				jQuery.each(states,function(key,value)
				{	
					
					jQuery("#State").append("<option id=\""+states[key]['StateId']+"\""+" value=\""+states[key]['StateId']+"\""+">"+states[key]['StateName']+"</option>");
				});
				
				jQuery("#State").val(stateId);
			}
		  else{
			  showMessage('red',statesData.Description,'');
		  }
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Funciton to load cities.
	 * 
	 */
	function loadCities(stateId,cityId)
	{
		jQuery("#City").empty();
		jQuery("#City").append("<option value='0'>Select</option>");
		//alert(stateId);
		showMessage("loading", "Searching for cities...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"loadCities",
				stateId:stateId,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				//alert(data);
				//alert(data);
				citiesData = jQuery.parseJSON(data);
			if(citiesData.Status==1){
				
				cities=citiesData.Result;
				jQuery.each(cities,function(key,value)
				{	
					
					jQuery("#City").append("<option id=\""+cities[key]['CityId']+"\""+" value=\""+cities[key]['CityId']+"\""+">"+cities[key]['CityName']+"</option>");
				});
				
				jQuery("#City").val(cityId);
			}
			else{
				showMessage('red',citiesData.Desription,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*function showSelectedCountry(countryId)
	{
		jQuery.each(countries,function(key,value){
			if(countryId == countries[key]['CountryId'])
			{
				var CountryName = countries[key]['CountryName'];
				jQuery("#Country").val(countryId);
			}
		});
	}
	
	---------------------------------------------------------------------------------------------------------------------------
	function showSelectedState(stateId)
	{
		jQuery("#State").val(stateId);

	}
	
	function showSelectedCity(cityId)
	{
		jQuery("#City").val(cityId);
	}*/
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	
	/*function showCountyStateCity()
	{
		//alert(states);
		jQuery.each(countries,function(key,value){
			if(countryId == countries[key]['CountryId'])
			{
				alert(countryId);
				var CountryName = countries[key]['CountryName'];
				alert(CountryName);
				jQuery("#Country option").filter(function() {
				    //may want to use $.trim in here
				    return jQuery("#Country").text() == CountryName; 
				}).attr('selected', true);
			}
		});
		//alert(states);
		jQuery.each(states,function(key,value){
			if(stateId == states[key]['StateId'])
			{
				alert(stateId);
				var StateName = states[key]['StateName'];
				alert(StateName);
				jQuery("#State option").filter(function() {
				    //may want to use $.trim in here
				    return jQuery("#State").text() == StateName; 
				}).attr('selected', true);
			}
		});
		//jQuery("#State").val(stateId);
		//jQuery("#City").val(cityId);
		
	}*/
	/*function checkLoadedStates(indicate)
	{
	
		
		if(indicate == 1)
		{
			//alert(stateId);
			jQuery("#State").val(stateId);
		}
	}*/
	
	
	/*----------------------------------Clear Fields-------------------------------------*/
	jQuery("#clear").click(function(){
		 jQuery("#btnCourierDetails").attr('disabled',true);
		clearAllFields();
		searchLogTeamInfo();
		logTeamOrderPaymentInfo();
		disableFieldsOnStartUp();
		loadCountries();
		disableButtons();
		jQuery("#btnPOS02Save").attr("disabled", false);
		jQuery("#zeroLog").attr('checked',false); 
		jQuery("#isChangeAddress").val(-1);
		actionButtonsStauts();
	});
	
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function to clear data from fields.
	 */
	function clearFields()
	{
		jQuery(".commonTextField").val('');
		jQuery(".commonTextArea").val('');
		jQuery("#Country").val(-1);
		jQuery("#State").val(-1);
		jQuery("#City").val(-1);
		
	}
	function clearAllFields()
	{
		jQuery(".commonTextField").val('');
		jQuery(".commonTextArea").val('');
		jQuery("#Country").val('');
		jQuery("#State").val('');
		jQuery("#City").val('');
		jQuery('.CommonSelect').val(0);
		jQuery("#LogTeamOrderGrid").jqGrid("clearGridData");
		jQuery("#LTOPaymentDescGrid").jqGrid("clearGridData");
		jQuery("#docketNo").val('');
		jQuery("#courierCompanyName").val('');
		jQuery("#noOfBoxes").val('');
		jQuery("#weightInGram").val('');
		
		jQuery("#docketNo2").val('');
		jQuery("#courierCompanyName2").val('');
		
		jQuery("#weightInGram2").val('');
		
		
	}
	
	
	/*-----------------------------Save log team order data------------------------------------*/

	jQuery("#btnPOS02Save").unbind("click").click(function(){
		jQuery("#addressLine1").css("background","white");
		jQuery("#addressLine2").css("background","white");
		jQuery("#LTOtype").css("background","white");
		jQuery("#Country").css("background","white");
		jQuery("#State").css("background","white");
		jQuery("#City").css("background","white");
		 jQuery("#btnCourierDetails").attr('disabled',true);
		var logTeamOrderFormData = jQuery("#LogTeamOrderForm").serialize();
		var gotValidatationStatus = ValidateLogTeamOrderForm();

		if(gotValidatationStatus == 1)
		{
			//alert("Your log number");
			
			var r=confirm("Are you sure you want to save data ?");
			if (r==true)
			  {
				showMessage("loading", "Saving log/team order...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LogTeamOrderCallback',
					data:
					{
						id:"getLogNumber",
						functionName:"Save",
						moduleCode:"POS02",
						logTeamOrderFormData:logTeamOrderFormData,
						isZeroLog:jQuery("#zeroLog").is(':checked')? 1 : 0,
						isChangeAddress:jQuery("#isChangeAddress").val(),
						source:"web" 
						
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						if(data == '')
						{
							showMessage('',"Unable to Save TeamOrder/Log. Error " + data,'');
						}
						
						else{
							
							var logNumberParsedJsonData = jQuery.parseJSON(data);
				if(logNumberParsedJsonData.Status==1){
					logNumberParsedJson=logNumberParsedJsonData.Result;
							//alert("CO Log is:"+data);
							jQuery.each(logNumberParsedJson,function(key,value){
								
								
								var location = logNumberParsedJson[key]['Name'];
								var logNo = logNumberParsedJson[key]['LogNo'];
								
								if(typeof location ==='undefined'){
								showMessage('green',"Log Saved.  "+logNo,'');
								}
								else if((typeof location==='undefined') && (typeof logNo!='undefined')){
									showMessage('',"Log Not Created. Error " + data,'');
											
								}
								else{		
									showMessage('green',"Log Saved. "+location+" "+logNo,'','');
								}
								jQuery("#LTONumber").val(logNo);
							});
							
							//clearAllFields();
							//saveLog(data);
						}
				else{
					showMessage('red',logNumberParsedJsonData.Description,'');
				}
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				
				});
			
			  }
			
		}
		else
		{
			showMessage('',"Please Fill all Required fields",'');
		}
		
	});
	/*function vallidationOnSearch(){
	   if(jQuery("#LTOtype").val()==1)
		   {
		      if(jQuery("#LTOPickUpCenterId").val()==0){
		    	  showMessage('red','Select pickup center','');
		      }
		   }
	   if(jQuery("#LTOtype").val()==2)
	   {
	      if(jQuery("#LTODistributorId").val()==''){
	    	  showMessage('red','','');
	      }
	   }
	   Return 1 ;
	}
	*/
	jQuery("#btnPOS02Search").unbind("click").click(function(){
		
		 jQuery("#btnCourierDetails").attr('disabled',true);
		jQuery("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd ");
		var logTeamOrderFormData = jQuery("#LogTeamOrderForm").serialize();
		jQuery("#fromDate").datepicker("option", "dateFormat", "d-M-yy");
		jQuery("#btnPOS02Save").attr("disabled", true);
	    	actionButtonsStauts();
			//alert("Your log number");
		showMessage("loading", "Searching logs...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LogTeamOrderCallback',
				data:
				{
					id:"searchLogTeamOrder",
					logTeamOrderFormData:logTeamOrderFormData,
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					/*jQuery("#loadingImage").hide();*/
					var logNumberParsedJsonData = jQuery.parseJSON(data);
				if(logNumberParsedJsonData.Status==1){
					logNumberParsedJson=logNumberParsedJsonData.Result;
					if(logNumberParsedJson.length==0)
					{
						jQuery("#LogTeamOrderGrid").jqGrid("clearGridData");
						showMessage('yellow',"No Record Found",'');
					}
					else
					{	
						var rowid=0;
						jQuery("#LogTeamOrderGrid").jqGrid("clearGridData");
						jQuery.each(logNumberParsedJson,function(key,value){
						
							var logType ='';
							if( logNumberParsedJson[key]['LogType']==1)
							{
								logType ='Log';
							}
							else
							{
								var logType ='TeamOrder';
							}
							
							var pcId = logNumberParsedJson[key]['PCId'];
							var distributorName = logNumberParsedJson[key]['DistributorName'];
							var status='';
							if( logNumberParsedJson[key]['Status']==1)
							{
								 status ='Open';
							}
							else
							{
								 status ='Closed';
							}
							var logOrderTotal =  parseFloat(logNumberParsedJson[key]['LogOrderTotal'],10).toFixed(2);
							var logInvoiceTotal = parseFloat(logNumberParsedJson[key]['LogInvoiceTotal'],10).toFixed(2);
							var createdDate = logNumberParsedJson[key]['CreatedDate'];		
							var newData = [{"LogNo":logNumberParsedJson[key]['LogNo'],"DistributorId":logNumberParsedJson[key]['DistributorId'],"LogType":logType,"DistributorName":distributorName,
								"PickUpCenter":logNumberParsedJson[key]['PCName'],'Status':status,'LogOrderTotal':logOrderTotal,"LogInvoiceTotal":logInvoiceTotal,"LogDate":createdDate,
								"Address1":logNumberParsedJson[key]['Address1'],"Address2":logNumberParsedJson[key]['Address2'],"Address3":logNumberParsedJson[key]['Address3'],"Address4":logNumberParsedJson[key]['Address4'],
								"LogInvoiceWeight":parseFloat(logNumberParsedJson[key]['LogInvoiceWeight'],10).toFixed(2),
								"CountryCode":logNumberParsedJson[key]['CountryCode'],"StateCode":logNumberParsedJson[key]['StateCode'],
								"CityCode":logNumberParsedJson[key]['CityCode'],"PinCode":logNumberParsedJson[key]['PinCode'],"Phone1":logNumberParsedJson[key]['Phone1'],"Phone2":logNumberParsedJson[key]['Phone2'],
								"Mobile1":logNumberParsedJson[key]['Mobile1'],"Mobile2":logNumberParsedJson[key]['Mobile2'],"Email1":logNumberParsedJson[key]['EmailId1'],"Email2":logNumberParsedJson[key]['EmailId2']
								,"Fax1":logNumberParsedJson[key]['Fax1'],"Fax2":logNumberParsedJson[key]['Fax2'],"Website":logNumberParsedJson[key]['Website'],"CityName":logNumberParsedJson[key]['CityName'],
								"StateName":logNumberParsedJson[key]['StateName'],"CountryName":logNumberParsedJson[key]['CountryName'],"CreatedBy":logNumberParsedJson[key]['CreatedBy']
							,"ModifiedBy":logNumberParsedJson[key]['ModifiedBy'],"ModifiedDate":logNumberParsedJson[key]['ModifiedDate'],"Website":logNumberParsedJson[key]['Website'],"LogCashTotal":logNumberParsedJson[key]['LogCashTotal'],"LogCreditCardTotal":logNumberParsedJson[key]['LogCreditCardTotal'],
							"LogChequeTotal":logNumberParsedJson[key]['LogChequeTotal'],"LogBonusChequeTotal":logNumberParsedJson[key]['LogBonusChequeTotal'],"LogbankTotal":logNumberParsedJson[key]['LogBankTotal'],"LogValue":logNumberParsedJson[key]['LogValue']
							,"isZeroLog":logNumberParsedJson[key]['isZeroLog'],"isChangeAddress":logNumberParsedJson[key]['isChangeAddress']
							,"UnpaidAmount":logNumberParsedJson[key]['UnpaidAmount']}];
					
							for (var i=0;i<newData.length;i++) {
							jQuery("#LogTeamOrderGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
							rowid=rowid+1;
							}
						});
							
			
						
						/*saveLog(data);*/
					}
					}
					else{
						showMessage('red',logNumberParsedJsonData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		
		
	});
	
	/*for exit*/
	jQuery("#exit").click(function(){
		
		//window.close();
	});
	
	
	
	/*---------------------------------------------------------------------------------------------------------------------*/
	/**
	 * function to save log order.
	 */
	function saveLog(logSeqNo)
	{
		var logType = jQuery("#LTOtype").val();
		if(jQuery("#LTODistributorId").is(':disabled')) // if disabled then distributor id will be zero.
		{
			
		}
		else
		{
			
		}
	}
	
	/*---------------------------------------------------------------------------------------------------------------------*/

	/**
	 * function to validate log team order form.
	 */
	
	function ValidateLogTeamOrderForm()
	{
		var validationStatus = 1;
		/*jQuery(".commonTextField").each(function(){
			var id = this.id;*/
		if(jQuery("#addressLine1").val() == '')
		{
			jQuery("#addressLine1").css("background","#FF9999");
			validationStatus = 0;
			
		}
		
		if(jQuery("#addressLine2").val() == '')
		{
			jQuery("#addressLine2").css("background","#FF9999");
			validationStatus = 0;
		}
		if(jQuery("#isChangeAddress").val() == -1)
		{
			jQuery("#isChangeAddress").css("background","#FF9999");
			validationStatus = 0;
		}
		else{
			jQuery("#isChangeAddress").css("background","white");
		}
		if(jQuery("#LTOtype").val() == -1)
		{
			jQuery("#LTOtype").css("background","#FF9999");
			validationStatus = 0;
		}
		if(jQuery("#Country").val() == 0)
		{
			jQuery("#Country").css("background","#FF9999");
			validationStatus = 0;
		}
		if(jQuery("#State").val() == 0)
		{
			jQuery("#State").css("background","#FF9999");
			validationStatus = 0;
		}
		if(jQuery("#City").val() == 0)
		{
			jQuery("#City").css("background","#FF9999");
			validationStatus = 0;
		}
		
		return validationStatus;
	}
	
	/*---------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function to undo all backgound color of fields.
	 */
	function undoFieldsBackgroundColor()
	{
		jQuery("#addressLine1").css("background","white");
		jQuery("#addressLine2").css("background","white");
		jQuery("#LTOtype").css("background","white");
	}
	
	jQuery("#btnPOS02Invoice2").unbind("click").click(function(){
		 jQuery("#btnCourierDetails").attr('disabled',true);
		var arr=getArrayFromMultipleSelect();
		printMultipleInvoice('',arr,1);
	});
	jQuery("#logInvoiceExit").unbind("click").click(function(){
		 jQuery("#btnCourierDetails").attr('disabled',true);
		jQuery("#logTeamOrderInvoicePopUp").dialog("close");
		
	});
	jQuery("#btnPOS02Invoice").unbind("click").click(function(){
		 jQuery("#btnCourierDetails").attr('disabled',true);
		var rowId=jQuery('#LogTeamOrderGrid').jqGrid('getGridParam', 'selrow');
		var logNo = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogValue');
		var Status = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Status');
		var amount = parseFloat(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogOrderTotal'),10).toFixed(0);
		if(amount==0 && Status=='Open'){
			var returnValue=confirm('No orders are added to this log. Do you want to close it ?');
			if(returnValue){
				printMultipleInvoice(logNo, '[]',2);
				return ;
			}
			return ;
		}
		if(Status=='Closed'){
		showMessage('','Log Already Closed. Choose Another Log.','');	
		return ;
		}
		if(vallidateInventoryForLog(logNo)==1){
			printMultipleInvoice(logNo, '[]',2);
		 }
		
	});
	
	
	jQuery("#receiveLogpayment").unbind("click").click(function(){
		
		if(jQuery('#LogTeamOrderGrid').getRowData().length == 0){
			
			showMessage("", "Select any log to receive payment", "");
			return;
		}
		jQuery("#btnCourierDetails").attr('disabled',true);
		var rowId=jQuery('#LogTeamOrderGrid').jqGrid('getGridParam', 'selrow');
		var logNo = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogValue');
		var Status = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'Status');
		var amount = parseFloat(jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'LogOrderTotal'),10).toFixed(0);
		if(amount==0 && Status=='Open'){
			var returnValue=confirm('No orders are added to this log. Do you want to close it ?');
			if(returnValue){
				printMultipleInvoice(logNo, '[]',2);
				return ;
			}
			return ;
		}
		if(Status=='Closed'){
			showMessage('','Log Already Closed. Choose Another Log.','');	
			return ;
		}
		if(vallidateInventoryForLog(logNo)==1){
			var unpaidAmount = jQuery("#LogTeamOrderGrid").jqGrid('getCell', rowId, 'UnpaidAmount');
			
			if(unpaidAmount == 0){
				showMessage("", "Amount for this log already received", "");
				return;
			}
			
			
			showReceivePaymentGrid();
			jQuery( "#receivePayPopUp" ).dialog( "open" );
			
			showReceivePaymentData('L', logNo);
			
			jQuery("#btnReceivePayOk").unbind('click').click(function(){
				receivePaymentForSingleOrder(logNo,unpaidAmount,rowId);
			});
			jQuery("#btnReceivePayClose").unbind('click').click(function(){
				jQuery( "#receivePayPopUp" ).dialog( "close" );
			});
			
			
			
			/*var confirmFlag = confirm("Total unpaid amount for this log is "+formatFloatNumbers(parseFloat(unpaidAmount)) + ". Do you want to receive ? ");
			if(confirmFlag == true){
				payMultipleOrders(logNo,unpaidAmount,rowId);
			}*/
			
		 }
		
	});
	
	function receivePaymentForSingleOrder(logNo,unpaidAmount,rowId){
		
		showMessage("loading", "Checking courier charges to be applied or not...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"payMultipleLogs",
				logsToBePaid:logNo

			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var oResult = jQuery.parseJSON(data);
				var orderstoShowForCourierChargeToApply =0;
				var toShowString = "Courier Charges will be applied on orders ";
				if(oResult.Status == 1){
					jQuery("#receivePayment").attr('disabled',false);

					var logResult = oResult.Result;
					var payAbleOrderList = [];
					
					//var orderToBePaidList =  ordersToBePaid.split('_');
					var messageToShowForCourierChargesToReceive = 0
					//var myAccountGridData = jQuery("#myAccountSearchGrid").jqGrid('getRowData');
						
					if(logResult[0]['CourierChargesAppliedOrNot'] == 0 && logResult[0]['OrderMode'] == 2 && 
							parseFloat(logResult[0]['LogOrderAmount']) < parseFloat(logResult[0]['MaxAmountForCourierToApply'])){
						/*jQuery.each(myAccountGridData,function(key1,value1){
							if(logResult[0]['LogNo'] == myAccountGridData[key1]['LogNo']){
								payAbleOrderList.push();
								return false;
							}
						});*/
						
						messageToShowForCourierChargesToReceive = 1;
						
					}
					
					if(messageToShowForCourierChargesToReceive == 1){
						var totalAmtToReceive = parseInt(unpaidAmount) + parseInt(logResult[0]['LogCourierCharges']);
						var confirmMsg = confirm("Courier charges will be applied on " + logResult[0]['FavourableOrderToBePaid'] + ".\n Total amount " +
								"to receive are as "+ unpaidAmount +" + "+ logResult[0]['LogCourierCharges'] +" = "+ totalAmtToReceive + " INR. Do you want to proceed ?");
						if(confirmMsg == true){

						
							if(logResult[0]['FavourableOrderToBePaid'] == '' || logResult[0]['FavourableOrderToBePaid'] == null){
								showMessage("red", "Problem choosing order on which courier charges to apply. contact branch.", "");
								return;
							}
							
						
							payMultipleOrders(logNo,unpaidAmount,rowId,logResult[0]['FavourableOrderToBePaid'],1);
							
							/*showReceivePaymentGrid();
							jQuery( "#receivePayPopUp" ).dialog( "open" );
							
							showReceivePaymentData('L', logNo);
							
							jQuery("#btnReceivePayOk").unbind('click').click(function(){
								payMultipleOrders(logNo,unpaidAmount,rowId,logResult[0]['FavourableOrderToBePaid']);
								//payMultipleOrders(logNo,unpaidAmount,rowId,logResult[0]['FavourableOrderToBePaid']);
							});
							jQuery("#btnReceivePayClose").unbind('click').click(function(){
								jQuery( "#receivePayPopUp" ).dialog( "close" );
							});*/
						}
						else{
							//no movement.
						}


					}
					else{

						payMultipleOrders(logNo,unpaidAmount,rowId,logResult[0]['OrderOnWhichCourierChargesApplied'],0);
						
						/*showReceivePaymentGrid();
						jQuery( "#receivePayPopUp" ).dialog( "open" );
						
						showReceivePaymentData('L', logNo);
						
						jQuery("#btnReceivePayOk").unbind('click').click(function(){
							payMultipleOrders(logNo,unpaidAmount,rowId,logResult[0]['FavourableOrderToBePaid']);
						});
						jQuery("#btnReceivePayClose").unbind('click').click(function(){
							jQuery( "#receivePayPopUp" ).dialog( "close" );
						});*/
					}
					
					
				}
				else{
					jQuery("#btnOrderPay").attr('disabled',false);
					jQuery("#receivePayment").attr('disabled',false);
					showMessage("red", oResult.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				jQuery("#receivePayment").attr('disabled',false);
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
			
		});
		
		
	}
	
	function vallidateInventoryForLog(logNo)
	{
		/*var StatusForVallidaton=0 ;
		showMessage("loading", "Loading information", "");
   	
   		jQuery.ajax({
   			type:'POST',
   			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
   			data:
   			{
   				id:"vallidateLogNo",
   				LogNo:logNo,

   			},
   		
	success:function(data)
   			{
   				//alert(data);

   				jQuery(".ajaxLoading").hide();
   				var shortInventoryData = jQuery.parseJSON(data);
   				if(shortInventoryData.Status==1){

   					shortInventory=shortInventoryData.Result;
   					jQuery("#ShortInventoryGrid").jqGrid("clearGridData");
			   					if(shortInventory.length>0){
						   					jQuery.each(shortInventory,function(key,value){
						   						var OrderNo = shortInventory[key]['CustomerOrderNo'];	
						   						var ItemName = shortInventory[key]['ItemName'];    
						   						var AvailableQty = shortInventory[key]['AvailableQty'];    
						   						var Qty = shortInventory[key]['Qty'];   
						   						var rowid=0;
						   						var newData = [{"OrderNo":OrderNo,"ItemName":ItemName,"AvailableQty":AvailableQty
						   							,"RequiredQty":Qty
						   						}];
						
						   						for (var i=0;i<newData.length;i++) {
						   							jQuery("#ShortInventoryGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
						   							rowid=rowid+1;
						   						}
						   					});
						
						   					jQuery( "#shortInventorydiv" ).dialog( "open" );
			   					 }
			   					else{
			   						StatusForVallidaton=1 ;
			   					  }
			   					
   					}

   				else{
   					showMessage('red',shortInventoryData.Description,'');
   				}
   			}
   			});
   		*/
		
	
   		return 1;

		}	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Short Inventory Message grid 
	 */
	jQuery("#ShortInventoryGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Order No.','Item Name','Available Qty.','Required Qty.'],
		colModel: [{ name: 'OrderNo', index: 'OrderNo', width:100 },
		           { name: 'ItemName', index: 'ItemName', width: 150},
		           { name: 'AvailableQty', index: 'AvailableQty', width: 150},
		           { name: 'RequiredQty', index: 'RequiredQty', width: 100}],
		           pager: jQuery('#PJmap_ShortInventoryGrid'),
		           width:400,
		           height:300,
		           rowNum: 20,
		           rowList: [500],
		           sortname: 'Label',
		           sortorder: "asc",
		           hoverrows: true,


		           afterInsertRow : function(ids)
		           {
	        
		        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//		        	 
		           },

		           loadComplete: function() {
//		        	   
		           }	
		         
	});

	jQuery("#ShortInventoryGrid").jqGrid('navGrid','#PJmap_ShortInventoryGrid',{edit:false,add:false,del:false});	
	jQuery("#PJmap_ShortInventoryGrid").hide();
	
	
	jQuery("#LOGPendingOrders").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Invoice','Order No','Distributor Id','Order Date','Order Ref. No','Order Amount'],
		colModel: [{ name: 'Invoice', index: 'Invoice', width: 50,edittype:'checkbox',formatter: 'checkbox',editoptions: { value:"True:False"},classes:'AddToLogSelection',editable:true,formatoptions: {disabled : false}},
		           { name: 'OrderNo', index: 'OrderNo', width: 150},
		           { name: 'DistributorId', index: 'DistributorId', width: 150},
				   { name: 'OrderRefNo', index: 'OrderRefNo', width: 100},
		           { name: 'OrderDate', index: 'OrderDate', width: 100},
		           { name: 'OrderAmount', index: 'OrderAmount', width: 100}
		           ],
		           pager: jQuery('#PJmap_LOGPendingOrders'),
		           width:600,
		           height:200,
		           rowNum: 20,
		           rowList: [500],
		           sortname: 'Label',
		           sortorder: "asc",
		           hoverrows: true,


		           afterInsertRow : function(ids)
		           {
	        	
		        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});        	 
		           },

		           loadComplete: function() {        	 
		           }	
		        

	});

	jQuery("#LOGPendingOrders").jqGrid('navGrid','#PJmap_LOGPendingOrders',{edit:false,add:false,del:false});	
	jQuery("#PJmap_LOGPendingOrders").hide();
	
	
	jQuery("#LOGInvoicedOrders").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Invoice','Order No','Distributor Id','Order Date','Order Ref. No','Order Amount'],
		colModel: [{ name: 'Invoice', index: 'Invoice', width: 150},
		           { name: 'OrderNo', index: 'OrderNo', width: 150},
		           { name: 'DistributorId', index: 'DistributorId', width: 150},
		           { name: 'OrderDate', index: 'OrderDate', width: 100},
				   { name: 'OrderRefNo', index: 'OrderRefNo', width: 100},
		           { name: 'OrderAmount', index: 'OrderAmount', width: 100}
		           ],
		           pager: jQuery('#PJmap_LOGInvoicedOrders'),
		           width:600,
		           height:200,
		           rowNum: 20,
		           rowList: [500],
		           sortname: 'Label',
		           sortorder: "asc",
		           hoverrows: true
		        

	});
	
	jQuery("#LOGInvoicedOrders").jqGrid('navGrid','#PJmap_LOGInvoicedOrders',{edit:false,add:false,del:false});	
	jQuery("#PJmap_LOGInvoicedOrders").hide();
	

	jQuery("#LOGInvoiceStatus").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Order No','Status','Description','Preview'],
		colModel: [{ name: 'OrderNo', index: 'OrderNo', width: 150},
		           { name: 'Status', index: 'Status', width: 10,hidden:true},
		           { name: 'Description', index: 'Description', width: 150},
		           { name: 'Preview', index: 'Preview', width: 50},
		  
		           ],
		           pager: jQuery('#PJmap_LOGInvoiceStatus'),
		           width:600,
		           height:200,
		           rowNum: 20,
		           rowList: [500],
		           sortname: 'Label',
		           sortorder: "asc",
		           hoverrows: true

	});

	jQuery("#LOGInvoiceStatus").jqGrid('navGrid','#PJmap_LOGInvoiceStatus',{edit:false,add:false,del:false});	
	jQuery("#PJmap_LOGInvoiceStatus").hide();
	
	
	function printMultipleInvoice(logNo,arrOfOrder,type){
		
		showMessage("loading", "Loading Log Information...", "");
	  	  jQuery.ajax({
	  		  type:'POST',
	  		  url:Drupal.settings.basePath + 'LogTeamOrderCallback',
	  		  data:
	  		  {
	  			  id:"multipleInvoices",
	  			  LogNo:logNo,
	  			  arrOfOrder:arrOfOrder,
	  			  type:type
	  		  },
	  		  
	  		  success:function(data)  /*ajax call for populating payment grid*/
	  		  {
	  			
	  			  jQuery(".ajaxLoading").hide();
	  			  var statusForOpenGrid=0;
	  			isprintButton='';
	  			  var rowId=1 ;
	  			  var resultData = jQuery.parseJSON(data);
	  			  if(resultData.Status==1){
	  				result=resultData.Result;
	  			  jQuery("#LOGInvoiceStatus").jqGrid("clearGridData");
	  				  if(result.length>0){
	  					
	 		  				  jQuery.each(result,function(key,value)
				  		
	 		  						  { 
					 		  						isprintButton='<BUTTON class="logPrintButtons" id="btn'+rowId+'">Print</BUTTON>';		
					 		  					
					 		  					var newData = [{"OrderNo":result[key]['orderNo'],"Status":result[key]['Status'],
					 		  						"Description":result[key]['InvDescription'],"Preview":isprintButton
					 	   					     ,
					 	   						}];
					 	   						for (var i=0;i<newData.length;i++) {
					 	   							jQuery("#LOGInvoiceStatus").jqGrid('addRowData',rowId, newData[newData.length-i-1], "first");
					 	   						rowId=rowId+1;
					 	   						}
					 	   					
				  			            		
				  						  });
	 		  			
	 		  				 jQuery( "#InvoiceStatusGrid" ).dialog( "open" );  
	 		  				hidePrintButton();
		 		  			 function hidePrintButton(){
		 		  		    	var allRowIds=jQuery('#LOGInvoiceStatus').jqGrid('getDataIDs');
		 		  		    	for(var index=0;index<allRowIds.length;index++){
		 		  		    	var Status=	jQuery('#LOGInvoiceStatus').jqGrid('getCell',allRowIds[index], 'Status');
		 		  				    	if(parseFloat(Status)!=1){
		 		  				    		jQuery("#btn"+allRowIds[index]).hide();
		 		  				    		
		 		  				    	}
		 		  				    	
		 		  				    	
		 		  		    	}
		 		  		    	
		 		  		    }
	 		  			hidePrintButton();
	 		  				jQuery(".logPrintButtons").click(function(){
	 		  					
	 		  					var rowId=this.id;
	 		  					rowId=rowId.replace("btn",'');
	 		  					var orderNo = jQuery("#LOGInvoiceStatus").jqGrid('getCell', rowId, 'OrderNo');
	 		  					showInvoiceReport(orderNo,1);
	 		  					
	 		  				});
	 		  				if(type==1){
	 		  					searchForOrders(currentLogNo);
	 		  				}
	  				   }
	  				 //showMessage('green','Log Orders Invoiced','');
	  			  }
	  			  else{
	  				  showMessage('red',resultData.Description,'');
	  			  }
	  		  },
	  		error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
	  	  });  	
	}
	
	function payMultipleOrders(LogNo,unpaidAmount,rowId,favourableOrderNoForCourier,withCourierCharges){
		
		showMessage("", "Wait, receiving payment...", "");
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"receivePaymentForLog",
				LogNo:LogNo,
				unpaidAmount:unpaidAmount,
				favourableOrderNoForCourier:favourableOrderNoForCourier,
				withCourierCharges:withCourierCharges
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var parsedResult = jQuery.parseJSON(data);
				if(parsedResult.Status == 1){
					var oResult = parsedResult.Result;
					if(oResult[0]['OutParam'] == 'Updated'){
						showMessage("","Amount of rupees "+formatFloatNumbers(parseFloat(oResult[0]['LogCurrentAmount']))+ " received successfully for log "+LogNo, "");
						
						jQuery( "#receivePayPopUp" ).dialog( "close" );
						
						if(rowId){ //To check if some how rowid did't found.
							jQuery("#jqgridtable_left").jqGrid('setCell',rowId,'UnpaidAmount',0); //Making unpaid amount as 0.
						}
						
					}
					else{
						showMessage("", "Problem in receiving amount for this log "+LogNo + " contact support. Sent Amount - "+formatFloatNumbers(parseFloat(unpaidAmount))+ " Current Log Amount"+oResult[0]['LogCurrentAmount'], "");
					}
					
				}
				else{
					showMessage("red", parsedResult.Description, "");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	function getArrayFromMultipleSelect(){
		var selRowId=	jQuery("#LOGPendingOrders").jqGrid('getRowData');
		var arrLogValue="[";
		var AddToLog;
		var historyOrderStatus;
		var OrderNo ;
		for(var i=0;i<selRowId.length; i++) {
			
			for(var anItem in selRowId[i])
		    { 
		
				if(anItem=='Invoice'){
					 AddToLog=selRowId[i][anItem];
				}
				if(anItem=='OrderNo'){
					OrderNo=selRowId[i][anItem];
				}
		       
		    }
							if( AddToLog=='True'){
								if(i!=selRowId.length-1){
										arrLogValue=arrLogValue+'{"OrderNo":"'+OrderNo+'"},';
									}
								else
										{
										arrLogValue=arrLogValue+'{"OrderNo":"'+OrderNo+'"}';
										}
								}
			       
			    
		}
		   arrLogValue=arrLogValue+"]";
		  return arrLogValue ;
	}
	
	jQuery("#exit").click(function(){
		window.close();
		
	});
	
	function showInvoiceReport(orderNo,originalOrDuplicate)  //this function call from POSBussiness class
	{
		//jQuery("#actionName").val('Print');
		
		showMessage("loading", "Opening Invoice Preview...", "");

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{
				id:"invoicePrint",
				orderNo:orderNo,
				functionName:"PrintOrder",
				moduleCode:"POS01",
				originalOrDuplicate:originalOrDuplicate
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var resultsData = jQuery.parseJSON(data);
				if(resultsData.Status==1){
					var results=resultsData.Result;
					jQuery.each(results,function(key,value){
						var arr = {'locationId':results[key]['locationId'],'orderID':results[key]['orderNo'],'isDuplicate':results[key]['isDuplicate']}; //invocie copy parameter need to watch
						showReport(results[key]['baseUrl'],results[key]['Url'],arr);

					});
				}
				else{

					showMessage('red',resultsData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	
	jQuery("#btnCourierDetails").click(function(){
		
		var logNumber = jQuery("#LTONumber").val();
		jQuery("#referenceLog").val(logNumber);
		jQuery("#itemsWeight").val( parseFloat(jQuery("#LogTeamOrderGrid").jqGrid('getRowData',jQuery("#LogTeamOrderGrid").getGridParam('selrow')).LogInvoiceWeight,10).toFixed(2));
	
		jQuery( "#divLookUp" ).dialog( "open" );  
		
		/*var width = 1000;
		var height = screen.height;
		var left = (screen.width/2)-(width/2);
		var top = (screen.height/2)-(height/2);
		window.open("/drupal-7.22/CourierDetails?LogNumber="+logNumber,'Courier Details','width='+width+',height='+height+', left='+left);
*/


	});
	
	function getCourierDetails(logNumber){
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'LogTeamOrderCallback',
			data:
			{
				id:"getCourierDetail",
				logNo:logNumber,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var resultsData = jQuery.parseJSON(data);
				if(resultsData.Status==1){
					var results=resultsData.Result;
							if(results.length>0){
								jQuery.each(results,function(key,value){
									jQuery("#docketNo").val(results[key]['DoketNo']);
									jQuery("#courierCompanyName").val(results[key]['CourierCompanyName']);
									jQuery("#noOfBoxes").val(results[key]['NoOfBoxes']);
									jQuery("#weightInGram").val(parseFloat(results[key]['Weigth'],10).toFixed(2));
									jQuery("#docketNo2").val(results[key]['DoketNo']);
									jQuery("#courierCompanyName2").val(results[key]['CourierCompanyName']);
									jQuery("#weightInGram2").val(parseFloat(results[key]['Weigth'],10).toFixed(2)+' ('+results[key]['NoOfBoxes']+')');
									
									jQuery("#CourierId").val(results[key]['CourierId']);
								});
							}
							else{
								jQuery("#docketNo").val('');
								jQuery("#courierCompanyName").val('');
								jQuery("#noOfBoxes").val('');
								jQuery("#weightInGram").val('');
								
								jQuery("#docketNo2").val('');
								jQuery("#courierCompanyName2").val('');
								
								jQuery("#weightInGram2").val('');
								
							}
				}
				else{

					showMessage('red',resultsData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});

	}
	
	
	jQuery("#Cancel").click(function(){

		jQuery( "#divLookUp" ).dialog( "close" );  

	});
	
	function vallidationOnSave(){
		if(	jQuery("#docketNo").val()==''){
			showMessage('yellow','Enter DocketNo','');
			jQuery("#docketNo").focus();
			return 0;
		}
		if(	jQuery("#courierCompanyName").val()==''){
			showMessage('yellow','Enter Courier Company Name','');
			jQuery("#courierCompanyName").focus();
			return 0;
			}
		if(	jQuery("#noOfBoxes").val()==''){
			showMessage('yellow','Enter No Of Boxes','');
			jQuery("#noOfBoxes").focus();
			return 0;
		}
		if(	jQuery("#weightInGram").val()==''){
			showMessage('yellow','Enter Weight','');
			jQuery("#weightInGram").focus();
			return 0;
		}
		if(isNaN(jQuery("#weightInGram").val())){
			showMessage('yellow','Enter Valid Weight','');
			jQuery("#weightInGram").focus();
			jQuery("#weightInGram").select();
			
			return 0;
		}
		if(parseFloat(jQuery("#noOfBoxes").val())==NaN){
			showMessage('yellow','Enter Valid No of Boxes','');
			jQuery("#noOfBoxes").focus();
			jQuery("#noOfBoxes").select();
			
			return 0;
		}
		if(parseInt(parseFloat(jQuery("#itemsWeight").val(),10).toFixed(2))>parseInt(parseFloat(jQuery("#weightInGram").val(),10).toFixed(2))){
			showMessage('yellow','Input weight for Courier is cannot be less than Item(s) weigth ','');
			jQuery("#weightInGram").focus();
			jQuery("#weightInGram").select();

			return 0;
		}
		return 1;
	}
	jQuery("#noOfBoxes").keyup(function(event){
		var intRegex = /^\d+$/;

		if(  !intRegex.test(jQuery("#noOfBoxes").val()) && jQuery("#noOfBoxes").val()!=''){
			jQuery("#noOfBoxes").val('');
			showMessage("yellow","Enter Valid No of Boxes .","");
			jQuery("#noOfBoxes").focus();
			jQuery("#noOfBoxes").select();

		}
		jQuery("#TOCreatePackSize").css("background","white");
	});
	/*jQuery("#weightInGram").keyup(function(event){
		var intRegex = /^\d+$/;

		if(  !intRegex.test(jQuery("#weightInGram").val()) && jQuery("#weightInGram").val()!=''){
			jQuery("#weightInGram").val('');
			showMessage("red","Enter valid no of boxes .","");
		}
		jQuery("#TOCreatePackSize").css("background","white");
	});*/
	jQuery("#save").unbind("click").click(function(){
		
		jQuery("#referenceLog").attr('disabled',false);
		jQuery("#itemsWeight").attr('disabled',false);
		
		courierDetailFormData = jQuery("#courierDetailsForm").serializeArray();
		
		var courierDetailJson = convertArrayToJson(courierDetailFormData);
		
		jQuery("#referenceLog").attr('disabled',true);
		jQuery("#itemsWeight").attr('disabled',true);
	if(vallidationOnSave()==1){
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CourierDetailHandler',
			
			data:
			{
				id:"courierDetailSave",
				courierDetailFormData:courierDetailJson,
				
			},
			success:function(data)
			{
			
				
				var logNumberParsedJsonData = jQuery.parseJSON(data);
				if(logNumberParsedJsonData.Status==1){
					logNumberParsedJson=logNumberParsedJsonData.Result;
							//alert("CO Log is:"+data);
							jQuery.each(logNumberParsedJson,function(key,value){
								showMessage('green','Courier Details Updated for '+jQuery("#LTONumber").val(),'');

								jQuery("#docketNo2").val(jQuery("#docketNo").val());
								jQuery("#courierCompanyName2").val(jQuery("#courierCompanyName").val());
								jQuery("#weightInGram2").val(jQuery("#weightInGram").val()+' ('+jQuery("#noOfBoxes").val()+')');
								jQuery( "#divLookUp" ).dialog( "close" );
							});
				}
				else{
					showMessage('red',logNumberParsedJsonData.Description,'');
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}
	else{
		
	}
		
		
	});

	
});
