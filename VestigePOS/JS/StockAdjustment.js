var jsonForLocations ;
var jsonForFromItems ;
var jsonForToItems ;
var loggedInUserPrivilegesForStockAdjustment;
var bucketBatchLocWiseQty ;
var toBatchNo='' ;
var fromBatchNo ='' ;
document.onkeydown = function (e) {
  if (e.keyCode === 116) {
    return false;
  }
};
jQuery(document).ready(function(){
	jQuery("#SACreateInitialDate").attr('disabled',true);
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 625,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	  });
	  
	var moduleCode = jQuery("#moduleCode").val();
	jQuery("#btnINV03Save").hide();
	showCalender();
	showCalender2();
	//jQuery("#SACreateInitialDate").datepicker({ maxDate: new Date, minDate: new Date });
	
	jQuery("#main-menu").hide();

	jQuery(".breadcrumb").hide();

	jQuery("#triptych-wrapper").hide();


	jQuery("#footer-wrapper").hide();
	
	getCurrentLocation();
		jQuery(function() {
			//alert("dfadf");
			jQuery( "#divStockAdjustmentTab" ).tabs(); // Create tabs using jquery ui.
		});
     jQuery("#divStockAdjustmentTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
			
			var tabIndex = tab.index; 
			if(tabIndex == 0)
			{
				disableOnSatrtup();
				ResetItemsOnSACreateClear();
				jQuery("#TOIItemGrid").jqGrid("clearGridData");
			}
			else if(tabIndex == 1){
				
			}
			});
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:
			{
				id:"moduleFunctionHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesStockAdj = jQuery.parseJSON(data);
				
				if(loggedInUserPrivilegesStockAdj.Status == 1)
				{
					
					loggedInUserPrivilegesForStockAdjustment  = loggedInUserPrivilegesStockAdj.Result;
					
					if(loggedInUserPrivilegesForStockAdjustment.length == 0)
					{
						//jQuery(".SCCreateActionButtons").attr('disabled',true); //all button coming as disabled already.
					}
					else
					{

						disableOnSatrtup();
						searchWHLocation();
						SAStatus();
						users();
						searchALLBucket();
						reasonCode();
					}
				}
				else
				{
					showMessage("red", loggedInUserPrivilegesStockAdj.Description, "");
				}
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
		
		
		
		function actionButtonsStauts()
		{
			jQuery(".StockAdjustmentActionButtons").each(function(){
				
				var buttonid = this.id;
				var extractedButtonId = buttonid.replace("btn","");
				
				if(loggedInUserPrivilegesForStockAdjustment[extractedButtonId] == 1)
				{
					//jQuery("#"+buttonid).attr('disabled',false);
				}
				else
				{
					jQuery("#"+buttonid).attr('disabled',true);
				}
				jQuery("#btnCreateReset").attr('disabled',false);
			});
		}
		
		
		function searchALLBucket()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchALLBucket",
				},
				success:function(data)
				{
					//alert(data);
					
				var searchALLBucketData = jQuery.parseJSON(data);
				if(searchALLBucketData.Status==1){
					var searchALLBucket=searchALLBucketData.Result;
					jQuery.each(searchALLBucket,function(key,value)
					{	
						jQuery("#SACreateToBucket").append("<option id=\""+searchALLBucket[key]['BucketId']+"\""+" value=\""+searchALLBucket[key]['BucketId']+"\""+">"+searchALLBucket[key]['BucketName']+"</option>");
						jQuery("#SACreateFromBucket").append("<option id=\""+searchALLBucket[key]['BucketId']+"\""+" value=\""+searchALLBucket[key]['BucketId']+"\""+">"+searchALLBucket[key]['BucketName']+"</option>");
						removeFromBucketOption();
					});
					jQuery("#SACreateToBucket").append("<option id=\""+'16'+"\""+" value=\""+'16'+"\""+">"+'Destroyed'+"</option>");
				}
				
				else{
					showMessage('red',searchALLBucketData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		function reasonCode()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"reasonCode",
				},
				success:function(data)
				{

					var reasonCodeData = jQuery.parseJSON(data);
				if(reasonCodeData.Status==1){
					reasonCode=reasonCodeData.Result;
					jQuery.each(reasonCode,function(key,value)
					{	
						jQuery("#SACreateReasonCode").append("<option id=\""+reasonCode[key]['keycode1']+"\""+" value=\""+reasonCode[key]['keycode1']+"\""+">"+reasonCode[key]['keyvalue1']+"</option>");
					
					//	jQuery("#SACreateReasonCode").find("option[value='11']").remove();
					});
				}
				else{
					showMessage('red',reasonCodeData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		function searchWHLocation()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchWHLocation",
				},
				success:function(data)
				{
				var whlocationsData = jQuery.parseJSON(data);
				if(whlocationsData.Status==1){
				whlocations=whlocationsData.Result;
				jsonForLocations=whlocationsData.Result;
					jQuery.each(whlocations,function(key,value)
							
					{	
						jQuery("#location").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#SACreateLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#SACreateLocation").find("option[value='-1']").remove();
					});
					jQuery('#SACreateLocation').val(currentLocationId);
					jQuery('#SACreateLocation').attr('disabled',true);
					jQuery('#SACreateLocation').css("background","#dddddd");
					jQuery('#location').val(currentLocationId);
					jQuery('#location').attr('disabled',true);
					jQuery('#location').css("background","#dddddd");
					var locationAddress=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'Address');
					jQuery("#SACreateLocationAddress").val(locationAddress);
					var locationCode=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationCode');
					jQuery("#SACreateLocationCode").val(locationCode);
					jQuery('#SACreateLocationAddress').attr('disabled',true);
					jQuery('#SACreateLocationCode').attr('disabled',true);
					backGroundColorForDisableField();
				}
			else{
				showMessage('red',whlocationsData.Description,'');
			}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		function SAStatus()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"SAStatus",
				},
				success:function(data)
				{
					var whlocationsData = jQuery.parseJSON(data);
					if(whlocationsData.Status==1){
						whlocations=whlocationsData.Result;
					jQuery.each(whlocations,function(key,value)
					{	
						jQuery("#status").append("<option id=\""+whlocations[key]['keycode1']+"\""+" value=\""+whlocations[key]['keycode1']+"\""+">"+whlocations[key]['keyvalue1']+"</option>");
					});
				}
					else{
						showMessage('red',whlocationsData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		function users()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"users",
				},
				success:function(data)
				{
					var userData = jQuery.parseJSON(data);
					if(userData.Status==1){
						user=userData.Result;
					jQuery.each(user,function(key,value)
					{	
						jQuery("#users").append("<option id=\""+user[key]['UserId']+"\""+" value=\""+user[key]['UserId']+"\""+">"+user[key]['Name']+"</option>");
					});
				}
					else{
						showMessage('red',userData.Description,'');
					}	
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
		
		
	jQuery("#SACreateExportItem").change(function ()	{
		if(jQuery("#SACreateExportItem").is(':checked')==true)
			
		{
	       jQuery("#SABatchAdjust").attr('checked',false); 
			jQuery("#SACreateToItemCode").css("background","white");
			//jQuery("#SACreateToBatchNo").css("background","white");
			jQuery("#SACreateToItemCode").attr('disabled',false);
			jQuery("#SACreateToItemCode").val('');
		//	jQuery("#SACreateToBatchNo").attr('disabled',false);
			jQuery("#SACreateToBatchNo").val('');
			jQuery("#SACreateToBucket").val(5);
			jQuery("#SACreateReasonCode").val(10);
			jQuery("#SACreateFromBucket").attr('disabled',true);
			jQuery("#SACreateToBucket").attr('disabled',true);
			jQuery("#SACreateToBatchNo").attr('disabled',true);
			jQuery("#SACreateToBatchNo").css("background",'#DDDDDD');
			jQuery("#SACreateReasonCode").attr('disabled',true);
			}
			else{
				disableOnSatrtup();
				ResetItemsOnSACreateClear();
				jQuery("#TOIItemGrid").jqGrid("clearGridData");
				jQuery("#SACreateExportItem").attr('checked',false); 
				//jQuery("#SACreateToBatchNo").css("background",'white');
				enableDisableButtonOnStatus(0);
			}
		isPrintButtonDisableOrNot();
		
		});
		
		jQuery("#SABatchAdjust").change(function ()	{
			if(jQuery("#SABatchAdjust").is(':checked')==true){
				jQuery("#SACreateExportItem").attr('checked',false); 
		//	jQuery("#SACreateToItemCode").css("background","white");
			jQuery("#SACreateToItemCode").attr('disabled',true);
			jQuery("#SACreateToItemCode").css("background","#DDDDDD");
			//jQuery("#SACreateToBatchNo").css("background","white");
			
			jQuery("#SACreateToItemCode").val('');
		//	jQuery("#SACreateToBatchNo").attr('disabled',false);
			jQuery("#SACreateToBatchNo").val('');
			jQuery("#SACreateToBucket").val(5);
			jQuery("#SACreateReasonCode").val(11);
			jQuery("#SACreateFromBucket").attr('disabled',true);
			jQuery("#SACreateToBucket").attr('disabled',true);
			jQuery("#SACreateReasonCode").attr('disabled',true);
			jQuery("#SACreateToBatchNo").attr('disabled',false);
			jQuery("#SACreateToBatchNo").css("background","white");
			
			}
			else{
				disableOnSatrtup();
				ResetItemsOnSACreateClear();
				jQuery("#TOIItemGrid").jqGrid("clearGridData");
				jQuery("#SABatchAdjust").attr('checked',false); 
				//jQuery("#SACreateToItemCode").css("background","white");
				enableDisableButtonOnStatus(0);
			}
			isPrintButtonDisableOrNot();
		});
		
		jQuery("#btnINV03Search").click(function ()	{
			if(jQuery("#fromInitialDate").val()!=''){
			initiateDate=jQuery.datepicker.formatDate("yy-mm-dd", new Date(jQuery("#fromInitialDate").val() ));
			}
			else{
				initiateDate='';
			}
			if(jQuery("#toInitialDate").val()!=''){
				ToInitiateDate=jQuery.datepicker.formatDate("yy-mm-dd", new Date(jQuery("#toInitialDate").val() ));
			}
			else{
				ToInitiateDate='';
			}
			showMessage("loading", "Searching stock adjustments...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchSA",
					moduleCode:"INV03",
					functionName:"Search",
					LocationId:jQuery("#location").val(),
					Status:jQuery("#status").val(),
					SeqNo:jQuery("#seqNo").val(),
					FromInitiateDate:initiateDate,
					ToInitiateDate:ToInitiateDate,
					ApprovedBy:jQuery("#users").val(),
					Isexported:2,
					InternalBatAdj:2
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					    var searchTOData = jQuery.parseJSON(data);
					    
					    if(searchTOData.Status==1){
					    	searchTO=searchTOData.Result;
					    
							if(searchTO.length==0)
							{
								jQuery("#SASearchGrid").jqGrid("clearGridData");
								showMessage("yellow","No Record Found","");
							}
							else
							{	
								var rowid=0;
								jQuery("#SASearchGrid").jqGrid("clearGridData");
								jQuery.each(searchTO,function(key,value){			
									var newData = [{
										"SeqNo":searchTO[key]['AdjustmentNo'],
										"Location":searchTO[key]['LocationName'],
										"InitiatedDate":searchTO[key]['InitiatedDate'],
										"InitiatedBy":searchTO[key]['InitiatedName'],
										"appRejBy":searchTO[key]['ApprovedName'],
										"appRejDate":searchTO[key]['ApprovedDate'],
										"Status":searchTO[key]['StatusName'],
										"LocationAddress":searchTO[key]['LocationName'],
										"LocationId":searchTO[key]['LocationId'],
							          	"ApprovedBy":searchTO[key]['ApprovedBy'],
							          	"ApprovedDate":searchTO[key]['ApprovedDate'],
							          	"InitiatedByid":searchTO[key]['InitiatedBy'],
										"InitiatedName":searchTO[key]['StatusName'],
										"ApprovedName":searchTO[key]['Status'],
										"LocationName":searchTO[key]['LocationName'],
										"StatusName":searchTO[key]['StatusName'],
										"StatusId":searchTO[key]['StatusId'],
										
										
										"Exportstatus":searchTO[key]['Exportstatus'],
							          	"Interadjstatus":searchTO[key]['Interadjstatus'],
							          	"ModifiedBy":searchTO[key]['ModifiedBy']
							
												
									}];
									
									for (var i=0;i<newData.length;i++) {
									jQuery("#SASearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
									rowid=rowid+1;
									}
								});
								
							}
								
				}
					    else{
					    	showMessage('red',searchTOData.Description,'');
					    }
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		});
		
		/*
		* Delete option for customer return grid row delete
		*/
		var stockAdjRowDel = {
			onclickSubmit:function(rw_pe, rowId){
				rw_pe.processing = true;
				//console.log("Deleted row id would be - " + rowId + " for this - " + rw_pe);
				jQuery('#TOIItemGrid').jqGrid('delRowData',rowId);
				jQuery("#delmod" + jQuery("#TOIItemGrid")[0].id).hide();
				return true;
			},
			processing:true,
		};
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * TOI Create Grid 
		 * 
		 * "UOMId":fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'UOMId'),
				"FromBucketId":jQuery("#SACreateFromBucket").val(),
				"ToBucketId":jQuery("#SACreateToBucket").val(),
				"ReasonId"
		 */
		jQuery("#TOIItemGrid").jqGrid({
			
			datatype: 'jsonstring',
			colNames: ['ID','Action','ItemId','Item Code','Item Name','UOM','From Bucket','To Bucket','Quantity','Reason Code','Approved Qty',
			           'From BatchNo','UOMId','From BucketName','To BucketName','ReasonId','MfgDate','ExpDate','BatchId','FromBucket','ToBucket','ToItemid','To Item Code',
			           'To Batch No','To BatchNo','To Item Name'],
			colModel: [	{ name: 'RowID', index: 'id', classes: 'id', width: 20, hidden:true},
						{name: 'Action', resize:false,sortable:false,width: 50,formatter: 'actions',
						formatoptions: {
										keys: true,
										onEdit:function(rowId){
											//alert("Edited row Id would be - " + rowId);
										},
										onSuccess:function(jqXHR) {
		                             // the function will be used as "succesfunc" parameter of editRow function
		                             // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
		                             alert("in onSuccess used only for remote editing:"+
		                                   "\nresponseText="+jqXHR.responseText+
		                                   "\n\nWe can verify the server response and return false in case of"+
		                                   " error response. return true confirm that the response is successful");
		                             // we can verify the server response and interpret it do as an error
		                             // in the case we should return false. In the case onError will be called
		                             return true;
		                         },
		                         onError:function(rowid, jqXHR, textStatus) {
		                             // the function will be used as "errorfunc" parameter of editRow function
		                             // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
		                             // and saveRow function
		                             // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#saverow)
		                             alert("in onError used only for remote editing:"+
		                                   "\nresponseText="+jqXHR.responseText+
		                                   "\nstatus="+jqXHR.status+
		                                   "\nstatusText"+jqXHR.statusText+
		                                   "\n\nWe don't need return anything");
		                         },
		                         afterSave:function(rowid) {
		                             //alert(rowData);
		                         },
		                         afterRestore:function(rowid) {
		                             //alert("in afterRestore (Cancel): rowid="+rowid+"\nWe don't need return anything");
		                            
		                         },
								 delOptions:stockAdjRowDel
								}
						},
			           { name: 'ItemId', index: 'Item', width:100,hidden:true },
			           { name: 'ItemCode', index: 'ItemCode', width:100 },
			           { name: 'ItemName', index: 'ItemName', width: 150},
			           { name: 'UOM', index: 'UOM', width: 100},
			           { name: 'FromBucketName', index: 'FromBucket', width: 100},
			           { name: 'ToBucketName', index: 'ToBucket', width: 70},
			           { name: 'Quantity', index: 'Quantity', width: 70},
			           { name: 'ReasonCode', index: 'ReasonCode', width: 100},
			           { name: 'ApprovedQty', index: 'ApprovedQty', width: 120},
			           { name: 'BatchNo', index: 'BatchNo', width: 100},
			           { name: 'UOMId', index: 'UOMId', width: 30 ,hidden:true},
			           { name: 'FromBucketId', index: 'FromBucketId', width: 30,hidden:true},
			           { name: 'ToBucketId', index: 'ToBucketId', width: 30,hidden:true},
			           { name: 'ReasonId', index: 'ReasonId', width: 30,hidden:true},
			           { name: 'MfgDate', index: 'MfgDate', width: 30,hidden:true},
			           { name: 'ExpDate', index: 'ExpDate', width: 30,hidden:true},
			           { name: 'BatchId', index: 'BatchId', width: 30,hidden:true},
			           { name: 'FromBucket', index: 'FromBucket', width: 100,hidden:true},
			           { name: 'ToBucket', index: 'ToBucket', width: 70,hidden:true},
			           { name: 'ToItemid', index: 'ToItemid', width: 30,hidden:true},
			           { name: 'ToItemCode', index: 'ToItemCode', width: 100},
			           { name: 'ToBatchNo', index: 'ToBatchNo', width: 30,hidden:true},
			           { name: 'ToManufactureBatchNo', index: 'ToManufactureBatchNo', width: 100,hidden:true},
			           { name: 'ToItemName', index: 'ToItemName', width: 150,hidden:true}
			           ],
			         
			           pager: jQuery('#PJmap_TOIItemGrid'),
			           width:1040,
			           height:150,
			           rowNum: 500,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           shrinkToFit:false,
			           afterInsertRow : function(ids)
			           {
//			        	   
			        	   var index1 = jQuery("#TOIItemGrid").jqGrid('getCol','Name',false);
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	
			           },

			           loadComplete: function() {
//			        	  
			           }	
		});

		jQuery("#TOIItemGrid").jqGrid('navGrid','#pjmap_downline_grid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_TOIItemGrid").hide();
		
		
		/*------------------------------add calender to fields --------------------------------*/
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * TOI Search Grid 
		 */
		jQuery("#SASearchGrid").jqGrid({
		
			datatype: 'jsonstring',
			colNames: ['Seq No','Location','Initiated Date','Initiated By','Approved/Rejected By','Approved/RejectedDate',
			           'Status','LocationId','ApprovedBy','ApprovedDate','InitiatedBy','InitiatedName','ApprovedName','LocationName',
			           'LocationAddress','StatusName','StatusId','Exportstatus','Interadjstatus','ModifiedBy'],
			colModel: [
			           { name: 'SeqNo', index: 'SeqNo', width: 150},
			           { name: 'Location', index: 'Location', width: 150},
			           { name: 'InitiatedDate', index: 'InitiatedDate', width: 100},
			           { name: 'InitiatedBy', index: 'InitiatedBy', width: 70},
			           { name: 'appRejBy', index: 'appRejBy', width: 70},
			           { name: 'appRejDate', index: 'appRejDate', width: 100},
			           { name: 'Status', index: 'Status', width: 100},
			           { name: 'LocationId', index: 'LocationId', width: 70,hidden:true},
			           { name: 'ApprovedBy', index: 'ApprovedBy', width: 70,hidden:true},
			           { name: 'ApprovedDate', index: 'ApprovedDate', width: 70,hidden:true},
			           { name: 'InitiatedByid', index: 'InitiatedByid', width: 70,hidden:true},
			           { name: 'InitiatedName', index: 'InitiatedName', width: 70,hidden:true},
			           { name: 'ApprovedName', index: 'ApprovedName', width: 70,hidden:true},
			           { name: 'LocationName', index: 'LocationName', width: 70,hidden:true},
			           { name: 'LocationAddress', index: 'LocationAddress', width: 70,hidden:true},
			           { name: 'StatusName', index: 'StatusName', width: 70,hidden:true},
			           { name: 'StatusId', index: 'StatusId', width: 70,hidden:true},
			           { name: 'Exportstatus', index: 'Exportstatus', width: 70,hidden:true},
			           { name: 'Interadjstatus', index: 'Interadjstatus', width: 70,hidden:true},
			           { name: 'ModifiedBy', index: 'ModifiedBy', width: 70,hidden:true}
		              ],
			           pager: jQuery('#PJmap_SASearchGrid'),
			           width:1040,
			           height:500,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,


			           afterInsertRow : function(ids)
			           {
			        	   var index1 = jQuery("#SASearchGrid").jqGrid('getCol','Name',false);
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
			           },

			           loadComplete: function() {
			           },
			           /*
			           'Seq No','Location','Initiated Date','Initiated By','Approved/Rejected By','Approved/RejectedDate',
			           'Status','LocationId','ApprovedBy','ApprovedDate','InitiatedBy','InitiatedName','ApprovedName','LocationName',
			           'LocationAddress','StatusName','StatusId','Exportstatus','Interadjstatus','ModifiedBy'],*/
			           ondblClickRow: function(rowId) {
			        	   
				        	if(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'Exportstatus')==1){
				            	jQuery("#SACreateExportItem").attr('checked',true); 
				        	} 
				        	else if (jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'Exportstatus')==2){
				        		jQuery("#SABatchAdjust").attr('checked',true);
				        	}
				        	else{
				        		jQuery("#SACreateExportItem").attr('checked',false); 	
				        	}
			        	 	jQuery("#SACreateLocation").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'LocationId'));
				       		
				       		jQuery("#SACreateLocationCode").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'LocationName'));
				       		jQuery("#SACreateLocationAddress").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'LocationAddress'));
				       		jQuery("#SACreateAdjustmentNo").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'SeqNo'));
				       		jQuery("#SACreateInitialDate").val(displayDate(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'InitiatedDate')));
				       		
				       		jQuery("#SACreateInitiatedBy").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'InitiatedName'));
				       		jQuery("#SACreateApprovedRejectedBy").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'appRejBy'));
				       		jQuery("#SACreateStatus").val(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'StatusName'));
				       		enableDisableButtonOnStatus(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'StatusId'));
				       		jQuery("#divStockAdjustmentTab").tabs( "select", "DivCreate" );
				       		inventoryDetails(jQuery("#SASearchGrid").jqGrid('getCell', rowId, 'SeqNo'));
				       		disableHeaderLevelInfo();
			           }
 
		});

		jQuery("#SASearchGrid").jqGrid('navGrid','#PJmap_SASearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_SASearchGrid").hide();
		
		jQuery("#SACreateLocation").change(function(){
			var locationAddress=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'Address');
			jQuery("#SACreateLocationAddress").val(locationAddress);
			var locationCode=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationCode');
			jQuery("#SACreateLocationCode").val(locationCode);
			changeOnLocationChange();
		});
		
		function inventoryDetails(AdjusmentNo){
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"inventoryDetails",
					AdjusmentNo:AdjusmentNo,
				},
				success:function(data)
				{
					//alert(data);	
					var InventoryDetailsData = jQuery.parseJSON(data);
					jQuery("#TOIItemGrid").jqGrid("clearGridData");
				
					if(InventoryDetailsData.Status == 1)
					{
									InventoryDetails=InventoryDetailsData.Result ;	
								if(InventoryDetails.length>0){
									jQuery.each(InventoryDetails,function(key,value){	
										 
											var newData = [{
												"ItemId":InventoryDetails[key]['ItemId'] ,
												"ItemCode":InventoryDetails[key]['ItemCode'],
												"ItemName":InventoryDetails[key]['ItemName'],
												"UOM":InventoryDetails[key]['UOMName'],
												"FromBucketName":InventoryDetails[key]['FromBucketName'] ,
												"ToBucketName":InventoryDetails[key]['ToBucketName'] ,
												"Quantity":parseFloat(InventoryDetails[key]['Quantity'],10).toFixed(0),
												"ReasonCode":InventoryDetails[key]['ReasonCodeDescription'] ,
												"ApprovedQty":parseFloat(InventoryDetails[key]['ApprovedQty'],10).toFixed(0),
												"BatchNo":InventoryDetails[key]['ManufactureBatchNo'],
												"UOMId":InventoryDetails[key]['UOMId'],
												"FromBucketId":InventoryDetails[key]['FromBucketId'],
												"ToBucketId":InventoryDetails[key]['ToBucketId'],
												"ReasonId":InventoryDetails[key]['ReasonCode'],
												
												"MfgDate":InventoryDetails[key]['Mfg'],
												"ExpDate":InventoryDetails[key]['Exp'],
												"BatchId":InventoryDetails[key]['BatchNo'],
												"ToItemid":InventoryDetails[key]['ToItemId'],
												"ToItemCode":InventoryDetails[key]['ToItemCode'],
										           "ToBatchNo":InventoryDetails[key]['ToBatchNo'],
										           "ToManufactureBatchNo":InventoryDetails[key]['ToManufactureBatchNo'],
										           "ToItemName":InventoryDetails[key]['ItemName']
											}];
											
											for (var i=0;i<newData.length;i++) {
													jQuery("#TOIItemGrid").jqGrid('addRowData',itemRowId, newData[newData.length-i-1], "first");
													itemRowId=itemRowId+1;
												}
											jQuery("#SACreateFromBucket").val(InventoryDetails[key]['FromBucketId']);
											jQuery("#SACreateToBucket").val(InventoryDetails[key]['ToBucketId']);
											jQuery("#SACreateReasonCode").val(parseInt(InventoryDetails[key]['ReasonCode']));
									});
								  
								}	
								
					}
					else
					{
						showMessage("red", InventoryDetailsData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		
		
		
		jQuery("#SACreateFromItemCode").keydown(function(e){
			
			jQuery("#SACreateAvailableQty").val('');
		    if(e.which==9 || e.which == 13){
		    	jQuery("#SACreateFromItemCode").val((jQuery("#SACreateFromItemCode").val()).toUpperCase());
		    	e.preventDefault(); 
		    	ResetItemsOnItemCodeChange();
		    	var itemCode = jQuery("#SACreateFromItemCode").val();
		    	
		    	searchItemUsingCode(itemCode,1);        
		    }
		    if(e.which==115 ){
		    	jQuery("#SACreateFromItemCode").val((jQuery("#SACreateFromItemCode").val()).toUpperCase());
		    	var currentItemSearchId = this.id;
				//alert(currentItemSearchId);
				showMessage("loading", "Please wait...", "");
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"itemLookUp",
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						
						//alert(data);
						jQuery("#divLookUp").html(data);
						
						itemSearchGrid(currentItemSearchId);
						jQuery("#divLookUp" ).dialog( "open","Item Search");
						
						jQuery("#divLookUp").dialog('option', 'title', 'Item Search');
						
						itemLookUpData();
						
						/*----------Dialog for item search is there ----------------*/
						
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				
				});

		    }

		});
		jQuery("#SACreateToItemCode").keydown(function(e){
			

			    if(e.which==9 || e.which == 13){
			    	jQuery("#SACreateToItemCode").val((jQuery("#SACreateToItemCode").val()).toUpperCase());
			    	e.preventDefault(); 
			    	var itemCode = jQuery("#SACreateToItemCode").val();
			    	 if(jQuery("#SACreateExportItem").is(':checked'))	{
			    			searchItemUsingCode(itemCode,2);       
					    }
			    	 else{
			    		 searchItemUsingCode(itemCode,3);
			    	 }
			          
			    }
			    if(e.which==115){
			    	jQuery("#SACreateToItemCode").val((jQuery("#SACreateToItemCode").val()).toUpperCase());
			    	var currentItemSearchId = this.id;
					//alert(currentItemSearchId);
					showMessage("loading", "Please wait...", "");
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'LookUpCallback',
						data:
						{
							id:"itemLookUp",
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							
							//alert(data);
							jQuery("#divLookUp").html(data);
							
							itemSearchGrid(currentItemSearchId);
							jQuery("#divLookUp" ).dialog( "open","Item Search");
							
							jQuery("#divLookUp").dialog('option', 'title', 'Item Search');
							
							itemLookUpData();
							
							/*----------Dialog for item search is there ----------------*/
							
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					
					});


			    }

		});
		
		function 	searchItemUsingCode (itemCode,forType){
			showMessage("loading", "Search for item details...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback', // use TOICallBack for PHP call already functionWritten
				data:
				{
					id:"searchItemUsingCode",
					itemCode:itemCode,
					type:forType,
					fromItemCode:jQuery("#SACreateFromItemCode").val(),
				},
				success:function(data)
				{
				   
					jQuery(".ajaxLoading").hide();
					
					itemDescData = jQuery.parseJSON(data);
		if(itemDescData.Status==1){
			itemDesc=itemDescData.Result;
			if(itemDesc.length!=0)
			{
					if(forType==1){      // for type tell item code is for from item or to item
						jsonForFromItems=itemDesc;
						}
					else if(forType==2)
						{
							jsonForToItems=itemDesc;
						}
				
					 jQuery.each(itemDesc,function(key,value)
					 {	
						 if(forType==1){
						jQuery("#SACreateFromItemDesc").val(itemDesc[key]['ItemName']);
						jQuery("#SACreateFromItemCode").css("background","white");
						jQuery("#SACreateFromWeight").val(parseFloat(itemDesc[key]['Weight'],10).toFixed(2));
						if(jQuery("#SACreateExportItem").is(':checked')==false){
						jQuery("#SACreateFromBatchNo").focus();
						}
						else{
							jQuery("#SACreateToItemCode").focus();
						}
						 }
						 
						 else if(forType==2)
							 {
							   jQuery("#SACreateToItemDesc").val(itemDesc[key]['ItemName']);
								jQuery("#SACreateToItemCode").css("background","white");
								jQuery("#SACreateToWeight").val(parseFloat(itemDesc[key]['Weight'],10).toFixed(2));
								jQuery("#SACreateToBatchNo").val(jQuery("#SACreateFromBatchNo").val());
								jQuery("#SACreateToBatchNo").focus();
							 }
						
					  });
					
				    }
			else {
					if (forType==1){
						jQuery("#SACreateFromItemCode").css("background","#FF9999");
				        showMessage("yellow","Enter valid Item Code .","");
				
					}
					else if (forType==2){
						jQuery("#SACreateToItemCode").css("background","#FF9999");
				        showMessage("yellow","Enter valid Item Code .","");
						
					    }
					}
				}
		else
		{
		  showMessage('red',itemDescData.Description,'');
		}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
				
			});	
			
		}
		
		/*jQuery("#SACreateFromBatchNo").keydown(function(e){
		    if(e.which==115){
		    	
		    	lookupForSearchingBatches();
		    	//searchBatchUsingCode(SACreateFromBatchNo,locationId,itemId);        
		    }

		});*/

		jQuery("#SACreateFromBatchNo").keydown(function(e){
		    if(e.which==9 || e.which == 13){
		    	e.preventDefault(); 
		    	//lookupForSearchingBatches();
		    	searchSinglemfgBatchCode(1);        
		    }
		    if(e.which==115){
		    	
		    	lookupForSearchingBatches(1);
		    	//searchBatchUsingCode(SACreateFromBatchNo,locationId,itemId);        
		    }

		});
		jQuery("#SACreateToBatchNo").keydown(function(e){
		    if(e.which==9 || e.which == 13){
		    	e.preventDefault(); 
		    	//lookupForSearchingBatches();
		    	//searchSinglemfgBatchCode(2);        
		    }
		    if(e.which==115){
		    	
		    	lookupForSearchingBatches(2);
		    	//searchBatchUsingCode(SACreateFromBatchNo,locationId,itemId);        
		    }

		});
		
				
		function searchSinglemfgBatchCode(typeForBatchSearch){
			var itemId ;
			var locationId;
			var SACreateFromBatchNo ;
			var bucketId ;
			SACreateFromBatchNo=jQuery("#SACreateFromBatchNo").val() ;
			 if(typeForBatchSearch==2){
			  var locationId=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationId');
		    	 itemId=fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId');
		    	 bucketId=  jQuery("#SACreateFromBucket").val() ;
		    	
		     }
			 else{
				    var locationId=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationId');
			    	var itemId=fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId');
			    	bucketId=  jQuery("#SACreateFromBucket").val() ;
			    	
			     }
		    	var rowid=0;
			  showMessage("loading", "Searching Batches...", "");
			   jQuery.ajax({
				   type:'POST',
					url:Drupal.settings.basePath + 'StockAdjustmentCallback',
					data:
					{
						id:"searchBatchUsingCode",
						bNo:SACreateFromBatchNo,
						locationId:locationId,
						itemId:itemId,
						bucketId:bucketId,
						type:1
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						jQuery("#SACreateFromBatchNo").css("background","white");
						 resultArrData = jQuery.parseJSON(data);
				if(resultArrData.Status==1){
					bucketBatchLocWiseQty=resultArrData.Result;
					resultArr=resultArrData.Result;
					if(resultArr.length==1){
						bucketBatchLocWiseQty=resultArr;
						 jQuery.each(resultArr,function(key,value){
							 
			            		jQuery("#SACreateAvailableQty").val(parseFloat(resultArr[key]['Quantity'],10).toFixed(0));
			            		jQuery("#SACreateAllocatedQty").focus();
			            		fromBatchNo=bucketBatchLocWiseQty[0].BatchNo ;
							             		
						 });
					}
				else if(resultArr.length>1){
					 showMessage("yellow","Multiple valid batches found. Use lookup to select one.","");
					}
					else {
						
				        showMessage("yellow","No Record Found.","");
					}
				}
				else{
					showMessage('red',resultArrData.Description,'');
				}
					
				  },
				  error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				
				});
			} 
		
		
	 function lookupForSearchingBatches(typeForBatchSearch){
		 
		    jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:"ManufactureBaLookUp",
				},
				success:function(data)
				{

					//alert(data);
					jQuery("#divLookUp").html(data);

					manufactureBatchSearchGrid('SACreateFromBatchNo',typeForBatchSearch);
					jQuery("#divLookUp" ).dialog( "open" );
					jQuery("#manufactureBatchNo").val('');
					//manufactureBatchLookUpData('',jQuery('#SACreateFromItemCode').val());
					manufactureBatchLookUpData(typeForBatchSearch);

					/*----------Dialog for item search is there ----------------*/


				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});
		    
	 }
	   
		

		function disableOnSatrtup(){
			
			jQuery("#SACreateAdjustmentNo").attr('disabled',true);
			jQuery("#SACreateAdjustmentNo").val('');
			jQuery("#SACreateInitiatedBy").attr('disabled',true);
			jQuery("#SACreateInitiatedBy").val('');
			jQuery("#SACreateApprovedRejectedBy").attr('disabled',true);
			jQuery("#SACreateApprovedRejectedBy").val('');
			jQuery("#SACreateStatus").attr('disabled',true);
			jQuery("#SACreateStatus").val('New');
			jQuery("#SACreateFromItemDesc").attr('disabled',true);
			jQuery("#SACreateFromItemDesc").val('');
			jQuery("#SACreateFromWeight").attr('disabled',true);
			jQuery("#SACreateFromWeight").val('');
			jQuery("#SACreateToItemDesc").attr('disabled',true);
			jQuery("#SACreateToItemDesc").val('');
			jQuery("#SACreateToWeight").attr('disabled',true);
			jQuery("#SACreateToWeight").val('');
			jQuery("#SACreateApprovedQty").attr('disabled',true);
			jQuery("#SACreateApprovedQty").val('');
			
			jQuery("#SACreateToItemCode").attr('disabled',true);
			jQuery("#SACreateToItemCode").val('');
			jQuery("#SACreateToBatchNo").attr('disabled',true);
			jQuery("#SACreateToBatchNo").val('');
			
			jQuery("#SACreateInitiatedQty").attr('disabled',true);
			jQuery("#SACreateInitiatedQty").val('');
			jQuery("#btnINV03Reject").attr('disabled',true);
			jQuery("#btnPrint").attr('disabled',true);
			jQuery("#btnINV03Cancel").attr('disabled',true);
			jQuery("#btnReset").attr('disabled',false);
			jQuery("#btnINV03Save").attr('disabled',true);
			jQuery("#btnINV03Approve").attr('disabled',true);
		 	jQuery("#btnINV03Initiate").attr('disabled',false);
			//jQuery("#SABatchAdjust").attr('disabled',true);
		
			jQuery("#SACreateFromBucket").val(5);
			jQuery("#SACreateFromBucket").css("background","white");
			jQuery("#SACreateFromBucket").attr('disabled',false);
			jQuery("#SACreateToBucket").val(-1);
			jQuery("#SACreateToBucket").attr('disabled',false);
			jQuery("#SACreateToBucket").css("background","white");
			jQuery("#SACreateReasonCode").val(-1);
			jQuery("#SACreateReasonCode").attr('disabled',false);
			jQuery("#SACreateReasonCode").css("background","white");
			jQuery("#SACreateAvailableQty").attr('disabled',true);
			jQuery("#SACreateAvailableQty").val('');
			jQuery("#SACreateFromItemCode").val('');
			jQuery("#SACreateToItemCode").val('');
			jQuery("#SACreateToItemCode").css("background","white");
			jQuery("#SACreateAllocatedQty").val('');
			jQuery("#SACreateAllocatedQty").css("background","white");
			jQuery("#SACreateApprovedQty").val('');
			jQuery("#SACreateInitiatedQty").val('');
			jQuery("#SACreateReason").val('');
			removeFromBucketOption();
			
			actionButtonsStauts();
		}
		
		function ResetItemsOnItemCodeChange(){
			jQuery("#SACreateFromItemDesc").val('');
			jQuery("#SACreateFromBatchNo").val('');
			jQuery("#SACreateFromWeight").val('');
			jQuery("#SACreateAllocatedQty").val('');
			jQuery("#SACreateApprovedQty").val('');
			jQuery("#SACreateInitiatedQty").val('');
			jQuery("#SACreateAvailableQty").val('');
			
		
			
		}
		
		
		function removeFromBucketOption(){
			
			jQuery("#SACreateFromBucket").val(5);
			
		}
		
		jQuery("#SACreateClear").click(function(){
			
			ResetItemsOnSACreateClear();
			   
		});
		
		function ResetItemsOnSACreateClear(){
			jQuery("#SACreateFromItemCode").val('');
			jQuery("#SACreateFromItemDesc").val('');
			jQuery("#SACreateFromBatchNo").val('');
			jQuery("#SACreateFromWeight").val('');
			
			jQuery("#SACreateToItemCode").val('');
			jQuery("#SACreateToItemCode").css("background","white");
			jQuery("#SACreateToItemDesc").val('');
			jQuery("#SACreateToBatchNo").val('');
			jQuery("#SACreateToWeight").val('');
			
			
			jQuery("#SACreateAllocatedQty").val('');
			jQuery("#SACreateApprovedQty").val('');
			jQuery("#SACreateInitiatedQty").val('');
			jQuery("#SACreateAvailableQty").val('');
			jQuery("#SACreateReason").val('');
			
		}
		
		
		function changeOnLocationChange(){
			jQuery("#SACreateLocationCode").attr('disabled',true);
			
			jQuery("#SACreateLocationAddress").attr('disabled',true);
			
			jQuery("#SACreateAdjustmentNo").attr('disabled',true);
			jQuery("#SACreateAdjustmentNo").val('');
			jQuery("#SACreateInitiatedBy").attr('disabled',true);
			jQuery("#SACreateInitiatedBy").val('');
			jQuery("#SACreateApprovedRejectedBy").attr('disabled',true);
			jQuery("#SACreateApprovedRejectedBy").val('');
			jQuery("#SACreateStatus").attr('disabled',true);
			jQuery("#SACreateStatus").val('New');
			jQuery("#SACreateFromItemDesc").attr('disabled',true);
			jQuery("#SACreateFromItemDesc").val('');
			jQuery("#SACreateFromWeight").attr('disabled',true);
			jQuery("#SACreateFromWeight").val('');
			jQuery("#SACreateToItemDesc").attr('disabled',true);
			jQuery("#SACreateToItemDesc").val('');
			jQuery("#SACreateToWeight").attr('disabled',true);
			jQuery("#SACreateToWeight").val('');
			jQuery("#SACreateApprovedQty").attr('disabled',true);
			jQuery("#SACreateApprovedQty").val('');
			jQuery("#SACreateInitiatedQty").attr('disabled',true);
			jQuery("#SACreateInitiatedQty").val('');
			jQuery("#btnINV03Reject").attr('disabled',true);
			jQuery("#btnPrint").attr('disabled',true);
			jQuery("#btnINV03Cancel").attr('disabled',true);
			jQuery("#btnReset").attr('disabled',false);
			jQuery("#btnINV03Save").attr('disabled',true);
			jQuery("#btnINV03Approve").attr('disabled',true);
			jQuery("#btnINV03Initiate").attr('disabled',false);
			jQuery("#SACreateFromBucket").val(5);
			ResetItemsOnSACreateClear();
			jQuery("#TOIItemGrid").jqGrid("clearGridData");
			
			actionButtonsStauts();
			
		}
		
		jQuery("#btnCreateReset").click(function(){
		
			disableOnSatrtup();
			ResetItemsOnSACreateClear();
			jQuery("#TOIItemGrid").jqGrid("clearGridData");
			jQuery("#SACreateExportItem").attr('checked',false); 
			jQuery("#SABatchAdjust").attr('checked',false); 
			enableDisableButtonOnStatus(0);

		});
	  function enableDisableButtonOnStatus(status){
		  
		  if(status==0)  //reset
			{
				jQuery("#btnINV03Reject").attr('disabled',true);
				jQuery("#btnINV03Print").attr('disabled',true);
				jQuery("#btnINV03Cancel").attr('disabled',true);
				jQuery("#btnReset").attr('disabled',false);
				jQuery("#btnINV03Save").attr('disabled',true);
				jQuery("#btnINV03Approve").attr('disabled',true);
				jQuery("#btnINV03Initiate").attr('disabled',false);
				
				jQuery("#SACreateInitialDate").attr('disabled',false);
				
				jQuery("#SACreateInitialDate").attr('disabled',true);
				
				//jQuery("#SACreateInitialDate").css('background','white');
				 jQuery("#SACreateExportItem").attr('disabled',false);
				 jQuery("#SABatchAdjust").attr('disabled',false);
				 disableEnableFieldOnStatus(0);
			} 
			if(status==1)  //created
			{
				jQuery("#btnINV03Reject").attr('disabled',true);
				jQuery("#btnINV03Print").attr('disabled',true);
				jQuery("#btnINV03Cancel").attr('disabled',false);
				jQuery("#btnReset").attr('disabled',false);
				jQuery("#btnINV03Save").attr('disabled',true);
				jQuery("#btnINV03Approve").attr('disabled',true);
				jQuery("#btnINV03Initiate").attr('disabled',false);
				jQuery("#SACreateInitialDate").attr('disabled',false);
				jQuery("#SACreateInitialDate").attr('disabled',true);
				 jQuery("#SACreateExportItem").attr('disabled',true);
				 jQuery("#SABatchAdjust").attr('disabled',true);
				 disableEnableFieldOnStatus(0);
			} 
			if(status==2)  //initiated
			{
				jQuery("#btnINV03Reject").attr('disabled',false);
				jQuery("#btnINV03Print").attr('disabled',true);
				jQuery("#btnINV03Cancel").attr('disabled',true);
				jQuery("#btnReset").attr('disabled',false);
				jQuery("#btnINV03Save").attr('disabled',true);
				jQuery("#btnINV03Approve").attr('disabled',false);
				jQuery("#btnINV03Initiate").attr('disabled',true);		
				jQuery("#SACreateInitialDate").attr('disabled',false);
				jQuery("#SACreateInitialDate").attr('disabled',true);
				 jQuery("#SACreateExportItem").attr('disabled',true);
				 jQuery("#SABatchAdjust").attr('disabled',true);
				 disableEnableFieldOnStatus(0);
			} 
			if(status==3)  // Approved
			{
				jQuery("#btnINV03Reject").attr('disabled',true);
				jQuery("#btnINV03Print").attr('disabled',false);
				jQuery("#btnINV03Cancel").attr('disabled',true);
				jQuery("#btnReset").attr('disabled',false);
				jQuery("#btnINV03Save").attr('disabled',true);
				jQuery("#btnINV03Approve").attr('disabled',true);
				jQuery("#btnINV03Initiate").attr('disabled',true);	
				 jQuery("#SACreateExportItem").attr('disabled',true);
				jQuery("#SACreateInitialDate").attr('disabled',true);
				 jQuery("#SABatchAdjust").attr('disabled',true);
				 disableEnableFieldOnStatus(1);
			} 
			if(status==4)  //rejected
			{
				jQuery("#btnINV03Reject").attr('disabled',true);
				jQuery("#btnINV03Print").attr('disabled',true);
				jQuery("#btnINV03Cancel").attr('disabled',true);
				jQuery("#btnReset").attr('disabled',false);
				jQuery("#btnINV03Save").attr('disabled',true);
				jQuery("#btnINV03Approve").attr('disabled',true);
				jQuery("#btnINV03Initiate").attr('disabled',true);	
				jQuery("#SACreateInitialDate").attr('disabled',true);
				 jQuery("#SACreateExportItem").attr('disabled',true);
				 jQuery("#SABatchAdjust").attr('disabled',true);
				 disableEnableFieldOnStatus(1);
			} 
			if(status==5)  //canceled
			{
				jQuery("#btnINV03Reject").attr('disabled',true);
				jQuery("#btnINV03Print").attr('disabled',true);
				jQuery("#btnINV03Cancel").attr('disabled',true);
				jQuery("#btnReset").attr('disabled',false);
				jQuery("#btnINV03Save").attr('disabled',true);
				jQuery("#btnINV03Approve").attr('disabled',true);
				jQuery("#btnINV03Initiate").attr('disabled',true);
				jQuery("#SACreateInitialDate").attr('disabled',true);
				 jQuery("#SACreateExportItem").attr('disabled',true);
				 jQuery("#SABatchAdjust").attr('disabled',true);
				 disableEnableFieldOnStatus(1);
			} 
			
			actionButtonsStauts();
		
		 }
		
	  function disableEnableFieldOnStatus(IsDisable){
			if(IsDisable==1)
			{
				jQuery("#SACreateFromBatchNo").attr('disabled',true);
				jQuery("#SACreateAllocatedQty").attr('disabled',true);
				jQuery("#SACreateAllocatedQty").attr('disabled',true);
				jQuery("#SACreateFromItemCode").attr('disabled',true);
				jQuery("#SACreateAdd").attr('disabled',true);
				jQuery("#SACreateReason").attr('disabled',true);
			}		
			else{
				if(jQuery("#SACreateExportItem").is(':checked')==true){
					jQuery("#SACreateToItemCode").css("background","white");
					//jQuery("#SACreateToBatchNo").css("background","white");
					jQuery("#SACreateToItemCode").attr('disabled',false);
					jQuery("#SACreateToItemCode").val('');
				}
				jQuery("#SACreateFromBatchNo").attr('disabled',false);
				jQuery("#SACreateAllocatedQty").attr('disabled',false);
				jQuery("#SACreateFromItemCode").attr('disabled',false);
				jQuery("#SACreateFromItemCode").css("background","white");
				jQuery("#SACreateFromBatchNo").css("background","white");
				jQuery("#SACreateAdd").attr('disabled',false);
				
				jQuery("#SACreateReason").attr('disabled',false);
			}
			isPrintButtonDisableOrNot() ;
				
			}
	  
		function isPrintButtonDisableOrNot(){
			if(jQuery("#SABatchAdjust").is(':checked')){
				jQuery("#btnINV03Print").attr('disabled',true);
			}
			else{
				jQuery("#btnINV03Print").attr('disabled',false);
			}
			
		}
	  function searchBatchUsingCode(){
		  
		  var locationId=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationId');
	    	var itemId=fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId');
	    	bucketId=  jQuery("#SACreateFromBucket").val() ;
	    	SACreateFromBatchNo=jQuery("#manufactureBatchNo").val() ;
	    	var rowid=0;
		  showMessage("loading", "Searching Batches...", "");
		   jQuery.ajax({
			   type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchBatchUsingCode",
					bNo:SACreateFromBatchNo,
					locationId:locationId,
					itemId:itemId,
					bucketId:bucketId
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					jQuery("#SACreateFromBatchNo").css("background","white");
					 resultArrData = jQuery.parseJSON(data);
			if(resultArrData.Status==1){
				bucketBatchLocWiseQty=resultArrData.Result;
				resultArr=resultArrData.Result;
				if(resultArr.length==1){
					
				}
				if(resultArr.length>=1){
							jQuery.each(resultArr,function(key,value)
							{
								var newData = [{
									"ManufacturingCode":resultArr[key]['ManufacturingCode'],
									"ItemName":resultArr[key]['LocationName'],
									"ItemCode":resultArr[key]['InitiatedDate'],
									"MRP":resultArr[key]['MRP'],
									"Quantity":resultArr[key]['Quantity'],
									"Weight":resultArr[key]['Weight'],
									"UOMId":resultArr[key]['UOMId'],
									"MfgDate":resultArr[key]['MfgDate'],
									"ExpDate":resultArr[key]['ExpDate'],
									"UOMName":resultArr[key]['UOMName'],
									"BatchNo":resultArr[key]['BatchNo'],
											
								}];
								
								for (var i=0;i<newData.length;i++) {
								jQuery("#manufactureBatchSearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
								rowid=rowid+1;
								}
							 
							});
				   }
				else {
					
			        showMessage("yellow","No Record Found.","");
				}
			}
			else{
				showMessage('red',resultArrData.Description,'');
			}
				
			  },
			  error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		} 
 function searchSinglemfgToBatchCode(){
		  
		  var locationId=fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#SACreateLocation").val(),'LocationId');
	    	var itemId=fetchvaluesFromJsonOnSelectRow(jsonForToItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId');
	    	bucketId=  jQuery("#SACreateFromBucket").val() ;
	    	SACreateToBatchNo=jQuery("#SACreateToBatchNo").val() ;
	    	var rowid=0;
		  showMessage("loading", "Searching Batches...", "");
		   jQuery.ajax({
			   type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"searchToBatchUsingCode",
					bNo:SACreateToBatchNo,
					locationId:locationId,
					itemId:itemId,
					bucketId:bucketId,
					type:1
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					jQuery("#SACreateFromBatchNo").css("background","white");
					 resultArrData = jQuery.parseJSON(data);
			if(resultArrData.Status==1){
				bucketBatchLocWiseQty=resultArrData.Result;
				resultArr=resultArrData.Result;
				if(resultArr.length==1){
					bucketBatchLocWiseQty=resultArr;
					 jQuery.each(resultArr,function(key,value){
		            		jQuery("#SACreateAvailableQty").val(parseFloat(resultArr[key]['Quantity'],10).toFixed(0));
		            		jQuery("#SACreateAllocatedQty").focus();
		            		
					 });
				}
			else if(resultArr.length>1){
				 showMessage("yellow","Multiple valid batches found. Use lookup to select one.","");
				}
				else {
					
			        showMessage("yellow","No Record Found.","");
				}
			}
			else{
				showMessage('red',resultArrData.Description,'');
			}
				
			  },
			  error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		} 
	
	   
	  jQuery("#SACreateFromBucket").change(function(e){
		if(  jQuery("#SACreateFromBucket").val()!=-1){   
		var availQty=  fetchvaluesFromJsonOnSelectRow(bucketBatchLocWiseQty,'BucketId',jQuery("#SACreateFromBucket").val(),'Quantity') ;
		jQuery("#SACreateAvailableQty").val(parseInt(availQty));
		}
		});
	  
	  
	  jQuery("#SACreateAllocatedQty").keyup(function(event){	
   	   var intRegex = /^\d+$/;
   	    jQuery("#SACreateAllocatedQty").css("background","white");
	    if(  intRegex.test(jQuery("#SACreateAllocatedQty").val())){
					 if(parseInt(jQuery("#SACreateAvailableQty").val())>=parseInt(jQuery("#SACreateAllocatedQty").val())){
					
					 }
					 else{
						 showMessage("yellow","Cannot Adjust Quantity more than Availble Quantity.","");
						 jQuery("#SACreateAllocatedQty").select();
						 jQuery("#SACreateAllocatedQty").focus();
					 }
				 }
		 else{
			 jQuery("#SACreateAllocatedQty").select();
			 jQuery("#SACreateAllocatedQty").focus();
			 showMessage("yellow","Allocated Quantity should be numeric.");
		 }

		});
	  function matchItemsInJQGridUsingTOColumn2(gridName,columnName,columnValue,columnName2,columnValue2){
			var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
			var gridRowsForColumn2 = jQuery("#"+gridName).jqGrid('getCol',columnName2,false);
		   var found=0;
			for(var i=0;i<gridRowsForColumn.length;i++)
			{

				if(columnValue == gridRowsForColumn[i] && gridRowsForColumn2[i]==columnValue2)
				{
					if(jQuery("#SABatchAdjust").is(':checked')){
						return 0 ;
					}
					found = 1;
					break;
				}
				else 
					{
					
					found=0;
					}
//				pv_sum+= parseFloat(PV[i]);
			}
			return found;
		}
	  /*
	  ['Item Code','Item Name','UOM','FromBucket','ToBucket','Quantity','Reason Code','ApprovedQty',
       'BatchNo'],*/
	  				var itemRowId =0;
	jQuery("#SACreateAdd").click(function(){
		//this is for export items and internal stock adjusent
		//if(jQuery("#SABatchAdjust").is(':checked')==false)
		var toItemId  ;
				if(jQuery("#SACreateExportItem").is(':checked'))
				{
					toItemId= fetchvaluesFromJsonOnSelectRow(jsonForToItems,'ItemCode',jQuery("#SACreateToItemCode").val(),'ItemId') ;
				}
				else if (jQuery("#SABatchAdjust").is(':checked')){
					toItemId=fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId') ;
				}
				else{
					toItemId=   0 ;
				}
						var status=0;
							if(jQuery("#SACreateStatus").val()=='New'){
								status=0;
							}
							if(jQuery("#SACreateStatus").val()=='Initiated'){
								status=1;
							}
							if(jQuery("#SACreateStatus").val()=='Approved'){
								status=2;
							}
							if(jQuery("#SACreateStatus").val()=='Canceled'){
								status=3;
							}
							if(jQuery("#SACreateStatus").val()=='Rejected'){
								status=4;
							}
							
					 if(vallidationOnAdd()==1){
						 var itemID ;
						 var mfgDate ;
						 var expdate ;
						 var batchId ;
						 var UOMName ;
						 var UOMId ;
						var result=	matchItemsInJQGridUsingTOColumn2("TOIItemGrid",'ItemCode',jQuery("#SACreateFromItemCode").val(),
								'BatchNo',jQuery("#SACreateFromBatchNo").val());
							if(result==0){
								var id=jQuery("#manufactureBatchSearchGrid").getGridParam('selrow') ;
							if(id!=undefined){
								 
								 mfgDate=getDate(jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id,'MfgDate'));
								 expdate=getDate(jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id,'ExpDate'));
							    //batchId=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id,'BatchNo');
								 batchId=fromBatchNo ;
							    UOMName	=jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'UOMName');
							    UOMId=	jQuery("#manufactureBatchSearchGrid").jqGrid('getCell', id, 'UOMId');
							}
							else {
								 
								 mfgDate=(bucketBatchLocWiseQty[0].MfgDate);
								 expdate=(bucketBatchLocWiseQty[0].ExpDate);
							    //batchId=(bucketBatchLocWiseQty[0].BatchNo);
								batchId=fromBatchNo ;
							    UOMName	=(bucketBatchLocWiseQty[0].UOMName);
							    UOMId=	(bucketBatchLocWiseQty[0].UOMId);
							}
								var newData = [{
								"ItemId":fetchvaluesFromJsonOnSelectRow(jsonForFromItems,'ItemCode',jQuery("#SACreateFromItemCode").val(),'ItemId') ,
								"ItemCode":jQuery("#SACreateFromItemCode").val(),
								"ItemName":jQuery("#SACreateFromItemDesc").val(),
								"UOM":UOMName,
								"FromBucketName":jQuery('select[name=SACreateFromBucket] option:selected').text() ,
								"ToBucketName":jQuery('select[name=SACreateToBucket] option:selected').text() ,
								"Quantity":jQuery("#SACreateAllocatedQty").val(),
								"ReasonCode":jQuery('select[name=SACreateReasonCode] option:selected').text() ,
								"ApprovedQty":jQuery("#SACreateAllocatedQty").val(),
								"BatchNo":jQuery("#SACreateFromBatchNo").val(),
								"UOMId":UOMId,
								"FromBucketId":jQuery("#SACreateFromBucket").val(),
								"ToBucketId":jQuery("#SACreateToBucket").val(),
								"ReasonId":jQuery("#SACreateReasonCode").val(),
								"MfgDate":mfgDate,
								"ExpDate":expdate,
								"BatchId":batchId,
								"ToItemid":toItemId ,
								"ToItemCode":jQuery("#SACreateExportItem").is(':checked')?jQuery("#SACreateToItemCode").val():'',
						           "ToBatchNo":jQuery("#SACreateExportItem").is(':checked')?batchId:toBatchNo,
						           "ToManufactureBatchNo":jQuery("#SACreateExportItem").is(':checked')?jQuery("#SACreateFromBatchNo").val():'',
						           "ToItemName":jQuery("#SACreateExportItem").is(':checked')?jQuery("#SACreateFromItemDesc").val():''
							}];
							
							for (var i=0;i<newData.length;i++) {
							jQuery("#TOIItemGrid").jqGrid('addRowData',itemRowId, newData[newData.length-i-1], "first");
							itemRowId=itemRowId+1;
							}
							fromBatchNo=='' ;
							toBatchNo=='' ;
							disableHeaderLevelInfo();
							ResetItemsOnSACreateClear();
							}
							else {
								showMessage("yellow","This item already exists in list.","");
							}
					      }
		 
		
		
		
		});
	
	function disableHeaderLevelInfo(){
		jQuery("#SACreateFromBucket").attr('disabled',true) ;
		jQuery("#SACreateToBucket").attr('disabled',true) ;
	   jQuery("#SACreateReasonCode").attr('disabled',true);
	   jQuery("#SACreateLocation").attr('disabled',true);
	 
	   backGroundColorForDisableField();
	   jQuery("#SACreateExportItem").attr('disabled',true);
	}
		
	jQuery("#btnINV03Save").unbind("click").click(function(){
				 var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
					order_json_string = JSON.stringify(oOrderJson);
		if( vallidationOnSave()==1 )
	         {
			
				var r=confirm("Are you sure you want to save data ?");
	   if(r==true){
		   showMessage("loading", "Saving stock adjustment...", "");
				   jQuery.ajax({
					   type:'POST',
					url:Drupal.settings.basePath + 'StockAdjustmentCallback',
					data:
					{
						
						id:"saveSA",
						moduleCode:"INV03",
						functionName:"Save",
						InitiatedDate:jQuery("#SACreateInitialDate").val(),
						jsonForItems:order_json_string,
						SourceLocationId:jQuery("#SACreateLocation").val(),
						StatusId:1,
						tempIsInternalBatchAdj:'true',
						SeqNo:jQuery("#SACreateAdjustmentNo").val(),
						approvedBy:'',
						isExported:jQuery("#SACreateExportItem").is(':checked')||jQuery("#SABatchAdjust").is(':checked')? 1 : 0,
					},
	
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							resultsData = jQuery.parseJSON(data);
						if(resultsData.Status==1){
								results=resultsData.Result;
							 jQuery.each(results,function(key,value)
									   {	
								        showMessage("green","Record Created Successfully. "+ results[key]['SeqNo'] +" .","");
								
								        jQuery("#SACreateAdjustmentNo").val( results[key]['SeqNo'])	;
								        enableDisableButtonOnStatus(1);
								        ResetItemsOnSACreateClear();
								        
								      });
						}
						else{
							showMessage('yellow',resultsData.Description,'');
						}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					
				
				     });
			    	}
				
		       }
			
			});	
			
			jQuery("#btnINV03Initiate").unbind("click").click(function(){
				 var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
					order_json_string = JSON.stringify(oOrderJson);
						jQuery("#btnINV03Initiate").attr('disabled',true);
	if( vallidationOnSave()==1 )
	         {
			
				var r=confirm("Are you sure you want to Initiate data ?");
	   if(r==true){
		   showMessage("loading", "Initiating stock adjustment...", "");
				   jQuery.ajax({
					   type:'POST',
					url:Drupal.settings.basePath + 'StockAdjustmentCallback',
					data:
					{
						id:"saveSA",
						moduleCode:"INV03",
						functionName:"Initiate",
						InitiatedDate:jQuery("#SACreateInitialDate").val(),
						jsonForItems:order_json_string,
						SourceLocationId:jQuery("#SACreateLocation").val(),
						StatusId:2,
						tempIsInternalBatchAdj:'true',
						SeqNo:jQuery("#SACreateAdjustmentNo").val(),
						approvedBy:'',
						isExported:jQuery("#SACreateExportItem").is(':checked')||jQuery("#SABatchAdjust").is(':checked')? 1 : 0,
					},
	
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						results=resultsData.Result;
							 jQuery.each(results,function(key,value)
									   {	
								jQuery("#SACreateStatus").val('Initiated'); 
								 showMessage("green","Record Initiated Successfully "+ results[key]['SeqNo'] +" .","");
								        jQuery("#SACreateAdjustmentNo").val( results[key]['SeqNo'])	;
								        enableDisableButtonOnStatus(2);
								      });
							 jQuery("#btnINV03Initiate").removeAttr("disabled");
						}
					else{
						showMessage('yellow',resultsData.Description,'');
						jQuery("#btnINV03Initiate").removeAttr("disabled");
					}
					
				   },
				   error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				     });
			    	}
				
		       }
			
			});	
	
			
			jQuery("#btnINV03Approve").unbind("click").click(function(){
				 var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
					order_json_string = JSON.stringify(oOrderJson);
	if( vallidationOnSave()==1 )
	         {
			
				var r=confirm("Are you sure you want to approve data ?");
	   if(r==true){
		   showMessage("loading", "Approving stock adjusment...", "");
				   jQuery.ajax({
					   type:'POST',
					url:Drupal.settings.basePath + 'StockAdjustmentCallback',
					data:
					{
						id:"saveSA",
						moduleCode:"INV03",
						functionName:"Approve",
						InitiatedDate:jQuery("#SACreateInitialDate").val(),
						jsonForItems:order_json_string,
						SourceLocationId:jQuery("#SACreateLocation").val(),
						StatusId:3,
						tempIsInternalBatchAdj:'true',
						SeqNo:jQuery("#SACreateAdjustmentNo").val(),
						approvedBy:1,
						isExported:jQuery("#SACreateExportItem").is(':checked')||jQuery("#SABatchAdjust").is(':checked')? 1 : 0,
					},
	
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							resultsData = jQuery.parseJSON(data);
						if(resultsData.Status==1){
							results=resultsData.Result;
							 jQuery.each(results,function(key,value)
									   {
								 jQuery("#SACreateStatus").val('Approved'); 
								 showMessage("green","Record Approve Successfully For "+ results[key]['SeqNo'] +" .","");
								        jQuery("#SACreateAdjustmentNo").val( results[key]['SeqNo'])	;
								        enableDisableButtonOnStatus(3);
								      });
							
						}
						else{
							showMessage('yellow',resultsData.Description,'');
						}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
				
				     });
			    	}
				
		       }
			
			});	
	
			jQuery("#btnINV03Reject").unbind("click").click(function(){
				 var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
					order_json_string = JSON.stringify(oOrderJson);
	if( vallidationOnSave()==1 )
	         {
			
				var r=confirm("Are you sure you want to Reject data ?");
	   if(r==true){
		   showMessage("loading", "Rejecting stock adjusment...", "");
				   jQuery.ajax({
					   type:'POST',
					url:Drupal.settings.basePath + 'StockAdjustmentCallback',
					data:
					{
						
						id:"saveSA",
						moduleCode:"INV03",
						functionName:"Reject",
						InitiatedDate:jQuery("#SACreateInitialDate").val(),
						jsonForItems:order_json_string,
						SourceLocationId:jQuery("#SACreateLocation").val(),
						StatusId:4,
						tempIsInternalBatchAdj:'true',
						SeqNo:jQuery("#SACreateAdjustmentNo").val(),
						approvedBy:1,
						isExported:jQuery("#SACreateExportItem").is(':checked')||jQuery("#SABatchAdjust").is(':checked')? 1 : 0,
					},
	
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							resultsData = jQuery.parseJSON(data);
						if(resultsData.Status==1){
							results=resultsData.Result;
							 jQuery.each(results,function(key,value)
									   {	
								 showMessage("green","Record Rejected Successfully  "+ results[key]['SeqNo'] +" .","");
								        jQuery("#SACreateAdjustmentNo").val( results[key]['SeqNo'])	;
								        enableDisableButtonOnStatus(4);
								      });
							
						}
						else{
							showMessage('red',resultsData.Description,'');
						}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
				
				     });
			    	}
				
		       }
			
			});	
			jQuery("#btnINV03Cancel").unbind("click").click(function(){
				var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
				order_json_string = JSON.stringify(oOrderJson);
				if( vallidationOnSave()==1 )
				{

					var r=confirm("Are you sure you want to Reject data ?");
					if(r==true){
						showMessage("loading", "Rejecting stock adjusment...", "");
						jQuery.ajax({
							type:'POST',
							url:Drupal.settings.basePath + 'StockAdjustmentCallback',
							data:
							{

								id:"saveSA",
								moduleCode:"INV03",
								functionName:"Reject",
								InitiatedDate:jQuery("#SACreateInitialDate").val(),
								jsonForItems:order_json_string,
								SourceLocationId:jQuery("#SACreateLocation").val(),
								StatusId:4,
								tempIsInternalBatchAdj:'true',
								SeqNo:jQuery("#SACreateAdjustmentNo").val(),
								approvedBy:1,
								isExported:jQuery("#SACreateExportItem").is(':checked')||jQuery("#SABatchAdjust").is(':checked')? 1 : 0,
							},

							success:function(data)
							{
								jQuery(".ajaxLoading").hide();
								resultsData = jQuery.parseJSON(data);
								if(resultsData.Status==1){
									results=resultsData.Result;
									jQuery.each(results,function(key,value)
											{	
										showMessage("green","Record Rejected Successfully  "+ results[key]['SeqNo'] +" .","");
										jQuery("#SACreateAdjustmentNo").val( results[key]['SeqNo'])	;
										enableDisableButtonOnStatus(4);
											});

								}
								else{
									showMessage('red',resultsData.Description,'');
								}
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) { 

								jQuery(".ajaxLoading").hide();
								var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
								showMessage("", defaultMessage, "");
							}

						});
					}

				}

			});	
	
	function	vallidationOnSave(){
				if(jQuery("#SACreateLocation").val()==0){
					jQuery("#SACreateLocation").css("background","#FF9999");
					showMessage("","Select valid price mode","");
					return 0;
				}
				   var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
					order_json_string = JSON.stringify(oOrderJson);
		         if( jQuery.parseJSON(order_json_string).length==0){
		        	 showMessage('yellow',"Select at least one Item","");
		        	 return 0;
		         }
		           
		             return 1;
			}
	function vallidationOnAdd(){
		
		if(jQuery("#SACreateFromItemCode").val()==''){
			jQuery("#SACreateFromItemCode").css("background","#FF9999");
			showMessage("yellow","Enter valid Item Code","" );
			jQuery("#SACreateFromItemCode").focus();
			return 0;
		}
		 if(jQuery("#SACreateFromItemDesc").val()==''){
			jQuery("#SACreateFromItemDesc").css("background","#FF9999");
			showMessage("yellow","Enter Name. Press Tab on Item Count","");
			jQuery("#SACreateFromItemCode").focus();

			return 0;
		}
		 if(jQuery("#SACreateFromBatchNo").val()==''){
			jQuery("SACreateFromBatchNo").css("background","#FF9999");
			showMessage("yellow","Enter valid Batch No","");
			jQuery("SACreateFromBatchNo").focus();
			return 0;
		}
		 if(jQuery("#SACreateFromBucket").val()==-1){
				jQuery("#SACreateFromBucket").css("background","#FF9999");
				
				showMessage("yellow","Select From Bucket","");
				jQuery("#SACreateFromBucket").focus();
				return 0;
	        }
		 if(jQuery("#SACreateToBucket").val()==-1){
			jQuery("#SACreateToBucket").css("background","#FF9999");
			
			showMessage("yellow","select ToBucket","");
			jQuery("#SACreateToBucket").focus();
			return 0;
        }
		 if(fromBatchNo==''){
				
				showMessage("yellow","Error in searching FromBatchNo","");
				
				return 0;
	        }
		 if(toBatchNo==''  && ( jQuery("#SABatchAdjust").is(':checked')==true)){
			 showMessage("yellow","Error in searching TobatchNo","");	
				return 0;
	        }
		 if(jQuery("#SACreateToBucket").val()==jQuery("#SACreateFromBucket").val() && (jQuery("#SACreateExportItem").is(':checked')==false && jQuery("#SABatchAdjust").is(':checked')==false))
			{
				showMessage("yellow","From bucket can't be same as To bucket","");
				jQuery("#SACreateToBucket").focus();

				return 0;
			}
	   if(jQuery("#SACreateReasonCode").val()==-1){
			jQuery("#SACreateReasonCode").css("background","#FF9999");
			showMessage("yellow","Select reason ","");
			jQuery("#SACreateReasonCode").focus();
			return 0;
			
        }
		 if(jQuery("#SACreateAvailableQty").val()==''){
				jQuery("#SACreateAvailableQty").css("background","#FF9999");
				showMessage("yellow","Available Quantity Not Found! Press Tab after entering valid batch no.","");
				jQuery("SACreateFromBatchNo").focus();
				
				return 0;
	        }
   
		 if(jQuery("#SACreateAllocatedQty").val()==''){
				jQuery("#SACreateAllocatedQty").css("background","#FF9999");
				showMessage("yellow","Enter Allocated Quantity","");
				jQuery("#SACreateAllocatedQty").focus();
				
				return 0;
	        }
		 if(parseInt(jQuery("#SACreateAllocatedQty").val())<=0){
				jQuery("#SACreateAllocatedQty").css("background","#FF9999");
				showMessage("yellow","Enter valid Allocated Quantity","");
				jQuery("#SACreateAllocatedQty").focus();
				
				return 0;
	        }
		 if(parseInt(jQuery("#SACreateAllocatedQty").val())>parseInt(jQuery("#SACreateAvailableQty").val())){
				jQuery("#SACreateAllocatedQty").css("background","#FF9999");
				showMessage("yellow","Enter valid Allocated Quantity","");
				jQuery("#SACreateAllocatedQty").focus();
				
				return 0;
	        }
		 if(jQuery("#SACreateExportItem").is(':checked')==true){
			 if(jQuery("#SACreateToItemCode").val()==''){

					jQuery("#SACreateFromItemCode").css("background","#FF9999");
					showMessage("yellow","Enter valid Item Code","");
					jQuery("#SACreateFromItemCode").select();
					jQuery("#SACreateFromItemCode").focus();

					return 0;
				}
				 if(jQuery("#SACreateToItemDesc").val()==''){

					jQuery("#SACreateFromItemDesc").css("background","#FF9999");
					showMessage("yellow","Item Name Not Found! Press Tab on Item Code","");
					jQuery("#SACreateFromItemCode").focus();

					return 0;
				}
		 }

         return 1;

	}

	
	
	jQuery("#SACreateReasonCode").change(function(){
		jQuery("#SACreateReasonCode").css("background","white");
		
	});
	jQuery("#SACreateFromBucket").change(function(){
		jQuery("#SACreateFromBucket").css("background","white");
		
	});
	jQuery("#SACreateToBucket").change(function(){
		jQuery("#SACreateToBucket").css("background","white");
		
	});
	

	  jQuery("#btnReset").click(function (){
		  
		 jQuery("#seqNo").val('');
		 jQuery("#status").val(-1);
		 jQuery("#users").val(-1);
		 jQuery( ".showCalender" ).datepicker("setDate",new Date());
		 
	  });
	  
	/*  jQuery("#btnINV03Print").click(function(){
			var arr = {'locationId':10,'adjustmentNo':jQuery("#SACreateAdjustmentNo").val()};
			showReport('reports/SAInvoice.rptdesign',arr);
			});*/
	  
		jQuery("#btnINV03Print").click(function(){
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'StockAdjustmentCallback',
				data:
				{
					id:"printSA",
					adjustmentNo:jQuery("#SACreateAdjustmentNo").val(),
					functionName:'Approve',
					moduleCode:"INV03"
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var resultsData = jQuery.parseJSON(data);
				if(resultsData.Status==1){
					var results=resultsData.Result;
					 jQuery.each(results,function(key,value)
							   {
						 			var arr = {'locationId':results[key]['locationId'],'adjustmentNo':results[key]['adjustmentNo']};
						 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
								 
						      });
				}
				else{
					
					showMessage('red',resultsData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});

		
		});

		
	  
});
