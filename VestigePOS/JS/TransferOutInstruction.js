var jsonForLocations='';
var jsonForItems;
var loggedInUserPrivileges;
jQuery(document).ready(function(){
	
	
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	});
	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	var moduleCode = jQuery("#moduleCode").val();

	
		jQuery(function() {
			//alert("dfadf");
			jQuery( "#DivDistributorInfoSearchTab" ).tabs(); // Create tabs using jquery ui.
			
		});


	jQuery("#DivDistributorInfoSearchTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
			
			var tabIndex = tab.index; 
			if(tabIndex == 0)
			{
				ResetAllItems();
			}
			else if(tabIndex == 1){
				
			}
			});
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
		data:
		{
			id:"StockCountModuleFuncHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);
			
			if(loggedInUserPrivilegesForStockCountData.Status == 1)
			{
				
				loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;
				
				if(loggedInUserPrivileges.length == 0)
				{
					jQuery(".TOICreateActionButtons").attr('disabled',true);
				}
				else
				{
					callOnStarup();
				}
				
				
			}
			else
			{
				showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
	function actionButtonsStauts()
	{
		jQuery(".TOICreateActionButtons").each(function(){
			
			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");
			
			if(loggedInUserPrivileges[extractedButtonId] == 1)
			{
				
				//jQuery("#"+buttonid).attr('disabled',false);
				
			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}
			
			jQuery("#btnReset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			
		});
	}
	
	
function callOnStarup(){

		
		searchWHLocation();
		searchBucket();
		TOIStatus();
		fillPriceStatus();
		fillPricePercentage();
		fillAppDpp();
		disableFieldOnStartup();
		showCalender();
		showCalender2();

	}

		/*             Search For Items isomg ItemCode
-----------------------------------------------------------------------------------------------------		
		*/
		jQuery("#txtItemCode").keydown(function(e){
			
			clearItemOnItemKeyDown();
		    if((e.which==9 || e.which == 13 ) && jQuery("#txtItemCode").val()!=''){
		    	jQuery("#txtItemCode").val((jQuery("#txtItemCode").val()).toUpperCase());
		    	e.preventDefault(); 
		    	var itemCode = jQuery("#txtItemCode").val();
		    	searchItemUsingCode(itemCode);        
		    }
		    if(e.which == 115) 
			{
		    	//jQuery("#txtItemCode").val((jQuery("#txtItemCode").val()).toUpperCase());
				var currentItemSearchId = this.id;
				//alert(currentItemSearchId);
				showMessage("loading", "Please wait...", "");
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"itemLookUp",
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						
						//alert(data);
						jQuery("#divLookUp").html(data);
						
						itemSearchGrid(currentItemSearchId);
						jQuery("#divLookUp" ).dialog( "open","Item Search");
						
						jQuery("#divLookUp").dialog('option', 'title', 'Item Search');
						
						itemLookUpData();
						
						/*----------Dialog for item search is there ----------------*/
						
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				
				});
			}
		

		});
		
		
		
		
		function 	searchItemUsingCode (itemCode){
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"searchItemUsingCode",
					itemCode:itemCode
					
				},
				success:function(data)
				{
				var itemDescData=	jQuery.parseJSON(data);
				
						if(itemDescData.Status==1){
									jsonForItems=itemDescData.Result;
									var itemDesc = itemDescData.Result;
									if(itemDesc.length!=0)
									{
									 jQuery.each(itemDesc,function(key,value)
									 {	
										jQuery("#txtUOM").val(itemDesc[key]['UOMName']);
										jQuery("#TOIItemWeight").val(itemDesc[key]['Weight']);
										jQuery("#txtItemDescription").val(itemDesc[key]['ItemName']);
										jQuery("#txtItemCode").css("background","white");
										jQuery("#txtUOM").css("background","white");
										jQuery("#txtItemDescription").css("background","white");
										jQuery("#TOIUnitQty").focus();
									  });
									 callCalculateTransferPrice();
									 jQuery("#bucket").val(-1);
									 jQuery("#createAvailableQty").attr('disabled',false);	
									 // code for automatic fill quantity for bucket On hand ;
									 findQuantityForBucket(jQuery("#txtItemCode").val(),jQuery("#createSourceLocation").val(),5); 
								    }
									else {
										
										jQuery("#txtItemCode").css("background","#FF9999");
								        showMessage("red","Enter valid Item Code .","");
								        jQuery("#txtItemCode").focus();
								        jQuery("#txtItemCode").select();
										return;
									}
									
						}
						else
							{
							 showMessage('red',itemDescData.Description,'');
						        jQuery("#txtItemCode").focus();
						        jQuery("#txtItemCode").select();

							}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			
		}
		
		function calculateTransferPrice(destinationLocationId,mode,itemCode,percentage,appDpp){
			showMessage("loading", "Calculating transfer price...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"calculateTP",
					itemCode:itemCode,
					mode:mode,
					destinationLocationId:destinationLocationId,
					percentage:percentage,
					appDpp:appDpp
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var itemDescData = jQuery.parseJSON(data);
					
			if(itemDescData.Status==1){
				var itemDesc=itemDescData.Result;
					if(itemDesc.length!=0)
					{
					 jQuery.each(itemDesc,function(key,value)
					 {	
						jQuery("#txtTransferPrice").val(parseFloat(itemDesc[key][''],10).toFixed(2));
						
						
					  });

				    	jQuery("#TOIItemAmount").val(parseFloat(jQuery("#TOIUnitQty").val()*jQuery("#txtTransferPrice").val(),10).toFixed(2));
				    }
					
				}
			else{
				showMessage('red',itemDescData.Description,'');
		        jQuery("#txtItemCode").focus();
		        jQuery("#txtItemCode").select();

			}
				
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
		
			
		}
		
		
		function callCalculateTransferPrice(){
			var appDpp=jQuery("#appDpp").val();
			var percentage=jQuery("#Percentage").val();
			var mode=jQuery("#pricemode").val();
			var itemCode=jQuery("#txtItemCode").val();
			var destinationId=jQuery("#createDestinationLocation").val();
			if(appDpp!=-1 && percentage!=-1 && mode!=-1 && itemCode!='' && destinationId!=-1){
				
				calculateTransferPrice(destinationId,mode,itemCode,percentage,appDpp);
			}
			else return;
		}
		
		
		
		jQuery("#bucket").change(function(){
			showMessage("loading", "Searching for quantity...", "");
			jQuery("#bucket").css("background","white");
			findQuantityForBucket(jQuery("#txtItemCode").val(),jQuery("#createSourceLocation").val(),jQuery("#bucket").val());		
		});
		
		function findQuantityForBucket(itemCode,location,bucket){
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"searchAvailQty",
					itemCode:itemCode,
					destinationLocationId:location,
					bucket:bucket

				},
				success:function(data)
				{
				
					jQuery(".ajaxLoading").hide();
					var itemDescData = jQuery.parseJSON(data);
					
				if(itemDescData.Status==1){
					var itemDesc=itemDescData.Result ;
					if(itemDesc.length!=0)
					{
						
					 jQuery.each(itemDesc,function(key,value)
					 {	
						jQuery("#TOIAvailableQty").val(parseFloat(itemDesc[key]['Quantity'],10).toFixed(0));
						jQuery("#TOIAvailableQty").attr('disabled',true);
					  });
						
				    }
					else{
						jQuery("#TOIAvailableQty").val('');
			             jQuery("#TOIAvailableQty").val('');
			             jQuery("#TOIUnitQty").val('');
			             jQuery("#TOIItemAmount").val('');	
					}
				}
				else
				{
					
					showMessage('red',itemDescData.Description,'');
			        jQuery("#txtItemCode").focus();
			        jQuery("#txtItemCode").select();

				}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	

		}

		
		
		jQuery("#Percentage").change(function(){
			callCalculateTransferPrice();
			jQuery("#Percentage").css("background","white");
		});
		jQuery("#appDpp").change(function(){
			callCalculateTransferPrice();
			jQuery("#appDpp").css("background","white");
		});
		jQuery("#pricemode").change(function(){
			callCalculateTransferPrice();
			jQuery("#pricemode").css("background","white");
		});
		jQuery("#txtUOM").change(function(){
			jQuery("#txtUOM").css("background","white");
		});
		jQuery("#txtItemCode").change(function(){
			jQuery("#txtItemCode").css("background","white");
		});
		
		
		jQuery("#txtTransferPrice").change(function(){	
			jQuery("#txtTransferPrice").css("background","white");
		});
		
	
		
		
		jQuery("#TOIAvailableQty").change(function(){
			jQuery("#bucket").css("background","white");
		});
		jQuery("#TOIItemAmount").change(function(){
			jQuery("#TOIItemAmount").css("background","white");
		});
		
		
		
		
		jQuery("#createSourceLocation2").focusout(function(){
			jQuery("#createSourceLocation").css("background","white");
			var value =fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#createSourceLocation").val(),'Address');
			jQuery("#createSourceAddress").val(value);
			jQuery("#createSourceAddress").attr('disabled',true);
			resetButtonAccordingToCombo();
			if(jQuery("#createDestinationLocation").val()==jQuery("#createSourceLocation").val() && jQuery("#createSourceLocation").val()!=0)
			{
				showMessage("red","Source location cannot be same as Destination location","");
			//	alert("SourceLocation can not same as DestinationLocation");
				jQuery("#createSourceLocation").val(-1);
				jQuery("#createSourceLocation").focus();
			}
		});
		jQuery("#createSourceLocation").change(function(){
			jQuery("#createSourceLocation").css("background","white");
			var value =fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#createSourceLocation").val(),'Address');
			jQuery("#createSourceAddress").val(value);
			jQuery("#createSourceAddress").attr('disabled',true);
			resetButtonAccordingToCombo();
			if(jQuery("#createDestinationLocation").val()==jQuery("#createSourceLocation").val())
			{
				showMessage("red","Source location cannot be same as Destination location","");
			//	alert("SourceLocation can not same as DestinationLocation");
				jQuery("#createSourceLocation").val(-1);
			}
		});
		
		jQuery("#createDestinationLocation2").focusout(function(){
			jQuery("#createDestinationLocation").css("background","white");
			var value =fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#createDestinationLocation").val(),'Address');
			jQuery("#createDestinationAddress").val(value);
			
			resetButtonAccordingToCombo();
			if(jQuery("#createDestinationLocation").val()==jQuery("#createSourceLocation").val() && jQuery("#createSourceLocation").val()!=0)
			{
				showMessage("Source location cannot be same as Destination location","");
				//alert("SourceLocation can not same as DestinationLocation");
				jQuery("#createDestinationLocation").val(-1);
				//jQuery("#createDestinationLocation").focus();
			}
			callCalculateTransferPrice();
		});
		jQuery("#createDestinationLocation").change(function(){
			jQuery("#createDestinationLocation").css("background","white");
			var value =fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#createDestinationLocation").val(),'Address');
			jQuery("#createDestinationAddress").val(value);
			
			resetButtonAccordingToCombo();
			if(jQuery("#createDestinationLocation").val()==jQuery("#createSourceLocation").val() && jQuery("#createSourceLocation").val()!=0)
			{
				showMessage("red","Source location cannot be same as Destination location","");
				//alert("SourceLocation can not same as DestinationLocation");
				jQuery("#createDestinationLocation").val(-1);
			}
			callCalculateTransferPrice();
		});
		jQuery("#createDestinationLocation2").keydown(function(e){
			  if(e.which == 115) 
				{
					var currentItemSearchId = this.id;
					//alert(currentItemSearchId);
					showMessage("loading", "Please wait...", "");
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'LookUpCallback',
						data:
						{
							id:"LocationSearchLookUp",
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							
							//alert(data);
							jQuery("#divLookUp").html(data);
							
							locationSearchGrid(currentItemSearchId,2);
							jQuery("#divLookUp" ).dialog( "open","Location Search");
							
							jQuery("#divLookUp").dialog('option', 'title', 'Location Search');
							
							locationsLookUpData();
							
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					
					});
				}
			


		});
		
		

		jQuery("#createSourceLocation2").keydown(function(e){
			
			  if(e.which == 115) 
				{
					var currentItemSearchId = this.id;
					//alert(currentItemSearchId);
					showMessage("loading", "Please wait...", "");
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'LookUpCallback',
						data:
						{
							id:"LocationSearchLookUp",
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							
							//alert(data);
							jQuery("#divLookUp").html(data);
							
							locationSearchGrid(currentItemSearchId,1);
							jQuery("#divLookUp" ).dialog( "open","Location Search");
							
							jQuery("#divLookUp").dialog('option', 'title', 'Location Search');
							
							locationsLookUpData();
							
							
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					
					});
				}
			
		

		});
       jQuery("#TOIUnitQty").keyup(function(event){	
    	   var intRegex = /^\d+$/;
    	
		 if(  intRegex.test(jQuery("#TOIUnitQty").val())){
			 
			 jQuery("#TOILineWeight").val(parseFloat(jQuery("#TOIUnitQty").val()*jQuery("#TOIItemWeight").val(),10).toFixed(2));

			 if(parseInt(jQuery("#TOIUnitQty").val())<=parseInt(jQuery("#TOIAvailableQty").val())){
		    	jQuery("#TOIItemAmount").val(parseFloat(jQuery("#TOIUnitQty").val()*jQuery("#txtTransferPrice").val(),10).toFixed(2));
				jQuery("#TOIUnitQty").css("background","white");
			 }
			 else{
				 showMessage("red","Unit Quantity cannot be greater then Available Quantity","");
				 //jQuery("#TOIUnitQty").val('');
				 jQuery("#TOIUnitQty").focus();
				 jQuery("#TOIUnitQty").select();
			 }
			 
		 }
		 else{
			 jQuery("#TOIUnitQty").val('');
			 showMessage("red","Unit Quantity should be numeric.");
			 jQuery("#TOIUnitQty").focus();

		 }

		});
		
       jQuery("#pricemode").change(function () 
    		   {
    	   var Compare =jQuery("#pricemode").val();
    	   if (Compare == "2")
    	   {
    		   jQuery("#appDpp").val("1");
    	   }
    	   else if(Compare == "1")
    		   {
    		   jQuery("#appDpp").val("2");
    		   }
    	   jQuery("#appDpp").attr('disabled',true);
    		   });
		
		jQuery("#btnAdd").click(function(){
			vallidationOnAdd();
			
			itemId=fetchvaluesFromJsonOnSelectRow(jsonForItems,"ItemCode",jQuery("#txtItemCode").val(),"ItemId");
			UOMId=fetchvaluesFromJsonOnSelectRow(jsonForItems,"ItemCode",jQuery("#txtItemCode").val(),"UOMId");
			
			if(vallidationOnAdd()==1){
				jQuery("#createDestinationAddress").attr('disabled',true);
			var result=	matchItemsInJQGrid("TOIItemGrid",'ItemCode',jQuery("#txtItemCode").val());
			if(result==0){
			var newData = [{"Bucketid":jQuery("#bucket").val(),"UOMId":UOMId,"ItemId":itemId,"ItemCode":jQuery("#txtItemCode").val(),
				"ItemName":jQuery("#txtItemDescription").val(),"UOM":jQuery("#txtUOM").val(),"UnitPrice":jQuery("#txtTransferPrice").val()
				,"Bucket":jQuery("#bucket option:selected").text(),"AvailableQty":jQuery("#TOIAvailableQty").val(),
				"UnitQty":parseFloat(jQuery("#TOIUnitQty").val(),10).toFixed(0),"TotalAmount":jQuery("#TOIItemAmount").val(),"TOINumber":"","rid":"",
				"LineWeight":parseFloat(jQuery("#TOILineWeight").val())}];
			for (var i=0;i<newData.length;i++) {
				jQuery("#TOIItemGrid").jqGrid('addRowData',1, newData[newData.length-i-1], "first");
				
		      }
			jQuery("#TOICreateTOIAmount").val(parseFloat(sumOfColumnValueJQGrid2('TOIItemGrid','TotalAmount'),10).toFixed(2));
			jQuery("#TOICreateTOIQuantity").val(parseFloat(sumOfColumnValueJQGrid('TOIItemGrid','UnitQty'),10).toFixed(0));
			jQuery("#TOICreatePOStatus").val(parseFloat(sumOfColumnValueJQGrid2('TOIItemGrid','LineWeight'),10).toFixed(2)/1000);
			clearItemBlock();
			disableField();
			jQuery("#txtItemCode").focus();
			
			
			}
			else{
				showMessage("red","This item already exists in list","");
				
			}
	    	}

		});
		

		
		jQuery("#btnClear").click(function(){
				
			clearItemBlock();
		});	
		function clearItemBlock(){
			 jQuery("#txtItemCode").val('');
			 jQuery("#txtUOM").val('');
			jQuery("#txtItemDescription").val('');
				 jQuery("#txtTransferPrice").val('');
				 jQuery("#TOIItemWeight").val('');
				 jQuery("#TOILineWeight").val('');	 
				 jQuery("#bucket").val(-1);
	             jQuery("#TOIAvailableQty").val('');
	             jQuery("#TOIUnitQty").val('');
	             jQuery("#TOIItemAmount").val('');	
	             jQuery("#TOIAvailableQty").css("background","white");	
		}
		function clearItemOnItemKeyDown(){
			
			 jQuery("#txtUOM").val('');
			jQuery("#txtItemDescription").val('');
				 jQuery("#txtTransferPrice").val('');
				 jQuery("#TOIItemWeight").val('');
				 jQuery("#TOILineWeight").val('');
				 jQuery("#bucket").val(-1);
	             jQuery("#TOIAvailableQty").val('');
	             jQuery("#TOIUnitQty").val('');
	             jQuery("#TOIItemAmount").val('');	
		}
		
		jQuery("#btnCreateReset").click(function(){
			ResetAllItems();
    		             
			
		});
		
		function ResetAllItems(){
			clearItemBlock();
			enableFields();
			disableFieldOnStartup();
			
			jQuery("#TOIItemGrid").jqGrid("clearGridData");
			 jQuery("#txtItemCode").val('');
			 jQuery("#txtUOM").val('');
			jQuery("#txtItemDescription").val('');
				 jQuery("#txtTransferPrice").val('');
				 jQuery("#TOIItemWeight").val('');
				 jQuery("#TOILineWeight").val('');
				 jQuery("#bucket").val(-1);
	             jQuery("#TOIAvailableQty").val('');
	             jQuery("#TOIUnitQty").val('');
	             jQuery("#TOIItemAmount").val('');		
	             jQuery("#TOICreateTOIQuantity").val('');		
	        		jQuery("#TOICreateTOIAmount").val('');		
	        		jQuery("#createSourceAddress").val('');		
	 			jQuery("#createDestinationAddress").val('');		
	        		jQuery("#TOICreatePOStatus").val('');		
	 			jQuery("#createSourceLocation").val(-1);		
	 			jQuery("#createDestinationLocation").val(-1);	
	 			jQuery("#createSourceLocation2").val('');		
	 			jQuery("#createDestinationLocation2").val('');	
	 			jQuery("#createDestinationLocation2").attr('disabled',false);
	 			jQuery("#createDestinationLocation2").attr('disabled',false);
	 			jQuery("#pricemode").val(-1);	
	 			jQuery("#TOICreateStatus").val('');	
	 			jQuery("#Indentised").val(-1);	
	 			jQuery("#TOICreateNumber").val('');	
	 			jQuery("#appDpp").val(-1);	
	 			jQuery("#Percentage").val(-1);	
	 			jQuery("#TOICreatePONumber").val('');	
	 			jQuery("#btnAdd").attr('disabled',false);
	 			
	 			backgroundColourWhite();
	
		}
	
		function resetButtonAccordingToCombo(){
			jQuery("#bucket").val(-1);
            jQuery("#TOIAvailableQty").val('');
            jQuery("#TOIUnitQty").val('');
            jQuery("#TOIItemAmount").val('');	
			
		}
		
	function	vallidationOnSave(){
			if(jQuery("#pricemode").val()==-1){
				jQuery("#pricemode").css("background","#FF9999");
				showMessage("","Select valid price mode","");
				return 0;
			}
			 if(jQuery("#Percentage").val()==-1){
				jQuery("#Percentage").css("background","#FF9999");
				showMessage("","Select valid percentage","");

				return 0;
			}
			 if(jQuery("#appDpp").val()==-1){
				jQuery("#appDpp").css("background","#FF9999");
				showMessage("","Select valid AppDpp","");

				return 0;
			}
			  if(jQuery("#createDestinationLocation").val()==0){
	             	jQuery("#createDestinationLocation").css("background","#FF9999");
	             	showMessage("","Select valid destination location","");

	             	return 0;
	             }
	             if(jQuery("#createSourceLocation").val()==0){
	              	jQuery("#createSourceLocation").css("background","#FF9999");
	              	showMessage("","Select valid source location","");

	              	return 0;
	              }
	             var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
					order_json_string = JSON.stringify(oOrderJson);
	         if( jQuery.parseJSON(order_json_string).length==0){
	        	 showMessage('red',"Select atleast one Item","");
	        	 return 0;
	         }
	           
	             return 1;
		}
		
		function vallidationOnAdd(){
			if(jQuery("#pricemode").val()==-1){
				jQuery("#pricemode").css("background","#FF9999");
				showMessage("","Select valid price mode","");
				jQuery("#pricemode").focus();
				return 0;
			}
			 if(jQuery("#Percentage").val()==-1){
				jQuery("#Percentage").css("background","#FF9999");
				showMessage("","Select valid percentage","");
				jQuery("#Percentage").focus();


				return 0;
			}
			 if(jQuery("#appDpp").val()==-1){
				jQuery("#appDpp").css("background","#FF9999");
				showMessage("","Select valid Appreciation/Depriciation","");
				jQuery("#appDpp").focus();
				
				return 0;
			}
			 if(jQuery("#txtItemCode").val()==''){
				jQuery("#txtItemCode").css("background","#FF9999");
				jQuery("#txtItemCode").focus();
				showMessage("","Enter valid Item Code","");
				return 0;
            }
		 if(jQuery("#txtUOM").val()==''){
				jQuery("#txtUOM").css("background","#FF9999");
				showMessage("","Enter valid Item Code for UOM. Press Tab.","");
				jQuery("#txtItemCode").focus();

				return 0;
            }
		 if(jQuery("#txtItemDescription").val()==''){
				jQuery("#txtItemDescription").css("background","#FF9999");
				showMessage("","Enter valid Item Code for Item Description. Press Tab.","");
				jQuery("#txtItemCode").focus();

				return 0;
            }
			 if(jQuery("#txtTransferPrice").val()==''){
				jQuery("#txtTransferItem").css("background","#FF9999");
				showMessage("","Enter valid Item Code for Item Description. Press Tab.","");
				jQuery("#txtItemCode").focus();
				
				return 0;
            }
			 if(jQuery("#bucket").val()==-1){
				jQuery("#bucket").css("background","#FF9999");
				showMessage("","Enter valid bucket.","");
				jQuery("#bucket").focus();
				
				return 0;
            }
             if(jQuery("#TOIAvailableQty").val()=='' ){
            	 jQuery("#TOIAvailableQty").val('');
            	jQuery("#TOIAvailableQty").css("background","#FF9999");
    			showMessage("","Enter valid Item Code/Bucket. Press Tab.","");
				jQuery("#txtItemCode").focus();
	        	
            	return 0;
            }
             if( jQuery("#TOIAvailableQty").val()=="NaN"){
            	 jQuery("#TOIAvailableQty").val('');
            	jQuery("#TOIAvailableQty").css("background","#FF9999");
     			showMessage("","Enter valid Item Code/Bucket. Press Tab.","");
				jQuery("#txtItemCode").focus();

            	return 0;
            }
             if(jQuery("#TOIUnitQty").val()==''){
            	jQuery("#TOIUnitQty").css("background","#FF9999");
     			showMessage("","Enter valid unit Quantity.","");
				jQuery("#TOIUnitQty").focus();
            	
            	
            	return 0;
           }
             
             
             if(jQuery("#TOIItemAmount").val()=='' && parseInt(jQuery("#TOIItemAmount").val())>0 ){
            	jQuery("#TOIItemAmount").css("background","#FF9999");
            	
            	return 0;
            }
             if(jQuery("#createDestinationLocation").val()==0){
             
            	 jQuery("#createDestinationLocation").css("background","#FF9999");
             	return 0;
             }
             if(jQuery("#createSourceLocation").val()==0){
              	jQuery("#createSourceLocation").css("background","#FF9999");
              	return 0;
              }
             return 1;
			
		}
		
		
		function backgroundColourWhite(){
			
				jQuery("#pricemode").css("background","white");
			
				jQuery("#Percentage").css("background","white");
				
				jQuery("#appDpp").css("background","white");
			
				jQuery("#txtItemCode").css("background","white");
			
				jQuery("#txtUOM").css("background","white");
			
				jQuery("#txtItemDescription").css("background","white");
		
				jQuery("#txtTransferItem").css("background","white");
			
				jQuery("#bucket").css("background","white");
				
    
            	jQuery("#TOIAvailableQty").css("background","white");
        
            	jQuery("#TOIUnitQty").css("background","white");
            	
           
            	jQuery("#TOIItemAmount").css("background","white");
            
             	jQuery("#createDestinationLocation").css("background","white");
             	
              	jQuery("#createSourceLocation").css("background","white");
              	jQuery("#createDestinationLocation2").css("background","white");
             	
              	jQuery("#createSourceLocation2").css("background","white");
            
			
		}
	
		
		
	/*	------------------------------------------------------------------------------------------------------------*/	
		
		
		
		
		function searchWHLocation()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"searchWHLocation",
				},
				success:function(data)
				{
					var whlocationsData = jQuery.parseJSON(data);
					//alert(data);
			if(whlocationsData.Status==1){
					jsonForLocations=whlocationsData.Result;
					var whlocations=whlocationsData.Result;
					jQuery.each(whlocations,function(key,value)
					{	
						jQuery("#sourceLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#destinationLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						
						jQuery("#createSourceLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#createDestinationLocation").append("<option id=\""+whlocations[key]['LocationId']+"\""+" value=\""+whlocations[key]['LocationId']+"\""+">"+whlocations[key]['DisplayName']+"</option>");
						jQuery("#createSourceLocation").find("option[value='-1']").remove();
						jQuery("#createDestinationLocation").find("option[value='-1']").remove();
		
					});
				}
			else{
				showMessage('red',whlocationsData.Result,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
			
			});
		}
		function searchBucket()
		{
		
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"searchBucket",
				},
				success:function(data)
				{
					
					var bucketData = jQuery.parseJSON(data);
				if(bucketData.Status==1){
					//alert(data);
					bucket=bucketData.Result;
					jQuery.each(bucket,function(key,value)
					{	
				
						jQuery("#bucket").append("<option id=\""+bucket[key]['BucketId']+"\""+" value=\""+bucket[key]['BucketId']+"\""+">"+bucket[key]['BucketName']+"</option>");
					});
				}
				else{
					showMessage('red',bucketData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		function TOIStatus()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"TOIStatus",
				},
				success:function(data)
				{
				
					var toiData = jQuery.parseJSON(data);
					
					if(toiData.Status==1){
						var toi=toiData.Result;
					jQuery.each(toi,function(key,value)
					{	
						
						jQuery("#TOIStatus").append("<option id=\""+toi[key]['keycode1']+"\""+" value=\""+toi[key]['keycode1']+"\""+">"+toi[key]['keyvalue1']+"</option>");
					});
					}
					else{
						showMessage('red',toiData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		function fillPriceStatus()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"FillPriceStatus",
				},
				success:function(data)
				{
				
					var pricestatusData = jQuery.parseJSON(data);
					if(pricestatusData.Status==1){
						var pricestatus=pricestatusData.Result;
					jQuery.each(pricestatus,function(key,value)
					{	
						var pricestatus=pricestatusData.Result;
						jQuery("#pricemode").append("<option id=\""+pricestatus[key]['keycode1']+"\""+" value=\""+pricestatus[key]['keycode1']+"\""+">"+pricestatus[key]['keyvalue1']+"</option>");
					});
					}
					else{
						showMessage('red',pricestatusData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		function fillPricePercentage()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"FillPricePercentage",
				},
				success:function(data)
				{
					
					var percentagestatusData = jQuery.parseJSON(data);
					if(percentagestatusData.Status==1){
						var percentagestatus=percentagestatusData.Result;
					jQuery.each(percentagestatus,function(key,value)
					{	
						
						jQuery("#Percentage").append("<option id=\""+percentagestatus[key]['keycode1']+"\""+" value=\""+percentagestatus[key]['keycode1']+"\""+">"+percentagestatus[key]['keyvalue1']+"</option>");
					});
					}
					else{
						
						showMessage('red',percentagestatusData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		function fillAppDpp()
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"FillAppDpp",
				},
				success:function(data)
				{
				
					var appDppData = jQuery.parseJSON(data);
				if(appDppData.Status==1){
					var appDpp=appDppData.Result;
					jQuery.each(appDpp,function(key,value)
					{	
						
						jQuery("#appDpp").append("<option id=\""+appDpp[key]['keycode1']+"\""+" value=\""+appDpp[key]['keycode1']+"\""+">"+appDpp[key]['keyvalue1']+"</option>");
					});
				}
				else{
					showMessage('red',appDppData.Description,'');
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
	


		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		
		myDelOptions = {
				onclickSubmit: function (rp_ge, rowid) {
            rp_ge.processing = true;
   
            	jQuery("#TOIItemGrid").jqGrid('delRowData', rowid);
       
                jQuery("#delmod" + jQuery("#TOIItemGrid")[0].id).hide();
 
                if (jQuery("#TOIItemGrid")[0].p.lastpage > 1) {
                	jQuery("#TOIItemGrid").trigger("reloadGrid", [{ page: jQuery("#TOItemGrid")[0].p.page}]);
                }
                jQuery("#TOICreateTOIAmount").val(parseFloat(sumOfColumnValueJQGrid2('TOIItemGrid','TotalAmount'),10).toFixed(2));
    			jQuery("#TOICreateTOIQuantity").val(parseFloat(sumOfColumnValueJQGrid('TOIItemGrid','UnitQty'),10).toFixed(0));
    			jQuery("#TOICreatePOStatus").val(parseFloat(sumOfColumnValueJQGrid2('TOIItemGrid','LineWeight'),10).toFixed(2)/1000);
            return true;
        },
        processing: true
        };
		
		
	
		
		jQuery("#TOIItemGrid").jqGrid({
			
			datatype: 'jsonstring',
			colNames: ['Edit','Bucketid','UOMId','ItemId','Item Code','Item Name','UOM','Unit Price','Bucket',
			           'Available Qty','Quantity','LineWeight','Total Amoount','TOINumber','rid'],
			colModel: [
{ name: 'Edit', width:60, fixed:true, sortable:false, resize:false, formatter:'actions',
	formatoptions:{
		keys:true,
		onEdit:function(rowid) {

			var thisid = this.id;

			updateOnEditableValue = jQuery(".editable").val();

			var itemQuantity = parseInt(jQuery(".editable").val());

			var rowEditableOrNot = parseInt(jQuery('#jqgridtable_left').jqGrid('getCell',rowid, 'IsEditableOrNot'));

			if(rowEditableOrNot == 0)
			{
				//alert(rowEditableOrNot);
				showMessage("red", "Promotional items quantity can not be changed", "");

				jQuery("#jqgridtable_left").jqGrid('setCell',rowid,'Qty',itemQuantity);

			}
			else
			{
				jQuery("#jqgridtable_left").jqGrid('setColProp', 'Qty', {editable:true});
			}


		},
		onSuccess:function(jqXHR) {
	
			alert("in onSuccess used only for remote editing:"+
					"\nresponseText="+jqXHR.responseText+
					"\n\nWe can verify the server response and return false in case of"+
			" error response. return true confirm that the response is successful");
			return true;
		},
		onError:function(rowid, jqXHR, textStatus) {
			alert("in onError used only for remote editing:"+
					"\nresponseText="+jqXHR.responseText+
					"\nstatus="+jqXHR.status+
					"\nstatusText"+jqXHR.statusText+
			"\n\nWe don't need return anything");
		},
		afterSave:function(rowid) {
			//alert("in afterSave (Submit): rowid="+rowid+"\nWe don't need return anything");
			var rowData = jQuery('#jqgridtable_left').jqGrid ('getRowData', rowid);
			var itemDistributorPrice = rowData.Price;
			var itemId = rowData.RowID;
			var itemQty = rowData.Qty;
			if(itemQty <= 0)
			{
				var intUpdateOnEditableValue = parseInt(updateOnEditableValue);

				showMessage("red", "Quantity can't be negative or zero", "");

				jQuery("#jqgridtable_left").jqGrid('setCell',rowid,'Qty',intUpdateOnEditableValue);

			}
			else
			{
				
			}


			//alert(rowData);
		},
		afterRestore:function(rowid) {
			//alert("in afterRestore (Cancel): rowid="+rowid+"\nWe don't need return anything");

		},
		delOptions:myDelOptions
	}
},
                       { name: 'Bucketid', index: 'BucketId', width: 70, hidden:true},
                       { name: 'UOMId', index: 'UOMId', width: 70, hidden:true},
                       { name: 'ItemId', index: 'ItemId', width: 70, hidden:true},
			           { name: 'ItemCode', index: 'ItemCode', width:100 },
			           { name: 'ItemName', index: 'DistributorId', width: 150},
			           { name: 'UOM', index: 'FirstName', width: 150},
			           { name: 'UnitPrice', index: 'UnitPrice', width: 100},
			           { name: 'Bucket', index: 'Bucket', width: 70},
			           { name: 'AvailableQty', index: 'AvailableQty', width: 70, hidden:true},
			           { name: 'UnitQty', index: 'UnitQty', width: 100},
			           { name: 'LineWeight', index: 'LineWeight', width: 100},
			           { name: 'TotalAmount', index: 'TotalAmount', width: 70},
			           { name: 'TOINumber', index: 'TOINumber', width: 70, hidden:true},
			           { name: 'rid', index: 'rid', width: 70, hidden:true},
			          ],
			           pager: jQuery('#PJmap_TOIItemGrid'),
			           width:1040,
			           height:150,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           editurl:'clientArray',

			           afterInsertRow : function(ids)
			           {
//			        	  
			 
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});

			           },
			           
			           loadComplete: function() {
//			        	   
			           }	,
			         
			           onSelectRow: function(ids) {
				       		var rowId=jQuery('#TOIItemGrid').jqGrid('getGridParam', 'selrow');
				       		
				       		jQuery("#txtItemCode").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'ItemCode'));
				       		jQuery("#txtUOM").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'UOM'));
				       		jQuery("#txtItemDescription").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'ItemName'));
				       		jQuery("#txtTransferPrice").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'UnitPrice'));
				       		jQuery("#Bucket").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'BucketId'));
				       		jQuery("#TOILineWeight").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'LineWeight'));
				       		var availQty=jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'AvailableQty');
				       		
				       	if(availQty==0){
				       		jQuery("#TOIAvailableQty").val('');
				       		jQuery("#btnAdd").attr('disabled',true);
				       	}
				       	else{
				       		jQuery("#TOIAvailableQty").val(availQty);
				       	}
				       		jQuery("#TOIUnitQty").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'UnitQty'));

				       		jQuery("#TOIItemAmount").val(jQuery("#TOIItemGrid").jqGrid('getCell', rowId, 'TotalAmount'));
			           }

		});

		jQuery("#TOIItemGrid").jqGrid('navGrid','#pjmap_downline_grid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_TOIItemGrid").hide();

		
		
		
		
		/*------------------------------add calender to fields --------------------------------*/
		
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*
		 * TOI Search Grid 
		 */
		jQuery("#TOISearchGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Edit','TOI Number','Source Address','Destination Address','TOI Creation Date','TOI Date','Total TOI Quantity','Total TOI Amount','Gross Weight','TOI Status','PO Number','PO Status',
			           'SourceLocationId','DestinationLocationId','Isexported','Statusid','Indentised','POStatus','POStatusName','PriceMode','Percentage','AppDep'],
			colModel: [{ name: 'Edit', index: 'Edit', width:100,hidden:true },
			           { name: 'TOINumber', index: 'TOINumber', width: 150},
			           { name: 'SourceAddress', index: 'SourceAddress', width: 200},
			           { name: 'DestinationAddress', index: 'DestinationAddress', width: 200},
			           { name: 'TOICreationDate', index: 'DiscountAmount', width: 70},
			           { name: 'TOIDate', index: 'TOIDate', width: 70},
			           { name: 'TotalTOIQuantity', index: 'TotalTOIQuantity', width: 100},
			           { name: 'TotalTOIAmount', index: 'TotalTOIQuantity', width: 100},
			           { name: 'Grossweight', index: 'Grossweight', width: 100},
			           { name: 'TOIStatus', index: 'TOIStatus', width: 70},
			           { name: 'PONumber', index: 'PONumber', width: 70},
			           
			           
			           { name: 'POStatus', index: 'POStatus', width: 50, hidden:false},

			           { name: 'SourceLocationId', index: 'SourceLocationId', width: 70, hidden:true},
			           { name: 'DestinationLocationId', index: 'DestinationLocationId', width: 70, hidden:true},
			           { name: 'Isexported', index: 'Isexported', width: 70, hidden:true},
			           { name: 'Statusid', index: 'Statusid', width: 70, hidden:true},
			           { name: 'Indentised', index: 'Indentised', width: 70, hidden:true},
			           { name: 'POStatus', index: 'POStatus', width: 70, hidden:true},
			           { name: 'POStatusName', index: 'POStatusName', width: 70, hidden:true},
			           { name: 'PriceMode', index: 'PriceMode', width: 70, hidden:true},
			           { name: 'Percentage', index: 'Percentage', width: 70, hidden:true},
			           { name: 'AppDep', index: 'AppDep', width: 70, hidden:true}
 
			           ],
			           pager: jQuery('#PJmap_TOISearchGrid'),
			           width:1040,
			           height:350,
			           rowNum: 20,
			           rowList: [500],
			           sortname: 'Label',
			           sortorder: "asc",
			           hoverrows: true,
			           autowidth:true,
                       scrollable:true,
                       shrinkToFit:false,
                       autoheight:true,

			           afterInsertRow : function(ids)
			           {
//			        	   alert("ids is" + ids);
			        	  // var index1 = jQuery("#TOISearchGrid").jqGrid('getCol','Name',false);
//			        	   alert("after insert row" + index1);
			        	   jQuery(this).jqGrid('setRowData', ids, false,{'background-color':'white'});
//			        	  
			           },
			           ondblClickRow: function(ids) {
			        	
				       		var rowId=jQuery('#TOISearchGrid').jqGrid('getGridParam', 'selrow');
				       	
				       		jQuery("#TOICreateTOIQuantity").val( parseFloat( jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'TotalTOIQuantity'),10).toFixed(2));
				       		jQuery("#TOICreateTOIAmount").val( parseFloat( jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'TotalTOIAmount'),10).toFixed(2));
				       		jQuery("#createSourceAddress").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'DestinationAddress'));
							jQuery("#createDestinationAddress").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'SourceAddress'));
							jQuery("#TOICreatePOStatus").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'Grossweight'));
							jQuery("#createSourceLocation").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'SourceLocationId'));
							jQuery("#createDestinationLocation").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'DestinationLocationId'));
							jQuery("#createSourceLocation2").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'SourceLocationId'),'LocationName'));
						
							jQuery("#createDestinationLocation2").val(fetchvaluesFromJsonOnSelectRow(jsonForLocations,'LocationId',jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'DestinationLocationId'),'LocationName'));
							jQuery("#createDestinationLocation2").attr('disabled',true);
							jQuery("#createSourceLocation2").attr('disabled',true);
							jQuery("#pricemode").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'PriceMode'));
							jQuery("#TOICreateStatus").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'TOIStatus'));
							jQuery("#Indentised").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'Indentised'));
							jQuery("#TOICreateNumber").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'TOINumber'));
							jQuery("#appDpp").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'AppDep'));
							jQuery("#Percentage").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'Percentage'));
							jQuery("#TOICreatePOnumber").val(jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'PONumber'));
				       		
				       		
				       		disableButtons((jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'Statusid')));
				       		jQuery("#DivDistributorInfoSearchTab").tabs( "select", "DivCreate" );
				       		
				            disableField();
				       		var TOINumber = jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'TOINumber');
				       		var SourceLocationId = jQuery("#TOISearchGrid").jqGrid('getCell', rowId, 'SourceLocationId');
				       		//alert (myCellData);
				       		showMessage("loading", "Seaching for item detail...", "");
				       		var rowId=1;
				       		jQuery.ajax({
				       			type:'POST',
								url:Drupal.settings.basePath + 'TOICallback',
								data:
								{
									id:"searchTOItems",
									TOINo:TOINumber,
									sourceId:SourceLocationId
								},
								success:function(data)  /*ajax call for populating payment grid*/
								{
									jQuery(".ajaxLoading").hide();
									var  itemsJsonData = jQuery.parseJSON(data);
								if(itemsJsonData.Status==1){
									jQuery("#TOIItemGrid").jqGrid("clearGridData");
									var itemsJson=itemsJsonData.Result;
									jQuery.each(itemsJson,function(key,value)
									 {
										var itemsJson=itemsJsonData.Result;
										var ItemCode = itemsJson[key]['ItemCode']	;
										var ItemId=itemsJson[key]['ItemId']	;
										var ItemDescription = itemsJson[key]['ItemDescription'];    
										var UOMId = itemsJson[key]['UOMId']  ;
										var UOMName = itemsJson[key]['UOMName']  ;
										var UnitPrice =parseFloat( itemsJson[key]['TransferPrice'],10).toFixed(2);    
										var Bucketid = itemsJson[key]['BucketId'];
										var Bucket=itemsJson[key]['BucketName'];
										var AvailableQty =parseFloat( itemsJson[key]['AvailableQty'],10).toFixed(2);     
										var ItemQuantity =parseFloat( itemsJson[key]['ItemQuantity'],10).toFixed(2);           
										var TotalAmount =parseFloat( itemsJson[key]['TotalAmount'],10).toFixed(2); 
										var LineWeight =parseFloat( itemsJson[key]['LineWeight'],10).toFixed(2); 
										var TOINo =itemsJson[key]['TOINumber'] ; 
										var rid =itemsJson[key]['RowNo'] ;
										var newData = [{"Bucketid":Bucketid,"UOMId":UOMId,"ItemId":ItemId,"ItemCode":ItemCode,"ItemName":ItemDescription,"UOM":UOMName,"UnitPrice":UnitPrice
											,"Bucket":Bucket,"AvailableQty":AvailableQty,"UnitQty":parseFloat(ItemQuantity,10).toFixed(0),"LineWeight":parseFloat(LineWeight,10).toFixed(0),"TotalAmount":TotalAmount,"TOINumber":TOINo,"rid":rid}];
										for (var i=0;i<newData.length;i++) {
											jQuery("#TOIItemGrid").jqGrid('addRowData',rowId, newData[newData.length-i-1], "first");
											rowId=rowId+1;
									   }
									
										
										   //  clearFields();
								    /*       'SourceLocationId','DestinationLocationId','Isexported','Statusid','Indentised','POStatus','POStatusName',
								           'PriceMode','Percentage','AppDep'],
								           'Edit','TOI Number','Source Address','Destination Address','TOI Creation Date','TOI Date','Total TOI Quantity',
								           'Total TOI Amount','TOI Status','PO Number','PO Status',
								           */
								           
								          
											
								    });
									}
								else{
									showMessage('red',itemsJsonData.Description,'');
								}
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) { 

									jQuery(".ajaxLoading").hide();
									var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
									showMessage("", defaultMessage, "");
								}
				       		});  	
		           }	   	

			           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

		});

		jQuery("#TOISearchGrid").jqGrid('navGrid','#PJmap_TOISearchGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_TOISearchGrid").hide();
		
		
		/*jQuery("#TOIInfoSearchTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
			
			
			jQuery('#TOISearchGrid').jqGrid('clearGridData', true);
			var tabIndex = tab.index; 
			if(tabIndex == 1)
			{
				
			}
			
		});*/
		
		
		
	
		function disableField(){
			jQuery("#TOICreateTOIQuantity").attr('disabled',true);
       		jQuery("#TOICreateTOIAmount").attr('disabled',true);
       		jQuery("#createSourceAddress").attr('disabled',true);
			jQuery("#createDestinationAddress").attr('disabled',true);

       		jQuery("#createSourceAddress2").attr('disabled',true);
			jQuery("#createDestinationAddress2").attr('disabled',true);
			
       		jQuery("#TOICreatePOStatus").attr('disabled',true);
			jQuery("#createSourceLocation").attr('disabled',true);
			jQuery("#createDestinationLocation").attr('disabled',true);
			jQuery("#createSourceLocation2").attr('disabled',true);
			jQuery("#createDestinationLocation2").attr('disabled',true);
			jQuery("#pricemode").attr('disabled',true);
			jQuery("#TOICreateStatus").attr('disabled',true);
			jQuery("#Indentised").attr('disabled',true);
			jQuery("#TOICreateNumber").attr('disabled',true);
			jQuery("#appDpp").attr('disabled',true);
			jQuery("#Percentage").attr('disabled',true);
			jQuery("#TOICreatePONumber").attr('disabled',true);
			backGroundColorForDisableField();
		
		}
		function disableFieldOnStartup(){
			jQuery("#txtUOM").attr('disabled',true);
			jQuery("#txtItemDescription").attr('disabled',true);
			jQuery("#TOICreateTOIQuantity").attr('disabled',true);
       		jQuery("#TOICreateTOIAmount").attr('disabled',true);
       		jQuery("#TOICreatePOStatus").attr('disabled',true);
			jQuery("#TOICreateStatus").attr('disabled',true);
			jQuery("#TOICreateNumber").attr('disabled',true);
			jQuery("#TOICreatePONumber").attr('disabled',true);
			disableButtons(0);
			jQuery("#txtTransferPrice").attr('disabled',true);
			jQuery("#TOIItemWeight").attr('disabled',true);
			jQuery("#TOILineWeight").attr('disabled',true);
			jQuery("#createDestinationAddress").attr('disabled',true);
			jQuery("#createSourceAddress").attr('disabled',true);
            jQuery("#TOIAvailableQty").attr('disabled',true);
           
            jQuery("#TOIItemAmount").attr('disabled',true);	
            
		}
		
		function enableButtons(){
			jQuery("#btnTSF01Cancel").attr('disabled',false);	
			jQuery("#btnTSF01Confirm").attr('disabled',false);	
			jQuery("#btnTSF01Save").attr('disabled',false);
			jQuery("#btnTSF01Reject").attr('disabled',false);
			jQuery("#btnTSF01Print").attr('disabled',false);
		}


		function enableFields(){
			jQuery("#TOICreateTOIQuantity").attr('disabled',false);
       		jQuery("#TOICreateTOIAmount").attr('disabled',false);
       		jQuery("#createSourceAddress").attr('disabled',false);
			jQuery("#createDestinationAddress").attr('disabled',false);
			jQuery("#createSourceAddress2").attr('disabled',false);
			jQuery("#createDestinationAddress2").attr('disabled',false);
       		jQuery("#TOICreatePOStatus").attr('disabled',false);
			jQuery("#createSourceLocation").attr('disabled',false);
			jQuery("#createDestinationLocation").attr('disabled',false);
			jQuery("#createSourceLocation2").attr('disabled',false);
			jQuery("#createDestinationLocation2").attr('disabled',true);
			jQuery("#pricemode").attr('disabled',false);
			jQuery("#TOICreateStatus").attr('disabled',false);
			jQuery("#Indentised").attr('disabled',false);
			jQuery("#TOICreateNumber").attr('disabled',false);
			jQuery("#appDpp").attr('disabled',false);
			jQuery("#Percentage").attr('disabled',false);
			jQuery("#TOICreatePONumber").attr('disabled',false);
			jQuery("#btnadd").attr('disabled',true);
		}
	function disableButtons(statusId){
		  
		
		    if(statusId==0){
		    	jQuery("#btnTSF01Cancel").attr('disabled',true);	
				jQuery("#btnTSF01Confirm").attr('disabled',true);	
				jQuery("#btnTSF01Reject").attr('disabled',true);
				jQuery("#btnTSF01Print").attr('disabled',true);
				jQuery("#btnTSF01Search").attr('disabled',false);
				jQuery("#btnTSF01Save").attr('disabled',false);
		    }
			//CREATED
			if(statusId==1){
				
				jQuery("#btnTSF01Reject").attr('disabled',true);	
				jQuery("#btnTSF01Print").attr('disabled',true);	
				jQuery("#btnTSF01Cancel").attr('disabled',false);	
				jQuery("#btnTSF01Confirm").attr('disabled',false);
				jQuery("#btnTSF01Search").attr('disabled',false);
				jQuery("#btnTSF01Save").attr('disabled',false);
			}
			//confirm
			else if(statusId==2){
				jQuery("#btnTSF01Cancel").attr('disabled',true);	
				jQuery("#btnTSF01Confirm").attr('disabled',true);	
				jQuery("#btnTSF01Save").attr('disabled',true);
				jQuery("#btnAdd").attr('disabled',true);	
				jQuery("#btnTSF01Print").attr('disabled',false);
				jQuery("#btnTSF01Reject").attr('disabled',false);	
				jQuery("#btnTSF01Search").attr('disabled',false);
				
			}
			//cancel
			else if(statusId==3){
				jQuery("#btnTSF01Cancel").attr('disabled',true);	
				jQuery("#btnTSF01Confirm").attr('disabled',true);	
				jQuery("#btnTSF01Save").attr('disabled',true);
				jQuery("#btnTSF01Reject").attr('disabled',true);	
				jQuery("#btnAdd").attr('disabled',true);
				jQuery("#btnTSF01Search").attr('disabled',false);
				jQuery("#btnTSF01Print").attr('disabled',true);
			}
			//reject
			else if(statusId==4){
				jQuery("#btnTSF01Cancel").attr('disabled',true);	
				jQuery("#btnTSF01Confirm").attr('disabled',true);	
				jQuery("#btnTSF01Save").attr('disabled',true);
				jQuery("#btnTSF01Reject").attr('disabled',true);
				jQuery("#btnAdd").attr('disabled',true);
				jQuery("#btnTSF01Search").attr('disabled',false);
				jQuery("#btnTSF01Print").attr('disabled',true);
			}
			
			//closed
			else if(statusId==5){
				jQuery("#btnTSF01Cancel").attr('disabled',true);	
				jQuery("#btnTSF01Confirm").attr('disabled',true);	
				jQuery("#btnTSF01Save").attr('disabled',true);
				jQuery("#btnTSF01Reject").attr('disabled',true);	
				jQuery("#btnAdd").attr('disabled',true);
				jQuery("#btnTSF01Print").attr('disabled',false);
				jQuery("#btnTSF01Search").attr('disabled',false);
			}
			actionButtonsStauts();
			
		}
		
		

		jQuery("#btnTSF01Search").click(function(){
			showMessage("loading", "Searching transfer out instructions...", "");
			jQuery("#actionName").val('Search');

			jQuery("#fromTODate").datepicker("option", "dateFormat", "yy-mm-dd ");
			jQuery("#toTODate").datepicker("option", "dateFormat", "yy-mm-dd ");
			jQuery("#fromTOShipDate").datepicker("option", "dateFormat", "yy-mm-dd ");
			jQuery("#toTOShipDate").datepicker("option", "dateFormat", "yy-mm-dd ");
				var searchFormData = jQuery("#formSearchTOI").serialize();
				 jQuery("#fromTODate").datepicker("option", "dateFormat", "d-M-yy");
				 jQuery("#toTODate").datepicker("option", "dateFormat", "d-M-yy");
				 jQuery("#fromTOShipDate").datepicker("option", "dateFormat", "d-M-yy");
				 jQuery("#toTOShipDate").datepicker("option", "dateFormat", "d-M-yy");
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'TOICallback',
						data:
						{
							id:"searchTOI",
							functionName:jQuery("#actionName").val(),
							moduleCode:"TSF01",
							TOIFormData:searchFormData,
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							var rowid=0;
							var logNumberParsedJsonData = jQuery.parseJSON(data);
						if(logNumberParsedJsonData.Status==1){
							
							logNumberParsedJson=logNumberParsedJsonData.Result;
							if(logNumberParsedJson.length==0)
							{
								jQuery("#TOISearchGrid").jqGrid("clearGridData");
								showMessage("red","No Record Found ","");
								
						
							}
							else
							{	
								
								jQuery("#TOISearchGrid").jqGrid("clearGridData");
								jQuery.each(logNumberParsedJson,function(key,value){			
									var newData = [{"TOINumber":logNumberParsedJson[key]['TOINumber'],
										"DestinationAddress":logNumberParsedJson[key]['DestinationAddress'],
										"TOICreationDate":logNumberParsedJson[key]['CreationDate'],
										"TOIDate":logNumberParsedJson[key]['TOIDate'],
										"TotalTOIAmount":parseFloat(logNumberParsedJson[key]['TotalTOIAmount'],10).toFixed(2),
										"Grossweight":parseFloat(logNumberParsedJson[key]['GrossWeight'],10).toFixed(2),
										"PONumber":logNumberParsedJson[key]['PONumber'],
										"POStatus":logNumberParsedJson[key]['POStatus'],
							          	"SourceAddress":logNumberParsedJson[key]['SourceAddress'],
												"TOIStatus":logNumberParsedJson[key]['StatusName'],
												"TotalTOIQuantity":parseFloat(logNumberParsedJson[key]['TotalTOIQuantity'],10).toFixed(2),                      
												"Edit":'Edit',
												"SourceLocationId":logNumberParsedJson[key]['SourceLocationId'],
												"DestinationLocationId":logNumberParsedJson[key]['DestinationLocationId'],
												"Isexported":logNumberParsedJson[key]['Isexported'],
									          	"Statusid":logNumberParsedJson[key]['Status'],  	
									          	"Indentised":logNumberParsedJson[key]['Indentised'],
												"POStatus":logNumberParsedJson[key]['POStatus'],
												"POStatusName":logNumberParsedJson[key]['POStatusName'],
									          	"PriceMode":logNumberParsedJson[key]['PriceMode'],
									          	"Percentage":logNumberParsedJson[key]['Percentage'],
												"AppDep":logNumberParsedJson[key]['AppDep'],
											
												
									}];
							
									for (var i=0;i<newData.length;i++) {
									jQuery("#TOISearchGrid").jqGrid('addRowData',rowid, newData[newData.length-i-1], "first");
									rowid=rowid+1;
									}
								});
								
							}
							
							}
						else{

							showMessage('red',logNumberParsedJsonData.Description,'');

						}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
			});
		
		/*
..........................................GENERAL FUNCTIONS..............................................................................................................	*/	
		
		
		jQuery("#btnTSF01Save").unbind("click").click(function(){
			jQuery("#actionName").val('Save');
			 var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
				order_json_string = JSON.stringify(oOrderJson);
if( vallidationOnSave()==1 )
         {
				 enableFields();
			var formCreateTOI = jQuery("#formCreateTOI").serialize();
			
			disableField();
		
			var r=confirm("Are you sure you want to save data ?");
   if(r==true){
	   			showMessage("loading", "Saving transfer out instruction...", "");
			   jQuery.ajax({
				   type:'POST',
				url:Drupal.settings.basePath + 'TOICallback',
				data:
				{
					id:"saveOrder",
					status:1,
					items:order_json_string,
					formData:formCreateTOI ,
					functionName:jQuery("#actionName").val(),
					moduleCode:"TSF01",
				},
				success:function(data)
				{
			
					jQuery(".ajaxLoading").hide();
					var resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
					 jQuery.each(results,function(key,value)
							   {	
						 showMessage("green","Record Created Successfully "+ results[key]['TNumber'] +" .","");
						
						 jQuery("#TOICreateNumber").val( results[key]['TNumber'])	;
						 jQuery("#TOICreateStatus").val('created')	;
								 
						      });
					 disableButtons(1);
				}
					else{
						showMessage('red',resultsData.Description,'');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			     });
		    	}
			
	       }
			
		});
		
		jQuery("#btnTSF01Cancel").unbind("click").click(function(){
			jQuery("#actionName").val('Cancel');
			 enableFields();
			
				var formCreateTOI = jQuery("#formCreateTOI").serialize();
				var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
				order_json_string = JSON.stringify(oOrderJson);
				disableField();
				var r=confirm("Are you sure you want to cancel data ?");
				   if(r==true){
					   showMessage("loading", "Cancelling transfer out instruction...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'TOICallback',
					data:
					{
						id:"saveOrder",
						status:3,
						items:order_json_string,
						functionName:jQuery("#actionName").val(),
						moduleCode:"TSF01",
						formData:formCreateTOI 
						
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
						 jQuery.each(results,function(key,value)
								   {	
							 showMessage("green","Record Cancelled Successfully " + results[key]['TNumber'] +" .","");
						
							 jQuery("#TOICreateNumber").val( results[key]['TNumber'])	;
							 jQuery("#TOICreateStatus").val('Cancelled')	;
									 
							      });
						 disableButtons(3);
					}
					else{
						showMessage('red',resultsData.Description,'');
					}
				
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
			
				});
				   }
		
		});
		jQuery("#btnTSF01Confirm").unbind("click").click(function(){
			 enableFields();
			 jQuery("#actionName").val('Confirm');
				var formCreateTOI = jQuery("#formCreateTOI").serialize();
				var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
				order_json_string = JSON.stringify(oOrderJson);
				disableField();
				var r=confirm("Are you sure you want to confirm data ?");
				   if(r==true){
					   showMessage("loading", "Confirming transfer out instruction...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'TOICallback',
					data:
					{
						id:"saveOrder",
						status:2,
						items:order_json_string,
						formData:formCreateTOI ,
						functionName:jQuery("#actionName").val(),
						moduleCode:"TSF01",
						
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
					var	resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
						 jQuery.each(results,function(key,value)
								   {
						showMessage("green","Record Confirmed Successfully "+ results[key]['TNumber'] +" .","");
							 
							 jQuery("#TOICreateNumber").val( results[key]['TNumber'])	;
							 jQuery("#TOICreateStatus").val('Confirmed')	;
									 
							      });
						 disableButtons(2);
					}
					
					else{
						showMessage('red',resultsData.Description,'');
					}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
				   }
			
			
		});
		
		
		jQuery("#btnTSF01Reject").unbind("click").click(function(){
			 enableFields();
			 jQuery("#actionName").val('Reject');
				var formCreateTOI = jQuery("#formCreateTOI").serialize();
				var oOrderJson = jQuery("#TOIItemGrid").jqGrid('getRowData');
				order_json_string = JSON.stringify(oOrderJson);
				disableField();
				var r=confirm("Are you sure you want to reject data ?");
				   if(r==true){
					   showMessage("loading", "Rejecting transfer out instruction...", "");
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'TOICallback',
					data:
					{
						id:"saveOrder",
						status:4,
						items:order_json_string,
						functionName:jQuery("#actionName").val(),
						moduleCode:"TSF01",
						formData:formCreateTOI 
						
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						var resultsData = jQuery.parseJSON(data);
					if(resultsData.Status==1){
						var results=resultsData.Result;
						 jQuery.each(results,function(key,value)
								   {
							 
							 showMessage("green","Record Rejected Successfully "+ results[key]['TNumber'] +" .","");
							
							 jQuery("#TOICreateNumber").val( results[key]['TNumber'])	;
							 jQuery("#TOICreateStatus").val('Rejected')	;
									 
							      });
						 disableButtons(4);
					}
					else{
						
						showMessage('red',resultsData.Description,'');
					}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			
		}
			
		});
		
		
		function matchItemsInJQGrid(gridName,columnName,columnValue){
			var gridRowsForColumn = jQuery("#"+gridName).jqGrid('getCol',columnName,false);
		   var found=0;
			for(var i=0;i<gridRowsForColumn.length;i++)
			{

				if(columnValue == gridRowsForColumn[i])
				{
					found = 1;
					break;
				}
				else 
					{
					
					found=0;
					}
//				pv_sum+= parseFloat(PV[i]);
			}
			return found;
		}
		backGroundColorForDisableField();
	
		
		
		jQuery("#btnReset").unbind("click").click(function(){
			 jQuery("#TOINumber").val('');
				jQuery("#sourceLocation").val(-1);
					 jQuery("#destinationLocation").val(-1);
					
		             jQuery("#TOIStatus").val(-1);
		             jQuery("#indentised").val(0);		
		    	
		        		jQuery("#TOISearchGrid").jqGrid("clearGridData"); 
		        		
		        		
			
		});
		
		jQuery("#btnReset").click(function(){
			 jQuery("#TOINumber").val('');
				jQuery("#sourceLocation").val(-1);
					 jQuery("#destinationLocation").val(-1);
					
		             jQuery("#TOIStatus").val(-1);
		             jQuery("#indentised").val(0);		
		    	
		        		jQuery("#TOISearchGrid").jqGrid("clearGridData"); 
		        		
		        		
			
		});
		jQuery("#btnTSF01Print").click(function(){
		jQuery("#actionName").val('Print');
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'TOICallback',
			data:
			{
				id:"printTOI",
				TONumber:jQuery('#TOICreateNumber').val(),
				functionName:jQuery("#actionName").val(),
				moduleCode:"TSF01"
				
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				 jQuery.each(results,function(key,value)
						   {
					 			var arr = {'locationId':results[key]['locationId'],'TONumber':results[key]['TONumber']};
					 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
							 
					      });
			}
			else{
				
				showMessage('red',resultsData.Description,'');
			}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	

		
		});
		
});
