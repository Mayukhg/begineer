var loggedInUserPrivilegesForStockCount ;
jQuery(document).ready(function(){
	
	
	
	jQuery("#lbtotalQty").hide();
	jQuery("#txttotalQty").hide();
	backGroundColorForDisableField();
	var oItemsInPackInformation;
	var oPackUnpackCreateBatchInformation;
	var itemId;
	var unpackQty;
	var oConstituentItems; //constituent item json object.

	var moduleCode = jQuery("#moduleCode").val();
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:
		{
			id:"moduleFunctionHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesPackUnpack = jQuery.parseJSON(data);
			
			if(loggedInUserPrivilegesPackUnpack.Status == 1)
			{
				
				loggedInUserPrivilegesForStockCount  = loggedInUserPrivilegesPackUnpack.Result;
				
				if(loggedInUserPrivilegesForStockCount.length == 0)
				{
					//jQuery(".SCCreateActionButtons").attr('disabled',true); //all button coming as disabled already.
				}
				else
				{

					showCalender();
					disableFieldOnStartup();
				}
			}
			else
			{
				showMessage("red", loggedInUserPrivilegesPackUnpack.Description, "");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
	
	
	
	function actionButtonsStauts()
	{
		jQuery(".PackUnpackCreateButtons").each(function(){
			
			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");
			
			if(loggedInUserPrivilegesForStockCount[extractedButtonId] == 1)
			{
				
				//jQuery("#"+buttonid).attr('disabled',false);
				
			}
			else
			{
				jQuery("#"+buttonid).attr('disabled',true);
			}
		
			
		});
	}

	jQuery("#packUnpackCreateMFGBatchSelect").hide();
	
	jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery("#packUnpackCreateMRP").attr('disabled',true);//disabled permanently.
	
	jQuery("#packUnpackCreateMFG").attr('disabled',true);
	
	jQuery("#packUnpackCreateEXP").attr('disabled',true);	
	
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 625,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	  });
	
	
	function disableFieldOnStartup(){
		jQuery("#packUnpackCreateMRP").attr('disabled',true);
		jQuery("#packUnpackCreateMFG").attr('disabled',true);
		jQuery("#packUnpackCreateEXP").attr('disabled',true);
		jQuery("#packUnpackCreateItemName").attr('disabled',true);
		jQuery("#btnPUN01Save").attr('disabled',true);
		jQuery("#packunpacktotalQty").attr('disabled',true);
		jQuery('#btnPUN01Print').attr('disabled',true);
		actionButtonsStauts();
	}
	
	//jQuery( "#TOCreateExpectedDeliveryDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', yearRange: '1950:2010',minDate: +1, maxDate: "100D"});
	
		jQuery(function() {
			//alert("dfadf");
			jQuery( "#divPackUnpackTab" ).tabs(); // Create tabs using jquery ui.
		});
		
		jQuery("#packUnpackCreateItemCode").keydown(function(e){

			if(e.which == 115) 
			{
				var currentCompositeSearchId = this.id;

				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'LookUpCallback',
					data:
					{
						id:"CompositeItemSearchLookUp",
					},
					success:function(data)
					{

						//alert(data);
						jQuery("#divLookUp").html(data);

						CompositeItemSearchGrid(currentCompositeSearchId);
						jQuery("#divLookUp" ).dialog( "open" );

						compositeItemLookUpData();

						/*----------Dialog for item search is there ----------------*/


					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}

				});
			}


		});
		
		
		
		function packUnpackList()
		{
			jQuery("#packUnpackSelect").append("<option name='packUnpackOption' value='-1'>Select</option>");
			jQuery("#packUnpackSelect").append("<option name='packUnpackOption' value='0'>Pack</option>");
			jQuery("#packUnpackSelect").append("<option name='packUnpackOption' value='1'>Unpack</option>");
			
			
		}
		
		/*-------------------------------applying datepicker on necessary fields-------------------------------*/
		
		jQuery(function() {
			jQuery( ".PackUnpackInputDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
			jQuery( ".PackUnpackInputDate" ).datepicker("setDate",new Date());
		});
		
		jQuery(function() {
			jQuery( "#packUnpackCreatePackUnpackDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
			jQuery( "#packUnpackCreatePackUnpackDate" ).datepicker("setDate",new Date());
		});
		
		jQuery(function() {
			jQuery( "#packUnpackCreateMFG" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
			jQuery( "#packUnpackCreateMFG" ).datepicker("setDate",new Date());
		});
		jQuery(function() {
			jQuery( "#packUnpackCreateEXP" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd'});
			jQuery( "#packUnpackCreateEXP" ).datepicker("setDate",new Date());
		});
	
	
		
		/*-----------------------------------------------------------------------------------------------------------------*/
		/*
		 *Group item grid
		 */
		jQuery("#PackUnpackGroupItemGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Print','PUNo','Date','Item Code','Item Name','Pack/Unpack','Quantity','Remarks'],
			colModel: [{ name: 'Print', index: 'Print', width:50 },
			           { name: 'PUNo', index: 'PUNo', width: 100},
			           { name: 'Date', index: 'Date', width: 100},
			           { name: 'ItemCode', index: 'ItemCode', width: 70},
			           { name: 'ItemName', index: 'ItemName', width: 120},
			           { name: 'PackUnpack', index: 'PackUnpack', width: 50,formatter:PackUnpackFormatter},
			           { name: 'Qunatity', index: 'Qunatity', width: 70,sorttype:'number'},
			           { name: 'Remarks', index: 'Remarks', width: 100}],
			           pager: jQuery('#PJmap_PackUnpackGroupItemGrid'),
			           width:530,
			           height:450,
			           rowNum: 10000,
			           rowList: [500],
			           hoverrows: true,
			           ignoreCase: true,
			           shrinkToFit: false,
			           ondblClickRow: function (id) {
			        	   
	                		var selectedPUNo = jQuery("#PackUnpackGroupItemGrid").jqGrid('getCell', id, 'PUNo');
	                		
	                		
	                		
	                		
	                		//function to call pack unpack report.
	                		
	                   }, 

			           onSelectRow: function (id) {
			        	   
			        	   var packUnpackNumber = jQuery('#PackUnpackGroupItemGrid').jqGrid('getCell',id, 'PUNo');
			        	   
			        	   itemsInPackUnpackGroup(packUnpackNumber);
		               }

		});
		
		function PackUnpackFormatter(cellvalue, options, row) 
		{
			if (cellvalue == 0) 
			{
				return "Unpack";

			}
			if (cellvalue == 1) 
			{
				return "Pack";

			}
			
		}
		
			
		jQuery("#PackUnpackGroupItemGrid").jqGrid('navGrid','#PJmap_PackUnpackGroupItemGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_PackUnpackGroupItemGrid").hide();
		
		
		function itemsInPackUnpackGroup(packUnpackNumber)
 	   {
			
			jQuery('#PackUnpackItemsInsideGroupGrid').jqGrid('clearGridData', true);
			
			var itemInGroupRowId = 0;
			
			jQuery.each(oItemsInPackInformation,function(key,value){
				
				if(oItemsInPackInformation[key]['PUNo'] == packUnpackNumber)
				{
					itemInGroupRowId += 1;

					var ItemsInPackUnpackData = [{"ItemCode":oItemsInPackInformation[key]['ItemCode'],
						"ItemName":oItemsInPackInformation[key]['ItemName'],
						"Quantity":parseInt(oItemsInPackInformation[key]['Quantity'])}];
					for (var i=0;i<ItemsInPackUnpackData.length;i++) {
						jQuery("#PackUnpackItemsInsideGroupGrid").jqGrid('addRowData', itemInGroupRowId, ItemsInPackUnpackData[ItemsInPackUnpackData.length-i-1], "first");

					}
				}
				
				


			});
 	   }
		
		
		/*-----------------------------------------------------------------------------------------------------------------*/
		/*
		 *Items inside group grid
		 */
		jQuery("#PackUnpackItemsInsideGroupGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Item Name','Quantity'],
			colModel: [{ name: 'ItemCode', index: 'ItemCode', width:100 },
			           { name: 'ItemName', index: 'itemName', width: 250},
			           { name: 'Quantity', index: 'Quantity', width: 70,sorttype:'number'}],
			           pager: jQuery('#PJmap_PackUnpackItemsInsideGroupGrid'),
			           width:450,
			           height:450,
			           rowNum: 10000,
			           rowList: [500],
			           hoverrows: true,
			           ignoreCase: true,
			           shrinkToFit: false

		});
			
		jQuery("#PackUnpackItemsInsideGroupGrid").jqGrid('navGrid','#PJmap_PackUnpackGroupItemGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_PackUnpackItemsInsideGroupGrid").hide();
		
		
		
		/*-----------------------------------------------------------------------------------------------------------------*/
		/*
		 *Pack Unpack create Grid.
		 */
		jQuery("#packUnpackCreateGrid").jqGrid({

			datatype: 'jsonstring',
			colNames: ['Item Code','Short Name','Item Name','Available Qty','Qty/Case Pack','Total Qty'],
			colModel: [{ name: 'ItemCode', index: 'ItemCode', width:100 },
			           { name: 'ShortName', index: 'ShortName', width: 200},
			           { name: 'ItemName', index: 'ItemName', width: 200},
			           { name: 'AvailableQty', index: 'AvailableQty', width: 120},
			           { name: 'QtyCasePack', index: 'QtyCasePack', width: 120},
			           { name: 'TotalQty', index: 'TotalQty', width: 120}],
			           pager: jQuery('#PJmap_packUnpackCreateGrid'),
			           width:1040,
			           height:320,
			           rowNum: 10000,
			           rowList: [500],
			           hoverrows: true,
			           ignoreCase: true,
			           

			           onSelectRow: function (id) {
			        	   
			        	   //var packUnpackNumber = jQuery('#packUnpackCreateGrid').jqGrid('getCell',id, 'PUNo');
			        	   
			        	   //itemsInPackUnpackGroup(packUnpackNumber);
		               }

		});
		
		jQuery("#packUnpackCreateGrid").jqGrid('navGrid','#PJmap_packUnpackCreateGrid',{edit:false,add:false,del:false});	
		jQuery("#PJmap_packUnpackCreateGrid").hide();
		
		/*-----------------------------click on search option---------------------------*/
		jQuery("#btnPUN01Search").unbind("click").click(function(){
			

			showMessage("loading", "Searching availabile...", "");

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'PackUnpackCallback',
				data:
				{
					id:"searchPackUnpack",
					moduleCode:"PUN01",
					functionName:"Search",
					packOrUnpack:jQuery("#packUnpackSelect").val(),
					fromDate:jQuery("#packUnpackFromDate").val(),
					toDate:jQuery("#packUnpackToDate").val(),
					itemCode:jQuery("#packUnpackItemCode").val(),
					itemId:0,
					searchParam:"PU",
			
				},
				success:function(data)
				{

					jQuery(".ajaxLoading").hide();

					var packUnpackSearchResult = jQuery.parseJSON(data);
					var packUnpackSearchResultStatus = packUnpackSearchResult.Status;
					
					if(packUnpackSearchResultStatus == 1)
					{
						
						var oPackAndItemInformation = packUnpackSearchResult.Result;
						
						var oPackInformation = oPackAndItemInformation[0];
						
						oItemsInPackInformation = oPackAndItemInformation[1];
						
						displayPackedItems(oPackInformation);
						
						function displayPackedItems(oPackInformation)
						{
							jQuery('#PackUnpackGroupItemGrid').jqGrid('clearGridData', true);
							
							var rowid = 0;
							jQuery.each(oPackInformation,function(key,value){
								
								rowid += 1;
								
								var ItemsPackUnpackData = [{"Print":"<BUTTON class='logPrintButtons' id='"+rowid+"'>Print</BUTTON>","PUNo":oPackInformation[key]['PUNo'],"Date":oPackInformation[key]['PUDate'],"ItemCode":oPackInformation[key]['ItemCode'],"ItemName":oPackInformation[key]['ItemName'],'PackUnpack':oPackInformation[key]['PU_Flag'],'Qunatity':parseInt(oPackInformation[key]['Quantity']),"Remarks":oPackInformation[key]['Remarks']}];
								for (i=0;i<ItemsPackUnpackData.length;i++) {
									jQuery("#PackUnpackGroupItemGrid").jqGrid('addRowData', rowid, ItemsPackUnpackData[ItemsPackUnpackData.length-i-1], "first");
//									alert(price);
								}
								
							});
							
								
						}
						jQuery(".logPrintButtons").unbind("click").click(function(e){
							e.preventDefault();
 		  					var rowId=this.id;
 		  					var selectedPUNo = jQuery("#PackUnpackGroupItemGrid").jqGrid('getCell', rowId, 'PUNo');
 		  					CallBirtReport(selectedPUNo);
 		  					
 		  				});
					}
					
					else
					{
						showMessage("red", packUnpackSearchResult.Description, "");
					}
					
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
			

		});
		
		jQuery(".logPrintButtons").unbind("click").click(function(e){
			e.preventDefault();
				var rowId=this.id;
				var selectedPUNo = jQuery("#PackUnpackGroupItemGrid").jqGrid('getCell', rowId, 'PUNo');
				CallBirtReport(selectedPUNo);
		});	
		function resetAllItems(){
			jQuery("#packUnpackCreateMRP").val('');
			jQuery("#packUnpackCreateEXP").val('');
			jQuery("#packUnpackCreateRemarks").val('');
			jQuery("#packUnpackCreatePackUnpackQty").val('');
			jQuery("#packUnpackCreateSelect").val(-1);
			jQuery("#packUnpackCreateItemCode").val('');
			jQuery("#packUnpackCreateItemName").val('');
			jQuery("#packunpacktotalQty").val('');
			jQuery("#btnPUN01Save").attr('disabled',true);
			jQuery("#packUnpackCreateGrid").jqGrid('clearGridData');
			jQuery('#btnPUN01Print').attr('disabled',true);
			actionButtonsStauts();
			
		}
		
		jQuery("#packUnpackCreatePackUnpackQty").keyup(function(event){	
	    	   var intRegex = /^\d+$/;
	    	
			 if(  intRegex.test(jQuery("#packUnpackCreatePackUnpackQty").val()) && jQuery("#packUnpackCreatePackUnpackQty").val()!=''){
				if(jQuery("#packUnpackCreateSelect").val()==1){
					 if(parseInt(jQuery("#packunpacktotalQty").val())>=parseInt(jQuery("#packUnpackCreatePackUnpackQty").val())){
						    
							jQuery("#packUnpackCreatePackUnpackQty").css("background","white");
						 }
						 else{
							 showMessage("yellow","UnPack Quantity cannnot be more than Availble Quantity","");
							 jQuery("#packUnpackCreatePackUnpackQty").select();
							 jQuery("#packUnpackCreatePackUnpackQty").focus();
						 }
					
				}
				 
			 }
			 else if(jQuery("#packUnpackCreatePackUnpackQty").val()!=''){
				 //jQuery("#packUnpackCreatePackUnpackQty").val('');
				 jQuery("#packUnpackCreatePackUnpackQty").select();
				 jQuery("#packUnpackCreatePackUnpackQty").focus();

				 showMessage("yellow","Quantity should be numeric.");
			 }

			});

		jQuery("#btnPackUnpackcreateReset").click(function(){
			resetAllItems();

		});
		jQuery("#btnPackUnpackcreateReset2").click(function(){
			resetAllItems();

		});
		
		function resetOnItemChange(){
			jQuery("#packUnpackCreateMRP").val('');
			jQuery("#packUnpackCreateEXP").val('');
			jQuery("#packUnpackCreateRemarks").val('');
			jQuery("#packUnpackCreatePackUnpackQty").val('');
			jQuery("#packUnpackCreateItemName").val('');
			jQuery("#packunpacktotalQty").val('');
			jQuery("#packUnpackCreateGrid").jqGrid('clearGridData');
			showCalenderForPack();
		}
		jQuery("#btnPackUnpackReset").click(function(){
			
			jQuery("#packUnpackSelect").empty();
			
			showCalenderForPack();
			
			packUnpackList();
			
			jQuery("#packUnpackItemCode").val('');
			
			jQuery('#PackUnpackGroupItemGrid').jqGrid('clearGridData', true);
		});
		
		function showCalenderForPack(){
			jQuery(function() {
				jQuery( "#packUnpackFromDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy'});
				jQuery( "#packUnpackFromDate" ).datepicker("setDate",new Date());
			});
			
			jQuery(function() {
				jQuery( "#packUnpackCreatePackUnpackDate" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy'});
				jQuery( "#packUnpackCreatePackUnpackDate" ).datepicker("setDate",new Date());
			});
		}
		
		jQuery("#packUnpackCreateSelect").change(function(){
			resetOnItemChange();
			jQuery("#packUnpackCreateItemCode").val('');
			itemId='';
			oConstituentItems='';
			if(jQuery("#packUnpackCreateSelect").val()==0){
				
				jQuery("#lbexpDate").show();
				jQuery("#txtexpDate").show();
				jQuery("#lbmfgDate").show();
				jQuery("#txtmfgDate").show();
				jQuery("#lbtotalQty").hide();
				jQuery("#txttotalQty").hide();
			}
			else if (jQuery("#packUnpackCreateSelect").val()==1){
				jQuery("#lbexpDate").hide();
				jQuery("#txtexpDate").hide();
				jQuery("#lbmfgDate").hide();
				jQuery("#txtmfgDate").hide();
				jQuery("#lbtotalQty").show();
				jQuery("#txttotalQty").show();
				jQuery("#packunpacktotalQty").attr('disabled',true);
			}
				
		});
		
		jQuery("#packUnpackCreateItemCode").keydown(function(e) {
			resetOnItemChange();
			if((e.which == 13  || e.which == 9) && jQuery("#packUnpackCreateItemCode").val()!='')
			{
				packUnpackGetItemIdFromItemCode();
				
				
			}
		});
		
	/*	jQuery("#packUnpackCreateItemCode").focusout(function() {
			if(jQuery("#packUnpackCreateItemCode").val()!=''){
			resetOnItemChange();
				packUnpackGetItemIdFromItemCode();
			}
			
		});*/
		
		
		/*
		 *function provide item id from item code for composite items.
		 */
		function packUnpackGetItemIdFromItemCode()
		{

			showMessage("loading", "Searching for item details...", "");

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'PackUnpackCallback',
				data:
				{
					id:"searchItemId",
					
					itemCode:jQuery("#packUnpackCreateItemCode").val(),
		
			
				},
				success:function(data)
				{

					jQuery(".ajaxLoading").hide();
					var itemIdSearchResult = jQuery.parseJSON(data);					

					if(itemIdSearchResult.Status == 1)
					{
						//var oItemIdInfo = jQuery.parseJSON(data);
						var oItemIdInfo = itemIdSearchResult.Result;
					if(oItemIdInfo.length!=0){
						jQuery("#packUnpackCreateItemCode").css("background","white");
						itemId = oItemIdInfo[0]['ItemId'];
						//alert("from database"+itemId);
						jQuery("#packUnpackCreateItemId").val(itemId);
						
						jQuery("#packUnpackCreateMRP").val(parseFloat(oItemIdInfo[0]['DistributorPrice'],10).toFixed(2));
						jQuery("#packUnpackCreateItemName").val(oItemIdInfo[0]['ItemName']);
						jQuery("#packunpacktotalQty").val(parseInt(parseFloat(oItemIdInfo[0]['TotalQuantity'],10).toFixed(2)));
						jQuery("#packUnpackCreatePackUnpackQty").focus();
						packUnpackCreateItemFetch();
						
					}
			
						else{
							jQuery("#packUnpackCreateItemCode").css("background","#ff9999");
							
						}
					
					}
					else
					{
						showMessage("red", itemIdSearchResult.Description, "");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
		}
		
		
		/*
		 * fetch pack unpack item information.
		 */
		function packUnpackCreateItemFetch()
		{
			/*------------------------------Fetch saved pack/unpack results-------------------------------*/

			showMessage("loading", "Searching for Composite Items...", "");

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'PackUnpackCallback',
				data:
				{
					id:"searchPackUnpack",
					moduleCode:"PUN01",
					functionName:"Search",
					packOrUnpack:jQuery("#packUnpackCreateSelect").val(),
					fromDate:'',
					toDate:'',
					itemCode:jQuery("#packUnpackCreateItemCode").val(),
					itemId:itemId,
					searchParam:"COMPOSITE",
			
				},
				success:function(data)
				{

					jQuery(".ajaxLoading").hide();

					var packUnpackData = jQuery.parseJSON(data);
					
					if(packUnpackData.Status == 1)
					{
						var oPackUnpackCreateResult = packUnpackData.Result;
						if(oPackUnpackCreateResult[0].length!=0){
											 oConstituentItems = oPackUnpackCreateResult[1];			
											
											displayOtherNecessaryInfoForPackUnpack(oPackUnpackCreateResult[2]);
											
											/*
											 * funciton used to display other info like exp and mfg dates.
											 */
											
								}
						else{
							jQuery("#packUnpackCreateItemCode").css("background","#ff9999");
							
						}
				
					}
					else
					{
						showMessage("red", packUnpackData.Description, "");
					}
					
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});	
		}
		
		
		function displayOtherNecessaryInfoForPackUnpack(oPackUnpackCreateBatchInfo) 
		{		
		jQuery("#packUnpackCreateEXP").val(displayDate(oPackUnpackCreateBatchInfo[0][0]));
			
		}
		/*
		 * function to fetch pack unpack constituent items
		 */
	/*	function packUnpackFetchConstituentItems(itemId)
		{
<<<<<<< HEAD
			
			alert("item id"+itemId);
=======
			showMessage("loading", "Searching for Constituent Items...", "");
		//	alert("item id"+itemId);
>>>>>>> branch 'master' of https://prvn-vrma@bitbucket.org/prvn-vrma/vestige-pos.git
			jQuery.ajax({
				url:Drupal.settings.basePath + 'PackUnpackCallback',
				data:
				{
					id:"searchPackUnpack",
					packOrUnpack:jQuery("#packUnpackCreateSelect").val(),
					fromDate:'',
					toDate:'',
					itemCode:'',
					itemId:itemId,
					searchParam:"CONSTITUENT",
			
				},
				success:function(data)
				{
<<<<<<< HEAD
=======
					jQuery(".ajaxLoading").hide();
>>>>>>> branch 'master' of https://prvn-vrma@bitbucket.org/prvn-vrma/vestige-pos.git
					var constituentItemsData = jQuery.parseJSON(data);
					
					if(constituentItemsData.Status == 1)
					{
						oConstituentItemsInfo = constituentItemsData.Result;
						
						oConstituentItems = oConstituentItemsInfo[0];
					}
					else
					{
						showMessage("red", constituentItemsData.Description, "");
					}
					
				}
			});	
		}*/
		
		/*--------------------change pack unpack create batch option----------------*/
		
		jQuery("#packUnpackCreateMFGBatchSelect").change(function(){
			
			var currentBatchSelected = this.value;
			
			jQuery.each(oPackUnpackCreateBatchInformation,function(key,value){
				
				/*chagne option corresponding to current batch selected in create pack unpack "Unpack" option*/
				if(currentBatchSelected == oPackUnpackCreateBatchInformation[key]['BatchNo'])
				{
					
					jQuery("#packUnpackCreateEXP").val(oPackUnpackCreateBatchInformation[key]['ExpDate']);
					jQuery("#packUnpackCreateMFG").val(oPackUnpackCreateBatchInformation[key]['MfgDate']);
					jQuery("#packUnpackCreateMRP").val(oPackUnpackCreateBatchInformation[key]['MRP']);
				}
					
			});
			
			
			
		});
		
		
		
		jQuery("#packUnpackCreateMFGBatch").keydown(function(e){

			  
			  }); 
		 jQuery("#packUnpackCreateMFG").keydown(function(e){

			  
			  }); 
		 jQuery("#packUnpackCreateEXP").keydown(function(e){

			  
			  });
		 
		 
		 /*function for checking packing quantity for current location*/ 
		 function checkInventoryForConstituent(){
			var gridData= jQuery("#packUnpackCreateGrid").jqGrid('getRowData')
			 for (var i=0; i<gridData.length;i++){
				 if(parseInt(gridData[i].AvailableQty)<parseInt(gridData[i].TotalQty)){
					showMessage('yellow','Inventory not available for Item Code -'+gridData[i].ItemCode,'');
					 jQuery("#packUnpackCreatePackUnpackQty").select();
					 jQuery("#packUnpackCreatePackUnpackQty").focus();

					 return 0;
				 }
				
			 }
			return 1;
		 }
		  
		 		
		
		/*--------------------------------------------process pack unpack -----------------------------------------*/
		jQuery("#btnPackUnpackCreateProcess").click(function(){
			var flag=0;
				/*------------------------check has been validated or not-------------------------*/
			if(validatePackUnpackProcess() == 1)
				{
				if(	jQuery("#packUnpackCreateGrid").jqGrid('getRowData').length>0){
				var r =confirm("Are you sure you want clear processed list?");
				}
				else{
					r=true;
				}
				if(r==true){
				   jQuery("#btnPUN01Save").attr('disabled',false);
				   
				   actionButtonsStauts();
				   
					var quantityToPack = jQuery("#packUnpackCreatePackUnpackQty").val();
					var packUnapckCreateId = 0;
					jQuery("#packUnpackCreateGrid").jqGrid('clearGridData');
					//processing procedure.
					jQuery.each(oConstituentItems,function(key,value){
						packUnapckCreateId += 1;
						var caseQuantity = parseInt(oConstituentItems[key]['Quantity']);
						var totalQuantityToPack = quantityToPack * caseQuantity;
						
						if(totalQuantityToPack>parseInt(parseFloat(oConstituentItems[key]['AvailableQty'],10).toFixed(2)) && jQuery('#packUnpackCreateSelect').val()==0){
							/*flag=1;*/
							/*showMessage('red','Inventory short for '+ oConstituentItems[key]['ItemName']+'. Maximum  Qty ' + parseInt(parseFloat(oConstituentItems[key]['AvailableQty'],10).toFixed(2))/caseQuantity + ' can be pack','');
							return ;*/
						}
						var packUnpackConstituentItems = [{"ItemCode":oConstituentItems[key]['ItemCode'],"ShortName":oConstituentItems[key]['ShortName'],"ItemName":oConstituentItems[key]['ItemName'],"AvailableQty":parseInt(parseFloat(oConstituentItems[key]['AvailableQty'],10).toFixed(2)),'QtyCasePack':caseQuantity,'TotalQty':totalQuantityToPack}];
						for (var i=0;i<packUnpackConstituentItems.length;i++) {
							jQuery("#packUnpackCreateGrid").jqGrid('addRowData', packUnapckCreateId, packUnpackConstituentItems[packUnpackConstituentItems.length-i-1], "first");
//							alert(price);
						}
						
					});
				}
				}
				
			
			if(flag==1 ){ //this flag for clear grid.
				jQuery("#packUnpackCreateGrid").jqGrid('clearGridData');
			}
			
		});
		
		jQuery("#btnPUN01Save").unbind("click").click(function(){
			
			 //string form of json object.
			if(validatePackUnpackProcess() == 1){
					if(jQuery("#packUnpackCreateSelect").val()==0){
						savePack();
					}

					else if(jQuery("#packUnpackCreateSelect").val()==1){
						unPackSave();

					}
			}		
			
		});

		
		function savePack(){
			
					if(checkInventoryForConstituent()==0){
						return ;
					}
					var r =confirm("Are you sure you want to pack items  ?");
					if(r==true){
					showMessage("loading", "Packing Items...", "");
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'PackUnpackCallback',
						data:
						{
							id:"savePack",
							moduleCode:"PUN01",
							functionName:"Save",
							CompositeItemId:itemId,
							DBQuantity:jQuery("#packUnpackCreatePackUnpackQty").val(),
							Remarks:jQuery("#packUnpackCreateRemarks").val()
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();	
							var keypairData = jQuery.parseJSON(data);
							if(keypairData.Status==1){
								keypair=keypairData.Result;
								if(keypair.length>=0){	
								jQuery.each(keypair,function(key,value)
								{	
									if(keypair[key]['PUNo']!=undefined){
										 jQuery("#btnPUN01Save").attr('disabled',true);
											jQuery('#packUnpackNo').val(keypair[key]['PUNo']);
										 //resetAllItems();
									showMessage("green","Record created Successfully  "+keypair[key]['PUNo'],"");
									jQuery('#btnPUN01Print').attr('disabled',false);
									}
									else
										{
										  if(keypair[key]['OutParam']=='VAL0057'){
											  showMessage("yellow","Packing quantity is greater than item(s) quantity at current location. ","");
										  }
										
										}
									
								});
							 }
								else{
									showMessage("red","Packing not done. Error. " + keypairData,"");
								}
						    
							}
							else{
								showMessage('red',keypairData.Description,'');
							}
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					
					});	
					}
		}

		function unPackSave(){
			var r =confirm("Are you sure you want to unpack   ?");
			if(r==true){
					showMessage("loading", "Unpacking Items, wait...", "");
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'PackUnpackCallback',
						data:
						{
							id:"unPackSave",
							moduleCode:"PUN01",
							functionName:"Save",
							CompositeItemId:itemId,
							DBQuantity:jQuery("#packUnpackCreatePackUnpackQty").val(),
							Remarks:jQuery("#packUnpackCreateRemarks").val()
						},
						success:function(data)
						{
				
							jQuery(".ajaxLoading").hide();	
							var keypairData = jQuery.parseJSON(data);
							if(keypairData.Status==1){
								keypair=keypairData.Result;
								if(keypair.length>=0){
									
								jQuery.each(keypair,function(key,value){
											if(keypair[key]['PUNo']!=undefined){
												jQuery("#btnPUN01Save").attr('disabled',true);
												//resetAllItems();
												jQuery('#packUnpackNo').val(keypair[key]['PUNo']);
												showMessage("green","Record created Successfully "+keypair[key]['PUNo'],"");
												jQuery('#btnPUN01Print').attr('disabled',false);
												}
											else
												{
												  if(keypair[key]['OutParam']=='VAL0058'){
													  showMessage("yellow","Unpacking quantity is greater than quantity available at current location.","");
												  }
												  if(keypair[key]['OutParam']=='VAL0059'){
													  showMessage("yellow","Cannot unpack, packing batches not found. Contact system admin.","");
												  }
												
												}
											
										});
							 }
								else{
									showMessage("red","Unpacking not done. Error" + keypair ,"");
								}
						    
							}
							else{
								showMessage('red',keypairData.Description,'');
							}
						
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});	
			}
		}
		function validatePackUnpackProcess()
		{
			
			
		
			if(jQuery("#packUnpackCreateItemCode").val() == '')
			{
				jQuery("#packUnpackCreateItemCode").css("background","#ff9999");
				showMessage('yellow','Enter Item Code.','');
				jQuery("#packUnpackCreateItemCode").select();
				jQuery("#packUnpackCreateItemCode").focus();
				return 0;
				
			}
			else
			{
				jQuery("#packUnpackCreateItemCode").css("background","white");
				
			}
			if(jQuery("#packUnpackCreatePackUnpackQty").val() == '' || parseInt(parseFloat(jQuery("#packUnpackCreatePackUnpackQty").val(),10).toFixed(2))<=0)
			{
				jQuery("#packUnpackCreatePackUnpackQty").css("background","#ff9999");
				showMessage('yellow','Enter Valid Quantity.','');
				jQuery("#packUnpackCreatePackUnpackQty").focus();
				return 0;
				
			}
			else
			{
				jQuery("#packUnpackCreatePackUnpackQty").css("background","white");
			}
			if(jQuery("#packUnpackCreateMRP").val() == '')
			{
				return 0;
				
			}
			else
			{
				jQuery("#packUnpackCreateMRP").css("background","white");
			}
			
			return 1;
		}
	
	jQuery("#btnPUN01Print").click(function(){
		       
		     CallBirtReport(jQuery('#packUnpackNo').val());
			});	
	
	
function CallBirtReport(PUNo){
		
	jQuery("#actionName").val('Print');
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'PackUnpackCallback',
		data:
		{
			id:"printPU",
			PUNo:PUNo,
			functionName:jQuery("#actionName").val(),
			moduleCode:"PUN01"
		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			var resultsData = jQuery.parseJSON(data);
		if(resultsData.Status==1){
			var results=resultsData.Result;
			 jQuery.each(results,function(key,value)
					   {
				 			var arr = {'locationNo':results[key]['locationId'],'PUNo':results[key]['PUNo']};
				 			showReport(results[key]['baseUrl'],results[key]['Url'],arr);
						 
				      });
		}
		else{
			
			showMessage('red',resultsData.Description,'');
		}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	});


	}
	

		
		
});		
