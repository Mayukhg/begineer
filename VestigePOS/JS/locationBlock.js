$( document ).ready(function() {
	
	var getGridData=new Array();
    jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LocationBlockData',
				data:
				{
					id:"FetchLocForLoginLoc"
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").show();
					
					//alert(data);
					var getResult = jQuery.parseJSON(data);
					
				if(getResult.Status == 1)
				{
            console.log("hii");
					var loggedInUserLocation  = getResult.Result;

					if(loggedInUserLocation.length == 0)
					{
						//jQuery(".GRNCreateActionButtons").attr('disabled',true);
					}
					else
					{	getGridData=loggedInUserLocation;
						
						
						var rowid = 0;
						
						$("#list4").jqGrid("clearGridData");
						$.each(loggedInUserLocation, function (key, value) {
        	
		
			
							var getGridData = 
							[{
								"id": loggedInUserLocation[key]['id'],
								"Location": loggedInUserLocation[key]['Location'],
								"LocationCode": loggedInUserLocation[key]['LocationCode'],
								"Type": loggedInUserLocation[key]['Type'],
								"action": loggedInUserLocation[key]['action'],
								"TypeVal": loggedInUserLocation[key]['TypeVal'],
								"CurrentStatus": loggedInUserLocation[key]['CurrentStatus']
								
								
							}];	
							for (var i = 0; i < getGridData.length; i++) 
							{
								jQuery("#list4").jqGrid('addRowData', rowid, getGridData[getGridData.length - i - 1], "last");
								rowid = rowid + 1;
							}
					});
						
						
						
						
						
						
						
						
						
						
						
					}	
					
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
	
	
	
	 

    /* 'use strict';
     getGridData = [{
           "id":searchTO[key]['InvoiceNo'],"Location":searchTO[key]['CustomerOrderNo'],"LocationCode":searchTO[key]['StatusName'],
           "Type":searchTO[key]['Status'],"action":searchTO[key]['LogNo']
       
          }];,
        theGrid = $("#list4"),
        numberTemplate = {formatter: 'number', align: 'right', sorttype: 'number'} 
  */
 
    jQuery("#list4").jqGrid({
        datatype: 'jsonstring',
       data:getGridData,
        colNames: ['Location', 'Location Code',  'Location Type','Action','TypeVal','CurrentStatus'],
        colModel: [
            
                  {name: 'Location', index: 'Location',align: 'center',search:true,stype:'text', width: 286},
            {name: 'LocationCode', index: 'LocationCode',search:true,sorttype:'integer', width: 286, align: 'center', 
                },
            {name: 'Type', index: 'Type', align: 'center', width: 214, search:false },
			 {name:'action',index:'action',width: 230,align: 'center',sortable:false, formatter: displayButtons,search:false },
			{name: 'TypeVal', index: 'TypeVal', hidden:'true', align: 'center', width: 180},
			{name: 'CurrentStatus', hidden:'true', index: 'CurrentStatus', align: 'center', width: 180},
			
                            
        ],
		
		           
		          
		           viewrecords: true,
		           gridview: true,
		           sortorder: "asc",
		          	           	           	           
			gridview: true,             
			rownumbers: true,
			rowNum: 50,
			rowList: [50, 100, 150],
			pager: '#gridPager',
			viewrecords: true,  
			loadonce:true,
			loadtext: "Loading...",
            sortable: true,
           sortorder: "asc",
           ignoreCase: true,
          height:300
            
           
           
           
		
        
    });
	 jQuery("#list4").jqGrid('navGrid','#gridPager',{del:false,add:false,edit:false,search:true});
	jQuery("#list4").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false,defaultSearch: 'cn'});
});
 
function displayButtons(cell, options, rowObject)
    {  debugger;
	var class1='';
		 var rowData = rowObject.CurrentStatus;
		 if (rowData=='Unblock')
		 {
			 var class1='button3';
			 
		 }
		 else 
			 if(rowData=='Block')
		 {
			 var class1='button';
		 }
		
		
        var edit = "<input class='"+class1+"' type='button' value='"+rowData+"' onclick=\"BlockLocation(this,'" + options.rowId + "',this.value);\"  />"
             ;
        return edit;
    }
	
	
	function BlockLocation(button,id,val){
	
	var LocationType = jQuery('#list4').jqGrid ('getCell', id, 'TypeVal');
	var LocationId = jQuery('#list4').jqGrid ('getCell', id, 'LocationCode');
	
	// button.setAttribute("disabled", "true");
	 button.setAttribute("class", "button2");
    button.setAttribute("value", "Wait..");
	if (val=='Unblock')
	{
	
		
		jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LocationBlockData',
				data:
				{
					id:"BlockUnblock",
					LocationType:LocationType,
					LocationId:LocationId,
					mode:'Unblock',
					functionName:'Block/Unblock',
					moduleCode:'BL001'
					
				},
				success:function(data)
				{	debugger;
					var getResult = jQuery.parseJSON(data);
					if(getResult.Status == 1)
				{
						console.log("Success"+LocationId+" is "+val+"");
					 button.setAttribute("value", "Block");
						button.setAttribute("class", "button");
						
					
				}
				
		},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					
					showMessage("", defaultMessage, "");
				}
			
			});
    }
	else if (val=='Block') 
	{
	 
	
	jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LocationBlockData',
				data:
				{
					id:"BlockUnblock",
					LocationType:LocationType,
					LocationId:LocationId,
					mode:'Block',
					functionName:'Block/Unblock',
					moduleCode:'BL001'
					
				},
				success:function(data)
				{
					button.setAttribute("value", "Unblock");
					
					var getResult = jQuery.parseJSON(data);
					if(getResult.Status == 1)
				{
					console.log("Success"+LocationId+" is "+val+"");
					button.setAttribute("class", "button3");
					
					
				
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					button.setAttribute("disabled", "False");
					showMessage("", defaultMessage, "");
					
				}
			
			});
		
		
		
		
		
		
	 
	
	
    }
	 
	}