function distributorRequestingDBR(){
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'POSClient',
		data:
		{
			id:"distributorRequestingDBR",
		},
		success:function(data)
		{
			var oResult = jQuery.parseJSON(data);
			
			var reportBaseURL = "birt-viewer";
			var reportUrl = "/getreport/requestDBRReport";

			if(oResult.Status == 1)
			{
				searchForDBRReport=oResult.Result;
				var DBRReportCount = searchForDBRReport[0].DistributorRequestingDBR.split(',');
				showReport(searchForDBRReport[0]["reportUrl"],searchForDBRReport[0]["reportPath"],searchForDBRReport[0],'',DBRReportCount.length);
				
				/*---commented  code ----*/
				/*
				var form = document.createElement("form");

				form.setAttribute("method", "POST");

				form.setAttribute("action", reportBaseURL+reportUrl);


				form.setAttribute("target", "formresult");
				*/
				
				/*----------000------------*/
				
				//var reportName = document.createElement("input");   
				//reportName.setAttribute("name","__report");
				//reportName.setAttribute("value",reportUrl);
				//reportName.setAttribute("type",'hidden');
				//form.appendChild(reportName);

				//var params = new Array();
				
				/*---commented  code ----*/

				/*
				jQuery.each(distributorReqDBRReport,function(key,value){
					
					var key = key;
					var value = reportParam[keys];
					var hiddenField = document.createElement('input');
					hiddenField.setAttribute("name",key);
					hiddenField.setAttribute("value",value);
					hiddenField.setAttribute("type",'hidden');
					form.appendChild(hiddenField);
					
				});
				*/
				
				/*---000----*/
				
				/*for(keys in reportParam){
					var key = keys;
					var value = reportParam[keys];
					var hiddenField = document.createElement('input');
					hiddenField.setAttribute("name",key);
					hiddenField.setAttribute("value",value);
					hiddenField.setAttribute("type",'hidden');
					form.appendChild(hiddenField);
				}*/

				/*---commented  code ----*/
				/*
				var hiddenField = document.createElement('input');
				hiddenField.setAttribute("name","GeneratedBy");
				hiddenField.setAttribute("value",loggedInUserFullName);
				hiddenField.setAttribute("type",'hidden');
				form.appendChild(hiddenField);

				document.body.appendChild(form);
				*/
				
				/*---000----*/

				/*htmlToAppendToWindowOpen = "<html><head><script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>" +
						"<script src='http://localhost/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/JS/progressBarScript.js'></script></head><body>" +
						"<div id='dialog'><div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div></div></body></html>";*/

				// creating the 'formresult' window with custom features prior to submitting the form
				
				
				/*---commented  code ----*/
				/*
				var result = window.open('', 'formresult', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');

				if(result){
				*/
					/*---000----*/	

					/*var divElement = result.document.createElement("div");

					divElement.setAttribute("id", "dialog");

					result.document.body.appendChild(divElement);

					jQuery("#dialog").append("<div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div>");*/

					//result.document.write("<div id='dialog'><div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div></div>");

					//result.document.write(htmlToAppendToWindowOpen);

					//showProgressBarForLogReport(progressCount);

				/*---commented  code ----*/
				/*
					form.submit();
				}
				else{
					alert("Error in submitting to url");
				}
				*/
			/*---000 ----*/
			}
			else
			{
				showMessage("red", oResult.Description, "");
			}
			
		},// end success.
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	});
	
}