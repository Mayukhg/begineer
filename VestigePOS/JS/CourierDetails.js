jQuery(document).ready(function(){
	
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	jQuery("#referenceLog").val(getParameterByName('LogNumber'));
	var chromeBrowser = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()); 

	if(!chromeBrowser){
	 
		alert("Update your browser to Chrome and use again");
	
		window.location.replace("http://180.179.67.207/drupal-7.22/Login");
	}
	else
	{
		//jQuery(".content").hide();
		jQuery("#main-menu").hide();

		jQuery(".breadcrumb").hide();

		jQuery("#triptych-wrapper").hide();

		jQuery(".title").hide();

		jQuery("#footer-wrapper").hide();
		
		jQuery("#locationOption").hide(); //Location selection option hidden initially.
		
		jQuery("#login").hide(); //hide login button 
		
		//Login pop up.
		jQuery(function() {
		    jQuery( "#loginScreenPopUp" ).dialog({
		      autoOpen: true,
		      minWidth: 600,
		      modal:true,
		      show: {
		        effect: "clip",
		        duration: 200
		      },
		      hide: {
		        effect: "clip",
		        duration: 200
		      }
		   });
		});
		
		jQuery("#Cancel").click(function(){
			window.close();
		});
		
		jQuery("#noOfBoxes").keyup(function(event){
			var intRegex = /^\d+$/;

			if(  !intRegex.test(jQuery("#noOfBoxes").val()) && jQuery("#noOfBoxes").val()!=''){
				jQuery("#noOfBoxes").val('');
				showMessage("","Enter valid no of boxes .","");
			}
			jQuery("#TOCreatePackSize").css("background","white");
		});
		jQuery("#weightInGram").keyup(function(event){
			var intRegex = /^\d+$/;

			if(  !intRegex.test(jQuery("#weightInGram").val()) && jQuery("#weightInGram").val()!=''){
				jQuery("#weightInGram").val('');
				showMessage("","Enter valid no of boxes .","");
			}
			jQuery("#TOCreatePackSize").css("background","white");
		});
		function vallidationOnSave(){
		if(	jQuery("#docketNo").val()==''){
			showMessage('','Enter DocketNo','');
			return 0;
		}
		if(	jQuery("#courierCompanyName").val()==''){
			showMessage('','Enter DocketNo','');
			return 0;
			}
		if(	jQuery("#noOfBoxes").val()==''){
			showMessage('','Enter noOfBoxes','');
			return 0;
		}
		if(	jQuery("#weightInGram").val()==''){
			showMessage('','Enter weightInGram','');
			return 0;
		}
	    return 1;
		}
		
		jQuery("#save").click(function(){
			
			jQuery("#referenceLog").attr('disabled',false);
			jQuery("#itemsWeight").attr('disabled',false);
			
			courierDetailFormData = jQuery("#courierDetailsForm").serializeArray();
			
			var courierDetailJson = convertArrayToJson(courierDetailFormData);
			
			jQuery("#referenceLog").attr('disabled',true);
			jQuery("#itemsWeight").attr('disabled',true);
		if(vallidationOnSave()==1){
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'CourierDetailHandler',
				
				data:
				{
					id:"courierDetailSave",
					courierDetailFormData:courierDetailJson,
					
				},
				success:function(data)
				{
					showMessage('green','Record successfully update ','');

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});
		}
		else{
			
		}
			
			
		});


	}
	

});
