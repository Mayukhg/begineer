
var rowid = 0;
var historyOrderNo; 
var loggedInUserPrivileges;
var jsonForLocations ;
var expectedDate ;
jQuery(document).ready(function(){
	jQuery(".breadcrumb").hide();
jQuery("#PaymentGatewayRecordGrid").jqGrid({
    
    
    datatype: 'jsonstring',
    colNames: ['AddToLog','Status','Payment Date','Transaction Id','Payment Status','Total Amount','Order No','Order Status','Distributor Id','LocationName','Log No','Distributor Name','Order Date','OrderType','Remarks'
/*		                ,'DeliverToCityName',
               'DeliverToStateName','DeliverToCountryName','DeliverFromCityName','DeliverFromStateName','DeliverFromCountryName','IsPrintAllowed','InvoiceDate','ValidReportPrintDays',
               'OrderMode','UsedForRegistration','TerminalCode','TotalBV','TotalPV','DistributorAddress','OrderType','PCId','PCCode','BOId','DeliverFromAddress1','DelverFromAddress2',
               'DeliverFromAddress3','DeliverFromAddress4','DeliverFromCityId','DeliverFromPincode','DeliverFromStateId','DeliverFromCountryId','DeliverFromTelephone','DeliverFromMobile',
               'DeliverToMobile','TotalUnits','TotalWeight','OrderAmount','DiscountAmount','TaxAmount','PaymentAmount','ChangeAmount','CreatedBy','CreatedByName','CreatedDate',
               'DeliverToAddressLine1','DeliverTo','DeliverToAddressLine2','DeliverToAddressLine3','DeliverToAddressLine4','DeliverToCityId','DeliverToPincode','DeliverToStateId',
               'DeliverToCountryId','DeliverToTelephone'
               ,'LogValue'*/
               ],
    colModel: [

               { name: 'AddToLog', index: 'AddToLog', width: 50,edittype:'checkbox',formatter: 'checkbox',editoptions: { value:"True:False"},classes:'AddToLogSelection',editable:true,formatoptions: {disabled : false},hidden:true},
               { name: 'Status', index: 'Status', width:100,hidden:true},
               { name: 'PaymentDate', index: 'PaymentDate', width:150},
               { name: 'TransactionId', index: 'TransactionId', width:160 },
               { name: 'Payment Status', index: 'Payment Status', width:110},
               { name: 'TotalAmount', index: 'TotalAmount', width:95 },
               { name: 'OrderNo', index: 'OrderNo', width:160 },
               { name: 'OrderStatus', index: 'OrderStatus', width:160 },
               { name: 'DistributorId', index: 'DistributorId', width:95 },
               { name: 'LocationName', index: 'LocationName', width:100 },
               { name: 'LogNo', index: 'LogNo', width:170 },
               
              
               
               { name: 'DistributorName', index: 'DistributorName', width:150 },
               { name: 'OrderDate', index: 'OrderDate', width:100 },
               
               { name: 'OrderType', index: 'OrderType', width:200,hidden:true},
               { name: 'Remarks', index: 'Remarks', width:200},
               
             /*  { name: 'DeliverToCityName', index: 'DeliverToCityName', width:20 ,hidden:true},
               { name: 'DeliverToStateName', index: 'DeliverToStateName', width:20 ,hidden:true},
               { name: 'DeliverToCountryName', index: 'DeliverToCountryName', width:20 ,hidden:true},
               { name: 'DeliverFromCityName', index: 'DeliverFromCityName', width:20 ,hidden:true},
               { name: 'DeliverFromStateName', index: 'DeliverFromStateName', width:20 ,hidden:true},
               { name: 'DeliverFromCountryName', index: 'DeliverFromCountryName', width:20 ,hidden:true},
               { name: 'IsPrintAllowed', index: 'IsPrintAllowed', width:20 ,hidden:true},
               { name: 'InvoiceDate', index: 'InvoiceDate', width:20 ,hidden:true},
               { name: 'ValidReportPrintDays', index: 'ValidReportPrintDays', width:20 ,hidden:true},
               { name: 'OrderMode', index: 'OrderMode', width:20 ,hidden:true},
               { name: 'UsedForRegistration', index: 'UsedForRegistration', width:20 ,hidden:true},
               { name: 'TerminalCode', index: 'TerminalCode', width:20 ,hidden:true},
               { name: 'TotalBV', index: 'TotalBV', width:20 ,hidden:true},
               { name: 'TotalPV', index: 'TotalPV', width:20 ,hidden:true},
               { name: 'DistributorAddress', index: 'DistributorAddress', width:20 ,hidden:true},          
               { name: 'OrderType', index: 'OrderType', width:20 ,hidden:true},
               { name: 'PCId', index: 'PCId', width:20 ,hidden:true}, 
               { name: 'PCCode', index: 'PCCode', width:20 ,hidden:true},
               { name: 'BOId', index: 'BOId', width:20 ,hidden:true},
               

               { name: 'DeliverFromAddress1', index: 'DeliverFromAddress1', width:20 ,hidden:true},
               { name: 'DelverFromAddress2', index: 'DelverFromAddress2', width:20 ,hidden:true},
               { name: 'DeliverFromAddress3', index: 'DeliverFromAddress3', width:20 ,hidden:true},
               { name: 'DeliverFromAddress4', index: 'DeliverFromAddress4', width:20 ,hidden:true}, 
               { name: 'DeliverFromCityId', index: 'DeliverFromCityId', width:20 ,hidden:true},
               { name: 'DeliverFromPincode', index: 'DeliverFromPincode', width:20 ,hidden:true},
               { name: 'DeliverFromStateId', index: 'DeliverFromStateId', width:20 ,hidden:true},
               { name: 'DeliverFromCountryId', index: 'DeliverFromCountryId', width:20 ,hidden:true},
               { name: 'DeliverFromTelephone', index: 'DeliverFromTelephone', width:20 ,hidden:true},
               { name: 'DeliverFromMobile', index: 'DeliverFromMobile', width:20 ,hidden:true}, 
              
               { name: 'DeliverToMobile', index: 'DeliverToMobile', width:20 ,hidden:true},
               { name: 'TotalUnits', index: 'TotalUnits', width:20 ,hidden:true},
               { name: 'TotalWeight', index: 'TotalWeight', width:20 ,hidden:true},
               { name: 'OrderAmount', index: 'OrderAmount', width:20 ,hidden:true},
               { name: 'DiscountAmount', index: 'DiscountAmount', width:20 ,hidden:true},
               { name: 'TaxAmount', index: 'TaxAmount', width:20 ,hidden:true},
               { name: 'PaymentAmount', index: 'PaymentAmount', width:20 ,hidden:true},
               { name: 'ChangeAmount', index: 'ChangeAmount', width:20 ,hidden:true},
               { name: 'CreatedBy', index: 'CreatedBy', width:20 ,hidden:true},
               { name: 'CreatedByName', index: 'CreatedByName', width:20 ,hidden:true},
               { name: 'CreatedDate', index: 'CreatedDate', width:20 ,hidden:true},
               
               { name: 'DeliverToAddressLine1', index: 'DeliverToAddressLine1', width:20,hidden:true },
               { name: 'DeliverTo', index: 'DeliverTo', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine2', index: 'DeliverToAddressLine2', width:20,hidden:true },
               { name: 'DeliverToAddressLine3', index: 'DeliverToAddressLine3', width:20 ,hidden:true},
               { name: 'DeliverToAddressLine4', index: 'DeliverToAddressLine4', width:20 ,hidden:true},
               { name: 'DeliverToCityId', index: 'DeliverToCityId', width:20 ,hidden:true},
               { name: 'DeliverToPincode', index: 'DeliverToPincode', width:20 ,hidden:true},
               { name: 'DeliverToStateId', index: 'DeliverToStateId', width:20 ,hidden:true},
               { name: 'DeliverToCountryId', index: 'DeliverToCountryId', width:20 ,hidden:true},
               { name: 'DeliverToTelephone', index: 'DeliverToTelephone', width:20 ,hidden:true},
             
               
           
            
        
               { name: 'LogValue', index: 'LogValue', width:20, hidden:true }
*/		              
               ],
               pager: jQuery('#GatewayGridPager'),
               width:925,
               height:300,
               rowNum: 1000,
               rowList: [500],
               sortable: true,
               sortorder: "asc",
               hoverrows: true,
               shrinkToFit: false,
               ondblClickRow: function (id)
               {
            	   dblclkAction(id);
               }
               		


   });
	
	jQuery("#PaymentGatewayRecordGrid").jqGrid('navGrid','#GatewayGridPager',{edit:false,add:false,del:false}); 
	jQuery("#GatewayGridPager").hide();	
	

	
jQuery("#btnPGR01Search").unbind("click").click(function(){
	
	jQuery("#actionName").val('');
	showMessage("loading", "Searching Payments...", "");
	
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'PaymentGatewayRecordtCallBack',
			data:
			{
				
				id:"searchPaymentGatewayRecord",
				HistoryOrderNo:jQuery("#historyOrderNo").val(),
				moduleCode:"PGR01",
				historyLogNo:jQuery("#historyLogNo").val(),
				HistoryFromDate:jQuery("#historyFromDate").val(),
				historyToDate:jQuery("#historyToDate").val(),
			    historyDistributorNo:jQuery("#historyDistributorNo").val(),
			    functionName:"Search",
				HistoryStatus:jQuery("#HistoryStatus").val(),
				HistoryAmount:jQuery("#Amount").val(),
				Status:jQuery("#PaymentStatus").val(),
			},
			success:function(data)
			{

				jQuery(".ajaxLoading").hide();

				var historyOrderData = jQuery.parseJSON(data);
				
				if(historyOrderData.Status == 1)
				{
					
					
					
					var historyrowid=0;
					var searchTO = historyOrderData.Result;
					if(searchTO.length==0)
					{
						jQuery("#PaymentGatewayRecordGrid").jqGrid("clearGridData");
						showMessage("","No Record Found","");
					}

					else
					{	
						var message = "Total " + searchTO.length + " records found.";
						showMessage("", message,"");
						
						/*['Add To Log','Print','Invoice','Order No','Status','Log No'],*/
						jQuery("#PaymentGatewayRecordGrid").jqGrid("clearGridData");
						jQuery.each(searchTO,function(key,value){	
							
							

							var newData = [{
								"InvoiceNo":searchTO[key]['InvoiceNo'],"OrderNo":searchTO[key]['customerorderno'],"OrderStatus":searchTO[key]['OrderStatus'],"StatusName":searchTO[key]['status'],
								"Status":searchTO[key]['Status'],"LogNo":searchTO[key]['logno'],"DistributorId":searchTO[key]['distributorid'],"LocationName":searchTO[key]['Name'],"DistributorName":searchTO[key]['DistributorName'],
								"OrderDate":displayDate(searchTO[key]['date']),"TotalAmount":searchTO[key]['transactionamount'],"OrderType":searchTO[key]['OrderType'],
								"PC":searchTO[key]['PCCode'],"TransactionId":searchTO[key]['transactionid'],"Payment Status":searchTO[key]['transactionstatus'],"PaymentDate":searchTO[key]['InitiatedDateTime'],"Remarks":searchTO[key]['OrderRemarks']
														/*		"DeliverToCityName":searchTO[key]['DeliverToCityName'],"DeliverToStateName":searchTO[key]['DeliverToStateName'],"DeliverToCountryName":searchTO[key]['DeliverToCountryName'],
								"DeliverFromCityName":searchTO[key]['DeliverFromCityName'],"DeliverFromCountryName":searchTO[key]['DeliverFromCountryName'],"IsPrintAllowed":searchTO[key]['IsPrintAllowed'],
								"InvoiceDate":searchTO[key]['InvoiceDate'],"ValidReportPrintDays":searchTO[key]['ValidReportPrintDays'],"OrderMode":searchTO[key]['OrderMode'],"UsedForRegistration":searchTO[key]['UsedForRegistration'],"TerminalCode":searchTO[key]['TerminalCode'],
								"TotalBV":searchTO[key]['TotalBV'],"TotalPV":searchTO[key]['TotalPV'],
								"DistributorAddress":searchTO[key]['DistributorAddress'],"OrderType":searchTO[key]['OrderType'],"PCId":searchTO[key]['PCId'],"PCCode":searchTO[key]['PCCode'],"BOId":searchTO[key]['BOId'],"DeliverFromAddress1":searchTO[key]['DeliverFromAddress1'],"DelverFromAddress2":searchTO[key]['DelverFromAddress2'],
								"DeliverFromAddress3":searchTO[key]['DeliverFromAddress3'],"DeliverFromAddress4":searchTO[key]['DeliverFromAddress4'],"DeliverFromCityId":searchTO[key]['DeliverFromCityId'],"DeliverFromPincode":searchTO[key]['DeliverFromPincode'],
								"DeliverFromStateId":searchTO[key]['DeliverFromStateId'],"DeliverFromCountryId":searchTO[key]['DeliverFromCountryId'],"DeliverFromTelephone":searchTO[key]['DeliverFromTelephone'],"DeliverFromMobile":searchTO[key]['DeliverFromMobile'],
								"DeliverToMobile":searchTO[key]['DeliverToMobile'],"TotalUnits":searchTO[key]['TotalUnits'],"TotalWeight":searchTO[key]['TotalWeight'],
								"OrderAmount":searchTO[key]['OrderAmount'],"DiscountAmount":searchTO[key]['DiscountAmount'],"TaxAmount":searchTO[key]['TaxAmount'],
								"PaymentAmount":searchTO[key]['PaymentAmount'],"ChangeAmount":searchTO[key]['ChangeAmount'],"CreatedBy":searchTO[key]['CreatedBy'],"CreatedByName":searchTO[key]['CreatedByName'],
								"CreatedDate":searchTO[key]['CreatedDate'],"DeliverToAddressLine1":searchTO[key]['DeliverToAddressLine1'],"DeliverTo":searchTO[key]['DeliverTo'],"DeliverToAddressLine2":searchTO[key]['DeliverToAddressLine2'],
								"DeliverToAddressLine3":searchTO[key]['DeliverToAddressLine3'],"DeliverToAddressLine4":searchTO[key]['DeliverToAddressLine4'],"DeliverToCityId":searchTO[key]['DeliverToCityId'],"DeliverToPincode":searchTO[key]['DeliverToPincode'],
								"DeliverToStateId":searchTO[key]['DeliverToStateId'],"DeliverToCountryId":searchTO[key]['DeliverToCountryId'],"DeliverToTelephone":searchTO[key]['DeliverToTelephone'],
								"LogValue":searchTO[key]['LogValue'],*/

							}];

							for (var i=0;i<newData.length;i++) {
								jQuery("#PaymentGatewayRecordGrid").jqGrid('addRowData',historyrowid, newData[newData.length-i-1], "first");
								historyrowid=historyrowid+1;
							}
						});

					}
				}
				else
				{
					showMessage("red", historyOrderData.Description, "");
				}
			
			},// end success.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
});

showCalender();
jQuery("#btnReset").click(function(){
	 jQuery("#historyLogNo").val('');
		jQuery("#historyDistributorNo").val('');
			 jQuery("#historyOrderNo").val('');
			 jQuery("#Amount").val('');
			 jQuery("#PaymentStatus").val(-1);
   	
       		jQuery("#PaymentGatewayRecordGrid").jqGrid("clearGridData"); 
       		showCalender();
       		
	
});

jQuery("#btnPGR01Report").unbind("click").click(function(){
	
	showMessage("loading", "Generating Report...", "");
	
	var DistributorNo =jQuery("#historyDistributorNo").val();
	var OrderNo =jQuery("#historyOrderNo").val();
	var LogNo=jQuery("#historyLogNo").val();
	var FromDate=jQuery("#historyFromDate").val();
	var ToDate=jQuery("#historyToDate").val();
	var Status=jQuery("#PaymentStatus").val();
	
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'PaymentGatewayRecordtCallBack',
		data:
		{
			id:"Paymentgatewayreport",
			DistributorNo:DistributorNo,
			LogNo:LogNo,
			FromDate:FromDate,
			ToDate:ToDate,
			Status:Status,
			functionName:"Save",
			moduleCode:"PGR01",
			OrderNo:OrderNo,
			
		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				jQuery.each(results,function(key,value){
					 
					var arr = {'locationId':results[key]['LocationId'],'orderid':results[key]['OrderNo'],'historyLogNo':results[key]['LogNo'],'historyDistributorNo':results[key]['DistributorID'],'HistoryFromDate':results[key]['FromDate'],'historyToDate':results[key]['ToDate'],'Status':results[key]['Status'],'GeneratedBy':results[key]['UserName']};
					
				var	reportBaseURL ='birt-viewer/frameset?__format=xlsx&__report=' ;
				                     
					showReport(reportBaseURL,results[key]['Url'],arr);

				});
			}
			else{

				showMessage('red',resultsData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
		
	});
	
});

jQuery("#btnPGR01repayment").unbind("click").click(function(){
	
	batchRow= jQuery("#PaymentGatewayRecordGrid").getGridParam('selrow');
	
	if(batchRow!= null){
		jQuery("#actionName").val('Save');
			var selRowId=jQuery("#PaymentGatewayRecordGrid").jqGrid('getRowData',batchRow);
		
		
		var LogNo=selRowId.LogNo;
		
			
		var r = confirm("Are you sure to repayment ?");
		if(r==true)	{	
			showMessage("loading", "Repayment...", "");
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'PaymentGatewayRecordtCallBack',
			data:
			{
				id:"AllowRepayment",
				LogNo:LogNo,
				functionName:jQuery("#actionName").val(),
				moduleCode:"PGR01"
				
			},
			success:function(data)
			{
			jQuery(".ajaxLoading").hide();
			var MovedData = jQuery.parseJSON(data);
			if(MovedData.Status==1){
				var keypair=MovedData.Result;
				if(keypair.length>=0){

					jQuery.each(keypair,function(key,value)
							{
						
						showMessage("green","Log Repayment "+ keypair[key]['logno']);
							});


				}
				else{
					showMessage("red","Log Repayment. Error " + keypair ,"");
				}
			}
			else {
				showMessage('red',MovedData.Description,'');
			}

			
			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
	
});	}
		
	}
	else {
		showMessage("red","Please select log From list" ,"");
	}
});
});
//function used to print invoice.
function showInvoiceReport(orderNo,originalOrDuplicate)
{
	jQuery("#actionName").val('');
	//jQuery("#actionName").val('Print');
	
	showMessage("loading", "Opening Invoice Preview...", "");

	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'POSClient',
		data:
		{
			id:"invoicePrint",
			orderNo:orderNo,
			functionName:"PrintOrder",
			moduleCode:"PGR01",
			originalOrDuplicate:originalOrDuplicate
		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				jQuery.each(results,function(key,value){
					var arr = {'locationId':results[key]['locationId'],'orderID':results[key]['orderNo'],'isDuplicate':results[key]['isDuplicate'],'GeneratedBy':loggedInUserFullName}; //invocie copy parameter need to watch

					showReport(results[key]['baseUrl'],results[key]['Url'],arr,0); //last parameter to define report called using php java bridge library.


				});
			}
			else{

				showMessage('red',resultsData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	});
}

/*----------------------------------------------------*/
function showOrderReport(orderNo)
{
	//jQuery("#actionName").val('Print');
	jQuery("#actionName").val('');
	
	showMessage("loading", "Opening invoice/order preview...", "");

	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'POSClient',
		data:
		{
			id:"OrderPrint",
			orderNo:orderNo,
			functionName:"PrintOrder",
			moduleCode:"PGR01",
			originalOrDuplicate:1
		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1){
				var results=resultsData.Result;
				jQuery.each(results,function(key,value){
					var arr = {'locationId':results[key]['locationId'],'orderID':results[key]['orderNo'],'isDuplicate':results[key]['isDuplicate'],'GeneratedBy':loggedInUserFullName}; //invocie copy parameter need to watch

					showReport(results[key]['baseUrl'],results[key]['Url'],arr,0); //last parameter to define report called using php java bridge library.


				});
			}
			else{

				showMessage('red',resultsData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	});
}


/*-------------------------------------------*/
var orderStatus;
function showOrderStatus(orderNo)
{
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'PaymentGatewayRecordtCallBack',
		data:
		{
			id:"OrderStatus",
			functionName:"Save",
			moduleCode:"PGR01",
			orderNo:orderNo,
			
		},
		success:function(data)
		{
			jQuery(".ajaxLoading").hide();
			
			var resultsData = jQuery.parseJSON(data);
			if(resultsData.Status==1)
			{
				var results=resultsData.Result;
				jQuery.each(results,function(key,value){
					orderStatus=results[key]['Status'];	
				   	if(orderStatus==4)
			   		{
			   		showInvoiceReport(orderNo,1);
			   		}
				   	else
			   		{
				   		showOrderReport(orderNo);
			   		}
				});
			}
			else
			{

				showMessage('red',resultsData.Description,'');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) 
		{ 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	});
	
}
function dblclkAction(id)
{
	
   	historyOrderNo = jQuery("#PaymentGatewayRecordGrid").jqGrid('getCell', id, 'OrderNo');
   	showOrderStatus(historyOrderNo);
   	
}
