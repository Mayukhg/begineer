
var distributorTitle;
var captchaText="";
var validatedDistributorId;
var validatedUplineDistributorId;

var loggedInUserPrivilegesForDistRegistration;

var primaryObjectForVars = function(){
	
	validatedDistributorId = null;
	validatedUplineDistributorId = null;
	
};

jQuery(document).ready(function(){
	
	jQuery("#addressField").hide();
	
	jQuery("#address").hide();
	
	jQuery("#RTGSCodeValidated").val(0);
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 640,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	  });
	
	
	refreshCaptcha(); //function to generate captcha;
	
	function generateCapchaText(){
		var text = "";
		var possibility = "ABCDEFGHIJKLMNOPQRSTUWXYZabcdefghijklmnopqrstuwxyz0123456789";

		for(var i = 0 ; i < 4; i++){
			text+=possibility.charAt(Math.floor(Math.random()*possibility.length));
		}
		return text;
	}
	
	function generateDistributorPassword()
	{
		var password = "";
		var possibility = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

		for(var i = 0 ; i < 8; i++){
			password+=possibility.charAt(Math.floor(Math.random()*possibility.length));
		}
		return password;
	}
	
	
	function refreshCaptcha()
	{
		captchaText = generateCapchaText();
		var canvas = document.getElementById('distributorVarificationCapcha');


		var ctx = canvas.getContext("2d");
		ctx.clearRect(0,0, canvas.width, canvas.height);
		var gradient = ctx.createLinearGradient(0,0,canvas.width,0);
		gradient.addColorStop("0","magenta");
		gradient.addColorStop("0.5","blue");
		gradient.addColorStop("1.0","red");
		ctx.font = "200px Verdana Arial";
		ctx.fillStyle = gradient;
		ctx.fill();
		ctx.fillText(captchaText, 0, 130, canvas.width , canvas.height);
	}
	
	
	var DISTRIBUTOR_NO_VALIDATION = 0;
	var UPLINE_NO_VALIDATION = 0; 
	var DISTRIBUTOR_AGE_VALIDATION = 0;
	var CO_DISTRIBUTOR_AGE_VALIDATION = 0;
	var DISTRIBUTOR_MODILE_VALIDATION = 0;
	var DISTRIBUTOR_EMAIL_VALIDATION = 0;
	var DISTRIBUTOR_PIN_NO_VALIDATION = 0;
	var DISTRIBUTOR_PAN_NO_VALIDATION = 0;
	var DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
	var DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 0;
	
	
	var distributorCountries;
	var distributorStates;
	var distributorZones;
	var distributorSubZones;
	var distributorArea;
	
	var distributorBankDetails;
	var bankAccountLength;
	
	var intRegex = /^\d+$/;
	
	
	jQuery("#header").hide();
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	jQuery(".title").hide();
		
	jQuery("#footer-wrapper").hide();
	
		var date = new Date();
		var month = date.getUTCMonth() + 1;
		var year = date.getUTCFullYear();
		var json_object_city;
		var current_date = month+"-"+year;
		jQuery("#pv_month").val(current_date);
		var countries = new Array();
		
		var moduleCode = jQuery("#moduleCode").val();
		
		
		function actionButtonsStauts()
		{
			jQuery(".distributorRegistrationFormOptions").each(function(){
				
				var buttonid = this.id;
				var extractedButtonId = buttonid.replace("btn","");
				
				if(loggedInUserPrivilegesForDistRegistration[extractedButtonId] == 1)
				{
					
					//jQuery("#"+buttonid).attr('disabled',false);
					
				}
				else
				{
					jQuery("#"+buttonid).attr('disabled',true);
				}
				
				
				jQuery("#Reset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
				jQuery("#Close").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
				
				
			});
		}
		
		function getDistributorBankDetails()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:"bankDetails",
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var resultData = jQuery.parseJSON(data);
					
					if(resultData.Status == 1)
					{
						distributorBankDetails = resultData.Result;
						
						jQuery.each(distributorBankDetails,function(key,value){
							
							jQuery("#distributorBank").append("<option  value=\""+distributorBankDetails[key]['BankId']+"\""+">"+distributorBankDetails[key]['BankName']+"</option>"); //Bank option to shown.
							
						});
					}
					else
					{
						showMessage("red",resultData.Description,"");
					}
					
					
					//return data;
				}, //end success
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
		
		
		
		function checkingStatusForButton(buttonid)
		{
			
			var extractedButtonId = buttonid.replace("btn","");
			
			if(loggedInUserPrivilegesForDistRegistration[extractedButtonId] == 1)
			{
				jQuery("#"+buttonid).attr('disabled',false);
			}
			else
			{
				//jQuery(".distributorRegistrationFormOptions").attr('disabled',true); //disable on event on screen.
			}
			
			
			//jQuery("#Reset").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			//jQuery("#Close").attr('disabled',false);
			
		}
		
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:
			{
				id:"moduleFunctionHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesDistRegiData = jQuery.parseJSON(data);
				
				if(loggedInUserPrivilegesDistRegiData.Status == 1)
				{
					
					loggedInUserPrivilegesForDistRegistration  = loggedInUserPrivilegesDistRegiData.Result;
					
					if(loggedInUserPrivilegesForDistRegistration.length == 0)
					{
						//jQuery(".SCCreateActionButtons").attr('disabled',true); //all button coming as disabled already.
					}
					else
					{
						actionButtonsStauts();
						
						checkingStatusForButton("btnPOS03Save"); //function chekcing  status for one particular button.
						
						loadCountries(); //
						
						loadValidStates(); //
						
						loadValidZones(); //
						
						loadValidSubZones(); //
						
						loadValidArea(); //
						
						fetchTitles(); 
						
						getDistributorBankDetails();
					}
					
					
					
				}
				else
				{
					showMessage("red", loggedInUserPrivilegesDistRegiData.Description, "");
				}
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
		
		function fetchTitles()
		{
			//showMessage("loading", "Loading titles...", "");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'CommonActionCallback',
				data:
				{
					id:"titles",
					
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					
					var resultData = jQuery.parseJSON(data);
					
					if(resultData.Status == 1)
					{
						jQuery("distributorTitle").empty();
						
						jQuery("#co_distributor_title").empty();
						
						var results = resultData.Result;
						
						distributorTitle = results;
						
						jQuery.each(results,function(key,value){
							
							jQuery("#distributorTitle").append("<option value=\""+results[key]['DistributorTitleId']+"\""+">"+results[key]['DistributorTitle']+"</option>"); 
							
							jQuery("#co_distributor_title").append("<option value=\""+results[key]['DistributorTitleId']+"\""+">"+results[key]['DistributorTitle']+"</option>"); 
							
						});
					}
					else
					{
						showMessage("red",resultData.Description,"");
					}
					
					
					//return data;
				}, //end success
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
		
		function loadValidStates()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:5,
					
				},
				success:function(data)
				{
					
					var results = jQuery.parseJSON(data);
					
					if(results.Status == 1)
					{
						var result = results.Result;
						
						distributorStates = result;
					}
					else
					{
						showMessage("red", results.Description, "");
					}
				}, //end success
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
		
		function loadValidZones()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:"getZones",

				},
				success:function(data)
				{
					var zonesData = jQuery.parseJSON(data);

					if(zonesData.Status == 1)
					{
						distributorZones = zonesData.Result;

						/*jQuery.each(oZone,function(key,value){

							var zone = oZone[key]['OrgHierarchyCode'];
							var zoneId = oZone[key]['OrgHierarchyId'];

							jQuery("#zone").append("<option name='zone' id=\""+zoneId+"\""+" value=\""+zoneId+"\""+">"+zone+"</option>"); //append zone.
							subZones(zoneId);
						});*/
					}
					else
					{
						showMessage("red", zonesData.Desription, "");
					}

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});
		}

		
		
		function loadValidSubZones()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:"getSubZones",
					
				},
				success:function(data)
				{
					
					var subZonesData = jQuery.parseJSON(data);
					
					if(subZonesData.Status == 1)
					{
						distributorSubZones = subZonesData.Result;
						
					}
					else
					{
						showMessage("red", subZonesData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		function loadValidArea()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:"getArea",
					
				},
				success:function(data)
				{
					
					var areaData = jQuery.parseJSON(data);
					
					if(areaData.Status == 1)
					{
						distributorArea = areaData.Result;
						
					}
					else
					{
						showMessage("red", areaData.Description, "");
					}
				
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		
		function loadCountries()
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:1,
					
				},
				success:function(data)
				{
					
					var countriesData = jQuery.parseJSON(data);
					
					if(countriesData.Status == 1)
					{
						jQuery("#country").append("<option name='country' value='0'>Select</option>");
						
						var json_object_country = countriesData.Result;
						
						distributorCountries = json_object_country;
						
						jQuery.each(json_object_country,function(key,value)
						{	
							jQuery("#country").append("<option value=\""+json_object_country[key]['CountryId']+"\""+">"+json_object_country[key]['CountryName']+"</option>");
							
						});
					}
					else
					{
						showMessage("red", countriesData.Description, "");
					}
					
					/*jQuery("#country").val(1);
					returnStates(1);*/
					
				}, //end success
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
		
		
		//returnStates(1);
		function returnStates(country_id)
		{
			jQuery("#state").append("<option name='state' value='0'>Select</option>");
			
			jQuery.each(distributorStates,function(key,value){
				
				if(country_id == distributorStates[key]['CountryId'])
				{
					jQuery("#state").append("<option value=\""+distributorStates[key]['StateId']+"\""+">"+distributorStates[key]['StateName']+"</option>");
				}
				
			});
			
			
			/*jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:5,
					country_id:country_id,
	
				},
				success:function(data)
				{
					if (data.indexOf("null") >= 0)
					{
						showMessage("","Not any state registered yet","");
						//alert("Not any state registered yet");
					}
					else
					{
						
						jQuery("#state").append("<option name='state' value='0'>Select</option>");
					
						var statesData = jQuery.parseJSON(data);
						
						if(statesData.Status == 1)
						{
							dynamic_button_json_object = statesData.Result;
							
							var json_object_state = dynamic_button_json_object;
							//alert(json_object_state);
							jQuery.each(json_object_state,function(key,value)
							{	
								
								//alert(value);
								//alert(json_object_country[key]['CountryId']);
								var state_id = json_object_state[key]['StateId'];
								var state_name = json_object_state[key]['StateName'];
								jQuery("#state").append("<option value=\""+state_id+"\""+">"+state_name+"</option>");
							});
						}
						else
						{
							showMessage("red", statesData.Description, "");
						}
					
					}
				
					
				}
				
			});*/
		}
		
	      jQuery("#disable_link").click(function(){
				
	    	  jQuery(".menu-343.menu-path-stockadjustment.first.odd").hide();
				/*jQuery.ajax({
					url:Drupal.settings.basePath + 'POSDistributorRegistration',
					data:
					{
						id:12,
		
					},
					success:function(data)
					{
						alert(data);
						
					}
				
				});*/
			});
	      jQuery("#secondary-menu").hide();
	      jQuery("#form_submit_button").click(function(){
	    	  
	    	  jQuery("#secondary-menu").show();
	    	  jQuery("#sidebar-first").hide();
	    	  
	      });
		var states = new Array();
		var check_on_state_change=0;
		jQuery("#country").change(function(){
			check_on_state_change = 1;
			jQuery("#state option").remove();
			jQuery("#city option").remove();
			//jQuery("#state option").remove();
			var country_id = jQuery(this).val();
			returnStates(country_id);//function to get states on country id change
		});
		jQuery("#state").change(function(){
			jQuery("#city option").remove();
			
			
			var state_id = jQuery(this).val();
			//alert(state_id);
			
			stateIdToCities(state_id);
			
			zones(state_id);
			
		});
		
		
		/**
		 * function used to load cities from zones.
		 */
		function stateIdToCities(state_id)
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:8,
					stateid:state_id,
	
				},
				success:function(data)
				{
					var citiesData = jQuery.parseJSON(data);
					
					if(citiesData.Status == 1)
					{
						jQuery("#city").append("<option name='city' value='0'>Select</option>");
						
						var json_object_city = citiesData.Result;
						
						jQuery.each(json_object_city,function(key,value){	

							jQuery("#city").append("<option value=\""+json_object_city[key]['CityId']+"\""+">"+json_object_city[key]['CityName']+"</option>");
						
						});
						
					}
					else
					{
						showMessage("red", citiesData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		
		/*--------------------------------------------------------------------------------------------------------------------------------*/
		/**
		 * Get all zones available for one state id.
		 */
		function zones(state_id)
		{
			
			jQuery("#zone").empty(); // Empty zone list everytime function called and then append new one later.
			
						jQuery.each(distributorZones,function(key,value){
							
							if(distributorZones[key]['StateId'] == state_id)
							{
								jQuery("#zone").append("<option name='zone' id=\""+distributorZones[key]['OrgHierarchyId']+"\""+" value=\""+distributorZones[key]['OrgHierarchyId']+"\""+">"+distributorZones[key]['OrgHierarchyCode']+"</option>"); //append zone.
								subZones(distributorZones[key]['OrgHierarchyId']);
							}
							
							
						});
					
			
		}
		
		
		
		/*--------------------------------------------------------------------------------------------------------------------------------*/
		/**
		 * Get all sub - zones available for one zone.
		 */
		function subZones(zoneId)
		{
			
			jQuery("#sub_zone").empty(); // Empty zone list everytime function called and then append new one later.
			jQuery("#sub_zone").append("<option name='subzone' value='0'>Select</option>");
			
						jQuery.each(distributorSubZones,function(key,value){
							
							if(distributorSubZones[key]['ParentHierarchyId'] == zoneId)
							{
								jQuery("#sub_zone").append("<option name='SubZone' id=\""+distributorSubZones[key]['OrgHierarchyId']+"\""+" value=\""+distributorSubZones[key]['OrgHierarchyId']+"\""+">"+distributorSubZones[key]['OrgHierarchyCode']+"</option>"); //append sub zone.
							}
						});
				
			
		}
		
		
		/*------------------------change sub zone--------------------------*/
		
		jQuery("#sub_zone").change(function(){
			
			var subZoneId = jQuery(this).val();
			
			area(subZoneId);
			
		});
		
		
		/*------------------------Bank option change------------------------*/
		/*jQuery("#distributorBank").change(function(){
			
			getBankAccountFixedLength(this.value);
			
		});*/
		
		/**
		 * function used to give fixed length for account number for one bank.
		 */
		function getBankAccountFixedLength(bankId)
		{
			if(bankId == -1)
			{
				//No any bank selected.
			}
			else
			{
				jQuery.each(distributorBankDetails,function(key,value){
					
					if(distributorBankDetails[key]['BankId'] == bankId)
					{
						jQuery("#bankAccountLength").val(distributorBankDetails[key]['BankAccountFixedLength']);
					}
					
				});
			}
			
			
		}
		
		/*--------------------------------------------------------------------------------------------------------------------------------*/
		/**
		 * Get all area available for sub zone.
		 */
		function area(subZoneId)
		{
			
			jQuery("#area").empty(); // Empty zone list everytime function called and then append new one later.
			jQuery("#area").append("<option name='area' value='0'>Select</option>");
			
			jQuery.each(distributorArea,function(key,value){
				
				if(distributorArea[key]['ParentHierarchyId'] == subZoneId)
				{
					jQuery("#area").append("<option name='area' id=\""+distributorArea[key]['OrgHierarchyId']+"\""+" value=\""+distributorArea[key]['OrgHierarchyId']+"\""+">"+distributorArea[key]['OrgHierarchyCode']+"</option>"); //append sub zone.
				}

			});
			
		}
		
		jQuery("#distributor_no").attr('maxlength','8');
		
		jQuery("#distributor_no").keydown(function(e){

			if(e.which == 13  || e.which == 9)
			{
				
				

				if(  !intRegex.test(jQuery("#distributor_no").val())){
					
					jQuery("#distributor_no").css("background", "#FF9999");
					
					showMessage("","Enter valid distributor id. Press Tab","");
					jQuery("#distributor_no").focus();
				}
				
				else
				{
					var distributorId = jQuery("#distributor_no").val(); //S - search distributor id field.
					
					if(distributorId.length != 8)
					{
						jQuery("#distributor_no").css("background", "#FF9999");
						
						showMessage("", "Enter valid distributor id. Press Tab..", "");
						jQuery("#distributor_no").focus();
					}
					else
					{
						checkValidDistributor(distributorId); //function to validate distributor id.
					}
				}
				
				
				
			}
		});
			
		
		function checkValidDistributor(distributorId)
		{
			if(distributorId == '')
			{
				jQuery("#distributor_no").css("background", "#FF9999");
				
				showMessage("", "Enter distributor id", "");
				jQuery("#distributor_no").focus();
				
				emptyFieldsData();
			}
			else
			{
				showMessage("loading", "Validating distributor details...", "");
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'POSDistributorRegistration',
					data:
					{
						id:"checkDistributor",
						distributorId:distributorId,

					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						
						var returnedDistibutorData = jQuery.parseJSON(data);
						
						if(returnedDistibutorData.Status == 1)
						{
							var results = returnedDistibutorData.Result;
							
							if(results.length == 0)  //means not any distributor registered yetn on this id.
							{
								jQuery("#distributor_no").css("background", "white");
								
								primaryObjectForVars.validatedDistributorId = distributorId;
								//validatedDistributorId.validatedUplineDistributorId = 1;
								
								DISTRIBUTOR_NO_VALIDATION = 1;
							//	jQuery("#distributor_no").attr("disabled", true);
								jQuery("#upline_no").focus();
								
								//emptyFieldsData();
							}
							else
							{
								//distributor_id = distributorId;
								
								jQuery("#distributor_no").css("background", "#FF9999");
								
								showMessage("", "Distributor ID Already Assigned. Choose Another.", "");
								jQuery("#distributor_no").focus();
								jQuery("#distributor_no").select();
								
								//distributorInfoSearch(distributorId); //call function to populate distributor information after all validation.
							}
						}
						else
						{
							showMessage("red", returnedDistibutorData.Description, "");
						}
						
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
				});
			}
		}
		
		jQuery("#upline_no").change(function(){
			jQuery("#firstname").val("");
			jQuery("#lastname").val("");
			jQuery("#address").val("");
		});
		//jQuery(document).keydown(function (e)
		
		jQuery("#upline_no").attr('maxlength','8');
		
		jQuery("#upline_no").keydown(function(event) {


			if(event.keyCode == 13 || event.keyCode == 9)
			{
				var upline_no = jQuery("#upline_no").val();
				//jQuery("#c1").attr("checked", true);
				if(upline_no == "")
				{
					//alert("please fill upline no.");
					showMessage("", "Upline Distributor Not Filled", "");
					jQuery("#upline_no").focus();

				}
				else if(upline_no.length != 8)
				{
					showMessage("", "Invalid upline number", "");
					jQuery("#upline_no").focus();
				}
				else if(!intRegex.test(upline_no)){
					
					jQuery("#upline_no").css("background", "#FF9999");
					
					showMessage("","Enter Valid Upline Number.","");
					jQuery("#upline_no").focus();
				}
				else
				{
					showMessage("loading", "Validating upline...", "");

					var upline_informaton = new Array();
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'POSDistributorRegistration',
						data:
						{
							id:3,
							upline_no:upline_no,

						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							
							var uplineData = jQuery.parseJSON(data);

							if(uplineData.Status == 1)
							{
								var upline_no_info_obj = uplineData.Result;
								var upline_no_info_obj_len = upline_no_info_obj.length;
								if (upline_no_info_obj_len == 0){
									jQuery("#upline_no").css({"background-color": "#ff9999"});

									showMessage("", "Upline distributor id is not valid.", "");
									jQuery("#upline_no").focus();

								}
								else
								{
									jQuery("#upline_no").css({"background-color": "white"});
									//var a = new Array();
									//dynamic_button_json_object = jQuery.parseJSON(data);
									jQuery.each(upline_no_info_obj,function(key,value){	
										//alert(value);

										var address1 = upline_no_info_obj[key]['DistributorAddress1'];
										var address2 = upline_no_info_obj[key]['DistributorAddress2'];
										var address3 = upline_no_info_obj[key]['DistributorAddress3'];
										var address = address1 + "," + address2 + "," + address3 + " "+upline_no_info_obj[key]['CityName']+" "+upline_no_info_obj[key]['StateName'];
										jQuery("#firstname").val(upline_no_info_obj[key]['DistributorFirstName']);
										jQuery("#lastname").val(upline_no_info_obj[key]['DistributorLastName']);
										jQuery("#address").val(address);


									});
									
									jQuery("#distributorTitle").focus();
									
									primaryObjectForVars.validatedUplineDistributorId = upline_no;

									UPLINE_NO_VALIDATION = 1;
							//		jQuery("#upline_no").attr("disabled", true);
									
 
								}
							}
							else
							{
								showMessage("red", uplineData.Description, "");
							}

						},// end success
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}


					});


					//}

				}

			}	
		});	
				
				/*---------------Mobile number focus out---------------*/
				jQuery("#mobile").focusout(function(){
					
					var enteredMobileNumber = jQuery("#mobile").val();
					mobileNumberValidation(enteredMobileNumber);
					
					
				});
				
				/*---------------Mobile number press enter---------------*/
				jQuery("#mobile").keydown(function(event){

					if(event.keyCode == 13)
					{
						
						var enteredMobileNumber = jQuery("#mobile").val();
						mobileNumberValidation(enteredMobileNumber); //validate mobile number
						
					}
				});
				
				
				/*-------------------------------------------------------------------------------------------------------------------------*/
				/**
				 * function to validate mobile number
				 */
				function mobileNumberValidation(enteredMobileNumber)
				{
					if(enteredMobileNumber == '')
					{
						showMessage("", "Mobile number not entered", "");
						jQuery("#mobile").focus();
					}
					else
					{
						var mobileValidationFlag = validateMobileNumber(enteredMobileNumber);
						
						if(mobileValidationFlag == 1)
						{
							//valid mobile number
							 jQuery("#mobile").css("background", "white");
							 
							 DISTRIBUTOR_MODILE_VALIDATION = 1;
						}
						else
						{
							 jQuery("#mobile").css("background", "#FF9999");
							 showMessage("","Not a valid mobile number","");
							 jQuery("#mobile").focus();
							 //alert("Not a valid mobile number");
						}
					}
					
					
				}
				
				
				
				/*---------------Pin number focus out---------------*/
				jQuery("#pin").focusout(function(){
					
					var enteredPinCode = jQuery("#pin").val();
					pinCodeValidation(enteredPinCode);
					
					
				});
				
				/*---------------Pan number press enter---------------*/
				jQuery("#pin").keydown(function(event){

					if(event.keyCode == 13)
					{
						
						var enteredPinCode = jQuery("#pin").val();
						pinCodeValidation(enteredPinCode); //validate mobile number
						
					}
				});
				
				
				/*-------------------------------------------------------------------------------------------------------------------------*/
				/**
				 * function to validate mobile number
				 */
				function pinCodeValidation(enteredPinCode)
				{
					
					if(enteredPinCode == '')
					{
						showMessage("", "Pin number not entered ", "");
						jQuery("#pin").focus();
					}
					else
					{
						var pinValidationFlag = validatePinCode(enteredPinCode);
						
						if(pinValidationFlag == 1)
						{
							//valid mobile number
							 jQuery("#pin").css("background", "white");
							 
							 DISTRIBUTOR_PIN_NO_VALIDATION = 1;
						}
						else
						{
							 jQuery("#pin").css("background", "#FF9999");
							 showMessage("","Not a valid pin code","");
								jQuery("#pin").focus();

							 //alert("Not a valid pin code");
						}
					}
					
					
				}
				
				
				
				
				
				/*-------------------Email id field focus out----------------*/
				jQuery("#email").focusout(function(){
					
					var enteredEmailAddress = jQuery("#email").val();
					emailAddressValidation(enteredEmailAddress);
					
					
				});
				
				/*-------------------Email id press enter----------------*/
				jQuery("#email").keydown(function(event){

					if(event.keyCode == 13)
					{
						
						var enteredEmailAddress = jQuery("#email").val();
						emailAddressValidation(enteredEmailAddress);
						
					}
				});
				
				/*-------------------------------------------------------------------------------------------------------------------------*/
				/**
				 * function to validate Email Address
				 */
				function emailAddressValidation(enteredEmailAddress)
				{
					
					if(enteredEmailAddress == '')
					{
						showMessage("", "Email ID not entered", "");
						//jQuery("#email").focus(); - IF email id not entered then it just a indication to show tht id not entered not to restrict user to definently enter that.

					}
					else
					{
						var emailValidationFlag = validateEmailAddress(enteredEmailAddress);
						
						if(emailValidationFlag == 1)
						{
							//valid mobile number
							jQuery("#email").css("background", "white");
							
							DISTRIBUTOR_EMAIL_VALIDATION = 1; 
							
						}
						else
						{
							 jQuery("#email").css("background", "#FF9999");
							 showMessage("","Not a valid email address","");
							jQuery("#email").focus();

							 //alert("not a valid email address");
						}
					}
					
					
				}
				
				
				jQuery(".upload_files").click(function(e){
					
					var documentUploadOrNot = 1;
					
					call_ajax_status1();
					
					if(DISTRIBUTOR_NO_VALIDATION != 1)
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#distributor_no").css('background','#FF9999');
						
						
					}
					else
					{
						jQuery("#distributor_no").css('background','white');
					}
					
					if(UPLINE_NO_VALIDATION != 1)
					{
						
						
						documentUploadOrNot = 0;
						
						jQuery("#upline_no").css('background','#FF9999');
						
					}
					else
					{
						jQuery("#upline_no").css('background','white');
					}
					if(jQuery("#distributorTitle").val() == -1)
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#distributorTitle").css('background','#FF9999');
						
						
					}
					else
					{
						jQuery("#distributorTitle").css('background','white');
					}
					if(jQuery("#address1").val() == '')
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#address1").css('background','#FF9999');
						
						
					}
					else
					{
						jQuery("#address1").css('background','white');
					}
					if(DISTRIBUTOR_MODILE_VALIDATION != 1)
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#mobile").css('background','#FF9999');
						
					}
					else
					{
						jQuery("#mobile").css('background','white');
					}
					

					if(jQuery("#distributor_firstname").val() == '')
					{
						
						
						documentUploadOrNot = 0;
						
						jQuery("#distributor_firstname").css('background','#FF9999');
					}
					else
					{
						jQuery("#distributor_firstname").css('background','white');
					}
					
					if(jQuery("#country").val() == 0)
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#country").css('background','#FF9999');
						
					}
					else
					{
						jQuery("#country").css('background','white');
					}
					if(jQuery("#state").val() == 0 || jQuery("#state").val()== null)
					{
						
						
						documentUploadOrNot = 0;
						
						jQuery("#state").css('background','#FF9999');
					}
					else
					{
						jQuery("#state").css('background','white');
					}
					
					if(jQuery("#city").val() == 0 || jQuery("#city").val() == null)
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#city").css('background','#FF9999');
						
						
					}
					else
					{
						jQuery("#city").css('background','white');
					}
					
					if(jQuery("#sub_zone").val() == 0 || jQuery("#sub_zone").val() == null )
					{
						
						
						documentUploadOrNot = 0;
						jQuery("#sub_zone").css('background','#FF9999');
						
						
					}
					else
					{
						jQuery("#sub_zone").css('background','white');
					}
					if(jQuery("#area").val() == 0 || jQuery("#area").val() == null)
					{
						
						
						documentUploadOrNot = 0;
						
						jQuery("#area").css('background','#FF9999');
					}
					else
					{
						jQuery("#area").css('background','white');
					}
					
					if(documentUploadOrNot == 0)
					{
						showMessage("yellow", "Enter required information before uploading document(s)", "");
						
						e.preventDefault();//Prevent to open screen for opening file.
					}
					
					
						
					
						
						
					
				});
				
				
				jQuery(".checkbox_status").attr("disabled", true);
				var current_file_upload_id;
				jQuery(".upload_files").change(function () {
					
					
						var distributorId = jQuery("#distributor_no").val();
						
						var current_id = this.id;
						current_file_upload_id = current_id + "1";
						var file_index = new Array("0","1");
						
						jQuery("#"+current_file_upload_id).removeAttr("disabled");
						jQuery("#"+current_file_upload_id).attr("checked", true);
						jQuery.ajax({
							type:'POST',
							url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
							//data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
							data:
							{
								
								id:"username",
							},
							success: function(data)
							{

								//alert(data);
								jQuery(".ajaxLoading").hide();
								
								
								var distributorinfoEditData = jQuery.parseJSON(data);
								
								
									
									
									
									upload(current_id,file_index[0],current_file_upload_id,distributorId,distributorinfoEditData);
					
							
									
								
								
								
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) { 

								jQuery(".ajaxLoading").hide();
								var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
								showMessage("", defaultMessage, "");
							}
						});
						
						jQuery("#"+current_file_upload_id).change(function() {
							var input1 = jQuery(this); //input type file id.
							if(input1.is('checked')==false){
								//var control = jQuery("#"+current_file_upload_id);
								//control.replaceWith( control = control.clone( true ));
								//alert("dff");
								//jQuery("#"+current_id).show();
								jQuery("#"+current_id).attr("disabled",false);
								
								jQuery("#"+current_file_upload_id).attr("disabled", true);
							}
							
					    });
					
						
				});
				
				jQuery(".checkbox_status").change(function(){
					var currentCheckboxId = this.id;
					var idLength = currentCheckboxId.length;
					var currentFileInputId = currentCheckboxId.substring(0,idLength-1);
					if (jQuery("#"+currentCheckboxId).is(':checked')) {
					   
					}
					else
					{
						 jQuery("#"+currentFileInputId).show();
					}
				});
				jQuery("#address_checkbox").change(function () {
					jQuery("#address_check").removeAttr("disabled");
					jQuery("#address_check").attr("checked", true);
					jQuery("#address_check").change(function() {
						var input1 = jQuery(this);
						if(input1.is('checked')==false){
							var control = jQuery("#address_checkbox");
							control.replaceWith( control = control.clone( true ) );
							jQuery("#address_check").attr("disabled", true);
						}
						
				    });
					
				});
				jQuery("#cencelled_check_checkbox").change(function () {
					jQuery("#cancelled_check").removeAttr("disabled");
					jQuery("#cancelled_check").attr("checked", true);
					jQuery("#cancelled_check").change(function() {
						var input1 = jQuery(this);
						if(input1.is('checked')==false){
							var control = jQuery("#cencelled_check_checkbox");
							control.replaceWith( control = control.clone( true ) );
							jQuery("#cancelled_check").attr("disabled", true);
						}
						
				    });
					
				});
				jQuery(function() {
					jQuery( "#distributor_dob" ).datepicker({changeMonth: true,dateFormat: 'dd/mm/yy',changeYear: true,yearRange: '-100:+0',
						onSelect:function(){
							
							var selectedDOB = jQuery(this).val();
								
							distributorAgeValidation(selectedDOB);
							
							jQuery("#co_distributor_title").focus();
						}
					});
					
					jQuery("#distributor_dob").focusout(function(event){
						
						if(jQuery("#distributor_dob").val() == '')
						{
							
						}
						else
						{
							var selectedDOB = jQuery("#distributor_dob").val();
							
							distributorAgeValidation(selectedDOB);
						}
						
						
						
					});

					
					//jQuery('#distributor_dob').attr('readonly', true); //made distributor no read only.
					
					jQuery( "#co_distributor_dob" ).datepicker({changeMonth: true, dateFormat: 'dd/mm/yy',changeYear: true, yearRange: '-100:+0',
						onSelect:function(){
							
							var selectedDOB = jQuery(this).val();
							
							coDistributorAgeValidation(selectedDOB);
						}
					});
					
					jQuery("#co_distributor_dob").focusout(function(event){
						
						if(jQuery("#co_distributor_dob").val() == '')
						{
							
						}
						else
						{
							var selectedDOB = jQuery("#co_distributor_dob").val();
							
							coDistributorAgeValidation(selectedDOB);
						}
						
						
						
					});
					
					//jQuery('#co_distributor_dob').attr('readonly', true); //made distributor no read only.
					
					
				});
				
				function distributorAgeValidation(selectedDOB)
				{
					
					
					var ageVaidationFlag = validateAge(selectedDOB,18);
					
					if(ageVaidationFlag == 1)
					{
						//nothing //distributor age validated.
						DISTRIBUTOR_AGE_VALIDATION = 1;
						
						jQuery("#distributor_dob").css('background','white');
					}
					else
					{
						showMessage("", "Minimum Age for being a distributor is 18 years.", "");
						
						jQuery("#distributor_dob").css('background','#FF9999');
						jQuery("#distributor_dob").focus();
					}
					
				}
				
				function coDistributorAgeValidation(selectedDOB)
				{
					var ageVaidationFlag = validateAge(selectedDOB,18);
					
					if(ageVaidationFlag == 1)
					{
						//nothing //distributor age validated.
						CO_DISTRIBUTOR_AGE_VALIDATION = 1;
						
						jQuery("#co_distributor_dob").css('background','white');
					}
					else
					{
						showMessage("", "Minimum age for being a distributor is 18 years.", "");
						
						//jQuery("#co_distributor_dob").val('');
						
						jQuery("#co_distributor_dob").css('background','#FF9999');
						jQuery("#co_distributor_dob").focus();
					}
					
				}
				
				
				function call_ajax_status1()
				{
	
var call_ajax_status = 0;
					
if(jQuery("#distributorTitle").val() == -1){
	
	showMessage("", "Enter distributor title", "");
	call_ajax_status = 1;
	jQuery("#distributorTitle").css("background", "#FF9999");
}
else{
	jQuery("#distributorTitle").css("background", "white");
}	


if(primaryObjectForVars.validatedDistributorId != jQuery("#distributor_no").val().trim())
{
	
	DISTRIBUTOR_NO_VALIDATION = 0;
}

if(primaryObjectForVars.validatedUplineDistributorId != jQuery("#upline_no").val().trim())
{
	UPLINE_NO_VALIDATION = 0;
}



validateBankAccountLength(); //validating again length of bank account//May be user after validating account number just enter wrong digit.


distributorAgeValidation(jQuery("#distributor_dob").val());

if(jQuery("#co_distributor_dob").val()!=-1 && jQuery("#co_distributor_firstname").val()!='')
{
	coDistributorAgeValidation(jQuery("#co_distributor_dob").val());
}
else
{
	CO_DISTRIBUTOR_AGE_VALIDATION = 1;
}





if(jQuery("#co_distributor_title").val() == -1 && (jQuery("#co_distributor_firstname").val() != '' || jQuery("#co_distributor_lastname").val() != '' || jQuery("#co_distributor_dob").val() != ''))
{
	//Nothing.
	jQuery("#co_distributor_title").css("background", "#FF9999");
	
	showMessage("", "Fill Co-Distributor information", "");
	jQuery("#co_distributor_title").focus();
	call_ajax_status += 1 ;
}
else
{
	jQuery("#co_distributor_title").css("background", "white");
	
	
}

if(jQuery("#email").val() == '')
{
	DISTRIBUTOR_EMAIL_VALIDATION = 1;

}



if(jQuery("#co_distributor_firstname").val() == '' && jQuery("#co_distributor_lastname").val() == '')
{
	CO_DISTRIBUTOR_AGE_VALIDATION = 1;
}

if(DISTRIBUTOR_AGE_VALIDATION == 0)
{
	showMessage("", "Distributor age not validated. Press Tab.", "");
	call_ajax_status = 1;
}

else if(DISTRIBUTOR_MODILE_VALIDATION == 0)
{
	showMessage("", "Distributor mobile number not validated", "");
	
	call_ajax_status = 1;
}
else if(DISTRIBUTOR_NO_VALIDATION == 0)
{

	showMessage("", "Distributor number not validated", "");
	
	call_ajax_status = 1;
}
else if(DISTRIBUTOR_PIN_NO_VALIDATION == 0)
{
	showMessage("", "Distributor Pin number not validated", "");
	
	call_ajax_status = 1;
}
else if(UPLINE_NO_VALIDATION == 0)
{
	showMessage("", "Upline number not validated", "");
	
	call_ajax_status = 1;
}
else if(CO_DISTRIBUTOR_AGE_VALIDATION == 0)
{
	showMessage("", "Co Distributor age not validated ", "");
	
	call_ajax_status = 1;
}

else if(DISTRIBUTOR_PAN_NO_VALIDATION == 0)
{
	showMessage("", "Pan number not validated", "");
	call_ajax_status = 1;
}
else if(DISTRIBUTOR_EMAIL_VALIDATION == 0)
{
	showMessage("", "Email ID not validated", "");
	call_ajax_status = 1;
}

else if(jQuery("#IFSCCode").val() == '' && (jQuery("#distributorBank").val() != -1 || jQuery("#AccountNumber").val() !=''))
{

	var confirmProceed = confirm("Your Bank and Account number information will be lost without RTGS/IFSC code. Do you want to proceed ? ");

	if(confirmProceed == true)
	{
		jQuery("#distributorBank").val(-1);
		jQuery("#AccountNumber").val('');

		DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION =1;
		DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
	}
	else
	{
		call_ajax_status = 1;
	}



}
else if(jQuery("#IFSCCode").val() != '' && jQuery("#AccountNumber").val() == '')
{
	DISTRIBUTOR_ACCOUNT_NO_VALIDATION =0;
	
	showMessage("", "Enter Distributor Account Number", "");
	
	call_ajax_status = 1;
}
else if(DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION == 0)
{
	showMessage("", "RTGS Code not validated", "");
	call_ajax_status = 1;
}
else if(DISTRIBUTOR_ACCOUNT_NO_VALIDATION == 0)
{
	showMessage("", "Account number length not according to Selected Bank Branch", "");
	call_ajax_status = 1;
}

if (call_ajax_status1==0)
{
	return 1;
	}
else
	{
	return 0;
	}

				}
				
				
				jQuery("#btnPOS03Save").click(function(){
					var call_ajax_status = 0;
					
					jQuery(".required_fields").each(function(){
						var field_value = this.value;
						var required_field_id = this.id;
						if(field_value == '')
						{
							//alert("field empty ");
							var required_field_id = this.id;
							//alert(required_field_id);
							jQuery("#"+required_field_id).css("background", "#FF9999");
							call_ajax_status += 1 ;
							
						}
						else //To be checked.
						{
							jQuery("#"+required_field_id).css("background", "white");
						}
						
						
						
						var validateLocationInformationflag = validateLocationInformation();
						if(validateLocationInformationflag != 1)
						{
							call_ajax_status = call_ajax_status + 1;
							
						}
						
						
					});
					
					if(jQuery("#panNumber").val() == '')
					{
						DISTRIBUTOR_PAN_NO_VALIDATION = 1;
					}
					
					if(jQuery("#IFSCCode").val() == '')
					{
						DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 1;
						
						DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
					}
					
					
					jQuery(".most_required_fields").each(function(){
						var field_value = this.value;
						var most_required_field_id = this.id;
						if(field_value == '')
						{
							//alert("field empty ");
							var required_field_id = this.id;
							//alert(required_field_id);
							jQuery("#"+required_field_id).css("background", "#FF9999");
							call_ajax_status += 1 ;
							
						}
						
						if(jQuery(this).css("background-color") == "rgb(255, 153, 153)") 
						{
							
							jQuery("#"+most_required_field_id).css("background", "#FF9999");
							call_ajax_status += 1 ;
							
						}
						
						
					});

					if(jQuery("#distributorTitle").val() == -1){
					
						showMessage("", "Enter distributor title", "");
						call_ajax_status = 1;
						jQuery("#distributorTitle").css("background", "#FF9999");
					}
					else{
						jQuery("#distributorTitle").css("background", "white");
					}	
					
					
					if(primaryObjectForVars.validatedDistributorId != jQuery("#distributor_no").val().trim())
					{
						
						DISTRIBUTOR_NO_VALIDATION = 0;
					}
					
					if(primaryObjectForVars.validatedUplineDistributorId != jQuery("#upline_no").val().trim())
					{
						UPLINE_NO_VALIDATION = 0;
					}
					
					
					
					validateBankAccountLength(); //validating again length of bank account//May be user after validating account number just enter wrong digit.
					
					
					distributorAgeValidation(jQuery("#distributor_dob").val());
					
					if(jQuery("#co_distributor_dob").val()!=-1 && jQuery("#co_distributor_firstname").val()!='')
					{
						coDistributorAgeValidation(jQuery("#co_distributor_dob").val());
					}
					else
					{
						CO_DISTRIBUTOR_AGE_VALIDATION = 1;
					}
					
					
					
					
					
					if(jQuery("#co_distributor_title").val() == -1 && (jQuery("#co_distributor_firstname").val() != '' || jQuery("#co_distributor_lastname").val() != '' || jQuery("#co_distributor_dob").val() != ''))
					{
						//Nothing.
						jQuery("#co_distributor_title").css("background", "#FF9999");
						
						showMessage("", "Fill Co-Distributor information", "");
						jQuery("#co_distributor_title").focus();
						call_ajax_status += 1 ;
					}
					else
					{
						jQuery("#co_distributor_title").css("background", "white");
						
						
					}
					
					if(jQuery("#email").val() == '')
					{
						DISTRIBUTOR_EMAIL_VALIDATION = 1;
					
					}
					
					
					
					if(jQuery("#co_distributor_firstname").val() == '' && jQuery("#co_distributor_lastname").val() == '')
					{
						CO_DISTRIBUTOR_AGE_VALIDATION = 1;
					}
					
					if(DISTRIBUTOR_AGE_VALIDATION == 0)
					{
						showMessage("", "Distributor age not validated. Press Tab.", "");
						call_ajax_status = 1;
					}
					
					else if(DISTRIBUTOR_MODILE_VALIDATION == 0)
					{
						showMessage("", "Distributor mobile number not validated", "");
						
						call_ajax_status = 1;
					}
					else if(DISTRIBUTOR_NO_VALIDATION == 0)
					{
					
						showMessage("", "Distributor number not validated", "");
						
						call_ajax_status = 1;
					}
					else if(DISTRIBUTOR_PIN_NO_VALIDATION == 0)
					{
						showMessage("", "Distributor Pin number not validated", "");
						
						call_ajax_status = 1;
					}
					else if(UPLINE_NO_VALIDATION == 0)
					{
						showMessage("", "Upline number not validated", "");
						
						call_ajax_status = 1;
					}
					else if(CO_DISTRIBUTOR_AGE_VALIDATION == 0)
					{
						showMessage("", "Co Distributor age not validated ", "");
						
						call_ajax_status = 1;
					}
				
					else if(DISTRIBUTOR_PAN_NO_VALIDATION == 0)
					{
						showMessage("", "Pan number not validated", "");
						call_ajax_status = 1;
					}
					else if(DISTRIBUTOR_EMAIL_VALIDATION == 0)
					{
						showMessage("", "Email ID not validated", "");
						call_ajax_status = 1;
					}
					
					else if(jQuery("#IFSCCode").val() == '' && (jQuery("#distributorBank").val() != -1 || jQuery("#AccountNumber").val() !=''))
					{

						var confirmProceed = confirm("Your Bank and Account number information will be lost without RTGS/IFSC code. Do you want to proceed ? ");

						if(confirmProceed == true)
						{
							jQuery("#distributorBank").val(-1);
							jQuery("#AccountNumber").val('');

							DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION =1;
							DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
						}
						else
						{
							call_ajax_status = 1;
						}



					}
					else if(jQuery("#IFSCCode").val() != '' && jQuery("#AccountNumber").val() == '')
					{
						DISTRIBUTOR_ACCOUNT_NO_VALIDATION =0;
						
						showMessage("", "Enter Distributor Account Number", "");
						
						call_ajax_status = 1;
					}
					else if(DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION == 0)
					{
						showMessage("", "RTGS Code not validated", "");
						call_ajax_status = 1;
					}
					else if(DISTRIBUTOR_ACCOUNT_NO_VALIDATION == 0)
					{
						showMessage("", "Account number length not according to Selected Bank Branch", "");
						call_ajax_status = 1;
					}
					
					
					
					if(call_ajax_status == 0){
						
						
						if(captchaText.toLowerCase() != jQuery("#captchaText").val().toLowerCase())
						{
							refreshCaptcha();
							
							showMessage("", "Entered text not verified. Try again", "");
							jQuery("#captchaText").focus();
						}
						else
						{
							//alert(formDataAll);
							
							//Drupal.settings.basePath
							//var url = "/drupal-7.22/sites/all/modules/POS_Client/Business/distributor_registration_data.php" // the script where you handle the form input.
							
							jQuery("#firstname").attr('disabled',false);
							
							jQuery("#lastname").attr('disabled',false);
							
							var formDataAll = jQuery("#distributor_reg_form").serialize();
							
							jQuery("#firstname").attr('disabled',true);
							
							jQuery("#lastname").attr('disabled',true);
							
							showMessage("loading", "Saving distributor information...", "");
							
							registredDistributorPassword = generateDistributorPassword();
							
							var forSkinCareItemOrNot = jQuery("#sf9RegisterOrNot").is(':checked')? 1 : 0;
							
							jQuery.ajax({
								type:'POST',
							   url:Drupal.settings.basePath + 'POSDistributorRegistration',
							   //data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
							   data:
								{
									id:10,
									moduleCode:"POS03",
									functionName:"Save",
									form_data:formDataAll,
									forSkinCareItem:parseInt(forSkinCareItemOrNot),
									bankId:jQuery("#distributorBank").val(),
									distributorPassword:registredDistributorPassword,

								},
							   success: function(data)
							   {
								   jQuery(".ajaxLoading").hide();
								   
								  var distributorRegistrationSeqNoData = jQuery.parseJSON(data);
								  
								  if(distributorRegistrationSeqNoData.Status == 1)
								  {
									  
									 // alert(distributorRegistrationSeqNoData.Result);
									  
									  var result = distributorRegistrationSeqNoData.Result;
									  
									  
									  jQuery("#serial_no").val(result[0]['DistributorSeqNo']);
									  
									  var r=confirm("Distributor Registered Successfully. Serial No : "+result[0]['DistributorSeqNo'] +"\n Want To Clear Data.");
									   if (r==true)
									     {
										    clearAllFields();
										    
										    jQuery("#serial_no").val('');
										   	
									     }
									   else
									     {
										   
									     } // show response from the php script.
								  }
								  else
								  {
									  showMessage("red", distributorRegistrationSeqNoData.Description, "");
								  }
								   
								   
							   },//end success
							   error: function(XMLHttpRequest, textStatus, errorThrown) { 

									jQuery(".ajaxLoading").hide();
									var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
									showMessage("", defaultMessage, "");
								}
								
							});
						}
						
						
	
						//return false; // avoid to execute the actual submit of the form.
					}			
				});

				/*------------------------------------------------------------------------------------------------------------------*/
				/**
				 * funcition to clear all fields
				 */
				function clearAllFields()
				{
					jQuery(".not_required_fields").val('');
					jQuery(".not_required_fields").css("background","white");
				   	jQuery(".required_fields").val('');
				   	jQuery(".required_fields").css("background","white");
				   	jQuery(".most_required_fields").val('');
				   	jQuery(".most_required_fields").css("background","white");
				   	jQuery(".requiredList").empty();
				   	jQuery(".requiredList").css("background","white");
				   	jQuery(".notRequiredList").empty();
				   	jQuery(".notRequiredList").css("background","white");
				   	jQuery(".disable_fields").val('');
				   	//jQuery(".disable_fields").css("background","white");
				   	jQuery("#co_distributor_firstname").val('');
				   	jQuery("#co_distributor_lastname").val('');
				   	jQuery("#co_distributor_dob").val('');
				   	jQuery("#email").val('');
				   	jQuery(".checkbox_status").attr("checked", false);
				   	jQuery(".upload_files").attr("disabled",false);
				   	jQuery(".most_required_fields1").val('');
				   	jQuery("#distributorBank").val('');
				   	jQuery(".upload_files").val('');
				   	jQuery("#captchaText").val('');
				   	
				   	var date = new Date();
					var month = date.getUTCMonth() + 1;
					var year = date.getUTCFullYear();
					var current_date = month+"-"+year;
					jQuery("#pv_month").val(current_date);
				   	
				   	
				   	loadCountries(); //Load countries
				   	//returnStates(1); //load states for india whose id is 1 which is passed in function parameter.
				   	//fetchTitles();
				   	reloadingTitles();
				   	
				   	
				   	
				   	getDistributorBankDetails();
				}
				
				
				/**
				 * function used to reload titles.
				 */
				function reloadingTitles()
			   	{
					jQuery("#co_distributor_title").empty();
					
					jQuery("#distributor_title").empty();
					
			   		jQuery.each(distributorTitle,function(key,value){
						
						jQuery("#distributorTitle").append("<option value=\""+distributorTitle[key]['DistributorTitleId']+"\""+">"+distributorTitle[key]['DistributorTitle']+"</option>"); 
						
						jQuery("#co_distributor_title").append("<option value=\""+distributorTitle[key]['DistributorTitleId']+"\""+">"+distributorTitle[key]['DistributorTitle']+"</option>"); 
						
					});
			   	}
				
				
				/*------------------------------------------------------------------------------------------------------------------*/
				/**
				 * funcition to check data in location related fields
				 */
				function validateLocationInformation()
				{
					var selectionListFlag = 1;
					jQuery(".requiredList").each(function(){
						var selectionListId = this.id;
						if(jQuery("#"+selectionListId).val() == 0 || jQuery("#"+selectionListId).has('option').length == 0)
						{
							selectionListFlag = selectionListFlag + 1;
							jQuery("#"+selectionListId).css("background", "#FF9999");
							//return false;
						}
						else
						{
							jQuery("#"+selectionListId).css("background", "white");
						}
					});
					
					return selectionListFlag;
				}
				
				
				
				
				/*-------------------Enter Pan Number--------------------*/
				

				jQuery("#panNumber").attr('maxlength','10');
				
				jQuery("#panNumber").keydown(function(event){
					
					if(event.keyCode == 13 || event.keyCode == 9)
					{
					
						focusOutValidatePanNumber();
					}
					
				});
				
				jQuery("#panNumber").focusout(function(event){
					
					focusOutValidatePanNumber();
					
				});
				
				function focusOutValidatePanNumber()
				{

					var panNumber = jQuery("#panNumber").val();
					if(panNumber == '')
					{

					}
					else
					{
						if(panNumber.length != 10)
						{
							showMessage("", "Invalid pan number - Should be of 10 digit", "");
							jQuery("#panNumber").focus();
						}
						else
						{
							validatePanNumber(panNumber);
						}


					}

				}
				
				/*--------------------------------------------------------------------------------------------------------------- */
				function validatePanNumber(panNumber)
				{
					var distributorLastName = jQuery("#distributor_lastname").val();
					
					/*if((panNumber.charAt(4)).toUpperCase() != (distributorLastName.charAt(0)).toUpperCase())
					{
						showMessage("", "Invalid pan number - 5th character should be same as distributor last name", "");
						jQuery("#panNumber").focus();
						DISTRIBUTOR_PAN_NO_VALIDATION = 0;
					}
					else*/ 
					if(panNumberValidation(panNumber) == 1) //may be reverse.//have to change later.
					{
						//showMessage("", "Invalid pan number", "");
						
						DISTRIBUTOR_PAN_NO_VALIDATION = 1;
					}
					else
					{
						showMessage("", "Invalid pan number", "");
						jQuery("#panNumber").focus();
						DISTRIBUTOR_PAN_NO_VALIDATION = 0;
					}
					
				}
				
				
				jQuery("#AccountNumber").focusout(function(event){
					
						/*var bankIFSCCode = jQuery("#IFSCCode").val();
						var accountNumber = jQuery("#AccountNumber").val();
						
						
						if(bankIFSCCode == '' && accountNumber == '')
						{
							showMessage("","Enter IFSC and Account Number","");
							//alert("Enter IFSC and Account Number");
						}
						else if(bankIFSCCode != '' && accountNumber == '')
						{
							showMessage("","Enter Account Number","");
							//alert("Enter Account Number");
						}
						else if(bankIFSCCode == '' && accountNumber != '')
						{
							showMessage("","Enter Bank IFSC Number","");
							//alert("Enter Bank IFSC Number");
						}
						else if(bankIFSCCode != '' && accountNumber != '')
						{
							validateBankAndAccount(bankIFSCCode,accountNumber);
						}
						else
						{
							//
						}*/
						
					validateBankAccountLength();
					
					
				});
				
				
				function validateBankAccountLength()
				{
					var bankAccountLength = jQuery("#bankAccountLength").val();
					
					if(bankAccountLength == 0)
					{
						//bank account length not defined for this bank.
						DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
					}
					else
					{
						if((jQuery("#AccountNumber").val()).length != bankAccountLength)
						{
							showMessage("", "Account number length not according to selected bank", "");
							jQuery("#AccountNumber").focus();
							DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 0;
							
						}
						else
						{
							DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
						}
					}
				}
				
				/*-----------------Enter RTGS/IFSC code ----------------------------*/
				
				jQuery("#IFSCCode").keydown(function(event){

					if(event.keyCode == 13 || event.keyCode == 9)
					{
						
						var distributorRTGScode = jQuery("#IFSCCode").val();  
						
							showMessage("loading", "Validating RTGS code...", "");
							
							jQuery.ajax({
								type:'POST',
								url:Drupal.settings.basePath + 'POSDistributorRegistration',
								data:
								{
									id:"getBankAndAccountInfoFromRTGS",
									RTGSCode:distributorRTGScode,
								},
								success:function(data)
								{
									jQuery(".ajaxLoading").hide();
									
									var results = jQuery.parseJSON(data);
									if(results.Status == 1)
									{
										var result = results.Result;
										if(result.length > 0)
										{
											jQuery("#distributorBank").val(result[0]['BankId']);
											
											jQuery("#bankAccountLength").val(result[0]['FixedLength']);
											
											DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 1;
										}
										else
										{
											DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
											
											showMessage("", "Invalid RTGS Code", "");
										}
									}
									else
									{
										DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
										
										showMessage("red", results.Description, "");
									}
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) { 

									jQuery(".ajaxLoading").hide();
									var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
									showMessage("", defaultMessage, "");
								}

							});
						
						
					}
					
					if(event.which == 115) 
					{
						var currentRTGSCodeId = this.id;
						var currentRTGSCodeValue = this.value;

						jQuery.ajax({
							type:'POST',
							url:Drupal.settings.basePath + 'LookUpCallback',
							data:
							{
								id:"distributorRTGSCodeLookUp",
							},
							success:function(data)
							{

								//alert(data);
								jQuery("#divLookUp").html(data);

								distributorRTGSlookUpGrid(currentRTGSCodeId,currentRTGSCodeValue);
								jQuery("#divLookUp" ).dialog( "open" );

								var distributorBankId = distributorRTGSLookUpData(currentRTGSCodeValue);

								/*----------Dialog for item search is there ----------------*/


							},
							error: function(XMLHttpRequest, textStatus, errorThrown) { 

								jQuery(".ajaxLoading").hide();
								var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
								showMessage("", defaultMessage, "");
							}

						});
					}
				});	
				
				
				
				
				
				
				/*--------------------------------------------------------------------------------------------------------------- */
				function validateBankAndAccount(bankIFSCCode,accountNumber)
				{
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'POSDistributorRegistration',
						data:
						{
							id:"validateBankIFSCAndAccount",
							bankIFSCCode:bankIFSCCode,
							accountNumber:accountNumber,
			
						},
						success:function(data)
						{
							bankAccountData = jQuery.parseJSON(data);
							
							if(bankAccountData.Status == 1)
							{
								var oBankAndAccount = bankAccountData.Result;
								
								jQuery.each(oBankAndAccount,function(key,value){
									
									if(oBankAndAccount[key]['BankAccountStatus'] == 0)
									{
										jQuery("#IFSCCode").css("background","white");
										jQuery("#AccountNumber").css("background","white");
										
										//saveDistributorAccountInformation(bankIFSCCode,accountNumber)
										
									}
									else
									{
										showMessage("","Already a Distributor! Account Details With Same Branch Code Already Exist.","");
										//alert("Account Details With Same Branch Code Already Exist");
										jQuery("#IFSCCode").css("background","#ff9999");
										jQuery("#AccountNumber").css("background","#ff9999");
									}
									
									
								});
							}
							else
							{
								showMessage("red", bankAccountData.Descriptoin, "");
							}
						
						}, //end success.
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					
					});
				}
				
				/*--------------------------------------------------------------------------------------------------------------- */
				/**
				 * Save Distributor Account Information.
				 * 
				 */
				function saveDistributorAccountInformation(bankIFSCCode,accountNumber)
				{
					
					
					
				}
				
				
				/*--------------------Reset Button Click---------------------*/
				jQuery("#Reset").click(function(){
					
					clearAllFields();
					DISTRIBUTOR_NO_VALIDATION = 0;
					
					 UPLINE_NO_VALIDATION = 0; 
				 DISTRIBUTOR_AGE_VALIDATION = 0;
					 CO_DISTRIBUTOR_AGE_VALIDATION = 0;
				 DISTRIBUTOR_MODILE_VALIDATION = 0;
				 DISTRIBUTOR_EMAIL_VALIDATION = 0;
				 DISTRIBUTOR_PIN_NO_VALIDATION = 0;
			     DISTRIBUTOR_PAN_NO_VALIDATION = 0;
				 DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
				 DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 0;
				 
				 jQuery("#distributor_no").attr("disabled", false);
				 jQuery("#upline_no").attr("disabled", false);
					
					
					
					
				});
				
				/*--------------------Close Form--------------------*/
				jQuery("#Close").click(function(){
					
					
					window.close();
					
				});
				
				
		});
	
