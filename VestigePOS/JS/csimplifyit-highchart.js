
function getTopIncidneceReportingLocations(toplevel){
	
	if(toplevel != 0){
		return $.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:{
				id:"Top5IncedenceReportedLocation",
				TopLevel:toplevel
			},
			beforeSend:function(){
				$.mobile.loading( 'show', {
					text: "Loading...",
					textVisible: true,
					theme: 'b',
					html: ''
				});
			},
			complete:function(){
				$.mobile.loading('hide');
				$(".ajaxLoading").hide();
			}
		});
	}
	else{
		return $.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:{
				id:"IncidenceReportedReviewLoctionWise",
			}
		});
	}
	
	
}

$(function(){
	getTopIncidneceReportingLocations(5).done(function(data){
		if(checkDataCorrectOrNot(data) == 1){
			var xAxisCategories = getCategory(data);
			var chartReqData = getChartData(data);
			generateChart('bar','Top Incidence Reported Location',xAxisCategories,'Incidence Reported',chartReqData);
		}
	});
});


/*$(function(){
	getTopIncidneceReportingLocations(0).done(function(data){
		if(checkDataCorrectOrNot(data) == 1){
			var xAxisCategories = getCategory(data);
			var chartReqData = getChartData(data);
			generateChart('bar','Incidence Reporting Location',xAxisCategories,'Incidence Reported',chartReqData);
		}
	});
});*/

function getCategory(data){
	var oData = jQuery.parseJSON(data).Result;
	var categories = new Array();
	jQuery.each(oData,function(key,value){
		categories.push(oData[key]['LocationName']);
	});
	return categories;
}
function getChartData(data){
	var oData = jQuery.parseJSON(data).Result;
	var chartData = new Array();
	jQuery.each(oData,function(key,value){
		var singleObj = {};
		singleObj.y = parseInt(oData[key]['INCReported']);
		singleObj.locationId = parseInt(oData[key]['locationId']);
		chartData.push(singleObj);
	});
	return chartData;
}

function checkDataCorrectOrNot(data){
	var oData = jQuery.parseJSON(data);
	var returnFlag = 0;
	if(oData.Status == 1){
		//alert("right data");
		returnFlag = 1;
	}
	else{
		alert("wrong data");
		returnFlag = 0;
	}
	return returnFlag;
}

function generateChart(type,chartTitle,xAxisCategories,yAxisTitle,graphReqData){
    $('#container').highcharts({
        chart: {
            type: type
        },
        title: {	
            text: chartTitle
        },
        xAxis: {
            categories: xAxisCategories//['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: yAxisTitle
            }
        },
        plotOptions: {
        	series: {
        		borderWidth: 0,
        		dataLabels: {
        			enabled: true
        		},
        		point:{
        			events:{
        				click:function(){
        					if(this.locationId == undefined){
        						alert("Location id undefined");
        						return;
        					}
        					getIncidenceInformation(this.locationId).done(function(data){
        						if(checkDataCorrectOrNot(data) == 1){
        							var incidenceData = $.parseJSON(data).Result;
        							if(incidenceData.length == 0){
        								alert("incidence captured without description about incidence. Hence unable to show.");
        								return;
        							}
        							var items = '';
        							$.each(incidenceData,function(key,value){

        								items += "" +
        								"<li data-icon='false'><div id='divItemDesc'>"+
        								"<h2 class='itemHeading' id='itemHeading-itemCode'>"+incidenceData[key]['Description']+"</h2>"+
        								"<p class='itemDescription' id='itemDesc-itemCode'>"+"</p><a target='_blank' href='"+incidenceData[key]['INCImgPathRef']+"'>Incidence path"+"</a>"+
        								"<p class='itemDescription' id='itemDesc-itemCode'>Incidence Date time :"+incidenceData[key]['INCDateTime']+"</p>"+
        								"</div>" +
        								"</li>";
        							});

        							$.mobile.changePage("#pageIncidenceInfo",{'transition':'flip'});

        							$("#incidenceList").html(items);
        							$("#incidenceList").listview();
        							$("#incidenceList").listview('refresh');
        							$("#incidenceList").trigger("create");
        						}
        					});	
        				}
        			}
        		}
        	}
        },
        series: [{colorByPoint:true,data:graphReqData}]
    });
}


function getIncidenceInformation(locationId){
	
	return $.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:{
			id:"getIncidenceInfoForLocation",
			locationId:locationId
		},
		beforeSend:function(){
			$.mobile.loading( 'show', {
				text: "Loading...",
				textVisible: true,
				theme: 'b',
				html: ''
			});
		},
		complete:function(){
			$.mobile.loading('hide');
			$(".ajaxLoading").hide();
		}
	
	});
	
}


