var  jsonForLocations='';
var jsonForGRNSearch='';
var todayDate='';
var loggedInUserPrivileges;
jQuery(document).ready(function(){
jQuery("#main-menu").hide();
	
	jQuery(".breadcrumb").hide();
	
	jQuery("#triptych-wrapper").hide();
	
	//jQuery(".title").hide();
	
	jQuery("#footer-wrapper").hide();
	
	jQuery(function() {
		//alert("dfadf");
		jQuery( "#divGoodReceiptNoteTab" ).tabs(); // Create tabs using jquery ui.
	});
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 600,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	});
	
	getCurrentLocation();
	var moduleCode = jQuery("#moduleCode").val();
	 todayDate=jQuery("#todayDate").val();
	 
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'StockCountPOSTCallBack',
			data:
			{
				id:"StockCountModuleFuncHandling",
				moduleCode:moduleCode,
			},
			success:function(data)
			{
				//alert(data);	
				var loggedInUserPrivilegesForStockCountData = jQuery.parseJSON(data);

				if(loggedInUserPrivilegesForStockCountData.Status == 1)
				{

					loggedInUserPrivileges  = loggedInUserPrivilegesForStockCountData.Result;

					if(loggedInUserPrivileges.length == 0)
					{
						jQuery(".TrackJoining").attr('disabled',true);
					}
					else
					{	
					
					}


				}
				else
				{
					showMessage("red", loggedInUserPrivilegesForStockCountData.Description, "");
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});


		
jQuery("#Distid").keydown(function(e){
			
			if(e.which == 13 || e.which == 9) 
			{
				Distributorid=jQuery('#Distid').val();
				if(Distributorid=='')
					{
					showMessage('yellow','Enter DistributorId.','');
					}
				else
					{
					
				ValidateDistributor(Distributorid);
			}
			}
		});
		
jQuery("#DistrReset").click(function(){
    jQuery("#Distid").val('');
    jQuery("#Distid").attr('readonly',false);
    jQuery("#DistName").val('');
    jQuery("#DistName").attr('readonly',false);
	jQuery("#DistContact").val('');
	jQuery("#DistContact").attr('readonly',false);
	jQuery("#DistJoining").val('');
	jQuery("#DistJoining").attr('readonly',false);
	jQuery("#recieved").val('');
	jQuery("#recieved").attr('checked', false);
	jQuery("#Distid").css("background","#FFFFFF");
	
});

		
		jQuery('#remarks').focus(
			    function(){
			    	jQuery(this).val('');
			    });
		
		function ValidateDistributor(Distributorid)	
		{
			
			var flag = DistributorValidation2(Distributorid);
			if (flag==0)
				{
				showMessage('yellow','Enter Valid DistributorId.','');
				}
			else
				{
				jQuery("#Distid").css("background","#FFFFFF");
			//alert(currentItemSearchId);
			showMessage("loading", "Please wait...", "");

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'JoiningFormTrackingCallBack',
				data:
				{
					Distributorid: Distributorid,
					id:"ValidateDistributor",
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();
					var keypairData = jQuery.parseJSON(data);
					if(keypairData.Status==1){
					keypair=keypairData.Result;
					if(keypair.length!=0){
						
				jQuery.each(keypair,function(key,value)
					{
					if (keypair[key]['distributorstatus']=='')
						{
						showMessage("red","Please Enter Distributorid.");
						}
					else if(keypair[key]['distributorstatus']==1 || keypair[key]['distributorstatus']==2)
					{
						jQuery("#Distid").attr('readonly',true);
						}
						else
							{
							showMessage("red","Distributor is Blocked.");
							jQuery("#Distid").css("background","#FF9999");
							jQuery("#Distid").focus();
							}
						
					});
				 }
					else{
						showMessage("red","Distributor Does not Exist.");
						jQuery("#Distid").css("background","#FF9999");
						jQuery("#Distid").focus();
					}
			    
				}
				else{
					showMessage('red',keypairData.Description,'');
				}
					


				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});

				}

		}
		
		function DistributorValidation2(Distributorid)
		{
			
			if(Distributorid!='')
				{
			if(Distributorid.length!=8)
				{
				jQuery('#DistId').css("background","#FF9999");
				
				return 0;
				}
			var intonly =new RegExp('^[0-9]*$');
			if(intonly.test(Distributorid))
				{
				return 1;
				}
			else 
				{
				jQuery('#DistId').css("background","#FF9999");
				
				return 0;
				}
				}
			return 1;
		}
	
		
		jQuery("#DistSearch").unbind("click").click(function(){
			var Distributorid=jQuery("#Distid").val();
			if (Distributorid=='')
			{
				showMessage('yellow','Please Enter DistributorId.','');
				}
			else{
			var flag = DistributorValidation2(Distributorid);
			if (flag==0)
				{
				showMessage('yellow','Enter Valid DistributorId.','');
				}
			else{
			showMessage("loading", "Searching for DistributorDetails...", "");
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'JoiningFormTrackingCallBack',
				data:
				{
					id:"SearchDistributorDetails",
					Distributorid: Distributorid,
					moduleCode:"DFT01",
					functionName:jQuery("#actionName").val()
				},
				success:function(data)
				{
					jQuery(".ajaxLoading").hide();

					var historyOrderData = jQuery.parseJSON(data);
					
					if(historyOrderData.Status == 1)
					{
						var searchResult = historyOrderData.Result;
						if(searchResult.length==0)
						{ 	
							showMessage("","No Record Found","");
						}
						
						else
						{	

							jQuery .each(searchResult,function(key,value){	
								if(searchResult[key]['DistributorStatus']==1 || searchResult[key]['DistributorStatus']==2)
								{
								if (searchResult[key]['Isrecieved']==1)
									{
									jQuery("#recieved").attr('checked', true);
									}
								else 
									jQuery("#recieved").attr('checked', false);
								
								jQuery("#Distid").attr('readonly',true);
								
								jQuery('#DistName').val(searchResult[key]['Name']);
								jQuery("#DistName").attr('readonly',true);
								
								jQuery('#DistContact').val(searchResult[key]['DistributorMobNumber']);
								jQuery("#DistContact").attr('readonly',true);
								
								jQuery('#DistJoining').val(searchResult[key]['DateOfJoining']);
								jQuery("#DistJoining").attr('readonly',true);
								}
								
								else
									{
									showMessage("red", "Disttributor is Blocked!", "");
									}
							});

						}
					}
					else
					{
						showMessage("red", historyOrderData.Description, "");
					}
			}
			,
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});	
			}
			}

		});
		
		jQuery("#DistSave").unbind("click").click(function(){
			jQuery("#actionName").val('Save');
			var flag =0;
			var Distributorid =jQuery("#Distid").val();
			var DistContact =jQuery("#DistContact").val();
			var DistName =jQuery("#DistName").val();
			var Repurchase = jQuery("#recieved").is(':checked')? 1 : 0;
			flag = ValidateJoining(Distributorid,flag,Repurchase,DistContact,DistName);
			if(flag==0)
				{
			var r =confirm("Are you sure you want to Approve this Joining?");
			if(r==true)
				{
				  showMessage("loading", "Saving details...", "");
				  
					jQuery.ajax({
						type:'POST',
								url:Drupal.settings.basePath + 'JoiningFormTrackingCallBack',
								data:
								{
								id : 'SaveDistributordetails',
								Distributorid : Distributorid,
								Remarks : jQuery('#remarks').val(),
								DistContact : DistContact,
								Repurchase : Repurchase,
								moduleCode : "DFT01",
								functionName:jQuery("#actionName").val()		
								},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							var itemDescData = jQuery.parseJSON(data);
							if(itemDescData.Status==1)
							{

								var resuts=itemDescData.Result;
								if(resuts[0]['find']=='New')
									{
									showMessage("green","Joining of DistributorId: "+resuts[0]['DistributorId']+' ' +"is Approved Successfully!","");
									}
								else
									{
									showMessage("red","Allready approved by "+resuts[0]['username']+' ' +" at "+resuts[0]['Name']+' ' +" On "+resuts[0]['CreatedDate']+".","");
									
									}
							}
							else
							{
								showMessage('red',itemDescData.Description,'');
							}


								
		
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}
						});
				}


				}

	});
		
		function ValidateJoining(Distributorid,flag,Repurchase,DistContact,DistName)
		
		{
			if(Distributorid=='' || DistName=='')
			{
			showMessage("yellow",'Please Validate details by clicking on Search Button.','');
			flag++;
			return flag;
			}

			if(Repurchase==0)
			{
			showMessage("yellow",'Please check the IsRecieved checbox','');
			flag++;
			return flag;
			}
			
			return flag;
		}

});