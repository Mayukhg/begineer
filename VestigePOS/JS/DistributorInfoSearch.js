/*
Main logic implemented here is distributor enter distributor id.
if new distributor id then clear all grid data.
if not then check all grid data length
 if lenght in grid is not null or 0 then grids are not loaded otherwise grid data loaded according to new distributor.
 */



jQuery(document).ready(function(){ 
	
	

	jQuery("#S_DISDistributorId").attr('maxlength','8');
	
	jQuery("#btnDS01Password").hide();
	
	jQuery("#header").hide();
	jQuery(".breadcrumb").hide();
	jQuery("#triptych-wrapper").hide();
	
	jQuery("#footer-wrapper").hide();
	
	var distributor_id;
	var rowid = 0;
	var validatedDistributorId = 0;
	var distributorPanBankHistoryAvailable;
	var distributorHistoryAvailable;
	var DISTRIBUTOR_PAN_NO_VALIDATION = 0;
	var DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
	var DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 0;
	var UploadedPan=0;
	var UploadedBank=0;
	var moduleCode = jQuery("#moduleCode").val();
	var pan;
	var IFSCCode;
	
	jQuery( "#DISDOB" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0'});
	jQuery( "#Co_DISDOB" ).datepicker({changeMonth: true, changeYear: true, dateFormat: 'd-M-yy',yearRange: '-100:+0'});
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'CommonActionCallback',
		data:
		{
			id:"moduleFunctionHandling",
			moduleCode:moduleCode,
		},
		success:function(data)
		{
			//alert(data);	
			var loggedInUserPrivilegesDIS = jQuery.parseJSON(data);
			
			if(loggedInUserPrivilegesDIS.Status == 1)
			{
				
				loggedInUserPrivilegesForDIS  = loggedInUserPrivilegesDIS.Result;
				
				if(loggedInUserPrivilegesForDIS.length == 0)
				{
					//jQuery(".SCCreateActionButtons").attr('disabled',true); //all button coming as disabled already.
				}
				else
				{
					/*-------------calling initial state----------------*/
					initialStateDIS();
					
					//enable access privileges buttons.
					//There is special type of security because one button from privileges is not depend on other.
					//hence can be enabled all at once.
					//security here is on fields level too .//hence to hide not privileges fields.
					//password and distributor name are two fields here.
					
					enableOrShowElementByPrivilges();
					
					
				}
				
				
				
			}
			else
			{
				showMessage("red", loggedInUserPrivilegesDistRegiData.Description, "");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}
	
	});
	
	//function used to print invoice.
	function showInvoiceReport(invoiceNumber)
	{
		//jQuery("#actionName").val('Print');
		
		showMessage("loading", "Printing invoice...", "");

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSClient',
			data:
			{ 
				
				id:"distributorInfoSearchInvoicePrint",
				invoiceNumber:invoiceNumber,
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var resultsData = jQuery.parseJSON(data);
				if(resultsData.Status==1){
					var results=resultsData.Result;
					jQuery.each(results,function(key,value){
						var arr = {'locationId':results[key]['locationId'],'orderID':results[key]['orderNo'],'isDuplicate':results[key]['isDuplicate'],'GeneratedBy':loggedInUserFullName}; //invocie copy parameter need to watch
						showReport(results[key]['baseUrl'],results[key]['Url'],arr);

					});
				}
				else{

					showMessage('red',resultsData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
			
		});
	}
	
	
	//function used to print Agreement.
	function showDistributorAgreementReport(distributorId)
	{
		//jQuery("#actionName").val('Print');

		
		showMessage("loading", "Printing Distributor Agreement...", "");

		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			data:
			{
				id:"printingDistributorAgreement",
				DistributorId:distributorId,	
				moduleCode:moduleCode,
				functionname:"PrintDistributorAgreement",
			
				
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var resultsData = jQuery.parseJSON(data);
				if(resultsData.Status==1){
					var results=resultsData.Result;
					jQuery.each(results,function(key,value){
						var arr = {'DistributorID':results[key]['DistributorID']}; //
						showReport(results[key]['baseUrl'],results[key]['Url'],arr);

					});
				}
				else{

					showMessage('red',resultsData.Description,'');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
			
		});
	}

	

	
	/*function used to enable or show fields*/
	function enableOrShowElementByPrivilges()
	{
		jQuery(".DISActionButtons").each(function(){

			var buttonid = this.id;
			var extractedButtonId = buttonid.replace("btn","");

			if(loggedInUserPrivilegesForDIS[extractedButtonId] == 1)
			{
				if(extractedButtonId == "DS01EditPassword")
				{
					jQuery("#"+buttonid).show();
				}
				
				else
				{
					jQuery("#"+buttonid).attr('disabled',false);
					
				}
				
			}
			else
			{
				//jQuery("#"+buttonid).attr('disabled',true); //button already disabled.
			}


			//jQuery("#distResignation").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			jQuery("#panBankHistory").attr('disabled',false); //temporary button for filling all fields.
			jQuery("#distInfoHistory").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			jQuery("#searchUpline").attr('disabled',false); //temporary button for filling all fields.
			jQuery("#save").attr('disabled',false); //no status flag coming for this and perhaps have to be enabled all the time.
			jQuery("#reset").attr('disabled',false); //temporary button for filling all fields.
			

		});
	}
	
	
	/*----------------------Distributor history pop-----------------------*/
	
	jQuery(function() {
	    jQuery( "#distributorHistoryPopUp" ).dialog({
	      autoOpen: false,
	      minWidth: 800,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	   });
	});
	
	
	jQuery(function() {
	    jQuery( "#DistributorPanBankInfoPopUp" ).dialog({
	      autoOpen: false,
	      minWidth: 800,
	      minHeight: 500,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	   });
	});
	
	jQuery(function() {
	    jQuery( "#divLookUp" ).dialog({
	      autoOpen: false,
	      minWidth: 640,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	     
	    });
	  });
	
	
	
/*----------------------Distributor PAN Bank Account pop-----------------------*/
	
	jQuery(function() {
	    jQuery( "#BankPANAccountPopUp" ).dialog({
	      autoOpen: false,
	      minWidth: 500,
	      show: {
	        effect: "clip",
	        duration: 200
	      },
	      hide: {
	        effect: "clip",
	        duration: 200
	      }
	   });
	});
	
	
	



	/*----------------------Generating tabs using jquery ui-------------------------*/
	
	jQuery(function() {
		//alert("dfadf");
	    jQuery( "#DivDistributorInfoSearchTab" ).tabs(); // Create tabs using jquery ui.
	});
	
	
	
	
	
	
	/*-------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Initial state of distributor information search.
	 */
	function initialStateDIS()
	{
		jQuery("#save").hide();
		
		jQuery("#distributorInfoSearchAction .DISTSearchInput").attr("readonly",false);
		jQuery("#distributorInfoSearchAction .DISTSearchInput").css("background","white");
		
		jQuery(".DistributorMasterInformation .CommonSelect").attr("disabled",true);
		//jQuery(".DistributorMasterInformation .CommonSelect").attr("readonly",true);
		//jQuery(".DistributorMasterInformation .CommonSelect").css("background","#ebebe3");
		
		//jQuery(".DistributorMasterInformation .DISTInput").attr("disabled",true);
		jQuery(".DistributorMasterInformation .DISTInput").attr("readonly",true);
		jQuery(".DistributorMasterInformation .DISTInput").css("background","#ebebe3");
		
		//jQuery(".DistributorCurrentRanking .DISTInput").attr("disabled",true);
		jQuery(".DistributorCurrentRanking .DISTInput").attr("readonly",true);
		jQuery(".DistributorCurrentRanking .DISTInput").css("background","#ebebe3");
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("disabled",true);
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("readonly",true);
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").css("background","#ebebe3");
		
		jQuery("#IsformAvailable").attr("disabled",true);
		
	}
	
	
	
	/*-------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Edit state of distributor information search.
	 */
	function editStateDIS()
	{
		
		//jQuery(".distributorInfoSearchAction .DISTSearchInput").attr("disabled",true);
		jQuery("#distributorInfoSearchAction .DISTSearchInput").attr("readonly",true);
		jQuery("#distributorInfoSearchAction .DISTSearchInput").css("background","#ebebe3");
		
		jQuery(".DistributorMasterInformation .CommonSelect").attr("disabled",false);
		//jQuery(".DistributorMasterInformation .CommonSelect").attr("readonly",false);
		//jQuery(".DistributorMasterInformation .CommonSelect").css("background","white");
		
		//jQuery(".DistributorMasterInformation .DISTInput").attr("disabled",false);
		jQuery(".DistributorMasterInformation .DISTInput").attr("readonly",false);
		jQuery(".DistributorMasterInformation .DISTInput").css("background","white");
		
		//jQuery(".DistributorCurrentRanking .DISTInput").attr("disabled",false);
		jQuery(".DistributorCurrentRanking .DISTInput").attr("readonly",true);
		jQuery(".DistributorCurrentRanking .DISTInput").css("background","#ebebe3");
		
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("readonly",true);
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").css("background","#ebebe3");
		
		if(jQuery("#IsformAvailable").attr("checked") == false)
				{
			jQuery("#IsformAvailable").attr("disabled",false);
				}
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("disabled",false);
		//jQuery(".DistributorCurrentRanking .DISTInput").attr("readonly",true);
		
		//jQuery(".DistributorCurrentRanking .DISTInput").addClass("DisabledField");
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("readonly",true);
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").addClass("DisabledField");
		
		//disabledFieldsInEditMode();
		
		if(jQuery("#Pan").val() != '')
			{
			jQuery("#Pan").attr("readonly",true);
			}
		if (jQuery("#BankDetail").val() != '')
			{
			jQuery("#BankDetail").attr("disabled",true);
			jQuery("#Bank").attr("disabled",true);
			jQuery("#AccountNO").attr("disabled",true);
			}
		
		
	}
	
	function editStateDISReset()
	{
		
		//jQuery(".distributorInfoSearchAction .DISTSearchInput").attr("disabled",true);
		jQuery("#distributorInfoSearchAction .DISTSearchInput").attr("readonly",false);
		jQuery("#distributorInfoSearchAction .DISTSearchInput").css("background","#ebebe3");
		
		jQuery(".DistributorMasterInformation .CommonSelect").attr("disabled",true);
		//jQuery(".DistributorMasterInformation .CommonSelect").attr("readonly",false);
		//jQuery(".DistributorMasterInformation .CommonSelect").css("background","white");
		
		//jQuery(".DistributorMasterInformation .DISTInput").attr("disabled",false);
		jQuery(".DistributorMasterInformation .DISTInput").attr("readonly",true);
		jQuery(".DistributorMasterInformation .DISTInput").css("background","#ebebe3");
		
		//jQuery(".DistributorCurrentRanking .DISTInput").attr("disabled",false);
		jQuery(".DistributorCurrentRanking .DISTInput").attr("readonly",true);
		jQuery(".DistributorCurrentRanking .DISTInput").css("background","#ebebe3");
		
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("readonly",true);
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").css("background","#ebebe3");
		
		
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("disabled",false);
		//jQuery(".DistributorCurrentRanking .DISTInput").attr("readonly",true);
		
		//jQuery(".DistributorCurrentRanking .DISTInput").addClass("DisabledField");
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("readonly",true);
		
		//jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").addClass("DisabledField");
		
		//disabledFieldsInEditMode();
		
		
		
	}
	

	/*-------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Edit state of distributor information search.
	 */
	function saveStateDIS()
	{
		
		jQuery("#distributorInfoSearchAction .DISTSearchInput").attr("disabled",true);
		
		jQuery(".DistributorMasterInformation .CommonSelect").attr("disabled",false);
		
		jQuery(".DistributorMasterInformation .DISTInput").attr("disabled",false);
		
		jQuery(".DistributorCurrentRanking .DISTInput").attr("readonly",true);
		
		jQuery(".DistributorCurrentRanking .DISTInput").addClass("DisabledField");
		
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").attr("readonly",true);
		
		jQuery(".DistributorCurrentPreviousPvBvComputation .DISTInput").addClass("DisabledField");
		
		disabledFieldsInEditMode();
		
	}
	
	/*----------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * disabled fields in edit mode.
	 */
	function disabledFieldsInEditMode()
	{
		jQuery("#SerialNo").attr("readonly",true); 
		jQuery("#SerialNo").addClass("DisabledField");
		
		jQuery("#Zone").attr("readonly",true);
		jQuery("#Zone").addClass("DisabledField");
		
		jQuery("#bank").attr("readonly",true);
		jQuery("#bank").addClass("DisabledField");
		
		jQuery("#SaveOn").attr("readonly",true);
		jQuery("#SaveOn").addClass("DisabledField");
	}
	
	
	
jQuery("#btn-printagree").click(function(){
		
		
		var distributorId = jQuery("#S_DISDistributorId").val(); //S - search distributor id field.
		
		if(distributorId.length != 8)
		{
			showMessage("", "Enter valid distributor id", "");
			
			jQuery("#S_DISDistributorId").css("background", "#FF9999");
		}
		else
		{
			showDistributorAgreementReport(distributorId);
			
		}
		
		
	});
	
	
	
	/*----------------------Enter pressing after entering distributor id-------------------------*/
	
	jQuery("#S_DISDistributorId").keydown(function(event){

		if(event.which == 13 || event.which == 9){
			
			var distributorId = jQuery("#S_DISDistributorId").val(); //S - search distributor id field.
			
			if(distributorId.length != 8)
			{
				showMessage("", "Enter valid distributor id", "");
				
				jQuery("#S_DISDistributorId").css("background", "#FF9999");
			}
			else
			{
				checkValidDistributor(distributorId,"current"); //function to validate distributor id.
				
			}
			
			
		}
	});
	
	
	/*-------------------search upline--------------------*/
	jQuery("#searchUpline").click(function(){
		var distributorId = jQuery("#S_DISDistributorId").val();
		
		if(distributorId.length != 8)
		{
			showMessage("", "Enter valid distributor id", "");
			
			jQuery("#S_DISDistributorId").css("background", "#FF9999");
		}
		else
		{
			checkValidDistributor(distributorId,"upline");
		}
		
		 
	});
	
	
	/*---------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function checking about null and invalide distributor id.
	 */
	function checkValidDistributor(distributorId,searchCondition)
	{
		if(distributorId == '')
		{
			jQuery("#S_DISDistributorId").css("background", "#FF9999");
			emptyFieldsData();
		}
		else
		{
			
			showMessage("loading", "Loading information...", "");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
				data:
				{
					id:"checkDistributor",
					distributorId:distributorId,

				},
				success:function(data)
				{
					var returnedDistibutorId = jQuery.parseJSON(data);
					
					if(returnedDistibutorId.Status == 1)
					{
						var result = returnedDistibutorId.Result;
						
						if(result.length == 0)
						{
							
							jQuery(".ajaxLoading").hide();
							
							jQuery("#S_DISDistributorId").css("background", "#FF9999");
							showMessage("red", "Invalid distributor id", "");
							emptyFieldsData();
							
							jQuery("#S_DISDistributorId").val(distributorId);
						}
						else
						{
							distributor_id = distributorId;
							var distributorFirstName = jQuery("#S_DISFirstName").val();
							var distributorLastName = jQuery("#S_DISLastName").val();
							jQuery("#S_DISDistributorId").css("background", "white");
							
							jQuery(".upload_files").attr('disabled',false);
							if(searchCondition == "current")
							{
								distributorInfoSearch(distributorId,distributorFirstName,distributorLastName,searchCondition); //call function to populate distributor information after all validation.
							}
							if(searchCondition == "upline")
							{
								distributorInfoSearch(distributorId,distributorFirstName,distributorLastName,searchCondition); //call function to populate distributor information after all validation.
							}
							
							
						}
					}
					else
					{
						showMessage("red",returnedDistibutorId.Description, "");
					}
					
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}
	}
	
	/*---------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * function to remove fields data.
	 */
	function emptyFieldsData()
	{
		jQuery(".DISTInput").val('');// Empty all textfiled except Search fields.
		
		
		jQuery(".CommonSelect").empty(); //Empty all select in form .
		
		jQuery("#title").append("<option value='0'>Select</option>");
		jQuery("#title").append("<option value='1'>Mr.</option>");
		jQuery("#title").append("<option value='2'>Ms.</option>");
		jQuery("#title").append("<option value='3'>Dr.</option>");
		jQuery("#title").append("<option value='4'>M/s.</option>");
		
		jQuery("#co_title").append("<option value='0'>Select</option>");
		jQuery("#co_title").append("<option value='1'>Mr.</option>");
		jQuery("#co_title").append("<option value='2'>Ms.</option>");
		jQuery("#co_title").append("<option value='3'>Dr.</option>");
		jQuery("#co_title").append("<option value='4'>M/s.</option>");
		
		jQuery(".DISTSearchInput").val('');//Empty all search fields.
		
		jQuery("#S_DISDistributorId").attr("readonly",false);
		
		jQuery(".upload_files").val('');   
		jQuery(".checkbox_status").val(''); 
		jQuery(".upload_files").attr("disabled",true);
		jQuery(".checkbox_status").attr("checked", false);
		
		jQuery("#save").hide();
		jQuery("#btnDS01Update").show();
		jQuery("#S_DISDistributorId").css("background","white");
		editStateDISReset();
		jQuery("#S_DISDistributorId").focus(); //move focus to distributor information search id.
		loadingBankDetails();
		
	}
	
	
	
	jQuery("#btnDS01Search").click(function(){
		var distributorId = jQuery("#S_DISDistributorId").val();
		//distributor_id = distributorId;
		
		if(distributorId.length!=8)
		{
			showMessage("red", "Enter valid distributor id", "");
			
			jQuery("#S_DISDistributorId").css("background", "#FF9999");
		}
		else
		{
			checkValidDistributor(distributorId,"current");  //function to validate distributor id.
		}
		
	});
	
	/*----------------reset--------------------*/
	
	jQuery("#reset").click(function(){
		emptyFieldsData();
		
		
		
	});
	
	
	/*---------------------------------Distributor History Naviation------------------------------*/
	
	jQuery("#distInfoHistory").click(function(){
		
		var distributorId = jQuery("#S_DISDistributorId").val();
		//alert(distributorId);
		
		if(distributorId == '')
		{
			Message("", "Please, enter distributor id", "");
		}
		else
		{
			distributorHistoryInformation(distributorId);
		}
		
		
		
	});
	
/*---------------------------------Distributor Pan Bank Naviation------------------------------*/
	
	jQuery("#panBankHistory").click(function(){
		
		var distributorId = jQuery("#S_DISDistributorId").val();
		//alert(distributorId);
		
		if(distributorId == '')
		{
			showMessage("", "Please, enter distributor id", "");
		}
		else
		{
			distributorPanBankHistoryInformation(distributorId);
		}
		
	});
	
	
	loadingBankDetails();
	
	var distributorBankDetails;
	
	function loadingBankDetails()
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSDistributorRegistration',
			data:
			{
				id:"bankDetails",
				
			},
			success:function(data)
			{
				jQuery(".ajaxLoading").hide();
				
				var resultData = jQuery.parseJSON(data);
				
				if(resultData.Status == 1)
				{
					distributorBankDetails = resultData.Result;
					
					jQuery.each(distributorBankDetails,function(key,value){
						
						jQuery("#Bank").append("<option  value=\""+distributorBankDetails[key]['BankId']+"\""+">"+distributorBankDetails[key]['BankName']+"</option>"); //Bank option to shown.
						
					});
				}
				else
				{
					showMessage("red",resultData.Description,"");
				}
				
				
				//return data;
			}, //end success
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
			
		});
	}
	
	
	/*--------------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * function provide distributor Pan Bank history details.
	 */
	function distributorPanBankHistoryInformation(distributorId)
	{
		jQuery( "#BankPANAccountPopUp" ).dialog( "open" );
		
		panBAnkNavigationInitialState();
		
		function panBAnkNavigationInitialState()
		{
			jQuery(".DISPanBankInput").attr('disabled',true);
			
			jQuery(".DISPanBankInput").val('');
		}
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			//data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
			data:
			{
				id:"distributorPanBankHistorySearch",
				distributorId:distributorId,
			},
			success: function(data)
			{
				//alert(data);
				var panBankHistoryData = jQuery.parseJSON(data);
				
				if(panBankHistoryData.Status == 1)
				{
					distributorPanBankHistoryAvailable = panBankHistoryData.Result;
					
					var noOfPanBankHistoryRecord = distributorPanBankHistoryAvailable.length;
					
					//alert(noOfPanBankHistoryRecord);
					
					var noOfPanBankHistoryRecordTemp = noOfPanBankHistoryRecord - 1;
					
					if(noOfPanBankHistoryRecord > 0)
					{
						jQuery("#previousPanBankHistoryResult").unbind("click").click(function(){
							
							if(noOfPanBankHistoryRecordTemp <= 0)
							{
								showMessage("", "You already on first record", "");
							}
							else
							{
								noOfPanBankHistoryRecordTemp = noOfPanBankHistoryRecordTemp - 1;
								
								distributorPanBankHistoryFormFill(distributorPanBankHistoryAvailable,noOfPanBankHistoryRecordTemp);
							}
							
							
						});
						
						jQuery("#nextPanBankHistoryResult").unbind("click").click(function(){
							
							if(noOfPanBankHistoryRecordTemp >= noOfPanBankHistoryRecord-1)
							{
								showMessage("", "You are already on the Last Record", "");
							}
							else
							{
								noOfPanBankHistoryRecordTemp = noOfPanBankHistoryRecordTemp + 1;
								
								distributorPanBankHistoryFormFill(distributorPanBankHistoryAvailable,noOfPanBankHistoryRecordTemp);
							}
							
							
						});
						
						jQuery("#firstPanBankHistoryResult").unbind("click").click(function(){
							
							noOfPanBankHistoryRecordTemp = 0;
							
							distributorPanBankHistoryFormFill(distributorPanBankHistoryAvailable,noOfPanBankHistoryRecordTemp);
							
						});
						
						jQuery("#lastPanBankHistoryResult").unbind("click").click(function(){
							
							noOfPanBankHistoryRecordTemp = noOfPanBankHistoryRecord - 1;
							
							distributorPanBankHistoryFormFill(distributorPanBankHistoryAvailable,noOfPanBankHistoryRecordTemp);
							
						});
						
						
					}
					
					
					function distributorPanBankHistoryFormFill(distributorPanBankHistoryAvailable,Key)
					{
						
						//alert(distributorHistoryAvailable[noOfRecordUpdatedTempVar]);
						jQuery.each(distributorPanBankHistoryAvailable,function(sKey,value){
							
							if(Key == sKey)
							{
								
								jQuery("#DISPanBankId").val(distributorPanBankHistoryAvailable[sKey]['distributorid']);
								jQuery("#DISPanBankFirstName").val(distributorPanBankHistoryAvailable[sKey]['DistributorFirstName']);
								jQuery("#DISPanBankLastName").val(distributorPanBankHistoryAvailable[sKey]['DistributorLastName']);
								jQuery("#DISPanBankPan").val(distributorPanBankHistoryAvailable[sKey]['distributorPANNumber']);
								jQuery("#DISPanBankBankName").val(distributorPanBankHistoryAvailable[sKey]['BankName']);
								jQuery("#DISPanBankBankAccount").val(distributorPanBankHistoryAvailable[sKey]['DistributorBankAccNumber']);
								jQuery("#DISPanBankRTGS").val(distributorPanBankHistoryAvailable[sKey]['DistributorBankBranch']);
								jQuery("#DISPanBankSavedBy").val(distributorPanBankHistoryAvailable[sKey]['UserName']);
								jQuery("#DISPanBankSavedOn").val(distributorPanBankHistoryAvailable[sKey]['ModifiedDate']);
								
							}
							
						});
						
					}
				}
				else
				{
					showMessage("red",panBankHistoryData.Description,"");
				}
				
			},//end success.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	
	
	/*--------------------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * function provide distributor history details.
	 */
	function distributorHistoryInformation(distributorId)
	{
		jQuery( "#distributorHistoryPopUp" ).dialog( "open" );
		
		historyNavigationInitialState();
		
		function historyNavigationInitialState()
		{
			jQuery(".DISHistInput").attr('disabled',false);
			
			jQuery(".DISHistInput").val('');
		}
		
		jQuery(".DISHistInput").attr('disabled',true);
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			//data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
			data:
			{
				id:"distributorHistorySearch",
				distributorId:distributorId,
			},
			success: function(data)
			{
				var distributorHistorySearchData = jQuery.parseJSON(data);
				
				if(distributorHistorySearchData.Status == 1)
				{
					distributorHistoryAvailable = distributorHistorySearchData.Result;
					
					var noOfRecordUpdated = distributorHistoryAvailable.length;
					
					var noOfRecordUpdatedTempVar = noOfRecordUpdated - 1;
					
					if(noOfRecordUpdated > 0)
					{
						jQuery("#previousHistoryResult").click(function(){
							
							if(noOfRecordUpdatedTempVar <= 0)
							{
								showMessage("", "You are already on the First Record", "");
							}
							else
							{
								noOfRecordUpdatedTempVar = noOfRecordUpdatedTempVar - 1;
								
								distributorHistoryFormFill(distributorHistoryAvailable,noOfRecordUpdatedTempVar);
							}
							
							
						});
						
						jQuery("#nextHistoryResult").click(function(){
							
							
							if(noOfRecordUpdatedTempVar >= noOfRecordUpdated-1)
							{
								showMessage("", "You are already on the Last Record", "");
							}
							else
							{
								noOfRecordUpdatedTempVar = noOfRecordUpdatedTempVar + 1;
								
								distributorHistoryFormFill(distributorHistoryAvailable,noOfRecordUpdatedTempVar);
							}
							
						});
						
						jQuery("#firstHistoryResult").click(function(){
							
							noOfRecordUpdatedTempVar = 0;
							
							distributorHistoryFormFill(distributorHistoryAvailable,noOfRecordUpdatedTempVar);
							
						});
						
						jQuery("#lastHistoryResult").click(function(){
							
							noOfRecordUpdatedTempVar = noOfRecordUpdated - 1;
							
							distributorHistoryFormFill(distributorHistoryAvailable,noOfRecordUpdatedTempVar);
							
						});
						
						
					}
					else
					{
						showMessage("", "No History available", "");
					}
					
					
					
					function distributorHistoryFormFill(distributorHistoryAvailable,Key)
					{
						//alert(distributorHistoryAvailable[noOfRecordUpdatedTempVar]);
						jQuery.each(distributorHistoryAvailable,function(sKey,value){
							
							if(Key == sKey)
							{
								
								jQuery("#DISHistTitle").val(distributorHistoryAvailable[sKey]['distributortitle']);
								jQuery("#DISCoHistTitle").val(distributorHistoryAvailable[sKey]['co_distributortitle']);
								jQuery("#DISHistFirstname").val(distributorHistoryAvailable[sKey]['distributorfirstname']);
								jQuery("#DISCoHistFirstname").val(distributorHistoryAvailable[sKey]['co_distributorfirstname']);
								jQuery("#DISHistLastname").val(distributorHistoryAvailable[sKey]['distributorlastname']);
								jQuery("#DISCoHistLastname").val(distributorHistoryAvailable[sKey]['co_distributorlastname']);
								jQuery("#DISHistDOB").val(distributorHistoryAvailable[sKey]['distributordob']);
								jQuery("#DISCoHistDOB").val(distributorHistoryAvailable[sKey]['co_distributordob']);
								jQuery("#DISHistEnrolledOn").val(distributorHistoryAvailable[sKey]['enrolledNo']);
								jQuery("#DISHistUplineNo").val(distributorHistoryAvailable[sKey]['uplinedistributorid']);
								jQuery("#DISHisAddress1").val(distributorHistoryAvailable[sKey]['distributoraddress1']);
								jQuery("#DISHisAddress2").val(distributorHistoryAvailable[sKey]['distributoraddress2']);
								jQuery("#DISHisAddress3").val(distributorHistoryAvailable[sKey]['distributoraddress3']);
								jQuery("#DISHisPhone").val(distributorHistoryAvailable[sKey]['distributortelenumber']);
								jQuery("#DISHisMobile").val(distributorHistoryAvailable[sKey]['distributormobnumber']);
								jQuery("#DISHisEmail").val(distributorHistoryAvailable[sKey]['distributoremailid']);
								jQuery("#DISHisCity").val(distributorHistoryAvailable[sKey]['cityName']);
								jQuery("#DISHisState").val(distributorHistoryAvailable[sKey]['statename']);
								jQuery("#DISHisPin").val(distributorHistoryAvailable[sKey]['distributorpincode']);
								jQuery("#DISHisZone").val(distributorHistoryAvailable[sKey]['zonename']);
								jQuery("#DISHisLevel").val(distributorHistoryAvailable[sKey]['level']);
								jQuery("#DISHisBank").val(distributorHistoryAvailable[sKey]['bankname']);
								jQuery("#DISHisSavedBy").val(distributorHistoryAvailable[sKey]['username']);
								jQuery("#DISHisPan").val(distributorHistoryAvailable[sKey]['distributorpannumber']);
								jQuery("#DISHisSavedOn").val(distributorHistoryAvailable[sKey]['modifieddate']);
								jQuery("#DISHisRemarks").val(distributorHistoryAvailable[sKey]['remarks']);
								
								
							}
							
							
						});
					}
				}
				else
				{
					ShowMessage("red",distributorHistorySearchData.Description,"");
				}
	
			},// end success
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	

	/*---------------------------edit distributor information-----------------------------*/
	jQuery("#btnDS01Update").click(function(){
		
		var distributorId = jQuery("#S_DISDistributorId").val();
		if(distributorId == '')
		{
			showMessage("","Please, enter distributor id", "");
		}
		else
		{
			editStateDIS();
			var edit=1;
			jQuery("#save").show();
			jQuery("#btnDS01Update").hide();
		}
		
		
		
	});
	
	/*---------------------------------save editaed Distributor information-------------------------------*/
	
	jQuery("#save").click(function(){
	
		if(validateDistributorToUpdate() == 1)
		{
			updateValidatedDistributorInfo();
		}
		else if(validateDistributorToUpdate() == 2)
		{
			
		}
		else
		{
			showMessage("", "Enter required fields", "");
		}
		
		
		
	});
	
	
	function validateDistributorToUpdate()
	{
		var validateInputFields = 1;
		
		if(jQuery('#Pan').val()!=pan) 
			{
			
			if(jQuery('#pan_checkbox').attr('disabled')==false)
				{
			
			if (UploadedPan!=1)
				{
				showMessage("", "Please Update Pan Image", "");
				validateInputFields = 2;
				}
				}
		} 
	
			
			
			
		if(jQuery('#BankDetail').val()!=IFSCCode) 
		{
			
			var Bank = this.value;
			if(jQuery('#cencelled_cheque_checkbox').attr('disabled')==false)
			{
			if (UploadedBank!=1)
				{
				showMessage("", "Please Update Bank Image", "");
				validateInputFields = 2;
				}
			
			}
		}
         
         jQuery("#AccountNO").change(function(){   
 			
 			var Account = this.value;
 			if(jQuery('#cencelled_cheque_checkbox').attr('disabled')==false)
 				{
 			if (UploadedBank!=1)
 				{
 				showMessage("", "Please Update Bank Image", "");
 				validateInputFields = 2;
 				}
 			
 				}
 		});

      
		
		 
		jQuery(".clsRequiredInput").each(function(){
			
			if(jQuery("#"+this.id).val() == '')
			{
				jQuery("#"+this.id).css('background','#ff9999'); //To indicate one empty field.
				
				validateInputFields = 0;
			}
			
		});
		jQuery(".clsRequiredSelect").each(function(){
			
			if(jQuery("#"+this.id).val() == 0)
			{
				jQuery("#"+this.id).css('background','#ff9999'); //To indicate one empty field.
				
				validateInputFields = 0;
			}
			
		});
		
		return validateInputFields;
		
	}
	
	function updateValidatedDistributorInfo()
	{
		
		if(jQuery("#Pan").val() == '')
		{
			DISTRIBUTOR_PAN_NO_VALIDATION = 1;
		}
		else if(jQuery("#Pan").attr('readonly') == true)
			{
			
			DISTRIBUTOR_PAN_NO_VALIDATION = 1;
			}
		
		if(jQuery("#BankDetail").val() == '')
		{
			DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 1;
			
			DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
		}
		else if(jQuery("#BankDetail").attr('disabled') == true)
			{
              DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 1;
			
			  DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
			}
		
		 if(DISTRIBUTOR_PAN_NO_VALIDATION == 0)
			{
				showMessage("", "Pan number not validated", "");
				return;
			}
		 
			else if(jQuery("#BankDetail").val() == '' && (jQuery("#Bank").val() != -1 || jQuery("#AccountNO").val() !=''))
			{

				var confirmProceed = confirm("Your Bank and Account number information will be lost without RTGS/IFSC code. Do you want to proceed ? ");

				if(confirmProceed == true)
				{
					jQuery("#Bank").val(-1);
					jQuery("#AccountNO").val('');

					DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION =1;
					DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
				}
				else
				{
					return;
				}



			}
			else if(jQuery("#BankDetail").val() != '' && jQuery("#AccountNO").val() == '')
			{
				DISTRIBUTOR_ACCOUNT_NO_VALIDATION =0;
				
				showMessage("", "Enter Distributor Account Number", "");
				
				
			}
			else if(DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION == 0)
			{
				showMessage("", "RTGS Code not validated", "");
				return;
				
			}
			else if(DISTRIBUTOR_ACCOUNT_NO_VALIDATION == 0)
			{
				showMessage("", "Account number length not according to Selected Bank Branch", "");
				return;
				
			}

		//alert("edit clicked");
		if(validateMobileNumber(jQuery("#Mobile").val()) == 0){ 	 	
			jQuery("#Mobile").css('background-color','#FF9999'); 	 	
			showMessage("", "Invalid Mobile Number", ""); 	 	
			return; 	 	
		} 
		showMessage("loading", "Updating distributor information", "");
		
		jQuery("#save").hide();
		jQuery("#btnDS01Update").show();
		
		var distributorDOB = jQuery("#DISDOB").val();
		var fomattedDistributorDOB = getDate(distributorDOB);
		if(returnAge(fomattedDistributorDOB,18) == 0){			
			jQuery("#DISDOB").css('background-color','#FF9999');		 
			showMessage("", "Invalid Distributor Age", ""); 	 	
			return; 	 	
		} 
		
		var coDistributorDOB = jQuery("#Co_DISDOB").val();
		var fomatted_CO_DistributorDOB = getDate(coDistributorDOB);
		if(returnAge(fomatted_CO_DistributorDOB,18) == 0){ 	 	
			jQuery("#Co_DISDOB").css('background-color','#FF9999'); 	 	
			showMessage("", "Invalid Co-Distributor Age", ""); 	 	
			return; 	 	
		} 	 	
			 	 	
		jQuery("#DISDOB").val(fomattedDistributorDOB); 
		jQuery("#Co_DISDOB").val(fomatted_CO_DistributorDOB);
		
		jQuery("#Bank").attr("disabled",false);
		if(!jQuery("#Bank").val()){ 	 	
			jQuery("#Bank").val(-1); 	 	
		} 
		var formdata = jQuery("#distributorInfoSearchForm").serialize();
		
		var oFormData =  jQuery("#distributorInfoSearchForm").serializeArray();
		
		jQuery("#DISDOB").val(distributorDOB);
		jQuery("#Co_DISDOB").val(coDistributorDOB);	
		
		
		//alert(formdata);
		

		 var oJsonFormData = {};
		   
		    jQuery.each(oFormData, function() {
		        if (oJsonFormData[this.name] !== undefined) {
		            if (!oJsonFormData[this.name].push) {
		            	oJsonFormData[this.name] = [oJsonFormData[this.name]];
		            }
		            oJsonFormData[this.name].push(this.value || '');
		        } else {
		        	oJsonFormData[this.name] = this.value || '';
		        }
		    });
		    
		   // alert(oJsonFormData);
		    //alert(JSON.stringify(o));
		    
		    DISUpdateJsonFormData = oJsonFormData;
		
		    if(jQuery("#IsformAvailable").attr("checked") == true)
		    	{
		    	  var isformavailable='1';
		    	}
		    jQuery("#Bank").attr("disabled",true);
		
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			//data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
			data:
			{
				moduleCode:moduleCode,
				functionName:"Update",
				id:"distributorInformationSearchEdit",
				DISFormDataForJson:DISUpdateJsonFormData,
				DISEditformData:formdata,
				isformavailable:isformavailable,
			},
			success: function(data)
			{

				//alert(data);
				jQuery(".ajaxLoading").hide();
				
				
				var distributorinfoEditData = jQuery.parseJSON(data);
				
				if(distributorinfoEditData.Status == 1)
				{
					
					showMessage("green", "Distributor information updated", "");
					
					initialStateDIS();
	
				}
				else
				{
					showMessage("red", distributorinfoEditData.Description, "");
				}
				
				
				
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		});
	}
	
	/**
	 * Funciton populating fields with distributor information.
	 */
	function distributorInfoSearch(distributorId,distributorFirstName,distributorLastName,searchCondition)
	{
		//alert(distributorId);
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			data:
			{
				id:"distributorInformation",
				distributorId:distributorId,
				distributorFirstName:distributorFirstName,
				distributorLastName:distributorLastName,
				searchCondition:searchCondition,
				index:0
			},
			success:function(data)
			{
				//alert(data);
				var distributorData = jQuery.parseJSON(data);
				
				if(distributorData.Status == 1)
				{
					var oDistributorInfo = distributorData.Result;
					
					jQuery(".ajaxLoading").hide();// To hide loading image (div)
					
					jQuery('#downline_jqgrid').jqGrid('clearGridData', true); //clear downline grid.
					
					jQuery('#bonusGrid').jqGrid('clearGridData', true); //clear all bonus.
					
					jQuery('#todayInvoiceGrid').jqGrid('clearGridData', true); //clear all today invoice.
					
					jQuery('#currentMonthInvoiceGrid').jqGrid('clearGridData', true); //clear current month invoice.
					
					jQuery('#allInvoiceGrid').jqGrid('clearGridData', true); //clear all invoice.
					
					jQuery('#distributorRankOrgGrid').jqGrid('clearGridData', true); //clear distributor level success grid.
					
					jQuery.each(oDistributorInfo,function(key,value){
						
						jQuery("#S_DISDistributorId").val(oDistributorInfo[key]['DISTRIBUTORID']);
						
						jQuery("#Bank").val(oDistributorInfo[key]['BANKID']);
						
						if (oDistributorInfo[key]['panisapprove']==0 || oDistributorInfo[key]['panisapprove']==1)
							{
							
							jQuery("#pan_checkbox").attr("disabled",true);
							jQuery("#pan_checkbox1").attr("checked", false);
							}
						
						if (oDistributorInfo[key]['bankisapprove']==0 || oDistributorInfo[key]['bankisapprove']==1)
						{
							jQuery("#cencelled_cheque_checkbox").attr("disabled",true);
							jQuery("#cencelled_cheque_checkbox1").attr("checked", false);
						
						}
						
						
						if (oDistributorInfo[key]['PANNUMBER']==undefined || oDistributorInfo[key]['PANNUMBER']==null || oDistributorInfo[key]['PANNUMBER']=='-1')
						{
							pan='';
						}
					else
						{
						pan=oDistributorInfo[key]['PANNUMBER'];
						}
						
						if (oDistributorInfo[key]['BankBranchCode']==undefined || oDistributorInfo[key]['BankBranchCode']==null || oDistributorInfo[key]['BankBranchCode']=='-1')
							{
							IFSCCode='';
							}
						else
							{
						IFSCCode=oDistributorInfo[key]['BankBranchCode'];
							}
						if(oDistributorInfo[key]['PANNUMBERLINK'] != null )
						{
							jQuery("#distributorPanNumber").html("<a id='panLink' title='Pan proof' class='distributorProofsLinks' style='text-decoration: underline;' href="+oDistributorInfo[key]['PANNUMBERLINK']+">Pan number</a>");
						}
						if(oDistributorInfo[key]['ADDRESSLINK'] != null )
						{
							jQuery("#distributorAddress").html("<a id='addressLink' title='Address proof' class='distributorProofsLinks' style='text-decoration: underline;' href="+oDistributorInfo[key]['ADDRESSLINK']+">Address</a>");
						}
						if(oDistributorInfo[key]['CANCELLEDCHEQUELINK'] != null )
						{
							jQuery("#distributorCancelledCheque").html("<a id='cancelledChequeLink' title='Cancelled cheque proof' class='distributorProofsLinks' style='text-decoration: underline;' href="+oDistributorInfo[key]['CANCELLEDCHEQUELINK']+">Account</a>");
						}
						if(oDistributorInfo[key]['IsFormAvailable'] == 1){
							jQuery("#IsformAvailable").attr("checked", true);
							jQuery("#IsformAvailable").attr("disabled", true);
							
						}
						else{
							jQuery("#IsformAvailable").attr("checked", false);
						}
						
						distributor_id = oDistributorInfo[key]['DISTRIBUTORID'];
						jQuery("#isMiniBranch").val(oDistributorInfo[key]['IsMiniBranch']);
						jQuery("#S_DISFirstName").val(oDistributorInfo[key]['DISTRIBUTORFIRSTNAME']);
						jQuery("#S_DISLastName").val(oDistributorInfo[key]['DISTRIBUTORLASTNAME']);
						jQuery("#title").val(oDistributorInfo[key]['DISTRIBUTORTITLEID']); 
						jQuery("#btnDS01EditDistributorName").val(oDistributorInfo[key]['DISTRIBUTORFIRSTNAME']); //given button because of one common function written.//later have to change with textfield.
						jQuery("#DISLastname").val(oDistributorInfo[key]['DISTRIBUTORLASTNAME']);
						jQuery("#DISDOB").val(displayDate(oDistributorInfo[key]['DISTRIBUTORDOB']));
						jQuery("#EnrolledOn").val(oDistributorInfo[key]['DISTRIBUTORREGISTRATIONDATE']);
						jQuery("#UplineNo").val(oDistributorInfo[key]['UPLINEDISTRIBUTORID']);
						jQuery("#co_title").val(oDistributorInfo[key]['CODISTRIBUTORTITLEID']);
						jQuery("#Co_DISFirstname").val(oDistributorInfo[key]['CO_DISTRIBUTORFIRSTNAME']);
						jQuery("#Co_DISLastname").val(oDistributorInfo[key]['CO_DISTRIBUTORLASTNAME']);
						jQuery("#Co_DISDOB").val(displayDate(oDistributorInfo[key]['CO_DISTRIBUTORDOB']));
						jQuery("#btnDS01Password").val(oDistributorInfo[key]['PASSWORD']);
						jQuery("#Address1").val(oDistributorInfo[key]['ADDRESS1']);
						jQuery("#Address2").val(oDistributorInfo[key]['ADDRESS2']);
						jQuery("#Address3").val(oDistributorInfo[key]['ADDRESS3']);
						jQuery("#SerialNo").val(oDistributorInfo[key]['SERIALNO']);
						jQuery("#Zone").val(oDistributorInfo[key]['ZONE']);
						jQuery("#Pin").val(oDistributorInfo[key]['PINCODE']);
						jQuery("#Fax").val(oDistributorInfo[key]['FAXNUMBER']);
						jQuery("#Phone").val(oDistributorInfo[key]['TELEPHONENUMBER']);
						jQuery("#Mobile").val(oDistributorInfo[key]['MOBILENUMBER']);
						jQuery("#Pan").val(oDistributorInfo[key]['PANNUMBER']);
						jQuery("#panNumber").val(oDistributorInfo[key]['PANNUMBER']); 
						jQuery("#Email").val(oDistributorInfo[key]['EMAILID']);
						jQuery("#BankDetail").val(oDistributorInfo[key]['BankBranchCode']); 
						jQuery("#IFSCCode").val(oDistributorInfo[key]['BankBranchCode']);
						jQuery("#distributorBank").val(oDistributorInfo[key]['BANKNAME']);
						jQuery("#AccountNO").val(oDistributorInfo[key]['BANKACCOUNTNO']);
						jQuery("#AccountNumber").val(oDistributorInfo[key]['BANKACCOUNTNO']);
						jQuery("#DirectorGroup").val(oDistributorInfo[key]['DIRECTORGROUP']);
						jQuery("#Star").val(oDistributorInfo[key]['STAR']);
						jQuery("#SaveOn").val(oDistributorInfo[key]['Saveon']);
						jQuery("#DirectorName").val(oDistributorInfo[key]['DIRECTORNAME']);
						//jQuery("#Entitled").val(oDistributorInfo[key]['STAR']);
						jQuery("#RecordSavedBy").val(oDistributorInfo[key]['SAVEDBY']);
						//jQuery("#RecordSavedOn").val(oDistributorInfo[key]['EMAILID']);
						jQuery("#CurrentPrevCumPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PrevCumCurrentPV'])));
						jQuery("#CurrentExclPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['EXCLPV'])));
						jQuery("#CurrentSelfBv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['SELFBV'])));
						jQuery("#CurrentGroupPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['GROUPPV'])));
						jQuery("#CurrentTotalpv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['TOTALPV'])));
						jQuery("#CurrentPercentage").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['BONUSPERCENT'])));
						jQuery("#CurrentTotalCumPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['TotalCumCurrentPV'])));
						jQuery("#CurrentTotalBv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['TOTALBV'])));
						jQuery("#PreviousPrevCumPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVCUMPV'])));
						jQuery("#PreviousExclPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVEXCLPV'])));
						jQuery("#PreviousSelfBv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVSELFBV'])));
						jQuery("#PreviousGroupPv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVGROUPPV'])));
						jQuery("#PreviousTotalpv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVTOTALPV'])));
						jQuery("#PerviousPercentage").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVBONUSPERCENT'])));
						//jQuery("#CurrentTotalCumPv").val(oDistributorInfo[key]['TotalCumCurrentPV']);
						jQuery("#PreviousTotalBv").val(formatFloatNumbers(parseFloat(oDistributorInfo[key]['PREVTOTALBV'])));
						var stateCode = oDistributorInfo[key]['STATECODE'];
						var state = oDistributorInfo[key]['STATE'];
						var countryCode = oDistributorInfo[key]['COUNTRYCODE'];
						var cityCode = oDistributorInfo[key]['CITYCODE'];
						var city = oDistributorInfo[key]['CITY'];
						getCountry(countryCode);
						getState(countryCode,stateCode,"country");
						getCity(stateCode,cityCode,"state");
						
						jQuery(".distributorProofsLinks").click(function(e){
							
							e.preventDefault();
							
							var currentLinkId = this.id;
							
							var currentTitle = this.title;
							
							var currentLinkAddress = jQuery("#"+currentLinkId).attr("href");
							
							jQuery( "#DistributorPanBankInfoPopUp" ).dialog( "open" );
							
							jQuery("#DistributorPanBankInfoPopUp").dialog('option', 'title', currentTitle);
							
							jQuery("#DistributorPanBankInfoPopUp").html("<img src='"+currentLinkAddress+"'>");
							
							
							
							//jQuery("#panClick").attr("target","iframeId");
							// $("#tos-description").dialog("open").load("/tos/");
							
							
						});
						
						//jQuery( "#DistributorPanBankInfoPopUp" ).dialog( "open" );
						
					});
				}
				else
				{
					showMessage("red", distributorData.Description, "");
				}
				
				
			},// end else.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}
	
	
	
	
	/*------------------+++++++++++++++++++++++++===---------------------------------------------*/
	
	/*-----------------------------------------------------------------------------------------------------------------------------------*/
	function getCountry(countryId)
	{
		//alert(countryId);
		
			jQuery("#Country").empty();
			jQuery("#Country").append("<option value='0'>select</option>");
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:1,
					
				},
				success:function(data)
				{
					var countryData = jQuery.parseJSON(data);
					
					if(countryData.Status == 1)
					{
						dynamic_button_json_object = countryData.Result;
						
						var json_object_country = dynamic_button_json_object;
					
						jQuery.each(json_object_country,function(key,value){
							
							var country_id = json_object_country[key]['CountryId'];
							var country_name = json_object_country[key]['CountryName'];
							jQuery("#Country").append("<option value=\""+country_id+"\""+">"+country_name+"</option>");
							
						});
						
						
						jQuery("#Country").val(countryId);
					}
					else
					{
						showMessage("red", countryData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
			
			
		
		/*else
		{
			jQuery.ajax({
				url:Drupal.settings.basePath + 'POSClient',
				data:
				{
					id:"countryFromCountryId",
					countryId: countryId,
				},
				success:function(data)
				{
					//alert(data);
					jQuery("#Country").empty();
					var countryFromId = jQuery.parseJSON(data);
					jQuery("#Country").append("<option value=\""+countryFromId[0]['CountryId']+"\""+">"+countryFromId[0]['CountryName']+"</option>");
					
				}
				
			});
		}*/
	
	
	/*-----------------+++++++++++++++++++++_______________________-----------------------------*/
	
	/*function getCountry(countryCode)
	{
		jQuery("#Country").empty();
		
		jQuery.ajax({
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			data:
			{
				id:"getCountry",
				countryCode:countryCode,
			},
			success:function(data)
			{
				//alert(data);
				var country = jQuery.parseJSON(data);
				jQuery.each(country,function(key,value)
						{	

					jQuery("#Country").append("<option id=\""+countryCode+"\""+" value=\""+countryCode+"\""+">"+country[key]['CountryName']+"</option>");
						});

				
			}

		});
	}*/
	}	
	
	function getState(countryCode,stateCode,correspondingId)
	{
		
		//jQuery("#State").empty();
		//jQuery("#State").append("<option id=\""+stateCode+"\""+" value=\""+stateCode+"\""+">"+state+"</option>");
		
		/***********************************_____________________________________)))))))))))))))))))))))))))))))))))))*/
		
		if(correspondingId == "country")
		{
			
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:5,
					country_id:countryCode,
	
				},
				success:function(data)
				{
					var statesData = jQuery.parseJSON(data);
					
					var sStateData = JSON.stringify(statesData.Result);
					
					if(statesData.Status == 1)
					{
						if (sStateData.indexOf("null") >= 0)
						{
							showMessage("","Not any state registered yet","");
		
						}
						else
						{
							jQuery("#State").empty();
							
							jQuery("#State").append("<option name='state' value='0'>Select</option>");
						
							dynamic_button_json_object = statesData.Result;
							
							var json_object_state = dynamic_button_json_object;
							
							jQuery.each(json_object_state,function(key,value)
							{	
								
							
								jQuery("#State").append("<option value=\""+json_object_state[key]['StateId']+"\""+">"+json_object_state[key]['StateName']+"</option>");
							});
							
							if(stateCode == '')
							{
								
							}
							else
							{
								jQuery("#State").val(stateCode);
							}
							
						}
					}
					else
					{
						showMessage("red", statesData.Description, "");
					}
				
				},//end success
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
		else
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSClient',
				data:
				{
					id:"stateFromStateId",
					stateId: stateCode,
				},
				success:function(data)
				{
					stateFromStateIdData = jQuery.parseJSON(data);
					
					if(stateFromStateIdData.Status == 1)
					{
						jQuery("#getAddressState").empty();
						var stateFromId = stateFromStateIdData.Result;
						
						jQuery("#getAddressState").append("<option value=\""+stateFromId[0]['StateId']+"\""+">"+stateFromId[0]['StateName']+"</option>");
					}
					else
					{
						showMessage("red", stateFromStateIdData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
		
		
		/*--------------------------------------------------------------------------------------------------------------*/
			
	}
	
	
	function getCity(stateCode,cityCode,correspondingId)
	{
		
		//jQuery("#City").empty();
		//jQuery("#City").append("<option id=\""+cityCode+"\""+" value=\""+cityCode+"\""+">"+city+"</option>");
		if(correspondingId == "state")
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSDistributorRegistration',
				data:
				{
					id:8,
					stateid:stateCode,
	
				},
				success:function(data)
				{
					
					var cityData = jQuery.parseJSON(data);
					
					if(cityData.Status == 1)
					{
						jQuery("#City").empty();
						
						jQuery("#City").append("<option name='state' value='0'>Select</option>");
						json_object_city = cityData.Result;
						
						jQuery.each(json_object_city,function(key,value)
						{	
							
							jQuery("#City").append("<option value=\""+json_object_city[key]['CityId']+"\""+">"+json_object_city[key]['CityName']+"</option>");
							
						});
						
						if(cityCode == '')
						{
							
						}
						else
						{
							jQuery("#City").val(cityCode);
						}
						
					}
					else
					{
						showMessage("red", cityData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			
			});
		}
		else
		{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'POSClient',
				data:
				{
					id:"cityFromCityId",
					cityId: id,
				},
				success:function(data)
				{
					
					var cityFromCityIdData = jQuery.parseJSON(data);
					
					if(cityFromCityIdData.Status == 1)
					{
						jQuery("#getAddressCity").empty();
						var stateFromId = jQuery.cityFromCityIdData.Result;
						jQuery("#getAddressCity").append("<option value=\""+stateFromId[0]['CityId']+"\""+">"+stateFromId[0]['CityName']+"</option>");
					}
					else
					{
						showMessage("red", cityFromCityIdData.Description, "");
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
				
			});
		}
	}
	
	
	jQuery("#Country").change(function(){
		jQuery("#City").empty();
		var selectedCountryId = this.value;
		getState(selectedCountryId,'',"country");
	});
	jQuery("#State").change(function(){
		var selectedStateId = this.value;
		getCity(selectedStateId,'',"state");
	});
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Downline Grid 
	 */
	jQuery("#downline_jqgrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Reg.Date','Distributor Id','First Name','Last Name','Excl PV','Self PV','Group PV','PV','PB','Pcent','Cum. PV','Cum. BV','Payment Qualified','Action'],
		colModel: [{ name: 'RegDate', index: 'RegDate', width:100 },
		           { name: 'DistributorId', index: 'DistributorId', width: 150,sorttype:'number'},
		           { name: 'FirstName', index: 'FirstName', width: 150},
		           { name: 'LastName', index: 'LastName', width: 100},
		           { name: 'ExclPV', index: 'ExclPV', width: 70,sorttype:'float'},
		           { name: 'SelfPV', index: 'SelfPV', width: 70,sorttype:'float'},
		           { name: 'GroupPV', index: 'GroupPV', width: 100,sorttype:'float'},
		           { name: 'PV', index: 'PV', width: 70,sorttype:'float'},
		           { name: 'PB', index: 'PB', width: 70,sorttype:'float'},
		           
		           { name: 'PCent', index: 'PCent', width: 50,sorttype:'float'},	

		           { name: 'CumPV', index: 'CumPV', width: 100,sorttype:'float'},
		           { name: 'CumBV', index: 'CumBV', width: 100,sorttype:'float' },
		           { name: 'PaymentQualified', index: 'Label', width: 100,sorttype:'float' },
		           { width: 132, sortable: false, classes: 'action', align: 'center',hidden: true}],
		           pager: jQuery('#pjmap_downline_grid'),
		           width:1040,
		           height:600,
		           rowNum: 10000,
		           rowList: [500],
		           hoverrows: true,
		           ignoreCase: true,
		           ondblClickRow: function (id) {

		        	   var selectedDistributorId = jQuery("#downline_jqgrid").jqGrid('getCell', id, 'DistributorId');
		        	   
		        	   jQuery("#DivDistributorInfoSearchTab").tabs( "select", "DivDistributor" );


		        	   checkValidDistributor(selectedDistributorId,"current");

		        	   //function to call pack unpack report.

		           },


		           afterInsertRow : function(ids)
		           {
		        	   var index1 = jQuery("#downline_jqgrid").jqGrid('getCol','Name',false);

		        	   jQuery(this).jqGrid('setRowData', rowid, false,{'background-color':'white'});

		           }	
		         

	});
		
	jQuery("#downline_jqgrid").jqGrid('navGrid','#pjmap_downline_grid',{edit:false,add:false,del:false});	
	//jQuery("#pjmap_downline_grid").hide();
	
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Invoice Grid
	 */
	jQuery("#todayInvoiceGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['BVM','Invoice No','Date','Inv Consider Date','Qty','BP','BV','Amount','Paid','Change'],
		colModel: [{ name: 'BVM', index: 'BVM', width:100 },
		           { name: 'InvoiceNo', index: 'InvoiceNo', width: 150,sorttype:'float'},
		           { name: 'Date', index: 'Date', width: 150},
		           { name: 'InvConsiderdate', index: 'InvConsiderdate', width: 100},
		           { name: 'Qty', index: 'Qty', width: 70,sorttype:'float'},
		           { name: 'BP', index: 'BP', width: 100,sorttype:'float'},
		           { name: 'BV', index: 'BV', width: 70,sorttype:'float'},
		           { name: 'Amount', index: 'Amount', width: 30,sorttype:'float'},
		           
		           { name: 'Paid', index: 'Paid', width: 50,sorttype:'float'},	

		           { name: 'Change', index: 'Change', width: 100,sorttype:'float'}],
		           pager: jQuery('#todayInvoiceGridPager'),
		           width:1040,
		           height:600,
		           rowNum: 10000,
		           rowList: [500],
		           sortable: true,
		           sortorder: "asc",
		           hoverrows: true,
		           ondblClickRow: function (id) {

		        	   var selectedInvoiceNo = jQuery("#todayInvoiceGrid").jqGrid('getCell', id, 'InvoiceNo');


		        	   showInvoiceReport(selectedInvoiceNo);

		        	   //function to call pack unpack report.

		           },


		           afterInsertRow : function(ids)
		           {
//		        	   alert("ids is" + ids);
		        	   var index1 = jQuery("#todayInvoiceGrid").jqGrid('getCol','Name',false);
//		        	   alert("after insert row" + index1);
		        	   jQuery(this).jqGrid('setRowData', rowid, false,{'background-color':'white'});
//		        	   jQuery("tr.jqgrow").css("background", "#DDDDDC");
		           }	
		           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

	});
		
	jQuery("#todayInvoiceGrid").jqGrid('navGrid','#todayInvoiceGridPager',{edit:false,add:false,del:false});	
	jQuery("#todayInvoiceGridPager").hide();
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Invoice Grid
	 */
	jQuery("#currentMonthInvoiceGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['BVM','Invoice No','Date','Inv Consider Date','Qty','BP','BV','Amount','Paid','Change'],
		colModel: [{ name: 'BVM', index: 'RegDate', width:100},
		           { name: 'InvoiceNo', index: 'DistributorId', width: 150},
		           { name: 'Date', index: 'FirstName', width: 100},
		           { name: 'InvConsiderdate', index: 'DiscountPercent', width: 100},
		           { name: 'Qty', index: 'PromotionId', width: 70,sorttype:'float'},
		           { name: 'BP', index: 'PromoDescription', width: 100,sorttype:'float'},
		           { name: 'BV', index: 'promotionParticipation', width: 70,sorttype:'float'},
		           { name: 'Amount', index: 'merchHierarchyDetailId', width: 70,sorttype:'float'},
		           
		           { name: 'Paid', index: 'uid', width: 50,sorttype:'float'},	

		           { name: 'Change', index: 'Label', width: 100 }],
		           pager: jQuery('#currentMonthInvoiceGridPager'),
		           width:1040,
		           height:600,
		           rowNum: 10000,
		           rowList: [500],
		           sortable: true,
		           sortorder: "asc",
		           hoverrows: true,
		           ondblClickRow: function (id) {
		        	   
               		var selectedInvoiceNo = jQuery("#currentMonthInvoiceGrid").jqGrid('getCell', id, 'InvoiceNo');
               		
               		
               		showInvoiceReport(selectedInvoiceNo);
               		
               		//function to call pack unpack report.
               		
                  },


		           afterInsertRow : function(ids)
		           {
//		        	   alert("ids is" + ids);
		        	   var index1 = jQuery("#currentMonthInvoiceGrid").jqGrid('getCol','Name',false);
//		        	   alert("after insert row" + index1);
		        	   jQuery(this).jqGrid('setRowData', rowid, false,{'background-color':'white'});
//		        	   jQuery("tr.jqgrow").css("background", "#DDDDDC");
		           }	
		           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

	});
		
	jQuery("#currentMonthInvoiceGrid").jqGrid('navGrid','#currentMonthInvoiceGridPager',{edit:false,add:false,del:false});	
	jQuery("#currentMonthInvoiceGridPager").hide();
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * All Invoice Grid
	 */
	jQuery("#allInvoiceGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['BVM','Invoice No','Date','Inv Consider Date','Qty','BP','BV','Amount','Paid','Change'],
		colModel: [{ name: 'BVM', index: 'BVM', width:100 },
		           { name: 'InvoiceNo', index: 'InvoiceNo', width: 130},
		           { name: 'Date', index: 'Date', width: 100},
		           { name: 'InvConsiderdate', index: 'InvConsiderdate', width: 100},
		           { name: 'Qty', index: 'Qty', width: 70,sorttype:'float'},
		           { name: 'BP', index: 'BP', width: 100,sorttype:'float'},
		           { name: 'BV', index: 'BV', width: 70,sorttype:'float'},
		           { name: 'Amount', index: 'Amount', width: 70,sorttype:'float'},
		           
		           { name: 'Paid', index: 'Paid', width: 50, editable:true},	

		           { name: 'Change', index: 'Change', width: 100 }],
		           pager: jQuery('#allInvoiceGridPager'),
		           width:1040,
		           height:600,
		           rowNum: 10000,
		           rowList: [500],
		           sortable: true,
		           sortorder: "asc",
		           hoverrows: true,

		           ondblClickRow: function (id) {

		        	   var selectedInvoiceNo = jQuery("#allInvoiceGrid").jqGrid('getCell', id, 'InvoiceNo');


		        	   showInvoiceReport(selectedInvoiceNo);

		        	   //function to call pack unpack report.

		           },

		           afterInsertRow : function(ids)
		           {
//		        	   alert("ids is" + ids);
		        	   var index1 = jQuery("#allInvoiceGrid").jqGrid('getCol','Name',false);
//		        	   alert("after insert row" + index1);
		        	   jQuery(this).jqGrid('setRowData', rowid, false,{'background-color':'white'});
//		        	   jQuery("tr.jqgrow").css("background", "#DDDDDC");
		           }	
		           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

	});
		
	jQuery("#allInvoiceGrid").jqGrid('navGrid','#allInvoiceGridPager',{edit:false,add:false,del:false});	
	jQuery("#allInvoiceGridPager").hide();
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Bonus Grid 
	 */
	jQuery("#bonusGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['BVM','EBP','SBP','GBP','BP','SBV','GBV','BV','PCENT','GB','PB','LB','DB'
		           ,'CCN','TF','CF','HF','SB','PYBL','TDSRATE','TDS','BALCF','HOLD','PAID','NETPAY','ONACDED','ADVDED','ACTPAY'
		           ,'BNK','DONE','CQNO','CHQDATE','TTL','FN','SN','SELF','NOFA','QUAL'],
		           
		colModel: [{ name: 'BVM', index: 'BVM', width:70},
		           { name: 'EBP', index: 'EBP', width: 70,sorttype:'float'},
		           { name: 'SBP', index: 'SBP', width: 70,sorttype:'float'},
		           { name: 'GBP', index: 'GBP', width: 80,sorttype:'float'},
		           { name: 'BP', index: 'BP', width: 80,sorttype:'float'},
		           { name: 'SBV', index: 'SBV', width: 100,sorttype:'float'},
		           { name: 'GBV', index: 'GBV', width: 100,sorttype:'float'},
		           { name: 'BV', index: 'BV', width: 70,sorttype:'float'},
		           { name: 'PCENT', index: 'PCENT', width: 40,sorttype:'float'},
		           { name: 'GB', index: 'GB', width: 80,sorttype:'float'},
		           { name: 'PB', index: 'PB', width: 50,sorttype:'float'},
		           { name: 'LB', index: 'LB', width: 80,sorttype:'float'},	
		           { name: 'DB', index: 'DB', width: 90,sorttype:'float'},
		           { name: 'CCN', index: 'CCN', width: 70,sorttype:'float'},
		           { name: 'TF', index: 'TF', width: 80,sorttype:'float'},
		           { name: 'CF', index: 'CF', width: 50,sorttype:'float'},
		           { name: 'HF', index: 'HF', width: 50,sorttype:'float'},
		           { name: 'SB', index: 'SB', width: 50,sorttype:'float'},
		           
		           { name: 'PYBL', index: 'PYBL', width: 50,sorttype:'float'},
		           { name: 'TDSRATE', index: 'TDSRATE', width: 50,sorttype:'float'},
		           
		           { name: 'TDS', index: 'TDSRATE', width: 50,sorttype:'float'},
		           { name: 'BALCF', index: 'BALCF', width: 50,sorttype:'float'},
		           { name: 'HOLD', index: 'Hold', width: 50,sorttype:'float'},
		           { name: 'PAID', index: 'PAID', width: 50,sorttype:'float'},
		           { name: 'NETPAY', index: 'NETPAY', width: 50,sorttype:'float'},
		           { name: 'ONACDED', index: 'ONACDED', width: 50,sorttype:'float'},
		           { name: 'ADVDED', index: 'ADVDED', width: 50,sorttype:'float'},
		           { name: 'ACTPAY', index: 'ACTPAY', width: 50,sorttype:'float'},
		           { name: 'BNK', index: 'BNK', width: 50,sorttype:'float'},
		           { name: 'DONE', index: 'DONE', width: 50},
		           { name: 'CQNO', index: 'CQNO', width: 50,sorttype:'float'},
		           { name: 'CHQDATE', index: 'CHQDATE', width: 80,sorttype:'float'},
		           { name: 'TTL', index: 'TTL', width: 50},
		           { name: 'FN', index: 'FN', width: 70,sorttype:'float'},
		           { name: 'SN', index: 'SN', width: 70,sorttype:'float'},
		           { name: 'SELF', index: 'SELF', width: 50,sorttype:'float'},
		           { name: 'NOFA', index: 'NOFA', width: 50,sorttype:'float'},
		           
		           { name: 'QUAL', index: 'QUAL', width: 50}],
		           pager: jQuery('#bonusGridPager'),
		           width:1040,
		           height:600,
		           rowNum: 10000,
		           rowList: [500],
		           sortable: true,
		           sortorder: "asc",
		           hoverrows: true,
		           shrinkToFit: false,


		           afterInsertRow : function(ids)
		           {
//		        	   alert("ids is" + ids);
		        	   var index1 = jQuery("#bonusGrid").jqGrid('getCol','Name',false);
//		        	   alert("after insert row" + index1);
		        	   jQuery(this).jqGrid('setRowData', rowid, false,{'background-color':'white'});
//		        	   jQuery("tr.jqgrow").css("background", "#DDDDDC");
		           }	
		           //beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

	});
		
	jQuery("#bonusGrid").jqGrid('navGrid','#bonusGridPager',{edit:false,add:false,del:false});	
	//jQuery("#bonusGridPager").hide();
	
	
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Success grid. 
	 */
	jQuery("#distributorRankOrgGrid").jqGrid({

		datatype: 'jsonstring',
		colNames: ['Title','Obtained On'],

		           colModel: [{ name: 'Title', index: 'Title', width:100 },
		                      { name: 'ObtainedOn', index: 'ObtainedOn', width: 150 }],
		                      pager: jQuery('#distributorRankOrgGridPager'),
		                      width:520,
		                      height:475,
		                      rowNum: 10000,
		                      rowList: [500],
		                      sortable: true,
		                      sortorder: "asc",
		                      hoverrows: true
	//beforeSelectRow: function (rowid, e) { return false; } //this disables row being highlighted when clicked

	});

	jQuery("#distributorRankOrgGrid").jqGrid('navGrid','#distributorRankOrgGridPager',{edit:false,add:false,del:false});	
	jQuery("#distributorRankOrgGridPager").hide();
	
	
	/*--------------------------Tab Selection -------------------------*/
	

	jQuery("#DivDistributorInfoSearchTab").bind("tabsselect", function(e, tab) { //selecting tab fire evert and give index of selected tab.
		
		var tabIndex = tab.index; 
		if(tabIndex == 1)
		{
			var distributorId = jQuery("#S_DISDistributorId").val();
			
			if(distributorId == '')
			{
				showMessage("", "Select one Distributor", "");
			}
			else
			{
				
				var downlineGridData = jQuery("#downline_jqgrid").jqGrid('getCol','FirstName',false);
				
				var downlineGridDataLength = downlineGridData.length;
				
				if(downlineGridDataLength > 0)
				{
					
				}
				else
				{
					showMessage("loading","Loading Downlines...", "");
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
						data:
						{
							id:"downlines",
							distributorId:distributor_id,
							searchCondition:"current",
							index:tabIndex,  //index is 1 here.
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							//alert(data);
							var downlineData = jQuery.parseJSON(data);
							
							if(downlineData.Status == 1)
							{
								var downlines = downlineData.Result;
								
								if(downlines.length > 0)
								{
									jQuery.each(downlines,function(key,value){
										rowid = rowid + 1;
										var downlineData = [{"RegDate":downlines[key]['BUSINESSMONTH'],"DistributorId":downlines[key]['DISTRIBUTORID'],"FirstName":downlines[key]['DISTRIBUTORFIRSTNAME'],"LastName":downlines[key]['DISTRIBUTORLASTNAME'],'ExclPV':formatFloatNumbers(parseFloat(downlines[key]['EXCLPV'])),'SelfPV':formatFloatNumbers(parseFloat(downlines[key]['SELFPV'])),"GroupPV":formatFloatNumbers(parseFloat(downlines[key]['GROUPPV'])),"PV":formatFloatNumbers(parseFloat(downlines[key]['TOTALPV'])),"PB":formatFloatNumbers(parseFloat(downlines[key]['PERFORMANCEBONUS'])),"PCent":formatFloatNumbers(parseFloat(downlines[key]['BONUSPERCENT'])), "CumPV":formatFloatNumbers(parseFloat(downlines[key]['TOTALCUMPV'])),"CumBV":formatFloatNumbers(parseFloat(downlines[key]['TOTALCUMBV'])),"PaymentQualified":formatFloatNumbers(parseFloat(downlines[key]['BUSINESSMONTH']))}]
										for (var i=0;i<downlineData.length;i++) {
										jQuery("#downline_jqgrid").jqGrid('addRowData', rowid, downlineData[downlineData.length-i-1], "first");

										}
									});
								}
								
								else
								{
									showMessage("","No Downlines","");
								}
							}
							else
							{
								showMessage("red", downlineData.Description, "");
							}
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
				}
				
				
				
			}
			
			
		}
		
		
		
		
		
		/*------------------------------For third index that is of bonus----------------------------*/
		
		if(tabIndex == 2)
		{
			
			var distributorId = jQuery("#S_DISDistributorId").val();
			var isMiniBranch = jQuery("#isMiniBranch").val();
			if(distributorId == '')
			{
				showMessage("", "Select one Distributor", "");
			}
			else if(isMiniBranch == 1){
				showMessage("", "DLCP is not authorize to view Bonus", "");
			}
			else
			{
				var bonusGridData = jQuery("#bonusGrid").jqGrid('getCol','BVM',false);
				
				var bonusGridDataLength = bonusGridData.length;
				
				if(bonusGridDataLength > 0)
				{
					
				}
				else
				{
					showMessage("loading", "Loading Bonus data...", "");
					
					var bonusRowid = 0;
					
					var distributorFirstName = jQuery("#S_DISFirstName").val();
					var distributorLastName = jQuery("#S_DISLastName").val();
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
						data:
						{
							id:"bonus",
							distributorId:distributor_id,
							distributorFirstName:distributorFirstName,
							distributorLastName:distributorLastName,
							searchCondition:"current",
							
							index:tabIndex,  //index is 2 here for bonus.
						},
						success:function(data)
						{
							var bonusResult = jQuery.parseJSON(data);
							
							jQuery(".ajaxLoading").hide();
							
							if(bonusResult.Status == 1)
							{
								distributorBonusInfo = bonusResult.Result;
								
								
								if(distributorBonusInfo.length == 0)
								{
									showMessage("", "Bonus data not found", "");
								}
								else
								{
									jQuery.each(distributorBonusInfo,function(key,value){
										bonusRowid = bonusRowid + 1;
										var bonusData = [{"BVM":distributorBonusInfo[key]['bvm'],"EBP":parseFloat(distributorBonusInfo[key]['ebp'])
														,"SBP":parseFloat(distributorBonusInfo[key]['sbp']),"GBP":parseFloat(distributorBonusInfo[key]['gbp'])
														,'BP':parseFloat(distributorBonusInfo[key]['bp']),'SBV':distributorBonusInfo[key]['sbv No'],"GBV":distributorBonusInfo[key]['gbv']
														,"BV":parseFloat(distributorBonusInfo[key]['bv']),"PCENT":distributorBonusInfo[key]['pcent']
														,"GB":distributorBonusInfo[key]['gb']
														,"PB":distributorBonusInfo[key]['pb'],"LOB":distributorBonusInfo[key]['LOB'],"Payable":parseFloat(distributorBonusInfo[key]['Payable'])
														,"LB":parseFloat(distributorBonusInfo[key]['lb']),"DB":parseFloat(distributorBonusInfo[key]['db']),"CCM":parseFloat(distributorBonusInfo[key]['ccm'])
														,"TF":parseFloat(distributorBonusInfo[key]['tf']),"CF":distributorBonusInfo[key]['cf'],"HF":distributorBonusInfo[key]['hf']
														,"SB":parseFloat(distributorBonusInfo[key]['sb']),"PYBL":distributorBonusInfo[key]['pybl'],"TDSRATE":distributorBonusInfo[key]['tdsrate']
														,"TDS":parseFloat(distributorBonusInfo[key]['tds']),"BALCF":distributorBonusInfo[key]['balcf'],"hf":distributorBonusInfo[key]['hf']
														,"TF":parseFloat(distributorBonusInfo[key]['tf']),"CF":distributorBonusInfo[key]['cf'],"PAID":distributorBonusInfo[key]['paid']
														,"NETPAY":parseFloat(distributorBonusInfo[key]['netpay']),"ONACDED":distributorBonusInfo[key]['onacded'],"TFDED":distributorBonusInfo[key]['tfded']
														,"ADVDED":parseFloat(distributorBonusInfo[key]['advded']),"ACTPAY":distributorBonusInfo[key]['ACTPAY'],"BNK":distributorBonusInfo[key]['bnk']
														,"DONE":distributorBonusInfo[key]['done'],"CQNO":distributorBonusInfo[key]['cqno'],"CHQDATE":distributorBonusInfo[key]['chqdate']
														,"TTL":distributorBonusInfo[key]['ttl'],"FN":distributorBonusInfo[key]['fn'],"SN":distributorBonusInfo[key]['sn']
														
														,"SELF":parseFloat(distributorBonusInfo[key]['self']),"NOFA":distributorBonusInfo[key]['nofa'],"QUAL":distributorBonusInfo[key]['qual']
														}];
										for (var i=0;i<bonusData.length;i++) {
											jQuery("#bonusGrid").jqGrid('addRowData', bonusRowid, bonusData[bonusData.length-i-1], "first");

										}
									});
								}
							}
							else
							{
								showMessage("red",bonusResult.Description, "");
							}
							
							
							
							
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
				}
			}
			
			
		}
		
		
		/*------------------------------For fourth index( Today's Invoice)----------------------------*/
		
		if(tabIndex == 3)
		{
			
			var distributorId = jQuery("#S_DISDistributorId").val();
			var isMiniBranch = jQuery("#isMiniBranch").val();
			if(distributorId == '')
			{
				showMessage("", "Select one Distributor", "");
			}
			else if(isMiniBranch == 1){
				showMessage("", "DLCP is not authorize to view Today's Invoices", "");
			}
			else
			{
				var todayInvoiceGridData = jQuery("#todayInvoiceGrid").jqGrid('getCol','InvoiceNo',false);
				
				var bonusGridDataLength = todayInvoiceGridData.length;
				
				if(bonusGridDataLength > 0)
				{
					
				}
				else
				{

					showMessage("loading", "Loading today's Invoice... ", "");
					
					var invoiceId = 0;
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
						data:
						{
							id:"invoiceSearch",
							distributorId:distributor_id,
							
							sConditionParam:"Day",  
						},
						success:function(data)
						{
							//alert(data);
							var distributorInvoiceData = jQuery.parseJSON(data);
							
							if(distributorInvoiceData.Status == 1)
							{
								var distributorAllInvoice = distributorInvoiceData.Result;
								
								jQuery(".ajaxLoading").hide();
								
								if(distributorAllInvoice.length == 0)
								{
									showMessage("", "Today Invoice not found", "");
								}
								else
								{
								
									jQuery.each(distributorAllInvoice,function(key,value){
										invoiceId = invoiceId + 1;
										var allInvoiceData = [{"BVM":distributorAllInvoice[key]['BUSINESSMONTH'],"InvoiceNo":distributorAllInvoice[key]['INVOICENO']
														,"Date":distributorAllInvoice[key]['INVOICEDATE'],"InvConsiderdate":distributorAllInvoice[key]['INVOICECONSIDERDATE']
														,'Qty':distributorAllInvoice[key]['QUANTITY'],'BP':distributorAllInvoice[key]['TOTALPV'],"PB":distributorAllInvoice[key]['TOTALBV']
														,"BV":distributorAllInvoice[key]['TOTALBV'],"Amount":distributorAllInvoice[key]['INVOICEAMOUNT']
														,"Paid":distributorAllInvoice[key]['PAIDAMOUNT']
														,"Change":distributorAllInvoice[key]['CHANGEAMOUNT']}];
										for(var i=0;i<allInvoiceData.length;i++) {
											jQuery("#todayInvoiceGrid").jqGrid('addRowData', invoiceId, allInvoiceData[allInvoiceData.length-i-1], "first");

										}
									});
								}
							}
							else
							{
								showMessage("red", distributorInvoiceData.Description, "");
							}
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
				}
			}
			
			
			
			
		}
		
		
		/*------------------------------For fifth index( Current Month Invoice)----------------------------*/
		
		if(tabIndex == 4)
		{
			var distributorId = jQuery("#S_DISDistributorId").val();
			var isMiniBranch = jQuery("#isMiniBranch").val();
			if(distributorId == '')
			{
				showMessage("", "Select one Distributor", "");
			}
			else if(isMiniBranch == 1){
				showMessage("", "DLCP is not authorize to view Current Month Invoices", "");
			}
			else
			{
				var currentMonthInvoiceGridData = jQuery("#currentMonthInvoiceGrid").jqGrid('getCol','InvoiceNo',false);
				
				var currentMonthInvoiceGridDataLength = currentMonthInvoiceGridData.length;
				
				if(currentMonthInvoiceGridDataLength > 0)
				{
					
				}
				else
				{
					showMessage("loading", "Loading current month Invoice...", "");
					
					var invoiceId = 0;
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
						data:
						{
							id:"invoiceSearch",
							distributorId:distributor_id,
							
							sConditionParam:"Month",  
						},
						success:function(data)
						{
							//alert(data);
							
							var currentMonthInvoicesData = jQuery.parseJSON(data);
							
							if(currentMonthInvoicesData.Status == 1)
							{
								var distributorAllInvoice = currentMonthInvoicesData.Result;
								
								jQuery(".ajaxLoading").hide();
								
								if(distributorAllInvoice.length == 0)
								{
									showMessage("", "Current Month invoice not found", "");
								}
								else
								{
								
									jQuery.each(distributorAllInvoice,function(key,value){
										invoiceId = invoiceId + 1;
										var allInvoiceData = [{"BVM":distributorAllInvoice[key]['BUSINESSMONTH'],"InvoiceNo":distributorAllInvoice[key]['INVOICENO']
															,"Date":distributorAllInvoice[key]['INVOICEDATE'],"InvConsiderdate":distributorAllInvoice[key]['INVOICECONSIDERDATE']
															,'Qty':distributorAllInvoice[key]['QUANTITY'],'BP':distributorAllInvoice[key]['TOTALPV'],"PB":distributorAllInvoice[key]['TOTALBV']
															,"BV":distributorAllInvoice[key]['TOTALBV'],"Amount":distributorAllInvoice[key]['INVOICEAMOUNT']
															,"Paid":distributorAllInvoice[key]['PAIDAMOUNT']
															,"Change":distributorAllInvoice[key]['CHANGEAMOUNT']}];
										for(var i=0;i<allInvoiceData.length;i++) {
											jQuery("#currentMonthInvoiceGrid").jqGrid('addRowData', invoiceId, allInvoiceData[allInvoiceData.length-i-1], "first");

										}
									});
								}
							}
							else
							{
								showMessage("red", distributorAllInvoice.Description, "");
							}
								
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
				}
			}
				
			
		}
		
		
		/*------------------------------For sixth index(All Invoice)----------------------------*/
		
		if(tabIndex == 5)
		{
			var distributorId = jQuery("#S_DISDistributorId").val();
			var isMiniBranch = jQuery("#isMiniBranch").val();
			if(distributorId == '')
			{
				showMessage("", "Select one Distributor", "");
			}
			else if(isMiniBranch == 1){
				showMessage("", "DLCP is not authorize to view All Invoices", "");
			}
			else
			{
				var currentMonthInvoiceGridData = jQuery("#allInvoiceGrid").jqGrid('getCol','InvoiceNo',false);
				
				var currentMonthInvoiceGridDataLength = currentMonthInvoiceGridData.length;
				
				if(currentMonthInvoiceGridDataLength > 0)
				{
					
				}
				else
				{
					
					showMessage("loading", "Loading all Invoice...", "");
					
					var invoiceId = 0;
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
						data:
						{
							id:"invoiceSearch",
							distributorId:distributor_id,
							
							sConditionParam:"All",  
						},
						success:function(data)
						{
							
							var distributorAllInvoiceData = jQuery.parseJSON(data);
							//alert(data);
							if(distributorAllInvoiceData.Status == 1)
							{
								var distributorAllInvoice = distributorAllInvoiceData.Result;
								
								jQuery(".ajaxLoading").hide();
								
								if(distributorAllInvoice.length == 0)
								{
									//showMessage(messageCode, defaultMessage, messageParam)
									
									
									showMessage("", "Information Unavailable", "");
									//alert("No");
								}
								else
								{
									
									//alert(data);
									jQuery.each(distributorAllInvoice,function(key,value){
										invoiceId = invoiceId + 1;
										var allInvoiceData = [{"BVM":distributorAllInvoice[key]['BUSINESSMONTH'],"InvoiceNo":distributorAllInvoice[key]['INVOICENO']
														,"Date":distributorAllInvoice[key]['INVOICEDATE'],"InvConsiderdate":distributorAllInvoice[key]['INVOICECONSIDERDATE']
														,'Qty':formatFloatNumbers(parseFloat(distributorAllInvoice[key]['QUANTITY']))
														,'BP':formatFloatNumbers(parseFloat(distributorAllInvoice[key]['TOTALPV']))
														,"PB":formatFloatNumbers(parseFloat(distributorAllInvoice[key]['TOTALBV']))
														,"BV":formatFloatNumbers(parseFloat(distributorAllInvoice[key]['TOTALBV'])),"Amount":formatFloatNumbers(parseFloat(distributorAllInvoice[key]['INVOICEAMOUNT']))
														,"Paid":formatFloatNumbers(parseFloat(distributorAllInvoice[key]['PAIDAMOUNT']))
														,"Change":formatFloatNumbers(parseFloat(distributorAllInvoice[key]['CHANGEAMOUNT']))}];
										for(var i=0;i<allInvoiceData.length;i++) {
											jQuery("#allInvoiceGrid").jqGrid('addRowData', invoiceId, allInvoiceData[allInvoiceData.length-i-1], "first");

										}
									});
								}
							}
							else
							{
								showMessage("red", distributorAllInvoiceData.Description, "");
							}
								
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
					
				}
			}
				
		}

		/*------------------------------For sixth index(All Invoice)----------------------------*/

		if(tabIndex == 6)
		{
			
			var distributorId = jQuery("#S_DISDistributorId").val();
			var isMiniBranch = jQuery("#isMiniBranch").val();
			if(distributorId == '')
			{
				showMessage("", "Select one Distributor", "");
			}
			else if(isMiniBranch == 1){
				showMessage("", "DLCP is not authorize to view Distributors Levels", "");
			}
			else
			{
				var successGridData = jQuery("#distributorRankOrgGrid").jqGrid('getCol','Title',false);
				
				var successGridDataLength = successGridData.length;
				
				if(successGridDataLength > 0)
				{
					
				}
				else
				{
					showMessage("loading", "Loading distributor success...", "");
					
					var bonusRowid = 0;
					
					var distributorFirstName = jQuery("#S_DISFirstName").val();
					var distributorLastName = jQuery("#S_DISLastName").val();
					
					jQuery.ajax({
						type:'POST',
						url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
						data:
						{
							id:"distributorSuccess",
							distributorId:distributor_id,
							distributorFirstName:distributorFirstName,
							distributorLastName:distributorLastName,
							searchCondition:"current",
							
							index:tabIndex,  //index is 6 here for distributor success.
						},
						success:function(data)
						{
							jQuery(".ajaxLoading").hide();
							
							var successRowid = 0;
							
							var distributorSuccessInfo = jQuery.parseJSON(data);
							
							if(distributorSuccessInfo.Status == 1)
							{
								
								jQuery("#distributorFirstName").val(distributorFirstName);
								
								jQuery("#distributorLastName").val(distributorLastName);
								
								var distributorSuccessData_Organization = distributorSuccessInfo.Result[1]['RankMonthHeirarchy'];
								
								var distributorSuccessData_Month = distributorSuccessInfo.Result[0]['RankOrganizationHeirarchy'];
								
								
								
								if(distributorSuccessData_Organization.length > 0)
								{
									jQuery.each(distributorSuccessData_Organization,function(key,value){
										successRowid = successRowid + 1;
										var successData = [{"Title":distributorSuccessData_Organization[key]['LEVELNAME'],"ObtainedOn":distributorSuccessData_Organization[key]['ACHIEVEDDATE']}];
										for (i=0;i<successData.length;i++) {
											jQuery("#distributorRankOrgGrid").jqGrid('addRowData', successRowid, successData[successData.length-i-1], "first");

										}
									});
									
									for(var i=1;i<=12;i++)
									{
										jQuery.each(distributorSuccessData_Month,function(key,value){
											
											if(distributorSuccessData_Month[key]['MONTHDIFF'] == i)
											{
												var dynamicDistributorLevel = "Dist"+i+"MonthLevelName";
												
												var dynamicDistributorPercent = "dist"+i+"MonthPecent";
												
												var dynamicDistributorLevelId = "dist"+i+"MonthLevel";
												
												jQuery("#"+dynamicDistributorLevel).val(distributorSuccessData_Month[key]['LEVELNAME']);
												
												jQuery("#"+dynamicDistributorPercent).val(distributorSuccessData_Month[key]['BONUSPERCENT']);
												
												jQuery("#"+dynamicDistributorLevelId).val(distributorSuccessData_Month[key]['LEVELID']);
											}
											
											//return false;
											
										});
									}
								}
								else
								{
									// nothing.
								}
								
								
							}
							else
							{
								showMessage("red", distributorSuccessInfo.Description, "");
							}
							
							
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 

							jQuery(".ajaxLoading").hide();
							var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
							showMessage("", defaultMessage, "");
						}
					});
				}
			}
		
		}
		
		
	});
	
	jQuery("#distResignation").click(function(){
		
		var distributorId = jQuery("#S_DISDistributorId").val();
		
		if(distributorId == '')
		{
			showMessage("", "Enter distributor id to resign", "");
		}
		else
		{
			var confirmResignation = confirm("Do you really want to resign distributor ? ");
			
			if(confirmResignation == true)
			{
				distributorResignation(distributorId);
			}
			else
			{
				//No action selected.
			}
			
		}
		
	});
	
	function distributorResignation(distributorId)
	{
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
			data:
			{
				id:"resignDistributor",
				moduleCode:moduleCode,
				functionName:"DistributorResignation",
				distributorId:distributorId,
			},
			success:function(data)
			{
				var results = jQuery.parseJSON(data);
				
				if(results.Status == 1)
				{
					var result = results.Result;
					
					if(result[0]['ResignationStatus'] == 3)
					{
						showMessage("green", "Distributor resigned successfully", "");
					}
					else
					{
						showMessage("red", "Unable to resign distributor", "");
					}
				}
				else
				{
					showMessage("red",results.Description,"");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}
	
	/*-----------------Enter RTGS/IFSC code ----------------------------*/
	
	jQuery("#BankDetail").keydown(function(event){

		if(event.keyCode == 13 || event.keyCode == 9)
		{
			
			var distributorRTGScode = jQuery("#BankDetail").val();  
			
				showMessage("loading", "Validating RTGS code...", "");
				
				jQuery.ajax({
					type:'POST',
					url:Drupal.settings.basePath + 'POSDistributorRegistration',
					data:
					{
						id:"getBankAndAccountInfoFromRTGS",
						RTGSCode:distributorRTGScode,
					},
					success:function(data)
					{
						jQuery(".ajaxLoading").hide();
						
						var results = jQuery.parseJSON(data);
						if(results.Status == 1)
						{
							var result = results.Result;
							if(result.length > 0)
							{
								jQuery("#Bank").val(result[0]['BankId']);
								
								jQuery("#bankAccountLength").val(result[0]['FixedLength']);
								
								DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 1;
							}
							else
							{
								DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
								
								showMessage("", "Invalid RTGS Code", "");
							}
						}
						else
						{
							DISTRIBUTOR_IFSC_RTGS_CODE_VALIDATION = 0;
							
							showMessage("red", results.Description, "");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 

						jQuery(".ajaxLoading").hide();
						var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
						showMessage("", defaultMessage, "");
					}

				});
			
			
		}
		
		if(event.which == 115) 
		{
			var currentRTGSCodeId = this.id;
			var currentRTGSCodeValue = this.value;

			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'LookUpCallback',
				data:
				{
					id:"distributorRTGSCodeLookUp",
				},
				success:function(data)
				{

					//alert(data);
					jQuery("#divLookUp").html(data);

					distributorRTGSlookUpGrid(currentRTGSCodeId,currentRTGSCodeValue);
					jQuery("#divLookUp" ).dialog( "open" );

					var distributorBankId = distributorRTGSLookUpData(currentRTGSCodeValue);

					/*----------Dialog for item search is there ----------------*/


				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}

			});
		}
	});	
	
	
	
	
	
	
	/*--------------------------------------------------------------------------------------------------------------- */
	function validateBankAndAccount(bankIFSCCode,accountNumber)
	{
		
		jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'POSDistributorRegistration',
			data:
			{
				id:"validateBankIFSCAndAccount",
				bankIFSCCode:bankIFSCCode,
				accountNumber:accountNumber,

			},
			success:function(data)
			{
				bankAccountData = jQuery.parseJSON(data);
				
				if(bankAccountData.Status == 1)
				{
					var oBankAndAccount = bankAccountData.Result;
					
					jQuery.each(oBankAndAccount,function(key,value){
						
						if(oBankAndAccount[key]['BankAccountStatus'] == 0)
						{
							jQuery("#IFSCCode").css("background","white");
							jQuery("#AccountNumber").css("background","white");
							
							//saveDistributorAccountInformation(bankIFSCCode,accountNumber)
							
						}
						else
						{
							showMessage("","Already a Distributor! Account Details With Same Branch Code Already Exist.","");
							//alert("Account Details With Same Branch Code Already Exist");
							jQuery("#IFSCCode").css("background","#ff9999");
							jQuery("#AccountNumber").css("background","#ff9999");
						}
						
						
					});
				}
				else
				{
					showMessage("red", bankAccountData.Descriptoin, "");
				}
			
			}, //end success.
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}
		
		});
	}

	
	/*   ----   enter pan number      ----------------------------------------                  */
	
	jQuery("#Pan").attr('maxlength','10');
	
	jQuery("#Pan").keydown(function(event){
		
		if(event.keyCode == 13 || event.keyCode == 9)
		{
		
			focusOutValidatePanNumber();
		}
		
	});
	
	jQuery("#Pan").focusout(function(event){
		
		focusOutValidatePanNumber();
		
	});
	
	function focusOutValidatePanNumber()
	{

		var panNumber = jQuery("#Pan").val();
		if(panNumber == '')
		{

		}
		else
		{
			if(panNumber.length != 10)
			{
				showMessage("", "Invalid pan number - Should be of 10 digit", "");
				jQuery("#Pan").focus();
			}
			else
			{
				validatePanNumber(panNumber);
			}


		}

	}
	
	/*   -----------------------------------------                 */
	
	function validatePanNumber(panNumber)
	{
		
		
		/*if((panNumber.charAt(4)).toUpperCase() != (distributorLastName.charAt(0)).toUpperCase())
		{
			showMessage("", "Invalid pan number - 5th character should be same as distributor last name", "");
			jQuery("#panNumber").focus();
			DISTRIBUTOR_PAN_NO_VALIDATION = 0;
		}
		else*/ 
		if(panNumberValidation(panNumber) == 1) //may be reverse.//have to change later.
		{
			//showMessage("", "Invalid pan number", "");
			
			DISTRIBUTOR_PAN_NO_VALIDATION = 1;
		}
		else
		{
			showMessage("", "Invalid pan number", "");
			jQuery("#Pan").focus();
			DISTRIBUTOR_PAN_NO_VALIDATION = 0;
		}
		
	}
	
	jQuery("#AccountNO").focusout(function(event){
		
		/*var bankIFSCCode = jQuery("#IFSCCode").val();
		var accountNumber = jQuery("#AccountNumber").val();
		
		
		if(bankIFSCCode == '' && accountNumber == '')
		{
			showMessage("","Enter IFSC and Account Number","");
			//alert("Enter IFSC and Account Number");
		}
		else if(bankIFSCCode != '' && accountNumber == '')
		{
			showMessage("","Enter Account Number","");
			//alert("Enter Account Number");
		}
		else if(bankIFSCCode == '' && accountNumber != '')
		{
			showMessage("","Enter Bank IFSC Number","");
			//alert("Enter Bank IFSC Number");
		}
		else if(bankIFSCCode != '' && accountNumber != '')
		{
			validateBankAndAccount(bankIFSCCode,accountNumber);
		}
		else
		{
			//
		}*/
		
	validateBankAccountLength();
	
	
});


function validateBankAccountLength()
{
	var bankAccountLength = jQuery("#bankAccountLength").val();
	
	if(bankAccountLength == 0)
	{
		//bank account length not defined for this bank.
		DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
	}
	else
	{
		if((jQuery("#AccountNO").val()).length != bankAccountLength)
		{
			showMessage("", "Account number length not according to selected bank", "");
			jQuery("#AccountNO").focus();
			DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 0;
			
		}
		else
		{
			DISTRIBUTOR_ACCOUNT_NO_VALIDATION = 1;
		}
	}
}
	
	
	
	
	
	
	jQuery(".upload_files").click(function(e){
		
		var documentUploadOrNot = 1;
		
		var current_id = this.id;
		var distributorId = jQuery("#S_DISDistributorId").val();
		
		 
		
		 if(jQuery("#S_DISDistributorId").val())
			{
				
				
				jQuery("#S_DISDistributorId").css('background','white');
				
				
			}
			else
			{
				
				documentUploadOrNot = 0;
				jQuery("#S_DISDistributorId").css('background','#FF9999');
			}
			
			
			
			
			
			
			
			
				
			if(documentUploadOrNot == 0)
			{
				showMessage("yellow", "document is already updated", "");
				
				e.preventDefault();//Prevent to open screen for opening file.
			}
			
			
		
	});
	
	function validateFileSize(size)
	{
		if(size/1024 <= 100)
			{
			return true;
			}
		else
			{
			jQuery(".checkbox_status").attr("checked", false);
			jQuery(".upload_files").val('');  
			showMessage("yellow", "File size cannot be greater than 100KB", "");
			return false;
			}
		
	}
	

	jQuery(".checkbox_status").attr("disabled", true);
	var current_file_upload_id;
	jQuery(".upload_files").change(function () {
		
		
			var distributorId = jQuery("#S_DISDistributorId").val();
			
			var current_id = this.id;
			current_file_upload_id = current_id + "1";
			var file_index = new Array("0","1");
			
			jQuery("#"+current_file_upload_id).removeAttr("disabled");
			jQuery("#"+current_file_upload_id).attr("checked", true);
			
				var sizef = this.files[0].size;//document.getElementById("#"+current_file_upload_id).files[0].size;
		
			if(validateFileSize(sizef))
			{
			jQuery.ajax({
				type:'POST',
				url:Drupal.settings.basePath + 'DistributorInfoSearchCallback',
				//data: jQuery("#distributor_reg_form").serialize(), // serializes the form's elements.
				data:
				{
					
					id:"username",
				},
				success: function(data)
				{

					//alert(data);
					jQuery(".ajaxLoading").hide();
					
					
					var distributorinfoEditData = jQuery.parseJSON(data);
					
					
						
						
						
						upload(current_id,file_index[0],current_file_upload_id,distributorId,distributorinfoEditData);
						if (current_id=='pan_checkbox')
		                {
						UploadedPan=1;
		                }
						else
							{
							UploadedBank=1;
							}
						
					
					
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 

					jQuery(".ajaxLoading").hide();
					var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
					showMessage("", defaultMessage, "");
				}
			});
		}	
			
			jQuery("#"+current_file_upload_id).change(function() {
				var input1 = jQuery(this); //input type file id.
				if(input1.is('checked')==false){
					//var control = jQuery("#"+current_file_upload_id);
					//control.replaceWith( control = control.clone( true ));
					//alert("dff");
					//jQuery("#"+current_id).show();
					jQuery("#"+current_id).attr("disabled",false);
					
					jQuery("#"+current_file_upload_id).attr("disabled", true);
				}
				
		    });
		
			
	});
	
	jQuery(".checkbox_status").change(function(){
		var currentCheckboxId = this.id;
		var idLength = currentCheckboxId.length;
		var currentFileInputId = currentCheckboxId.substring(0,idLength-1);
		if (jQuery("#"+currentCheckboxId).is(':checked')) {
		   
		}
		else
		{
			 jQuery("#"+currentFileInputId).show();
		}
	});
	jQuery("#cencelled_check_checkbox").change(function () {
		jQuery("#cancelled_check").removeAttr("disabled");
		jQuery("#cancelled_check").attr("checked", true);
		jQuery("#cancelled_check").change(function() {
			var input1 = jQuery(this);
			if(input1.is('checked')==false){
				var control = jQuery("#cencelled_check_checkbox");
				control.replaceWith( control = control.clone( true ) );
				jQuery("#cancelled_check").attr("disabled", true);
			}
			
	    });
	
	});
	
});
