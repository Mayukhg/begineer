/*
since you already pushed a empty object into the array, you need to modify that object:

jsonObj.itemlist[0]['title']=gentitle;
jsonObj.itemlist[0]['itemid']=genitemid;
To add more objects, you can do the same thing: push in an empty object, then modify that object. Or, you can create an object, modify it, then push it into the list.

var new_obj = {'title':gentitle, 'itemid':genitemid};
jsonObj.itemlist.push( new_obj );
To delete objects with certain attribute value:

for (var i = jsonObj.itemlist.length-1; i >= 0; i--)
    if(jsonObj.itemlist[i]['title'] == "to-be-removed")
        jsonObj.itemlist.splice(i,1);

*/
var actualOrder ;

function PromotionEngine(customerOrder, promotionsAvailable,promotionApplicabilityParticipation,promotionApplicabiliyItems)
{
	// ON PROMOTION 
	actualOrder=customerOrder ;
	var PROMOTION_CATEGORY = {"BILLBUSTER" : 1, "LINE": 2, "QUANTITY":3,"VOLUME": 4};
	var PROMOTION_APPLICABILITY = {"FIRST_PURCHASE" : 1, "REPURCHASE": 2, "APPLY_ALWAYS":3};
	var CONDITION_OPERAND = {"AND" : 1, "OR": 2};

    
	// ON CONDITIONS     
	var CONDITION_TYPE = {"BUY" : 1, "GET": 2};
	var CONDITION_ON = {"PRODUCTGROUP" : 2, "MERCHANDISING": 3, "PRODUCT":1};
	var DISCOUNT_TYPE = {"FREE_ITEM": 1, "PERCENT_OFF" : 2, "VALUE_OFF": 3, "FIXED_PRICE":4};

	//this.CustomerOrder = customerOrder;

	this.CustomerOrder = jQuery.grep(customerOrder, function(order, index){
		if(order.PromotionParticipation == 1 )
		{
			return true ;
		}
		return false ;

	});
	this.PromotionsForThisLocation = promotionsAvailable;
	this.gcf = gcf;

	this.billBusterPromotions = jQuery.grep(promotionsAvailable, function(promotion, index){
		if(promotion.PromotionCategory == PROMOTION_CATEGORY.BILLBUSTER && promotion.promotionApplicability== promotionApplicabilityParticipation)
		{
			return true ;
		}
		return false ;

	});
	this.quantityPromotions = jQuery.grep(promotionsAvailable, function(promotion, index){
		if(promotion.PromotionCategory == PROMOTION_CATEGORY.QUANTITY && promotion.promotionApplicability== promotionApplicabilityParticipation)
		{
			return true ;
		}
		return false ;

	});
	this.volumePromotions = jQuery.grep(promotionsAvailable, function(promotion, index){
		if(promotion.PromotionCategory == PROMOTION_CATEGORY.VOLUME && promotion.promotionApplicability== promotionApplicabilityParticipation)
		{
			return true ;
		}
		return false ;

	});	

	this.linePromotions = jQuery.grep(promotionsAvailable, function(promotion, index){
		if(promotion.PromotionCategory == PROMOTION_CATEGORY.LINE && promotion.promotionApplicability== promotionApplicabilityParticipation)
		{
			return true ;
		}
		return false ;

	});	
	this.countOfBillBusterPromotion =promotionsAvailable.reduce(function(previousValue, currentValue, index, array){
		var countOfPromotionType = previousValue;
		if(currentValue.PromotionCategory == PROMOTION_CATEGORY.BILLBUSTER)
		{
			countOfPromotionType = countOfPromotionType + 1;
		}

		return countOfPromotionType;
	}, 0);

	this.countOfLinePromotion =promotionsAvailable.reduce(function(previousValue, currentValue, index, array){
		var countOfPromotionType = previousValue;
		if(currentValue.PromotionCategory == PROMOTION_CATEGORY.LINE)
		{
			countOfPromotionType = countOfPromotionType + 1;
		}

		return countOfPromotionType;
	}, 0);

	this.countOfQuantityPromotion =promotionsAvailable.reduce(function(previousValue, currentValue, index, array){
		var countOfPromotionType = previousValue;
		if(currentValue.PromotionCategory == PROMOTION_CATEGORY.QUANTITY)
		{
			countOfPromotionType = countOfPromotionType + 1;
		}

		return countOfPromotionType;
	}, 0);
	this.countOfVolumePromotion =promotionsAvailable.reduce(function(previousValue, currentValue, index, array){
		var countOfPromotionType = previousValue;
		if(currentValue.PromotionCategory == PROMOTION_CATEGORY.VOLUME)
		{
			countOfPromotionType = countOfPromotionType + 1;
		}

		return countOfPromotionType;
	}, 0);

	//Promotion Category 1 Bill Buster 2Volume 3 Quantity 4 Line 
	// ConditionType -1 AND 
	//COnditionON Product 3 
	//DiscountType 1 FREE ITEM 3 FIXED PRICE 
	//ConditionType 2 is GET 1 is BUY 
	this.applyPromotion = applyPromotion;
	this.getApplicablePromotions = getApplicablePromotions;
	this.getLinePromotions = getLinePromotions;
	this.getQuantityPromotions = getQuantityPromotions;

	this.getBillBusterPromotions = getBillBusterPromotions;
	this.GetQuantityForQuantityPromotion = GetQuantityForQuantityPromotion;

	function applyPromotion(promotionID)
	{
		var modifiedCustomerOrder = this.customerOrder;


		return modifiedCustomerOrder;
	}

	function getApplicablePromotions()
	{
		var applicablePromotions = {};

		if (this.countOfBillBusterPromotion > 0 )
		{
			applicablePromotions["BillBusterPromotions"] = this.getBillBusterPromotions(this.CustomerOrder);
		}

		if (this.countOfQuantityPromotion > 0 )
		{
			applicablePromotions["QuantityPromotions"] = this.getQuantityPromotions(this.CustomerOrder);
		}

		if (this.countOfLinePromotion > 0 )
		{
			applicablePromotions["LinePromotions"] = this.getLinePromotions(this.CustomerOrder);

		}

		if (this.countOfVolumePromotion > 0 )
		{
			applicablePromotions["VolumePromotions"] = this.getVolumePromotions(this.CustomerOrder);
		}


		return applicablePromotions;


	}

	function getLinePromotions(item)
	{
		var promtionsApplicable ;

		for (var promotionIndex in this.PromotionsForThisLocation)
		{
			var promotion = this.PromotionsForThisLocation[promotionIndex];
			if (promotion.PromotionCategory == PROMOTION_CATEGORY.LINE) // 
			{
				if(item.Id == promotion.PCCOnditionCode)
				{
				}
			}

		}


		return true;
	}

	function getBillBusterPromotions(order)
	{
		//var actualOrder;actualOrder=order ;
		var pAwardPromotionItems = Array(); 
		var oQtyForPromotion = 0 ;
		var oValueForPromotion = 0 ;
		var reformedOrder;
		//Go through All Promotions and See BillBuster is to be Applied 
		for (var promotionIndex=0;promotionIndex<this.billBusterPromotions.length ; promotionIndex++)
		{

			oValueForPromotion=0 ;
			oQtyForPromotion=0;
			if(this.billBusterPromotions[promotionIndex].PCConditionON!=1){
				reformedOrder=actualOrder.slice(0); 
				reformedOrder= reformatedOrder(reformedOrder,this.billBusterPromotions[promotionIndex].PromotionId) ;
			}
			else{
				reformedOrder=actualOrder.slice(0) ;
			}

			//Go through All Promotions and See BillBuster is to be Applied 
			for (var orderIndex in reformedOrder)
			{
				var orderItem = reformedOrder[orderIndex];
				//Get Quantity and Order Value of All Items Participating in PROMOTION 
				if (orderItem.PromotionParticipation == 1)
				{
					//+in front of variable makes it number 
					oQtyForPromotion = +oQtyForPromotion +  +orderItem.Qty ;
					oValueForPromotion = +oValueForPromotion +  (+orderItem.Price);   	    		
				}    		
			}
			var promotion = this.billBusterPromotions[promotionIndex];

			var isPromotionApplicable = new Boolean();
			isPromotionApplicable = false ;

			if( oValueForPromotion>= +promotion.PTBuyQtyFrom)
			{
				//Promotion is applied when Value of Order Falls in between tier values 
				isPromotionApplicable = true ;

				var applicableQuantity = 1; //That should be there or not ? - Parveen 
				var result = (oValueForPromotion/+promotion.PTBuyQtyFrom);
				var integerPart = Math.floor(result);

				applicableQuantity = integerPart; // Change by Parveen.
				applicableQuantity=integerPart*promotion.PTQty;
				//Create Item Capturing Discount Information 
				var discountedItem = {"promotionID": promotion.PromotionId, "promotionItemID":promotion.PTConditionCode, 
						"promotionType":promotion.PTDiscountType,"PTBuyQtyFrom":promotion.PTBuyQtyFrom,"PTBuyQtyTo":promotion.PTBuyQtyTo, "promotionQty": applicableQuantity, "promotionValue":promotion.PTDiscountValue, "promotionName": promotion.PromotionName};

				//Push Item in Array to give the Promotion 
				pAwardPromotionItems.push(discountedItem);

			}

		}

		return pAwardPromotionItems;
	}

	function getPromotedItem(promotion, order)
	{
	}

	function getVolumePromotions(order)
	{

	}

	function getQuantityPromotions(order)
	{

		var pAwardPromotionItems = Array();


		var oQtyForPromotion = 0 ;
		var oValueForPromotion = 0 ;
		var reformattedOrderItems = {};
		var reformedOrder ;
		//Go through All Promotions and See Quantity is to be Applied 


		var isPromotionApplicable = new Boolean();
		isPromotionApplicable = false ;
		var previousPromotion;
		var isItemPresentInOrder = 0; // 0 Not Sure, 1 So far available for all items, 2 Not found in Order for at least one item 
		var qualifiedQtyForPromotion = 0 ;

		var promotionGETs = [];

		//Go through All Promotions and See BillBuster is to be Applied 
		for (var promotionIndex in this.quantityPromotions)
		{


         if (GetOrderAmount(order)>this.quantityPromotions[promotionIndex].MaxOrdeQty){
			// var CONDITION_TYPE = {"BUY" : 1, "GET": 2};
			if(this.quantityPromotions[promotionIndex].PCConditionON!=1){
				reformedOrder=actualOrder.slice(0); 
				reformedOrder= reformatedOrder(reformedOrder,this.quantityPromotions[promotionIndex].PromotionId) ;
			}
			else{
				reformedOrder=actualOrder.slice(0) ;
			}
			for (var orderIndex in reformedOrder)
			{
				var orderItem = reformedOrder[orderIndex];
				//reformatting Order Item for easy search 
				reformattedOrderItems[orderItem.RowID] = orderItem;
			}   

			var promotion = this.quantityPromotions[promotionIndex];
			var conditionOperand = promotion.conditPMConditionType ;
			if (promotionIndex == 0)
			{
				previousPromotion = promotion; // For First Time Consider previous and Current Promotions are same
			}else if(((+previousPromotion.PromotionId != +promotion.PromotionId)))
			{
				if (isItemPresentInOrder == 1 )
				{
					// ADD Promotions for current promotion ID 
					for (var getIndex in promotionGETs )
					{
					 if (getIndex!='contains'){
						var addPromotion = promotionGETs[getIndex];
						if (this.quantityPromotions[promotionIndex].MaxOrdeQty==0){
							discountedItem = {"promotionID": addPromotion.PromotionId, "promotionItemID":addPromotion.PCCOnditionCode, 
								"promotionType":addPromotion.PCDiscountType, "promotionQty": qualifiedQtyForPromotion, "promotionValue":addPromotion.PCDiscountValue, "promotionName": addPromotion.PromotionName};
						
						}
						else {
							var temQty=Math.floor(GetOrderAmount(order)/this.quantityPromotions[promotionIndex].MaxOrdeQty);
							qualifiedQtyForPromotion=Math.min(temQty,qualifiedQtyForPromotion );
						
							discountedItem = {"promotionID": addPromotion.PromotionId, "promotionItemID":addPromotion.PCCOnditionCode, 
									"promotionType":addPromotion.PCDiscountType, "promotionQty": qualifiedQtyForPromotion, "promotionValue":addPromotion.PCDiscountValue, "promotionName": addPromotion.PromotionName};
						
							
						}
					//	var discountedItem = {"promotionID": addPromotion.PromotionId, "promotionItemID":addPromotion.PCCOnditionCode, 
						//		"promotionType":addPromotion.PCDiscountType, "promotionQty": qualifiedQtyForPromotion, "promotionValue":addPromotion.PCDiscountValue, "promotionName": addPromotion.PromotionName};
						//Push Item in Array to give the Promotion 
						pAwardPromotionItems.push(discountedItem);	
					 }
					}
					promotionGETs = [];
					qualifiedQtyForPromotion = 0;


				}else
				{
					promotionGETs = [];
				}
				previousPromotion = promotion; // For First Time Consider previous and Current Promotions are same
				isItemPresentInOrder = 0;
			}	

			if ((+previousPromotion.PromotionId == +promotion.PromotionId))
			{
				if (isItemPresentInOrder == 2)
				{
					continue; // No need to check more conditions for same promotion ID 
				}

				if ((+promotion.PCConditionType == CONDITION_TYPE.GET))
				{
					promotionGETs.push(promotion);
					continue ; // This is what user will GET and not to be tested for purchase 
				}
				if(this.quantityPromotions[promotionIndex].PCConditionON==1)  //this is ony for product type
				{
					var itemDetail = reformattedOrderItems[promotion.PCCOnditionCode];
					if (typeof itemDetail === "undefined") 
					{
						// item not found 
						isItemPresentInOrder = 2 ;
						isPromotionApplicable = false ;

					}else
					{
						if (+itemDetail.Qty >= +promotion.PCGetQty)
						{	
							// Continue to check Other Items 
							isItemPresentInOrder = 1 ;

							//Find out how many promotional items qualify 
							// Implement Logic here 
							//Find out how many promotional items qualify 
							// Implement Logic here 
							var qtyForAssociatedPromotion = this.GetQuantityForQuantityPromotion(promotion.PromotionId); 
							var maxQualificationQty = Math.floor(+itemDetail.Qty/+promotion.PCGetQty)*qtyForAssociatedPromotion;
							qualifiedQtyForPromotion = (qualifiedQtyForPromotion == 0)?maxQualificationQty:Math.min(maxQualificationQty,qualifiedQtyForPromotion );

						}else
						{
							isItemPresentInOrder = 2 ;
							isPromotionApplicable = false ;

						}
					}
				}
				else{
					var getTotalqtyFromOrders= GetQtyFromOrder(reformedOrder);
					if (+getTotalqtyFromOrders >= +promotion.PCGetQty)
					{	
						// Continue to check Other Items 
						isItemPresentInOrder = 1 ;

						//Find out how many promotional items qualify 
						// Implement Logic here 
						//Find out how many promotional items qualify 
						// Implement Logic here 
						var qtyForAssociatedPromotion = this.GetQuantityForQuantityPromotion(promotion.PromotionId); 

						var maxQualificationQty = Math.floor(+getTotalqtyFromOrders/+promotion.PCGetQty)*qtyForAssociatedPromotion;
						qualifiedQtyForPromotion = (qualifiedQtyForPromotion == 0)?maxQualificationQty:Math.min(maxQualificationQty,qualifiedQtyForPromotion );

					}else
					{
						isItemPresentInOrder = 2 ;
						isPromotionApplicable = false ;

					}

				}

			}

			if ((isItemPresentInOrder == 1) && (this.quantityPromotions.length == (+promotionIndex+1) ))
			{
				// ADD Promotion 
				for (var getIndex in promotionGETs )
				{
				 if (getIndex!='contains'){
						
						
						var discountedItem ;
						var addPromotion = promotionGETs[getIndex];
						if (this.quantityPromotions[promotionIndex].MaxOrdeQty==0){
							discountedItem = {"promotionID": addPromotion.PromotionId, "promotionItemID":addPromotion.PCCOnditionCode, 
								"promotionType":addPromotion.PCDiscountType, "promotionQty": qualifiedQtyForPromotion, "promotionValue":addPromotion.PCDiscountValue, "promotionName": addPromotion.PromotionName};
						
						}
						else {
							var temQty=Math.floor(GetOrderAmount(order)/this.quantityPromotions[promotionIndex].MaxOrdeQty);
							qualifiedQtyForPromotion=Math.min(temQty,qualifiedQtyForPromotion );
						
							discountedItem = {"promotionID": addPromotion.PromotionId, "promotionItemID":addPromotion.PCCOnditionCode, 
									"promotionType":addPromotion.PCDiscountType, "promotionQty": qualifiedQtyForPromotion, "promotionValue":addPromotion.PCDiscountValue, "promotionName": addPromotion.PromotionName};
						
							
						}
						//Push Item in Array to give the Promotion 
						pAwardPromotionItems.push(discountedItem);	
				 }
				}
				promotionGETs = [];

				// Next Promotional ID needs to qualify again 
				isPromotionApplicable == false;
				qualifiedQtyForPromotion = 0;
				isItemPresentInOrder = 0;
			}

         }
		}
		return pAwardPromotionItems;

	}
	
	function GetOrderAmount(order){
		
		var oQtyForPromotion=0;
		var oValueForPromotion=0 ;
		for (var orderIndex in order)
		{
			var orderItem = order[orderIndex];
			//Get Quantity and Order Value of All Items Participating in PROMOTION 
			if (orderItem.PromotionParticipation == 1)
			{
				//+in front of variable makes it number 
				oQtyForPromotion = +oQtyForPromotion +  +orderItem.Qty ;
				oValueForPromotion = +oValueForPromotion +  (+orderItem.Price);   	    		
			}    		
		}
		return oValueForPromotion  ;
	}



function  GetQtyFromOrder(reformedOrder)
{

	var oQtyForPromotion =0;
	for (var orderIndex in reformedOrder)
	{
		var orderItem = reformedOrder[orderIndex];
		//Get Quantity and Order Value of All Items Participating in PROMOTION 
		if (orderItem.PromotionParticipation == 1)
		{
			//+in front of variable makes it number 
			oQtyForPromotion = +oQtyForPromotion +  +orderItem.Qty ;
			// oValueForPromotion = +oValueForPromotion +  (+orderItem.Price);   	    		
		}    		
	}
	return oQtyForPromotion ;
}


function gcf(a, b) { 
	return ( b == 0 ) ? (a):( gcf(b, a % b) ); 
}



function applyLinePromotions(item, promotionapplicable)
{

}

function applyBillBusterPromotions(order, promotionapplicable)
{

}

function applyVolumePromotions(order, promotionapplicable)
{

}

function applyQuantityPromotions(order, promotionapplicable)
{

}

function reformatedOrder(reformedorder,promotionId){
	//var  oValueForPromotion=0;
	var promotionApplicablityRow ;
	for (var promotionIndex=0 ; promotionIndex< promotionApplicabiliyItems.length;promotionIndex++)
	{
		if(promotionId==promotionApplicabiliyItems[promotionIndex].PromotionID){
			promotionApplicablityRow =promotionApplicabiliyItems[promotionIndex];
		}
	}
	for (var orderIndex=0 ; orderIndex< reformedorder.length;orderIndex++)
	{
		var orderItem = reformedorder[orderIndex];
		//Get Quantity and Order Value of All Items Participating in PROMOTION 
		if (orderItem.PromotionParticipation == 1)
		{
			//+in front of variable makes it number 
			if(ItemExistInPromotionHirarchy(orderItem.RowID,promotionApplicablityRow)==false){
				/* //  oQtyForPromotion = +oQtyForPromotion +  +orderItem.Qty ;
    		    oValueForPromotion = +oValueForPromotion +  (+orderItem.Price); */  	
				reformedorder.splice(orderIndex,1);
				orderIndex=-1;
			}
		}    		
	}       
	return reformedorder;
}

function ItemExistInPromotionHirarchy(itemId,promotionApplicablityRow){

	if (promotionApplicablityRow.ItemId.indexOf(itemId)>=0){
		return true ;

	}


	return false ;
}

function GetQuantityForQuantityPromotion(forPromotionID)
{
	var qtyToReturn = 0 ;
	for (var promotionIndex in this.quantityPromotions)
	{

		var promotion = this.quantityPromotions[promotionIndex];

		if(forPromotionID == +promotion.PromotionId )
		{
			if (+promotion.PCConditionType == 2)
			{
				qtyToReturn = +promotion.PCGetQty ;  
				break; 
			}
		}
	}

	return qtyToReturn ;
}

}
    
    

