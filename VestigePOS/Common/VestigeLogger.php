<?php
	include('Logger.php');
	
	class VestigeLogger{
		private $log;
		
		function __construct(){
			$this->log = Logger::getLogger(__CLASS__);
		}
		
		public function logMessage($message){
			$this->log->info($message);
		}

	}
?>