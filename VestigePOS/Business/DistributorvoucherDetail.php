<?php
Class DisVoucherRecode
{
	/*------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function will return distributor information from pickup center id.
	 * @param unknown $selectedPickUpCenter
	 * @return multitype:
	 */
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}

	function searchDistributorVoucher($Distributorid)
	{

		try{

			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			/* $file = fopen("D://GetItems.txt", "w");
			fwrite($file,"select bvs.DistributorID,bvs.VoucherNO,bvs.CreatedBY,bvs.Amount,bvs.LocationId from BonseVoucher_Detail bvs with (nolock) where bvs.DistributorID = "$Distributorid);
			fclose($file); */
			
			$sql = "select bvs.DistributorID,bvs.VoucherNO,us.UserName,bvs.Amount,lm.Name,(pm.KeyValue1)as paymentmode from BonseVoucher_Detail bvs with (nolock)
					inner join  location_master lm on lm.locationid=bvs.locationid
					inner join Parameter_Master pm on pm.KeyCode1=bvs.paymentmode
					inner join User_Master us on us.UserId=bvs.CreatedBY  where  ParameterCode = 'PAYMENTMODE' and bvs.DistributorID = '". $Distributorid."'";

			
			$stmt = $pdo_object->prepare($sql);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$disVoucherDetail = $this->vestigeUtil->formatJSONResult(json_encode($results), '');

			return $disVoucherDetail;

		}
		catch (PDOException $e) {

			$disVoucherDetail = $this->vestigeUtil->formatJSONResult('', $e->getMessage());

			return $disVoucherDetail;

		}
	}
	
	function savevoucher($locationId,$loggedInUserId,$distributorid,$voucheramount,$paymentmode)
	{
	
		try
		{
			$connectionString = new DBHelper();
	
	        $SeqNo ="" ;
	        $OutParam="" ;
	        
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "{CALL CreateVoucherForAdvancePayment (@inputParam=:Amount,@inputParam2=:distributorid,@inputParam3=:locationId,@inputParam4=:loginuserid,@inputParam5=:paymentmode,@OutParam=:OutParam)}";
			$stmt = $pdo_object->prepare($sql);
			
			  
	
			$stmt->bindParam(':Amount',$voucheramount, PDO::PARAM_STR);
			$stmt->bindParam(':distributorid',$distributorid,PDO::PARAM_STR);
			$stmt->bindParam(':locationId',$locationId,PDO::PARAM_STR);
			$stmt->bindParam(':loginuserid',$loggedInUserId,PDO::PARAM_STR);
			$stmt->bindParam(':paymentmode',$paymentmode,PDO::PARAM_STR);
			
			
				
			$stmt->bindParam(':OutParam',$OutParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
		
			$stmt->execute();
			
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
		
			
		    if(sizeof($results[0]['OutParam']) > 0)
			  		{
			  				throw new vestigeException($results[0]['OutParam']);
			  		}
			  			
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}
		return $results;
}

function checkDistributor($enteredDistributorId)
{
	try
	{
		$connectionString = new DBHelper();
			
		$pdo_object = $connectionString->dbConnection();  
			
		$sql = "select DistributorId,(isnull(DistributorFirstName,'')+' '+isnull(DistributorlastName,'')) as Distributorname from DistributorMaster(nolock) where DistributorStatus=2  and DistributorId = '". $enteredDistributorId."'";
			
		$stmt = $pdo_object->prepare($sql);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
		$checkDistributorData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
		return $checkDistributorData;
	}
	catch(Exception $e)
	{
		$checkDistributorData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());

		return $checkDistributorData;
	}

}
}