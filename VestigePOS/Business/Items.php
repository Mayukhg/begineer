<?php
include 'DBHelper.php'; 


Class Items
{
	function getItems($locationId,$loggedInUserType){
		
		try {
			$connectionString = new DBHelper();
			
			if($loggedInUserType == 'D'){
				$dynamicWhereClause = ' AND IM.IsItemVisibleToDistributor = 1 ';
			}
			else{
				$dynamicWhereClause = '';
			}
		
			$pdo_object = $connectionString->dbConnection();
			
			//$sql = "select keycode1 from Parameter_Master(nolock) where ParameterCode = 'POSDISPLAYITEMDESC'";
			
				$sql = "SELECT  IM.ItemId 'Id', IM.ItemCode 'ItemCode', IM.MerchHierarchyDetailId 'ParentCatId',MM.MerchHierarchyCode 'CategoryName', IM.DisplayName 'DisplayName', IM.BusinessVolume 'BV',IM.PointValue 'PV',
				im.DistributorPrice 'ItemPrice', IM.PromotionParticipation 'PromotionParticipation',
				IM.Status Status,IM.DisplayName 'ShortName',
				IM.TaxCategoryId 'TaxCategoryId' ,isnull(IWE.ImageName,'') 'ItemImageUrl',ILL.LocationId,
				isnull(MPOS_Cat,'') MPOS_Cat,isnull(MPOS_SubCat,'') MPOS_SubCat,isnull(MPOS_ProductShortName,'') MPOS_ProductShortName,
				ISNULL(a.Quantity, 0) 'CurrentInventoryPosition'
				FROM MerchHierarchy_Master MM with(nolock) ,Item_Master IM with(nolock)
				INNER JOIN ITEMLOCATION_LINK ILL with(nolock)
				
				ON IM.ItemId = ILL.ItemId
				INNER JOIN ItemMRP IMRP with(nolock)
				ON IM.ItemId = IMRP.ItemId
				INNER JOIN ItemUOM_Link IUOM with(nolock)
				ON IM.ItemId = IUOM.ItemId
				AND IUOM.TypeOfMeasure = 2
				LEFT OUTER JOIN
				(
				SELECT SUM(Quantity) Quantity, Ilbb.ItemId, LocationId FROM dbo.Inventory_LocBucketBatch ilbb with(nolock) LEFT JOIN ItemBatch_Detail IBD with(nolock)
				ON IBD.BatchNo=ILBB.BatchNo
				WHERE ilbb.BucketId = (SELECT BucketId FROM dbo.Bucket with(nolock) WHERE ParentId IS NOT NULL AND sellable = 1) -- AND IBD.ExpDate>GETDATE()
				GROUP BY ilbb.ItemId, LocationId
				) a
				ON IM.ItemId = a.ItemId
				AND ILL.LocationId = a.LocationId
				LEFT JOIN ItemWebExtensions IWE with(nolock)
				ON IWE.ItemID = IM.ItemId
				WHERE
				MM.MerchHierarchyId=IM.MerchHierarchyDetailId
				AND ILL.LocationId = $locationId
				AND IM.MerchHierarchyDetailId in (Select MerchHierarchyId From MerchHierarchy_Master with(nolock) where Status = 1 AND IsTradable = 1)
				AND IM.STATUS = 1 AND IM.IsItemAvailableForPOS=1
				AND ILL.Status = 1
				AND IM.DistributorPrice > 0
				$dynamicWhereClause
				--AND ((@isKitOrder = 1 AND IM.IsKit = 1) OR(@isKitOrder = 0 AND 1=1))
				--AND IM.ItemId IN (361, 362)
				AND ((ISNULL(IM.IsForRegistrationPurpose,0) = ISNULL(0,0) AND 1 = 2)
				OR (1 = 0  AND ISNULL(IM.IsForRegistrationPurpose,0) <> 1)--AND IM.IsKit <> 1
					
				OR (1 = 1 AND ( ISNULL(IM.IsForRegistrationPurpose,0) = 0))--IM.IsKit = 1 OR
				)
				ORDER BY MM.Name
				;";
			
			
				/* $file = fopen("D://GetItems.txt", "w");
				fwrite($file,$sql);
				fclose($file); */
			
			
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			

			return $results;
			
			
		} catch (Exception $e) {
			
			throw new Exception($e);
			
		}
		
		
		
	}

}
?>
