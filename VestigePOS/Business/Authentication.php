<?php


Class Authentication
{
	var $vestigeUtil;
	function  __construct(){
		$this->vestigeUtil = new VestigeUtil();
	}
	
	
	function searchWHLocation()
	{
		try
		{
			$vestigeCommon = new VestigeCommon();
		
			$WHLocations = $vestigeCommon->searchWHLocation(); //function in vestigeCommon.php.
		
			return $WHLocations;
		}
		catch(PDOException $e){
		
			return $e->getMessage();
		}
	}
	
	
	function fetchUserLocations($username,$password)
	{
		$connectionString = new DBHelper();
		
		$pdo_object = $connectionString->dbConnection();
			
			$sql = "select userId,UserName from User_Master(nolock) where UserName = '$username'"; //Need to change query for location.
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$countOfResult = count($results);
			
			$fetchedLocations = '';
			
			if($countOfResult == 1) //user found.
			{
				$sDecryptedPassword = Authentication :: getDecryptedPassword($password,$results[0]['userId']);
				
				if($sDecryptedPassword == $password)
				{
					$fetchedLocations = Authentication :: fetchAuthenticatedUserLocation($username);
				}
				else
				{
					throw new vestigeException("User Authentication failed ! Try again");
				}
				
				
			}
			else if ($countOfResult > 1 )
			{
				// Multiple users with same username ! Tell error how this can heppen
				// Define Error codes in the costants file
			
				throw new vestigeException("Multiple users with same username", 10000);
			}else
			{
			
			
				// USer not found // Not Authenticated // tell Module to destroy the session // Suspected breach
				// throw vestigeEx
				throw new vestigeException("User not found", 1000);
			
			}
			
		 return $fetchedLocations;
	}
	
	
	
	
	function fetchAuthenticatedUserLocation($username)
	{
		try
		{
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "Select r.LocationId ,LM.Name+'-'+LM.LocationCode as LocationName from User_Master u(nolock), UserRoleLoc_link r(nolock) left join Location_Master LM(nolock) 
			 	on r.LocationId = LM.LocationId
				Where r.UserId = u.UserId
				AND u.UserName = '$username' AND LM.Status = 1 AND
				(IsNull(NullIf('',''),'1')='1' Or r.LocationId = '') AND LM.isLocationOnline = 1
				Group by r.UserId, u.UserName, r.LocationId,u.Password,LM.Name,LM.LocationCode
				having COUNT(r.UserId) > 0
				Order By Count(r.UserId) DESC";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
		}
		catch(Exception $e)
		{
			throw new vestigeException($e->getMessage(),$e->getCode());
		}
		
		return $results;
	} 
	
	Function userAuthentication($username,$password,$locationId,$url,$source)
	{
		try
		{
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
			
			if($locationId == '')
			{
				$sql = "select userId,UserName from User_Master(nolock) where UserName = '$username' and status = 1"; //Need to change query for location.
			}
			else
			{
				$sql = "Select r.LocationId ,LM.Name as LocationName, r.UserId, COUNT(r.UserId) as userActivation,
				u.UserName,u.FirstName+' '+u.LastName userFullName,u.Password from User_Master u(nolock), UserRoleLoc_link r(nolock) left join Location_Master LM(nolock)
			 	on r.LocationId = LM.LocationId
				Where r.UserId = u.UserId
				AND u.UserName = '$username' AND u.Status = 1 AND
				(IsNull('$locationId','')='' Or r.LocationId = '$locationId')
				Group by r.UserId, u.UserName, r.LocationId,u.Password,LM.Name,u.FirstName,u.LastName
				having COUNT(r.UserId) > 0
				Order By Count(r.UserId) DESC";
				
			}
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			$userPrivilege = array();
			
			$countOfResult = count($results); 
			
			
			
			if ($countOfResult == 1 )
			{
				// User Found ! 
				
				$sql = "Select top 1 r.LocationId ,LM.Name as LocationName,LM.IsMiniBranch,LM.AssignedURL, r.UserId,u.password Token, COUNT(r.UserId) as userActivation,
				u.UserName,u.FirstName+' '+u.LastName userFullName,u.Password from User_Master u, UserRoleLoc_link r left join Location_Master LM
				on r.LocationId = LM.LocationId
				Where r.UserId = u.UserId
				AND u.UserName = '$username' AND u.Status = 1 AND
				(IsNull(NullIf('$locationId',''),'1')='1' Or r.LocationId = '$locationId')
				Group by r.UserId, u.UserName, r.LocationId,u.Password,LM.Name,u.FirstName,u.LastName,LM.isMiniBranch,LM.AssignedURL
				having COUNT(r.UserId) > 0
				Order By Count(r.UserId) DESC";
				
				
				$stmt = $pdo_object->prepare($sql);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				

				//$authenticateUserData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				if($locationId != '' && $source == "web"){
					$assignedUrl = $results[0]['AssignedURL'];
					$pos = strrpos($url,$assignedUrl);
					
					if ($pos === false) { // note: three equal signs
						// not found...
						throw new vestigeException("URL assigned for your location is <a href='http://".$assignedUrl."'>$assignedUrl</a>",222333);
					
					}
				}
				else{
					//Url checked previosuly in case of web users in their respective sections.//locationid = '' come in case of those web users only.
				}
				
				
				$userId = $results[0]['UserId'];
					
				$locationId = $results[0]['LocationId'];
				
				$locationName = $results[0]['LocationName'];
				
				$username = $results[0]['UserName'];
					
				$userFullName = $results[0]['userFullName'];
				
				$token = $results[0]['Token'];
				
				$isMiniBranch = $results[0]['IsMiniBranch'];
				
				$sDecryptedPassword = Authentication :: getDecryptedPassword($results[0]['Password'],$userId);
					
				$sUserName = $results[0]['UserName'];
				
			//	$file = fopen("D://History1234.txt", "w");
			//	fwrite($file,$password.','.$sDecryptedPassword.','.$userId.','.$results[0]['Password']);
			//	fclose($file);
				
				
				if ($password == $sDecryptedPassword && $username == $sUserName)
				{
					$privilegesArray = Authentication :: getUserPriviledges($results[0]['UserId'], $results[0]['UserName'], $results[0]['LocationId']);
					
					$modulePrivilegesArray = Authentication :: getUserModulePriviledges($results[0]['UserId'], $results[0]['UserName'], $results[0]['LocationId']);
				
					$userPrivilege['UserId'] = $userId;
				
					$userPrivilege['LocationId'] = $locationId;
				
					$userPrivilege['PrivilageArray'] = $privilegesArray;
					
					$userPrivilege['ModulePrivilageArray'] = $modulePrivilegesArray;
					
					$userPrivilege['LocationName'] = $locationName;
					
					$userPrivilege['UserName'] = $username;
					
					$userPrivilege['LoggedInUserType'] = 'E';
					
					$userPrivilege['UserFullName'] = $userFullName;

					$userPrivilege['Token'] = $token;
					
					$userPrivilege['IsMiniBranch'] = $isMiniBranch;
					
				}
				else
				{
					throw new vestigeException("Login failed .Check your username/password. ");
				}
					
				
			}else if ($countOfResult > 1 )
			{
				// Multiple users with same username ! Tell error how this can heppen 
				// Define Error codes in the costants file 
				
				throw new vestigeException("Multiple users with same username", 10000);
			}else 
			{
				
				
				// USer not found // Not Authenticated // tell Module to destroy the session // Suspected breach 
				// throw vestigeEx
				throw new vestigeException("User not found", 1000);
				
			}
				
			
			
			
		
		}
		catch(Exception $e)
		{
			$exceptionMessage = $e->getMessage();
			
			$exceptionCode = $e->getCode();
			
			throw new vestigeException($exceptionMessage,$exceptionCode);
		} 
		
		return $userPrivilege;
	}
	
	
	 
	function loggedinDistributorAuthentication($username,$password,$url,$source,$ut)
	{
		try
		{
			$distributorAuthenticationInfo = Authentication :: distributorAuthentication($username,$password,$url,$source,$ut);
			
			if($distributorAuthenticationInfo['DistributorLoggedInStatus'] == 1)
			{
						
			}	 
			else
			{
				throw new vestigeException("Distributor not found");
			}
			
		}
		catch(Exception $e)
		{
			//if($e->getCode() == 1001)
			//{
				
			//}
			throw new vestigeException($e->getMessage(),$e->getCode());
			 
		}
		
		
			
		
		return $distributorAuthenticationInfo;
	}
	
	function distributorAuthentication($distributorId,$distributorPassword,$url,$source,$ut)
	{
		
		try
		{
			$distributorOrPUCInfo = array();
			
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$distributorLoggedinStatus = 0;
			
			$sql = "select distributorid,distributorFirstName+' '+distributorLastName DistributorName ,LocationId,DistributorStateCode,
			isNull(ForSkinCareItem,0)ForSkinCareItem, DistributorMobNumber from DistributorMaster(nolock) where DistributorId = $distributorId and Password = '$distributorPassword'";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$distributorResults = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(sizeof($distributorResults))
			{
					
				$distributorId = $distributorResults[0]['distributorid'];
					
				$distributorOrPUCInfo['DistributorId'] = $distributorId;
				
				$distributorOrPUCInfo['DistributorFullName'] = $distributorResults[0]['DistributorName'];
					
				$distributorOrPUCInfo['ForSkinCareItem'] = $distributorResults[0]['ForSkinCareItem'];
					
					
					
				$PUCAuthenticationInfo = Authentication :: PUCAuthentication($distributorResults[0]['distributorid']);
				
				
									
				if(sizeof($PUCAuthenticationInfo))
				{
					$sql = "select lm1.distributorid,lm1.LocationId as PUCId, lm1.ReplenishmentLocationId as locationId,lm2.AssignedURL,
					LM2.Name+'-'+lm2.LocationCode as LocationCode, lm2.Name as LocationName
					from Location_Master LM1,Location_Master LM2
					where LM1.DistributorId = $distributorId AND LM1.replenishmentLocationId = LM2.locationId AND LM2.isLocationOnline = 1	AND 
					LM2.status =1 AND LM1.Status = 1";
						
					$stmt = $pdo_object->prepare($sql);
					$stmt->execute();
					$PUCAuthenticationInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					if(sizeof($PUCAuthenticationInfo) == 0)
					{
						throw new vestigeException("No online location privileges for this pick up centre");
					}
					else if(sizeof($PUCAuthenticationInfo) > 1)
					{
						throw new vestigeException("Can't Login ! Pick up centre linked with more than one location.");
					}
					
					$assignedUrl = $PUCAuthenticationInfo[0]['AssignedURL'];
					$pos = strrpos($url,$assignedUrl);
						
					if ($pos === false && $source == "web") { // note: three equal signs
						// not found...
						throw new vestigeException("URL assigned for your location is <a href='http://".$assignedUrl."'>$assignedUrl</a>",222333);
							
					}
					
					if($source == "mobile"){
						throw new Exception("Mobile application can be used for distributor only");
					}
			
					//session storage for puc.
					$distributorOrPUCInfo['LocationId'] = $PUCAuthenticationInfo[0]['locationId'];
					$distributorOrPUCInfo['PUCId'] = $PUCAuthenticationInfo[0]['PUCId'];
			
					$distributorOrPUCInfo['LoggedInUserType'] = 'P';
			
					$distributorOrPUCInfo['LocationCode'] = $PUCAuthenticationInfo[0]['LocationCode'];
					$distributorOrPUCInfo['LocationName'] = $PUCAuthenticationInfo[0]['LocationName'];
			
			
				}
				else
				{
				
					if($distributorResults[0]['DistributorMobNumber'] == '' || $distributorResults[0]['DistributorMobNumber'] == null){ 	 	
						//throw new Exception("Distributor not registered with mobile number. Contant branch to register mobile number and start billing."); 	 	
					}
				
					$distributorAuthenticationInfo = Authentication :: getLocationForAuthenticatedDistributor($distributorResults[0]['DistributorStateCode']);
			
					$assignedUrl = $distributorAuthenticationInfo[0]['AssignedURL'];
					$pos = strrpos($url,$assignedUrl);
					
					if ($pos === false && $source == "web") { // note: three equal signs
						// not found...
						throw new vestigeException("URL assigned for billing is <a href='http://".$assignedUrl."'>$assignedUrl</a>",222333);
							
					}
					
					if($source == "mobile"){
						//update token for distributor.
						$ut =
						$distributorTokenInfo = Authentication :: updateDistributorToken($distributorId,$ut);
						if(isset($distributorTokenInfo[0]['userToken'])){
							$distributorOrPUCInfo['UserToken'] =$distributorTokenInfo[0]['userToken'];
							$distributorOrPUCInfo['AppUrl'] = 'm.veston.in';
							
							
							
						}
					
					}
			
					$distributorOrPUCInfo['LocationId'] = $distributorAuthenticationInfo[0]['LocationId'];
			
					$distributorOrPUCInfo['LoggedInUserType'] = 'D';
			
					$distributorOrPUCInfo['LocationCode'] = $distributorAuthenticationInfo[0]['LocationCode'];
					$distributorOrPUCInfo['LocationName'] = $PUCAuthenticationInfo[0]['LocationName'];
					
					
					
			
					//session storage for distributor.
					//throw new vestigeException("Distributor not found",1000);
				}
					
				$distributorLoggedinStatus = 1;
				$distributorOrPUCInfo['DistributorLoggedInStatus'] = $distributorLoggedinStatus;
			
			}
			else
			{
				throw new vestigeException("DistributorId/Password is Incorrect ",1001);
			}
			
			 
			
			return $distributorOrPUCInfo;
		}
		catch(Exception $e)
		{
			throw new vestigeException($e->getMessage());
		} 
		
		
		
	} 
	
	
	function getLocationForAuthenticatedDistributor($stateId) //pdo object can be passed in argument too.
	{
		$connectionString = new DBHelper(); 
		
		$pdo_object = $connectionString->dbConnection();
		
		$sql = "select MRB.ALternateLocationId as LocationId,LM.Name+'-'+LM.LocationCode as LocationCode,LM.AlternateUrl AssignedURL, LM.Name as LocationName from MainRegionalBranches MRB left join location_master LM
				on MRB.ALternateLocationId = LM.LocationId
				where MRB.StateID = $stateId AND LM.IsLocationOnline = 1"; //Table is on HO datebase .
			
		$stmt = $pdo_object->prepare($sql);
		$stmt->execute();
		$distributorResults = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if(sizeof($distributorResults) == 0)
		{
			throw new vestigeException("Distributor not linked any online location.");
		}
		
		
		return $distributorResults;
			
	}
	
	function updateDistributorToken($distributorId,$ut){
	
		$connectionString = new DBHelper();
	
		$pdo_object = $connectionString->dbConnection();
	
		$sql = "update distributormaster set userToken = '$ut' output inserted.userToken where distributorid = $distributorId";
			
		$stmt = $pdo_object->prepare($sql);
		$stmt->execute();
		$distributorTokenResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
		return $distributorTokenResult;
	}		

	
	
	
	function PUCAuthentication($distributorId)
	{
		$connectionString = new DBHelper();
		
		$pdo_object = $connectionString->dbConnection();
		
		$sql = "select lm1.distributorid,lm1.LocationId as PUCId, lm1.ReplenishmentLocationId as locationId, 
				LM2.Name+'-'+lm2.LocationCode as LocationCode, lm2.Name as LocationName 
				from Location_Master LM1,Location_Master LM2
				where LM1.DistributorId = $distributorId AND LM1.replenishmentLocationId = LM2.locationId";
			
		$stmt = $pdo_object->prepare($sql);
		$stmt->execute();
		$PUCResults = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $PUCResults;
		
		
		
	}
	
	function fetchUserLocation($username)
	{
		try
		{
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
				

			$sql = "Select r.LocationId ,LM.Name+'-'+LM.LocationCode as LocationName from User_Master u(nolock), UserRoleLoc_link r(nolock) left join Location_Master LM(nolock)
			 		on r.LocationId = LM.LocationId
					Where r.UserId = u.UserId
					AND u.UserName = '$username' AND LM.Status = 1 AND
					(IsNull(NullIf('',''),'1')='1' Or r.LocationId = '')
					Group by r.UserId, u.UserName, r.LocationId,u.Password,LM.Name,LM.LocationCode
					having COUNT(r.UserId) > 0
					Order By Count(r.UserId) DESC";

			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			return $results;
				
		}
		catch(Exception $e)
		{
			//return $e->getMessage();
			throw new vestigeException($e->getMessage(),$e->getCode());
				
		}
	}
	
	function getDecryptedPassword($encryptedPassword,$userId)
	{
		//return "ok";
		
		$connectionString = new DBHelper();
			
		$pdo_object = $connectionString->dbConnection();
			
		
		$sql = "select WebUserPassword from user_master with (nolock) where UserId = $userId";
		
		$stmt = $pdo_object->prepare($sql);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		
		if($results[0]['WebUserPassword'] == null)
		{
			$userPassword = "ok";
		}
		else
		{
			$userPassword = $results[0]['WebUserPassword'];
		}

		return $userPassword;
		
	}
	
	
	function getUserPriviledges($userid,$userName,$locationId)
	{
		try
		{
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
				
			$sql = "select URLL.Roleid,MM.ModuleCode,FM.FunctionName from UserRoleLoc_link URLL(nolock)
					left join RoleModFuncCond_Link RMFCL(nolock)
					on URLL.RoleId = RMFCL.RoleId left join Module_Master MM on RMFCL.ModuleId = MM.ModuleId
					left join Function_Master FM on RMFCL.FunctionId = FM.FunctionId
					where URLL.UserId = $userid and URLL.LocationId = $locationId";
				
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			$privilagesArray  = array();
			
			for($i = 0; $i < sizeof($results); $i++)
			{
				$privilagesArray[$results[$i]['ModuleCode'].':'.$results[$i]['FunctionName']] =  1;
			}
			
			
			return $privilagesArray;
			
		}
		catch(Exception $e)
		{
			//return $e->getMessage();
			throw new vestigeException($e->getMessage(),$e->getCode());
			
		}
	}
	
	
	function getUserModulePriviledges($userid,$userName,$locationId)
	{
		try
		{
			$connectionString = new DBHelper();

			$pdo_object = $connectionString->dbConnection();


			$sql = " select count(URLL.Roleid) as CountedRole,MM.ModuleCode from UserRoleLoc_link URLL(nolock)
			left join RoleModFuncCond_Link RMFCL(nolock)
			on URLL.RoleId = RMFCL.RoleId left join Module_Master MM on RMFCL.ModuleId = MM.ModuleId
			where URLL.UserId = $userid and URLL.LocationId = $locationId 	group by MM.ModuleCode ";

			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);


			$modulePrivilagesArray  = array();

			for($i = 0; $i < sizeof($results); $i++)
			{
				$modulePrivilagesArray[$results[$i]['ModuleCode']] =  1;
			}


			return $modulePrivilagesArray;

		}
		catch(Exception $e)
		{
			//return $e->getMessage();
			throw new vestigeException($e->getMessage(),$e->getCode());

		}
	}

	function insertIncedenceValues($INCId,$INCUser,$locationId,$INCUserType,$INCImgPathRef,$msg)
	{
		try
		{
			$connectionString = new DBHelper();

			$pdo_object = $connectionString->dbConnection();


			$sql = "insert into Incidence (INCUser,INCDateTime,Status,INCLocation,INCUserType,INCImgPathRef,INCDescription,INCSolutionRef
			,INCSessionData,INCLatitude,INCLongitude,INCFixedBy,INCFixedDescription) values
			('$INCUser',getdate(),0,'$locationId','$INCUserType','$INCImgPathRef','$msg','','',0,0,0,'')
			";


			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $results;

		}
		catch(Exception $e)
		{
				
		}
	}
		

}		
		
?>
