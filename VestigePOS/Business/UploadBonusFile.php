<?php

$target_file =  basename($_FILES["bonusExcelFile"]["name"]);

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if($imageFileType == 'XLS' || $imageFileType == 'xlsx'){
	//Allow file to get uploaded.
}
else{
	echo "Only excel file are allowded";
	return;
}

if ( isset($_FILES["bonusExcelFile"]) && !empty($_FILES["bonusExcelFile"]) ) {

	/*-------------date calculation to appendin file name---------------*/
	date_default_timezone_set('asia/kolkata');

	$currentDate = getDate();
	$presentDateTime = $currentDate['yday'].'-'.$currentDate['mon'].'-'.$currentDate['year'].'-'.time();

	/*-----------------------------------------------------------------*/
	// Generate a random name
	$name= $presentDateTime.$_FILES["bonusExcelFile"]["name"];

	$imagesPathForEmail = "http://localhost/ErrorImages/".$name;
	
	// Save the file to the server
	move_uploaded_file($_FILES["bonusExcelFile"]['tmp_name'], "../UploadedBonusFile/".$name);

	// Return the name
	echo $imagesPathForEmail;
}

?>