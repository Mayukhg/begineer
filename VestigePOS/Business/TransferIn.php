<?php

Class TransferIn
{
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	function searchWHLocation()
	{
		$connectionString = new DBHelper();

		$pdo_object = $connectionString->dbConnection();
 try{
		$stmt = $pdo_object->prepare("Select
						'-1'  LocationId,
						'All' DisplayName,
						''  LocationType,
						'All' LocationName,
						'-' [Address],
						'-1' CityId,
						'Select' CityName,
						'Select' LocationCode
						,'' DistributorName
						,'' IsexportLocation
						,'' CountryName
						,'' IECCode
				        ,'-1' StateId
					Union All
					Select	lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName,
						 CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType, [Name] AS LocationName,
						ISNULL([Address1],'')+' '+ISNULL([Address2],'')+ISNULL([Address3],'')+ISNULL([Address4],'') +  ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'')
						As [Address],cm.CityId, cm.CityName,
						lc.LocationCode,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,IsexportLocation,com.CountryName,IECCode,sm.StateId StateId
					From Location_Master lc with (NOLOCK)
					Left join City_Master cm with (NOLOCK)
		
					On cm.CityId = lc.CityId

					Left join state_master sm with (NOLOCK)
					On sm.StateId = lc.StateId

					Left join Country_Master com with (NOLOCK)
					On com.CountryId = lc.CountryId
					LEFT JOIN DistributorMaster dm ON lc.DistributorId = dm.DistributorId
						WHERE LocationType IN (2, 3)
						AND lc.Status=1 ");
							$stmt->execute();
							$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					 $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
											
						return $outputData;
					}
					catch(Exception $e)
					{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
					}
	}

	function TIStatus()
	{
		$connectionString = new DBHelper();

		$pdo_object = $connectionString->dbConnection();
try{
		$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='TIStatus'
				And isactive=	1
			Order By
				sortorder Asc");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
		function searchTIItems($TONumber,$sourceId)
			{


			//	$LocationId=10;
			$outParam='';

			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			try{
				
			$sql = "{CALL usp_TItemSearch (@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
  		$stmt = $pdo_object->prepare($sql);
				
			$stmt->bindParam(':TNumber',$TONumber, PDO::PARAM_STR);
			$stmt->bindParam(':SourceAddressId',$sourceId,PDO::PARAM_INT);
  	
  		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 50);
  			
  		$stmt->execute();
  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
  			
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
}
    function searchTO($TONumber,$locationId)
				{
					$connectionString = new DBHelper();
					$pdo_object = $connectionString->dbConnection();
					try{
						
					$sql = "Select distinct head.TONumber,Convert(Varchar(20), head.CreationDate, 105) AS 'TOCreationDate', head.TOINumber,th.TOIDate AS 'TOIDate', head.SourceLocationId, head.DestinationLocationId, Convert(Varchar(20),head.CreationDate, 105) CreationDate, head.Status, head.TotalTOQuantity, head.TotalTOAmount, head.TotalTOTaxAmount, head.ModifiedDate,
		lm.Address1 + Char(13) + char(10) + IsNull(lm.Address2,'') + Char(13) + char(10) + IsNull(lm.Address3,'') + Char(13) + char(10) +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As SourceAddress, 
	    lm_1.Address1 + Char(13) + char(10) + IsNull(lm_1.Address2,'') + Char(13) + char(10) + IsNull(lm_1.Address3,'') + Char(13) + char(10) + cm_1.CityName +  ' ' + sm_1.StateName + ' '  + com_1.CountryName As DestinationAddress, 
		head.PackSize, Remarks, RefNumber,head.ShippingWayBillNo,
		prm.KeyValue1 StatusName, head.ShippingDetails, 
		Convert(Varchar(20), head.ShippingDate, 105)	ShipDate,  head.ExpectedDeliveryDate
		--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End As Indentise
		, IsNull(head.GrossWeight,0) GrossWeight,
		sm.StateId, head.CreationDate,head.ModifiedBY, UM.FirstName +' '+ UM.MiddleName +' '+ UM.LastName As ModifiedByName, 
		lm.Phone1 + ', '+ lm.Phone2 As SourcePhone, 
		lm.EmailId1,
		cm.CityName As SourceCity, 
		cm_1.CityName As DestinationCity,
		ISNULL(head.Isexported,'') AS Isexported,lm.IECCode,
		head.ExporterRef, head.OtherRef,head.BuyerOtherthanConsignee,head.PreCarriage,head.PlaceofReceiptbyPreCarrier,com.CountryName AS CountryOfOrigin,
		com_1.CountryName as CountryOfDestination,head.VesselflightNo,head.PortofLoading,head.PortofDischarge,head.PortofDestination,head.TermsofDelivery,
		head.DELIVERY,head.PAYMENT,head.BuyerOrderNo,head.BuyerOrderDate
		--Case When Isnull((head.Isexported,'')='')then '' Else  head.Isexported End AS Isexported
		From TO_Header	head with (NOLOCK)
		Inner Join TO_Detail det with (NOLOCK)
		On head.TONumber = det.TONumber
		Inner Join Location_Master lm  with (NOLOCK)
		On lm.LocationId = head.SourceLocationId
		Inner Join Location_Master lm_1 with (NOLOCK)
		On lm_1.LocationId = head.DestinationLocationId

		LEFT Join TOI_Header th with (NOLOCK)
		On th.TOINumber = head.TOINumber

		Inner Join Parameter_Master  prm  with (NOLOCK)
		On head.Status = prm.KeyCode1
		And prm.ParameterCode = 'TOStatus'
		
		Left join City_Master cm  with (NOLOCK)
		On cm.CityId = lm.CityId
		
		Left join state_master sm  with (NOLOCK)
		On sm.StateId = lm.StateId

		Left join Country_Master com  with (NOLOCK)
		On com.CountryId = lm.CountryId

		Left join City_Master cm_1  with (NOLOCK)
		On cm_1.CityId = lm_1.CityId

		Left join state_master sm_1 with (NOLOCK)
		On sm_1.StateId = lm_1.StateId

		Left join Country_Master com_1 with (NOLOCK)
		On com_1.CountryId = lm_1.CountryId

		Left Outer Join User_Master UM with (NOLOCK)
		On UM.UserId = Head.ModifiedBy

		Where	(IsNull('$locationId','-1')='-1' Or head.DestinationLocationId = '$locationId')
		And		(IsNull('-1','-1')='-1' Or head.SourceLocationId = '-1')
		And		(IsNull(NullIf('',''),'-1')='-1' Or head.RefNumber Like  '%' + '')
		And		(IsNull(NullIf('',''),'-1')='-1' Or head.TOINumber Like  '%' + '')
		And		(IsNull(NullIf('$TONumber',''),'-1')='-1' Or head.TONumber Like '%' + '$TONumber')
		AND		(IsNull('','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'2099-01-01'),112) >= Convert(varchar(10),CAST('' As DateTime),112))
		AND		(IsNull('2099-12-31T00:00:00','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'1900-01-01'),112) <= Convert(varchar(10),Cast('2099-12-31T00:00:00' As DateTime),112))
		AND		(IsNull('','')='' OR Convert(varchar(10),head.ShippingDate,112) >= Convert(varchar(10),Cast('' As DateTime),112))
		AND		(IsNull('','')='' OR Convert(varchar(10),head.ShippingDate,112) <= Convert(varchar(10),Cast('' As DateTime),112))
		AND		(IsNull('-1','-1')='-1' OR head.Status = '-1')
		AND		(IsNull('0','2')='2' Or IsNull('0','-1')='-1' Or '0' = Case When th.Indentised = 1 Then 1 Else 0 End)
		

		ORDER BY head.TONumber DESC
			
					";
						
					$stmt = $pdo_object->prepare($sql);

					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
					return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
			}
	function ShowControlTaxInfor($locationId)
				{
				$connectionString = new DBHelper();
				$pdo_object = $connectionString->dbConnection();
					try{
						
					$sql = "select phone1,phone2,mobile1,mobile2,tinNo,cstNO,VatNo from location_master with (NOLOCK)
					where  locationid ='$locationId'";
						
					$stmt = $pdo_object->prepare($sql);

					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

					$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
}
  	function saveTI($jsonForItems,$TONumber,$statusId,$LocationId,$tnumber,$createdBy,$SWNo,$Sdetail,$Remark){
	$outParam='';
	$connectionString = new DBHelper();
	$pdo_object = $connectionString->dbConnection();
	try{

	$sql = "{CALL sp_TISave (@jsonForItems=:jsonForItems,@TONumber=:TONumber,@StatusId=:StatusId,
  		@LocationId=:LocationId,@createdBy=:createdBy,@tnumber=:tnumber,@SWNo=:SWNo,@Sdetail=:Sdetail,@Remark=:Remark,@outParam=:outParam)}";
	
	$stmt = $pdo_object->prepare($sql);
  		
  		$stmt->bindParam(':jsonForItems',$jsonForItems, PDO::PARAM_STR);
		$stmt->bindParam(':TONumber',$TONumber, PDO::PARAM_STR);
		$stmt->bindParam(':StatusId',$statusId, PDO::PARAM_INT);
		$stmt->bindParam(':LocationId',$LocationId, PDO::PARAM_INT);
		$stmt->bindParam(':createdBy',$createdBy, PDO::PARAM_INT);
		$stmt->bindParam(':tnumber',$tnumber, PDO::PARAM_STR);
		
		$stmt->bindParam(':SWNo',$SWNo, PDO::PARAM_STR);
		$stmt->bindParam(':Sdetail',$Sdetail, PDO::PARAM_STR);
		$stmt->bindParam(':Remark',$Remark, PDO::PARAM_STR);
		
		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				
		$stmt->execute();
  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
  		if($results[0]['OutParam']=='INF0022')
  		{
  			throw new vestigeException('TO modified/created date mismatch current date.');
  		}
  		
  		 if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
	
			}
			catch(Exception $e){
				throw new Exception($e->getMessage());
				
			}
		return  $results ;
  		
	}
	function adjustItemAndBatch($SourceAddressId,$TOINumber,$TONumber){
		$outParam='';
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
			$sql = "{CALL sp_TIItemSearch (@TONumber=:TONumber,@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':TONumber',$TONumber, PDO::PARAM_STR);
			$stmt->bindParam(':TNumber',$TOINumber, PDO::PARAM_STR);
			$stmt->bindParam(':SourceAddressId',$SourceAddressId,PDO::PARAM_INT);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			 
			 
		 if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
			}
			catch(Exception $e){
				throw new Exception($e->getMessage());
				
			}
			return  $results ;
			
		 
	}
	function searchTI($TIFormData){
		parse_str($TIFormData, $output);
		
		//$locationId = $output['location_id']; //check name in html form
		
		
			
			
		$sourceLocation=$output['TISourceLocation'];
		$destinationLocation = $output['TIDestinationLocation'];
		
		$FromTODate = $output['fromTIShipDate'];
		
		$ToTODate = $output['toTOShipDate'];
		
		$FromReceiveDate = $output['RecFromDate'];
		$TOReceiveDate  = $output['recToDate'];
		$TONumber  = $output['TONumber'];
		$TIStatus = $output['TIStatus'];
		$TINumber  = $output['TINumber'];
		
		$indentised= $output['indentised'];
		$indentised=2;
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
				
			$sql = "Select distinct head.TOINumber, head.TONumber, head.TINumber,
		 head.SourceLocationId, head.DestinationLocationId, Convert(Varchar(20),
		 TOShippingDate, 105) TOShippingDate, head.Status, TotalTOQuantity, TotalTOAmount,
		  TotalTIQuantity, TotalTIAmount, head.ModifiedDate,
		IsNull(lm.Address1,'') + ' ' + IsNull(lm.Address2,'') + ' ' + IsNull(lm.Address3,'') + ' ' +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As SourceAddress, 
		IsNull(lm_1.Address1,'') + ' ' + IsNull(lm_1.Address2,'') + ' ' + IsNull(lm_1.Address3,'') + ' '+ cm_1.CityName +  ' ' + sm_1.StateName + ' '  + com_1.CountryName As DestinationAddress, 
		head.PackSize, Remarks ,VehicleNo As ShippingWayBillNo,IsNull(head.GrossWeight,0) As GrossWeight, 
		prm.KeyValue1 StatusName, head.ShippingDetails,  Convert(Varchar(20), ReceivedTime, 105)	ReceivedTime,
		Convert(Varchar(20), ReceivedDate, 105)	ReceivedDate--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End As Indentise
		,ISNULL(head.Isexported,0) Isexported
		From TIHeader	head with (NOLOCK)
		Inner Join TIDetail det with (NOLOCK)
		On head.TINumber = det.TINumber
		Inner Join Location_Master lm  with (NOLOCK)
		On lm.LocationId = head.SourceLocationId
		Inner Join Location_Master lm_1 with (NOLOCK)
		On lm_1.LocationId = head.DestinationLocationId

--		LEFT Join TOI_Header th with (NOLOCK)
--		On th.TOINumber = head.TOINumber
--
		
		Inner Join Parameter_Master  prm with (NOLOCK)
		On head.Status = prm.KeyCode1
		And prm.ParameterCode = 'TIStatus'
		
		Left join City_Master cm with (NOLOCK)
		On cm.CityId = lm.CityId

		Left join state_master sm with (NOLOCK)
		On sm.StateId = lm.StateId

		Left join Country_Master com with (NOLOCK)
		On com.CountryId = lm.CountryId

		Left join City_Master cm_1 with (NOLOCK)
		On cm_1.CityId = lm_1.CityId

		Left join state_master sm_1 with (NOLOCK)
		On sm_1.StateId = lm_1.StateId

		Left join Country_Master com_1 with (NOLOCK)
		On com_1.CountryId = lm_1.CountryId


		Where	(IsNull('$destinationLocation','-1')='-1' Or head.DestinationLocationId = '$destinationLocation')
		And		(IsNull('$sourceLocation','-1')='-1' Or head.SourceLocationId = '$sourceLocation')
		And		(IsNull(NullIf('$TINumber',''),'-1')='-1' Or head.TINumber Like '%' + '$TINumber')
		And		(IsNull(NullIf('$TONumber',''),'-1')='-1' Or head.TONumber Like '%' + '$TONumber')
		AND		(IsNull('$FromTODate','')='' OR Convert(varchar(10),IsNull(TOShippingDate,'2099-01-01'),101) >= Convert(varchar(10),CAST('$FromTODate' As DateTime),101))
		AND		(IsNull('$ToTODate','')='' OR Convert(varchar(10),IsNull(TOShippingDate,'1900-01-01'),101) <= Convert(varchar(10),Cast('$ToTODate' As DateTime),101))
		AND		(IsNull('$FromReceiveDate','')='' OR (Isnull(ReceivedDate,'') ='' AND Convert(Varchar(10),'$FromReceiveDate',101)='1900-01-01' AND Convert(Varchar(10),'$TOReceiveDate',101)='2099-12-31') OR Convert(varchar(10),ReceivedDate,101) >= Convert(varchar(10),Cast('$FromReceiveDate' As DateTime),101))
		AND		(IsNull('$TOReceiveDate','')=''  OR (Isnull(ReceivedDate,'') ='' AND Convert(Varchar(10),'$FromReceiveDate',101)='1900-01-01' AND Convert(Varchar(10),'$TOReceiveDate',101)='2099-12-31') OR Convert(varchar(10),ReceivedDate,101) <= Convert(varchar(10),Cast('$TOReceiveDate' As DateTime),101))
		AND		(IsNull('$TIStatus','-1')='-1' Or head.Status = '$TIStatus')
--		AND		(IsNull('$indentised','2')='2' Or IsNull('$indentised','-1')='-1' Or '$indentised' = Case When th.Indentised = 1 Then 1 Else 0 End)
		
		ORDER BY head.TINumber ASC	";
			
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
	}
?>



