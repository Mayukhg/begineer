<?php

Class Logout
{
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}

	/**
	 * function used to clear session.
	 */
	function clearSessionData()
	{
		try
		{

			//$this->vestigeUtil->destroySessionData();
			
			session_destroy();
			
			
		}
		catch(PDOException $e){

			return $e->getMessage();
		}
	}
}
?>