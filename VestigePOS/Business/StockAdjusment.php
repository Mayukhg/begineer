<?php

class StockAdjusment{
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	    function searchStatus(){
	     	$connectionString = new DBHelper();
	     	
	     	$pdo_object = $connectionString->dbConnection();
	     try{
	     	$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='INVENTORYSTATUS'
				And isactive=	1
			Order By
				sortorder Asc");
	     	$stmt->execute();
	     	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	     }
	    function intiatedBy(){
	    	$connectionString = new DBHelper();
	    	 
	    	$pdo_object = $connectionString->dbConnection();
	    try{ 
	    	$stmt = $pdo_object->prepare("Select
	    	'-1'  UserId,
	    	'Select'  UserName,
	    	'Select'  Name
	    	Union All
	    	Select UserId, UserName, FirstName + ' ' + LastName as Name
	    	From User_Master with (NOLOCK)
	    	Where Status = 1 
	    	And UserId >0");
	    	$stmt->execute();
	    	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	    $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	   	
	     }
	     function searchWHLocation()
	     {
	     	$connectionString = new DBHelper();
	     
	     	$pdo_object = $connectionString->dbConnection();
	     try{
	     	$stmt = $pdo_object->prepare("Select
						'-1'  LocationId,
						'All' DisplayName,
						''  LocationType,
						'All' LocationName,
						'-' [Address],
						'-1' CityId,
						'Select' CityName,
						'Select' LocationCode
						,'' DistributorName
						,'' IsexportLocation
						,'' CountryName
						,'' IECCode
					Union All
					Select	lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName,
						 CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType, [Name] AS LocationName,
						ISNULL([Address1],'')+' '+ISNULL([Address2],'')+ISNULL([Address3],'')+ISNULL([Address4],'') +  ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'')
						As [Address],cm.CityId, cm.CityName,
						lc.LocationCode,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,IsexportLocation,com.CountryName,IECCode
					From Location_Master lc with (NOLOCK)
					Left join City_Master cm with (NOLOCK)
	     
					On cm.CityId = lc.CityId
	     
					Left join state_master sm with (NOLOCK)
					On sm.StateId = lc.StateId
	     
					Left join Country_Master com with (NOLOCK)
					On com.CountryId = lc.CountryId
					LEFT JOIN DistributorMaster dm with (NOLOCK) ON lc.DistributorId = dm.DistributorId
						WHERE LocationType IN (2, 3)
						AND lc.Status=1 ");
	     	$stmt->execute();
	     	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	    	 $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	     }
	     function searchSA($LocationId,$Status,$SeqNo,$FromInitiateDate,$ToInitiateDate,$ApprovedBy,$Isexported,$InternalBatAdj){
	     	$connectionString = new DBHelper();
	     	try{
	     		$pdo_object = $connectionString->dbConnection();

	     		$sql = "	exec  [dbo].[sp_InventoryAdjustSearch]
	     		'$LocationId','$SeqNo','$FromInitiateDate','$ToInitiateDate',
	     		'$Status',
	     		'$ApprovedBy','$ApprovedBy',
	     		'$Isexported','$InternalBatAdj',''";
	     		/* $file = fopen("F://lookup.txt", "w+");
	     		 fwrite($file, $sql);
	     		fclose($file); */
	     		$stmt = $pdo_object->prepare($sql);
	     		$stmt->execute();
	     		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');

	     		return $outputData;
	     	}
	     	catch(Exception $e)
	     	{
	     		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());

	     		return $exception;
	     	}
	     }
		  function searchALLBucket(){
		  	$connectionString = new DBHelper();
		  	
		  	$pdo_object = $connectionString->dbConnection();
		  	try{
		  	$sql = "Select	
						'-1' BucketId,
						'Select' BucketName, 
						-1 Sellable
				Union All
				Select 
						buc.BucketId,
						BucketName, 
						Sellable
				From Bucket buc(nolock)
				Where buc.Status = 1 AND buc.ParentId Is Not Null
				And BucketId <>9
		  		
		  			";
		  	
		  	$stmt = $pdo_object->prepare($sql);
		  	$stmt->execute();
		  	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  	$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
							
				return $outputData;
			  }
		catch(Exception $e)
		 			{
							$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
							
							return $exception;
					}
		  }
		  function reasonCode(){
		  	$connectionString = new DBHelper();
		  
		  	$pdo_object = $connectionString->dbConnection();
		  try{
		  	$sql = "Select
						-1 'keycode1',
						'Select' 'keyvalue1',
						-1 'keycode2',
						'Select' 'keyvalue2',
						-1 'keycode3',
						'Select' 'keyvalue3',
						1 'isactive',
						-1 'sortorder',
						'' 'ParameterCode',
						'' 'description'
				Union All
				Select
						keycode1,
						keyvalue1,
						ISNULL(keycode2, 0) 'keycode2',
						ISNULL(keyvalue2, '') 'keyvalue2',
						ISNULL(keycode3, 0) 'keycode3',
						ISNULL(keyvalue3, '') 'keyvalue3',
						isactive,
						sortorder,
						ParameterCode,
						ISNULL([description], '') 'description'
				From	Parameter_Master with (NOLOCK)
				Where
					parametercode='ReasonCode'
					And isactive=	1
				Order By
					sortorder Asc";
		  
		  
		  	$stmt = $pdo_object->prepare($sql);
		  	$stmt->execute();
		  	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
							
				return $outputData;
			  }
		catch(Exception $e)
		 			{
							$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
							
							return $exception;
					}
		  
		  }
		//  function searchQtyForItem($sourceLocationId,$batchNo,$itemCode,$bucketId,$type,$itemCodeOrItemId,$searchScreen){
		  function searchQtyForItem($sourceLocationId,$batchNo,$itemId,$bucketId,$type){
		  	$connectionString = new DBHelper();
		  	$sql="";
		  	$pdo_object = $connectionString->dbConnection();
			
		  	try{
		  		if($type==10){
		  			$sql="  select DISTINCT IBD.BatchNo  BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  			,IBD.ManufactureBatchNo ManufacturingCode,ISNULL(ILBB.Quantity,0) Quantity,'OnHand'BucketName ,'1' UOMId,'each' UOMName
		  			,IBD.MfgDate,IBD.ExpDate,GETDATE() TodayDate
		  			from ItemBatch_Detail IBD  with (NOLOCK) LEFT JOIN Inventory_LocBucketBatch ILBB with (NOLOCK)
		  			on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId and ILBB.LocationId='$sourceLocationId' AND ILBB.BucketId='$bucketId'
		  			LEFT JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  			where  IBD.ManufactureBatchNo like '%$batchNo%' and ILBB.Quantity>0
		  			AND IBD.ItemId='$itemId' Order by  IBD.ManufactureBatchNo desc ";
		  		}
		  	else if($type!=1){
		  			$sql="  select DISTINCT IBD.BatchNo  BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  				,IBD.ManufactureBatchNo ManufacturingCode,ISNULL(ILBB.Quantity,0) Quantity,'OnHand'BucketName ,'1' UOMId,'each' UOMName
		  				,IBD.MfgDate,IBD.ExpDate,GETDATE() TodayDate
		  				from ItemBatch_Detail IBD  with (NOLOCK) LEFT JOIN Inventory_LocBucketBatch ILBB with (NOLOCK)
		  				on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId and ILBB.LocationId='$sourceLocationId' AND ILBB.BucketId='$bucketId'
		  				LEFT JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  				where  IBD.ManufactureBatchNo like '%$batchNo%'
		  				AND IBD.ItemId='$itemId' Order by  IBD.ExpDate,IBD.ManufactureBatchNo desc";
		  		}
		  		else{
		  		$sql=" select DISTINCT(IBD.BatchNo ) BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  		,IBD.ManufactureBatchNo ManufacturingCode,ILBB.Quantity Quantity,B.BucketName ,UM.UOMId,UM.UOMName
		  		,IBD.MfgDate,IBD.ExpDate
		  		from Inventory_LocBucketBatch ILBB with (NOLOCK)
		  		INNER JOIN
		  		ItemBatch_Detail IBD with (NOLOCK)
		  		on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId
		  		INNER JOIN Bucket B with (NOLOCK) ON
		  		B.BucketId=ILBB.BucketId
		  		INNER JOIN ItemUOM_Link IUL with (NOLOCK) ON
		  		IUL.ItemId=IBD.ItemId
		  		INNER JOIN UOM_Master UM with (NOLOCK) on
		  		UM.UOMId=IUL.UOMId INNER JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  		where  IBD.ManufactureBatchNo ='$batchNo'
		  		AND ILBB.ItemId='$itemId'  AND ILBB.LocationId='$sourceLocationId' AND B.BucketId='$bucketId'
		  		";
		  		}
		  	
		  		$stmt = $pdo_object->prepare($sql);
		  		$stmt->execute();
		  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
		  			
		  		return $outputData;
		  	}
			  catch(Exception $e)
			  {
			  	$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			  		
			  	return $exception;
					}
					 
		  }
		  function searchQtyForItemCR($sourceLocationId,$batchNo,$itemCode,$bucketId,$type,$itemCodeOrItemId,$searchScreen){
		  	$connectionString = new DBHelper();
		  	$sql="";
		  	$pdo_object = $connectionString->dbConnection();
		  	 
		  	/*$file = fopen('D://StockAdj20140621.txt','w');
		  	 fwrite($file, $sourceLocationId.'-'.$batchNo.'-'.$itemCode.'-'.$bucketId.'-'.$type.'-'.$itemCodeOrItemId.'-'.$searchScreen);
		  	fclose($file);*/
		  	 
		  	if($itemCodeOrItemId == 0)
		  	{
		  		$sql = "select ItemId from item_master(nolock) where ItemCode = '$itemCode'";
		  		$stmt = $pdo_object->prepare($sql);
		  		$stmt->execute();
		  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  
		  		$itemId = $results[0]['ItemId'];
		  	}
		  	else{
		  		$sql = "select ItemId from item_master(nolock) where ItemCode = '$itemCode'";
		  		$stmt = $pdo_object->prepare($sql);
		  		$stmt->execute();
		  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  
		  		$itemId = $results[0]['ItemId'];
		  	}
		  	 
		  	try{
		  		if($type!=1){
		  			 
		  			 
		  			if($searchScreen == 'CR')
		  			{
		  				/*$sql="select top 51 IBD.ManufactureBatchNo ManufacturingCode,IM.ItemName,IM.itemCode,IM.DistributorPrice MRP,'-' Quantity,'-' Weight,
		  				 IBD.MfgDate,IBD.ExpDate,'-' UOMId,'-' UOMName,IBD.BatchNo
		  				BatchNo from ItemBatch_Detail IBD with (NOLOCK) Left Join Item_Master IM with
		  				(NOLOCK) ON IM.ItemId=IBD.ItemId where IM.ItemId=$itemId
		  				AND ManufactureBatchNo like '%$batchNo%'";*/
		  				$sql="select DISTINCT IBD.BatchNo  BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  				,IBD.ManufactureBatchNo ManufacturingCode,ISNULL(ILBB.Quantity,0) Quantity,'OnHand'BucketName ,'1' UOMId,'each' UOMName
		  				,IBD.MfgDate,IBD.ExpDate,GETDATE() TodayDate
		  				from ItemBatch_Detail IBD  with (NOLOCK) LEFT JOIN Inventory_LocBucketBatch ILBB with (NOLOCK)
		  				on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId and ILBB.LocationId='$sourceLocationId' AND ILBB.BucketId='5'
		  				LEFT JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  				where  IBD.ManufactureBatchNo like '%$batchNo%'
		  				AND IBD.ItemId='$itemId' Order by  IBD.ExpDate desc
		  				";
		  			}
		  			else
		  			{
		  			$sql="select DISTINCT(IBD.BatchNo ) BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  			,IBD.ManufactureBatchNo ManufacturingCode,ILBB.Quantity Quantity,B.BucketName ,UM.UOMId,UM.UOMName
		  			,IBD.MfgDate,IBD.ExpDate
		  			from Inventory_LocBucketBatch ILBB with (NOLOCK)
		  			INNER JOIN
		  			ItemBatch_Detail IBD with (NOLOCK)
		  			on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId
		  			INNER JOIN Bucket B with (NOLOCK) ON
		  			B.BucketId=ILBB.BucketId
		  			INNER JOIN ItemUOM_Link IUL with (NOLOCK) ON
		  			IUL.ItemId=IBD.ItemId
		  			INNER JOIN UOM_Master UM with (NOLOCK) on
		  			UM.UOMId=IUL.UOMId INNER JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  			where  IBD.ManufactureBatchNo like '$batchNo' OR ILBB.ItemId='$itemId'  AND ILBB.LocationId='$sourceLocationId' AND B.BucketId='$bucketId'
		  			";
		  
		  			}
		  			 
		  			}
		  			else{
		  			 
		  			if($searchScreen == 'CR')
		  			{
		  			/*$sql="select top 51 IBD.ManufactureBatchNo ManufacturingCode,IM.ItemName,IM.itemCode,IM.DistributorPrice MRP,'-' Quantity,'-' Weight,
		  			IBD.MfgDate,IBD.ExpDate,'-' UOMId,'-' UOMName,IBD.BatchNo
		  			BatchNo from ItemBatch_Detail IBD with (NOLOCK) Left Join Item_Master IM with
		  			(NOLOCK) ON IM.ItemId=IBD.ItemId where IM.ItemId=$itemId
		  			AND ManufactureBatchNo like '%$batchNo%'";*/
		  			$sql="select DISTINCT(IBD.BatchNo ) BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  			,IBD.ManufactureBatchNo ManufacturingCode,ILBB.Quantity Quantity,B.BucketName ,UM.UOMId,UM.UOMName
		  				,IBD.MfgDate,IBD.ExpDate,GETDATE() TodayDate
		  				from Inventory_LocBucketBatch ILBB with (NOLOCK) INNER JOIN ItemBatch_Detail IBD with (NOLOCK)
		  				on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId
		  				INNER JOIN Bucket B with (NOLOCK) ON
		  				B.BucketId=ILBB.BucketId
		  				INNER JOIN ItemUOM_Link IUL with (NOLOCK) ON
		  				IUL.ItemId=IBD.ItemId
		  				INNER JOIN UOM_Master UM with (NOLOCK) on
		  				UM.UOMId=IUL.UOMId INNER JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  				where  IBD.ManufactureBatchNo ='$mgfNo'
		  				OR ILBB.ItemId='$itemId' AND ILBB.LocationId='$sourceLocationId' AND B.BucketId='5'
		  				";
		  			}
		  			else{
		  
		  			$sql=" select DISTINCT(IBD.BatchNo ) BatchNo, ILBB.BucketId,IM.ItemCode,Im.ItemName ,Im.DistributorPrice MRP,IM.Weight
		  			,IBD.ManufactureBatchNo ManufacturingCode,ILBB.Quantity Quantity,B.BucketName ,UM.UOMId,UM.UOMName
		  			,IBD.MfgDate,IBD.ExpDate
		  			from Inventory_LocBucketBatch ILBB with (NOLOCK)
		  			INNER JOIN
		  			ItemBatch_Detail IBD with (NOLOCK)
		  			on IBD.BatchNo=ILBB.BatchNo AND IBD.ItemId=ILBB.ItemId
		  			INNER JOIN Bucket B with (NOLOCK) ON
		  			B.BucketId=ILBB.BucketId
		  			INNER JOIN ItemUOM_Link IUL with (NOLOCK) ON
		  			IUL.ItemId=IBD.ItemId
		  			INNER JOIN UOM_Master UM with (NOLOCK) on
		  			UM.UOMId=IUL.UOMId INNER JOIN Item_Master IM with (NOLOCK) ON Im.ItemId=IBD.ItemId
		  			where  IBD.ManufactureBatchNo ='$batchNo' AND ILBB.ItemId='$itemCode' AND ILBB.LocationId='$sourceLocationId' AND B.BucketId='$bucketId'
		  			";
		  			}
		  			 
		  			}
		  
		  		
		  			$stmt = $pdo_object->prepare($sql);
		  			$stmt->execute();
		  			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
		  			 
		  			return $outputData;
		  	}
     catch(Exception $e)
		       {
		       $exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
		        
		       return $exception;
		  	}
		  
		  	}
		  function saveOrder($isExported,$SeqNo,$jsonForItems,$ApprovedBy,$LocationId,$UserId,$InitiatedDate,
	  		$StatusId,$ModifiedBy,$SourceLocationId,$tempIsInternalBatchAdj)
	  {
		
	  	$connectionString = new DBHelper();
	  	$pdo_object = $connectionString->dbConnection();
	  	try{
	  		$ApprovedDate='';
	  		$CreatedDate='';
	  		$ModifiedDate='';
	  		$outParam='';
	  		$tempIsInternalBatchAdj=1;
	  		$sql='';
	  		if($isExported==0){
	  		$sql = "{CALL sp_InventorySave (@jsonForItems=:jsonForItems,@ApprovedBy=:ApprovedBy,@SeqNo=:SeqNo,@ApprovedDate=:ApprovedDate
	  				,@LocationId=:LocationId
				,@UserId=:UserId,@InitiatedDate=:InitiatedDate,@StatusId=:StatusId,@ModifiedBy=:ModifiedBy,@CreatedDate=:CreatedDate
						,@ModifiedDate=:ModifiedDate,
						@SourceLocationId=:SourceLocationId,@tempIsInternalBatchAdj=:tempIsInternalBatchAdj,@outParam=:outParam)}";
	  		}
	  		else{
	  		$sql =	"{CALL sp_InventorySave2 (@jsonForItems=:jsonForItems,@ApprovedBy=:ApprovedBy,@SeqNo=:SeqNo,@ApprovedDate=:ApprovedDate
	  			,@LocationId=:LocationId
	  			,@UserId=:UserId,@InitiatedDate=:InitiatedDate,@StatusId=:StatusId,@ModifiedBy=:ModifiedBy,@CreatedDate=:CreatedDate
	  			,@ModifiedDate=:ModifiedDate,
	  			@SourceLocationId=:SourceLocationId,@tempIsInternalBatchAdj=:tempIsInternalBatchAdj,@outParam=:outParam)}";
	  		}
	  		$stmt = $pdo_object->prepare($sql);
	  		$stmt->bindParam(':jsonForItems',$jsonForItems, PDO::PARAM_STR);
	  		$stmt->bindParam(':ApprovedBy',$ApprovedBy, PDO::PARAM_INT);
	  		$stmt->bindParam(':SeqNo',$SeqNo, PDO::PARAM_STR);
	  		$stmt->bindParam(':ApprovedDate',$ApprovedDate, PDO::PARAM_STR);
	  		$stmt->bindParam(':LocationId',$LocationId, PDO::PARAM_INT);
	  		$stmt->bindParam(':UserId',$UserId, PDO::PARAM_INT);
	  
	  		$stmt->bindParam(':InitiatedDate',$InitiatedDate, PDO::PARAM_STR);
	  		$stmt->bindParam(':StatusId',$StatusId, PDO::PARAM_INT);
	  
	  		$stmt->bindParam(':ModifiedBy',$ModifiedBy, PDO::PARAM_INT);
	  		$stmt->bindParam(':CreatedDate',$CreatedDate, PDO::PARAM_STR);
	  		$stmt->bindParam(':ModifiedDate',$ModifiedDate, PDO::PARAM_STR);
	  		$stmt->bindParam(':SourceLocationId',$SourceLocationId, PDO::PARAM_INT);
	  		$stmt->bindParam(':tempIsInternalBatchAdj',$tempIsInternalBatchAdj, PDO::PARAM_INT);
	  		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
	
	  			
	  		$stmt->execute();
	  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  		if($results[0]['OutParam'] == 'VAL0022')
	  		{
	  			throw new vestigeException('Inventory for this location less then stock adjusment quantity.');
	  		}
	  		if(sizeof($results[0]['OutParam']) > 0)
	  		{
	  			throw new vestigeException($results[0]['OutParam']);
	  		}
	  		
	  		}
	  		catch(Exception $e){
	  			throw new Exception($e->getMessage());
	  		
	  		}
	  		return  $results ;  
	  }
         function getAdjustDetails($AdjusmentNo){
         	$connectionString = new DBHelper();
         	 
         	$pdo_object = $connectionString->dbConnection();
         	try{
         		$stmt = $pdo_object->prepare("exec usp_InventoryItemSearch '$AdjusmentNo',''");
         		$stmt->execute();
         		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
         		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
         	
							
				
			  }
		  catch(Exception $e)
		 			{
							$outputData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
							
					}
				return $outputData; 
         	
         }	   

}

?>
