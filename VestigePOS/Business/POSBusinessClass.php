
<?php

define('__PATH1__', 'C:\wamp\www\drupal-7.38\sites\all\modules\VestigePOS\VestigePOS'); 	 	
 	 	
include_once (__PATH1__.'/Common/VestigeUtil.php'); 	 	
include_once (__PATH1__.'/Business/vestigeException.php');


Class POSBusinessClass
{
	
	var $vestigeUtil;
	function  __construct(){
		$this->vestigeUtil = new VestigeUtil();
	}
	
	function sendEmail($dynamicText,$emailSubject){
		
		$emailTemplate = new EmailTemplate();
		$templateText = $emailTemplate->emailTemplateHtml($dynamicText);
		//$emailSubject = 'Security Alert - Fraud case attempted from unkonow IP Address';

		drupal_mail('VestigePOS','errorReportTemplate',"parveenverma42@gmail.com",'',
		array(
		'emailSubject'=>$emailSubject,
		'imageLink'=>$templateText
		//'typeOfEmail'=>distributorRegistration
		
		)
		);
	}
	
	/*
	 * function creating object of DBHelper class and getting connection string information from there.
	 */
	function dbConnectionInfo()
	{
		$dbHelperObject = new DBHelper(); //Contain database connection information or string.
		$pdo_db = $dbHelperObject->dbConnection(); //function in DBHelper class.
		return $pdo_db;
	}
	
	
	
	function distributorRegistation()
	{
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	/*
	 * Function to get Distributor information from upline number.
	 */
	function uplineDistributorInformation($upline_no)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT DM.DistributorFirstName,ISNULL(DM.AllInvoiceAmountSum,0) AllInvoiceAmountSum,DM.DistributorLastName,DM.DistributorAddress1,
					DM.DistributorAddress2,DM.DistributorAddress3,CM.CityName,SM.StateName
					FROM DistributorMaster DM
					LEFT JOIN City_Master CM
					ON DM.distributorCityCode = CM.cityid
					LEFT JOIN State_Master SM
					ON DM.distributorStateCode = SM.stateid
					WHERE DM.DistributorId=$upline_no AND(DM.DistributorStatus = 1 OR DM.DistributorStatus = 2);");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$uplineDistributorData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $uplineDistributorData;
		}
		catch(Exception $e)
		{
			$uplineDistributorData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $uplineDistributorData;
		}
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	/*
	 * Function to get availble countries.
	 */
	function countries()
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("select CountryId, CountryName from Country_Master with (NOLOCK) where countryid = 1  and status = 1");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$countriesData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $countriesData;
		}
		catch(Exception $e)
		{
			$countriesData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $countriesData;
		}
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	/*
	 * Function to get country from country id.
	*/
	function countryFromCountryId($countryId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT [CountryId]
					,[CountryCode]
					,[CountryName]
					,[SortOrder]
					,[Status]
					FROM Country_Master with (NOLOCK) where CountryId = $countryId");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$countryFromCountryIdData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $countryFromCountryIdData;
		}
		catch(Exception $e)
		{
			$countryFromCountryIdData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $countryFromCountryIdData;
		}
		
	}
	
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	/*
	 * Function to get state from State Id.
	*/
	function stateFromStateId($stateId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT [StateId]
					,[StateCode]
					,[StateName]
					,[CountryId]
					,[SortOrder]
					,[Status]
					,[ZoneId]
					,[IsState]
					,[ParentId]
					FROM  State_Master with (NOLOCK) where StateId = $stateId");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stateFromStateIdData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $stateFromStateIdData;
		}
		catch(Exception $e)
		{
			$stateFromStateIdData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $stateFromStateIdData;
		}
		
		
	}
	
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	/*
	 * Function to get city from city id.
	*/
	function cityFromCityId($cityId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT [CityId]
					,[CityCode]
					,[CityName]
					,[StateId]
					,[SortOrder]
					,[Status]
					FROM City_Master with (NOLOCK) where CityId = $cityId");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$cityFromCityIdData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $cityFromCityIdData;
		}
		catch(Exception $e)
		{
			$cityFromCityIdData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $cityFromCityIdData;
		}
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 *Function to get availale states.
	 */
	function states($country_id)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$stmt = $pdo_object->prepare("select StateId,StateName,CountryId from State_Master with (NOLOCK) where countryid = 1 and Status = 1");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$statesData = $this->vestigeUtil->formatJSONResult(json_encode($results), '','getStateMap');
			
			return $statesData;
		}
		catch(Exception $e)
		{
			$statesData = $this->vestigeUtil->formatJSONResult('', $e->getMessage(),'getStateMap');
				
			return $statesData;
		}
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function to get available cities
	 */
	function cities($state_id)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$stmt = $pdo_object->prepare("SELECT CityId,CityName FROM City_Master with (NOLOCK) WHERE StateId = $state_id ORDER BY CityName;");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$citiesData = $this->vestigeUtil->formatJSONResult(json_encode($results),'','getCityMap');
			
			return $citiesData;
		}
		catch(Exception $e)
		{
			$citiesData = $this->vestigeUtil->formatJSONResult('',$e->getMessage(),'getCityMap');
				
			return $citiesData;
		}
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function to get zone from stateid
	*/
	function zones($state_id)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$stmt = $pdo_object->prepare("SELECT OM.OrgHierarchyId, OM.OrgHierarchyCode,SM.StateId
					FROM OrgHierarchy_Master OM with (NOLOCK) left join State_Master SM with (NOLOCK)
					on OM.OrgHierarchyId = SM.ZoneId
					WHERE OM.OrgConfigId = 2");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$zonesData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $zonesData;
		}
		catch(Exception $e)
		{
			$zonesData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $zonesData;
		}
		
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function to get sub zone form zone id. 
	*/
	function subZones($zoneId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$stmt = $pdo_object->prepare("SELECT OrgHierarchyId, OrgHierarchyCode,ParentHierarchyId
					FROM OrgHierarchy_Master with (NOLOCK)
					WHERE OrgConfigId = 3");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$subZonesData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $subZonesData;
		}
		catch(Exception $e)
		{
			$subZonesData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $subZonesData;
		}
		
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function to get area form sub zone id.
	*/
	function area($subzoneId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$stmt = $pdo_object->prepare("SELECT OrgHierarchyId, OrgHierarchyCode,ParentHierarchyId
					FROM OrgHierarchy_Master with (NOLOCK)
					WHERE OrgConfigId = 4");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$areaData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $areaData;
		}
		catch(Exception $e)
		{
			$areaData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $areaData;
		}
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function validating RTGS/IFSC code.
	*/
	function validateRTGSCode($RTGSCode)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
				
			$stmt = $pdo_object->prepare("select BBM.BankId,BAL.FixedLength from BankBranch_Master BBM with (NOLOCK)
					left join BankAccountLength BAL with (NOLOCK)
					on BBM.BankId = BAL.BankId where BankBranckCode  = '$RTGSCode'");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$areaData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $areaData;
		}
		catch(Exception $e)
		{
			$areaData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $areaData;
		}
	
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function to get area form sub zone id.
	*/
	function validatePanNumber($panNumber)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$stmt = $pdo_object->prepare("select COUNT(*) 'PANStatus' from DistributorMaster with (NOLOCK) where DistributorPANNumber  = '$panNumber'");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$validatePanNumberData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $validatePanNumberData;
		}
		catch(Exception $e)
		{
			$validatePanNumberData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $validatePanNumberData;
		}
		
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function to get area form sub zone id.
	*/
	function validateBankAndAccountNumber($IFSCCode,$accountNumber)
	{
	
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$sql = "select COUNT(*) 'BankAccountStatus' from DistributorAccountDetails with (NOLOCK) where DistributorBankBranch = '$IFSCCode' and DistributorBankAccNumber = '$accountNumber'";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$validateBankAccountNumberData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $validateBankAccountNumberData;
		}
		catch(Exception $e)
		{
			$validateBankAccountNumberData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $validateBankAccountNumberData;
		}
		
	}
	
	
	function loadDistributorBankDetails()
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$sql = "select -1 BankId,'Select Bank' BankName,-1 BankAccountFixedLength union select PM.KeyCode1 BankId,
					PM.KeyValue1 BankName,isnull(BAL.FixedLength,0) BankAccountFixedLength  from Parameter_Master PM with (NOLOCK)
					left join BankAccountLength BAL with (NOLOCK)
					on PM.KeyCode1 = BAL.BankID
					where PM.parameterCode  = 'BankID'";
				
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
		
		return $results;
	}
	
	function loadingDistributorOrPucInfo($locationId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$sql = "  select address1+''+Address2+''+Address3 as Address from Location_Master with (NOLOCK) where LocationId = $locationId ";
				
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
				
			return $results;
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}
	
	
	function getOrderNumberFromInvoiceNumber($invoiceNo)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$sql = "select CustomerOrderNo from ciheader with (NOLOCK) where invoiceno = '$invoiceNo' ";
		
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
		
			return $results;
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}
	
	
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/*
	 * Function Saving Distributor Registration Information.
	 */
	function saveDistributorInformation($distributor_form_data,$forSkinCareItem,$distributorPassword,$bankId,$locationId,$loggedInUserId,$current_login_user_id)
	{
		try{

			$sendSMSClassObj = new SendSMS();

			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
				
			parse_str($distributor_form_data, $output);
				
			$outParam = '';
				
			if($bankId == -1)
			{
				$bankId = null;
			}
				
			$ditributor_no = strtoupper($output['distributor_no']);
			$serial_no = strtoupper($output['serial_no']);
			$pv_month = strtoupper($output['pv_month']);
			$upline_no = strtoupper($output['upline_no']);
				
				
			$firstname = strtoupper($output['firstname']);
			$lastname = strtoupper($output['lastname']);
			$address = strtoupper($output['address']);
			$distributor_title = strtoupper($output['title']);
			$distributor_firstname = strtoupper($output['distributor_firstname']);
			$distributor_lastname = strtoupper($output['distributor_lastname']);
			$distributor_dob = $output['distributor_dob'];
			
			
			$distributorModifiedDOB = explode("/",$distributor_dob);
			
			$distributor_dob = strtoupper($distributorModifiedDOB[2]."/".$distributorModifiedDOB[1]."/".$distributorModifiedDOB[0]);
			
			
			$co_distributor_title = strtoupper($output['co_title']);
			$co_distributor_firstname = strtoupper($output['co_distributor_firstname']);
			$co_distributor_lastname = strtoupper($output['co_distributor_lastname']);
			$co_distributor_dob = strtoupper($output['co_distributor_dob']);
			
			if($co_distributor_dob == null || $co_distributor_dob == '')
			{
				//Co distributor id DOB not entered.
			}
			else
			{

				$coDistributorModifiedDOB = explode("/",$co_distributor_dob);
				
				$co_distributor_dob = strtoupper($coDistributorModifiedDOB[2]."/".$coDistributorModifiedDOB[1]."/".$coDistributorModifiedDOB[0]);
				
			}
			
			
			$DistributorAddress1 = strtoupper($output['address1']);
			$DistributorAddress2 = strtoupper($output['address2']);
			$DistributorAddress3 = strtoupper($output['address3']);
			$IFSCCode = strtoupper($output['IFSCCode']);
			$accountNumber = strtoupper($output['AccountNumber']);
				
				
			$phone = strtoupper($output['phone']);
			$mobile = strtoupper($output['mobile']);
			$email = strtoupper($output['email']);
			$country = strtoupper($output['country']);
			$state = strtoupper($output['state']);
			$city = strtoupper($output['city']);
			$zone = strtoupper($output['Zone']);
			$subZone = $output['SubZone'];
			$area = $output['Area'];
			$Distributor_pin = $output['pin'];
			$Distributor_pan = strtoupper($output['Pan']);
				
			$distributorStatus = 1; //considered 1 for new distributor register.
				
			$minimumPurchasingAmount = 600.00;
				
				
			$pan_uploaded = $output['panUploadedDocument'];
			$address_uploaded = $output['addressUploadedDocument'];
			$cancel_cheque_uploaded = $output['cancelledChequeUploadedDocument'];
				
				
			$distributorRegistrationDate = '1/1/1990';
				
				
			$sql="{CALL sp_RegisterDistributor (@DistributorId=:DistributorId,@UplineDistributorId=:UplineDistributorId,
					@DistributorTitle=:DistributorTitle,
					@DistributorFirstName=:DistributorFirstName,@DistributorLastName=:DistributorLastName,@DistributorDOB=:DistributorDOB,
					@Co_DistributorTitle=:Co_DistributorTitle,@Co_DistributorFirstName=:Co_DistributorFirstName,
					@Co_DistributorLastName=:Co_DistributorLastName,
					@Co_DistributorDOB=:Co_DistributorDOB,@DistributorAddress1=:DistributorAddress1,@DistributorAddress2=:DistributorAddress2,
					@DistributorAddress3=:DistributorAddress3,@DistributorCityCode=:DistributorCityCode,@DistributorStateCode=:DistributorStateCode,
					@DistributorCountryCode=:DistributorCountryCode,
					@DistributorPinCode=:DistributorPinCode,@DistributorTeleNumber=:DistributorTeleNumber,@DistributorMobNumber=:DistributorMobNumber,
					@DistributorEMailID=:DistributorEMailID,@DistributorRegistrationDate=:DistributorRegistrationDate,@MinFirstPurchaseAmount=:MinFirstPurchaseAmount,
					@DistributorStatus=:DistributorStatus,@LocationId=:LocationId,@DistributorPANNumber=:DistributorPANNumber,@CreatedBy=:CreatedBy,
					@CreatedDate=:CreatedDate,@ModifiedBy=:ModifiedBy,@ModifiedDate=:ModifiedDate,@SubZone=:SubZone,
					@Area=:Area,@Zone=:Zone,@bankId=:bankId,@IFSCCode=:IFSCCode,@accountNumber=:accountNumber,@ForSkinCareItem=:forSkinCareItem,
					@distributorPassword=:distributorPassword,
					@uploadedPan=:uploadedPan, @uploadedAddress=:uploadedAddress, @uploadedCancelledCheque=:uploadedCancelledCheque, @currentloginuserid=:currentLoggedInUserId,@outParam=:outParam )}";
				

				
				
				
			$stmt = $pdo_object->prepare($sql);
				
			$stmt->bindParam(':DistributorId',$ditributor_no, PDO::PARAM_INT);
			$stmt->bindParam(':UplineDistributorId',$upline_no, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorTitle',$distributor_title, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorFirstName',$distributor_firstname, PDO::PARAM_STR);
			$stmt->bindParam(':DistributorLastName',$distributor_lastname, PDO::PARAM_STR);
			$stmt->bindParam(':DistributorDOB',$distributor_dob, PDO::PARAM_STR);
			$stmt->bindParam(':Co_DistributorTitle',$co_distributor_title, PDO::PARAM_INT);
			$stmt->bindParam(':Co_DistributorFirstName',$co_distributor_firstname,PDO::PARAM_STR);
			$stmt->bindParam(':Co_DistributorLastName',$co_distributor_lastname, PDO::PARAM_STR);
			$stmt->bindParam(':Co_DistributorDOB',$co_distributor_dob, PDO::PARAM_STR);
			$stmt->bindParam(':DistributorAddress1',$DistributorAddress1, PDO::PARAM_STR);
			$stmt->bindParam(':DistributorAddress2',$DistributorAddress2, PDO::PARAM_STR);
			$stmt->bindParam(':DistributorAddress3',$DistributorAddress3, PDO::PARAM_STR);
				
			$stmt->bindParam(':DistributorCityCode',$city, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorStateCode',$state, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorCountryCode',$country, PDO::PARAM_INT);
				
			$stmt->bindParam(':DistributorPinCode',$Distributor_pin, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorTeleNumber',$phone, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorMobNumber',$mobile, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorEMailID',$email, PDO::PARAM_STR);
			$stmt->bindParam(':DistributorRegistrationDate',$distributorRegistrationDate, PDO::PARAM_STR);
			$stmt->bindParam(':MinFirstPurchaseAmount',$minimumPurchasingAmount, PDO::PARAM_INT);
				
			$stmt->bindParam(':DistributorStatus',$distributorStatus, PDO::PARAM_INT);
			$stmt->bindParam(':LocationId',$locationId, PDO::PARAM_INT);
			$stmt->bindParam(':DistributorPANNumber',$Distributor_pan, PDO::PARAM_STR);
			$stmt->bindParam(':CreatedBy',$loggedInUserId, PDO::PARAM_INT);
				
			$stmt->bindParam(':CreatedDate',$distributorRegistrationDate, PDO::PARAM_STR);
			$stmt->bindParam(':ModifiedBy',$loggedInUserId, PDO::PARAM_INT);
			$stmt->bindParam(':ModifiedDate',$distributorRegistrationDate, PDO::PARAM_STR);
			$stmt->bindParam(':SubZone',$subZone, PDO::PARAM_INT);
				
			$stmt->bindParam(':Area',$area, PDO::PARAM_INT);
			$stmt->bindParam(':Zone',$zone, PDO::PARAM_INT);
			$stmt->bindParam(':bankId',$bankId, PDO::PARAM_INT);
			$stmt->bindParam(':IFSCCode',$IFSCCode, PDO::PARAM_STR);
			$stmt->bindParam(':accountNumber',$accountNumber, PDO::PARAM_STR);
			$stmt->bindParam(':forSkinCareItem',$forSkinCareItem, PDO::PARAM_BOOL);
			$stmt->bindParam(':distributorPassword',$distributorPassword, PDO::PARAM_STR);
				
				
			$stmt->bindParam(':uploadedPan',$pan_uploaded, PDO::PARAM_STR);
			$stmt->bindParam(':uploadedAddress',$address_uploaded, PDO::PARAM_STR);
			$stmt->bindParam(':uploadedCancelledCheque',$cancel_cheque_uploaded, PDO::PARAM_STR);
			$stmt->bindParam(':currentLoggedInUserId',$current_login_user_id, PDO::PARAM_STR);
				
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);

			$stmt->execute();
				
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
		
			//POSBusinessClass::saveAccountDetails($ditributor_no,$IFSCCode,$accountNumber,$loggedInUserId);
				
			if($results[0]['OutParam'] == 'INV001')
			{
				throw new vestigeException("Distributor already registered");
			}
			else if($results[0]['OutParam'] == 'INV002')
			{
				throw new vestigeException("Pan number already exist");
			}
			else if($results[0]['OutParam'] == 'INV003')
			{
				throw new vestigeException("IFSC Code and Account number already exist");
			}
			else if($results[0]['OutParam'] == 'INV004')
			{
				throw new vestigeException("Wrong RTGS/IFSC Code");
			}
			else if($results[0]['OutParam'] == 'INV005')
			{
				throw new vestigeException("Wrong RTGS/IFSC Code");
			}
			else if($results[0]['OutParam'] == 'INV006')
			{
				throw new vestigeException("Wrong bank account number");
			}
				
			else if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
			else
			{
				$distributorCompleteName = $distributor_firstname .''. $distributor_lastname;
				
				$sendSMSClassObj->sendSMSToDistributor($mobile, $ditributor_no, $distributorPassword); //Function send s

				//No need to do with sms response hence not taken any.
				
				if($output['email'] != '')
				{
					$to = $output['email'];
					
					$distributorCompleteName = $distributor_firstname .' '. $distributor_lastname;
					
					$distributorUplineCompleteName = $output['firstname'].' '.$output['lastname'];
					
					drupal_mail('VestigePOS','notice',$output['email'],'',
					array(
					'emailSubject'=> 'Distributor Registration Confirmation',
					'distributorId' =>$ditributor_no,
					'distributorName' => $distributorCompleteName,
					'distributorPassword'=> $distributorPassword,
					'distributorUpline'=>$upline_no,
					'distributorUplineName'=>$distributorUplineCompleteName
					//'typeOfEmail'=>distributorRegistration  
					
							)
					);
				}
				
				
				//$sendSMSClassReponse = $sendSMSClassObj->sendSMSToDistributor($mobile, $distributor_firstname, $distributorPassword);


				/* $sendSMSClassReponse =
				
				if($sendSMSClassReponse == 1)
				{
					//Message sent.
				}
				else
				{
					//Need to throw exception. Then need to throw from there.
				}
				 */
				
			}
				

		}
		catch (Exception $e) {
			
		
			//$pdo_object->rollBack();
			
			//$distributorRegistrationSeqNoData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			throw new Exception($e);	
		
			
		}
		
		//$pdo_object->commit();
		
		
		
		
		return $results;
		
	}
	
	function avehi($to,$messageParam)
	{
		
		
		$to ='parveenverma42@gmail.com';
			
		drupal_mail('VestigePOS','notice',$to,'',
		array(
		'emailSubject'=> 'Vestige Help Desk',
		'fileLink'=>'ac.hyml',
		'typeOfEmail'=>'helpDesk'
		
				)
		);
	}
	
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------*/
	function generateLogNumber($LocationId)
	{
		try {
			
			$SeqType='DSN' ;
			//$LocationId=10;
			$SeqNo='';
			$retval=2;
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnectionInfoForOutParam();
			
			
			$sql = "{CALL usp_GetSeqNo (@SeqType=:SeqType,@LocationId=:LocationId,@SeqNo=:SeqNo)}";
				
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->bindParam(':SeqType',$SeqType, PDO::PARAM_STR);
			$stmt->bindParam(':LocationId',$LocationId,PDO::PARAM_INT);
			
			$stmt->bindParam(':SeqNo',$SeqNo, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
			$stmt->execute();
			//$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			return $SeqNo;
			
		} catch (Exception $e) {
			
			throw new Exception($e);
			
			
		}
	
		
		
	}
	
	
	/*-----------------------------------------------------------------------------------------------------------------------------*/
	function heirarchyLevelUpdate($upline_no)
	{
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnectionInfoForOutParam();
		
		$stmt = $pdo_object->prepare("SELECT dm.HierarchyLevel
		FROM DistributorMaster  dm with (NOLOCK) WHERE dm.DistributorId = $upline_no");
		
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results[0]['HierarchyLevel'];
	}
	
	/*-----------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Save account details.
	 */
	function saveAccountDetails($ditributor_no,$IFSCCode,$accountNumber,$loggedInUserId)
	{
		$connectionString = new DBHelper();
		
		$isPrimary = 1; //default taken for new distributor as 1.
		
		$pdo_object = $connectionString->dbConnectionInfoForOutParam();
		$sql = "insert into DistributorAccountDetails with (NOLOCK) values('$ditributor_no','','$IFSCCode','$accountNumber','',$loggedInUserId,GETDATE(),$loggedInUserId,GETDATE(),$isPrimary)";

		$stmt = $pdo_object->prepare($sql);
		$stmt->execute();
		
		return ;
	}
	
	/************************************************************************************************************************************/
	
	
	
	
	/*-----------------------------------------------------------------------------------------------------------*/
	
	/*
	 *Provide Distributor information corresponding to id provided to it. 
	 */
	function distributorInformation($distributorId,$loggeinType,$loggedInDistributor,$loggedInlocationId)
	{
		try
		{
			/*function call in logTeamOrder please take care 
			 */
			
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			
			if ($loggeinType!='D')
			{
			$stmt = $pdo_object->prepare("SELECT  DM.*,CM.CityName,SM.StateName,lm.countryid as 'usercountry',
 					case when DM.DistributorStatus = 1 AND DM.firstOrderTaken = 0
					THEN 1
					WHEN DM.DistributorStatus = 2 
					THEN 2 
					ELSE -100
					END PromotionApplicability 		
					 FROM distributorMaster DM with (NOLOCK) LEFT JOIN City_Master CM with (NOLOCK) ON
					DM.DistributorCityCode = CM.CityId LEFT JOIN State_Master SM with (NOLOCK) ON DM.DistributorStateCode = SM.StateId
					inner join Location_Master lm with (NOLOCK) on DM.DistributorCountryCode=lm.countryid and lm.locationid=$loggedInlocationId
					WHERE DM.DistributorId= $distributorId AND (DM.DistributorStatus = 2 OR DM.DistributorStatus = 1 ) ");
			}
			else if($loggeinType=='D' and $distributorId== $loggedInDistributor ) {
				$stmt = $pdo_object->prepare("SELECT  DM.*,CM.CityName,SM.StateName,lm.countryid as 'usercountry',
						case when DM.DistributorStatus = 1 AND DM.firstOrderTaken = 0
						THEN 1
						WHEN DM.DistributorStatus = 2
						THEN 2
						ELSE -100
						END PromotionApplicability
						FROM distributorMaster DM with (NOLOCK) LEFT JOIN City_Master CM with (NOLOCK) ON
						DM.DistributorCityCode = CM.CityId LEFT JOIN State_Master SM with (NOLOCK) ON DM.DistributorStateCode = SM.StateId
						inner join Location_Master lm with (NOLOCK) on DM.DistributorCountryCode=lm.countryid and lm.locationid=$loggedInlocationId
						WHERE DM.DistributorId= $distributorId ");
			}
			else {
				$stmt = $pdo_object->prepare("SELECT  DM.*,CM.CityName,SM.StateName,lm.countryid as 'usercountry',
 					case when DM.DistributorStatus = 1 AND DM.firstOrderTaken = 0
					THEN 1
					WHEN DM.DistributorStatus = 2 
					THEN 2 
					ELSE -100
					END PromotionApplicability
					 FROM distributorMaster DM with (NOLOCK) LEFT JOIN City_Master CM with (NOLOCK) ON
					DM.DistributorCityCode = CM.CityId LEFT JOIN State_Master SM with (NOLOCK) ON DM.DistributorStateCode = SM.StateId 
						inner join Location_Master lm with (NOLOCK) on DM.DistributorCountryCode=lm.countryid and lm.locationid=$loggedInlocationId
					WHERE DM.DistributorId= $distributorId AND $loggedInDistributor = (
		              select DistributorId from  ufn_GetDistributorUpline ($distributorId) where distributorId
		              =$loggedInDistributor
		            ) AND
		(DM.DistributorStatus = 2 OR DM.DistributorStatus = 1 )
		 
		  ");
			}
			$file = fopen("C://History2345.txt", "w");
			fwrite($file,$stmt);
			fclose($file);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$distributorInfoData = $this->vestigeUtil->formatJSONResult(json_encode($results), '','distributorInfo');
			
				
			
			return $distributorInfoData;
			
			
			
			
		}
		catch(Exception $e)
		{
			$distributorInfoData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $distributorInfoData;
		}
		
	}
	
	/*
	 *Provide Distributor information corresponding to id provided to it.
	*/
	function checkFirstOrderOrNot($distributorId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("select dm.DistributorId,COUNT(co.CustomerOrderNo) as 'NoOfOrders' from DistributorMaster dm with (NOLOCK) left join COHeader co with (NOLOCK) on dm.DistributorId = co.DistributorId where dm.DistributorId = $distributorId group by dm.DistributorId");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$checkFirstOrderOrNotData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $checkFirstOrderOrNotData;
		}
		catch(Exception $e)
		{
			$checkFirstOrderOrNotData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $checkFirstOrderOrNotData;
		}
		
		
	}
	
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide all available payment modes Available.
	 */
	function paymentModes($locationId,$loggedInUserType)
	{
		
		try
		{
			
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			if($loggedInUserType == 'E')
			{
				$stmt = $pdo_object->prepare("SELECT
					PM.PaymentModeId as 'Id',
					PM.ParentId as 'ParentType',
					PM.[Level] as 'Level',
					case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.SmallDescription end as 'ShortDescription',
					case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.LongDescription end as 'Description',
					case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.ReceiptDisplay end as 'ReceiptDisplay',
					ISNULL(PM.[Value], 0) as 'Value',
					ISNULL(PM.Currency, '') as 'Currency',
					ISNULL(PM.PopupId, 0) as 'OpenPopup',
					PM.PaymentMode 'PaymentMode',
					pm.SortOrder AS  'SortOrder',
					case when PM.PaymentModeId=5 then 'Bonus Payorder' else pm.smalldescription  end AS 'smalldescription'
					FROM PAYMENT_MASTER PM with (NOLOCK)
					inner join LocationTender_link LTL with (NOLOCK)
					on PM.PaymentModeId=LTL.TenderID and LTL.status=1
					INNER JOIN ( SELECT * FROM  Parameter_Master with (NOLOCK) WHERE  ParameterCode = 'PAYMENTMODEDISPLAY')pm2 ON pm2.KeyCode1 =  pm.PaymentModeId
					WHERE
					PM.STATUS = 1  AND PM.Level = 1 and LTL.Locationid=$locationId ");
			}
			else if($loggedInUserType == 'D'){
					
					
				$stmt = $pdo_object->prepare("SELECT
						PM.PaymentModeId as 'Id',
						PM.ParentId as 'ParentType',
						PM.[Level] as 'Level',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.SmallDescription end as 'ShortDescription',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.LongDescription end as 'Description',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.ReceiptDisplay end as 'ReceiptDisplay',
						ISNULL(PM.[Value], 0) as 'Value',
						ISNULL(PM.Currency, '') as 'Currency',
						ISNULL(PM.PopupId, 0) as 'OpenPopup',
						PM.PaymentMode 'PaymentMode',
						pm.SortOrder AS  'SortOrder',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else pm.smalldescription  end AS 'smalldescription'
						FROM PAYMENT_MASTER PM with (NOLOCK)
						INNER JOIN ( SELECT * FROM  Parameter_Master with (NOLOCK) WHERE  ParameterCode = 'PAYMENTMODEDISPLAY')pm2 ON pm2.KeyCode1 =  pm.PaymentModeId
						WHERE
						PM.STATUS = 1  AND PM.Level = 1 and pm.PaymentModeId not in (12,14,4)");
			}
			ELSE
			{
				$stmt = $pdo_object->prepare("SELECT
						PM.PaymentModeId as 'Id',
						PM.ParentId as 'ParentType',
						PM.[Level] as 'Level',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.SmallDescription end as 'ShortDescription',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.LongDescription end as 'Description',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.ReceiptDisplay end as 'ReceiptDisplay',
						ISNULL(PM.[Value], 0) as 'Value',
						ISNULL(PM.Currency, '') as 'Currency',
						ISNULL(PM.PopupId, 0) as 'OpenPopup',
						PM.PaymentMode 'PaymentMode',
						pm.SortOrder AS  'SortOrder',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else pm.smalldescription  end AS 'smalldescription'
						FROM PAYMENT_MASTER PM with (NOLOCK)
						INNER JOIN ( SELECT * FROM  Parameter_Master with (NOLOCK) WHERE  ParameterCode = 'PAYMENTMODEDISPLAY')pm2 ON pm2.KeyCode1 =  pm.PaymentModeId
						
						WHERE
						PM.STATUS = 1  AND PM.Level = 1 
						
						INTERSECT
						
						SELECT
	PM.PaymentModeId as 'Id',
						PM.ParentId as 'ParentType',
						PM.[Level] as 'Level',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.SmallDescription end as 'ShortDescription',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.LongDescription end as 'Description',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else PM.ReceiptDisplay end as 'ReceiptDisplay',
						ISNULL(PM.[Value], 0) as 'Value',
						ISNULL(PM.Currency, '') as 'Currency',
						ISNULL(PM.PopupId, 0) as 'OpenPopup',
						PM.PaymentMode 'PaymentMode',
						pm.SortOrder AS  'SortOrder',
						case when PM.PaymentModeId=5 then 'Bonus Payorder' else pm.smalldescription  end AS 'smalldescription'
						FROM PAYMENT_MASTER PM with (NOLOCK)
						INNER JOIN ( SELECT * FROM  Parameter_Master with (NOLOCK) WHERE  ParameterCode = 'PAYMENTMODEDISPLAY')pm2 ON pm2.KeyCode1 =  pm.PaymentModeId
						 inner join LocationTender_link LTL with (NOLOCK)
					on PM.PaymentModeId=LTL.TenderID and LTL.status=1 
						WHERE
						PM.STATUS = 1  AND PM.Level = 1 
						and LTL.Locationid=$locationId
						 ");
			}
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$paymentModeData = $this->vestigeUtil->formatJSONResult(json_encode($results), '','payments');
			
		//	$file = fopen("D://History2345.txt", "w");
		//	fwrite($file,$paymentModeData);
		//	fclose($file);
			
			return $paymentModeData;
		}
		catch(Exception $e)
		{
			$paymentModeData = $this->vestigeUtil->formatJSONResult('', $e->getMessage(),'payments');
				
			return $paymentModeData;
		}
		
	}
	
	
	
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide all stock and delivery locations available.
	 */
	function stockPointDeliveryInfo($locationId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT * from (SELECT [LocationId],CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' WHEN 4 THEN 'PC' END LocationType
					, [Name] + '-' + [LocationCode] AS [LocationCode],[Name] AS LocationName, LocationConfigId,
					[Address1],[Address2],[Address3],[Address4]
					,LM.[CityId],LM.[Pincode],LM.[StateId],LM.[CountryId],[Phone1],[Phone2]
					,CM.CityName,SM.StateName,CNM.CountryName
					,[Mobile1],[Mobile2],[Fax1],[Fax2],[EmailId1],[EmailId2]
					,[WebSite],[ModifiedDate], ISNULL([ReplenishmentLocationId], -1) 'ReplenishmentId'
					,1 AS LocationCode1,1 isprimaryLocation
					FROM [Location_Master]LM with (NOLOCK)
					JOIN City_Master CM with (NOLOCK) ON LM.[CityId]=CM.[CityId]
					JOIN State_Master SM with (NOLOCK) ON LM.[StateId]=SM.[StateId]
					JOIN Country_Master [CNM] with (NOLOCK) ON LM.[CountryId]=CNM.[CountryId]
					WHERE (LM.[Locationid]=$locationId OR ISNULL($locationId,'')='')
					AND LM.Status =1
					UNION
					SELECT  [LocationId],CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' WHEN 4 THEN 'PC' END LocationType
					,[Name] + '-' + [LocationCode] AS [LocationCode] ,[Name] AS LocationName, LocationConfigId,
					[Address1],[Address2],[Address3],[Address4]
					,LM.[CityId],LM.[Pincode],LM.[StateId],LM.[CountryId],[Phone1],[Phone2]
					,CM.CityName,SM.StateName,CNM.CountryName
					,[Mobile1],[Mobile2],[Fax1],[Fax2],[EmailId1],[EmailId2]
					,[WebSite],[ModifiedDate], ISNULL([ReplenishmentLocationId], -1) 'ReplenishmentId'
					, 2 AS LocationCode1,0 isprimaryLocation
					FROM [Location_Master]LM with (NOLOCK)
					JOIN City_Master CM with (NOLOCK) ON LM.[CityId]=CM.[CityId]
					JOIN State_Master SM with (NOLOCK) ON LM.[StateId]=SM.[StateId]
					JOIN Country_Master [CNM] with (NOLOCK) ON LM.[CountryId]=CNM.[CountryId]
					WHERE (LM.[ReplenishmentLocationId]= $locationId)
					AND LM.Status =1 
		)A
					ORDER BY LocationCode1,LocationCode");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stockPointDeliveryData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $stockPointDeliveryData;
		}
		catch(Exception $e)
		{
			$stockPointDeliveryData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $stockPointDeliveryData;
		}

	}
	
	function getPOSUserNamePasswordForDistributor($typeOfUserLoggedIn)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			
			if($typeOfUserLoggedIn == 'P'){
				$stmt = $pdo_object->prepare("SELECT KeyValue1 as UserName, KeyValue2 as UserPassword FROM Parameter_Master with (NOLOCK)  Where ParameterCode = 'WEBPOSUSER' AND KeyCode1 = 0");
			}
			if($typeOfUserLoggedIn == 'D'){
				$stmt = $pdo_object->prepare("SELECT KeyValue1 as UserName, KeyValue2 as UserPassword FROM Parameter_Master with (NOLOCK)  Where ParameterCode = 'WEBPOSUSER' AND KeyCode1 = 1");
			}
			
			//$stmt = $pdo_object->prepare("SELECT KeyValue1 as UserName, KeyValue2 as UserPassword FROM Parameter_Master with (NOLOCK)  Where ParameterCode = 'WEBPOSUSER' AND KeyCode1 = 0");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			
			
			return $results;
		}
		catch(Exception $e)
		{
			$results = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $results;
		}
	
	
	
	}
	
	
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide all applicable promotion.
	 */
	function getPromotion()
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT	 pm.promotionId,
				pc.ConditionType,
				pc.ConditionId,
				ISNULL(pc.ConditionOn,0) as ConditionOn,
				ISNULL(pc.ConditionCode,0) as ConditionCode,
				pc.GetQty
				--pc.DiscountType,
				--pc.DiscountValue
				FROM promotionLocation_Link pll with (NOLOCK)
				INNER JOIN promotion_condition pc with (NOLOCK)
				ON pll.promotionId			= pc.promotionId
				INNER JOIN promotion_master pm with (NOLOCK)
				ON pll.promotionId			= pm.promotionId
				AND pm.promotionCategory	=	1
				WHERE pll.locationId		=	10--(SELECT LocationId FROM Location_Master with (NOLOCK) WHERE LocationCode = @iLocationCode)
				AND pll.status			=	1
				AND pm.status			=	1
				AND pc.ConditionType	=	1
				AND pm.Applicability <> (CASE WHEN 0 = 0 THEN 1 ELSE 2 END)
				--AND pm.BuyConditionType = 1
				--AND pc.GetQty		<=	5
				AND CONVERT(VARCHAR(10),getdate(),112)		>=	CONVERT(VARCHAR(10),pm.startdate,112)
				AND CONVERT(VARCHAR(10),getdate(),112)		<=	CONVERT(VARCHAR(10),pm.enddate,112)
				AND CONVERT(VARCHAR(12),getdate(),114)	>=	CONVERT(VARCHAR(12),pm.DurationStart,114)
				AND CONVERT(VARCHAR(12),getdate(),114)	<=	CONVERT(VARCHAR(12),pm.DurationEnd,114)
				Order BY PM.ModifiedDate DESC, pc.conditionId desc;");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$promotionData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $promotionData;
		}
		catch(Exception $e)
		{
			$promotionData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $promotionData;
		}
		
		

	}
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide Log/Team order infomation.
	 */
	function getLogTeamOrderInformation($status,$boid,$log_type,$pcid,$distributor_number)
	{
		
		try
		{
			
			//Reason to consider variable is, for team order sometimes pcid is getting saved as 0 and sometimes equal to boid.
			//and that check is getting failed in query that's why removing check to take care about pcid when logtype 2 (team orders) are been searched in system.
			$dynamicPart1 = "";
			$dynamicPart2 = "";
			if($log_type == 1)
			{
				$distributor_number = 0;
				$dynamicPart1 = "AND COH.PCId = $pcid";
				$dynamicPart2 = "and OL.PCId = $pcid";
			}
			else
			{
				
			}
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			/* $sql = "select ol.BOId,OL.LogNo,OL.DistributorId,ol.LogType,ol.isZeroLog,ol.LogNo LogValue,
			ol.Address1+ol.Address2+ol.Address3+ol.Address4+','+CM.CityName+','+SM.StateName LogAddress,ol.isChangeAddress DeliveryMode,
			(select isnull(sum(COP.PaymentAmount),0) from COPayment COP where cop.CustomerOrderNo IN
						(select COH.customerOrderNo from coheader COH where COH.LogNo = OL.LogNo
						 AND COH.Status = 3 AND COH.BOId = $boid AND COH.PCId = $pcid) AND
						COP.TenderType = 6) LogOrderAmount from OrderLog OL with (NOLOCK)
			LEFT JOIN City_Master CM with (NOLOCK) ON CM.CityId = OL.CityCode LEFT JOIN State_Master SM with (NOLOCK) ON SM.StateId = OL.StateCode
			where OL.BOId = $boid and OL.PCId = $pcid and OL.LogType = $log_type and OL.status = $status order by CreatedDate desc"; */
			
							
			$sql = "select ol.BOId,OL.LogNo,OL.DistributorId,ol.LogType,ol.isZeroLog,ol.LogNo LogValue,
			ol.Address1+ol.Address2+ol.Address3+ol.Address4+','+CM.CityName+','+SM.StateName LogAddress,ol.isChangeAddress DeliveryMode,
			(SELECT isnull(sum(COP.PaymentAmount),0) 
							FROM COPayment COP with(nolock) , coheader COH with(nolock) 
							WHERE cop.CustomerOrderNo = COH.CustomerOrderNo 
							AND COH.LogNo = OL.LogNo
							AND COH.Status = 3 
							AND COH.BOId = $boid 
							AND COH.PCId = $pcid 
							$dynamicPart1
							AND COP.TenderType = 6) LogOrderAmount from OrderLog OL with (NOLOCK)
			LEFT JOIN City_Master CM with (NOLOCK) ON CM.CityId = OL.CityCode LEFT JOIN State_Master SM with (NOLOCK) ON SM.StateId = OL.StateCode
			where OL.BOId = $boid $dynamicPart2 and OL.PCId = $pcid and OL.LogType = $log_type and OL.status = $status order by CreatedDate desc";
			
			/* COMMENTED TO FIX THE DEFECT WHERE LOG NO Amout is not shown 
			$sql = "select ol.BOId,OL.LogNo,OL.DistributorId,ol.LogType,ol.isZeroLog,ol.LogNo LogValue,
			ol.Address1+ol.Address2+ol.Address3+ol.Address4+','+CM.CityName+','+SM.StateName LogAddress,ol.isChangeAddress DeliveryMode
			from OrderLog OL with (NOLOCK)
			LEFT JOIN City_Master CM with (NOLOCK) ON CM.CityId = OL.CityCode LEFT JOIN State_Master SM with (NOLOCK) ON SM.StateId = OL.StateCode
			where OL.BOId = $boid and OL.PCId = $pcid and OL.LogType = $log_type and OL.status = $status order by CreatedDate desc";
			*/
			
			//$sql = "exec usp_OrderLogSearch $status,$boid,NULL,$log_type,$distributor_number,$pcid,'outparam'";


			
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$getLogTeamOrderInformationData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $getLogTeamOrderInformationData;
		}
		catch(Exception $e)
		{
			$getLogTeamOrderInformationData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $getLogTeamOrderInformationData;
		}
		
		
	}
	
	function displayMessage($loggedInUserType){
		$pdo_object = POSBusinessClass :: dbConnectionInfo();
		
		$sql = "select case when displayFor = '*' then  msgText when displayFor = '$loggedInUserType' then msgText end msgText from Messages where 
				getdate() > displayFrom and getdate() < displayTo";
		//$sql = "exec usp_OrderLogSearch $status,$boid,NULL,$log_type,$distributor_number,$pcid,'outparam'";
		
		
		$stmt = $pdo_object->prepare($sql);
		
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results;
		
	}
	
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide Log/Team order infomation.
	*/
	function getLogTeamOrderInformation1($status,$boid,$log_type,$pcid,$distributor_number,$orderMode)
	{
	
		try
		{
			/* if($log_type == 1)
				{
			$distributor_number = 0;
			}
			else
			{
	
			} */
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
				
			$sql = "select ol.BOId,OL.LogNo,OL.DistributorId,ol.LogType,ol.isZeroLog,ol.LogNo LogValue,
			ol.Address1 +''+isnull(ol.Address2,'')+''+isnull(ol.Address3,'')+''+isnull(ol.Address4,'')+','+isnull(CM.CityName,'')+','+isnull(SM.StateName,'') LogAddress,ol.isChangeAddress DeliveryMode,
			ol.Address1,isnull(ol.Address2,'') Address2,isnull(ol.Address3,'') Address3,isnull(CM.CityName,'') City, isnull(SM.StateName,'') State,isnull(CnM.CountryName,'') Country,
			(select isnull(sum(COP.PaymentAmount),0) from COPayment COP where cop.CustomerOrderNo IN
			(select COH.customerOrderNo from coheader COH where COH.LogNo = OL.LogNo
			AND COH.Status = 3 AND COH.BOId = $boid AND (isnull('$pcid','') = '' OR COH.PCId = '$pcid')) AND
			COP.TenderType = 6) LogOrderAmount from OrderLog OL with (NOLOCK)
			LEFT JOIN City_Master CM with (NOLOCK) ON CM.CityId = OL.CityCode LEFT JOIN State_Master SM with (NOLOCK) ON SM.StateId = OL.StateCode
			LEFT JOIN Country_Master CnM with (NOLOCK) ON CnM.CountryId = OL.CountryCode
			where OL.BOId = $boid and (isnull('$pcid','')='' OR OL.PCId = '$pcid') and OL.LogType = $log_type and OL.status = $status AND (isnull('$distributor_number','') = '' OR OL.Distributorid = '$distributor_number')
			AND (isnull('$orderMode','')='' OR ol.isChangeAddress='$orderMode') order by CreatedDate desc";
			//$sql = "exec usp_OrderLogSearch $status,$boid,NULL,$log_type,$distributor_number,$pcid,'outparam'";
				
				
			$stmt = $pdo_object->prepare($sql);
				
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$getLogTeamOrderInformationData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $getLogTeamOrderInformationData;
		}
		catch(Exception $e)
		{
			$getLogTeamOrderInformationData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $getLogTeamOrderInformationData;
		}
	
	
	}
	
	function payMultipleOrders($listOfOrders,$payAbleOrderList){
		
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		
		$outParam='';
		$sql = "{CALL sp_payMultipleOrders(@listOfOrders=:listOfOrders,@payableOrderList=:payableOrderList,@outParam=:outParam)}";
		$stmt = $pdo_object->prepare($sql);
		$stmt->bindParam(':listOfOrders',$listOfOrders);
		$stmt->bindParam(':payableOrderList',$payAbleOrderList);
		$stmt->bindParam(':outParam', $outParam);
			
		$stmt->execute();
		$results = $stmt->fetchAll();
		
		if($results[0]['OutParam'] == "DUP0001")
		{
			throw new vestigeException("Payment for order ".$results[0]['DuplicateOrderNo']." cancelled on ".date("d-M-Y H:i:s", strtotime($results[0]['InitiatedDateTime'])).". Contact your branch.");
		}
		if($results[0]['OutParam'] == "INF001")
		{
			throw new vestigeException("Nothing to pay");
		}
		else if(sizeof($results[0]['OutParam']) > 0)
		{
		    throw new vestigeException($results[0]['OutParam']);
		}
	
		
		return $results;
	}
	
	function payMultipleLogs($logNo){
		
		
		/* $file = fopen("D://History234.txt", "w");
		fwrite($file,$logList);
		fclose($file); */
		/*
		 *///$courierLogList = array();
		/* if(sizeof($availableLogs) > 1){
			throw new Exception("More than one log detected. Hence can't proceed");
		} */
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		
		$outParam='';
		//$logNo = $availableLogs[0];
			
		$sql = "{CALL CourierChargesDetailForLog(@logNo=:logNo,@outParam=:outParam)}";
		$stmt = $pdo_object->prepare($sql);
		$stmt->bindParam(':logNo',$logNo);
		$stmt->bindParam(':outParam', $outParam);

		$stmt->execute();
		$results = $stmt->fetchAll();
			
		
		return $results;
	}
	
	function receivePayment($orderNo,$withCourierCharges,$loggedInUserId){
		
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		
		$outParam='';
		$sql = "{CALL sp_receivePayment(@orderNo=:orderNo,@withCourierCharges=:withCourierCharges,@receivedBy=:receivedBy,@outParam=:outParam)}";
		$stmt = $pdo_object->prepare($sql);
		$stmt->bindParam(':orderNo',$orderNo );
		$stmt->bindParam(':withCourierCharges',$withCourierCharges );
		$stmt->bindParam(':receivedBy', $loggedInUserId);
		$stmt->bindParam(':outParam', $outParam);
			
		$stmt->execute();
		$results = $stmt->fetchAll();
		
		if($results[0]['OutParam'] == "INF0051")
		{
			throw new vestigeException("Payment already received by ".$results[0]['ReceivedBy'],1002);
		} 	
		if($results[0]['OutParam'] == "INF0052")
		{
			throw new vestigeException("Payment can't be received for order having payment only bonus.",1003);
		}
		
		return $results;
	}
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Save order.
	 */
	 
	// Decision to move distributor on payment gateway page will be decided using $loggedInUserType and $payment json.
	//No need to send additional parameters.
	function saveOrder($form_data,$payment_json,$order_json,$posOrderNumber,$locationId,$boid,$pcid,$createdBy,$loggedInUserType,$terminalCode,
	$changedBoid,$exportInvoiceMain,$source)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			parse_str($form_data, $output);
			$debitOrCreditCardAmount = 0;
			$paymentGivenByCreditCard = 0;
			$payment_json_Array = json_decode($payment_json);
			
			foreach($payment_json_Array as $paymentArrayObj){
				if($paymentArrayObj->TenderType == 2){
					$debitOrCreditCardAmount = $paymentArrayObj->PaymentAmount;
					$paymentGivenByCreditCard = 1;
					break;
				}
			}
			
			$jsonForItems = $order_json;
			
			
			$jsonForPayments = $payment_json;
			
			$orderId = $posOrderNumber;
			
			//Since logic has been changed hence no need to redirect second time on payment gateway page. As second time no need to move on 
			//payment gateway page.
			
			/* if($orderId != '' && $creditCardOrderOrNot == 1){
				$sendPerformReq =  new SendPerformRequest();
				$paymentGAtewayUrl = $sendPerformReq->sendRequestToPG($orderId,$debitOrCreditCardAmount);
				$saveOrderData[0]['PaymentGateWayUrl'] = $paymentGAtewayUrl;
				return $saveOrderData;
			} */
			
			$locationId = $locationId;
			$stockPoint = $output['stock_id'];
			$PUCOrderMode = 0; //Taken 0 here to run functionality of mobile pos.
			$otherChargesType = $output['OtherChargeType'];
			
			$distributorId = $output['DistributorId'];
			$stock_location_code = $output['stock_location_code'];
			$totalUnit = $output['TotalQty'];
			$totalWeight = 0.00;
			$orderAmount =  $output['order_amount'];
			$discountAmount = 0.00;
			$taxAmount = 0.00;
			$paymentAmount = $output['payment_amount'];
			$changeAmount =  $output['changeAmount'];
			
			
			$Remarks =  $output['POSRemarks'];
			$orderStartTime = $_SESSION['Order_Start_Time'];
			
			$logNo =  $output['log_or_team_order'];
			
			if($boid == '' && $pcid == '') //case when user login. bcoz pcid and boid made empty in module file.
			{
				$pcId = $output['pc_id'];
				//$boId = $output['bo_id'];
				$boId = $locationId;
				
				
				/* if($pcId==$boId && $logNo != '')
				{
					throw new vestigeException("Some problem in pick up centre selection. Please, refresh page or select again.");
				} */
				
			}
			else
			{
				$pcId = $pcid;
				$boId = $boid;
			}
			
			
			
			$orderType = $output['OrderType']; //2 reorder,1 first order
			$orderMode = $output['delivery_mode'];;//1.self,2 courier
			//$terminalCode = $terminalCode;
			$totalPv = $output['total_pv'];
			$totalBv = $output['total_bv'];
			$sourceLocationId = $locationId;
			$taxJurisdictionId = $output['location_id'];
			$mode = $output['mode'];
			
			
			if($logNo == '')
			{
				$logNo = null;
			}
			else
			{
				
			}

			if($pcId == $boId)
			{
				$pcId = 0;
			}
				
				
			$addressId = $output['AddressId'];
			$courierCharges = $output['CourierCharges'];
			$orderDispatchMode=$output['OrderDispatchMode'];
			if($orderMode == 2 && $orderDispatchMode == 0)
			{
				$orderDispatchMode = 2;
			}
			
			if($changedBoid != null && $orderMode == 1 && $loggedInUserType == 'D' ){
				$locationId = $changedBoid;
				$boId = $changedBoid;
				$pcId = $changedBoid;
			}
			
			
			if(intval($debitOrCreditCardAmount) == 0 && $paymentGivenByCreditCard == 1){
				throw new vestigeException("Order amount for credit card can't be 0");
			}
			
			
			if($source == "mobile"){
				$Remarks = "Mobile order";
			}
			
			if($source == "web")
			{
				$Remarks = "Web order";
			}
			
			
			$exportInvoiceToConsider = isset($exportInvoiceMain) ? 1:0;
			
			/* $file = fopen("D://checkingFile_POSBusinessClass123.txt", "w");
			fwrite($file, $jsonForItems.','.$jsonForPayments.','.$courierCharges.','.$locationId.','.$stockPoint.','.$distributorId.','.$totalUnit
			.','.$totalWeight.','.$orderAmount.','.$discountAmount.','.$taxAmount.','.$paymentAmount.','.$changeAmount.','.$pcId.','.$boId.','.$orderType.','
			.$orderMode.','.$terminalCode.','.$totalBv.','.$totalPv.','.$sourceLocationId.','.$taxJurisdictionId.','.$logNo.','.$createdBy.','.$addressId.','
			.$orderDispatchMode.','.$orderId.','.$Remarks.','.$loggedInUserType.','.$outparam);
			fclose($file); */

			
			
			$sql = "{CALL SP_orderSave (@JsonForItems=:jsonForOrder,@jsonForPayment=:jsonForPayments,@courierCharges=:courierCharges,@otherChargesType=:otherChargesType,@locationId=:locationId,@stockpoint=:stockPoint,@distributorId=:distributorId,
    @TotalUnit=:totalUnit,@PUCOrderMode=:pucOrderMode,@TotalWeight=:totalWeight,@OrderAmount=:orderAmount,@CreditCardPaymentOrNot=:creditCardPaymentOrNot,
    @DiscountAmount=:discountAmount,@TaxAmount=:taxAmount,@PaymentAmount=:paymentAmount,@ChangeAmount=:changeAmount,@PCId=:pcId,@BOId=:boId,@OrderType=:orderType,@OrderMode=:orderMode,@TerminalCode=:terminalCode,@TotalBv=:totalBv,@TotalPv=:totalPv,@SourceLocationId=:sourceLocationId,@TaxJurisdictionId=:taxJurisdictionId,
    @LogNo=:logNo,@createdBy=:createdBy,@addressId=:addressId,@orderDispatchMode=:orderDispatchMode,@orderId=:orderId,@Remarks=:Remarks,@TypeOfUserLoggedIn=:TypeOfUserLoggedIn,@OrderStartTime=:orderStartTime,@exportInvoiceToConsider=:exportInvoiceToConsider,@outParam=:outParam)}";
	
	
	
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':jsonForOrder',$jsonForItems, PDO::PARAM_STR);
			$stmt->bindParam(':jsonForPayments',$jsonForPayments,PDO::PARAM_INT);
			$stmt->bindParam(':courierCharges',$courierCharges,PDO::PARAM_INT);
			$stmt->bindParam(':otherChargesType',$otherChargesType,PDO::PARAM_INT);
			$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
			$stmt->bindParam(':stockPoint',$stockPoint,PDO::PARAM_INT);
			$stmt->bindParam(':distributorId',$distributorId,PDO::PARAM_INT);
			$stmt->bindParam(':totalUnit',$totalUnit,PDO::PARAM_INT);
			$stmt->bindParam(':pucOrderMode',$PUCOrderMode,PDO::PARAM_INT);
			$stmt->bindParam(':totalWeight',$totalWeight,PDO::PARAM_INT);
			$stmt->bindParam(':orderAmount',$orderAmount,PDO::PARAM_INT);
			$stmt->bindParam(':creditCardPaymentOrNot',$creditCardPaymentOrNot,PDO::PARAM_INT);
			$stmt->bindParam(':discountAmount',$discountAmount,PDO::PARAM_INT);
			$stmt->bindParam(':taxAmount',$taxAmount,PDO::PARAM_INT);
			$stmt->bindParam(':paymentAmount',$paymentAmount,PDO::PARAM_INT);
			$stmt->bindParam(':changeAmount',$changeAmount,PDO::PARAM_INT);
			$stmt->bindParam(':pcId',$pcId,PDO::PARAM_INT);
			$stmt->bindParam(':boId',$boId,PDO::PARAM_INT);
			$stmt->bindParam(':orderType',$orderType,PDO::PARAM_INT);
			$stmt->bindParam(':orderMode',$orderMode,PDO::PARAM_INT);
			$stmt->bindParam(':terminalCode',$terminalCode,PDO::PARAM_STR);
			$stmt->bindParam(':totalBv',$totalBv,PDO::PARAM_INT);
			$stmt->bindParam(':totalPv',$totalPv,PDO::PARAM_INT);
			$stmt->bindParam(':sourceLocationId',$sourceLocationId,PDO::PARAM_INT);
			$stmt->bindParam(':taxJurisdictionId',$taxJurisdictionId,PDO::PARAM_INT);
			$stmt->bindParam(':logNo',$logNo, PDO::PARAM_STR);
			$stmt->bindParam(':createdBy',$createdBy,PDO::PARAM_INT);
			$stmt->bindParam(':addressId',$addressId,PDO::PARAM_INT);
			$stmt->bindParam(':orderDispatchMode',$orderDispatchMode,PDO::PARAM_INT);
			$stmt->bindParam(':orderId', $orderId, PDO::PARAM_STR);
			$stmt->bindParam(':Remarks', $Remarks, PDO::PARAM_STR);
			$stmt->bindParam(':TypeOfUserLoggedIn', $loggedInUserType, PDO::PARAM_STR);
			$stmt->bindParam(':orderStartTime', $orderStartTime, PDO::PARAM_STR);
			$stmt->bindParam(':exportInvoiceToConsider', $exportInvoiceToConsider, PDO::PARAM_INT);
			$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
			
			
			//$stmt->bindParam(':retval', $retval, PDO::PARAM_INT|PDO::PARAM_INPUT_OUTPUT, 4);
			//$stmt->bindParam('userID', $userID, PDO::PARAM_INT);
			//$stmt->bindParam('userEmail', $userEmail, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 50);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(sizeof($results) > 0 && $loggedInUserType == 'D'){

				$results[0]['CreatedBy'] = $terminalCode;

			}
			
			/* $file = fopen("D://checkingFile_POSBusinessClass.txt", "w");
			fwrite($file, json_encode($results).','.$results[0]['OutParam']);
			fclose($file); */
			
			if($results[0]['OutParam'] == "INF0041")
			{
				
				throw new vestigeException("Gift voucher have been used",1002);
				
			}
			else if($results[0]['OutParam'] == "VAL0511")
			{
			
				throw new vestigeException("Order amount should be greater than minimum purchase amount",1003);
				
			}
			
			else if($results[0]['OutParam'] == "INF0042"){
				throw new Exception($results[0]['PromotionName']." is not available on this location");
			}
			
			else if($results[0]['OutParam'] == "EXP001")
			{
					
				throw new vestigeException("Promotion expired",1009);
					
			}
			else if($results[0]['OutParam'] == "EXP002")
			{
					
				throw new vestigeException("Joining promotions can't be given for repurchase",1010);
					
			}
			else if($results[0]['OutParam'] == "INF0048"){
				$assignedUrl = $this->vestigeUtil->getSessionData("AssignedUrl");
				$results[0]['AssignedUrl'] = $assignedUrl;
				
				
				$saveOrderData = $results;
			}
			else if($results[0]['OutParam'] == "INV001")
			{
					
				$saveOrderData = $results;
			
			}
			else if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
			
			else
			{
				$saveOrderData = $results;
				
				if($debitOrCreditCardAmount != 0 && $loggedInUserType == 'D' && $creditCardPaymentOrNot == 1){
					$sendPerformReq =  new SendPerformReQuest();
					$paymentGAtewayUrl = $sendPerformReq->sendRequestToPG($saveOrderData[0]['CustomerOrderNo'],$debitOrCreditCardAmount);
					$saveOrderData[0]['PaymentGateWayUrl'] = $paymentGAtewayUrl;
				}
				
				if($saveOrderData[0]['PaidOrder'] == 1 && $loggedInUserType == 'D'){
					POSBusinessClass :: invoiceOrder('', $saveOrderData[0]['CustomerOrderNo'], $createdBy, $saveOrderData[0]['LocationId']);
				}
			}

			
			
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
	
		return $saveOrderData;
	
	}
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 *Cancel order corresponding to one order no that has been generated creating order.
	 */
	function cancelOrder($form_data,$orderNo,$createdBy,$current_login_user_id) 
	{
	
		try{
			
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$statusParam = 2;
			parse_str($form_data, $output);
			$locationId = $output['location_id'];
			$distributorId = $output['DistributorId'];
			$cretedByInt=$createdBy;
			

			$outparam = '';
			
			
			//$sql = "{CALL SP_CancelOrder (:orderNo,:statusParam,:distributorId,:locationId,:createdBy)}";
			$sql = "{CALL SP_CancelOrder (@orderNo=:orderNo,@statusParam=:statusParam,@distributorId=:distributorId,
					@locationId=:locationId,@createdBy=:createdBy,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':orderNo',$orderNo, PDO::PARAM_STR);
			$stmt->bindParam(':statusParam',$statusParam,PDO::PARAM_INT);
			$stmt->bindParam(':distributorId',$distributorId,PDO::PARAM_INT);
			$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
			$stmt->bindParam(':createdBy',$cretedByInt,PDO::PARAM_INT);
			$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);

	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(sizeof($results) > 0 && $loggedInUserType == 'D'){
				$results[0]['CreatedBy'] = $current_login_user_id;
			}
			
			if(sizeof($results) ==0)
			{
				throw new vestigeException("Empty result set",1007);
			}
			
			else if($results[0]['OutParam'] == "NOCAN")
			{
				throw new vestigeException("Only confirmed order can be cancelled",1005);
			}
			else if($results[0]['OutParam'] == "NOMOD")
			{
				throw new vestigeException("Cancelled order can't be modified",1006);
			}
			
			else if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
			else
			{
				$cancelOrderData = $results;
			}
			
	
			
		}
		catch (PDOException $e) {
			
			throw new Exception($e);
	
		}
		
		return $cancelOrderData;
	
	}
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Invoice an order that generated before.
	 */
	function invoiceOrder($form_data,$orderNo,$modifiedBy,$locationId)
	{
	
		$VestigeUtil = new VestigeUtil(); 
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$outParam = '';
			
			/*
			 * Case  saved successfully but invoice not being done at that time and if user again want to invoice order then two possible 
			 * cases are possible
			 * 1. PG tables are not updated successfully then need to redirect him to payment gateway page again.
			 * 2. PG tables updated successfully then need to do direct invoice of that order.
			 */
			
			//Case 1 when order is of debit or credit card and PG tables not updated successfully.
			$sql = "select cop.CustomerOrderNo,opt.TransactionPaymentId,SUM(cop.PaymentAmount) PaymentAmount from COPayment cop left join
 			OnlinePayGOrderTrack OPT  on cop.CustomerOrderNo =  opt.TrackId where customerorderno = '$orderNo'  
			and cop.TenderType = 2 group by cop.CustomerOrderNo,opt.TransactionPaymentId";
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			//parse_str($form_data, $output);
			
			//$distributorId = $output['DistributorId'];
			
			$sql = "{CALL SP_InvoiceSave (@orderNo=:orderNo,@locationId=:locationId,@outparam=:outParam,@ModifiedBy=:modifiedBy)}";
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':orderNo',$orderNo, PDO::PARAM_STR);
			$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 50);
			$stmt->bindParam(':modifiedBy',$modifiedBy,PDO::PARAM_INT);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(isset($results[0]['OutParam']) && $results[0]['OutParam'] == "UnPaid_Order")
			{
				throw new vestigeException("Paid orders can be invoiced only. First receive payment for the order",10010);
			}
			else if(isset($results[0]['OutParam']) && $results[0]['OutParam'] == "40008")
			{
				throw new vestigeException("Quantity not available",1004);
			}
			else if(isset($results[0]['OutParam']) && $results[0]['OutParam'] == "40013")
			{
				throw new vestigeException("Order already invoiced",1005);
			}
			else if(isset($results[0]['OutParam']) && $results[0]['OutParam'] == "INV001")
			{
					
				$invoiceOrderData = $results;
				$invoiceOrderData[0]['PGRedirectStatus'] = 0;
			
			}
			else if(isset($results[0]['OutParam']) && $results[0]['OutParam'] == "INF0145")
			{
					
				$invoiceOrderData = $results;
				$invoiceOrderData[0]['PGRedirectStatus'] = 0;
					
			}
			else if(isset($results[0]['OutParam']) && sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
			else
			{
				$invoiceOrderData = $results;
				$invoiceOrderData[0]['PGRedirectStatus'] = 0;
				
			}
			
		}
		catch (Exception $e) {
			
			throw new Exception($e);
	
		}
	
		return $invoiceOrderData;
		
	}

	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * COD receive payment.
	*/
	function CODPaymentReceive($CODPaymentInvoiceNo,$CODReceiveComment, $loggedInUserType, $modifiedBy)
	{
		
		try{
			
			//Code to update table. //CODPayments
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$sql = "UPDATE CODPayments set PaymentReceivedBy=$modifiedBy,Comment='$CODReceiveComment',IsPaymentReceived = 1 OUTPUT 1 'UpdateStatus' where InvoiceNo = '$CODPaymentInvoiceNo'";
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				 
		}
		catch (Exception $e) {
				
			throw new Exception($e);
	
		}
	
		return $results;
	
	}
	
	/**
	 * 
	 * @param unknown $distributorId - Distributor to get registered to see DBR report.
	 */
	function requestDBRReport($distributorId,$requestedBy,$locationId,$servedStatus){
		try{

			$outparam = '';
			
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			//$sql = "{CALL SP_CancelOrder (:orderNo,:statusParam,:distributorId,:locationId,:createdBy)}";
			$sql = "{CALL sp_requestDBRReport (@distributorId=:distributorId,@locationId=:locationId,@requestedBy=:requestedBy,@servedStatus=:servedStatus,
					@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':distributorId',$distributorId, PDO::PARAM_INT);
			$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
			$stmt->bindParam(':requestedBy',$requestedBy,PDO::PARAM_INT	);
			$stmt->bindParam(':servedStatus',$servedStatus,PDO::PARAM_INT);
			$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);

	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(sizeof($results) ==0)
			{
				throw new vestigeException("Problem in registering DBR Report request",1007);
			}
			else if($results[0]['OutParam'] == 'INV001'){
				throw new vestigeException("DBR Request already registered for distributor ".$distributorId);
			}
			else if($results[0]['OutParam'] == 'INV002'){
				throw new vestigeException("Distributor -".$distributorId." not registered with any email address to send DBR report");
			}
			else if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
				
		
		}
		catch (PDOException $e) {
			
			throw new Exception($e);
			//$DRBRequestData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
		}
		
		return $results;
		
	}
	
	
	function checkAdderssTokeepSameForLog($currentLogSelected)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("select TOP 1 COH.CreatedDate,COH.DeliverFromAddress1 + ',' +
					COH.DelverFromAddress2 + ',' +  COH.DeliverFromAddress3 + ',' + isnull(COH.DeliverFromAddress4,'')
					as LogAddress
					, ISNULL(COH.OrderMode,0) OrderMode,ISNULL(COH.AddressId,0) AddressId,ISNULL(COH.OrderDispatchMode,0)
					OrderDispatchMode
					FROM OrderLog OL with (NOLOCK) LEFT JOIN COHeader COH with (NOLOCK) ON
					COH.LogNo=OL.LogNo
					Where OL.LogNo='$currentLogSelected'
					Order By COH.CreatedDate ASC");

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(sizeof($results) == 0)
			{
				
			}
			
			
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
		
		return $results;
	}
	
	function qtyToShowOrNotForItems($locationId)
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("select Case when (select COUNT(*) from StockCountHeader where LocationId=$locationId and Status in (1,3))>0
					then 0 else keycode1 end as 'QtyToShowOrNotForItems'
					from Parameter_Master with (NOLOCK) where ParameterCode = 'POSDISPLAYITEMDESC'");
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
			if(sizeof($results) == 0)
			{
				throw new vestigeException("Application Error",1006);
			}
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
	
		return $results;
	}
	
	/**
	 * function provide courier details.
	 */
	
	function loadCourierDetails()
	{
		try
		{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$stmt = $pdo_object->prepare("SELECT PM1.keycode1 as 'CourierChargeCondId',PM1.KeyValue1+'-INR '+PM1.KeyValue3 as 'CourierChargeCondValue',
					PM1.KeyValue2 as 'MinAmountToSaveCourierCharges',PM2.KeyValue1 as 'CourierCharges',PM1.KeyValue3 as 'CODCharges',
					PM3.KeyValue1 as 'OrderMaxAmount'
					from parameter_master PM1 with (NOLOCK),Parameter_Master PM2 with (NOLOCK),Parameter_Master PM3 with (NOLOCK) where PM1.ParameterCode = 'COURIERCHARGES' and
					PM2.ParameterCode = 'MINCOURIERCHARGES' and PM3.ParameterCode = 'OrderMaxAmount'");
		
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
			if(sizeof($results) == 0)
			{
				throw new vestigeException("Application Error",1006);
			}
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
		
		return $results;
	}
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide information about one bonus check for one distributor.
	*/
	function getBonusCheckInfo($bonusChequeNo)
	{
	
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$outParam = '';
			$sql = "{CALL usp_DistributorBonusChequeSearch (@ChequeNo=:ChequeNo,@outparam=:outParam)}";
			
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':ChequeNo',$bonusChequeNo, PDO::PARAM_STR);
			
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 50);
			
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$sBonusCheckExpiryDate = $results[0]['ExpiryDate']; //s for String date.
			$dBonusCheckExpiryDate = new DateTime($sBonusCheckExpiryDate); //d for DATE Object.
			$systemDate = new DateTime('now');
			
			if(sizeof($results) == 0)
			{
				$bonusChequeData = $this->vestigeUtil->formatJSONResult("null", '','bonusChequeInfo');
				
				return $bonusChequeData;
			
			}
			
			if($dBonusCheckExpiryDate > $systemDate)
			{
				$bonusChequeData = $this->vestigeUtil->formatJSONResult(json_encode($results), '','bonusChequeInfo');
				
				return $bonusChequeData;
				
			}
			if($dBonusCheckExpiryDate < $systemDate)
			{
				
				$bonusChequeData = $this->vestigeUtil->formatJSONResult("\"BonusChequeExpired\"", '','bonusChequeInfo');
				
				return $bonusChequeData;
				
			}
			
	
			
		}
		catch (PDOException $e) {
			
			$bonusChequeData = $this->vestigeUtil->formatJSONResult('', $e->getMessage(),'bonusChequeInfo');
			
			return $bonusChequeData;
	
		}
	
	}
	
	
	function getGiftVoucherInfo($giftChequeNo,$distributorId)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
	
			$sql = "SELECT GD.[GiftVoucherCode]
			,GD.[VoucherSrNo]
			,GD.[SeriesId]
			,GD.[IssuedTo]
			,GD.[IssueDate]
			,GD.[AvailedDate]
			,GD.[ApplicableFrom]
			,GD.[ApplicableTo]
			,GD.[Availed]
			,GD.[ModifiedBy]
			,GD.[ModifiedDate]
			,GV.[Name][VoucherName]
			,GV.[Description][VoucherDescription]
			,GV.[MinBuyAmount],
			GVID.ItemId,GVID.Quantity,
			IM.ItemName,IM.DistributorPrice,IM.MerchHierarchyDetailId,IM.PromotionParticipation
			FROM [GiftVoucher_Distributor_Link]GD with (NOLOCK)
			JOIN [GiftVoucherHeader]GV with (NOLOCK)
			ON GD.[GiftVoucherCode]=GV.[GiftVoucherCode]
			left join GiftVoucherItemDetail GVID on
			GD.GiftVoucherCode = GVID.GiftVoucherCode
			left join Item_Master IM on
			GVID.ItemId = IM.ItemId
			Where (ISNULL('$giftChequeNo','')='' OR GD.[VoucherSrNo]='$giftChequeNo')
			AND (ISNULL($distributorId,-1)=-1 OR $distributorId=0 OR [IssuedTo]=$distributorId)
			AND (ISNULL(0,-1)=-1 OR [Availed]=0)
			AND CONVERT(VARCHAR(10),ApplicableTo,112)>=CONVERT(VARCHAR(10),GetDate(),112)
			ORDER BY  GV.[GiftVoucherCode]";
			
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
			$giftVoucherData = $this->vestigeUtil->formatJSONResult(json_encode($results), '','giftVouchers');
	
	
	
		}
		catch (PDOException $e) {
			
		$giftVoucherData = $this->vestigeUtil->formatJSONResult('',$e->getMessage(),'giftVouchers');
	
		//return $giftVoucherData;
	
		}
	
		return $giftVoucherData;
	}
	
	/*-----------------------------------------------------------------------------------------------------------*/
	/*
	 * Provide information about one bonus check for one distributor.
	*/
	function getGiftVouchers($distributorId)
	{
	
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$sql = "SELECT GD.[GiftVoucherCode]
					,GD.[VoucherSrNo]
					,GD.[SeriesId]
					,GD.[IssuedTo]
					,GD.[IssueDate]
					,GD.[AvailedDate]
					,GD.[ApplicableFrom]
					,GD.[ApplicableTo]
					,GD.[Availed]
					,GD.[ModifiedBy]
					,GD.[ModifiedDate]
					,GV.[Name][VoucherName]
					,GV.[Description][VoucherDescription]
					,GV.[MinBuyAmount]
					FROM [GiftVoucher_Distributor_Link]GD with (NOLOCK)
					JOIN [GiftVoucherHeader]GV with (NOLOCK)
					ON GD.[GiftVoucherCode]=GV.[GiftVoucherCode]
					Where (ISNULL('','')='' OR GV.[GiftVoucherCode]='')
					AND (ISNULL($distributorId,-1)=-1 OR $distributorId=0 OR [IssuedTo]=$distributorId)
					AND (ISNULL(0,-1)=-1 OR [Availed]=0)
					AND CONVERT(VARCHAR(10),ApplicableTo,112)>=CONVERT(VARCHAR(10),GetDate(),112)
					ORDER BY  GV.[GiftVoucherCode]";
				
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$giftVoucherData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $giftVoucherData;
				
		}
		catch (PDOException $e) {
			
			$giftVoucherData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $giftVoucherData;
	
		}
	
	}
	
	
	function getGiftInformation($distributorId)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();

			$sql = "SELECT GD.[GiftVoucherCode]
					,GD.[VoucherSrNo]
					,GD.[SeriesId]
					,GD.[IssuedTo]
					,GD.[IssueDate]
					,GD.[AvailedDate]
					,GD.[ApplicableFrom]
					,GD.[ApplicableTo]
					,GD.[Availed]
					,GD.[ModifiedBy]
					,GD.[ModifiedDate]
					,GV.[Name][VoucherName]
					,GV.[Description][VoucherDescription]
					,GV.[MinBuyAmount],
					GVID.ItemId,GVID.Quantity,
					IM.ItemName,IM.DistributorPrice,IM.MerchHierarchyDetailId,IM.PromotionParticipation
					FROM [GiftVoucher_Distributor_Link]GD with (NOLOCK)
					JOIN [GiftVoucherHeader]GV with (NOLOCK)
					ON GD.[GiftVoucherCode]=GV.[GiftVoucherCode]
					left join GiftVoucherItemDetail GVID on
					GD.GiftVoucherCode = GVID.GiftVoucherCode
					left join Item_Master IM on
					GVID.ItemId = IM.ItemId
					Where (ISNULL('','')='' OR GV.[GiftVoucherCode]='')
					AND (ISNULL($distributorId,-1)=-1 OR $distributorId=0 OR [IssuedTo]=$distributorId)
					AND (ISNULL(0,-1)=-1 OR [Availed]=0)
					AND CONVERT(VARCHAR(10),ApplicableTo,112)>=CONVERT(VARCHAR(10),GetDate(),112)
					ORDER BY  GV.[GiftVoucherCode]";

			$stmt = $pdo_object->prepare($sql);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$giftVoucherData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');

			return $giftVoucherData;

		}
		catch (PDOException $e) {
			
		$giftVoucherData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
		
		return $giftVoucherData;
		
		}
	}
	
	function getGiftVoucherItems($giftVoucherCode)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
				
		
			$giftVoucherItemInfo = POSBusinessClass :: getGiftVoucherItemInfo($giftVoucherCode);
			
			$giftVoucherItemId = $giftVoucherItemInfo[0]['ItemId'];
			
			$giftVoucherAmount = $giftVoucherItemInfo[0]['Quantity'];
			
		
			$sql = "SELECT [ItemId]
					,[ItemCode]
					,[ItemName]
					,[ShortName]
					,[PrintName]
					,[ReceiptName]
					,[DisplayName]
					,[MerchHierarchyDetailId]
					,[PrimaryCost]
					,[DistributorPrice]
					,[BusinessVolume]
					,[PointValue]
					,[PromotionParticipation]
					,[IsKit]
					,[IsAvailableforGift]
					,[MinKitValue]
					,[TaxCategoryId]
					,[IsComposite]
					,[Height]
					,[Width]
					,[Length]
					,[Weight]
					,[StackLimit]
					,[BayNumber]
					,[ExpDuration]
					,[Status]
					,[CreatedBy]
					,[CreatedDate]
					,[ModifiedBy]
					,[ModifiedDate]
					,[TransferPrice]
					,[LandedPrice]
					,[IsForRegistrationPurpose]
					,[ItemTypeID]
					,[ItemPackSize]
					,[ExpiryDateFormat]
					,$giftVoucherAmount as 'Quantity'
					FROM Item_Master with (NOLOCK) where ItemId = $giftVoucherItemId";
		
			$stmt = $pdo_object->prepare($sql);
		
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$giftVoucherItemData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $giftVoucherItemData;
		
		}
		catch (Exception $e) {
		
			$giftVoucherItemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $giftVoucherItemData;
		
		}
		
	}
	
	function getGiftVoucherItemInfo($giftVoucherCode)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
		
			$sql = "SELECT  ItemId,Quantity	FROM GiftVoucherItemDetail with (NOLOCK) where GiftVoucherCode = '$giftVoucherCode'";
		
			
			$stmt = $pdo_object->prepare($sql);
		
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			return $results;
		
		}
		catch (PDOException $e) {
			
			throw new Exception($e);
			
		}
	}
	
	
	function fetchPreviousDeliveryAddress($distributorId)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
	
	
			$sql = "select * from DistributorAddress with (NOLOCK) where distributorId = $distributorId";
			
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$fetchPreviousDeliveryAddress = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $fetchPreviousDeliveryAddress;
	
		}
		catch (Exception $e) {
			
			$fetchPreviousDeliveryAddress = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $fetchPreviousDeliveryAddress;
	
		}
	}
	
	function getYBV($distributorId,$month)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
	
	
			$sql = " 
      SELECT  [bvm]
      ,[arn]
      ,[ebp]
      ,[sbp]
      ,[gbp]
      ,[bp]
      ,[sbv]
      ,[gbv]
      ,[bv]
      ,[pcent]
      ,[gb]
      ,[pb]
      ,[lb]
      ,[db]
      ,[ccm]
      ,[tf]
      ,[cf]
      ,[hf]
      ,[sb]
      ,[pybl]
      ,[tdsrate]
      ,[tds]
      ,[balcf]
      ,[hold]
      ,[paid]
      ,[netpay]
      ,[done]
      ,[cqno]
      ,[chqdate]
      ,[ttl]
      ,[fn]
      ,[sn]
      ,[self]
      ,[nofa]
      ,[qual]
      ,[onacded]
      ,[tfded]
      ,[advded]
      ,[actpay]
      ,[bnk] FROM ybv(nolock) where ybv.arn=$distributorId and ybv.bvm='$month'
     
      order by pybl
      
      
      ";
				
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$fetchPreviousDeliveryAddress = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $fetchPreviousDeliveryAddress;
	
		}
		catch (Exception $e) {
				
			$fetchPreviousDeliveryAddress = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $fetchPreviousDeliveryAddress;
	
		}
	}
	
	//check payment amount for puc in PUC Account.
	function checkAvailabeAmountInPUCAccount($PUCId)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
		
			$sql = "select PA.AvailableAmount-sum(PTD.TransAmount) as 'AvailableAmount' from PUCAccount PA with (NOLOCK), PUCTransactionDetail  
					PTD with (NOLOCK) where PA.PCId = $PUCId and PTD.PCId = $PUCId group by PA.AvailableAmount";
				
			$stmt = $pdo_object->prepare($sql);
		
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results[0]['AvailableAmount'] <= 0 )
			{
				throw new vestigeException("Your pick up centre have not that much amount for this order",1004);
			}
			
		
		}
		catch (Exception $e) {
				
			throw new Exception($e);
		
		}
		
		return $results;
	}
	
	function storeDeliveryAddress($deliveryAddressFormData,$distributorId,$pcid,$loggedInUser
,$orderMode,$loggedInUserType,$locationId)
	{
		
		try{
			parse_str($deliveryAddressFormData, $output);
			//echo $output['first'];  // value
			//echo $output['arr'][0]; // foo bar
			//echo $output['arr'][1];
			$getAddress1 = $output['GetAddressAddress1'];
			
			$getAddress2 = $output['GetAddressAddress2'];
			$getAddress3 = $output['GetAddressAddress3'];
			$getAddress4 = $output['GetAddressAddress4'];
		
			
			$getCountry = $output['GetAddressCountry'];
			$getState = $output['GetAddressState'];
			$getcity = $output['GetAddressCity'];
			$getPincode = $output['GetAddressPinCode'];
			$getPhone1 = $output['GetAddressPhone1'];
			$getPhone2 = $output['getAddressPhone2'];
			$getMobile1 = $output['GetAddressMobile1'];
			$getMobile2 = $output['GetAddressMobile2'];
			$getEmail1 = $output['GetAddressEmail1'];
			$getEmail2= $output['GetAddressEmail2'];
			$getFax1 = $output['GetAddressFax1'];
			$getFax2 = $output['getAddressFax2'];
			$getWebsite = $output['GetAdderssWebsite'];
		
			
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			$sql="insert into DistributorAddress(DistributorId,Address1,Address2,Address3,Address4,
					Pincode,StateCode,CityCode,CountryCode,Email1,Email2,Mobile1,Mobile2,Phone1,Phone2,
					Fax1,Fax2,Website)  output inserted.AddressId values($distributorId,'$getAddress1','$getAddress2','$getAddress3','$getAddress4'
					,'$getPincode','$getState','$getcity','$getCountry','$getEmail1','$getEmail2','$getMobile1','$getMobile2','$getPhone1','$getPhone2'
					,'$getFax1','$getFax2','$getWebsite')";
			
			
			$stmt = $pdo_object->prepare($sql);
				
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($loggedInUserType == 'D'){
				$pcid = $locationId;
				$logType = 2;
			}
			else if($loggedInUserType == 'P'){
				$distributorId = 0;
				$logType = 1;
			}
			else{
				return $results; //if not distributor or puc then retun directly from there.
			}
			
			$outParam = '';
			
			/* 
			$file = fopen("D://dddddd.txt", "w");
			fwrite($file, $distributorId .','.$pcid.','.$getAddress1.','.$getAddress2.','.$getAddress3.','.$getCountry.','.$getState.','.
					$getcity.','.$getPincode.','.$getPhone1.','.$getPhone2.','.$getMobile1.','.$getEmail1.','.$loggedInUser.','.$orderMode.','.$logType);
					fclose($file); */

			$sql = "{CALL sp_generateCourierLog(@distributorId=:distributorId,@pcid=:pcid,@address1=:address1,@address2=:address2,@address3=:address3,@coutryCode=:countryId
					,@stateCode=:stateId,@cityCode=:cityId,@pincode=:pincode,@phone1=:phone1,@phone2=:phone2,@mobile1=:mobile1,@email1=:email1,@createdBy=:createdBy,
					@orderMode=:orderMode,@logType=:logType,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':distributorId',$distributorId, PDO::PARAM_INT);
			$stmt->bindParam(':pcid',$pcid, PDO::PARAM_INT);
			$stmt->bindParam(':address1',$getAddress1, PDO::PARAM_STR);
			$stmt->bindParam(':address2',$getAddress2, PDO::PARAM_STR);
			$stmt->bindParam(':address3',$getAddress3, PDO::PARAM_STR);
			$stmt->bindParam(':countryId',$getCountry, PDO::PARAM_INT); 
			$stmt->bindParam(':stateId',$getState, PDO::PARAM_INT);
			$stmt->bindParam(':cityId',$getcity, PDO::PARAM_INT);
			$stmt->bindParam(':pincode',$getPincode,PDO::PARAM_INT);
			$stmt->bindParam(':phone1',$getPhone1, PDO::PARAM_STR);
			$stmt->bindParam(':phone2',$getPhone2, PDO::PARAM_STR);
			$stmt->bindParam(':mobile1',$getMobile1, PDO::PARAM_STR);
			$stmt->bindParam(':email1',$getEmail1, PDO::PARAM_STR);
			$stmt->bindParam(':createdBy',$loggedInUser, PDO::PARAM_INT);
			$stmt->bindParam(':orderMode',$orderMode, PDO::PARAM_INT);
			$stmt->bindParam(':logType',$logType, PDO::PARAM_INT);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
			$stmt->execute();
			$courierLogResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			if($courierLogResult[0]['OutParam'] == "INF0001"){
				throw new vestigeException("No state code received");
			}
			if($courierLogResult[0]['OutParam'] == "INF0002"){
				throw new vestigeException("No branch registered in ".$courierLogResult[0]['StateName']);
			}
				
			//$courierLogResultData = $this->vestigeUtil->formatJSONResult(json_encode($courierLogResult), '');
			
			//return $courierLogResultData;
			$results[0]['LogNo']= $courierLogResult[0]['LogNo'];
			$results[0]['LogOrderAmount']= $courierLogResult[0]['LogOrderAmount'];
			$results[0]['BOId']= $courierLogResult[0]['BOId'];
			$results[0]['LocationCode']= $courierLogResult[0]['LocationCode'];
			
			$results[0]['LogAddress']= $courierLogResult[0]['LogAddress'];
			$results[0]['OrderMode'] = $courierLogResult[0]['OrderMode'];
			$results[0]['LogType'] = $courierLogResult[0]['LogType'];
		
		}
		catch (PDOException $e) {
			
			throw new Exception($e->getMessage());
				
			//return $storeDeliveryAddressData;
			
		} 
		
		return $results;
	}
	
	function generateSelfLog($locationId,$distributorId,$orderMode,$loggedInUserId,$loggedInUserType){
		
		$pdo_object = POSBusinessClass :: dbConnectionInfo();
		$sql = "{CALL sp_generateSelfLog(@distributorId=:distributorId,@orderMode=:orderMode,@locationId=:locationId,@createdBy=:createdBy,
				@outParam=:outParam)}";
		$stmt = $pdo_object->prepare($sql);
		$stmt->bindParam(':distributorId',$distributorId, PDO::PARAM_INT);
		$stmt->bindParam(':orderMode',$orderMode, PDO::PARAM_INT);
		$stmt->bindParam(':locationId',$locationId, PDO::PARAM_INT);
		$stmt->bindParam(':createdBy',$loggedInUserId, PDO::PARAM_INT);
		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $result;
		
	}
	
	function getGiftVoucherItemDetail($giftVoucherItemId)
	{
		try{
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
		
			$sql = "SELECT [ItemId]
					,[ItemCode]
					,[ItemName]
					,[ShortName]
					,[PrintName]
					,[ReceiptName]
					,[DisplayName]
					,[MerchHierarchyDetailId]
					,[PrimaryCost]
					,[DistributorPrice]
					,[BusinessVolume]
					,[PointValue]
					,[PromotionParticipation]
					,[IsKit]
					,[IsAvailableforGift]
					,[MinKitValue]
					,[TaxCategoryId]
					,[IsComposite]
					,[Height]
					,[Width]
					,[Length]
					,[Weight]
					,[StackLimit]
					,[BayNumber]
					,[ExpDuration]
					,[Status]
					,[CreatedBy]
					,[CreatedDate]
					,[ModifiedBy]
					,[ModifiedDate]
					,[TransferPrice]
					,[LandedPrice]
					,[IsForRegistrationPurpose]
					,[ItemTypeID]
					,[ItemPackSize]
					,[ExpiryDateFormat]
					FROM Item_Master with (NOLOCK) where ItemId = '$giftVoucherItemId'";
		
			$stmt = $pdo_object->prepare($sql);
		
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$giftVoucherItemDetailsData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $giftVoucherItemDetailsData;
		
		}
		catch (PDOException $e) {
			
			$giftVoucherItemDetailsData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $giftVoucherItemDetailsData;
		
		}
	}
	
	
/* ...........................FORMHISTORY ...............................*/	
	
	function searchOrderHistory( $HistoryStatus,$HistoryOrderNo,$HistoryInvoiceNo,$historyLogNo,$HistoryFromDate,$historyToDate,$historyDistributorNo,$GetAddressCity,$locationId,$loggedInUserId,$isServiceCentreUser,$loggedInUserType)
	{
		
		try{
			
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			$dynamicOrderByClause = "";
			$dynamicWhereClause = "And (ISNULL('$historyDistributorNo',-1)=-1 OR '$historyDistributorNo'=0 OR CO.[DistributorId]='$historyDistributorNo') AND CO.BOId='$locationId' AND ((ISNULL('$GetAddressCity','-1') = '-1') OR ('$GetAddressCity' = '0') OR (CO.PCId = '$GetAddressCity'))";
			
			if($isServiceCentreUser == true){
				$dynamicWhereClause = "";
			}
			
			if($loggedInUserType == 'D'){
				$loggedInDistributor = $this->vestigeUtil->getSessionData("loggedInDistributorId");
				$dynamicWhereClause = " AND (ISNULL('$loggedInDistributor','')='' OR  OL.DistributorId = $loggedInDistributor) ";
				$dynamicOrderByClause = "OL.CreatedDate desc , ";
			}
			
			$sql = "SELECT ISNULL(CO.CustomerOrderNo,'')[CustomerOrderNo],
			 	PMS.KeyValue1 AS StatusName
			 	,ISNULL(co.[Date],'')[Date]
			 	, OL.LogNo AS LogValue,cop.TenderType,isnull(cop.PaymentAmount,0) as CardAmount,case when co.status=3 then (CO.PaymentAmount- isnull(cop2.PaymentAmount,0)) else 0 end as amountYetToBePaid,
			 	opay.TransactionId,'$loggedInUserType' LoggedInUserType
			 	, CASE WHEN OL.LogType=1 AND OL.BOId <> OL.PCId THEN ISNULL(Lm.Name,'') + ' ' + ISNULL(OL.LogNo,'') ELSE ISNULL(OL.LogNo,'') END [LogNo]
			 	--,ISNULL(CO.LogNo,'')[LogNo]
			 	,ISNULL(CO.DistributorId,0)[DistributorId]
			  ,ISNULL(LTRIM(RTRIM(DM.DistributorFirstName)),'')+' '+ISNULL(LTRIM(RTRIM(DM.DistributorLastName)) ,'')As [DistributorName]
			  ,ISNULL(CO.Status,0)[Status]
			  ,ISNULL(CO.TotalUnits,0)[TotalUnits]
			  ,ISNULL(CO.TotalWeight,0)[TotalWeight]
			  ,ISNULL(CO.OrderAmount,0)[OrderAmount]
			  ,ISNULL(CO.DiscountAmount,0)[DiscountAmount]
			  ,ISNULL(CO.TaxAmount,0)[TaxAmount]
			  ,ISNULL(CO.PaymentAmount,0)[PaymentAmount]
			  ,ISNULL(CO.ChangeAmount,0)[ChangeAmount]
			  ,ISNULL(CO.CreatedBy,0)[CreatedBy]
			  ,ISNULL(CO.[CreatedDate],'')[CreatedDate]
			  ,ISNULL(CO.[ModifiedBy],0)[ModifiedBy]
			  ,ISNULL(CO.[ModifiedDate],'')[ModifiedDate]
			  ,ISNULL([OrderType],0)[OrderType]
			  ,ISNULL(CO.PCId,0)[PCId]
			  ,ISNULL(lm.LocationCode,0)PCCode
			  ,ISNULL(CO.BOId,0)[BOId]
			  ,CASE WHEN CO.OrderMode = 1 THEN 'Self' ELSE 'Courier' END OrderModeName 
			  ,ISNULL(CO.OrderMode,0)[OrderMode]
			  ,ISNULL([UsedForRegistration],'')[UsedForRegistration]
			  ,ISNULL(CO.TerminalCode,'')TerminalCode
			  ,ISNULL(CO.TotalBV,0)TotalBV
			  ,ISNULL(CO.TotalPV,0)TotalPV
			  ,ISNULL(CO.DistributorAddress, '') DistributorAddress
			  ,ISNULL(c.InvoiceNo,'') AS InvoiceNo
			  , ISNULL(c.InvoiceDate,'') AS InvoiceDate
			  -- ,ISNULL(dm.SerialNo ,'') AS  SerialNo
			  ,CO.PaidOrder as 'PaidOrder'
			  FROM [COHeader] CO with (NOLOCK)
			  LEFT JOIN [OrderLog] OL with (NOLOCK)
			  ON OL.LogNo = CO.LogNo
			  LEFT JOIN DistributorMaster DM with (NOLOCK) On CO.DistributorId=DM.DistributorId
			  LEFT JOIN Location_Master lm with (NOLOCK) ON CO.PCID=lm.LocationId
			  LEFT JOIN CIHeader c with (NOLOCK) ON c.CustomerOrderNo = CO.CustomerOrderNo
			  LEFT JOIN Parameter_Master PMS with (NOLOCK) ON
			  PMS.ParameterCode='ORDERSTATUS' AND PMS.KeyCode1=CO.Status
			  left join COPayment cop on cop.CustomerOrderNo = co.CustomerOrderNo and cop.TenderType = 2
			    left join COPayment cop2 on cop2.CustomerOrderNo = cop2.CustomerOrderNo and cop.TenderType = 5
			  left join OnlinePayGOrderTrack opay on opay.TrackId = co.CustomerOrderNo
			  WHERE (ISNULL('$HistoryOrderNo','')='' OR CO.CustomerOrderNo LIKE '%$HistoryOrderNo%')
					AND (Convert(varchar(10),CAST('$HistoryFromDate' AS DATETIME),112)='19000101' OR Convert(varchar(10),CO.[Date],112)>=Convert(varchar(10),CAST('$HistoryFromDate' AS DATETIME),112))
					AND (Convert(varchar(10),CAST('$historyToDate' AS DATETIME),112)='19000101' OR CONVERT(varchar(10),CO.[Date],112)<=Convert(varchar(10),CAST('$historyToDate' AS DATETIME),112))
					AND (ISNULL(' $HistoryStatus',-1)=-1 OR '$HistoryStatus'=0 OR CO.[Status]=' $HistoryStatus')
					AND (ISNULL('$historyLogNo','')='' OR  CO.[LogNo] like '%$historyLogNo%' )
					AND (ISNULL('',-1)=-1 OR ''=0 OR CO.[OrderType]='')
					AND (ISNULL('','')='' OR DM.DistributorFirstName LIKE '%%')
					AND (ISNULL('$HistoryInvoiceNo','')='' OR c.InvoiceNo LIKE '%$HistoryInvoiceNo%')
					AND (ISNULL('$loggedInUserId',-1)=-1 OR '$loggedInUserId'=0 OR CO.CreatedBy='$loggedInUserId')
					AND ((ISNULL('$GetAddressCity','-1') = '-1') OR ('$GetAddressCity' = '0') OR (CO.PCId = '$GetAddressCity'))"
					.$dynamicWhereClause.		
					" ORDER BY ".$dynamicOrderByClause." "." CO.Date DESC";
			
			
			
			
	 	$stmt = $pdo_object->prepare($sql);
	 	 
	 	$stmt->execute();
	 	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 	
	 	$historyOrderData = $this->vestigeUtil->formatJSONResult(json_encode($results), '','myAccountLookUpData');
	 	
	 	return $historyOrderData;
	 	 
		}
		catch (PDOException $e) {
			
			$historyOrderData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			 
			return $historyOrderData;
	 	 
		}
	}
	 function historyOrderStatus()
	 {
	 	try
	 	{
	 		$connectionString = new DBHelper();
	 		
	 		$pdo_object = $connectionString->dbConnection();
	 		
	 		$stmt = $pdo_object->prepare("Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master
			Where
				parametercode='ORDERSTATUS'
				And isactive=	1
			Order By
				sortorder Asc");
	 		$stmt->execute();
	 		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 		
	 		$historyOrderStatusData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
	 		
	 		return $historyOrderStatusData;
	 	}
	 	catch(Exception $e)
	 	{
	 		$historyOrderStatusData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
	 		
	 		return $historyOrderStatusData;
	 	}
	 	
	 	
	 }
	 function changeLogForOrder($arrOfOrders,$logValue,$modifiedBy){

	 	$connectionString = new DBHelper();

	 	$pdo_object = $connectionString->dbConnection();
	 	try{
	 		$sql="Exec sp_UpdateOrderLog '$arrOfOrders','$logValue','$modifiedBy','' ";
	 		$stmt = $pdo_object->prepare($sql);
	 		$stmt->execute();
	 		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 		$historyOrderStatusData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');

	 		return $historyOrderStatusData;
	 	}
	 	catch(Exception $e)
	 	{
	 		$historyOrderStatusData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());

	 		return $historyOrderStatusData;
	 	}
	 }
	 
	 function getWEBLogNumber($distributorId,$pucId,$logType,$orderMode,$createdBy,$loggedInUserLocationId)
	 {
	 	$connectionString = new DBHelper();
	 	
	 	//$createdBy = $ves
	 	//$loggedInUserLocationId = 10; //logged in user location id taken as 10 for generating log with extension of WEB. 
	 	
	 	
	 	
	 	$pdo_object = $connectionString->dbConnection();
	 	try{
	 		$sql="Exec sp_GenerateLogNo '$pucId','$logType','$orderMode','$createdBy','$distributorId','$loggedInUserLocationId','' ";
	 		$stmt = $pdo_object->prepare($sql);
	 		$stmt->execute();
	 		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 		
	 		
	 	}
	 	catch(Exception $e)
	 	{
	 		throw new Exception($e);
	 	}
	 	
	 	return $results;
	 }
	 
	 
	 function getListOfLogForPucOrDistributors($pcid,$distributorid,$loggedInUserType,$orderMode){
	 	try
	 	{
	 		$pdo_object = POSBusinessClass :: dbConnectionInfo();
	 		$logType = 1;
	 		if($loggedInUserType == 'D'){
	 			$logType = 2;
	 			$pcid = -1;
	 		}
	 		if($loggedInUserType == 'P'){
	 			$logType = 1;
	 			$distributorid = -1;
	 		}
	 		
	 		$sql = "select LogNo,Address1+' '+Address2 + ' '+Address3 + ', '+cm.CityName + ', '+sm.StateName as LogAddress
			 ,ol.isChangeAddress OrderMode  from OrderLog ol
			 left join city_master cm on ol.CityCode = cm.CityId 
			 left join State_Master sm on ol.StateCode = sm.StateId
			 where (isnull(DistributorId,-1) = -1 OR DistributorId = $distributorid) 
			 and ( ISNULL(-1,-1)=-1 or PCId = -1)
			 and convert(varchar(12),CreatedDate,112) = convert(varchar(12),GETDATE(),112)
			 and LogType = $logType and (LogNo like '%dist%' OR LogNo like '%dist%') and ol.Status = 1 and isChangeAddress = $orderMode order by ol.CreatedDate desc";
	 	
	 		$stmt = $pdo_object->prepare($sql);
	 	
	 		$stmt->execute();
	 		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 	}
	 	catch(Exception $e)
	 	{
	 		throw new Exception($e->getMessage());
	 	}
	 	
	 	return $results;
	 }
	 
	 
	 function loadBranchesForState($searchString){
	 	
	 	$pdo_object = POSBusinessClass :: dbConnectionInfo();
	 		
	 	$sql = "Select lm.locationId,lm.name as locationName,lm.stateId,lm.locationCode,
		lm.Address1+' '+lm.Address2+' '+lm.Address3+' '+cm.CityName+' '+sm.StateName+' '  as LocationAddress
		from location_master lm join state_master sm on lm.stateId = sm.stateId 
		join City_Master cm on lm.CityId = cm.CityId where lm.Address1+' '+lm.Address2+' '+lm.Address3+' '+' '+cm.CityName+' '+sm.stateName
		like '%$searchString%' and locationtype = 3 and lm.isminibranch =0 and lm.status = 1 and lm.locationId != 10 AND
	 	lm.locationcode not like '%wh%' and lm.name not like '%wh%' AND SM.Status = 1 and lm.IsOnlineForDistributor=1";
	 		
	 	$stmt = $pdo_object->prepare($sql);
	 	$stmt->execute();
	 	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 
	 	return $results;
	 }
	 
	 
	 function receivePaymentAtBranch($orderType,$searchParameter){
	 	
	 	$pdo_object = POSBusinessClass :: dbConnectionInfo();
	 	
	 	if($orderType == 'S'){
	 		
	 		$dynamicPart = "('$searchParameter')";
	 	}
	 	if($orderType == 'M'){
	 		
	 		$orderIds = explode('_',$searchParameter);
	 		$commaSeperatedString = "";
	 		for($i=0;$i<sizeof($orderIds);$i++){
	 			if($i == sizeof($orderIds) - 1){
	 				$commaSeperatedString = $commaSeperatedString."'$orderIds[$i]'";
	 			}
	 			else{
	 				$commaSeperatedString = $commaSeperatedString."'$orderIds[$i]',";
	 			}
	 				
	 		}
	 		
	 		$dynamicPart = "($commaSeperatedString)";
	 	}
	 	if($orderType == 'L'){
	 		
	 		
	 		
	 		$sql = "Select customerorderno from coheader(nolock) where logno = '$searchParameter'";
	 	
		 	$stmt = $pdo_object->prepare($sql);
		 	$stmt->execute();
		 	$ordersListFromLog = $stmt->fetchAll(PDO::FETCH_ASSOC);
		 
		 	$commaSeperatedString = "";
		 	for($i=0;$i<sizeof($ordersListFromLog);$i++){
		 		$customerOrderNo = $ordersListFromLog[$i]['customerorderno'];
		 		if($i == sizeof($ordersListFromLog) - 1){
		 			$commaSeperatedString = $commaSeperatedString."'$customerOrderNo'";
		 		}
		 		else{
		 			$commaSeperatedString = $commaSeperatedString."'$customerOrderNo',";
		 		}
		 	}
		 	
		 	$dynamicPart = "($commaSeperatedString)";
		}
	 	
		
		
	 	$sql = "select CustomerOrderNo,LogNo,PaidOrder,sum(tod.CardAMount) CardAmount,SUM(CashAmount) CashAmount,SUM(BonusAmount) BonusAmount,
	 	sum(tod.ForexAmount) ForexAmount,SUM(tod.ChequeAmount) ChequeAmount,SUM(tod.BankAmount) BankAmount,
	 	SUM(tod.CodAmount) CodAmount,case when isnull(od.TransactionRefNo,0)=0 then 0 else 1 end  SuccessfulStatus from (
	 	select
	 	case when TenderType = 2 then cop.PaymentAmount else 0 end CardAmount,
	
	 	case when tendertype = 1 then cop.paymentamount else 0 end CashAmount,
	 	CASE WHEN TenderType = 3 THEN cop.PaymentAmount ELSE 0 END ForexAmount,
	 	CASE WHEN TenderType = 4 THEN cop.PaymentAmount ELSE 0 END ChequeAmount,
	 	case when TenderType = 5 then cop.PaymentAmount else 0 end BonusAmount,
	 	CASE WHEN TenderType = 6 THEN cop.PaymentAmount ELSE 0 END BankAmount,
	 	CASE WHEN TenderType = 8 THEN cop.PaymentAmount ELSE 0 END CodAmount,
	 	cop.CustomerOrderNo,coh.LogNo,coh.OrderShippingCharges,coh.PaidOrder from COPayment COP left join COHeader coh
	 	on cop.customerorderno = coh.customerorderno 
	    where cop.CustomerOrderNo in $dynamicPart ) tod
	    left join OnlinePayGOrderTrack od on od.TrackId = tod.CustomerOrderNo
	    group by TransactionRefNo,CustomerOrderNo,LogNo,PaidOrder";
	 	
	 	
	 	/* $file = fopen("D://History34456654.txt", "w");
	 	fwrite($file,$sql);
	 	fclose($file); */
	 	
	 	$stmt = $pdo_object->prepare($sql);
	 	$stmt->execute();
	 	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 	
	 	return $results;
	 	
	 }
	 
	/*--------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Load Previous order
	 */
	function loadPreviousOrders($historyOrderNo)
	{
		try{
			
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "{CALL sp_HistroryOrder (@orderNo=:OrderNo)}";
			
			$stmt = $pdo_object->prepare($sql);
		
			$stmt->bindParam(':OrderNo',$historyOrderNo, PDO::PARAM_STR);
			
			$stmt->execute();
			
			$results= array();
			do
			{
				$results[] = $stmt->fetchAll();
				
			}while($stmt->nextRowset());

			$stmt->closeCursor();
	
			$loadPreviousOrderData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $loadPreviousOrderData;

		}
		catch(PDOException $e){
			
			$loadPreviousOrderData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $loadPreviousOrderData;
			
		}
		
		
	}
	
	function formatJsonResult()
	{
		
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();


		$sql = "  select * from DistributorMaster with (NOLOCK) where distributorId = 11000008 or DistributorId = 11000005";

		$stmt = $pdo_object->prepare($sql);


		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		

		$status = 1;
		$description = "successfully implemented";
		

			
		$formatedResult = '{"Status"'.':'.$status.','.'"Description"'.':'.'"'."$description".'"'.','.'"Result"'.':'.json_encode($results).'}' ;

	return $formatedResult;
	
	}
	
	function returnMerchHeirarchyItemsDetail($locationId)
	{
		$connectionString = new DBHelper();
		 
		
		$pdo_object = $connectionString->dbConnection();
		try{
			$sql="Exec SP_MerchdisingHirarachyForPromotion '$locationId','' ";
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
		 
		return $results;
	}
	
	function getDistributorlistToSendDBR($generatedBy,$locationId){

		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
			
			$outParam = '';
			/*$sql = "
			 SELECT '$locationId' LocationId,'$GeneratedBy' GeneratedBy,'$logNo' TeamOrderNo, 'NO Logic' TeamOrderStatus,COP.CustomerOrderNo AllOrders from (SELECT  top 1
			 		CustomerOrderNo=STUFF((SELECT ','+ CustomerOrderNo FROM COHeader  t2 where t2.Status=4 and t2.LogNo='$logNo'
			 				FOR XML PATH ('')),1,1,'') from COHeader t1 ) COP
			";*/
			$sql = "{CALL SP_getDistributorForDBRReport(@locationId=:locationId,@generatedBy=:generatedBy, @outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':locationId',$locationId, PDO::PARAM_INT);
			$stmt->bindParam(':generatedBy',$generatedBy, PDO::PARAM_STR);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			 
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$results[0]['reportUrl'] = 'birt-viewer';
			$results[0]['reportPath'] = '/getreport/requestDBRReport';

			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
	
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());

			return $exception;
		}

	}
	

}//end class


?>
