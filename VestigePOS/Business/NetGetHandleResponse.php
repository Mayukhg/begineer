<?php

		
		class NetGetHandlerResponse{
		
			function handleResponse(){
				/*
				 ******************************************************************
				* COMPANY    - FSS Pvt. Ltd.$
				******************************************************************
				Name of the Program : Hosted UMI Sample Pages
				Page Description    : Allows Merchant to connect Payment Gateway and send request
				Request parameters  : TranporatID,TranportalPassword,Action,Amount,Currency,Merchant
				Response/Error URL & TrackID,Language,UDF1-UDF
				Hashing Parameters	: TranporatID,TrackID,Amount,Currency,Action
				Response parameters : Payment Id, Pay Page URL, Error
				Values from Session : No
				Values to Session   : No
				Created by          : FSS Payment Gateway Team
				Created On          : 12-03-2013
				Version             : Version 4.1
				****************************************************************
				The set of pages are developed and tested using below set of hardware and software only.
				In case of any issues noticed by merchant during integration, merchant can contact respective bank
				for technical assistance
					
				NOTE -
				This sample pages are developed and tested on below platform
					
				PHP  Version     - 5.3.5
				Web/App Server   - Apache 2.2.17/Wamp 2.1
				Operating System - Windows 2003/7
				*****************************************************************
				*/
					
				/*
				 Disclaimer:- Important Note in Pages
				- Transaction data should only be accepted once from a browser at the point of input, and then kept
				in a way that does not allow others to modify it (example server session, database  etc.)
					
				- Any transaction information displayed to a customer, such as amount, should be passed only as
				display information and the actual transactional data should be retrieved from the secure source
				last thing at the point of processing the transaction.
					
				- Any information passed through the customer's browser can potentially be modified/edited/changed
				/deleted by the customer, or even by third parties to fraudulently alter the transaction data/
				information. Therefore, all transaction information should not be passed through the browser to
				Payment Gateway in a way that could potentially be modified (example hidden form fields).
				*/
					
				/*
				 BELOW ARE LIST OF PARAMETERS THAT WILL BE RECEIVED BY MERCHANT FROM PAYMENT GATEWAY
				*/
					
				$connectionString = new DBHelper();
				$pdo_object =  $connectionString->dbConnection();
					
					
				$httpHeader = "";
				foreach (getallheaders() as $name => $value) {
					$httpHeader=$httpHeader."$name: $value,";
				}
					
				
		
					
				$query_string = "";
				if ($_POST) {
					$kv = array();
					foreach ($_POST as $key => $value) {
						$kv[] = "$key=$value";
					}
					$query_string = join("&", $kv);
				}
		
		
				else {
					$query_string = $_SERVER['QUERY_STRING'];
				}
		
		
		
				 
		
				 
				//echo $query_string;
				$serverInfo =$_SERVER['REQUEST_URI'];
					
				$sql = "insert into BankResponse(HttpHeader,HttpUrl,HttpPostedData) output inserted.Id values('$httpHeader','$serverInfo','$query_string')";
					
				
					
				$stmt = $pdo_object->prepare($sql);
					
				$stmt->execute();
					
				$results = $stmt->fetchAll();
					
				$updatedFlag = 0;
					
				try
				{
						
					/* Capture the IP Address from where the response has been received */
					$strResponseIPAdd = getenv('REMOTE_ADDR');// Minku
						
					//$strResponseIPAdd = "221.134.101.175";
					$strResponseIPAdd = "116.202.36.48";  //minku
					/* $connectionString = new DBHelper();
						$pdo_object =  $connectionString->dbConnection(); */
		
		 	
					/* Check whether the IP Address from where response is received is PG IP */
					if ($strResponseIPAdd == "221.134.101.175" || $strResponseIPAdd == "221.134.101.166" || $strResponseIPAdd == "221.134.101.187" || $strResponseIPAdd == "192.168.111.200" || $strResponseIPAdd == "116.202.36.48")
					{
							
							
							
							
						//---GET PARAMETER  from as like in response.php. instead of these all paramters written below.
							
							 	
						/*Variable Declaration*/
						/*=========================================================================================*/
							
							
						$ResErrorText=''; 	              //Error Text/message
						$ResPaymentId = isset($_POST['MerchantRefNo']) ? $_POST['MerchantRefNo'] : '';	             //Payment Id
						$ResTrackID = isset($_POST['Description']) ? $_POST['Description'] : '';        //Merchant Track ID
						//	$ResErrorNo = isset($_POST['Error']) ? $_POST['Error'] : '';                        //Error Number
						$ResResult = isset($_POST['ResponseCode']) ? $_POST['ResponseCode'] : '';           //Transaction Result
						$ResPosdate = isset($_POST['DateCreated']) ? $_POST['DateCreated'] : '';                   //Postdate
						$ResTranId = isset($_POST['TransactionID']) ? $_POST['TransactionID'] : '';           //Transaction ID
						//	$ResAuth = isset($_POST['auth']) ? $_POST['auth'] : '';                            //Auth Code
						//	$ResAVR = isset($_POST['avr']) ? $_POST['avr'] : '';                               //TRANSACTION avr
						//	$ResRef = isset($_POST['ResponseMessage']) ? $_POST['ResponseMessage'] : '';        //Reference Number also called Seq Number
						$ResAmount = isset($_POST['Amount']) ? $_POST['Amount'] : '';                       //Transaction Amount
						//	$Resudf1 = isset($_POST['BillingName']) ? $_POST['BillingName'] : '';                 //UDF1
						//	$Resudf2 = isset($_POST['BillingEmail']) ? $_POST['BillingEmail'] : '';             //UDF2
						//	$Resudf3 = isset($_POST['BillingAddress']) ? $_POST['BillingAddress'] : '';         //UDF3
						//	$Resudf4 = isset($_POST['BillingPhone']) ? $_POST['BillingPhone'] : '';              //UDF4
						$Resudf5 = isset($_POST['SecureHash']) ? $_POST['SecureHash'] : '';                 //Secure Hash
						$status = isset($_POST['Status']) ? $_POST['Status'] : '';
						$ResponseMessage = isset($_POST['ResponseMessage']) ? $_POST['ResponseMessage'] : '';
						$ResBillingName = isset($_POST['BillingName']) ? $_POST['BillingName'] : '';
						$ResBillingPhone = isset($_POST['BillingPhone']) ? $_POST['BillingPhone'] : '';
						$ResTransPaymentId = isset($_POST['PaymentID']) ? $_POST['PaymentID'] : '';  // payment id genrates in response
							
							 	
							 	 	
							 	 	 	
							 	 	 	  
						/*LIST OF PARAMETERS RECEIVED BY MERCHANT FROM PAYMENT GATEWAY ENDS HERE */
						/*/=================================================================================================	*/
						 	
						/*
						 First check, if error number is NOT present,then go for Hashing using required parameters
						*/
						/*
						 NOTE - MERCHANT MUST LOG THE RESPONSE RECEIVED IN LOGS AS PER BEST PRACTICE. Since the
						logging mechanism is merchant driven, the sample code for same is not provided in this
						pages
						*/
							
						$ResErrorNo='';
						//---Define  ResErrorNo as empty string to get entry from that if.
						if ($ResErrorNo == '')
						{
							/*******************HASHING CODE LOGIC START************************************/
							/*IMP NOTE: For Hashing below listed parameters have been used. In case merchant develops
							 his/her own pages, merchant to 		make note of these parameters to ensure hashing
							logic remains same.
							Tranportal ID, TrackID, Amount, Result, Payment ID, Reference Number, Auth Code, Transaction ID
								
							If any Hashing parameters is null of blank then merchant need to exclude those parameters
							from hashing*/
								
							/*
							 USE Tranportal ID FIELD as one parameter for hashing.
							Tranportal ID is a sensitive parameter, Merchant can store the Tranportal ID field in
							database as well in page as config value. We recommend merchant storing this parameter
							in database and then calling from database.
							*/
							$sql = "Select keyvalue1 SecretKey from parameter_master(nolock) where parametercode = 'BankSecretKey'";
								
							$stmt = $pdo_object->prepare($sql);
								
							$stmt->execute();
								
							$results = $stmt->fetchAll();
		
		
							//	if(isset($_POST['PaymentID']){
							//		echo "Payment Id not found"
							//	}
								
							//	$sessionResponse = $_SESSION[$_POST['PaymentID']];
								
							if($results[0]['SecretKey'] != '' || $results[0]['SecretKey'] != null){
								$strHashSecretKey=$results[0]['SecretKey']; //USE Tranportal ID FIELD FOR HASHING ,Mercahnt need to take this filed value  from his Secure channel such as DATABASE.
								$hashData = $results[0]['SecretKey'];
							}
							else{
								throw new Exception("Exception in fetching Secret Key");
							}
								
							/* 	$strhashstring="";            //Declaration of Hashing String
							 	
							$strhashstring=trim($strHashTraportalID);
								
							//Below code creates the Hashing String also it will check NULL and Blank parmeters and exclude from the hashing string
							if ($ResTrackID != '' && $ResTrackID != null )
								$strhashstring=trim($strhashstring).trim($ResTrackID);
							if ($ResAmount != '' && $ResAmount != null )
								$strhashstring=trim($strhashstring).trim($ResAmount);
							if ($ResResult != '' && $ResResult != null )
								$strhashstring=trim($strhashstring).trim($ResResult);
							if ($ResPaymentId != '' && $ResPaymentId != null )
								$strhashstring=trim($strhashstring).trim($ResPaymentId);
							if ($ResRef != '' && $ResRef != null )
								$strhashstring=trim($strhashstring).trim($ResRef);
							if ($ResAuth != '' && $ResAuth != null )
								$strhashstring=trim($strhashstring).trim($ResAuth);
							if ($ResTranId != '' && $ResTranId != null )
								$strhashstring=trim($strhashstring).trim($ResTranId);
								
							//Use sha256 method which is defined below for Hashing ,It will return Hashed valued of above strin
							$hashvalue= hash('sha256', $strhashstring); */
		
							$HASHING_METHOD = 'sha512';
							//	$strhashTPass=trim($Resudf5);
							//	$secureHash = strtoupper(hash($HASHING_METHOD, $strhashTPass));
		
		
							ksort($_POST);
							foreach ($_POST as $key => $value){
								if (strlen($value) > 0 && $key != 'SecureHash') {
									$hashData .= '|'.$value;
								}
							}
							if (strlen($hashData) > 0) {
								$secureHash = strtoupper(hash($HASHING_METHOD , $hashData));
							}
		
		
								
		
							/* $explodedOrders = explode("_", $ResTrackID);
							 	
							$sql = "Select RequestParamForPaymentId  from onlinepaygordertrack(nolock) where trackid = 'CO/WB/15/157439'";
								
								
								
							$stmt = $pdo_object->prepare($sql);
								
							$stmt->execute();
		
							$results = $stmt->fetchAll();
		
		
								
							foreach (explode('&', $results[0]['RequestParamForPaymentId']) as $Requestparam) {
							$Reqparam = explode("=", $Requestparam);
								
							if ($Reqparam) {
		
							$file = fopen("D://requestparamsecurehash1.txt","w");
							fwrite($file,urldecode($Reqparam[1]));
							fclose($file);
								
							if( urldecode($Reqparam[0])=='secure_hash'){
								
							$ReqsecureHash=urldecode($Reqparam[1]);
							}
								
							}
							} */
								
								 	
								 	 	
								 	 	 	
							/*******************HASHING CODE LOGIC END************************************/
								
							//	if ($secureHash == $strHashSecretKey)
							if($secureHash==$Resudf5)
							{
									
						//		$file = fopen("D://matchedsecurehash.txt","w");
						//		fwrite($file,'matchedstring');
						//		fclose($file);
		
								/* NOTE - MERCHANT MUST LOG THE RESPONSE RECEIVED IN LOGS AS PER BEST PRACTICE */
								/*IMPORTANT NOTE - MERCHANT DOES RESPONSE HANDLING AND VALIDATIONS OF
								 TRACK ID, AMOUNT AT THIS PLACE. THEN ONLY MERCHANT SHOULD UPDATE
								TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION
								AND THEN REDIRECT CUSTOMER ON RESULT PAGE*/
									
								/* !!IMPORTANT INFORMATION!!
								 During redirection, ME can pass the values as per ME requirement.
								NOTE: NO PROCESSING should be done on the RESULT PAGE basis of values passed in the RESULT PAGE from this page.
								ME does all validations on the responseURL page and then redirects the customer to RESULT PAGE ONLY FOR RECEIPT PRESENTATION/TRANSACTION STATUS CONFIRMATION
								For demonstration purpose the result and track id are passed to Result page	*/
									
								/* Hashing Response Successful	*/
									
								$posBusinessClassObj = new POSBusinessClass();
									
								$connectionString = new DBHelper();
								$pdo_object =  $connectionString->dbConnection();
									
								$outParam='';
									
								//---TRack id will be fetched from table corresponding to payment id coming in post parameter here.
								//---transID = time() and TranRefNo = time()
									
								$ResRef=time();
								// only two response code ,0 for success and 2 for failure
								if($ResResult==0)
								{
									$ResResult='CAPTURED';
								}
								if($ResResult == 2){
									$ResResult='CANCELED';
								}
									
									
					//			$file = fopen("D://sp_param.txt","w");
					//			fwrite($file,$ResponseMessage.', '.$ResTrackID.' ,'.$ResPaymentId.', '.$ResRef.', '.$ResTranId.', '.$ResErrorText.', '.$ResAmount);
					//			fclose($file);
									
								$sql = "{CALL SP_UpdatePaymentTrackForAll (@transationStatus=:transactionStatus,@trackId=:trackId,@PaymentId=:paymentId,
							@TranRefNo=:tranRefNo,@TransID=:transID,@TransError=:transError,@TransactionAmount=:transAmount,@TransPaymentId=:transPaymentId,@outParam=:outParam)}";
									
								$stmt = $pdo_object->prepare($sql);
									
								$stmt->bindParam(':transactionStatus', $ResResult);
								$stmt->bindParam(':trackId', $ResTrackID);
								$stmt->bindParam(':paymentId', $ResPaymentId);
								$stmt->bindParam(':tranRefNo', $ResRef);
								$stmt->bindParam(':transID', $ResTranId);
								$stmt->bindParam(':transError', $ResErrorText);
								$stmt->bindParam(':transAmount', $ResAmount);
								$stmt->bindParam(':transPaymentId', $ResTransPaymentId);
								$stmt->bindParam(':outParam', $outParam);
									
								$stmt->execute();
									
								$results = $stmt->fetchAll();
		
								for($i=0;$i<sizeof($results);$i++){
									if($results[$i]['OutParam'] == 'Trans_Success'){
										//Response received successfully.
									}
									else if($results[$i]['OutParam'] == 'PG_Corrupt_Data'){
											
										$incdenceid=time();
										$emailSubject = 'Security Alert [IT'.$incdenceid.'] - Unknown Track Id detected.';
											
										$dynamicText = '<p style="margin-bottom: 5px;font-family: CENTURY GOTHIC,tahoma;font-size: 15px;">
									Hi team, <br><br> Something wrong is going on in payment gateway as system detected wrong data which was not sent from client. That info may be track id,transaction amount or payment id, which is not available in system . Track id - .'.$results[$i]['OrderNo'].'
									</p>';
											
										$posBusinessClassObj->sendEmail($dynamicText,$emailSubject);
									}
									else if($results[$i]['OutParam'] == 'Trans_Insert'){
										$incdenceid=time();
										$emailSubject = 'Security Alert [IT'.$incdenceid.'] - Unknown Track Id detected.';
											
										$dynamicText = '<p style="margin-bottom: 5px;font-family: CENTURY GOTHIC,tahoma;font-size: 15px;">
									Hi team, <br><br> Something wrong is going on in payment gateway as system detected a track id which is not available in system . Track id - .'.$results[$i]['OrderNo'].'
									</p>';
											
										$posBusinessClassObj->sendEmail($dynamicText,$emailSubject);
									}
									else if($results[$i]['OutParam'] == 'Dup_Trans_Data'){
										$incdenceid=time();
										$emailSubject = 'Security Alert [IT'.$incdenceid.'] - Duplicate Transaction detected .';
											
										$dynamicText = '<p style="margin-bottom: 5px;font-family: CENTURY GOTHIC,tahoma;font-size: 15px;">
									Hi team, <br><br> Something wrong is going on in payment gateway as system detected DUPLICATE track id which is already available in order track table . Track id - '.$results[$i]['OrderNo'].'
									</p>';
											
										$posBusinessClassObj->sendEmail($dynamicText,$emailSubject);
									}
									else{
										throw new Exception($results[$i]['OutParam']);
									}
		
								}
								
								
								
								
								
								
								
								
									
								//For processi
									
								//---Send all paramters here to congfirm.php.
									
									
								$REDIRECT = 'http://www.veston.in/sites/all/modules/VestigePOS/VestigePOS/Business/NetStatusTRAN.php?ResResult='.$ResResult.'&ResTrackId='.$ResTrackID.'&ResAmount='.$ResAmount.'&ResPaymentId='.$ResPaymentId.'&ResRef='.$ResRef.'&ResTranId='.$ResTranId.'&ResError='.$ResErrorText;
								
								//echo $REDIRECT;
									
								header("Location: $REDIRECT");
								exit;
								//echo $REDIRECT;
									
								/* if($results[0]['OutParam'] == 1){
								 //echo "<div><h1>Transaction status upadted successfully</h1></div>";
								//.'&ResTrackId='.$ResTrackID.'&ResAmount='.$ResAmount.'&ResPaymentId='.$ResPaymentId.'&ResRef='.$ResRef.'&ResTranId='.$ResTranId.'&ResError='.$ResErrorText.'Hashing Response Successful';
		
								if($results[0]['UpdatedFlag'] == 1){
								$transactionResult = $results;
								$REDIRECT = 'REDIRECT=http://182.71.3.149/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/Business/StatusTRAN.php?ResResult='.$ResResult.'&ResTrackId='.$ResTrackID.'&ResAmount='.$ResAmount.'&ResPaymentId='.$ResPaymentId.'&ResRef='.$ResRef.'&ResTranId='.$ResTranId.'&ResError='.$ResErrorText.'Hashing Response Successful.&TransOrderDetails='.$transactionResult;
									
								echo $REDIRECT;
								}
								else{
								$sql = "INSERT INTO OnlinePayGOrderTrack(TrackId,TransactionStatus,TransactionPaymentId,TransactionRefNo,TransactionId,TransactionAmount
								TransactionError,ResponseDateTime,Remarks) values('$ResTrackID','Fraud',$ResPaymentId,$ResRef,$ResTranId,$ResAmount,'$ResErrorText',
								getdate(),'System do not have any records for this TrackId')";
									
								$stmt = $pdo_object->prepare($sql);
								$stmt->execute();
								$results = $stmt->fetchAll();
									
									
								$posBusinessClassObj = new POSBusinessClass();
									
								$incdenceid=time();
								$emailSubject = 'Security Alert [IT'.$incdenceid.'] - Unknown Track Id detected.';
									
								$dynamicText = '<p style="margin-bottom: 5px;font-family: CENTURY GOTHIC,tahoma;font-size: 15px;">
								Hi team, <br><br> Something wrong is going on in payment gateway as system detected wrong track id which is not available in system . Track id - .'.$ResTrackID.'
								</p>';
									
								$posBusinessClassObj->sendEmail($dynamicText,$emailSubject);
									
								}
		
								}
								else{
		
								if($results[0]['OutParam'] == "PG_Corrupt_Data"){
								throw new Exception("Unable to process transaction as data has been corrupted on the way for this track id.");
								}
								else if($results[0]['OutParam'] == "Dup_Trans_Data"){
								throw new Exception("Detected one duplicate entry in order trancking table. Corresponding transaction has been marked as disputed.");
								}
								else{
								throw new Exception($results[0]['OutParam']);
								}
								} */
							}
							else
							{
								/* NOTE - MERCHANT MUST LOG THE RESPONSE RECEIVED IN LOGS AS PER BEST PRACTICE */
								/*Udf5 field values not matched with calculetd hashed valued then show appropriate message to
								 Mercahnt for E.g.Hashing Response NOT Successful*/
									
								$sql = "UPDATE OnlinePayGOrderTrack set TransactionError = 'Hashing Response Mismatch' where TrackId = '$ResTrackID'";
									
								$stmt = $pdo_object->prepare($sql);
									
								$stmt->execute();
									
								$results = $stmt->fetchAll();
									
								if($results[0]['TrackId'] != ''){
									$updatedFlag = 1;
								}
									
									
								/* Hashing Response NOT Successful */
								$REDIRECT = 'http://www.veston.in/sites/all/modules/VestigePOS/VestigePOS/Business/NetStatusTRAN.php?ResError=Hashing Response Mismatch';
								//echo $REDIRECT;
								header("Location: $REDIRECT");
								exit;
							}
						}
						else
						{
							/*ERROR IN TRANSACTION PROCESSING
							 IMPORTANT NOTE - MERCHANT SHOULD UPDATE
							TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION
							AND THEN REDIRECT CUSTOMER ON RESULT PAGE*/
		
							$sql = "UPDATE OnlinePayGOrderTrack set Transactionstatus = '$ResResult',TransactionPaymentId= $ResTransPaymentId,TransactionRefNo=$ResRef,
							TransactionAmount=$ResAmount,TransactionId=$ResTranId,TransactionError='$ResErrorText', output inserted.TrackId
							where TrackId = '$ResTrackID' AND TransactionPaymentId = $ResPaymentId AND TransactionAmount = $ResAmount";
		
							$stmt = $pdo_object->prepare($sql);
		
							$stmt->execute();
		
							$results = $stmt->fetchAll();
		
							if($results[0]['TrackId'] != ''){
								$updatedFlag = 1;
							}
		
		
							$REDIRECT = 'http://www.veston.in/sites/all/modules/VestigePOS/VestigePOS/Business/NetStatusTRAN.php?ResResult='.$ResResult.'&ResTrackId='.$ResTrackID.'&ResAmount='.$ResAmount.'&ResPaymentId='.$ResPaymentId.'&ResRef='.$ResRef.'&ResTranId='.$ResTranId.'&ResError='.$ResErrorText;
							//echo $REDIRECT;
							header("Location: $REDIRECT");
							exit;
						}
							
							
					}
					else
					{
						/*
						 IMPORTAN NOTE - IF IP ADDRESS MISMATCHES, ME LOGS DETAILS IN LOGS,
						UPDATES MERCHANT DATABASE WITH PAYMENT FAILURE, REDIRECTS CUSTOMER
						ON FAILURE PAGE WITH RESPECTIVE MESSAGE
						*/
							
						/*to get the IP Address in case of proxy server used*/
						function getIPfromXForwarded() {
							$ipString=@getenv("HTTP_X_FORWARDED_FOR");
							$addr = explode(",",$ipString);
							return $addr[sizeof($addr)-1];
						}
							
							
						$posBusinessClassObj = new POSBusinessClass();
							
						$incdenceid=time();
						$emailSubject = 'Security Alert [IT'.$incdenceid.'] - Fraud case attempted from unkonow IP Address';
							
						$dynamicText = '<p style="margin-bottom: 5px;font-family: CENTURY GOTHIC,tahoma;font-size: 15px;">
							Hi team, <br><br> Please look into the System immediately as someone is trying to access payment gateway from unknown IP address ('.$strResponseIPAdd.')
							</p>';
							
						$posBusinessClassObj->sendEmail($dynamicText,$emailSubject);
							
						//sendEmail();
							
							
						$REDIRECT = 'http://www.veston.in/sites/all/modules/VestigePOS/VestigePOS/Business/NetStatusTRAN.php?ResError=--IP MISSMATCH-- Response IP Address is: '.$strResponseIPAdd;
						//echo $REDIRECT;
						header("Location: $REDIRECT");
						exit;
							
					}
				}
				catch(Exception $e)
				{
					var_dump($e->getMessage());
				}
					
		
		
			}
		}
		
		?>