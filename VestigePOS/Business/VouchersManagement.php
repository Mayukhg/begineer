<?php


/* define('__PATH__', dirname(dirname(__FILE__)));
include(__PATH__.'/Common/VestigeUtil.php'); */
   Class VouchersManagement {
		   	var $vestigeUtil;
		   	function __construct()
		   	{
		   		$this->vestigeUtil = new VestigeUtil();
		   	}
   			/* function searchGRNNumber($GRNNumber,$destinationLocationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   				
   					$sql = "select GH.GRNNo,GH.AmendmentNo,GH.GRNDate,GH.PONumber,GH.PODate,
                            PM.KeyValue1 Status,GH.ChallanNo,
                            GH.InvoiceNo,GH.GrossWeight,
							GH.VehicleNo,GH.TransportName,GH.GatePassNo,GH.NoOfBoxes,GH.InvoiceTaxAmount,
							GH.InvoiceDate,GH.ChallanDate,GH.InvoiceAmount,VM.VendorCode,PH.VendorName,PH.V_Address1,
							PH.V_Address2,PH.V_Address3,PH.V_Address4,cm.CityName,sm.StateName,com.CountryName
							from GRN_Header GH
							INNER JOIN Parameter_Master PM ON PM.KeyCode1=GH.Status							
							INNER JOIN PO_Header PH	ON GH.PONumber=PH.PONumber
							INNER JOIN Vendor_Master VM on Ph.VendorId=vm.VendorId
							Inner Join City_Master cm on ph.V_City=cm.CityId
							Inner join State_Master sm on ph.V_State=sm.StateId	
							inner join Country_Master com on ph.V_Country=com.CountryId					
 							where PARAMETERCODE LIKE '%GRNSTATUS%' AND GRNNo='$GRNNumber'";
   					
   					$stmt = $pdo_object->prepare($sql);
   				
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						$mydata=$outputData;
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   					
   			
   			}
 */
   			
 	function ValidateDistributorid($Distributorid){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   					
   				 	$sql="select distributorstatus,distributormobnumber from Distributormaster with (NOLOCK) where Distributorid='$Distributorid'" ;
   					$stmt = $pdo_object->prepare($sql);   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   					if($results==3 || $results==4)
   					{
   						throw new vestigeException("red",'Distributor is Blocked.');
   					}
   					$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), ''); 
   					}
   				catch(Exception $e)
   					{
   					$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());   				
   					}
   				
   				return $itemData;
   			
   	} 

   	
   	function Searchdetails($Distributorid,$Bonus){
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   	
   			$sql="select DBC.Distributorid,DM.DistributorMobNumber,DBC.ChequeNo,CONVERT(nvarchar(30),DBC.ExpiryDate, 105) as ExpiryDate,
   			DATEADD(month,-9,DBC.ExpiryDate) MsgDate,CONVERT(nvarchar(30),DATEADD(month,-9,DBC.ExpiryDate), 105) CreatedDate,DBC.Amount
   			,DBC.status,CIP.InvoiceNo
   			from distributorbonus_chequelink DBC with (NOLOCK)
   			left join cipayment CIP on CIP.reference=DBC.ChequeNo
   			inner join Distributormaster DM on DM.DistributorId=DBC.Distributorid
   			where ('$Distributorid' IS NULL OR '$Distributorid' = '' OR DBC.Distributorid = '$Distributorid')
   			and  ('$Bonus' IS NULL OR '$Bonus' = '' OR DBC.ChequeNo LIKE '%' + '$Bonus' + '%')" ;
   			
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   			
   		return $itemData;
   	
   	}
   
   	function SaveVoucherdetail($Distributorid,$Expiry,$Bonus,$createdBy)
   	{
   		
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   		
   			$sql = "Update distributorbonus_chequelink set Expirydate=DATEADD(MONTH,1,CONVERT(datetime, getdate(), 103)),modifiedby='$createdBy',modifieddate=getdate() where distributorid='$Distributorid' and chequeNo='$Bonus' and status=1";
   		
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$stmt->nextRowset();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   		
   		return $itemData;
   	}
   	
   	function SearchDistributorDetails($Distributorid)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   			 
   			$sql = "select COH.DistributorId,PM.KeyValue1,COH.CustomerOrderNo,CONVERT(VARCHAR(11),COH.Date,106) Date,Dm.Distributorstatus,DM.Firstordertaken,(DM.DistributorFirstName +' ' + DM.DistributorLastName) Name,
DM.DistributorMobNumber,CONVERT(VARCHAR(11),DM.CreatedDate,106) DateOfJoining from COHeader COH
Left join DistributorMaster DM on DM.Distributorid=COH.DistributorId
Left Join Parameter_master Pm on pm.keycode1=COH.status and parametercode='ORDERSTATUS'
where COH.status!=4 and COH.PaidOrder=0 and COH.distributorid='$Distributorid' and (Convert(varchar(10), COH.Date,120)) >= (select Convert(varchar(10), max(MonthEndDate),120) from businessmonth where status=3)
Union All
select DistributorId,'','','',Firstordertaken,Distributorstatus,(DistributorFirstName +' ' + DistributorLastName) Name,DistributorMobNumber,CONVERT(VARCHAR(11),CreatedDate,106) DateOfJoining from DistributorMaster
where distributorid='$Distributorid'";
   			 
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   		 
   		return $itemData;
   	}
   	
   	function SearchGiftdetails($Distributorid,$Gift)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql = "select GVDL.Availed,GVDL.VoucherSrNo,TEMP.InvoiceNo,GVDL.IssuedTo,CONVERT(nvarchar(30),GVDL.ApplicableTo, 105) ApplicableTo,CONVERT(DECIMAL(16,2), GVH.MinBuyAmount) as MinBuyAmount,CONVERT(nvarchar(30),GVH.CreatedDate, 105) CreatedDate,Dm.DistributorMobNumber from GiftVoucher_Distributor_Link GVDL with (NOLOCK)
   			Inner join GiftVoucherheader GVH on GVH.GiftVoucherCode=GVDL.GiftVoucherCode
   			left join (select CID.voucherSrNo,CID.InvoiceNo from CIHeader CIH inner join cidetail CID on CID.InvoiceNo=CIH.InvoiceNo and CID.GiftVoucherCode!='' where CIH.DIstributorid='$Distributorid') TEMP on TEMP.VoucherSrNo=GVDL.VoucherSrNo
   			
   			Inner join DistributorMaster DM on Dm.distributorid=GVDL.IssuedTo
   			where ('$Distributorid' IS NULL OR '$Distributorid' = '' OR GVDL.IssuedTo = '$Distributorid')
   			and  ('$Gift' IS NULL OR '$Gift' = '' OR GVDL.VoucherSrNo LIKE '%' + '$Gift' + '%')";
   				
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   	
   		return $itemData;
   	}
   	
   	function SaveGiftVoucherdetail($Distributorid,$Expiry,$Gift,$createdBy)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   			 
   			$sql = "Update GiftVoucher_Distributor_Link set ApplicableTo=DATEADD(MONTH,1,CONVERT(datetime, getdate(), 103)),modifiedby='$createdBy',modifieddate= getdate() where IssuedTo='$Distributorid' and VoucherSrNo='$Gift' and Availed=0";
   			 
   			$stmt = $pdo_object->prepare($sql);   			
   			$stmt->execute();
   			$stmt->nextRowset();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   		 
   		return $itemData;
   	}
   
   	function SaveDistributordetails($Distributorid,$createdBy)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();

   			$distributorstatus=1;
   			$firstordertaken=0;
   		
   		try{
   				
   			$sql = "{call SP_ActivateJoining(@DistributorId=:DistributorId,@ModifiedBy=:ModifiedBy,@DistributorStatus=:DistributorStatus,@FirstOrderTaken=:FirstOrderTaken,@outParam=:outParam)}";
   				
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->bindParam(':DistributorId',$Distributorid ,PDO::PARAM_STR);
   			$stmt->bindParam(':ModifiedBy',$createdBy ,PDO::PARAM_STR);
   			$stmt->bindParam(':DistributorStatus',$distributorstatus ,PDO::PARAM_STR);
   			$stmt->bindParam(':FirstOrderTaken',$firstordertaken ,PDO::PARAM_STR);
   			$stmt->bindParam(':outParam', $outParam,PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,500);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			if($results[0]['OutParam']=='INVJ001')
   			{
   				throw new vestigeException('Only todays Joining will be Activated.');
   			}
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   			
   		}
   	
   		return $itemData;
   	}
   	
   	function SendMessageToAll($myJsonString,$count)
   	{
   		$someArray = json_decode($myJsonString, true);
   		
   		for ($i=0; $i<$count; $i++) {
   			if($someArray[$i]["SendMessage"]=='True' && $someArray[$i]["InvoiceNo"]==null)
   			{
   				$DistributorId=$someArray[$i]["DistributorId"];
   				$MobileNo=$someArray[$i]["MobileNo"];
   				$MessageType='BONUSVOUCHERMSG';
   				$Voucherno=$someArray[$i]["VoucherNo"];
   				$Amount=$someArray[$i]["Amount"];
   				$Expiry=$someArray[$i]["ExpiryDate"];
   				$InvoiceNo=$someArray[$i]["InvoiceNo"];
   				
   				if($MobileNo == ''){
   					
   					throw new vestigeException("MOBILE NO.NOT REGISTERED");
   				}
   				else
   				{
   				$itemData= VouchersManagement :: SendMessage($DistributorId,$MobileNo,$MessageType,$Voucherno,$Amount,$Expiry);
   				}
   			}
   			
   		}
   		
   		return $itemData;
   		
   	}
   	
   	function ActivateOrders($myJsonString,$count,$createdBy)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		$someArray = json_decode($myJsonString, true);
   		 
   		for ($i=0; $i<$count; $i++) {
   			if($someArray[$i]["Activate"]=='True')
   			{
   				$OrderNo=$someArray[$i]["OrderNo"];
   				
   				try{
   						
   					$sql = "Update Coheader set status=3,PaidOrder=1,ModifiedBy='$createdBy',ModifiedDate=Getdate() where CustomerOrderNo ='$OrderNo'";
   						
   					$stmt = $pdo_object->prepare($sql);
   					$stmt->execute();
   					$stmt->nextRowset();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   					$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   				}
   				catch(Exception $e)
   				{
   					$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   				}
   				
   			}
   	
   		}
   		 
   		return $itemData;
   		 
   	}
   	
   	function SendGiftMessageToAll($myJsonString,$giftcount)
   	{
   		$someArray = json_decode($myJsonString, true);
   		 
   		for ($i=0; $i<$giftcount; $i++) {
   			if($someArray[$i]["SendMessage"]=='True')
   			{
   				$DistributorId=$someArray[$i]["DistributorId"];
   				$MobileNo=$someArray[$i]["MobileNo"];
   				$MessageType='GIFTVOUCHERMSG';
   				$Voucherno=$someArray[$i]["VoucherNo"];
   				$Amount=$someArray[$i]["Amount"];
   				$Expiry=$someArray[$i]["ExpiryDate"];
   	
   				if($MobileNo == ''){
   				
   					throw new vestigeException("MOBILE NO.NOT REGISTERED");
   				}
   				else
   				{
   				$itemData= VouchersManagement :: SendGiftMessage($DistributorId,$MobileNo,$MessageType,$Voucherno,$Amount,$Expiry);
   				}	
   			}
   	
   		}
   	
   		return $itemData;
   		
   	}
   	
   	function SendMessage($DistributorId,$MobileNo,$MessageType,$Voucherno,$Amount,$Expiry)
   	{
   		 
   		 
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql = "{call sp_VoucherSMS(@smsPhoneNo=:smsPhoneNo,@smscategory=:smscategory,@DistributorId=:DistributorId,@Voucherno=:Voucherno,@Amount=:Amount,@Expiry=:Expiry,@outParam=:outParam)}";
   				
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->bindParam(':smsPhoneNo', $MobileNo,PDO::PARAM_STR);
   			$stmt->bindParam(':smscategory',$MessageType ,PDO::PARAM_STR);
   			$stmt->bindParam(':DistributorId',$DistributorId ,PDO::PARAM_STR);
   			$stmt->bindParam(':Voucherno',$Voucherno ,PDO::PARAM_STR);
   			$stmt->bindParam(':Amount',$Amount ,PDO::PARAM_STR);
   			$stmt->bindParam(':Expiry',$Expiry ,PDO::PARAM_STR);
   			$stmt->bindParam(':outParam', $outParam,PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,500);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   			if(isset($results[0]['OutParam']))
   			{
   			}
   			else
   			{
   				for($i=0;$i<sizeof($results);$i++){
   					$sendMessage = new SendSMS();
   					$smsId=$results[0]['smsId'];
   	
   					$response=$sendMessage->APICallVoucherDetails($DistributorId,$MobileNo,$Voucherno,$Amount,$Expiry);
   	
   					$sql1="update vouchermessages set APIResponse='$response' where smsId=$smsId ";
   					$stmt = $pdo_object->prepare($sql1);
   					$stmt->execute();
   				}
   			}
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   	
   		return $itemData;
   		 
   	}
   	
   	function SendGiftMessage($DistributorId,$MobileNo,$MessageType,$Voucherno,$Amount,$Expiry)
   	{
   	
   	
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql = "{call sp_VoucherSMS(@smsPhoneNo=:smsPhoneNo,@smscategory=:smscategory,@DistributorId=:DistributorId,@Voucherno=:Voucherno,@Amount=:Amount,@Expiry=:Expiry,@outParam=:outParam)}";
   				
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->bindParam(':smsPhoneNo', $MobileNo,PDO::PARAM_STR);
   			$stmt->bindParam(':smscategory',$MessageType ,PDO::PARAM_STR);
   			$stmt->bindParam(':DistributorId',$DistributorId ,PDO::PARAM_STR);
   			$stmt->bindParam(':Voucherno',$Voucherno ,PDO::PARAM_STR);
   			$stmt->bindParam(':Amount',$Amount ,PDO::PARAM_STR);
   			$stmt->bindParam(':Expiry',$Expiry ,PDO::PARAM_STR);
   			$stmt->bindParam(':outParam', $outParam,PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,500);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   			if(isset($results[0]['OutParam']))
   			{
   			}
   			else
   			{
   				for($i=0;$i<sizeof($results);$i++){
   					$sendMessage = new SendSMS();
   					$smsId=$results[0]['smsId'];
   	
   					$response=$sendMessage->APICallGiftVoucherDetails($DistributorId,$MobileNo,$Voucherno,$Amount,$Expiry);
   	
   					$sql1="update vouchermessages set APIResponse='$response' where smsId=$smsId ";
   					$stmt = $pdo_object->prepare($sql1);
   					$stmt->execute();
   				}
   			}
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   	
   		return $itemData;
   	
   	}
   	
   	function SendPassword($MobNo,$Type)
   	{
   		$SendSMS = new smsPVBV;
   		try{
   			
   		$itemdata= $SendSMS->SendPV_BV($MobNo,$Type);
   		
   		}
   		
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   
   		return $itemdata;
   	}
   	
   }
?>

	
