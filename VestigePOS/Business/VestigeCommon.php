<?php

define('__PATH2__', dirname(dirname(__FILE__)));
include_once(__PATH2__.'/Common/VestigeUtil.php');



class VestigeCommon
{
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	
	function sendEmail($to,$messageParam)
	{
		$to ='parveenverma42@gmail.com';
			
			drupal_mail('VestigePOS','notice',$to,'',
			array(
			'emailSubject'=> 'Vestige Help Desk',
			'fileLink'=>'ac.hyml',
			'typeOfEmail'=>'helpDesk'
			
					)
			);
	}
	
	function returnInfoToBeUsedInFileUpload()
	{
		$vestigeUtil = new VestigeUtil();
		
		$fileUploadInfoNeeded = array();
		
		$fileUploadInfoNeeded['loggedInUserName'] = $vestigeUtil->getSessionData("loggedInUsername");
		
		$fileUploadInfoNeeded['loggedInUserLocationName'] = $vestigeUtil->getSessionData("loggedInUserLocationName");
		
		
		
		//return $fileUploadInfoNeeded;

		
	}
	
	/**
	 * 
	 * @param $LocationId
	 * @param $SeqType
	 * @return string
	 */
	function generateLogNumber($seqType,$locationId)
	{
		
		try{
			
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "{CALL usp_GetSeqNo (@SeqType=:SeqType,@LocationId=:LocationId,@SeqNo=:SeqNo)}";
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':SeqType',$seqType, PDO::PARAM_STR);
			$stmt->bindParam(':LocationId',$locationId,PDO::PARAM_INT);
	
			$stmt->bindParam(':SeqNo',$SeqNo, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
	
			$stmt->execute();
			
			$generatedSeqNo = $this->vestigeUtil->formatJSONResult("\"$SeqNo\"", '');
	
			return $generatedSeqNo;
		}
		catch(PDOException $e){
			
			$generatedSeqNo = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $generatedSeqNo;
		}
	}
	
	/**
	 * 
	 * @return multitype:
	 */
	function searchBucket()
	{
		try
		{

			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare("Select
					'-1' BucketId,
					'Select' BucketName,
					-1 Sellable
			Union All
			Select
					buc.BucketId,
					BucketName,
					Sellable
			From Bucket buc(nolock)
			Where buc.Status = 1 AND buc.ParentId Is Not Null
			And BucketId <>9");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$bucketData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $bucketData;
		}
		catch(Exception $e)
		{
			$bucketData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $bucketData;
		}
		
	}
	
	function checkIfHOUser($loggedInUserId){
		try
		{
			$connectionString = new DBHelper();
				
			$returnVar = 0;
			$pdo_object = $connectionString->dbConnection();
				
			$stmt = $pdo_object->prepare("select userid from userroleloc_link where userid = $loggedInUserId and locationid = 1");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(sizeof($results)){
				$returnVar = 1;
			}
			
		}
		catch(Exception $e)
		{
			throw new Exception($e);	
		}
		return $returnVar;
	}
	
	function searchWHLocation()
	{
		try
		{
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
				
			$stmt = $pdo_object->prepare("Select
				'-1'  LocationId,
				'Select' DisplayName,
				''  LocationType,
				'Select' LocationName,
				'-' [Address],
				'-1' CityId,
				'Select' CityName,
				'Select' LocationCode
				,'' DistributorName
				,'' IsexportLocation
				,'' CountryName
				,'' IECCode
				Union All
				Select	lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName,
				CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType, [Name] AS LocationName,
				ISNULL([Address1],'')+' '+ISNULL([Address2],'')+ISNULL([Address3],'')+ISNULL([Address4],'') +  ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'')
				As [Address],cm.CityId, cm.CityName,
				lc.LocationCode,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,IsexportLocation,com.CountryName,IECCode
				From Location_Master lc(nolock)
				Left join City_Master cm(nolock)
		
				On cm.CityId = lc.CityId
		
				Left join state_master sm(nolock)
				On sm.StateId = lc.StateId
		
				Left join Country_Master com(nolock)
				On com.CountryId = lc.CountryId
				LEFT JOIN DistributorMaster dm(nolock) ON lc.DistributorId = dm.DistributorId
				WHERE LocationType IN (2, 3)
				AND lc.Status=1");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$searchWHLocation = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $searchWHLocation;
		}
		catch(Exception $e)
		{
			$searchWHLocation = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $searchWHLocation;
		}
	
	
	}
	
	function getModuleFunctionsAccess($moduleCode,$loggedInUserId,$loggedInUserLocationId)
	{
		try
		{

			$vestigeUtil = new VestigeUtil();
			
			$modulePrivileges = $vestigeUtil->getSessionData("loggedInUserModulePrivilages");
			
			
			$moduleFunctionPrivilegesArray = array();
			
			$moduleCodeFoundFlag = 0;
			
			foreach($modulePrivileges as $key => $value)
			{
				if($key == $moduleCode)
				{
					$moduleCodeFoundFlag = 1;
			
				}
			}
			
			if($moduleCodeFoundFlag == 0)
			{
				//Screen link disable code.
				
				
			}
			else
			{
				$moduleFunctionPrivilegesArray = VestigeCommon :: moduleFunctionHandling($moduleCode,$loggedInUserId,$loggedInUserLocationId);
				
			}
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
		
		
		
		return $moduleFunctionPrivilegesArray;
	}
	
	function moduleFunctionHandling($moduleCode,$loggedInUserId,$loggedInUserLocationId)
	{
		try
		{
			$connectionString = new DBHelper();
		
			$pdo_object = $connectionString->dbConnection();
		
			$sql = "select URLL.Roleid,MM.ModuleCode,FM.FunctionName from UserRoleLoc_link URLL(nolock)
					left join RoleModFuncCond_Link RMFCL(nolock)
					on URLL.RoleId = RMFCL.RoleId left join Module_Master MM on RMFCL.ModuleId = MM.ModuleId
					left join Function_Master FM(nolock) on RMFCL.FunctionId = FM.FunctionId
					where URLL.UserId = $loggedInUserId and URLL.LocationId = $loggedInUserLocationId and MM.ModuleCode = '$moduleCode' ";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			$moduleActionPrivilegesArray  = array();
			
			for($i = 0; $i < sizeof($results); $i++)
			{
				$moduleActionPrivilegesArray[$results[$i]['ModuleCode'].$results[$i]['FunctionName']] =  1;
			}
			
			
			
		}
		catch(Exception $e)
		{
			throw new Exception($e);
		}
		
		
		return $moduleActionPrivilegesArray;
	}
	
	function checkStockCountInitiatedOrNotForLocation($locationId)
	{
		try {
			
			
			if($locationId=='')
			{
				throw new vestigeException("Refresh page or login again");
			}
				
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
				
			$sql = "select InitiatedDate,CASE WHEN CONVERT(VARCHAR(10),InitiatedDate, 112) = CONVERT(VARCHAR(10), GETDATE(), 112)
					then 1 ELSE 0 END as 'StockCountInitiateStatus' FROM StockCountHeader(nolock) WHERE LocationId = $locationId
					and Status <> 5 and Status <> 6 and Status <> 2 and CONVERT(VARCHAR(10),InitiatedDate, 112) =
					CONVERT(VARCHAR(10), GETDATE(), 112)";
				
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			/* $file = fopen("D://login.txt", "w");
			fwrite($file, json_encode($results));
			fclose($file); */
						
			
		}
			
			
		catch (Exception $e) {

			throw new vestigeException($e->getMessage());

		}
		
		return $results;
	}
	
	function fetchTitles()
	{
		try {


			$connectionString = new DBHelper();

			$pdo_object = $connectionString->dbConnection();

			$sql = "select -1 as 'DistributorTitleId', 'Select' as DistributorTitle union select KeyCode1 as
					DistributorTitleId,KeyValue1 as DistributorTitle from Parameter_Master(nolock) where ParameterCode = 'TITLE'";

			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);


		}


		catch (Exception $e) {

			throw new Exception($e);

		}

		return $results;
	}
	
	
	function changePassword($loggedInUserId,$newPassword,$loggedInUserType,$loggedInDistributorId)
	{
			
		try
		{
			
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
			
			if($loggedInUserType == 'P' || $loggedInUserType == 'D')
			{
				$sql = "update DistributorMaster set Password = '$newPassword' where DistributorId = $loggedInDistributorId"; //Need to change query for location. 
			}
			else
			{
				$sql = "update user_master set WebUserPassword = '$newPassword' where UserId = $loggedInUserId"; //Need to change query for location.
			}
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			//$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
			$changedPassword = array();
			$changedPassword['changedPassword'] = $newPassword;
				
			return $changedPassword;
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}
	
	
	}
	
	/* 
	function getLoggedInUserInfo()
	{
		$loggedInUserInfo = array();
		
		$loggedInUserInfo['loggedInUserFullName'] = $ves
		
	}
	 */
	/**
	 * Function to get users at this PC Locations
	 * @param PC Location Id $PCLocationId
	 */
	function getUsersAtPCLocation($PCLocationId){
		$users = array();
		try {
			$connectionString = new DBHelper();
			
			$pdo = $connectionString->dbConnection();
			
			$sql = "SELECT -1 LocationId ,-1 UserId,'All' UserName Union All
						SELECT  URL.LocationId,URL.UserId,UM.UserName
  						FROM UserRoleLoc_link URL(nolock) left join
  						User_Master UM(nolock)  on URL.UserId = UM.UserId
  						where LocationId = '$PCLocationId' group by URL.UserId,UM.UserName,URL.LocationId";
			
			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			throw new Exception($e);
		}
		return $users;
	}
	
	
	/**
	 * Function to get users at this PC Locations
	 * @param PC Location Id $PCLocationId
	 */
	function getVoucherAndBonusForDistributor($status,$distributorId,$modifiedDate){
	
		try {
			$connectionString = new DBHelper();
				
			$pdo = $connectionString->dbConnection();
				
			$sql = "SELECT '' Description, GVDL.GiftVoucherCode 'VoucherCodeOrChequeNo',GVDL.VoucherSrNo 'VoucherSerialNoOrChequeNo',GVDL.SeriesId 'SeriesId',
GVDL.IssueDate,GVDL.ApplicableFrom,GVDL.ApplicableTo,GVDL.ModifiedDate,'V' Type, GVID.ItemId 'ItemIdOrAmount','I' 'ProvodingType'
FROM 
GiftVoucher_Distributor_Link GVDL(nolock)
LEFT JOIN
GiftVoucherItemDetail GVID(nolock) 
ON GVDL.GiftVoucherCode = GVID.GiftVoucherCode 
WHERE   GVDL.ModifiedDate > (case when ISNULL('$modifiedDate','') = '' then GETDATE() else '$modifiedDate' END) AND GVDL.IssuedTo = $distributorId
UNION
Select DBCL.description Description,DBCL.ChequeNo 'VoucherCodeOrChequeNo',DBCL.ChequeNo 'VoucherSerialNoOrChequeNo',DBCL.BankID 'SeriesId',
GETDATE() IssueDate,GETDATE() ApplicableFrom,DBCL.ExpiryDate ApplicableTo,GETDATE() ModifiedDate,'B' Type,DBCL.Amount 'ItemIdOrAmount', 'A' 'ProvidingType' 
FROM 
distributorbonus_chequelink DBCL(nolock) 
WHERE DBCL.Status = $status AND DBCL.DistributorId = $distributorId AND DBCL.ExpiryDate > (case when ISNULL('$modifiedDate','') = '' then GETDATE() else '$modifiedDate' END)";
				
	
			
			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
		} catch (Exception $e) {
			throw new Exception($e);
		}
		return $results;
	}
	
	
	function getIncidenceReprotStatus($toDate,$fromDate,$locationId,$userId,$reportedUser){
		try {
			$connectionString = new DBHelper();
	
			$pdo = $connectionString->dbConnection();
	
			$sql = "select * from Incedence where INCUser = $userId and INCLocation = $locationId and INCUserType like '%$reportedUser%'
			and INCDateTime between '$fromDate' and '$toDate'";
	
			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
		} catch (Exception $e) {
			throw new Exception($e);
		}
		return $results;
	}
	
	function getTop5IncedenceReportedLocation($topLevel){
		try {
				
			$connectionString = new DBHelper();
				
			$pdo = $connectionString->dbConnection();
	
			$sql = "select top $topLevel COUNT(INCId) INCReported,INCLocation,lm.Name LocationName,lm.locationId from Incedence inc join
			Location_Master lm on inc.INCLocation = lm.LocationId where INCLocation!=0 GROUP BY INCLocation, lm.Name,lm.locationId
			order by COUNT(INCId) desc";
	
			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			/*
			$chartCategory = array();
				$chartData = array();
				
			for($i = 0;$i<sizeof($results);$i++){
			$chartCategory[$i] = $results[$i]['LocationName'];
			$chartData[$i] = $results[$i]['INCReported'];
			}
				
			$returnData = json_encode(array('category' => $chartCategory,'data'=>$chartData));
			*/
			//$post_data1 = json_encode(array('category' => $category,'data'=>$data), JSON_FORCE_OBJECT);
	
		} catch (Exception $e) {
		throw new Exception($e);
			}
			return $results;
	}
	
	
	function getIncidenceReportedReviewLoctionWise(){

		try {

			$connectionString = new DBHelper();

			$pdo = $connectionString->dbConnection();

			$sql = "select count(INCId) INCReported,INCLocation,lm.Name LocationName from incedence inc join
					Location_Master lm on inc.INCLocation = lm.LocationId where INCLocation != 0
					group by INCLocation,lm.Name";

			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			/*
			 $chartCategory = array();
			$chartData = array();

			for($i = 0;$i<sizeof($results);$i++){
			$chartCategory[$i] = $results[$i]['LocationName'];
			$chartData[$i] = $results[$i]['INCReported'];
			}

			$returnData = json_encode(array('category' => $chartCategory,'data'=>$chartData));
			*/
			//$post_data1 = json_encode(array('category' => $category,'data'=>$data), JSON_FORCE_OBJECT);

		} catch (Exception $e) {
			throw new Exception($e);
		}
		return $results;


	}
	
	function getIncidenceInfoForLocation($locationId){
		try {

			$connectionString = new DBHelper();

			$pdo = $connectionString->dbConnection();

			$sql = "select inc.*,lm.name,um.username from incedence inc join location_master lm on inc.INCLocation = lm.locationid
			join user_master um on inc.INCUser = um.userid where inclocation = $locationId
			and inc.Description is not null and inc.Description != ''";

			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				

		} catch (Exception $e) {
			throw new Exception($e);
		}
		return $results;
	}
	
	function incidenceResponse($INCId,$Comment,$CommentedBy,$Status){
		
		$connectionString = new DBHelper();
		$pdoObject = $connectionString->dbConnection();
		
		$outParam = '';
		
		try{
			$sql = "{CALL sp_IncidenceResponse(@INCId=:INCId,@Comment=:Comment,@CommentedBy=:CommentedBy,@Status=:Status,@outParam=:outParam)}";
			
			$stmt = $pdoObject->prepare($sql);
			$stmt->bindParam(":INCId", $INCId);
			$stmt->bindParam(":Comment", $Comment);
			$stmt->bindParam(":CommentedBy", $CommentedBy);
			$stmt->bindParam(":Status", $Status);
			$stmt->bindParam(":outParam", $outParam);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
		}
		catch (Exception $e){
			throw new Exception($e);
		}
		
		
		
		return $results;
	}
	
	
	/**
	 * To send dbr report to all distributors who are requested. Actually fucntion hitting one service which will try to send report to all requested distributors 
	 * independent of particular one.
	 */
	function sendDBRReport(){
		try{
		
			$params = '';
			
			$serverHost = $_SERVER['HTTP_HOST'];
		
		
			$sendSMScurlObj = curl_init();
		
			$curl_url = "http://".$serverHost."/birt-viewer/getreport/requestDBRReport";
			
			/* $file = fopen("D://login123.txt", "w");
			fwrite($file, $curl_url);
			fclose($file); */
				
			curl_setopt($sendSMScurlObj, CURLOPT_URL, $curl_url);
			curl_setopt($sendSMScurlObj, CURLOPT_POST, 1);
			curl_setopt($sendSMScurlObj, CURLOPT_POSTFIELDS, $params);
			curl_setopt($sendSMScurlObj, CURLOPT_RETURNTRANSFER, true);// Use for not printing reponse on page but to store in a variable. :)
			curl_setopt($sendSMScurlObj, CURLOPT_CONNECTTIMEOUT, 1);
				
			//curl_setopt($sendSMScurlObj, CURLOPT_NOBODY  , true);
				
				
			curl_exec($sendSMScurlObj);
				
			//$sendSMSHttpStatus = curl_getinfo($sendSMScurlObj, CURLINFO_HTTP_CODE);
			curl_close($sendSMScurlObj);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		
		}
	}
	
	function checkToken($post){
	
		$validToken = 0;
	
		if(isset($post['source'])){
			if($post['source'] == "mobile" && !isset($post['ut'])){
				throw new vestigeException("Token is necessary for mobile application");
			}
			if($post['source'] == "mobile" && !isset($post['username'])){
				throw new vestigeException("For mobile source username is necessary");
			}
			if($post['source'] == "mobile"){
	
				$ut = $post['ut'];
				$source = $post['source'];
				$username = $post['username'];
	
				$vestigeUtil = new VestigeUtil();
	
				//$vestigeUtil->decideApplicationToProceedOrNot();
	
				$connectionString = new DBHelper();
	
				$pdo_object = $connectionString->dbConnection();
	
				$sql = "Select distributorid,password,usertoken from distributormaster(nolock) where distributorid = $username ";
	
				$stmt = $pdo_object->prepare($sql);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
				if(sizeof($results) != 0){
					if($results[0]['usertoken'] != $ut){
						throw new vestigeException("Session Expired.Login again.");
					}
					else{
	
							
							
						if($vestigeUtil->checkSessionExists() == 0){
								
								
							//Set session for distributor.
							//$authenticateUser = new Authentication();
								
							$params = 'id=authenticateUser&username='.$username.'&password='.$results[0]['password'].'&source=mobile&ut='.$results[0]['usertoken'];
	
							$password=$results[0]['password'];
							$ut=$results[0]['usertoken'];
								
	
								
							$authenticateUser = new Authentication();
								
							if ($authenticatedDistributorInfo = $authenticateUser->loggedinDistributorAuthentication($username, $password,$url,$source,$ut))
							{
									
								$isDstributorLoggedIn = 1;
								$distributorIdStatus = $vestigeUtil->setSessionData("loggedInDistributorId", $authenticatedDistributorInfo['DistributorId']);
								$distributorLocationIdStatus = $vestigeUtil->setSessionData("loggedInDistributorLocation", $authenticatedDistributorInfo['LocationId']);
								$pickUpcentrIdeStatus = $vestigeUtil->setSessionData("pickUpCentreId", $authenticatedDistributorInfo['PUCId']);
								$loggedInUserTypeStatus = $vestigeUtil->setSessionData("loggedInUserType", $authenticatedDistributorInfo['LoggedInUserType']);
								$distributorLocationCodeStatus = $vestigeUtil->setSessionData("loggedInDistributorLocationCode", $authenticatedDistributorInfo['LocationCode']);
								$forSkinCareItemOrNot = $vestigeUtil->setSessionData("ForSkinCareItem", $authenticatedDistributorInfo['ForSkinCareItem']);
								$forSource = $vestigeUtil->setSessionData("source", $source);
	
	
								$current_login_user_id =  $vestigeUtil->setSessionData("loggedInUser", '452');
	
								$current_login_user_id =  $vestigeUtil->setSessionData("loggedInUserPrivilages", json_decode('{"DS01:Search":1,"DS01:Update":1,"DS01:Password":1,"DS01:DistributorResignation":1,"DS01:PrintDistributorAgreement":1,"POS01:Confirm":1,"POS01:Cancel":1,"POS01:NewOrder":1,"POS01:StockPoint":1,"POS01:ChangeDeliveryMode":1}'));
	
								$loggedInDistributorFullName = $vestigeUtil->setSessionData("loggedInDistributorFullName", $authenticatedDistributorInfo['DistributorFullName']);
	
								// IF Distributor or PUC login as dummy user
								$bc = new POSBusinessClass();
								//$dummyUserDetails = $bc->getPOSUserNamePasswordForDistributor();
								if($authenticatedDistributorInfo['LoggedInUserType'] == 'P'){
									$dummyUserDetails = $bc->getPOSUserNamePasswordForDistributor($authenticatedDistributorInfo['LoggedInUserType']);
								}
								if($authenticatedDistributorInfo['LoggedInUserType'] == 'D'){
									$dummyUserDetails = $bc->getPOSUserNamePasswordForDistributor($authenticatedDistributorInfo['LoggedInUserType']);
								}
	
								if (sizeof($dummyUserDetails))
								{
									$username = $dummyUserDetails[0]['UserName'];
									$password = $dummyUserDetails[0]['UserPassword'];
										
										
										
									$authenticatedUserPrivileges = $authenticateUser->userAuthentication($username, $password,'',$url);
	
								}
								if(sizeof($authenticatedUserPrivileges) == 0)
								{
									//
								}else
								{
								// Override the user location based on User PUC Location
								$loggedInUserLocationStatus = $vestigeUtil->setSessionData("loggedInUserLocation", $authenticatedDistributorInfo['LocationId']);
									
								$loggedInUserLocationNameStatus = $vestigeUtil->setSessionData("loggedInUserLocationName", $authenticatedDistributorInfo['LocationName']);
								$_SESSION["LAST_ACTIVITY"] = time();
									
								}
	
						}
							
	
							
	
	
							
						$params = 'id=authenticateUser&username='.$username.'&password='.$results[0]['password'].'&source=mobile&ut='.$results[0]['usertoken'];
	
						$serverHost = $_SERVER['HTTP_HOST'];
	
						$sendSMScurlObj = curl_init();
	
						$curl_url = "http://localhost/LoginCallback";
	
						curl_setopt($sendSMScurlObj, CURLOPT_URL, $curl_url);
						curl_setopt($sendSMScurlObj, CURLOPT_POST, 1);
						curl_setopt($sendSMScurlObj, CURLOPT_POSTFIELDS, $params);
						curl_setopt($sendSMScurlObj, CURLOPT_RETURNTRANSFER, true);// Use for not printing reponse on page but to store in a variable. :)
	
						$result = curl_exec($sendSMScurlObj);
	
						//$sendSMSHttpStatus = curl_getinfo($sendSMScurlObj, CURLINFO_HTTP_CODE);
						curl_close($sendSMScurlObj);
	
	
						$validToken = 1;
					}
					else{
						$validToken = 1;
					}
				}
			}
		}
	}
	else{
		$validToken = 1;
	}
	
	return $validToken;
	
	}

	
/* 	function  SendSingleSMS( $destimobile, $Message)
	{
		$APIDetails = "http://push1.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener?userId=vesalt&pass=vesalt&appid=vesalt&subappid=vesalt&contenttype=1&to=";
		$To = '';
		$From = "&from=VESTIG&text=";
		$Message = '';
		$RestOfString = "&selfid=true&alert=1&dlrreq=true";
		
		$uri = APIDetails + destimobile.Trim() + From.Trim() + Message + RestOfString;
		 UtilityClasses.HttpPost(uri, uri);
		lblMsg.Text = "SMS Sent Successfully!";
		  
	
	} */
	
}


?>