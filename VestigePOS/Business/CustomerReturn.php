<?php
    Class CustomerReturn {
    	
    	var $vestigeUtil;
    	function __construct()
    	{
    		$this->vestigeUtil = new VestigeUtil();
    	}
    	
     Function CRStatus(){
    	  	$connectionString = new DBHelper();
    	  Try{	
    	  	$pdo_object = $connectionString->dbConnection();
    	  	
    	  	$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='CUSTOMERRETURNSTATUS'
				And isactive=	1
			Order By
				sortorder Asc");
    	  	$stmt->execute();
    	  	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	   $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
											
						return $outputData;
					}
					catch(Exception $e)
					{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
					}
    	  }

    	 
      Function CRCustomerType(){
    	  	$connectionString = new DBHelper();
    	  	
    	  	$pdo_object = $connectionString->dbConnection();
    	  try{	
    	  	$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='CUSTOMERRETURNTYPE'
				And isactive=	1
			Order By
				sortorder Asc");
    	  	$stmt->execute();
    	  	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	
    	  	$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
    	  		
    	  	return $outputData;
    	  	}
    	  	catch(Exception $e)
    	  	{
    	  		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  	
    	  		return $exception;
    	  	}
    	  }
     Function CRApproveBy(){
    	  	$connectionString = new DBHelper();
    	  	try{
    	  	$pdo_object = $connectionString->dbConnection();
    	  	
    	  	$stmt = $pdo_object->prepare("Select	
					'-1'  UserId,
					'Select'  UserName,
					'Select'  Name
			Union All
			Select UserId, UserName, FirstName + ' ' + LastName as Name 
			From User_Master with (NOLOCK)
			Where Status = 1");
    	  	$stmt->execute();
    	  	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  	 $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
											
						return $outputData;
					}
					catch(Exception $e)
					{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
					}
    	  	
    	  }
      Function CRSearch($LocationId,$ReturnNumber,$FromDate,$ToDate,$StatusId,$ApprovedBy,$CustomerType){
    
    	  	$connectionString = new DBHelper();
    	  try{
    	  	$pdo_object = $connectionString->dbConnection();
    	  $sql="SELECT	ReturnNo, CustomerType,pm.KeyValue1 CustomerTypeValue, DistributorPCId, rch.LocationId,rch.MakePVBVZero, 
				lm.Name As LocationName, ApprovedDate,  ISNULL(ApprovedBy,-1) ApprovedBy,rch.Remarks Remark,
				FirstName + ' ' +LastName ApprovedName, ISNULL(TotalAmount,0) TotalAmount, rch.ModifiedDate,
				pm_1.KeyValue1 As StatusName, StatusId,
				ISNULL(DeductionAmount,0) DeductionAmount , ISNULL(AmountPayable,0) AmountPayable, 
				c.DistributorId, c.InvoiceDate,VoucherNo,TypeOfPayment
		FROM dbo.RetCustomer_Header rch with (NOLOCK)

		LEFT JOIN CIHeader (NOLOCK) c --Added by Kaushik
		ON c.InvoiceNo = rch.DistributorPCId

		INNER JOIN Parameter_Master pm with (NOLOCK)
		ON pm.ParameterCode = 'CUSTOMERRETURNTYPE'
		AND pm.KeyCode1 = rch.CustomerType

		INNER JOIN Location_Master lm with (NOLOCK)
		ON lm.LocationId = rch.LocationId

		INNER JOIN Parameter_Master pm_1 with (NOLOCK)
		ON pm_1.ParameterCode = 'CUSTOMERRETURNSTATUS'
		AND pm_1.KeyCode1 = rch.StatusId

		LEFT JOIN User_Master um with (NOLOCK)
		ON rch.ApprovedBy = um.UserId

		Where	(IsNull('$LocationId','-1')='-1' Or rch.LocationId = '$LocationId')
		And		(IsNull(NullIf('$ReturnNumber',''),'-1')='-1' Or rch.ReturnNo Like '%$ReturnNumber%')
		AND		(IsNull('$FromDate','')='' OR Convert(varchar(10),IsNull(rch.CreatedDate,'2099-01-01'),112) >= Convert(varchar(10),CAST('$FromDate' As DateTime),112))
		AND		(IsNull('$ToDate','')='' OR Convert(varchar(10),IsNull(rch.CreatedDate,'1900-01-01'),112) <= Convert(varchar(10),Cast('$ToDate' As DateTime),112))
		AND		(IsNull('$StatusId','-1')='-1' OR rch.StatusId = '$StatusId')
		AND		(IsNull('$ApprovedBy','-1')='-1' Or rch.ApprovedBy = '$ApprovedBy')
		AND		(IsNull('$CustomerType','-1')='-1' Or rch.CustomerType = '$CustomerType')
		ORDER BY rch.ReturnNo desc" ;
    	  
    	
    	  	$stmt = $pdo_object->prepare($sql);
    	  	
    	  	$stmt->execute();
    	  	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	   $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
											
						return $outputData;
					}
					catch(Exception $e)
					{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
					}
    	  }

    	  
     Function searchForInvoiceHeader($InvoiceNo,$type){
       	$stmt ='';
    	  	$connectionString = new DBHelper();
    	  	try{
    	  		$pdo_object = $connectionString->dbConnection();
    	  if($type==1){
    	  		$stmt = $pdo_object->prepare("
                       SELECT [InvoiceNo]
    	  				,[InvoiceDate]
    	  				,[CustomerOrderNo]
    	  				,[LogNo]
    	  				,[PCId]
    	  				,[BOId]
    	  				,[TINNo]
    	  				,CIH.[DistributorId]
    	  				,[TaxAmount]
    	  				,[InvoiceAmount]
    	  				,[DiscountAmount]
    	  				,[PaymentAmount]
    	  				,[ChangeAmount]
    	  				,[TotalBV]
    	  				,[TotalPV]
    	  				,[Status]
    	  				,[DeliveryMode]
    	  				,[IsProcessed]
    	  				,CIH.[CreatedBy]
    	  				,CIH.[CreatedDate]
    	  				,CIH.[ModifiedBy]
    	  				,CIH.[ModifiedDate]
    	  				,[StaffId]
    	  				,[DeliveryToId]
    	  				,[DeliverToAddressLine1]
    	  				,[DeliverToAddressLine2]
    	  				,[DeliverToAddressLine3]
    	  				,[DeliverToAddressLine4]
    	  				,[DeliverToCityId]
    	  				,[DeliverToPincode]
    	  				,[DeliverToStateId]
    	  				,[DeliverToCountryId]
    	  				,[DeliverToTelephone]
    	  				,[DeliverToMobile]
    	  				,[DeliverfromId]
    	  				,[DeliverFromAddress1]
    	  				,[DelverFromAddress2]
    	  				,[DeliverFromAddress3]
    	  				,[DeliverFromAddress4]
    	  				,[DeliverFromCityId]
    	  				,[DeliverFromPincode]
    	  				,[DeliverFromStateId]
    	  				,[DeliverFromCountryId]
    	  				,[DeliverFromTelephone]
    	  				,[DeliverFromMobile]
    	  				,[TotalUnits]
    	  				,[TotalWeight]
    	  				,[InvoicePrinted]
    	  				,[OrderMode]
    	  				,[TerminalCode]
    	  				,[InvoiceConsiderDate]
    	  				,[IsProcessedForBonus]
    	  				,[FileInvoNo]
    	  				,[TaxJurisdictionId],
    	  				DM.DistributorFirstName+' '+DM.DistributorLastName Name,
    	  				(select  top 1 ispromo from cidetail (nolock) where InvoiceNo='$InvoiceNo' and IsPromo=1) as IsPromo,
    	  				CASE WHEN (CONVERT(VARCHAR(16), (select MonthEndDate from BusinessMonth(nolock) where Status=1), 120)) = 
						CONVERT(VARCHAR(16), (select InvoiceConsiderDate from CIHeader where InvoiceNo='$InvoiceNo'), 120)
                        then 1 ELSE 0 END MakePVBYZero
    	  				FROM Parameter_Master PM(nolock),[dbo].[CIHeader] CIH with (NOLOCK)
    	  				LEFT JOIN DistributorMaster DM with (NOLOCK)
    	  				ON CIH.DistributorId=DM.DistributorId
    	  			   where InvoiceDate>DATEADD(DAY,-(KeyCode1),GETDATE()) AND PM.ParameterCode='CUSTOMERINVRET' AND
    	  				 InvoiceNo='$InvoiceNo' and InvoiceNo not in (select invoiceno from CODPayments with (nolock))
    	  			union all	 
    	  			  SELECT [InvoiceNo]
    	  				,[InvoiceDate]
    	  				,[CustomerOrderNo]
    	  				,[LogNo]
    	  				,[PCId]
    	  				,[BOId]
    	  				,[TINNo]
    	  				,CIH.[DistributorId]
    	  				,[TaxAmount]
    	  				,[InvoiceAmount]
    	  				,[DiscountAmount]
    	  				,[PaymentAmount]
    	  				,[ChangeAmount]
    	  				,[TotalBV]
    	  				,[TotalPV]
    	  				,[Status]
    	  				,[DeliveryMode]
    	  				,[IsProcessed]
    	  				,CIH.[CreatedBy]
    	  				,CIH.[CreatedDate]
    	  				,CIH.[ModifiedBy]
    	  				,CIH.[ModifiedDate]
    	  				,[StaffId]
    	  				,[DeliveryToId]
    	  				,[DeliverToAddressLine1]
    	  				,[DeliverToAddressLine2]
    	  				,[DeliverToAddressLine3]
    	  				,[DeliverToAddressLine4]
    	  				,[DeliverToCityId]
    	  				,[DeliverToPincode]
    	  				,[DeliverToStateId]
    	  				,[DeliverToCountryId]
    	  				,[DeliverToTelephone]
    	  				,[DeliverToMobile]
    	  				,[DeliverfromId]
    	  				,[DeliverFromAddress1]
    	  				,[DelverFromAddress2]
    	  				,[DeliverFromAddress3]
    	  				,[DeliverFromAddress4]
    	  				,[DeliverFromCityId]
    	  				,[DeliverFromPincode]
    	  				,[DeliverFromStateId]
    	  				,[DeliverFromCountryId]
    	  				,[DeliverFromTelephone]
    	  				,[DeliverFromMobile]
    	  				,[TotalUnits]
    	  				,[TotalWeight]
    	  				,[InvoicePrinted]
    	  				,[OrderMode]
    	  				,[TerminalCode]
    	  				,[InvoiceConsiderDate]
    	  				,[IsProcessedForBonus]
    	  				,[FileInvoNo]
    	  				,[TaxJurisdictionId],
    	  				DM.DistributorFirstName+' '+DM.DistributorLastName Name,
    	  				(select  top 1 ispromo from cidetail (nolock) where InvoiceNo='$InvoiceNo' and IsPromo=1) as IsPromo,
    	  				CASE WHEN (CONVERT(VARCHAR(16), (select MonthEndDate from BusinessMonth(nolock) where Status=1), 120)) = 
						CONVERT(VARCHAR(16), (select InvoiceConsiderDate from CIHeader where InvoiceNo='$InvoiceNo'), 120)
                        then 1 ELSE 0 END MakePVBYZero
    	  				FROM Parameter_Master PM(nolock),[dbo].[CIHeader] CIH with (NOLOCK)
    	  				LEFT JOIN DistributorMaster DM with (NOLOCK)
    	  				ON CIH.DistributorId=DM.DistributorId
    	  			   where InvoiceDate>DATEADD(DAY,-(KeyCode1),GETDATE()) AND PM.ParameterCode='CUSTOMERINVRET' AND
    	  				 InvoiceNo='$InvoiceNo' and InvoiceNo  in (select invoiceno from CODPayments with (nolock) where IsPaymentReceived=1)");
    	  }
    	  else{
    	 	$stmt = $pdo_object->prepare("	 
    	  			  SELECT [InvoiceNo]
    	  				,[InvoiceDate]
    	  				,[CustomerOrderNo]
    	  				,[LogNo]
    	  				,[PCId]
    	  				,[BOId]
    	  				,[TINNo]
    	  				,CIH.[DistributorId]
    	  				,[TaxAmount]
    	  				,[InvoiceAmount]
    	  				,[DiscountAmount]
    	  				,[PaymentAmount]
    	  				,[ChangeAmount]
    	  				,[TotalBV]
    	  				,[TotalPV]
    	  				,[Status]
    	  				,[DeliveryMode]
    	  				,[IsProcessed]
    	  				,CIH.[CreatedBy]
    	  				,CIH.[CreatedDate]
    	  				,CIH.[ModifiedBy]
    	  				,CIH.[ModifiedDate]
    	  				,[StaffId]
    	  				,[DeliveryToId]
    	  				,[DeliverToAddressLine1]
    	  				,[DeliverToAddressLine2]
    	  				,[DeliverToAddressLine3]
    	  				,[DeliverToAddressLine4]
    	  				,[DeliverToCityId]
    	  				,[DeliverToPincode]
    	  				,[DeliverToStateId]
    	  				,[DeliverToCountryId]
    	  				,[DeliverToTelephone]
    	  				,[DeliverToMobile]
    	  				,[DeliverfromId]
    	  				,[DeliverFromAddress1]
    	  				,[DelverFromAddress2]
    	  				,[DeliverFromAddress3]
    	  				,[DeliverFromAddress4]
    	  				,[DeliverFromCityId]
    	  				,[DeliverFromPincode]
    	  				,[DeliverFromStateId]
    	  				,[DeliverFromCountryId]
    	  				,[DeliverFromTelephone]
    	  				,[DeliverFromMobile]
    	  				,[TotalUnits]
    	  				,[TotalWeight]
    	  				,[InvoicePrinted]
    	  				,[OrderMode]
    	  				,[TerminalCode]
    	  				,[InvoiceConsiderDate]
    	  				,[IsProcessedForBonus]
    	  				,[FileInvoNo]
    	  				,[TaxJurisdictionId],
                         (select isnull(sum(PaymentAmount),0) from CIPayment(nolock) where InvoiceNo='$InvoiceNo' and TenderType=5) voucherAmt,
    	  				DM.DistributorFirstName+' '+DM.DistributorLastName Name,
    	  				CASE WHEN (CONVERT(VARCHAR(16), (select MonthEndDate from BusinessMonth(nolock) where Status=1), 120)) = 
						CONVERT(VARCHAR(16), (select InvoiceConsiderDate from CIHeader where InvoiceNo='$InvoiceNo'), 120)
                        then 1 ELSE 0 END MakePVBYZero
    	  				FROM Parameter_Master PM,[dbo].[CIHeader] CIH with (NOLOCK)
    	  				LEFT JOIN DistributorMaster DM with (NOLOCK)
    	  				ON CIH.DistributorId=DM.DistributorId
    	  			   where InvoiceDate>DATEADD(DAY,-(KeyCode1),GETDATE()) AND PM.ParameterCode='CUSTOMERINVRET' AND
    	  				 InvoiceNo='$InvoiceNo' and InvoiceNo  in (select invoiceno from CODPayments with (nolock) where IsPaymentReceived=0)");
    	  }
    	  		$stmt->execute();
    	  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');

    	  
    	  		return $outputData;
    	  	}
    	  	catch(Exception $e)
    	  	{
    	  		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  		 
    	  		return $exception;
    	  	}
    	  }
    Function searchForRetDetail($ReturnNo){
    	  
    	  	$connectionString = new DBHelper();
    	  	try{
    	  		$pdo_object = $connectionString->dbConnection();
    	  		 
					    	  		$stmt = $pdo_object->prepare("select RCBD.ReturnNo,IBD.ExpDate,RCBD.ItemId,RCBD.Quantity,RCBD.BatchNo,
											   IBD.ManufactureBatchNo ,IM.ItemName,Im.DistributorPrice DP,IM.ItemCode
												 from RetCustomerBatch_Detail RCBD with (NOLOCK)
												 LEFT JOIN ItemBatch_Detail IBD with (NOLOCK) ON 
												 IBD.BatchNo=RCBD.BatchNo
												 LEFT JOIN Item_Master IM
												 ON IM.ItemId=RCBD.ItemId
												 where ReturnNo='$ReturnNo'");
    	  						$stmt->execute();
    	  						$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  						$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
    	  							
    	  						return $outputData;
    	  						}
    	  						catch(Exception $e)
    	  						{
    	  						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  
    	  						return $exception;
    	  					}
    	      	  }

    function searchForInvoiceItemDetail($InvoiceNo,$type){
    	  	$connectionString = new DBHelper();
    	  	$stmt='';
    	  	try{
    	  		$pdo_object = $connectionString->dbConnection();
    	  	if($type==4){
    	  		$stmt = $pdo_object->prepare("
				       SELECT InvoiceNo,
				       DisplayName,
				      [ItemCode]
				      ,[ItemName],
				        CAST(ROUND( (SUM(Quantity)- ISNULL(RCDQuantity,0)),1,1) AS DECIMAL(18, 0))  Quantity
				      ,[DP]   
				      ,SUM(IsPromo
				      *Quantity) IsPromo
				     
				  FROM CIDetail CI with (NOLOCK)
				  LEFT JOIN
				  (  
				  SELECT SUM(RD.ReturnQty) RCDQuantity,RD.ItemId FROM RetCustomer_Detail RD(nolock) LEFT JOIN RetCustomer_Header RCH(nolock) ON RD.ReturnNo=RCH.ReturnNo
				   WHERE RCH.DistributorPCId='$InvoiceNo' GROUP BY ItemId ) RCD
				   ON RCD.ItemId=CI.ItemId
				    where InvoiceNo='$InvoiceNo'  
				  Group By InvoiceNo,ItemCode,ItemName,DP,DisplayName,RCDQuantity order By DisplayName
				  ");
    	  	}
    	  	else{
    	  		$stmt = $pdo_object->prepare("
    	  				Select IM.ItemCode ,IBD.ManufactureBatchNo,IM.ItemName ItemDescription ,IBD.BatchNo,CIBD.Quantity ReturnQty,IBD.MfgDate,
      IBD.ExpDate,Case when CID.Ispromo=1 then 1 else (CID.DP) end DistributorPrice from CiDetail CID with (nolock) INNER JOIN 
      CIBatchDetail CIBD  with (NOLOCK) on CID.INvoiceNo=CIBD.invoiceNO and CIBD.RecordNo=CID.RecordNo and CID.ItemId=CIBD.ItemId INNER JOIN
      Item_Master IM with (NOLOCK) ON IM.ItemId=CIBD.ItemId
      INNER JOIN ItemBatch_Detail IBD ON IBD.BatchNo=CIBD.BatchNo 
       WHere CID.InvoiceNo='$InvoiceNo'

    	  				");
    	  	}
    	  		$stmt->execute();
    	  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
    	  	
    	  		return $outputData;
    	  	}
    	  	catch(Exception $e)
    	  	{
    	  		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  		 
    	  		return $exception;
    	  	}	 
    	  }
 	  
    function searchForExpDate($ItemCode,$mgfNo,$locationId){
    	  	$connectionString = new DBHelper();
    	  	try{
    	  		$pdo_object = $connectionString->dbConnection();
				$sql="select ExpDate,GETDATE() TodayDate,BatchNo from ItemBatch_Detail IBD with (NOLOCK) Left Join Item_Master IM with (NOLOCK) ON IM.ItemId=IBD.ItemId where IM.ItemCode='$ItemCode'
    	  				 AND ManufactureBatchNo='$mgfNo'";
						 
				$stmt = $pdo_object->prepare($sql);
    	  		$stmt->execute();
    	  	
    	  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
    	  
    	  		return $outputData;
    	  	}
    	  	catch(Exception $e)
    	  	{
    	  		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  
    	  		return $exception;
    	  	}
    	  }
    	  
    	 
    	  
     function searchItemFortype1($distributorId,$ItemCode,$type,$pcId){
		    	  	$connectionString = new DBHelper();
		    	  	try{
		    	  		$pdo_object = $connectionString->dbConnection();
					    	  	if($type==1){
					    	  		 /* $sql="SELECT SUM(CID.Quantity) Quantity,CID.DP,CID.ItemCode,CID.ItemName from CIHeader CIH with (NOLOCK) LEFT JOIN CIDetail CID with (NOLOCK) ON
									CID.InvoiceNo=CIH.InvoiceNo
									 WHERE  CIH.DistributorId='$distributorId' AND
					    	  	     CID.ItemCode='$ItemCode' GROUP BY CID.DP,CID.ItemCode,CID.ItemName
					    	  		";  */
					    	  		 
					    	  		 $sql="select ItemCode,DistributorPrice AS DP,ItemName,0 as Quantity from Item_Master where Status = 1 and ItemCode = '$ItemCode'";
					    	  		 
					    	  		//$sql="SELECT 15000 Quantity,15000 DP,'$ItemCode' ItemCode,ItemName from Item_Master IM where IM.ItemCode='$ItemCode'";
					    	  	}
					    	  	
					    	  	//To update - Parveen
					    	  	//Query need to chnage to allow all item quantity from inventory loc bucket batch instead from 
					    	  	//ciheader and cidetail.
					    	  	
					    	  	else {
					    	  		/* $sql="SELECT SUM(CID.Quantity) Quantity,CID.DP,CID.ItemCode,CID.ItemName from CIDetail CID LEFT JOIN CIHeader CIH ON
					    	  		CID.InvoiceNo=CIH.InvoiceNo
					    	  		LEFT JOIN Location_Master LM with (NOLOCK)
					    	  		ON 
					    	  		CIH.PCId=LM.LocationId
					    	  		WHERE LM.LocationCode='$pcId'
					    	  		AND CID.ItemCode='$ItemCode' GROUP BY CID.DP,CID.ItemCode,CID.ItemName
					    	  		"; */

					    	  		$sql="select ItemCode,DistributorPrice AS DP,ItemName,0 as Quantity from Item_Master where Status = 1 and ItemCode = '$ItemCode'";
					    	  		
					    	  		
					    	  	}
					    	  	
					    	  	
					    	  	
		    	  		$stmt = $pdo_object->prepare($sql);
		    	  			
		    	  		$stmt->execute();
		    	  
		    	  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		    	  		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
		    	  		 
		    	  		return $outputData;
		    	  	}
		    	  	catch(Exception $e)
		    	  	{
		    	  	$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
		    	  		 
		    	  		return $exception;
		    	  	}
    	  	}
    	  	
   	function searchItemForPromo($distributorId,$type,$pcId,$itemCode){
    	  		$connectionString = new DBHelper();
    	  		try{
    	  			$pdo_object = $connectionString->dbConnection();
    	  			
    	  		if($type==1){
    	  			$sql=" 		  SELECT  CID.Discount,CID.InvoiceNo,CIH.InvoiceDate,CID.Quantity,CID.DP,CID.ItemCode,CID.ItemName,CID.IsPromo
    	  			FROM CIDetail CID(nolock) LEFT JOIN CIHeader CIH with (NOLOCK) ON
    	  			CID.InvoiceNo=CIH.InvoiceNo
    	  			
    	  			LEFT JOIN 
    	  			  (SELECT  CID.Discount,CID.InvoiceNo,CIH.InvoiceDate,Quantity,CID.DP,CID.ItemCode,CID.ItemName,CID.IsPromo
    	  			FROM CIDetail CID LEFT JOIN CIHeader CIH with (NOLOCK) ON
    	  			CID.InvoiceNo=CIH.InvoiceNo
    	  		
    	  			WHERE CIH.DistributorId='$distributorId' AND 
    	  			CIH.InvoiceDate>DATEADD(day,-90,getdate())) TOD
    	  			ON TOD.InvoiceNo=CIH.InvoiceNo 
    	  			WHERE CIH.DistributorId='$distributorId' AND 
    	  			CIH.InvoiceDate>DATEADD(day,-90,getdate())  AND  CID.IsPromo=1 AND
    	  			TOD.ItemCode='$itemCode' 	";
    	  		}
    	  		 else IF($type==2){
    	  			$sql="		  SELECT  CID.Discount,CID.InvoiceNo,CIH.InvoiceDate,CID.Quantity,CID.DP,CID.ItemCode,CID.ItemName,CID.IsPromo
    	  			FROM CIDetail CID(nolock) LEFT JOIN CIHeader CIH(nolock) ON
    	  			CID.InvoiceNo=CIH.InvoiceNo
    	  			LEFT JOIN  Location_Master LM
    	  			ON LM.LocationId=CIH.PCId
    	  			LEFT JOIN 
    	  			  (SELECT  CID.Discount,CID.InvoiceNo,CIH.InvoiceDate,Quantity,CID.DP,CID.ItemCode,CID.ItemName,CID.IsPromo
    	  			FROM CIDetail CID(nolock) LEFT JOIN CIHeader CIH(nolock) ON
    	  			CID.InvoiceNo=CIH.InvoiceNo
    	  			LEFT JOIN  Location_Master LM
    	  			ON LM.LocationId=CIH.PCId
    	  			WHERE LM.LocationCode='$pcId' AND 
    	  			CIH.InvoiceDate>DATEADD(day,-90,getdate())) TOD
    	  			ON TOD.InvoiceNo=CIH.InvoiceNo 
    	  			WHERE LM.LocationCode='$pcId' AND 
    	  			CIH.InvoiceDate>DATEADD(day,-90,getdate())  AND  CID.IsPromo=1 AND
    	  			TOD.ItemCode='$itemCode'
    	  			
    	  			";
    	  		} 
    	  			$stmt = $pdo_object->prepare($sql);
    	  	
    	  			$stmt->execute();
    	  			 
    	  			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
    	  	
    	  			return $outputData;
    	  		}
    	  		catch(Exception $e)
    	  		{
    	  		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  	
    	  		return $exception;
    	  	}
    	  		}
    	  	
    	  	
    	  	
   	function 	searchForDistributorInfo($distributorId,$type,$pcId){
    	  		$connectionString = new DBHelper();
    	  		try{
    	  			$pdo_object = $connectionString->dbConnection();
    	  		if ($type==1){
    	  			$sql=" select DistributorFirstName+' '+DistributorLastName as Name ,LM.Name as LocationName,
					 DM.DistributorId,LM.LocationCode,(ISNULL(LM.Address1,'')+' '+ISNULL(LM.Address2,'')+' '+ISNULL(LM.Address3,'')+' '+ ISNULL(LM.Address4,'')) as LocationAddress  from 
					 DistributorMaster  DM with (NOLOCK)
					 LEFT JOIN  Location_Master LM with (NOLOCK)
					 ON LM.LocationId=DM.LocationId
					 where DM.DistributorId='$distributorId'";
    	  			
    	  		}
    	  		else{
    	  			$sql=" 
					 select LM.LocationCode,LM.Name as LocationName,(ISNULL(LM.Address1,'')+' '+ISNULL(LM.Address2,'')+' 
					 '+ISNULL(LM.Address3,'')+' '+ ISNULL(LM.Address4,'')) as LocationAddress    from Location_Master LM with (NOLOCK) where LocationType=4 AND status=1 AND LocationCode
    	  			='$pcId'" ;
    	  		}
    	  			$stmt = $pdo_object->prepare($sql);
    	  	
    	  			$stmt->execute();
    	  			 
    	  			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	  			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
    	  	
    	  			return $outputData;
    	  		}
    	  		catch(Exception $e)
    	  		{
    	  		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
    	  	
    	  		return $exception;
    	  	}
    	  		}
    	  
	  function saveReturn($InvoiceNO ,$ReturnNo	,$customerType ,$jsonForItems ,$isPVBVZero   ,$Remarks ,
		    	  		$Status ,$locationId ,$createdBy,$PayableAmt,$DeductionAmt,$paymentMode  ){
		    	  	 
	  	
	  /* 	 $file = fopen("D://checkingFile_saveReturn.txt", "w");
	  	fwrite($file, $InvoiceNO.','.$ReturnNo.','.$customerType.','.$jsonForItems.','.$isPVBVZero.','.$Remarks.','.$Status.','.$locationId.','.$createdBy.','.$PayableAmt
	  	.','.$DeductionAmt);
	  	fclose($file); */
	  	
		    	  	try{

		    	  		$outParam='';
		    	  		$connectionString = new DBHelper();
		    	  		$pdo_object = $connectionString->dbConnection();
		    	  		$sql = "{CALL sp_CustomerReturnSave (@InvoiceNO=:InvoiceNO,@ReturnNo=:ReturnNo,@customerType=:customerType,
							@jsonForItems=:jsonForItems,@isPVBVZero=:isPVBVZero,@Remarks=:Remarks,@Status=:Status
							,@locationId=:locationId,@createdBy=:createdBy,@PayableAmt=:PayableAmt,@DeductionAmt=:DeductionAmt,@paymentMethod=:paymentMethod,@outParam=:outParam)}";
		    	  
		    	  		$stmt = $pdo_object->prepare($sql);
		    	  		$stmt->bindParam(':InvoiceNO',$InvoiceNO, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':ReturnNo',$ReturnNo, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':customerType',$customerType, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':jsonForItems',$jsonForItems, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':isPVBVZero',$isPVBVZero, PDO::PARAM_INT);
		    	  		$stmt->bindParam(':Remarks',$Remarks, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':Status',$Status, PDO::PARAM_INT);
		    	  		$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
		    	  		$stmt->bindParam(':createdBy',$createdBy, PDO::PARAM_INT);
		    	  		$stmt->bindParam(':PayableAmt',$PayableAmt, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':DeductionAmt',$DeductionAmt, PDO::PARAM_STR);
		    	  		$stmt->bindParam(':paymentMethod',$paymentMode, PDO::PARAM_INT);
		    	  		
		    	  		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
		    	  		 
		    	  		$stmt->execute();
		    	  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		    	  		 
		    	  		$saveToData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
		    	  
		    	  		return $saveToData;
		    	  	}
		    	  	catch(Exception $e){
		    	  
		    	  		$saveToData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
		    	  		 
		    	  		return $saveToData;
		    	  	}
		    	  	 
		    	  }
		    	   

    
    }
?>
