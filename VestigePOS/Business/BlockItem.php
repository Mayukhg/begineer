<?php


/* define('__PATH__', dirname(dirname(__FILE__)));
include(__PATH__.'/Common/VestigeUtil.php'); */
   Class BlockItem {
		   	var $vestigeUtil;
		   	function __construct()
		   	{
		   		$this->vestigeUtil = new VestigeUtil();
		   	}
   			/* function searchGRNNumber($GRNNumber,$destinationLocationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   				
   					$sql = "select GH.GRNNo,GH.AmendmentNo,GH.GRNDate,GH.PONumber,GH.PODate,
                            PM.KeyValue1 Status,GH.ChallanNo,
                            GH.InvoiceNo,GH.GrossWeight,
							GH.VehicleNo,GH.TransportName,GH.GatePassNo,GH.NoOfBoxes,GH.InvoiceTaxAmount,
							GH.InvoiceDate,GH.ChallanDate,GH.InvoiceAmount,VM.VendorCode,PH.VendorName,PH.V_Address1,
							PH.V_Address2,PH.V_Address3,PH.V_Address4,cm.CityName,sm.StateName,com.CountryName
							from GRN_Header GH
							INNER JOIN Parameter_Master PM ON PM.KeyCode1=GH.Status							
							INNER JOIN PO_Header PH	ON GH.PONumber=PH.PONumber
							INNER JOIN Vendor_Master VM on Ph.VendorId=vm.VendorId
							Inner Join City_Master cm on ph.V_City=cm.CityId
							Inner join State_Master sm on ph.V_State=sm.StateId	
							inner join Country_Master com on ph.V_Country=com.CountryId					
 							where PARAMETERCODE LIKE '%GRNSTATUS%' AND GRNNo='$GRNNumber'";
   					
   					$stmt = $pdo_object->prepare($sql);
   				
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						$mydata=$outputData;
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   					
   			
   			}
 */
   			
 	function itemSearchDetail($itemCode){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   						
   				 	$sql="select distinct ItemName from item_master with (NOLOCK) where itemCode='$itemCode' AND STATUS=1	" ;
   					$stmt = $pdo_object->prepare($sql);   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   					$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), ''); 
   				
   					}
   				catch(Exception $e)
   					{
   					$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());   				
   					}
   				
   				return $itemData;
   			
   	} 
   	
   	function locationDetail($itemCode){
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql="select distinct ISNULL(LM.[LocationId],'')[LocationId],
					ISNULL(LM.[LocationCode],'')[LocationCode],ISNULL(LM.[AssignedURL],'')[AssignedURL],
					ISNULL(LM.[Name],'')[Name] ,PM.KeyValue1 LocationType
					from  blockedItems bi
					inner join Location_Master LM with (NOLOCK) on bi.blockRef=lm.LocationId and lm.Status=1 and lm.locationType=3
					inner join Parameter_Master pm with (NOLOCK) on LM.LocationType=Pm.KeyCode1 and parameterCode like 'LOCATIONTYPE'
					where bi.itemCode='$itemCode' and lm.status=1
					
					Union
					
					select distinct ISNULL(LM.[LocationId],'')[LocationId],
					ISNULL(LM.[LocationCode],'')[LocationCode],ISNULL(LM.[AssignedURL],'')[AssignedURL],
					ISNULL(LM.[Name],'')[Name] ,PM.KeyValue1 LocationType from Location_Master LM with (NOLOCK)
					inner join Parameter_Master pm with (NOLOCK) on LM.LocationType=Pm.KeyCode1 and parameterCode like 'LOCATIONTYPE'
					where LocationId in
					(
						select distinct  lmm.ReplenishmentLocationId 
						from blockedItems bi with (NOLOCK)
						inner join Location_Master lmm with (NOLOCK) on bi.blockRef=lmm.DistributorId
						where bi.itemCode in ('$itemCode','-1') and  lmm.Status=1
					)
   					
   						Union
				select 452 [LocationId],'All India PUC' [LocationCode],'' [AssignedURL],'All India PUC' [Name],
				'All India PUC' LocationType
				from blockedItems bii with (NOLOCK) where bii.itemCode='$itemCode' and blockRef=452 " ;
   	
   			$stmt = $pdo_object->prepare($sql);
   				
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$locationData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		
   		}
   		catch(Exception $e)
   		{
   			$locationData= $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   	
   				
   		}
   		return $locationData;
   	
   	}
   	
   	function blockItemDetailOnLocation($locationId){
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql="select distinct bi.itemCode ItemCode,im.ItemId,im.ItemName,bi.blockFrom,pm.KeyValue1 BlockedLevel,
					bi.blockTill, ISNULL((select SUM(quantity) from inventory_locbucketbatch with (NOLOCK)
					where BucketId=5 and LocationId=$locationId and ItemId=IM.ItemId
					group by ItemId,ItemName,BucketId),0.0) as Quantity
					from blockedItems bi with (NOLOCK)
					inner join Item_Master im with (NOLOCK) on bi.itemCode=im.ItemCode
					inner Join Parameter_Master pm with (NOLOCK) on bi.blockedLevel=pm.KeyCode1 and ParameterCode like 'BLOCKITEMLEVEL'
					where bi.blockRef=$locationId   					
					Union					
					select distinct bi.itemCode ItemCode,im.ItemId,im.ItemName,bi.blockFrom,pm.KeyValue1 BlockedLevel,
					bi.blockTill, ISNULL((select SUM(quantity) from inventory_locbucketbatch with (NOLOCK)
					where BucketId=5 and LocationId=$locationId and ItemId=IM.ItemId
					group by ItemId,ItemName,BucketId),0.0) as Quantity
					from blockedItems bi with (NOLOCK)
					inner join Item_Master im with (NOLOCK) on bi.itemCode=im.ItemCode 
					inner Join Parameter_Master pm with (NOLOCK) on bi.blockedLevel=pm.KeyCode1 and ParameterCode like 'BLOCKITEMLEVEL'
					where bi.blockRef in ((select distributorid from Location_Master with (NOLOCK) where ReplenishmentLocationId=$locationId))   			
   					Union
   					select distinct 'All Item' ItemCode,-1 ItemId,'All Item' ItemName,
					bi.blockFrom,pm.KeyValue1 BlockedLevel,bi.blockTill, 0 Quantity
					from blockedItems bi with (NOLOCK)
					inner Join Parameter_Master pm with (NOLOCK) on bi.blockedLevel=pm.KeyCode1 and ParameterCode like 'BLOCKITEMLEVEL'
					where bi.blockRef in ((select distributorid from Location_Master with (NOLOCK) where ReplenishmentLocationId=$locationId)) and itemCode='-1' " ;
   	
   			$stmt = $pdo_object->prepare($sql);
   				
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$locationData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$locationData= $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   	
   				
   		}
   		return $locationData;
   	
   	}
   	
   	
   	function updateUrlOnLocation($locationCode,$locationName,$locationUrl)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql="update Location_Master set AssignedURL='$locationUrl' output inserted.LocationCode where LocationCode='$locationCode' and Name='$locationName'" ;
   			
   			$stmt = $pdo_object->prepare($sql);
   			//$stmt = sqlsrv_prepare( $pdo_object, $sql, array( &$locationUrl, &$locationCode, &$locationName,));
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$locationData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$locationData= $this->vestigeUtil->formatJSONResult('', $e->getMessage());   		
   				
   		}
   		return $locationData;
   		
   		
   	}
   	
  function 	saveBlockItemDetail($ItemCode,$ItemName,$ForLevel,$LocationCode,$LocationName,$UserType,$BlockUnblock,$Url)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   	
   			
   			   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		$outParam='';
   		$sql = "{CALL [dbo].[sp_BlockItemSave]( @ItemCode=:ItemCode,@ItemName=:ItemName	,@ForLevel=:ForLevel,@LocationCode=:LocationCode,
   				@LocationName=:LocationName,@UserType=:UserType,@BlockUnblock=:BlockUnblock,@Url=:Url,@outParam=:outParam)}";
   		
   		$stmt = $pdo_object->prepare($sql);
   		$stmt->bindParam(':ItemCode',$ItemCode, PDO::PARAM_STR);
   		$stmt->bindParam(':ItemName',$ItemName, PDO::PARAM_STR);
   		$stmt->bindParam(':ForLevel',$ForLevel, PDO::PARAM_STR);
   		$stmt->bindParam(':LocationCode',$LocationCode, PDO::PARAM_STR);
   		$stmt->bindParam(':LocationName',$LocationName, PDO::PARAM_STR);
   		$stmt->bindParam(':UserType',$UserType, PDO::PARAM_STR);
   		$stmt->bindParam(':BlockUnblock',$BlockUnblock, PDO::PARAM_STR);
   		$stmt->bindParam(':Url',$Url, PDO::PARAM_STR);
   		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
   		
   		$stmt->execute();
   		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   		if($results[0]['OutParam']=='ALRDB001')
   		{
   			throw new vestigeException('This item is already blocked for this location .');
   		}
   		if($results[0]['OutParam']=='BLKBO001')
   		{
   			throw new vestigeException('This item is already blocked on BO level .');
   		}
   		$saveData=$this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		/* $file = fopen("D://savedata.txt", "w");
   		fwrite($file,$saveData);
   		fclose($file); */
   		}
   		catch(Exception $e)
   		{
   			$saveData= $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   				
   		}
   		return $saveData;
   		
   		
   	}
   	
   	
   	
   	function searchLocationDetail($locationCode)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql="select LocationId,LocationType,LocationCode,Name,AssignedURL from location_master with (NOLOCK) where locationCode='$locationCode'and LocationType=3 and status=1" ;
   	
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$locationData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$locationData= $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   				
   		}
   		return $locationData;
   		 
   		 
   	}
   	
   	
   	function searchBIDetails($ItemCode,$ItemName,$ForLevel,$LocationCode,$LocationName,$UserType,$BlockUnblock)
   	{   		
   		if($ForLevel==-1)
   		{
   			$ForLevel='%%';
   		}
   		
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{

   				
   			$sql="select distinct ISNULL(LM.[LocationId],'')[LocationId],
					ISNULL(LM.[LocationCode],'')[LocationCode],ISNULL(LM.[AssignedURL],'')[AssignedURL],
					ISNULL(LM.[Name],'')[Name] ,PM.KeyValue1 LocationType
					from  blockedItems bi with (NOLOCK)
					inner join Location_Master LM with (NOLOCK) on bi.blockRef=lm.LocationId and lm.Status=1 and lm.locationType=3
					inner join Parameter_Master pm with (NOLOCK) on LM.LocationType=Pm.KeyCode1 and parameterCode like 'LOCATIONTYPE'
					where lm.status=1 
					and (ISNULL('$ItemCode','')='' OR bi.[itemCode]='$ItemCode') 
					and bi.[blockedLevel] like '$ForLevel'
					and bi.blockRef in (
					select LocationId from Location_Master lmm2 with (NOLOCK) where lmm2.status=1 and
					 (ISNULL('$LocationCode','')='' or lmm2.[LocationCode]='$LocationCode'))
					 
					 Union
					 
					 select distinct ISNULL(LM.[LocationId],'')[LocationId],
					ISNULL(LM.[LocationCode],'')[LocationCode],ISNULL(LM.[AssignedURL],'')[AssignedURL],
					ISNULL(LM.[Name],'')[Name] ,PM.KeyValue1 LocationType from Location_Master LM with (NOLOCK)
					inner join Parameter_Master pm with (NOLOCK) on LM.LocationType=Pm.KeyCode1 and parameterCode like 'LOCATIONTYPE'
					where  (ISNULL('$LocationCode','')='' OR LM.[LocationCode]='$LocationCode')
					and LocationId in
					(
						 select distinct  lmmm.ReplenishmentLocationId 
						from blockedItems bi with (NOLOCK)
						inner join Location_Master lmmm with (NOLOCK) on bi.blockRef=lmmm.DistributorId
						where lmmm.Status=1 and (ISNULL('$ItemCode','')='' OR bi.[itemCode]='$ItemCode')
						and bi.[blockedLevel] like '$ForLevel'
					)
				Union
				select 452 [LocationId],'All India PUC' [LocationCode],'' [AssignedURL],'All India PUC' [Name],
				'All India PUC' LocationType
				from blockedItems bii with (NOLOCK) where (ISNULL('$ItemCode','')='' OR bii.[itemCode]='$ItemCode') 
				and bii.[blockedLevel] like '$ForLevel'
				and blockRef=452	
					
   			 " ;
   	
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$locationData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$locationData= $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   				
   		}
   		return $locationData;
   	
   	
   	}
   	
   	
   	
   	
   	
  function blockItemLevels()
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql="  select 
					 -1 'KeyCode1',
					 'Select' 'keyvalue1'					
					  union all
					select
					KeyCode1,KeyValue1 
					from Parameter_Master with (NOLOCK) where parametercode like 'BLOCKITEMLEVEL' " ;
   		
   			$stmt = $pdo_object->prepare($sql);
   				
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$blockItemLevels= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$blockItemLevels= $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		
   				
   		}
   		return $blockItemLevels;
   	}
   	
   	
   	
   	
   	
   	
    
   
   }
?>

	
