<?php


/* define('__PATH__', dirname(dirname(__FILE__)));
include(__PATH__.'/Common/VestigeUtil.php'); */
   Class JoiningFormTracking {
		   	var $vestigeUtil;
		   	function __construct()
		   	{
		   		$this->vestigeUtil = new VestigeUtil();
		   	}
   			/* function searchGRNNumber($GRNNumber,$destinationLocationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   				
   					$sql = "select GH.GRNNo,GH.AmendmentNo,GH.GRNDate,GH.PONumber,GH.PODate,
                            PM.KeyValue1 Status,GH.ChallanNo,
                            GH.InvoiceNo,GH.GrossWeight,
							GH.VehicleNo,GH.TransportName,GH.GatePassNo,GH.NoOfBoxes,GH.InvoiceTaxAmount,
							GH.InvoiceDate,GH.ChallanDate,GH.InvoiceAmount,VM.VendorCode,PH.VendorName,PH.V_Address1,
							PH.V_Address2,PH.V_Address3,PH.V_Address4,cm.CityName,sm.StateName,com.CountryName
							from GRN_Header GH
							INNER JOIN Parameter_Master PM ON PM.KeyCode1=GH.Status							
							INNER JOIN PO_Header PH	ON GH.PONumber=PH.PONumber
							INNER JOIN Vendor_Master VM on Ph.VendorId=vm.VendorId
							Inner Join City_Master cm on ph.V_City=cm.CityId
							Inner join State_Master sm on ph.V_State=sm.StateId	
							inner join Country_Master com on ph.V_Country=com.CountryId					
 							where PARAMETERCODE LIKE '%GRNSTATUS%' AND GRNNo='$GRNNumber'";
   					
   					$stmt = $pdo_object->prepare($sql);
   				
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						$mydata=$outputData;
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   					
   			
   			}
 */
   			
 	function ValidateDistributorid($Distributorid){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   					
   				 	$sql="select distributorstatus from Distributormaster with (NOLOCK) where Distributorid='$Distributorid'" ;
   					$stmt = $pdo_object->prepare($sql);   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   					if($results==3 || $results==4)
   					{
   						throw new vestigeException("red",'Distributor is Blocked.');
   					}
   					$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), ''); 
   					}
   				catch(Exception $e)
   					{
   					$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());   				
   					}
   				
   				return $itemData;
   			
   	} 
   	
   	function SearchDistributorDetails($Distributorid)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		try{
   				
   			$sql = "select DM.Distributorid,DM.DistributorStatus,DTF.Isrecieved,(DistributorFirstName +' ' + DistributorLastName) Name,DM.DistributorMobNumber
,CONVERT(VARCHAR(11),DM.CreatedDate,106) DateOfJoining from DistributorMaster DM
Left join distributorformtracking DTF on DTF.Distributorid=DM.distributorid
where DM.distributorid='$Distributorid'";
   				
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   	
   		return $itemData;
   	}
   	
   	function SaveDistributordetails($Distributorid,$Remarks,$Repurchase,$DistContact,$createdBy,$locationId)
   	{
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		$Repurchase=1;
   		$vestigeUtil= New VestigeUtil();
   		$pcid =  $vestigeUtil->getSessionData("pickUpCentreId");
   		try{
   				
   			$sql = "{call SP_ApproveJoining(@Distributorid=:DistributorId,@ModifiedBy=:ModifiedBy,@Remarks=:Remarks,@IsRecieved=:Repurchase,@LocationId=:locationId,@PCid=:PCid,@DistContact=:DistContact,@outParam=:outParam)}";
   				
   			$stmt = $pdo_object->prepare($sql);
   			$stmt->bindParam(':DistributorId',$Distributorid ,PDO::PARAM_STR);
   			$stmt->bindParam(':ModifiedBy',$createdBy ,PDO::PARAM_STR);
   			$stmt->bindParam(':Remarks',$Remarks ,PDO::PARAM_STR);
   			$stmt->bindParam(':Repurchase',$Repurchase ,PDO::PARAM_STR);
   			$stmt->bindParam(':locationId',$locationId ,PDO::PARAM_STR);
			$stmt->bindParam(':PCid',$pcid ,PDO::PARAM_STR);
			$stmt->bindParam(':DistContact',$DistContact ,PDO::PARAM_STR);
   			$stmt->bindParam(':outParam', $outParam,PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,500);
   			$stmt->execute();
   			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$itemData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   			if($results[0]['OutParam']=='INVJ001')
   			{
   				throw new vestigeException('Distributor is not Registered yet.');
   			}
   			
   			else if($results[0]['find'] !='Duplicate')
   			{
   			
   			$sendMessage = new SendSMS();
   			$LocationName=$results[0]['LocationName'];
   			$RecievedBy=$results[0]['RecievedBy'];
   			$RecievedOn=$results[0]['RecievedOn'];
   			$response=$sendMessage->apicallFormTracking($DistContact,$LocationName,$RecievedBy,$RecievedOn);
   			
   			  $smsid1 = $results[0]['smsId'];
   			  $sql2="update SmsRequests set APIResponse='$response' where smsId=$smsid1";
   			  $stmt = $pdo_object->prepare($sql2);
   		      $stmt->execute();
   			
   			}
   			
   		}
   		catch(Exception $e)
   		{
   			$itemData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   	
   		}
   	
   		return $itemData;
   	}
   }
   	?>