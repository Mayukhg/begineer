<?php


/* define('__PATH__', dirname(dirname(__FILE__)));
include(__PATH__.'/Common/VestigeUtil.php'); */
   Class VendorReturn {
		   	var $vestigeUtil;
		   	function __construct()
		   	{
		   		$this->vestigeUtil = new VestigeUtil();
		   	}
   			function searchGRNNumber($GRNNumber,$destinationLocationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   				
   					$sql = "select GH.GRNNo,GH.AmendmentNo,GH.GRNDate,GH.PONumber,GH.PODate,
                            PM.KeyValue1 Status,GH.ChallanNo,
                            GH.InvoiceNo,GH.GrossWeight,
							GH.VehicleNo,GH.TransportName,GH.GatePassNo,GH.NoOfBoxes,GH.InvoiceTaxAmount,
							GH.InvoiceDate,GH.ChallanDate,GH.InvoiceAmount,VM.VendorCode,PH.VendorName,PH.V_Address1,
							PH.V_Address2,PH.V_Address3,PH.V_Address4,cm.CityName,sm.StateName,com.CountryName
							from GRN_Header GH
							INNER JOIN Parameter_Master PM ON PM.KeyCode1=GH.Status							
							INNER JOIN PO_Header PH	ON GH.PONumber=PH.PONumber
							INNER JOIN Vendor_Master VM on Ph.VendorId=vm.VendorId
							Inner Join City_Master cm on ph.V_City=cm.CityId
							Inner join State_Master sm on ph.V_State=sm.StateId	
							inner join Country_Master com on ph.V_Country=com.CountryId					
 							where PARAMETERCODE LIKE '%GRNSTATUS%' AND GRNNo='$GRNNumber'";
   					
   					$stmt = $pdo_object->prepare($sql);
   				
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						$mydata=$outputData;
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   					
   			
   			}

   			
   	function searchBatchDetail($ReturnNo,$GRNNo,$locationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   						
   		 $sql="	SELECT distinct ISNULL(GD.[GRNNo],'')[GRNNo]
		  ,ISNULL(GD.[PONumber],'')[PONumber]
		  ,ISNULL(ph.[VendorId],0)[VendorId]
		  ,ISNULL(ph.[VendorCode],0)[VendorCode]
		  ,ISNULL(GD.[SerialNo],0)[SerialNo]
		  ,ISNULL(GD.[ItemId],0)[ItemId]
		  ,ISNULL(GD.[ItemCode],'')[ItemCode]
		  ,ISNULL(ivll.IsItemVendorExcised,0)[IsItemVendorExcised]
		  ,ISNULL(GD.[ItemDescription],'')[ItemDescription]
		  ,ISNULL(GD.[POQty],0)[POQty]
		  ,ISNULL(GD.[AlreadyReceivedQty],0)[AlreadyReceivedQty] 
          ,ISNULL(GD.[InvoiceQty],0)[InvoiceQty]
		  ,ISNULL(GBD.[ReceivedQty],0)[ReceivedQty]
		  ,ISNULL(GBD.[BatchNumber],0)[BatchNumber]
		  ,ISNULL(GBD.[ManufacturingDate],'')[ManufacturingDate]
		  ,ISNULL(GBD.[ExpiryDate],'')[ExpiryDate]
		  ,ISNULL(GBD.[ManufacturerBatchNo],'')[ManufacturerBatchNo]
		  ,ISNULL((select ISNULL(ilb.[Quantity],0)[InStockQuantity] from inventory_locbucketbatch ilb 
				left join Item_Master im on ilb.ItemId=im.ItemId
				where ilb.LocationId='$locationId' and ilb.BatchNo=GBD.[BatchNumber] and BucketId=5),0) as InStockQuantity
		    ,ISNULL(pd.UnitPrice,0)[PerUnitPrice]
		   ,ISNULL(pd.UnitPrice+(pd.LineTaxAmount/pd.UnitQty),0)[UnitPriceWithTax]
		   ,ISNULL(pd.LineTaxAmount/pd.UnitQty,0)[UnitTax]
		  ,ISNULL((select ISNULL(rvbd.[ReturnQty],0)[ReturnQty] from RetVendorBatch_Detail  rvbd
					where rvbd.ReturnNo='$ReturnNo' and rvbd.BatchNo=GBD.[BatchNumber]),0) As [ReturnQty]
		  ,ISNULL((select isnull(sum(returnQty),0)[ReturnedQty] from RetVendorBatch_Detail rvbd
					inner join GRNBatch_Detail gbd1 on gbd1.BatchNumber=rvbd.BatchNo
					inner join RetVendor_Header rvh on rvh.returnNo=rvbd.returnNo and rvh.StatusId=3
					where gbd1.GRNNo='$GRNNo' and gbd1.BatchNumber=GBD.[BatchNumber]
					group by rvbd.BatchNo),0)
		  			 As [ReturnedQty] 
	,ISNULL(I.Weight,0)[ItemWeight]
	FROM [GRN_Detail]GD with (NOLOCK)
	JOIN [Item_Master]I with (NOLOCK) ON GD.[ItemId]=I.[ItemId]
	JOIN [GRNBatch_Detail]GBD with (NOLOCK) On GD.[GRNNo]=GBD.[GRNNo] AND GD.[ItemId]=GBD.[ItemId]
	inner join PO_Header ph on ph.PONumber=gd.PONumber
	inner join PO_Detail PD on pd.PONumber=GD.PONumber and pd.PONumber=ph.PONumber and PD.ItemId=GD.ItemId
	inner join ItemVendorLocation_Link ivll on ivll.VendorId=ph.VendorId and ivll.ItemId=gbd.ItemId
	WHERE (ISNULL('$GRNNo','')='' OR GD.[GRNNo]='$GRNNo')" ;

   					$stmt = $pdo_object->prepare($sql);
   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   					$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   					return $outputData;
   				}
   				catch(Exception $e)
   				{
   				$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   			
   				return $exception;
   				}
   			
   			
   				}
   				 
   	function searchGRNItemDetails($GRNNo,$locationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{

        $sql="	SELECT distinct ISNULL(GD.[GRNNo],'')[GRNNo]
		  ,ISNULL(GD.[PONumber],'')[PONumber]
		  ,ISNULL(ph.[VendorId],0)[VendorId]
		 ,ISNULL(ph.[VendorCode],0)[VendorCode]
		  ,ISNULL(GD.[SerialNo],0)[SerialNo]
		  ,ISNULL(GD.[ItemId],0)[ItemId]
		  ,ISNULL(GD.[ItemCode],'')[ItemCode]
		  ,ISNULL(ivll.IsItemVendorExcised,0)[IsItemVendorExcised]
		  ,ISNULL(GD.[ItemDescription],'')[ItemDescription]
		  ,ISNULL(GD.[POQty],0)[POQty]
		  ,ISNULL(GD.[AlreadyReceivedQty],0)[AlreadyReceivedQty] 
          ,ISNULL(GD.[InvoiceQty],0)[InvoiceQty]
		  ,ISNULL(GBD.[ReceivedQty],0)[ReceivedQty]
		  ,ISNULL(GBD.[BatchNumber],0)[BatchNumber]
		  ,ISNULL(GBD.[ManufacturingDate],'')[ManufacturingDate]
		  ,ISNULL(GBD.[ExpiryDate],'')[ExpiryDate]
		,ISNULL((select ISNULL(ilb.[Quantity],0)[InStockQuantity] from inventory_locbucketbatch ilb 
		left join Item_Master im on ilb.ItemId=im.ItemId
		where ilb.LocationId='$locationId' and ilb.BatchNo=GBD.[BatchNumber] and BucketId=5),0) as InStockQuantity 
		  ,ISNULL(GBD.[ManufacturerBatchNo],'')[ManufacturerBatchNo]
		   ,ISNULL(pd.UnitPrice,0)[PerUnitPrice]
		   ,ISNULL(pd.UnitPrice+(pd.LineTaxAmount/pd.UnitQty),0)[UnitPriceWithTax]
		   ,ISNULL(pd.LineTaxAmount/pd.UnitQty,0)[UnitTax]
		   ,ISNULL((select isnull(sum(returnQty),0)[ReturnedQty] from RetVendorBatch_Detail rvbd
					inner join GRNBatch_Detail gbd1 on gbd1.BatchNumber=rvbd.BatchNo
					inner join RetVendor_Header rvh on rvh.returnNo=rvbd.returnNo and rvh.StatusId=3
					where gbd1.GRNNo='$GRNNo' and gbd1.BatchNumber=GBD.[BatchNumber] and gbd1.ManufacturerBatchNo=GBD.ManufacturerBatchNo
					group by rvbd.BatchNo),0)
		  			 As [ReturnedQty] 
		  
		  ,ISNULL(I.Weight,0)[ItemWeight]
	FROM [GRN_Detail]GD with (NOLOCK)
	JOIN [Item_Master]I with (NOLOCK) ON GD.[ItemId]=I.[ItemId]
	JOIN [GRNBatch_Detail]GBD with (NOLOCK) On GD.[GRNNo]=GBD.[GRNNo] AND GD.[ItemId]=GBD.[ItemId]
	inner join PO_Header ph on ph.PONumber=gd.PONumber
	inner join PO_Detail PD on pd.PONumber=GD.PONumber and pd.PONumber=ph.PONumber and PD.ItemId=GD.ItemId
	inner join ItemVendorLocation_Link ivll on ivll.VendorId=ph.VendorId and ivll.ItemId=gbd.ItemId
	WHERE (ISNULL('$GRNNo','')='' OR GD.[GRNNo]='$GRNNo')" ;

   				

   					$stmt = $pdo_object->prepare($sql);
   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   			
   			
   			}
   			
   			
   function searchVR($TOFormData,$VRNo,$GRNNumber,$CreatedBy,$VendorID,$Status,$searchLocationID,$FromVRDate,$ToVRDate,$FromGRNDate,$ToGRNDate){
            	
            	$connectionString = new DBHelper();
            	$pdo_object = $connectionString->dbConnection();
            	parse_str($TOFormData,$output);
            	if($Status==-1)
            	{
            		$Status='';
            	}
            	if($VendorID==-1)
            	{
            		$VendorID='';
            	}
            	
            	Try{
            	$sql = "SELECT ISNULL(rvh.[ReturnNo],'')[ReturnNo],
            			ISNULL(rvh.[RetVendorDate],'')[RetVendorDate],ISNULL(rvh.[NoOfBoxes],0)[NoOfBoxes],ISNULL(rvh.[GrossWeight],0)[GrossWeight],
            			ISNULL(rvh.[TotalAmount],'')[TotalAmount],gh.GRNNo,gh.grnDate,GH.PONumber,GH.PODate,GH.ChallanNo,
						GH.InvoiceNo,gh.InvoiceDate,rvh.ShippingDetails,ISNULL(rvh.[OtherHandlingCharge],0)[HandlingCharge],
						ISNULL(rvh.[Quantity],0)[Quantity],rvh.DebitNoteNumber,
						ISNULL(rvh.[DebitNoteAmount],0)[DebitNoteAmount],
						ISNULL(rvh.[ShipmentDate],'')[ShipmentDate],lm.LocationCode,VM.VendorCode,PH.VendorName,PH.V_Address1,
						PH.V_Address2,PH.V_Address3,PH.V_Address4,CM2.CityName,SM2.StateName[V_StateName],CNM2.CountryName[V_CountryName],
						rvh.Remarks,PM.KEYVALUE1 Status,rvh.StatusId,pm1.KeyValue1 GRNStatus,
						ISNULL(PH.[V_Address1],'') +' '+ ISNULL(ph.[V_Address2],'') +' '+ ISNULL(PH.[V_Address3],'') +' '+ ISNULL(PH.[V_Address4],'') +' '+ CM2.CityName +' '+ SM2.StateName +' '+  CNM2.CountryName as address
						from  RetVendor_Header rvh
						inner join GRN_Header gh on rvh.GRNNo=gh.GRNNo
						INNER JOIN Location_Master lm on rvh.LocationId=lm.LocationId
						INNER JOIN PO_Header PH	ON GH.PONumber=PH.PONumber
						INNER JOIN Vendor_Master VM on PH.VendorId=Vm.VendorId
						INNER JOIN Parameter_Master pm on rvh.StatusId=pm.KeyCode1 and pm.ParameterCode='VENDORRETURNSTATUS'
						INNER JOIN Parameter_Master pm1 on gh.Status=pm1.KeyCode1 and pm1.ParameterCode='GRNSTATUS'
						
						LEFT JOIN City_Master CM2 with (NOLOCK) ON PH.[V_City]=CM2.[CityId]
						LEFT JOIN State_Master SM2 with (NOLOCK) ON PH.[V_State]=SM2.[StateId]
						LEFT JOIN Country_Master CNM2 with (NOLOCK) ON PH.[V_Country]=CNM2.[CountryId]
						where (ISNULL('$VRNo','')='' OR RVH.[ReturnNo] LIKE '%$VRNo')
						AND (ISNULL('$GRNNumber','')='' OR rvh.[GRNNo] LIKE '%$GRNNumber')
						AND (ISNULL('$Status','')='' OR rvh.[StatusId] LIKE '%$Status')
						AND (ISNULL('$VendorID','')='' OR rvh.[VendorId] LIKE '%$VendorID')
						AND (ISNULL('$FromVRDate','')=''  OR Convert(Varchar(8),IsNull(rvh.[retVendorDate],CAST('1900-01-01'AS DATETIME)),112)>=Convert(Varchar(8),Cast('$FromVRDate' as Datetime),112))
            			AND (ISNULL('$ToVRDate','')='' OR  Convert(Varchar(8),IsNull(rvh.[retVendorDate],CAST('2099-01-01'AS DATETIME)),112)<=Convert(Varchar(8),Cast('$ToVRDate' as Datetime),112))
            	 		AND (ISNULL('$FromGRNDate','')='' OR  Convert(Varchar(8),IsNull(GH.[GRNDate],CAST('1900-01-01'AS DATETIME)),112)>=Convert(Varchar(8),Cast('$FromGRNDate' as Datetime),112))
            			AND (ISNULL('$ToGRNDate','')='' OR  Convert(Varchar(8),IsNull(GH.[GRNDate],CAST('2099-01-01'AS DATETIME)),112)<=Convert(Varchar(8),Cast('$ToGRNDate' as Datetime),112))
            	";
            	
            	$stmt = $pdo_object->prepare($sql);
            	
            	$stmt->execute();
            	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            	
            	$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
            	
            	
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
            	 
            }
            
            
            
            
            
   	function VRStatus(){
   				$connectionString = new DBHelper();
   					
   				$pdo_object = $connectionString->dbConnection();
   			try{
   				$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='VENDORRETURNSTATUS'
				And isactive=	1
			Order By
				sortorder Asc");
   				$stmt->execute();
   				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				
   			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   			
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   				
   			}
   			

   		function searchVendor(){
   				$connectionString = new DBHelper();
   					
   				$pdo_object = $connectionString->dbConnection();
   			try{		
   				$stmt = $pdo_object->prepare("Select	
					'-1'  VendorId,
					'Select'  VendorName,
					'Select' VendorCode
			Union All
			Select	DISTINCT VM.VendorId, VM.VendorName, VM.VendorName +  ' [' + VM.VendorCode + ']' VendorCode 
			From ItemVendor_Link IV with (NOLOCK)
			RIGHT JOIN Vendor_Master VM with (NOLOCK)
				ON	IV.VendorId = VM.VendorId
			Where VM.Status = 1");
   				$stmt->execute();
   				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   }

   
   function saveVR($jsonForBatchDetail,$GRNNumber,$returnNO ,$Status ,$PONo  
				 ,$ShippingDetail ,$ShipmentDate ,$remarks,$TotalQuantity,$TotalAmount,$DebitNoteAmount,$HandlingCharge,
				 $receiveBy,$VendorCode,$NoOfBoxes,$GrossWeight,$locationId ,$createdBy,$jsonForViewDetail ){
   
   	try{
   		

   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		$outParam='';
   		$sql = "{CALL sp_VendorReturnSave (@jsonForBatchDetail=:jsonForBatchDetail,@GRNNumber=:GRNNumber,@returnNO=:returnNO,
					@Status=:Status,@PONo=:PONo,@ShippingDetail=:ShippingDetail,@ShipmentDate=:ShipmentDate
					,@remarks=:remarks,@TotalQuantity=:TotalQuantity,@TotalAmount=:TotalAmount,@DebitNoteAmount=:DebitNoteAmount,@HandlingCharge=:HandlingCharge,
   					@receiveBy=:receiveBy,@VendorCode=:VendorCode,@NoOfBoxes=:NoOfBoxes,@GrossWeight=:GrossWeight,@locationId=:locationId,@createdBy=:createdBy
   				,@jsonForViewDetail=:jsonForViewDetail
   				,@outParam=:outParam)}";
   		
   		$stmt = $pdo_object->prepare($sql);
   		$stmt->bindParam(':jsonForBatchDetail',$jsonForBatchDetail, PDO::PARAM_STR);
   		$stmt->bindParam(':GRNNumber',$GRNNumber, PDO::PARAM_STR);
   		$stmt->bindParam(':returnNO',$returnNO, PDO::PARAM_STR);
   		$stmt->bindParam(':Status',$Status, PDO::PARAM_INT);
   		$stmt->bindParam(':PONo',$PONoNo, PDO::PARAM_INT);
   		$stmt->bindParam(':ShippingDetail',$ShippingDetail, PDO::PARAM_STR);
   		$stmt->bindParam(':ShipmentDate',$ShipmentDate, PDO::PARAM_STR);
   		$stmt->bindParam(':remarks',$remarks, PDO::PARAM_STR);
   		$stmt->bindParam(':TotalQuantity',$TotalQuantity, PDO::PARAM_INT);
   		$stmt->bindParam(':TotalAmount',$TotalAmount, PDO::PARAM_STR);
   		$stmt->bindParam(':DebitNoteAmount',$DebitNoteAmount, PDO::PARAM_STR);
   		$stmt->bindParam(':HandlingCharge',$HandlingCharge, PDO::PARAM_STR);
   		$stmt->bindParam(':receiveBy',$receiveBy, PDO::PARAM_STR);
   		$stmt->bindParam(':VendorCode',$VendorCode, PDO::PARAM_STR);
   		$stmt->bindParam(':NoOfBoxes',$NoOfBoxes, PDO::PARAM_INT);
   		$stmt->bindParam(':GrossWeight',$GrossWeight, PDO::PARAM_STR);
   		$stmt->bindParam(':locationId',$locationId, PDO::PARAM_INT);
   		$stmt->bindParam(':createdBy',$createdBy, PDO::PARAM_INT);
   		$stmt->bindParam(':jsonForViewDetail',$jsonForViewDetail, PDO::PARAM_STR);
   		/* $stmt->bindParam(':GrossWeight',$GrossWeight, PDO::PARAM_INT); */
   		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
   		
   		$stmt->execute();
   		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   		
   		if($results[0]['OutParam']=='INF090')
   		{
   			throw new vestigeException('This return number already closed .');
   		}
   		if($results[0]['OutParam']=='INF091')
   		{
   			throw new vestigeException('This return number already Cancelled . ');
   		}
   		
   		if($results[0]['OutParam']=='INF0235')
   		{
   			throw new vestigeException('Vendor Return for this GRN order already open go to search and edit it .');
   		}
   		if($results[0]['OutParam']=='VAL0022')
   		{
   			throw new vestigeException('Return quantity exceeded from inventory, check available quantity in inventory .');
   		}
   		
   		 if(sizeof($results[0]['OutParam']) > 0)
			  		{
			  			throw new vestigeException($results[0]['OutParam']);
			  		}
	  		
		  		}
		  		catch(Exception $e){
		  			throw new Exception($e->getMessage());
		  		
		  		}
		  		return  $results ;  
   
   }
    
   
   }
?>

	
