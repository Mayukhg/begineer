<?php


/* define('__PATH__', dirname(dirname(__FILE__)));
include(__PATH__.'/Common/VestigeUtil.php'); */
   Class GRN {
		   	var $vestigeUtil;
		   	function __construct()
		   	{
		   		$this->vestigeUtil = new VestigeUtil();
		   	}
   			function searchPONumber($PONumber,$destinationLocationId){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   				
   					$sql = "exec   usp_POGRNSearch '<PurchaseOrder><FromDate>1900-01-01T00:00:00</FromDate>
   							<ToDate>1900-01-01T00:00:00</ToDate>  <LocationName /> <PONumber>$PONumber</PONumber >
   							  <AmendmentNo>0</AmendmentNo>  <POType>-1</POType>  <POTypeName />  <DestinationLocationID>$destinationLocationId</DestinationLocationID>  <DestinationLocationCode />  <VendorID>-1</VendorID>  <VendorCode />  <VendorName />  <VendorAddress>    <CountryId>0</CountryId>    <StateId>0</StateId>    <CityId>0</CityId>    <AddressModifiedDate>1900-01-01T00:00:00</AddressModifiedDate>  </VendorAddress>  <PaymentTerms />  <DeliveryAddress>    <CountryId>0</CountryId>    <StateId>0</StateId>    <CityId>0</CityId>    <AddressModifiedDate>1900-01-01T00:00:00</AddressModifiedDate>  </DeliveryAddress>  <PODate>1900-01-01T00:00:00</PODate>  <DisplayPODate>01-01-1900</DisplayPODate>  <ExpectedDeliveryDate>2013-10-08T16:58:15</ExpectedDeliveryDate>  <MaxDeliveryDate>2013-10-08T16:58:15</MaxDeliveryDate>  <Status>-1</Status>  <StatusName />  <Remarks />  <ShippingDetails />  <PaymentDetails />  <TotalTaxAmount>0</TotalTaxAmount>  <DisplayTotalTaxAmount>0</DisplayTotalTaxAmount>  <DBTotalTaxAmount>0</DBTotalTaxAmount>  <TotalPOAmount>0</TotalPOAmount>  <DisplayTotalPOAmount>0</DisplayTotalPOAmount>  <DBTotalPOAmount>0</DBTotalPOAmount>  <IsFormCApplicable>false</IsFormCApplicable>  <DisplayCreateDate>08-10-2013</DisplayCreateDate>  <TaxJurisdictionId>0</TaxJurisdictionId>  <CreatedBy>0</CreatedBy>  <CreatedDate>2013-10-08T00:00:00+05:30</CreatedDate>  <ModifiedBy>0</ModifiedBy>  <ModifiedDate>1900-01-01T00:00:00</ModifiedDate></PurchaseOrder>',''";

   					$stmt = $pdo_object->prepare($sql);
   				
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   					
   			
   			}

   			
   	function searchBatchDetail($GRNNo){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{
   						
   					$sql = "SELECT ISNULL(GBD.[GRNNo],'')[GRNNo]		
		  ,ISNULL(GBD.[SerialNo],0)[SerialNo]
		  ,ISNULL(GBD.RowNo,0)RowNo
		  ,ISNULL(GBD.[ItemId],0)[ItemId]
		  ,ISNULL(GD.[ItemCode],'')[ItemCode]		
		  ,ISNULL(GD.[ItemDescription],'')[ItemDescription]		
          ,ISNULL(GBD.[BatchNumber],'')[BatchNumber]	
		  ,[ManufacturingDate]	
		  ,ISNULL(GBD.[ManufacturerBatchNo],'')[ManufacturerBatchNo]	
		  ,[ExpiryDate]	
		  ,ISNULL(GBD.[MRP],0)[MRP]	
		  ,ISNULL(GBD.[ReceivedQty],0)[ReceivedQty]	
		  ,ISNULL(GBD.[InvoiceQty],0)[InvoiceQty]				 
			FROM [GRNBatch_Detail]GBD(nolock)
			JOIN GRN_Detail GD(nolock)
			On GBD.[GRNNo]=GD.[GRNNo] AND GD.[ItemId]=GBD.[ItemId]
			WHERE GBD.[GRNNo]='$GRNNo'";

   					$stmt = $pdo_object->prepare($sql);
   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   					$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
   			
   					return $outputData;
   				}
   				catch(Exception $e)
   				{
   				$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   			
   				return $exception;
   				}
   			
   			
   				}
   				 
   	function searchGRNItemDetails($PONumber,$GRNNo){
   				$connectionString = new DBHelper();
   				$pdo_object = $connectionString->dbConnection();
   				try{

   		If($GRNNo==''){
   					$sql = "
   							SELECT	DISTINCT ''[GRNNo],
					''[BatchNumber],
					PD.[PONumber],
					PD.[AmendmentNo],
					0[SerialNo],
					PD.[ItemId],
					I.[ItemCode],
					ISNULL(UM.[UOMName],'')[PurchaseUOM],
					ISNULL(PD.[ItemDescription],'')[ItemDescription],
					ISNULL([UnitQty],0)[POQty],
					ISNULL(GRN.[AlreadyReceivedQty],0)[AlreadyReceivedQty],
					ISNULL(GRN.AlreadyInvoicedQty,0) AlreadyInvoicedQty,
					ISNULL([UnitQty],0)-ISNULL(GRN.[AlreadyReceivedQty],0)[BalanceQty],
					0 [ChallanQty],
					'' [InvoiceQty],
					0 [ReceivedQty],
					(ISNULL((SELECT [KeyValue1] FROM [Parameter_Master] with (NOLOCK) where [ParameterCode]='VARIANCE' AND [IsActive]=1),0)*ISNULL([UnitQty],0))/100 + ISNULL([UnitQty],0) As [MaxQty],
					ISNULL(I.Weight,0)[ItemWeight]
			FROM [PO_Detail]PD with (NOLOCK)
			JOIN [PO_Header] PH1 with (NOLOCK) ON PD.[PONumber]=PH1.[PONumber] AND PD.[AmendmentNo]=PH1.[AmendmentNo]
			JOIN [Item_Master]I with (NOLOCK) ON PD.[ItemId]=I.[ItemId]
			JOIN [UOM_Master] UM with (NOLOCK) ON PD.[PurchaseUOM]=UM.UOMId			
			LEFT JOIN
			(
				SELECT sum(GD1.ReceivedQty) AlreadyReceivedQty,sum(GD1.InvoiceQty) AlreadyInvoicedQty, GD1.PONumber, GD1.ItemId
				FROM [GRN_Detail]  GD1(nolock)
				INNER JOIN GRN_Header gh with (NOLOCK)
				ON gh.GRNNo = GD1.GRNNo
				where GD1.[PONumber]= '$PONumber' AND GH.Status!=2	
				GROUP BY GD1.PONumber, GD1.ItemId
					
			)GRN
			ON PD.[PONumber]=GRN.[PONumber] 
			--AND PD.[AmendmentNo]=GRN.[AmendmentNo] 
			AND PD.[ItemId]=GRN.[ItemId]

			
			WHERE PD.[PONumber]='$PONumber' AND PD.AmendmentNo=(Select MAX([AmendmentNo]) from [PO_Header]PH(nolock) where PD.PONumber=PH.PONumber )
			AND PH1.Status in (1,3)
		--	AND ISNULL(GRN.[AlreadyReceivedQty],0)<ISNULL([UnitQty],0)

   							";
   		}
   		else {		
   					$sql="	SELECT ISNULL(GD.[GRNNo],'')[GRNNo]
		  ,ISNULL(GD.[PONumber],'')[PONumber]
		  ,ISNULL(GD.[AmendmentNo],0)[AmendmentNo]
		  ,ISNULL(GD.[SerialNo],0)[SerialNo]
		  ,ISNULL(GD.[ItemId],0)[ItemId]
		  ,ISNULL(GD.[ItemCode],'')[ItemCode]
		  ,ISNULL(GD.[PurchaseUOM],'')[PurchaseUOM]
		  ,ISNULL(GD.[ItemDescription],'')[ItemDescription]
		  ,ISNULL(GD.[POQty],0)[POQty]
		  ,ISNULL(GD.[AlreadyReceivedQty],0)[AlreadyReceivedQty]
		   ,ISNULL(GD.AlreadyInvoicedQty,0)[AlreadyInvoicedQty]
		  ,ISNULL(GD.[BalanceQty],0)[BalanceQty]
		  
          ,ISNULL(GD.[ChallanQty],0)[ChallanQty]
          ,ISNULL(GD.[InvoiceQty],0)[InvoiceQty]
		  ,ISNULL(GD.[ReceivedQty],0)[ReceivedQty]
		  ,(ISNULL((SELECT [KeyValue1] FROM [Parameter_Master] with (NOLOCK) 
		  where [ParameterCode]='VARIANCE' AND [IsActive]=1),0)*ISNULL(GD.[POQty],0))/100 + ISNULL(GD.[POQty],0)
		   As [MaxQty]
		  ,ISNULL(I.Weight,0)[ItemWeight]
	FROM [GRN_Detail]GD with (NOLOCK)
	JOIN [Item_Master]I with (NOLOCK) ON GD.[ItemId]=I.[ItemId]
--	JOIN [GRNBatch_Detail]GBD with (NOLOCK) On GD.[GRNNo]=GBD.[GRNNo] AND GD.[ItemId]=GBD.[ItemId]
	WHERE (ISNULL('$GRNNo','')='' OR GD.[GRNNo]='$GRNNo')
	AND (ISNULL('$PONumber','')='' OR GD.[PONumber]='$PONumber')" ;

   		}				

   					$stmt = $pdo_object->prepare($sql);
   						
   					$stmt->execute();
   					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   				$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   			
   			
   			}
            function searchGRN($GRNNo,$PONumber,$ReceivedBy,$VendorID,$Status,$LocationID,$FromGrnDate,$ToGrnDate,$FromPODate,$ToPODate){
            	
            	$connectionString = new DBHelper();
            	$pdo_object = $connectionString->dbConnection();
            	Try{
            	$sql = "SELECT 
			ISNULL([GRNNo],'')[GRNNo]
		  , [GRNDate]
		  ,ISNULL(GH.[PONumber],'')[PONumber]
		  ,ISNULL(GH.[AmendmentNo],'0')[AmendmentNo]
		  ,GH.PODate
		  ,ISNULL(GH.[ChallanNo],'')[ChallanNo]
		  ,[ChallanDate]
		  ,ISNULL([GrossWeight],'')[GrossWeight]
		  ,ISNULL([NoOfBoxes],'')[NoOfBoxes]
		  ,ISNULL([InvoiceNo],'')[InvoiceNo]
		  , [InvoiceDate]
		  ,ISNULL(InvoiceTaxAmount,0)InvoiceTaxAmount
		  ,ISNULL(InvoiceAmount,0)InvoiceAmount
		  ,ISNULL(GH.[ShippingDetails],'')[ShippingDetails]
		  ,ISNULL(GH.[VehicleNo],'')[VehicleNo]
		  ,ISNULL([ReceivedBy],'')[ReceivedBy]
		  ,ISNULL(GH.[Status],'-1')[Status]
		  ,ISNULL(PM.keyValue1,'')[StatusName]
		  ,ISNULL(GH.[CreatedBy],'0')[CreatedBy]
		  ,ISNULL(GH.[CreatedDate],'')[CreatedDate]
		  ,ISNULL(GH.[ModifiedBy],'0')[ModifiedBy]
		  ,ISNULL(GH.[ModifiedDate],'')[ModifiedDate]		  
		  ,ISNULL(PH.[VendorCode],'')[VendorCode]
		  ,ISNULL(PH.[VendorName],'')[VendorName]	
		  ,ISNULL(LM.LocationCode,'')LocationCode
		  ,ISNULL(PH.[D_Address1],'')  AS [D_Address1]
		  ,ISNULL(PH.[D_Address2],'')  AS [D_Address2]
		  ,ISNULL(PH.[D_Address3],'')  AS [D_Address3]
		  ,ISNULL(PH.[D_Address4],'')  AS [D_Address4]
		  ,ISNULL(PH.[D_City],'')  AS [D_City]
		  ,ISNULL(PH.[D_State],'')  AS [D_State]
		  ,ISNULL(PH.[D_Country],'')  AS [D_Country]
		  ,ISNULL(PH.[D_Phone1],'')  AS [D_Phone1]
		  ,ISNULL(PH.[D_Phone2],'')  AS [D_Phone2]
		  ,ISNULL(PH.[D_Mobile1],'')  AS [D_Mobile1]
		  ,ISNULL(PH.[D_Mobile2],'')  AS [D_Mobile2]
		  ,ISNULL(PH.[D_Fax1],'')  AS [D_Fax1]
		  ,ISNULL(PH.[D_Fax2],'')  AS [D_Fax2]
		  ,ISNULL(PH.[D_Email1],'')  AS [D_Email1]
		  ,ISNULL(PH.[D_Email2],'')  AS [D_Email2]
		  ,ISNULL(PH.[D_PinCode],'')  AS [D_PinCode]
		  ,ISNULL(PH.[D_Website],'')  AS [D_Website]
		  ,CM2.CityName[D_CityName],SM2.StateName[D_StateName],CNM2.CountryName[D_CountryName]
		  ,ISNULL(PH.V_Address1,'') + ' '+ ISNULL(PH.V_Address2,'') +' '+ ISNULL(PH.V_Address3,'')+ ' '+ ISNULL(PH.V_Address4,'') +' '+ ISNULL(CM3.CityName,'') +' '+ ISNULL(SM3.StateName,'') + ' ('+ ISNULL(PH.V_PinCode,'')+')' As VendorAddress
	  FROM [GRN_Header] GH with (NOLOCK)
	  JOIN [PO_Header] PH with (NOLOCK) ON GH.[PONumber]=PH.[PONumber] AND GH.[AmendmentNo]=PH.[AmendmentNo]
	  JOIN [Location_Master] LM with (NOLOCK) ON PH.D_LocationId=LM.LocationId
	  JOIN Parameter_Master PM with (NOLOCK) ON GH.[Status]=PM.KeyCode1
	  LEFT JOIN City_Master CM2 with (NOLOCK) ON PH.[D_City]=CM2.[CityId]
	  LEFT JOIN State_Master SM2 with (NOLOCK) ON PH.[D_State]=SM2.[StateId]
	  LEFT JOIN Country_Master CNM2 with (NOLOCK) ON PH.[D_Country]=CNM2.[CountryId]
	
	  LEFT JOIN City_Master CM3 with (NOLOCK) ON PH.V_City = CM3.CityID
	  LEFT JOIN State_Master SM3 with (NOLOCK) ON PH.V_State = SM3.StateId
	  Where PM.ParameterCode='GRNSTATUS' 
		AND (ISNULL('$GRNNo','')='' OR GH.[GRNNo] LIKE '%$GRNNo')
            	AND (ISNULL('$PONumber','')='' OR GH.[PONumber]LIKE '%$PONumber')
            	AND ('$VendorID'='' OR ISNULL('$VendorID',-1)=-1 OR PH.VendorId='$VendorID')
            	AND (ISNULL('$ReceivedBy','')='' OR GH.[ReceivedBy]='$ReceivedBy')
            	AND (ISNULL($Status,-1)=-1 OR GH.STATUS='$Status')
            	AND (ISNULL('$LocationID',-1)=-1 OR PH.D_LocationId='$LocationID')
            	AND (ISNULL('$FromGrnDate','')=''  OR Convert(Varchar(8),IsNull(GH.[GRNDate],CAST('1900-01-01'AS DATETIME)),112)>=Convert(Varchar(8),Cast('$FromGrnDate' as Datetime),112))
            	AND (ISNULL('$ToGrnDate','')='' OR  Convert(Varchar(8),IsNull(GH.[GRNDate],CAST('2099-01-01'AS DATETIME)),112)<=Convert(Varchar(8),Cast('$ToGrnDate' as Datetime),112))
            	 AND (ISNULL('$FromPODate','')='' OR  Convert(Varchar(8),IsNull(GH.[PODate],CAST('1900-01-01'AS DATETIME)),112)>=Convert(Varchar(8),Cast('$FromPODate' as Datetime),112))
            	AND (ISNULL('$ToPODate','')='' OR  Convert(Varchar(8),IsNull(GH.[PODate],CAST('2099-01-01'AS DATETIME)),112)<=Convert(Varchar(8),Cast('$ToPODate' as Datetime),112))
           
				ORDER BY  [GRNDate] ASC
            	
            	";

            	$stmt = $pdo_object->prepare($sql);
            		
            	$stmt->execute();
            	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            	$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
            	 
            }
   	function GRNStatus(){
   				$connectionString = new DBHelper();
   					
   				$pdo_object = $connectionString->dbConnection();
   			try{
   				$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='GRNStatus'
				And isactive=	1
			Order By
				sortorder Asc");
   				$stmt->execute();
   				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   				
   			}
   			

   		function searchVendor(){
   				$connectionString = new DBHelper();
   					
   				$pdo_object = $connectionString->dbConnection();
   			try{		
   				$stmt = $pdo_object->prepare("Select	
					'-1'  VendorId,
					'Select'  VendorName,
					'Select' VendorCode
			Union All
			Select	DISTINCT VM.VendorId, VM.VendorName, VM.VendorName +  ' [' + VM.VendorCode + ']' VendorCode 
			From ItemVendor_Link IV with (NOLOCK)
			RIGHT JOIN Vendor_Master VM with (NOLOCK)
				ON	IV.VendorId = VM.VendorId
			Where VM.Status = 1");
   				$stmt->execute();
   				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
   			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
   }

   
   function saveGRN($jsonForBatchDetail ,$PONumber	,$ambendMentNO ,$Status ,$GRNNo   ,$challanNo ,
   		$challanDate ,$invoiveNo ,$invoiceDate ,$invoiceTax ,$GrossWeight,$invoiceAmt ,$vehicleNo ,
   		$receiveBy ,$noOfBox   ,$locationId ,$createdBy,$TransporterName,$GatePassNo,$TransportNo,$jsonForViewDetail ){
   
   	try{
   
   		
   		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
   		$outParam='';
   		$sql = "{CALL sp_GRNSave (@jsonForBatchDetail=:jsonForBatchDetail,@PONumber=:PONumber,@ambendMentNO=:ambendMentNO,
					@Status=:Status,@GRNNo=:GRNNo,@challanNo=:challanNo,@challanDate=:challanDate
					,@invoiveNo=:invoiveNo,@invoiceDate=:invoiceDate,@invoiceTax=:invoiceTax,@invoiceAmt=:invoiceAmt
   				,@vehicleNo=:vehicleNo,@receiveBy=:receiveBy
					,@noOfBox=:noOfBox,@locationId=:locationId,@createdBy=:createdBy
   				,
			@TransporterName=:TransporterName,@TransporterNo=:TransporterNo,@GatepassNo=:GatepassNo,@jsonForViewDetail=:jsonForViewDetail
   				,@outParam=:outParam)}";

   		$stmt = $pdo_object->prepare($sql);
   		$stmt->bindParam(':jsonForBatchDetail',$jsonForBatchDetail, PDO::PARAM_STR);
   		$stmt->bindParam(':PONumber',$PONumber, PDO::PARAM_STR);
   		$stmt->bindParam(':ambendMentNO',$ambendMentNO, PDO::PARAM_STR);
   		$stmt->bindParam(':Status',$Status, PDO::PARAM_INT);
   		$stmt->bindParam(':GRNNo',$GRNNo, PDO::PARAM_INT);
   		$stmt->bindParam(':challanNo',$challanNo, PDO::PARAM_STR);
   		$stmt->bindParam(':challanDate',$challanDate, PDO::PARAM_STR);
   		$stmt->bindParam(':invoiveNo',$invoiveNo,PDO::PARAM_STR);
   		$stmt->bindParam(':invoiceDate',$invoiceDate, PDO::PARAM_STR);
   		$stmt->bindParam(':invoiceTax',$invoiceTax, PDO::PARAM_STR);
   		$stmt->bindParam(':invoiceAmt',$invoiceAmt, PDO::PARAM_STR);
   		$stmt->bindParam(':vehicleNo',$vehicleNo, PDO::PARAM_STR);
   		$stmt->bindParam(':receiveBy',$receiveBy, PDO::PARAM_STR);
   		$stmt->bindParam(':noOfBox',$noOfBox, PDO::PARAM_STR);
   		$stmt->bindParam(':locationId',$locationId, PDO::PARAM_INT);
   		$stmt->bindParam(':createdBy',$createdBy, PDO::PARAM_INT);
   		$stmt->bindParam(':TransporterName',$TransporterName, PDO::PARAM_STR);
   		$stmt->bindParam(':TransporterNo',$TransportNo, PDO::PARAM_STR);
   		$stmt->bindParam(':GatepassNo',$GatePassNo, PDO::PARAM_STR);
   		$stmt->bindParam(':jsonForViewDetail',$jsonForViewDetail, PDO::PARAM_STR);
   		/* $stmt->bindParam(':GrossWeight',$GrossWeight, PDO::PARAM_INT); */
   		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
   
   		$stmt->execute();
   		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

   		if($results[0]['OutParam']=='INF0066')
   		{
   			throw new vestigeException(' GRN for this purchase order already open . ');
   		}
   		
   		 if(sizeof($results[0]['OutParam']) > 0)
			  		{
			  			throw new vestigeException($results[0]['OutParam']);
			  		}
	  		
		  		}
		  		catch(Exception $e){
		  			throw new Exception($e->getMessage());
		  		
		  		}
		  		return  $results ;  
   
   }
    
   
   }
?>

	
