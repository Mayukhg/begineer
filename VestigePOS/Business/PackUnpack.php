<?php

//include 'VestigeUtil.php';
/* define('__PATH__', dirname(dirname(__FILE__)));
include(__PATH__.'/Common/VestigeUtil.php'); */

Class PackUnpack
{
	var $vestigeUtil;

	function __construct()
	{

		$this->vestigeUtil = new VestigeUtil();
	}
	function packUnpackSearch($packUnpackOption,$fromDate,$toDate,$itemCode,$itemId,$searchParam,$locationId)
	{
		
		try{
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			$outParam = '';
			$sql = "{CALL sp_GetPUVoucherSearch (@FromDate=:fromDate,@DisplayFromDate=:displayFromDate,@ToDate=:toDate
					,@DisplayToDate=:displayToDate,@PU_All=:PU_All,@SearchParam=:searchParam,@ItemCode=:itemCode,@LocationId=:locationId
					,@ItemId=:itemId,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':fromDate', $fromDate);
			$stmt->bindParam(':displayFromDate', $fromDate);
			$stmt->bindParam(':toDate', $toDate);
			$stmt->bindParam(':displayToDate', $toDate);
			$stmt->bindParam(':PU_All', $packUnpackOption);
			$stmt->bindParam(':searchParam', $searchParam);
			$stmt->bindParam(':itemCode', $itemCode);
			$stmt->bindParam(':locationId', $locationId);
			$stmt->bindParam(':itemId',$itemId );
			$stmt->bindParam(':outParam', $outParam);
			$stmt->execute();
			$results= array();
			do
			{
				$results[] = $stmt->fetchAll();

			}while($stmt->nextRowset());

			$stmt->closeCursor();
	
			$packUnpackResults = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $packUnpackResults;
		}
		catch (Exception $e) {
			
			$packUnpackResults = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $packUnpackResults;
		}
	}
	
	/*
	 * function used to get item id from item code of composite item.
	 */
	function searchItemId($itemCode,$locationId)
	{
		try
		{
	
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "select IM.ItemId,IM.DistributorPrice,IM.ItemName,ISNULL(SUM(Quantity),0) TotalQuantity from
	 Item_Master  IM with (NOLOCK) Left Join  Inventory_LocBucketBatch ILB with (NOLOCK) ON ILB.ItemId=IM.ItemId
	 and ILB.LocationId='$locationId' and BucketId=5 
	 LEFT JOIN ItemBatch_Detail IBD with (NOLOCK) ON IBD.BatchNo=ILB.BatchNo and IBD.ExpDate>GETDATE()
	  where ItemCode = '$itemCode'  and IsComposite =
	   1 group by IM.ItemId,IM.DistributorPrice,IM.ItemName";
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$searchItemId = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $searchItemId;
			
		}
		catch(Exception $e)
		{
			$searchItemId = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $searchItemId; 
		}
		
	
		
	}
	
	/*
	 * function used to save pack information.
	 */
	function packSave($CompositeItemId,$DBQuantity,$Remarks,$locationId,$createdBy)
	{

		$connectionString = new DBHelper();
		
		$pdo_object = $connectionString->dbConnection();

	TRY 
		     {		
				
				$PUNNo = '';
				$bucketId = 5;
				$outParam = '';
				$sql = "{CALL sp_PackSave (@CompositeItemId=:CompositeItemId,@DBQuantity=:DBQuantity,@Remarks=:Remarks,
						@CreatedBy=:CreatedBy,@locationId=:locationId
						,@BucketId=:BucketId,@outParam=:outParam)}";
				$stmt = $pdo_object->prepare($sql);
				$stmt->bindParam(':CompositeItemId', $CompositeItemId,PDO::PARAM_INT);
				$stmt->bindParam(':DBQuantity', $DBQuantity,PDO::PARAM_INT);
				$stmt->bindParam(':Remarks', $Remarks,PDO::PARAM_STR);
				$stmt->bindParam(':CreatedBy', $createdBy,PDO::PARAM_INT);
				$stmt->bindParam(':locationId', $locationId,PDO::PARAM_INT);
				$stmt->bindParam(':BucketId', $bucketId,PDO::PARAM_INT);
				$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if($results[0]['OutParam'] == 'VAL0057')
				{
					throw new vestigeException('Packing quantity is greater than item(s) quantity at current location.');
				}
	           if(sizeof($results[0]['OutParam']) > 0)
			  		{
			  			throw new vestigeException($results[0]['OutParam']);
			  		}
	  		
		  		}
		  		catch(Exception $e){
		  			throw new Exception($e->getMessage());
		  		
		  		}
		  		return  $results ;  
	}

	function unPackSave($CompositeItemId,$DBQuantity,$Remarks,$locationId,$createdBy)
	{
	
		$connectionString = new DBHelper();

		$pdo_object = $connectionString->dbConnection();
	
		TRY
		{
		
			$PUNNo = '';
			$bucketId = 5;
			$outParam = '';
				
			
	
			$sql = "{CALL sp_UnpackSave (@compositeItemId=:CompositeItemId,@Quantity=:DBQuantity,
						@CreatedBy=:CreatedBy,@locationId=:locationId
						,@BucketId=:BucketId,@Remarks=:Remarks,@outParam=:outParam)}";
	
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':CompositeItemId', $CompositeItemId,PDO::PARAM_INT);
			$stmt->bindParam(':DBQuantity', $DBQuantity,PDO::PARAM_INT);
	
			$stmt->bindParam(':CreatedBy', $createdBy,PDO::PARAM_INT);
			$stmt->bindParam(':locationId', $locationId,PDO::PARAM_INT);
			$stmt->bindParam(':BucketId', $bucketId,PDO::PARAM_INT);
			$stmt->bindParam(':Remarks', $Remarks,PDO::PARAM_STR);
			$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if($results[0]['OutParam'] == 'VAL0058')
			{
				throw new vestigeException('Unpacking quantity is greater than  quantity at current location.');
			}
			if($results[0]['OutParam'] == 'VAL0060')
			{
				throw new vestigeException('Corresponding batches not found.');
			}
			
	      	if($results[0]['OutParam'] == 'VAL0059')
			{
				throw new vestigeException('Can not unpack,Packing batches not found. Contact system admin.');
			}
			if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
			 
			}
		catch(Exception $e){
				throw new Exception($e->getMessage());
				 
			}
			return  $results ;
		 
	}

  
	function CallForDuplicateBatchNo($BatchNo,$MRP,$MFgDate,$ExpDate)
	{
		try
		{
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "select ItemBatchId  from ItemBatch_Detail with (NOLOCK) where  ManufactureBatchNo='$BatchNo'
					UNION ALL
					select ItemBatchId  from ItemBatch_Detail with (NOLOCK) where  ManufactureBatchNo='$BatchNo' and MRP='$MRP'
					AND MfgDate='$MFgDate' and ExpDate='$ExpDate'";
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$callForDuplicateBatchNoInfo = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $callForDuplicateBatchNoInfo;
		}
		catch(Exception $e)
		{
			$callForDuplicateBatchNoInfo = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $callForDuplicateBatchNoInfo;
		}
		
		
	}	
}

?>
