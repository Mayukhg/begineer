<?php
Class PaymentGatewayRecord
{
	/*------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function will return distributor information from pickup center id.
	 * @param unknown $selectedPickUpCenter
	 * @return multitype:
	 */
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}

	function searchOrderHistory($historyLogNo,$HistoryFromDate,$historyToDate,$historyDistributorNo,$locationId,$loggedInUserId,$isServiceCentreUser,$Status,$orderid)
	{

		try{

			$pdo_object = POSBusinessClass :: dbConnectionInfo();
			
			if ($locationId!=10)
			{
			$dynamicWhereClause1 = "AND LM.locationid='$locationId'";
			}

			if($isServiceCentreUser == true){
				$dynamicWhereClause = "";
			}
			
			if($Status==-1)
			{
				$Status="";
			}
			$sql = "select coh.customerorderno,case when coh.Status = 3 then 'Confirmed' when coh.Status = 4 then 
'Invoiced' when coh.Status = 2 then 'Cancelled' end 'OrderStatus',coh.logno,DM.distributorid,opg.transactionid,opg.transactionamount,
ISNULL(LTRIM(RTRIM(DM.DistributorFirstName)),'')+' '+ISNULL(LTRIM(RTRIM(DM.DistributorLastName)) ,'')As [DistributorName],
coh.date,opg.transactionstatus,coh.status,opg.InitiatedDateTime,LM.Name,coh.OrderRemarks
from onlinepaygordertrack opg with (NOLOCK)
inner join coheader coh  with (NOLOCK) on coh.customerorderno=opg.trackid
inner join distributormaster DM with (NOLOCK) on DM.distributorid=coh.distributorid
inner join location_master LM with (NOLOCK) on LM.locationid=coh.BOId
where (ISNULL('$historyDistributorNo',-1)=-1 OR '$historyDistributorNo'=0 OR DM.[DistributorId]='$historyDistributorNo')
and (Convert(varchar(10),CAST('$HistoryFromDate' AS DATETIME),112)='19000101' OR Convert(varchar(10),opg.[InitiatedDateTime],112)>=Convert(varchar(10),CAST('$HistoryFromDate' AS DATETIME),112))
AND (Convert(varchar(10),CAST('$historyToDate' AS DATETIME),112)='19000101' OR CONVERT(varchar(10),opg.[InitiatedDateTime],112)<=Convert(varchar(10),CAST('$historyToDate' AS DATETIME),112))
AND (ISNULL('$historyLogNo','')='' OR  coh.[LogNo] like '%$historyLogNo%' )
AND (ISNULL('$orderid','')='' OR  coh.[CustomerOrderNO] like '%$orderid%' )
AND (ISNULL('$Status','')=''  OR opg.[TransactionStatus] like '%$Status%' )"
.$dynamicWhereClause1.
"ORDER BY coh.LogNo";

			
		
			
			$stmt = $pdo_object->prepare($sql);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$historyOrderData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');

			return $historyOrderData;

		}
		catch (PDOException $e) {

			$historyOrderData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());

			return $historyOrderData;

		}
	}


	function historyOrderStatus()
	{
		try
		{
			$connectionString = new DBHelper();

			$pdo_object = $connectionString->dbConnection();

			$stmt = $pdo_object->prepare("Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master
			Where
				parametercode='ORDERSTATUS'
				And isactive=	1
			Order By
				sortorder Asc");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$historyOrderStatusData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');

			return $historyOrderStatusData;
		}
		catch(Exception $e)
		{
			$historyOrderStatusData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());

			return $historyOrderStatusData;
		}
			
			
	}
	
	function AllowRepayment($locationId,$loggedInUserId,$LogNo)
	{
	
		try
		{
			$connectionString = new DBHelper();
	
	
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "{CALL sp_AllowOnlineRepayment (@inputParam=:SourceLocationId,@inputParam2=:LogNo,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->bindParam(':SourceLocationId',$locationId, PDO::PARAM_STR);
			$stmt->bindParam(':LogNo',$LogNo,PDO::PARAM_STR);
			
	
			$stmt->bindParam(':outParam',$outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
	
	
			$stmt->execute();
				
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
				
				
			$allowrepayment = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
	
			return $allowrepayment;
				
		}
		catch(Exception $e)
		{
			$allowrepayment = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $allowrepayment;
		}
	
	}
	
	function  OrderStatus($orderNo)
	{
	
		try
		{
			$connectionString = new DBHelper();
	
	
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "select Status from coheader where customerorderno='$orderNo'";
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$OrderStatus = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $OrderStatus;
			
		
	
		}
		catch(Exception $e)
		{
			$OrderStatus = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $OrderStatus;
		}
	
	}
	

	
}