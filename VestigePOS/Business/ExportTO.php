
<?php

Class ExportInvoice
{

	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	function searchWHLocation()
	{
		try
		{
			$connectionString = new DBHelper();

			
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare("Select
				'-1'  LocationId,
				'All' DisplayName,
				''  LocationType,
				'All' LocationName,
				'-' [Address],
				'-1' CityId,
				'Select' CityName,
				'Select' LocationCode
				,'' DistributorName
				,'' IsexportLocation
				,'' CountryName
				,'' IECCode
				Union All
				Select	lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName,
				CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType, [Name] AS LocationName,
				ISNULL([Address1],'')+' '+ISNULL([Address2],'')+ISNULL([Address3],'')+ISNULL([Address4],'') +  ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'')
				As [Address],cm.CityId, cm.CityName,
				lc.LocationCode,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,IsexportLocation,com.CountryName,IECCode
				From Location_Master lc with (NOLOCK)
				Left join City_Master cm with (NOLOCK)
			
				On cm.CityId = lc.CityId
			
				Left join state_master sm with (NOLOCK)
				On sm.StateId = lc.StateId
			
				Left join Country_Master com with (NOLOCK)
				On com.CountryId = lc.CountryId
				LEFT JOIN DistributorMaster dm ON lc.DistributorId = dm.DistributorId
				WHERE LocationType IN (2, 3)
				AND lc.Status=1");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$searchWHLocation = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $searchWHLocation;
		}
		catch(Exception $e)
		{
			$searchWHLocation = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $searchWHLocation;
		}
		
		
	}
	function searchBucket()
	{
		try
		{
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare("Select
				'-1' BucketId,
				'Select' BucketName,
				-1 Sellable
				Union All
				Select
				buc.BucketId,
				BucketName,
				Sellable
				From Bucket buc with (NOLOCK)
				Where buc.Status = 1 AND buc.ParentId Is Not Null
				And BucketId <>9");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$searchBucket = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			return $searchBucket;
		}
		catch(Exception $e)
		{
			$searchBucket = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			return $searchBucket;
		}
		
		
	}
	function TOStatus()
	{
		try
		{
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare("Select
				-1 'keycode1',
				'Select' 'keyvalue1',
				-1 'keycode2',
				'Select' 'keyvalue2',
				-1 'keycode3',
				'Select' 'keyvalue3',
				1 'isactive',
				-1 'sortorder',
				'' 'ParameterCode',
				'' 'description'
				Union All
				Select
				keycode1,
				keyvalue1,
				ISNULL(keycode2, 0) 'keycode2',
				ISNULL(keyvalue2, '') 'keyvalue2',
				ISNULL(keycode3, 0) 'keycode3',
				ISNULL(keyvalue3, '') 'keyvalue3',
				isactive,
				sortorder,
				ParameterCode,
				ISNULL([description], '') 'description'
				From	Parameter_Master with (NOLOCK)
				Where
				parametercode='TOStatus'
				And isactive=	1
				Order By
				sortorder Asc");

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			

			$TOStatusData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');

			
			return $TOStatusData;
		}
		catch(Exception $e)
		{
			$TOStatusData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $TOStatusData;
		}
		
		
	}
	function searchTO($TOFormData,$TOIStatus,$TOILookUpDestAddId,$TOILookUpSourceAddId,$locationId)
	{
		try
		{
			parse_str($TOFormData, $output);
				
			$sourceLocation=$output['TOSourceLocation'];
			$destinationLocation = $output['ToDestinationLocation'];
			
	
					
				$fromTOIDate='1900-01-01T00:00:00' ;
			
				
				$ToTODate='2099-12-31T00:00:00' ;
			
			/*Modified for look up*/
				
			if($TOIStatus == '')
			{
				$TOStatus = $output['TOStatus'];
			}
			else
			{
				$TOStatus = 2;
				$sourceLocation = -1;
				$destinationLocation = $locationId;
					
			}
				
			/*--------------------*/
			$FromTOShipDate = $output['FromTODate'];
			$TORefNumber  = $output['TORefNumber'];
			$TONumber  = $output['TONumber'];
				
			$TOINumber  = $output['TOINumber'];
			$ToTOShipDate  = $output['ToTODate'];
			$indentised= $output['indentised'];
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
				
			$sql = "		Select distinct head.TONumber,Convert(Varchar(20), head.CreationDate, 105) AS 'TOCreationDate', head.TOINumber,th.TOIDate AS 'TOIDate', head.SourceLocationId, head.DestinationLocationId, Convert(Varchar(20),head.CreationDate, 105) CreationDate, head.Status, head.TotalTOQuantity, head.TotalTOAmount, head.TotalTOTaxAmount, head.ModifiedDate,
			lm.Address1 + Char(13) + char(10) + IsNull(lm.Address2,'') + Char(13) + char(10) + IsNull(lm.Address3,'') + Char(13) + char(10) +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As SourceAddress,
			lm_1.Address1 + Char(13) + char(10) + IsNull(lm_1.Address2,'') + Char(13) + char(10) + IsNull(lm_1.Address3,'') + Char(13) + char(10) + cm_1.CityName +  ' ' + sm_1.StateName + ' '  + com_1.CountryName As DestinationAddress,
			head.PackSize, Remarks, RefNumber,head.ShippingWayBillNo,
			prm.KeyValue1 StatusName, head.ShippingDetails,
			Convert(Varchar(20), head.ShippingDate, 105)	ShipDate, Convert(Varchar(20), head.ExpectedDeliveryDate, 105)	ExpectedDeliveryDate
			--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End As Indentise
			, IsNull(head.GrossWeight,0) GrossWeight,
			sm.StateId, head.CreationDate,head.ModifiedBY, UM.FirstName +' '+ UM.MiddleName +' '+ UM.LastName As ModifiedByName,
			lm.Phone1 + ', '+ lm.Phone2 As SourcePhone,
			lm.EmailId1,
			cm.CityName As SourceCity,
			cm_1.CityName As DestinationCity,
			ISNULL(head.Isexported,'') AS Isexported,lm.IECCode,
			head.ExporterRef, head.OtherRef,head.BuyerOtherthanConsignee,head.PreCarriage,head.PlaceofReceiptbyPreCarrier,com.CountryName AS CountryOfOrigin,
			com_1.CountryName as CountryOfDestination,head.VesselflightNo,head.PortofLoading,head.PortofDischarge,head.PortofDestination,head.TermsofDelivery,
			head.DELIVERY,head.PAYMENT,head.BuyerOrderNo,head.BuyerOrderDate
			--Case When Isnull((head.Isexported,'')='')then '' Else  head.Isexported End AS Isexported
			From TO_Header	head
			Inner Join TO_Detail det
			On head.TONumber = det.TONumber
			Inner Join Location_Master lm with (NOLOCK)
			On lm.LocationId = head.SourceLocationId
			Inner Join Location_Master lm_1 with (NOLOCK)
			On lm_1.LocationId = head.DestinationLocationId
				
			LEFT Join TOI_Header th with (NOLOCK)
			On th.TOINumber = head.TOINumber
				
			Inner Join Parameter_Master  prm with (NOLOCK)
			On head.Status = prm.KeyCode1
			And prm.ParameterCode = 'TOStatus'
				
			Left join City_Master cm with (NOLOCK)
			On cm.CityId = lm.CityId 
				
			Left join state_master sm with (NOLOCK)
			On sm.StateId = lm.StateId
				
			Left join Country_Master com with (NOLOCK)
			On com.CountryId = lm.CountryId
				
			Left join City_Master cm_1 with (NOLOCK)
			On cm_1.CityId = lm_1.CityId
				
			Left join state_master sm_1 with (NOLOCK)
			On sm_1.StateId = lm_1.StateId
				
			Left join Country_Master com_1 with (NOLOCK)
			On com_1.CountryId = lm_1.CountryId
				
			Left Outer Join User_Master UM with (NOLOCK)
			On UM.UserId = Head.ModifiedBy
				
			Where	(IsNull('$destinationLocation','-1')='-1' Or head.DestinationLocationId = '$destinationLocation')
			And		(IsNull('$sourceLocation','-1')='-1' Or head.SourceLocationId = '$sourceLocation')
			
			And		(IsNull(NullIf('$TOINumber',''),'-1')='-1' Or head.TOINumber Like  '%' + '$TOINumber')
			And		(IsNull(NullIf('$TONumber',''),'-1')='-1' Or head.TONumber Like '%' + '$TONumber' +'%')
			AND		(IsNull('$FromTOShipDate','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'2099-01-01'),112) >= Convert(varchar(10),CAST('$FromTOShipDate' As DateTime),112))
			AND		(IsNull('$ToTODate','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'1900-01-01'),112) <= Convert(varchar(10),Cast('$ToTODate' As DateTime),112))
			AND		(IsNull('$FromTOShipDate','')='' OR Convert(varchar(10),head.ShippingDate,112) >= Convert(varchar(10),Cast('$FromTOShipDate' As DateTime),112))
			AND		(IsNull('$ToTOShipDate','')='' OR Convert(varchar(10),head.ShippingDate,112) <= Convert(varchar(10),Cast('$ToTOShipDate' As DateTime),112))
			AND		(IsNull('$TOStatus','-1')='-1' OR head.Status = '$TOStatus')
			AND head.Isexported=1
				
			ORDER BY head.TONumber ASC
			";
		
		
		
			$stmt = $pdo_object->prepare($sql);
				
			$stmt->execute();
			
			$results = null;

			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
				
			$searchTOData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $searchTOData;
		}
		Catch(Exception $e)
		{
			$searchTOData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());

			return $searchTOData;
		}

	}
	//for claim
	function searchTO2($TOFormData,$TOIStatus,$TOILookUpDestAddId,$TOILookUpSourceAddId,$locationId)
	{
		try
		{
			parse_str($TOFormData, $output);
	
			$sourceLocation=$output['TOSourceLocation'];
			$destinationLocation = $output['ToDestinationLocation'];
				
			$FromTODate = $output['FromTODate'];
			if($fromTOIDate==''){
					
				$fromTOIDate='1900-01-01T00:00:00' ;
			}
			$ToTODate = $output['ToTODate'];
			if($ToTODate==''){
					
				$ToTODate='2099-12-31T00:00:00' ;
			}
			/*Modified for look up*/
	
			if($TOIStatus == '')
			{
				$TOStatus = $output['TOStatus'];
			}
			else
			{
				$TOStatus = 2;
				$sourceLocation = $locationId;
				$destinationLocation = -1;
					
			}
	
			/*--------------------*/
			$FromTOShipDate = $output['FromTOShipDate'];
			$TORefNumber  = $output['TORefNumber'];
			$TONumber  = $output['TONumber'];
	
			$TOINumber  = $output['TOINumber'];
			$ToTOShipDate  = $output['ToTOShipDate'];
			$indentised= $output['indentised'];
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "		Select distinct head.TONumber,Convert(Varchar(20), head.CreationDate, 105) AS 'TOCreationDate', head.TOINumber,th.TOIDate AS 'TOIDate', head.SourceLocationId, head.DestinationLocationId, Convert(Varchar(20),head.CreationDate, 105) CreationDate, head.Status, head.TotalTOQuantity, head.TotalTOAmount, head.TotalTOTaxAmount, head.ModifiedDate,
			lm.Address1 + Char(13) + char(10) + IsNull(lm.Address2,'') + Char(13) + char(10) + IsNull(lm.Address3,'') + Char(13) + char(10) +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As SourceAddress,
			lm_1.Address1 + Char(13) + char(10) + IsNull(lm_1.Address2,'') + Char(13) + char(10) + IsNull(lm_1.Address3,'') + Char(13) + char(10) + cm_1.CityName +  ' ' + sm_1.StateName + ' '  + com_1.CountryName As DestinationAddress,
			head.PackSize, Remarks, RefNumber,head.ShippingWayBillNo,
			prm.KeyValue1 StatusName, head.ShippingDetails,
			Convert(Varchar(20), head.ShippingDate, 105)	ShipDate, Convert(Varchar(20), head.ExpectedDeliveryDate, 105)	ExpectedDeliveryDate
			--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End As Indentise
			, IsNull(head.GrossWeight,0) GrossWeight,
			sm.StateId, head.CreationDate,head.ModifiedBY, UM.FirstName +' '+ UM.MiddleName +' '+ UM.LastName As ModifiedByName,
			lm.Phone1 + ', '+ lm.Phone2 As SourcePhone,
			lm.EmailId1,
			cm.CityName As SourceCity,
			cm_1.CityName As DestinationCity,
			ISNULL(head.Isexported,'') AS Isexported,lm.IECCode,
			head.ExporterRef, head.OtherRef,head.BuyerOtherthanConsignee,head.PreCarriage,head.PlaceofReceiptbyPreCarrier,com.CountryName AS CountryOfOrigin,
			com_1.CountryName as CountryOfDestination,head.VesselflightNo,head.PortofLoading,head.PortofDischarge,head.PortofDestination,head.TermsofDelivery,
			head.DELIVERY,head.PAYMENT,head.BuyerOrderNo,head.BuyerOrderDate
			--Case When Isnull((head.Isexported,'')='')then '' Else  head.Isexported End AS Isexported
			From TO_Header	head
			Inner Join TO_Detail det
			On head.TONumber = det.TONumber
			Inner Join Location_Master lm with (NOLOCK)
			On lm.LocationId = head.SourceLocationId
			Inner Join Location_Master lm_1 with (NOLOCK)
			On lm_1.LocationId = head.DestinationLocationId
	
			LEFT Join TOI_Header th with (NOLOCK)
			On th.TOINumber = head.TOINumber
	
			Inner Join Parameter_Master  prm with (NOLOCK)
			On head.Status = prm.KeyCode1
			And prm.ParameterCode = 'TOStatus'
	
			Left join City_Master cm with (NOLOCK)
			On cm.CityId = lm.CityId
	
			Left join state_master sm with (NOLOCK)
			On sm.StateId = lm.StateId
	
			Left join Country_Master com with (NOLOCK)
			On com.CountryId = lm.CountryId
	
			Left join City_Master cm_1 with (NOLOCK)
			On cm_1.CityId = lm_1.CityId
	
			Left join state_master sm_1 with (NOLOCK)
			On sm_1.StateId = lm_1.StateId
	
			Left join Country_Master com_1 with (NOLOCK)
			On com_1.CountryId = lm_1.CountryId
	
			Left Outer Join User_Master UM with (NOLOCK)
			On UM.UserId = Head.ModifiedBy
	
			Where	(IsNull('$destinationLocation','-1')='-1' Or head.DestinationLocationId = '$destinationLocation')
			And		(IsNull('$sourceLocation','-1')='-1' Or head.SourceLocationId = '$sourceLocation')
			And		(IsNull(NullIf('$TORefNumber',''),'-1')='-1' Or head.RefNumber Like  '%' + '$TORefNumber')
			And		(IsNull(NullIf('$TOINumber',''),'-1')='-1' Or head.TOINumber Like  '%' + '$TOINumber')
			And		(IsNull(NullIf('$TONumber',''),'-1')='-1' Or head.TONumber Like '%' + '$TONumber' +'%')
			AND		(IsNull('$FromTOShipDate','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'2099-01-01'),112) >= Convert(varchar(10),CAST('$FromTOShipDate' As DateTime),112))
			AND		(IsNull('$ToTODate','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'1900-01-01'),112) <= Convert(varchar(10),Cast('$ToTODate' As DateTime),112))
			AND		(IsNull('$FromTOShipDate','')='' OR Convert(varchar(10),head.ShippingDate,112) >= Convert(varchar(10),Cast('$FromTOShipDate' As DateTime),112))
			AND		(IsNull('$ToTOShipDate','')='' OR Convert(varchar(10),head.ShippingDate,112) <= Convert(varchar(10),Cast('$ToTOShipDate' As DateTime),112))
			AND		(IsNull('$TOStatus','-1')='-1' OR head.Status = '$TOStatus')
			AND		(IsNull('$indentised','2')='2' Or IsNull('$indentised','-1')='-1' Or '$indentised' = Case When th.Indentised = 1 Then 1 Else 0 End)
	
	
			ORDER BY head.TONumber ASC
			";
	
				
	
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
				
			$results = null;
	
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				
	
			$searchTOData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
	
			return $searchTOData;
		}
		Catch(Exception $e)
		{
		$searchTOData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
		return $searchTOData;
		}
	
		}
	
	function searchTOItems($TONumber,$sourceId)
	{

		try{
			$outParam='';
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			$sql = "{CALL usp_TOItemSearch (@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':TNumber',$TONumber, PDO::PARAM_STR);
			$stmt->bindParam(':SourceAddressId',$sourceId,PDO::PARAM_INT);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		   if($results[0]['OutParam'] == "INF0041")
			{	 
			   throw new vestigeException("Item does not exists at current location",2101); 
			}
			else if($results[0]['OutParam'] == "INF0042")
			{
			   throw new vestigeException("Item is not mapped to current location",2102);	 
			}
			else if($results[0]['OutParam'] == "VAL0027")
			{
				throw new vestigeException("Total quantity in TOI item(s) is greater than item(s) quantity at current location.",2103);					
			}
			else if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
	
		}
		catch(Exception $e){
			throw new Exception($e);
			
		}
		
		return $results;
	}
	function searchTOI($TOINumber,$sourceLocationId)
	{
		try{
			
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
		
				
			$sql = "	Select distinct head.TOINumber, SourceLocationId, DestinationLocationId, convert(nvarchar,TOIDate,105) TOIDate, head.Status, TotalTOIQuantity, ISNULL(TotalTOIAmount,0) TotalTOIAmount,
			TotalTOITaxAmount,
			ISNULL(head.Isexported,'') AS Isexported,
			head.ModifiedDate,

			IsNull(lm.Address1,'') + Char(13) + char(10) + IsNull(lm.Address2,'') + Char(13) + char(10) + IsNull(lm.Address3,'') + Char(13) + char(10) + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'') As SourceAddress,
			IsNull(lm_1.Address1,'')  + Char(13) + char(10) + IsNull(lm_1.Address2,'') + Char(13) + char(10) + IsNull(lm_1.Address3,'') + Char(13) + char(10) + IsNull(cm_1.CityName,'') +  ' ' + IsNull(sm_1.StateName,'') + ' '  + IsNull(com_1.CountryName,'') As DestinationAddress,
				
			prm.KeyValue1 StatusName, convert(nvarchar,CreationDate,105)	CreationDate, IsNull(lm.StateId,-1) StateId,Indentised, IsNull(icd.PONumber,'') PONumber, IsNull(PO.Status,-1) As POStatus, prm_1.KeyValue1 As POStatusName,head.Isexported,
			ISNULL(head.PriceMode, -1) PriceMode, ISNULL(head.Percentage, -1) Percentage, ISNULL(head.AppDep,-1) AppDep
			--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End
			From TOI_Header	head with (NOLOCK)
			Inner Join TOI_Detail det with (NOLOCK)
			On head.TOINumber = det.TOINumber
			Inner Join Location_Master lm with (NOLOCK)
			On lm.LocationId = head.SourceLocationId
			Inner Join Location_Master lm_1
			On lm_1.LocationId = head.DestinationLocationId
				
			Inner Join Parameter_Master  prm with (NOLOCK)
			On head.Status = prm.KeyCode1
			And prm.ParameterCode = 'TOIStatus'
				
			Left join City_Master cm with (NOLOCK)
			On cm.CityId = lm.CityId
				
			Left join state_master sm with (NOLOCK)
			On sm.StateId = lm.StateId
				
			Left join Country_Master com with (NOLOCK)
			On com.CountryId = lm.CountryId
				
			Left join City_Master cm_1 with (NOLOCK)
			On cm_1.CityId = lm_1.CityId
				
			Left join state_master sm_1 with (NOLOCK)
			On sm_1.StateId = lm_1.StateId
				
			Left join Country_Master com_1 with (NOLOCK)
			On com_1.CountryId = lm_1.CountryId
				
			Left Join dbo.IndentConsolidation_Detail icd with (NOLOCK)
			On icd.TOINumber = head.TOINumber
				
			Left Join dbo.PO_Header po with (NOLOCK)
			On po.PONumber = icd.PoNumber
				
			Left Join Parameter_Master  prm_1 with (NOLOCK)
			On po.Status = prm_1.KeyCode1
			And prm_1.ParameterCode = 'POStatus'
				
			Where	(IsNull('-1','-1')='-1' Or DestinationLocationId = '-1')
			And
			(IsNull('$sourceLocationId','-1')='-1' Or SourceLocationId = '$sourceLocationId')
			And		(IsNull(NullIf('$TOINumber',''),'-1')='-1' Or head.TOINumber = '$TOINumber')
			AND		(IsNull('1900-01-01T00:00:00','')='' OR Convert(varchar(8),IsNull(TOIDate,CAST('1900-01-01'AS DATETIME)),112) >= Convert(varchar(8),CAST('1900-01-01T00:00:00' As DateTime),112))
			AND		(IsNull('2099-12-31T00:00:00','')='' OR Convert(varchar(8),IsNull(TOIDate,CAST('2099-12-31' AS DATETIME)),112) <= Convert(varchar(8),Cast('2099-12-31T00:00:00' As DateTime),112))
			AND		(IsNull('1900-01-01T00:00:00','')='' OR Convert(varchar(8),CreationDate,112) >= Convert(varchar(8),Cast('1900-01-01T00:00:00' As DateTime),112))
			AND		(IsNull('2099-12-31T00:00:00','')='' OR Convert(varchar(8),CreationDate,112) <= Convert(varchar(8),Cast('2099-12-31T00:00:00' As DateTime),112))
			AND		(IsNull('2','-1')='-1' Or head.Status = '2')
			--AND		(IsNull('0','2')='2' Or IsNull('0','-1')='-1' Or '0' = Case When head.Indentised = 1 Then 1 Else 0 End)
			AND		 head.Isexported = 1 
				
				
			Order By TOINumber DESC

			";
				
			$stmt = $pdo_object->prepare($sql);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			 
			$searchTOIData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			return $searchTOIData;
		}
		catch(Exception $e){
			
			$searchTOIData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			return $searchTOIData;
		}
	}
	function ShowControlTaxInfor($locationId)
	{
		try{
				
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();

			$sql = "select phone1,phone2,mobile1,mobile2,tinNo,cstNO,VatNo from location_master(nolock)
			where  locationid ='$locationId'";

			$stmt = $pdo_object->prepare($sql);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$showControlTaxData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			return $showControlTaxData;
		}
		catch(Exception $e){
			
			$showControlTaxData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			return $showControlTaxData;
		}
	}
	function adjustItemAndBatch($SourceAddressId,$TOINumber){
		try{
 
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			$outParam='';
			$sql = "{CALL sp_TOAdjustItemSearch (@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);

			$stmt->bindParam(':TNumber',$TOINumber, PDO::PARAM_STR);
			$stmt->bindParam(':SourceAddressId',$SourceAddressId,PDO::PARAM_INT);
				
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			 	
					
											if($results[0]['OutParam'] > 'INF0041')
											{
												throw new vestigeException('Item does not exists at current location');
											}
											if($results[0]['OutParam'] > 'INF0042')
											{
												throw new vestigeException('Item is not mapped to current location');
											}
											if($results[0]['OutParam'] > 'VAL0027')
											{
												throw new vestigeException('Total quantity in TOI item(s) is greater than item(s) quantity at current location.');
											}
										    if(sizeof($results[0]['OutParam']) > 0)
											{
												throw new vestigeException($results[0]['OutParam']);
											}
	
		}
		catch(Exception $e){
			throw new Exception($e);
			
		}
		return  $results ;
	}


	function saveTO($jsonForItems,$TOINumber,$statusId,$LocationId,$PackSize,$ExpectedDeliveryDate
			,$ShippingBillNo,$ShippingDetails,$RefNumber,$GrossWeight,$Remarks,$CreatedBy,$ExporterRef,$OtherRef,$BuyerOtherthanConsignee,$PreCarriage,
				$PlaceofReceiptbyPreCarrier,$VesselflightNo,$PortofLoading,$PortofDischarge,$PortofDestination,$TermsofDelivery,$DELIVERY,$PAYMENT
			,$BuyerOrderNo,$BuyerOrderDate,$EOIECNumber){

		try{

			$outParam='';
			/*  $file = fopen("F://checkfile.txt","w");
			fwrite($file,$jsonForItems."ExporterRef-".$statusId." OtherRef".$LocationId." ". $CreatedBy);
			fclose($file) ;  */
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			//$PackSize=1 ;
			
			$outParam='';
				
			$sql = "{CALL sp_ExportSave (@jsonForItems=:jsonForItems,@TOINumber=:TNumber,@statusId=:statusId,
					@LocationId=:LocationId,@CreatedBy=:CreatedBy,@PackSize=:PackSize,@ExpectedDeliveryDate=:ExpectedDeliveryDate,@ShippingBillNo=:ShippingBillNo
					,@ShippingDetails=:ShippingDetails,@RefNumber=:RefNumber,@Remarks=:Remarks,@GrossWeight=:GrossWeight,@ExporterRef=:ExporterRef,
					@OtherRef=:OtherRef,@BuyerOtherthanConsignee=:BuyerOtherthanConsignee,@PreCarriage=:PreCarriage,@PlaceofReceiptbyPreCarrier=:PlaceofReceiptbyPreCarrier,
					@VesselflightNo=:VesselflightNo,@PortofLoading=:PortofLoading,@PortofDischarge=:PortofDischarge,@PortofDestination=:PortofDestination
					,@TermsofDelivery=:TermsofDelivery,@DELIVERY=:DELIVERY,@PAYMENT=:PAYMENT,@BuyerOrderNo=:BuyerOrderNo,@BuyerOrderDate=:BuyerOrderDate,
					@EOIECNumber=:EOIECNumber,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':jsonForItems',$jsonForItems, PDO::PARAM_STR);
			$stmt->bindParam(':TNumber',$TOINumber, PDO::PARAM_STR);
			$stmt->bindParam(':statusId',$statusId, PDO::PARAM_INT);
			$stmt->bindParam(':LocationId',$LocationId, PDO::PARAM_INT);
			$stmt->bindParam(':CreatedBy',$CreatedBy, PDO::PARAM_INT);
			$stmt->bindParam(':PackSize',$PackSize, PDO::PARAM_INT);
			$stmt->bindParam(':ExpectedDeliveryDate',$ExpectedDeliveryDate, PDO::PARAM_STR);
			$stmt->bindParam(':ShippingBillNo',$ShippingBillNo, PDO::PARAM_STR);
			$stmt->bindParam(':ShippingDetails',$ShippingDetails,PDO::PARAM_STR);
			$stmt->bindParam(':RefNumber',$RefNumber, PDO::PARAM_STR);
			$stmt->bindParam(':Remarks',$Remarks, PDO::PARAM_STR);
			$stmt->bindParam(':GrossWeight',$GrossWeight, PDO::PARAM_INT);
			$stmt->bindParam(':ExporterRef',$ExporterRef, PDO::PARAM_STR);
			$stmt->bindParam(':OtherRef',$OtherRef, PDO::PARAM_STR);
			$stmt->bindParam(':BuyerOtherthanConsignee',$BuyerOtherthanConsignee, PDO::PARAM_STR);
			$stmt->bindParam(':PreCarriage',$PreCarriage, PDO::PARAM_STR);
			$stmt->bindParam(':PlaceofReceiptbyPreCarrier',$PlaceofReceiptbyPreCarrier, PDO::PARAM_STR);
			$stmt->bindParam(':VesselflightNo',$VesselflightNo, PDO::PARAM_STR);
			$stmt->bindParam(':PortofLoading',$PortofLoading, PDO::PARAM_STR);
			$stmt->bindParam(':PortofDischarge',$PortofDischarge, PDO::PARAM_STR);
			$stmt->bindParam(':PortofDestination',$PortofDestination, PDO::PARAM_STR);
			$stmt->bindParam(':TermsofDelivery',$TermsofDelivery, PDO::PARAM_STR);
			$stmt->bindParam(':DELIVERY',$DELIVERY, PDO::PARAM_STR);
			$stmt->bindParam(':PAYMENT',$PAYMENT, PDO::PARAM_STR);
			$stmt->bindParam(':BuyerOrderNo',$BuyerOrderNo, PDO::PARAM_STR);	
			$stmt->bindParam(':BuyerOrderDate',$BuyerOrderDate, PDO::PARAM_STR);
			$stmt->bindParam(':EOIECNumber',$EOIECNumber, PDO::PARAM_STR);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if($results[0]['OutParam']=='INF0229')
			{
				throw new vestigeException('Duplicate item exists.');
			}
			if($results[0]['OutParam']=='INF0226')
			{
				throw new vestigeException('TOI already processed.');
			}
			
		    if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
	
			}
			catch(Exception $e){
				throw new Exception($e->getMessage());
				
			}
			return  $results ;

	}

	function searchQtyForItem($sourceLocationId,$batchNo,$itemCode){
		$connectionString = new DBHelper();
		 
		$pdo_object = $connectionString->dbConnection();
		try{
			$sql=" 
			select ISNULL(SUM(Quantity),0) Quantity,ItemCode,ManufactureBatchNo,IBD.BatchNo from Inventory_LocBucketBatch ilb with (NOLOCK) INNER JOIN ItemBatch_Detail IBD with (NOLOCK) ON IBD.BatchNo=ilb.BatchNo
			INNEr JOIN Item_Master IM with (NOLOCK) ON IM.ItemId=ilb.ItemId
			AND IBD.ItemId=ilb.ItemId Where ilb.LocationId='$sourceLocationId' And IBD.ManufactureBatchNo='$batchNo' AND im.ItemCode='$itemCode' And ilb.BucketId=5
			GROUP By ItemCode,ManufactureBatchNo,IBD.BatchNo
			";
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		 $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
	
		 return $outputData;
		}
		catch(Exception $e)
		{
		$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
				return $exception;
		 }
	
	}
	
}

?>
