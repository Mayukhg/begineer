<?php

Class TransferOutInstruction
{
	var $vestigeUtil;
	function __construct()

	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	
	function searchWHLocation($locationId,$Userid)
	{

		try
		{
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare("Select
						'-1'  LocationId,
						'All' DisplayName,
						''  LocationType,
						'All' LocationName,
						'-' [Address],
						'-1' CityId,
						'Select' CityName,
						'Select' LocationCode
						,'' DistributorName
						,'' IsexportLocation
						,'' CountryName
						,'' IECCode
					Union All
					Select	lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName,
						 CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType, [Name] AS LocationName,
						ISNULL([Address1],'')+' '+ISNULL([Address2],'')+ISNULL([Address3],'')+ISNULL([Address4],'') +  ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'')
						As [Address],cm.CityId, cm.CityName,
						lc.LocationCode,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,IsexportLocation,com.CountryName,IECCode
					From Location_Master lc with (NOLOCK)
					Left join City_Master cm with (NOLOCK)
			
					On cm.CityId = lc.CityId
			
					Left join state_master sm with (NOLOCK)
					On sm.StateId = lc.StateId
			
					Left join Country_Master com with (NOLOCK)
					On com.CountryId = lc.CountryId
					LEFT JOIN DistributorMaster dm ON lc.DistributorId = dm.DistributorId
						WHERE LocationType IN (2, 3) and lc.Locationid in (
						select distinct(Locationid) from UserRoleLoc_link where UserId=$Userid )
						AND lc.Status=1
						 ");

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$WHLocationData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $WHLocationData;
		}
		catch(Exception $e)
		{
			$WHLocationData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
				
			return $WHLocationData;
		}
		
	}
	function searchBucket()
	{
		$connectionString = new DBHelper();
	try
	{
		$pdo_object = $connectionString->dbConnection();
	
		$stmt = $pdo_object->prepare("
			Select 
					buc.BucketId,
					BucketName, 
					Sellable
			From Bucket buc(nolock)
			Where buc.Status = 1 AND buc.ParentId Is Not Null
			And BucketId <>9");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$searchBucket = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $searchBucket;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
	}
	function TOIStatus()
	{
		$connectionString = new DBHelper();
	try{
		$pdo_object = $connectionString->dbConnection();
	
		$stmt = $pdo_object->prepare("Select	
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
			1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select	
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master(nolock) 		
			Where
				parametercode='TOIStatus'
				And isactive=	1
			Order By
				sortorder Asc");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
	}
	function FillPriceStatus()
	{
		$connectionString = new DBHelper();
	
		$pdo_object = $connectionString->dbConnection();
	try{
		$stmt = $pdo_object->prepare("Select	
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select	
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master 	with (NOLOCK)	
			Where
				parametercode='PriceMode'
				And isactive=	1
			Order By
				sortorder Asc");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
	}
	function FillPricePercentage()	{
		$connectionString = new DBHelper();
	
		$pdo_object = $connectionString->dbConnection();
	try{
		$stmt = $pdo_object->prepare("Select	
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select	
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)	
			Where
				parametercode='PercentageType'
				And isactive=	1
			Order By
				sortorder Asc");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
	}

	function searchItemUsingCode($itemCode,$type,$fromItemCode){
		
		$connectionString = new DBHelper();
		
	try {
		$pdo_object = $connectionString->dbConnection();
		
		if($type==2){
			
			$stmt = $pdo_object->prepare("

			Select	 ICM.ToItemId ItemId, ItemName,ICM.ToItemCode ItemCode, PrimaryCost, iul.UOMId, UOMName, im.Weight, im.DistributorPrice
					From	Item_Master im(nolock)
					INNER JOIN ItemCode_Mapping ICM(nolock) ON
					(ICM.FromItemCode=Im.ItemCode
					AND IM.ItemCode='$fromItemCode')
					Inner Join ItemUOM_Link  iul
					On im.ItemId = iul.ItemId
					And iul.TypeOfMeasure = 1
					Inner Join UOM_Master um
					On um.UOMId = iul.UOMId
					
					Where
					im.Status = 1 and ICM.ToItemCode='$itemCode' AND ICM.FromItemCode='$fromItemCode'
		UNION ALL 
		
Select	 ICM.ToItemId ItemId, ItemName,ICM.ToItemCode ItemCode, PrimaryCost, iul.UOMId, UOMName, im.Weight, im.DistributorPrice
					From	Item_Master im(nolock)
					INNER JOIN ItemCode_Mapping ICM(nolock) ON
					(ICM.FromItemCode=Im.ItemCode
					AND IM.ItemCode='$itemCode')
					Inner Join ItemUOM_Link  iul
					On im.ItemId = iul.ItemId
					And iul.TypeOfMeasure = 1
					Inner Join UOM_Master um
					On um.UOMId = iul.UOMId
					
					Where
					im.Status = 1 and ICM.ToItemCode='$fromItemCode' AND ICM.FromItemCode='$itemCode'
					
					
					");
		}
		else if($type==3){
			$stmt = $pdo_object->prepare("Select	  IM.ItemId , ItemName, ItemCode, PrimaryCost, iul.UOMId, UOMName, im.Weight, im.DistributorPrice
					From	Item_Master im(nolock)
					Inner Join ItemUOM_Link  iul(nolock)
					On im.ItemId = iul.ItemId
					And iul.TypeOfMeasure = 1
					Inner Join UOM_Master um
					On um.UOMId = iul.UOMId
					Where
					im.Status = 1 and  im.ItemCode='$fromItemCode'");
			
		}
		else{
		$stmt = $pdo_object->prepare("Select	im.ItemId, ItemName,ItemCode, PrimaryCost, iul.UOMId, UOMName, im.Weight, im.DistributorPrice
				From	Item_Master im(nolock)
				Inner Join ItemUOM_Link  iul(nolock)
				On im.ItemId = iul.ItemId
				And iul.TypeOfMeasure = 1
				Inner Join UOM_Master um
				On um.UOMId = iul.UOMId
				Where
					im.Status = 1 and im.ItemCode='$itemCode'");
		}
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
		
	}
	
	function calculateTP($itemCode,$percentage,$mode,$appDpp,$destinationLocationId){
		$outParam='';
			
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
				
			$sql = "{CALL usp_TOICalculateTransferPrice (@mode=:mode,@percentage=:percentage,
					@appDpp=:appDpp,@itemCode=:itemCode,@destinationLocationId=:destinationLocationId,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
				
			$stmt->bindParam(':mode',$mode,PDO::PARAM_INT );

			$stmt->bindParam(':percentage',$percentage,PDO::PARAM_INT);

			$stmt->bindParam(':appDpp',$appDpp,PDO::PARAM_INT);

			$stmt->bindParam(':itemCode',$itemCode,PDO::PARAM_STR);
			$stmt->bindParam(':destinationLocationId',$destinationLocationId,PDO::PARAM_INT);
				
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
		
	}
	
	function searchAvailQty($itemCode,$destinationLocationId,$bucket){
		
		$connectionString = new DBHelper();
		
		try {
		$pdo_object = $connectionString->dbConnection();
		
		$stmt = $pdo_object->prepare("Exec sp_SearchTOIAvailWQty '$itemCode','$destinationLocationId','$bucket','' " );
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
	}
	
	
	 function FillAppDpp()
	{
		$connectionString = new DBHelper();
	try
		{
		$pdo_object = $connectionString->dbConnection();

		$stmt = $pdo_object->prepare("Select	
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select	
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master(nolock) 		
			Where
				parametercode='AppDepreciation'
				And isactive=	1
			Order By
				sortorder Asc");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $exception;
		}
	}
	
		function searchTOItems($TOINumber,$sourceId)
		{
			$outParam='';
			
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			try{
					
				$sql = "{CALL usp_TOIItemSearch (@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
				$stmt = $pdo_object->prepare($sql);
			
				$stmt->bindParam(':TNumber',$TOINumber, PDO::PARAM_STR);
				$stmt->bindParam(':SourceAddressId',$sourceId,PDO::PARAM_INT);
			
				$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			    if(sizeof($results[0]['OutParam']) > 0)
					{
						throw new vestigeException($results[0]['OutParam']);
					}
			
				}
				catch(Exception $e){
					throw new Exception($e);
					
				}
				
				return $results;
				}
		
    	function searchTOI($TOIFormData,$TOIStatus,$TOILookUpDestAddId,$TOILookUpSourceAddId,$locationId)
			{
				parse_str($TOIFormData, $output);
			
				//$locationId = $output['location_id']; //check name in html form
				
				
				$sourceLocation=$output['SourceLocation'];
				$destinationLocation = $output['DestinationLocation'];
				
				$fromTOIDate = $output['FromTOIDate'];
				if($fromTOIDate==''){
						
					$fromTOIDate='1900-01-01T00:00:00' ;
				}
				$toTOIDate = $output['ToTOIDate'];
				if($toTOIDate==''){
						
					$toTOIDate='2099-12-31T00:00:00' ;
				}
				$toCreationDate  = $output['ToTOICreationDate'];
				if($toCreationDate==''){
			
					$toCreationDate='2099-12-31T00:00:00' ;
				}
				$fromCreationDate=$output['FromTOICreationDate'];
				if($fromCreationDate==''){
						
					$fromCreationDate='1900-01-01T00:00:00' ;
				}
				if($TOIStatus == '')
				{
					$status = $output['TOIStatus'];
				}
				else
				{
					$status = 2;
					$sourceLocation = $locationId;
					$destinationLocation = -1;
					$TOINumber = $output['TOINumber'];
				}
				
				$indentised  = $output['indentised'];
				$isexported  = 0;
				if($distributorId==''){
						
					$distributorId=0;
				}
				if($pcId==0){
						
					$pcId=-1;
				}
			
					
				$connectionString = new DBHelper();
				$pdo_object = $connectionString->dbConnection();
				try{
			
					$sql = "	Select distinct head.TOINumber, SourceLocationId, DestinationLocationId, convert(nvarchar,TOIDate,105) TOIDate, head.Status, TotalTOIQuantity, ISNULL(TotalTOIAmount,0) TotalTOIAmount,
					TotalTOITaxAmount,
					ISNULL(head.Isexported,'') AS Isexported,
					head.ModifiedDate,
					head.GrossWeight,
					IsNull(lm.Address1,'') + Char(13) + char(10) + IsNull(lm.Address2,'') + Char(13) + char(10) + IsNull(lm.Address3,'') + Char(13) + char(10) + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'') As SourceAddress,
					IsNull(lm_1.Address1,'')  + Char(13) + char(10) + IsNull(lm_1.Address2,'') + Char(13) + char(10) + IsNull(lm_1.Address3,'') + Char(13) + char(10) + IsNull(cm_1.CityName,'') +  ' ' + IsNull(sm_1.StateName,'') + ' '  + IsNull(com_1.CountryName,'') As DestinationAddress,
			
					prm.KeyValue1 StatusName, convert(nvarchar,CreationDate,105)	CreationDate, IsNull(lm.StateId,-1) StateId,Indentised, IsNull(icd.PONumber,'') PONumber, IsNull(PO.Status,-1) As POStatus, prm_1.KeyValue1 As POStatusName,head.Isexported,
					ISNULL(head.PriceMode, -1) PriceMode, ISNULL(head.Percentage, -1) Percentage, ISNULL(head.AppDep,-1) AppDep
					--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End
					From TOI_Header	head with (NOLOCK)
					Inner Join TOI_Detail det
					On head.TOINumber = det.TOINumber
					Inner Join Location_Master lm with (NOLOCK)
					On lm.LocationId = head.SourceLocationId
					Inner Join Location_Master lm_1 with (NOLOCK)
					On lm_1.LocationId = head.DestinationLocationId
			
					Inner Join Parameter_Master  prm
					On head.Status = prm.KeyCode1
					And prm.ParameterCode = 'TOIStatus'
			
					Left join City_Master cm
					On cm.CityId = lm.CityId
			
					Left join state_master sm
					On sm.StateId = lm.StateId
			
					Left join Country_Master com
					On com.CountryId = lm.CountryId
			
					Left join City_Master cm_1
					On cm_1.CityId = lm_1.CityId
			
					Left join state_master sm_1
					On sm_1.StateId = lm_1.StateId
			
					Left join Country_Master com_1
					On com_1.CountryId = lm_1.CountryId
			
					Left Join dbo.IndentConsolidation_Detail icd
					On icd.TOINumber = head.TOINumber
			
					Left Join dbo.PO_Header po
					On po.PONumber = icd.PoNumber
			
					Left Join Parameter_Master  prm_1
					On po.Status = prm_1.KeyCode1
					And prm_1.ParameterCode = 'POStatus'
			
					Where	(IsNull('$destinationLocation','-1')='-1' Or DestinationLocationId = '$destinationLocation')
					And
					(IsNull('$sourceLocation','-1')='-1' Or SourceLocationId = '$sourceLocation')
					And		(IsNull(NullIf('$TOINumber',''),'-1')='-1' Or head.TOINumber LIKE '%' + '$TOINumber' +'%')
					AND		(IsNull('$fromTOIDate','')='' OR Convert(varchar(8),IsNull(TOIDate,CAST('1900-01-01'AS DATETIME)),112) >= Convert(varchar(8),CAST('$fromTOIDate' As DateTime),112))
					AND		(IsNull('$toTOIDate','')='' OR Convert(varchar(8),IsNull(TOIDate,CAST('2099-12-31' AS DATETIME)),112) <= Convert(varchar(8),Cast('$toTOIDate' As DateTime),112))
					AND		(IsNull('$fromCreationDate','')='' OR Convert(varchar(8),CreationDate,112) >= Convert(varchar(8),Cast('$fromCreationDate' As DateTime),112))
					AND		(IsNull('$toCreationDate','')='' OR Convert(varchar(8),CreationDate,112) <= Convert(varchar(8),Cast('$toCreationDate' As DateTime),112))
					AND		(IsNull('$status','-1')='-1' Or head.Status = '$status')
					AND		(IsNull('$indentised','2')='2' Or IsNull('$indentised','-1')='-1' Or '$indentised' = Case When head.Indentised = 1 Then 1 Else 0 End)
							
					Order By TOINumber ASC
					";
					
							$stmt = $pdo_object->prepare($sql);
								
							$stmt->execute();
							$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
							
										
						$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
						return $outputData;
					}
					catch(Exception $e)
					{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
					}
			}
		function saveOrder($items,$form_data,$locationId,$createdBy,$STATUS)
		{
			$outParam='';
				
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			try{
				parse_str($form_data, $output);
				$jsonForItems =$items	 ;
		
				
				
				$jsonRemoveItem   = ''; 
				$iHnd      = '';
				$BucketId			 = $output['bucket'];
				$SourceLocationId		 = $output['createSourceLocation'];
				$DestinationLocationId	 = $output['createDestinationLocation'];
				$TOIDate				 = '';
				$CreationDate			 = '';
				$StatusId				 = $STATUS; //created
				$ModifiedBy			 = $createdBy;
				$ModifiedDate			 = '';
				$LocationCode='';
				$TNumber				 = $output['TOICreateNumber'];
				$TotalTOIQuantity		 = $output['TOICreateTOIQuantity'];
				$TotalTOIAmount			 = $output['TOICreateTOIAmount'];
				$TotalWeight			 = $output['TOICreatePOStatus'];
				$CreatedDate			  = '';
				$Indentised				 = 0 /* $output['Indentised'] */;
				$Isexported			 = 0;
				$SystemDate				 = '';
				$LocationId				 = 1;
				$IndexSeqNo				 = -1;
				$PriceMode				 = $output['pricemode'];
				$Percentage				 = $output['Percentage'];
				$AppDep	                  = $output['appDpp'];
				
				$sql = "{CALL SP_TOISave (@jsonForItems=:jsonForItems,@jsonRemoveItems=:jsonRemoveItems,@iHnd=:iHnd,@BucketId=:BucketId,@SourceLocationId=:SourceLocationId
				 ,@DestinationLocationId=:DestinationLocationId  ,@TOIDate=:TOIDate  ,@CreationDate=:CreationDate ,@StatusId=:StatusId ,@ModifiedBy=:ModifiedBy 
						,@ModifiedDate=:ModifiedDate,
						@TNumber=:TNumber,@TotalTOIQuantity=:TotalTOIQuantity,@TotalTOIAmount=:TotalTOIAmount ,@CreatedDate=:CreatedDate ,@Indentised=:Indentised,
						@Isexported=:Isexported,
						@SystemDate=:SystemDate,@LocationId=:LocationId,@IndexSeqNo=:IndexSeqNo,@LocationCode=:LocationCode,@PriceMode=:PriceMode,@Percentage=:Percentage
                      ,@AppDep=:AppDep,@TotalWeight=:TotalWeight,@outParam=:outParam)}";
				$stmt = $pdo_object->prepare($sql);
				$stmt->bindParam(':jsonForItems',$jsonForItems, PDO::PARAM_STR);
				$stmt->bindParam(':jsonRemoveItems',$jsonRemoveItems, PDO::PARAM_STR);
				$stmt->bindParam(':iHnd',$iHnd, PDO::PARAM_INT);
				$stmt->bindParam(':BucketId',$BucketId, PDO::PARAM_INT);
				$stmt->bindParam(':SourceLocationId',$SourceLocationId, PDO::PARAM_INT);
				$stmt->bindParam(':DestinationLocationId',$DestinationLocationId, PDO::PARAM_INT);
				
				$stmt->bindParam(':TOIDate',$TOIDate, PDO::PARAM_STR);
				$stmt->bindParam(':CreationDate',$CreationDate, PDO::PARAM_STR);
				
				$stmt->bindParam(':StatusId',$StatusId, PDO::PARAM_INT);
				$stmt->bindParam(':TotalWeight',$TotalWeight, PDO::PARAM_STR);
				$stmt->bindParam(':ModifiedBy',$ModifiedBy, PDO::PARAM_INT);
				$stmt->bindParam(':ModifiedDate',$ModifiedDate, PDO::PARAM_STR);
				
				$stmt->bindParam(':TNumber',$TNumber, PDO::PARAM_STR);
				$stmt->bindParam(':TotalTOIQuantity',$TotalTOIQuantity, PDO::PARAM_INT);
				$stmt->bindParam(':TotalTOIAmount',$TotalTOIAmount, PDO::PARAM_INT);
				$stmt->bindParam(':CreatedDate',$CreatedDate, PDO::PARAM_STR);
				$stmt->bindParam(':Indentised',$Indentised, PDO::PARAM_INT);
				$stmt->bindParam(':Isexported',$Isexported, PDO::PARAM_INT);
				$stmt->bindParam(':SystemDate',$SystemDate, PDO::PARAM_STR);
				$stmt->bindParam(':LocationId',$LocationId, PDO::PARAM_INT);
				$stmt->bindParam(':IndexSeqNo',$IndexSeqNo, PDO::PARAM_INT);
				$stmt->bindParam(':LocationCode',$LocationCode, PDO::PARAM_STR);
				$stmt->bindParam(':PriceMode',$PriceMode, PDO::PARAM_INT);
				$stmt->bindParam(':Percentage',$Percentage, PDO::PARAM_INT);
				$stmt->bindParam(':AppDep',$AppDep, PDO::PARAM_INT);
				$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
									
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				if($results[0]['OutParam']=='VAL0022')
				{
					throw new vestigeException('TOI Quantity is greater then location Qty');
				}
				if($results[0]['OutParam']=='INF0022')
				{
					throw new vestigeException('Concurrency');
				}
				if($results[0]['OutParam']=='INF0036')
				{
					throw new vestigeException('User Can not Add or Edit Item');
				}
				if(sizeof($results[0]['OutParam']) > 0)
				{
					throw new vestigeException($results[0]['OutParam']);
				}
					
				}
				catch(Exception $e){
					throw new Exception($e->getMessage());
						
				}
				
				return $results;
			
		}		
}


?>
