<?php
Class PickUpCentreAccount
{

	function POSLocationInfo($locationId)
	{
		try {
			
			$connectionString = new DBHelper();
			
			
			
			$pdo_object = $connectionString->dbConnection();
			
			//$locationId = $_SESSION['LocationId'];
			
			$stmt = $pdo_object->prepare("  select LocationId,Name, Locationcode from Location_master with (NOLOCK) where LocationId = $locationId");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			return $results;
			
		} catch (Exception $e) {
			
			throw new Exception($e);
		}

		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------*/
	function PUCAccDeposit($POSLocationId,$PUCId,$paymentMode,$transactionNo,$date,$mode,$depmnt)
	{
		try{
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();


			$sql = "{CALL sp_getPUCInfo (@inputParam=:POSLocationId,@inputParam2=:PUCId,@inputParam3=:paymentMode,
					@inputParam4=:transactionNo,@inputParam5=:date,@inputParam6=:mode,@inputParam7=:depmnt,@outParam=:outParam)}";
			
			
			$stmt = $pdo_object->prepare($sql);

			$stmt->bindParam(':POSLocationId',$POSLocationId, PDO::PARAM_INT);
			$stmt->bindParam(':PUCId',$PUCId,PDO::PARAM_INT);
			$stmt->bindParam(':paymentMode',$paymentMode, PDO::PARAM_INT);
			$stmt->bindParam(':transactionNo',$transactionNo,PDO::PARAM_STR);
				
			$stmt->bindParam(':date',$date, PDO::PARAM_STR);
			$stmt->bindParam(':mode',$mode,PDO::PARAM_INT);
			$stmt->bindParam(':depmnt',$depmnt,PDO::PARAM_INT);
			$stmt->bindParam(':outParam',$outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);

			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $results;
		}
		catch(Exception $e){
			//print_r($e->getMessage());
			
			throw new Exception($e);
			
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------*/
	function PUCAccSaveInfo($PUCAccDepositJson, $POSLocationId, $PUCId, $availableAmount, $DBAvailAmt,$modifiedDate,$modifiedBy,$createdBy,$reverseTransactionStatus,$reverseTransactionRemarks)
	{
		try{

			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();


			$sql = "{CALL sp_SavePUCInfo (@PUCAccDepositJson=:PUCAccDepositJson,@PCId=:PCId,@AVAILABLEAMOUNT=:AVAILABLEAMOUNT,
					@CurrentLocationId=:CurrentLocationId,@LocationCodeId=:LocationCodeId,@CREATEDBY=:CREATEDBY,@MODIFIEDBY=:MODIFIEDBY,@MODIFIEDDATE=:MODIFIEDDATE
					,@DBAvailAmt=:DBAvailAmt,@ReverseTransactionStatus=:ReverseTransactionStatus,@ReverseTransactionRemarks=:ReverseTransactionRemarks
					,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
				
			$stmt->bindParam(':PUCAccDepositJson',$PUCAccDepositJson, PDO::PARAM_STR);
			$stmt->bindParam(':PCId',$PUCId,PDO::PARAM_INT);
			$stmt->bindParam(':AVAILABLEAMOUNT',$availableAmount, PDO::PARAM_INT);
			$stmt->bindParam(':CurrentLocationId',$POSLocationId,PDO::PARAM_INT);
			$stmt->bindParam(':LocationCodeId',$POSLocationId,PDO::PARAM_INT);
			$stmt->bindParam(':CREATEDBY',$createdBy, PDO::PARAM_INT);
			$stmt->bindParam(':MODIFIEDBY',$modifiedBy,PDO::PARAM_INT);
			$stmt->bindParam(':MODIFIEDDATE',$modifiedDate,PDO::PARAM_STR);
			$stmt->bindParam(':DBAvailAmt',$availableAmount,PDO::PARAM_INT);
			$stmt->bindParam(':ReverseTransactionStatus',$reverseTransactionStatus,PDO::PARAM_INT);
			$stmt->bindParam(':ReverseTransactionRemarks',$reverseTransactionRemarks,PDO::PARAM_STR);
			$stmt->bindParam(':outParam',$outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
				
			$outputFlag = 1;
				
			if($results[0]['outParam'] == "INF0144")
			{
				$outputFlag = 0;
				throw new vestigeException("Transaction number already exist",3004);
			}
			else if($results[0]['outParam'] == "INF0228")
			{

				
				
				throw new vestigeException("Multiple Transaction with same transaction number can't be saved",3005);
				
				
			}
			else if($results[0]['outParam'] == "PUCEXP001")
			{
				throw new vestigeException("Reverse transaction amount is greater than available amount",3001);
			}
			else if($results[0]['outParam'] == "PUCEXP002")
			{
				throw new vestigeException("Used amount is greater than available amount",3002);
			}
			else if($results[0]['outParam'] == "PUCEXP003")
			{
				throw new vestigeException("Transaction already reversed",3003);
			}
			
				
		}
		catch(Exception $e){
			//print_r($e->getMessage());
			
			
			
			throw new vestigeException($e->getMessage(),$e->getCode());
				
		}
		
		return $results;
	}
	
	
	
	
	function reversePUCTransaction($PUCId,$transactionNo,$remarks,$transactionDepositAmount,$modifiedBy)
	{
		try {
				
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
			
			
				
			//$locationId = $_SESSION['LocationId'];
				
			//$stmt = $pdo_object->prepare("update PUCDeposit set Amount = 0.0000,Remarks='$remarks',ModifiedBy = $modifiedBy,ModifiedDate = GETDATE() where TransactionNo = '$transactionNo' and PCId = $PUCId");
			
			$sql = "select PA.AvailableAmount-sum(PTD.TransAmount) as 'AvailableAmount' from PUCAccount PA(nolock), PUCTransactionDetail PTD(nolock) where PA.PCId = $PUCId
					and PTD.PCId = $PUCId group by PA.AvailableAmount";
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			if(intval($results[0]['AvailableAmount']) > 0)
			{
				
				if(intval($results[0]['AvailableAmount']) > $transactionDepositAmount)
				{
					
					$pdo_object->beginTransaction();
					
					$sql = "
							update PUCDeposit set Amount = 0.0000,Remarks='$remarks',ModifiedBy = $modifiedBy,ModifiedDate = GETDATE() where TransactionNo = '$transactionNo' and PCId = $PUCId;
							
							update PUCAccount set AvailableAmount = AvailableAmount - $transactionDepositAmount from PUCAccount where PCId = $PUCId;
							
							";
					
					
					$stmt = $pdo_object->prepare($sql);
					
					$stmt->execute();
					
					$pdo_object->commit();
					//$pdo_object->prepare("update PUCAccount set AvailableAmount = AvailableAmount - $transactionDepositAmount from PUCAccount where PCId = $PUCId");
						
					//$stmt->execute();
					
					$reversedPUCTransactionArray = array();
						
					$reversedPUCTransactionArray['ReverseTransactionStatus'] = 1;
					
				}
				else
				{
					$pdo_object->rollBack();
					
					throw new vestigeException("Reverse transaction amount is greater than available amount",4002);
				}
			}
			else
			{
				throw new vestigeException("Used amount is greater than available amount",4001);
			}
			//return $results;
			
				
		} catch (Exception $e) {
				
			throw new Exception($e);
		}
		
		return $reversedPUCTransactionArray;
	}
	
	
}	


?>