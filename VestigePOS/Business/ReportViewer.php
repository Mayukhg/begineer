<?php
 Class  ReportViewer {
function PUCId($locationId)
{

$connectionString = new DBHelper();

		$pdo_object = $connectionString->dbConnection();
	$stmt = $pdo_object->prepare("SELECT * from (SELECT [LocationId],CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' WHEN 4 THEN 'PC' END LocationType
					, [Name] + '-' + [LocationCode] AS [LocationCode],[Name] AS LocationName, LocationConfigId,
					[Address1],[Address2],[Address3],[Address4]
					,LM.[CityId],LM.[Pincode],LM.[StateId],LM.[CountryId],[Phone1],[Phone2]
					,CM.CityName,SM.StateName,CNM.CountryName
					,[Mobile1],[Mobile2],[Fax1],[Fax2],[EmailId1],[EmailId2]
					,[WebSite],[ModifiedDate], ISNULL([ReplenishmentLocationId], -1) 'ReplenishmentId'
					,1 AS LocationCode1,1 isprimaryLocation
					FROM [Location_Master]LM with (NOLOCK)
					JOIN City_Master CM with (NOLOCK) ON LM.[CityId]=CM.[CityId]
					JOIN State_Master SM with (NOLOCK) ON LM.[StateId]=SM.[StateId]
					JOIN Country_Master [CNM] with (NOLOCK) ON LM.[CountryId]=CNM.[CountryId]
					WHERE (LM.[Locationid]=$locationId OR ISNULL($locationId,'')='')
					AND LM.Status =1
					UNION
					SELECT  [LocationId],CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' WHEN 4 THEN 'PC' END LocationType
					,[Name] + '-' + [LocationCode] AS [LocationCode] ,[Name] AS LocationName, LocationConfigId,
					[Address1],[Address2],[Address3],[Address4]
					,LM.[CityId],LM.[Pincode],LM.[StateId],LM.[CountryId],[Phone1],[Phone2]
					,CM.CityName,SM.StateName,CNM.CountryName
					,[Mobile1],[Mobile2],[Fax1],[Fax2],[EmailId1],[EmailId2]
					,[WebSite],[ModifiedDate], ISNULL([ReplenishmentLocationId], -1) 'ReplenishmentId'
					, 2 AS LocationCode1,0 isprimaryLocation
					FROM [Location_Master]LM with (NOLOCK)
					JOIN City_Master CM with (NOLOCK) ON LM.[CityId]=CM.[CityId]
					JOIN State_Master SM with (NOLOCK) ON LM.[StateId]=SM.[StateId]
					JOIN Country_Master [CNM] with (NOLOCK) ON LM.[CountryId]=CNM.[CountryId]
					WHERE (LM.[ReplenishmentLocationId]= $locationId)
					AND LM.Status =1
			)A
					ORDER BY LocationCode1,LocationCode;");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $results;

}

function getBusinessMonths(){

	$connectionString = new DBHelper();
	$pdo_object = $connectionString->dbConnection();
	$stmt = $pdo_object->prepare("Select Convert(varchar(20),MonthCloseDate,105) as MonthCloseDate from BusinessMonth(nolock) order by MonthCloseDate");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $results;
}
 }
?>