<?php
/**
 * Define a custom exception class
 */



class vestigeException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // some code
    
    	//$vestigeUtil = new VestigeUtil();
    	
    	//$loggedInUserLocationName = $vestigeUtil->getSessionData("loggedInUserLocationName");
    	
    	
    	
    	
    	watchdog("Vestige", $message."-".$code); 
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
       // return __CLASS__ . "- [{$this->code}]- {$this->message}\n";
       return "{$this->message}";
    }

    public function getMessageForUser() {
        return  "A custom function for this type of exception\n";
    }
    
    /* public function putErrorCodeAndMessageInArray()
    {} */
}
