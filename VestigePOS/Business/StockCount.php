<?php

//include 'VestigeCommon.php';

Class StockCount
{

	var $vestigeUtil;
	function __construct()
	
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	
	function searchWHLocation()
	{
	
		try
		{
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
				
			$stmt = $pdo_object->prepare("Select
					'-1'  LocationId,
					'All' DisplayName,
					''  LocationType,
					'All' LocationName,
					'-' [Address],
					'-1' CityId,
					'Select' CityName,
					'Select' LocationCode
					,'' DistributorName
					,'' IsexportLocation
					,'' CountryName
					,'' IECCode
					
					");
	
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
					$WHLocationData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
					return $WHLocationData;
		}
		catch(Exception $e)
		{
			$WHLocationData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
	
			return $WHLocationData;
		}
	
	}



	
	/**
	 * function used to generating seq no.
	 */
	
	function generateLogNumber($seqType,$locationId)
	{
		try
		{
			
			$vestigeCommon = new VestigeCommon();

			$generatedLogNo = $vestigeCommon->generateLogNumber($seqType, $locationId); //function in vestigeCommon.php.
			
			return $generatedLogNo;
		}
		catch(PDOException $e){
				
			return $e->getMessage();
		}
	}
	
	
	
	/**
	 * function used to search bucket.
	 */
	function searchBucket()
	{
		try
		{
			$vestigeCommon = new VestigeCommon();
	
			$generatedLogNo = $vestigeCommon->searchBucket(); //function in vestigeCommon.php.
				
			return $generatedLogNo;
		}
		catch(PDOException $e){
	
			return $e->getMessage();
		}
	}
	function searchItemInformation($itemCode)
	{
		try
		{
		
			$vestigeUtil = new VestigeUtil();
			
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
				
			$stmt = $pdo_object->prepare("select * from item_master with (NOLOCK) where ItemCode = '$itemCode';");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$itemData = $vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $itemData;
		}
		catch(Exception $e)
		{
			$itemData = $vestigeUtil->formatJSONResult('', $e->getMessage());
		
			return $itemData;
		}
		
		
	}
	
	function validateBatchNo($batchNo)
	{
		$vestigeUtil = new VestigeUtil();
		
		try
		{
			$connectionString = new DBHelper();
		
			$pdo_object = $connectionString->dbConnection();
		
			$stmt = $pdo_object->prepare(" select ItemId,BatchNo,ItemBatchId,MRP,ManufactureBatchNo,Status,MfgDate,ExpDate,CreatedBy,CreatedDate,
											ModifiedBy,ModifiedDate from ItemBatch_Detail  with (NOLOCK) where ItemBatchId = '$batchNo'");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
			$batchValidationData = $vestigeUtil->formatJSONResult(json_encode($results), '');
		
			return $batchValidationData;
		}
		catch(Exception $e)
		{
			$batchValidationData = $vestigeUtil->formatJSONResult('',$e->getMessage());
		
			return $batchValidationData;
		}	
	}
	
	function stockCountInitiatedForLocation($locationId)
	{
		try
		{
			 
			$key = 'isStockCountInitiatedFor'.$locationId;
			
			$setSessionFlag = $vestigeUtil->setSessionData($key, 1);// SIMPLY setting up session data for currenly selected location id.
			 
			return $setSessionFlag;
			
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}
	
	function getSessionValue($locationId)
	{
		$vestigeUtil = new VestigeUtil();
		
		$locationSessionKey = 'isStockCountInitiatedFor'.$locationId;
		
		$sessionInitiatedFlag = $vestigeUtil->getSessionData($locationSessionKey);
		
		return $sessionInitiatedFlag;
	}
	
	function getItemsForLocation($locationId)
	{
		$vestigeUtil = new VestigeUtil();
		
		$itemsObject = new Items();
		
		$getItems = $itemsObject->getItems($locationId);
		
		$itemsData = $vestigeUtil->formatJSONResult(json_encode($getItems), '');
		
		return $itemsData;
		
		
	}
	
	function stockCountCreate($itemInformation,$batchInformation,$stockCountNo,$locationId,$statusId,$initiatedDate,$remarks,$createdBy)
	{
		
		try
		{
			$vestigeUtil = new VestigeUtil();
		
			$connectionString = new DBHelper();
		
			$pdo_object = $connectionString->dbConnection();
			
			$itemInformation = $itemInformation;
				
			$batchInformation = $batchInformation;
				
			$stockCountNo = $stockCountNo;
			
			$outparam = '';
			
			$sql = "{CALL sp_StockCountSave (@jsonForItems=:itemInformation,@jsonForItemBatch=:batchInformation,@SeqNo=:SeqNo
					,@LocationId=:locationId,@InitiatedDate=:initiatedDate,@createdBy=:createdBy,@StatusId=:statusId
					,@Remarks=:remarks,@outParam=:outParam)}";
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->bindParam(':itemInformation',$itemInformation,PDO::PARAM_STR);
			$stmt->bindParam(':batchInformation',$batchInformation,PDO::PARAM_STR);
			$stmt->bindParam(':SeqNo',$stockCountNo,PDO::PARAM_STR);
			$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
			$stmt->bindParam(':initiatedDate',$initiatedDate,PDO::PARAM_STR);
			$stmt->bindParam(':createdBy',$createdBy,PDO::PARAM_INT);
			$stmt->bindParam(':statusId',$statusId,PDO::PARAM_INT);
			$stmt->bindParam(':remarks',$remarks,PDO::PARAM_STR);
			
			
			$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			if($outparam == "INF0022")
			{
			
				$stockCountData = $vestigeUtil->formatJSONResult("\"$outparam\"", '');
			
				return $stockCountData;
			}
			else if($results[0]['outParam'] == "SC0001")
			{
				throw new vestigeException("Stock count already initiated for this location.");
			}
			else if($results[0]['outParam'] == "SC0002")
			{
				throw new vestigeException("Stock count not confirmed or closed after initiation for this location.");
			}
			else if(sizeof($result[0]['outparam']) > 0)
			{
			
				throw new vestigeException($result[0]['outparam']);
			}
			else
			{
				$stockCountData = $vestigeUtil->formatJSONResult(json_encode($results), '');
				
				if($statusId == 6 || $statusId == 5)
				{
					$stockCountInitiateKey = "isStockCountInitiatedFor".$locationId;
					
					$vestigeUtil->setSessionData($stockCountInitiateKey, 0);
				}
			
				return $stockCountData;
			}
			
			
			
		}
		catch(Exception $e)
		{
			/* $stockCountData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $stockCountData; */
			throw new Exception($e);
			
		}
	}
	
	/**
	 * Stock Count Status
	 */
	
	
	function getStockCountStatusInfo()
	{
		try
		{
			$vestigeUtil = new VestigeUtil();
		
			$connectionString = new DBHelper();
		
			$pdo_object = $connectionString->dbConnection();
		
			$stmt = $pdo_object->prepare("select KeyCode1,KeyValue1  from Parameter_Master with (NOLOCK) where ParameterCode = 'STOCKSTATUS' and KeyCode1 <> 4");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$stockCountStatusData = $vestigeUtil->formatJSONResult(json_encode($results), '');
		
			return $stockCountStatusData;
		}
		catch(Exception $e)
		{
			$stockCountStatusData = $vestigeUtil->formatJSONResult('',$e->getMessage());
		
			return $stockCountStatusData;
		}
	}
	
	/**
	 * Function used to show search result.
	 */
	function getStockCountSearchData($stockCountSearchFormData)
	{
		$vestigeUtil = new VestigeUtil();
		
		try
		{
			
			
			
			parse_str($stockCountSearchFormData, $output);
			
			$stockCountSeqNo = $output['SCSearchSeqNo'];
			$stockCountSearchLocationId  = $output['SCSearchLocation'];
			$stockCountSearchStatus  = $output['SCSearchStatus'];
			
			$stockCountInitiatedDate  = $output['SCInitiatedDate'];
			$stockCountInitiatedBy  = $output['CRInitiatedBy'];
			
			$stockCountConfirmDate= $output['CRConfirmedDate'];
			
			$stockCountConfirmedBy = $output['SCConfirmedBy'];
			 
			if($stockCountSeqNo=='')
			{
				$stockCountSeqNo='-1';
			}
			
			
			
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "select UM.UserName as InitiatedBy,UM2.UserName as ExecutedBy,PM.KeyValue1 as StatusName,
					LM.Address1+LM.Address2+LM.Address3 as LocationAddress,LM.Name+'-'+LM.LocationCode as LocationName,LM.LocationId,SCH.StockCountSeqNo,
					convert(varchar(20),SCH.InitiatedDate,105) InitiatedDate,convert(varchar(20),SCH.ExecutedDate,105) ExecutedDate, SCH.Status
                     from Location_Master LM
                      LEFT JOIN StockCountHeader SCH ON LM.LOCATIONID=SCH.LOCATIONID
                      LEFT JOIN USER_MASTER UM ON UM.USERID=SCH.INITIATEDBY
                      LEFT JOIN USER_MASTER UM2 ON UM2.USERID=SCH.ExecutedBy
                      LEFT JOIN parameter_Master PM ON PM.KeyCode1=SCH.Status AND PM.parametercode='STOCKSTATUS'
                      WHERE LM.STATUS=1 AND LM.LOCATIONTYPE=3 and 
					 (IsNull('$stockCountSeqNo','-1')='-1' Or SCH.StockCountSeqNo LIKE '%' + '$stockCountSeqNo')and
					(isnull('$stockCountSearchLocationId','-1')='-1' or SCH.locationid='$stockCountSearchLocationId') and
					(isNull('$stockCountSearchStatus','-1') = '-1' or SCH.Status = '$stockCountSearchStatus') and
					(isnull('$stockCountInitiatedBy','')='' or SCH.InitiatedBy = '$stockCountInitiatedBy') and
					(isnull('$stockCountConfirmedBy','')='' or SCH.ExecutedBy = '$stockCountConfirmedBy') and
					(isnull('$stockCountInitiatedDate','')='' or 
					(convert(varchar(10),cast(SCH.InitiatedDate as datetime),112)) = convert(varchar(10),cast('$stockCountInitiatedDate' as datetime),112 ))
					 and
					(isnull('$stockCountConfirmDate','')='' or
					(convert(varchar(10),cast(SCH.ExecutedDate as datetime),112)) =
					convert(varchar(10),cast('' as datetime),112 )) order by SCH.InitiatedDate DESC";
			
		
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stockCountSearchData = $vestigeUtil->formatJSONResult(json_encode($results), '');
		
			return $stockCountSearchData;
		}
		catch(Exception $e)
		{
			$stockCountSearchData = $vestigeUtil->formatJSONResult('',$e->getMessage());
		
			return $stockCountSearchData;
		}
	}
	
	/**
	 * Function used to save stock count related information.
	 */
	function stockCountSave($locationId,$createdBy,$griddata)
	{
		$vestigeUtil = new VestigeUtil();
		
		try {
		
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$griddata=$griddata;
			
				
			
			
			$outparam = '';
			
			$sql = "{CALL SP_StockCountSaveStatus (@jsonForstockcount=:stockcountinfo,@createdBy=:createdBy
					,@outParam=:outParam)}";
			
			
			
			
			$stmt = $pdo_object->prepare($sql);
			
			
			
			$stmt->bindParam(':stockcountinfo',$griddata,PDO::PARAM_STR);
			
			$stmt->bindParam(':createdBy',$createdBy,PDO::PARAM_INT);
			
			
			
			
			
			$stmt->bindParam(':outParam', $outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
			
			
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
		
					
				$initiatedStockCountData = $vestigeUtil->formatJSONResult(json_encode($results), '');
					
				return $initiatedStockCountData;
			}
			catch(Exception $e)
			{
				$initiatedStockCountData = $vestigeUtil->formatJSONResult('',$e->getMessage());
					
				return $initiatedStockCountData;
			}
	}
	
	
	/**
	 * Function used to load stock count related information.
	 */
	
	function getStockCountRelatedInformation($stockCountStatusId,$stockCountSeqNo)
	{
		
		$vestigeUtil = new VestigeUtil();
		
		if($stockCountStatusId == 3)
		{
			try
			{
			
				
				
				$connectionString = new DBHelper();
			
				$pdo_object = $connectionString->dbConnection();
			
				$sql = "select SCD.ItemId,IM.ItemCode,IM.ItemName from
						StockCountDetail SCD with (NOLOCK) left join Item_Master IM with (NOLOCK) on SCD.ItemId =
						IM.ItemId where SCD.StockCountSeqNo = '$stockCountSeqNo' order by IM.ItemName";
				
				
				$stmt = $pdo_object->prepare($sql);
				
				
				
				
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
				$initiatedStockCountData = $vestigeUtil->formatJSONResult(json_encode($results), '');
			
				return $initiatedStockCountData;
			}
			catch(Exception $e)
			{
				$initiatedStockCountData = $vestigeUtil->formatJSONResult('',$e->getMessage());
			
				return $initiatedStockCountData;
			}
		}
		if($stockCountStatusId == 1)
		{
			try
			{
					
				$connectionString = new DBHelper();
					
				$pdo_object = $connectionString->dbConnection();
					
				
				$sql = " select SCD1.Itemid,IM.ItemCode,IM.ItemName,SCD1.PhysicalQuantity as OnHandQty,SCD2.PhysicalQuantity as DamagedQty
						from StockCountDetail SCD1 with (NOLOCK) ,StockCountDetail SCD2 with (NOLOCK) left join Item_Master IM with (NOLOCK) on SCD2.ItemId = IM.ItemId
						where  SCD1.BucketId=5 AND SCD2.BucketId=6 AND
						SCD1.ItemId=  SCD2.ItemId AND SCD1.StockCountSeqNo=SCD2.StockCountSeqNo
						AND SCD1.StockCountSeqNo = '$stockCountSeqNo'";
				
				$stmt = $pdo_object->prepare($sql);
			
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
				$savedStockCountData = $vestigeUtil->formatJSONResult(json_encode($results), '');
					
				return $savedStockCountData;
			}
			catch(Exception $e)
			{
				$savedStockCountData = $vestigeUtil->formatJSONResult('',$e->getMessage());
					
				return $savedStockCountData;
			}	
		}
		if($stockCountStatusId == 5)
		{
			try
			{
					
				$connectionString = new DBHelper();
					
				$pdo_object = $connectionString->dbConnection();
					
				$sql = "select SCD1.Itemid,IM.ItemCode,IM.ItemName,SCD1.PhysicalQuantity as OnHandQty,SCD2.PhysicalQuantity as DamagedQty
						,SCD1.SystemQuantity as OnHandSystem,SCD2.SystemQuantity as DamagedSystem,
						((SCD1.SystemQuantity+SCD2.SystemQuantity)-(SCD1.physicalQuantity + SCD2.physicalQuantity)) as Varience
						,((SCD1.SystemQuantity+SCD2.SystemQuantity)-(SCD1.physicalQuantity + SCD2.physicalQuantity))*IM.DistributorPrice as VariancePrice
						 from StockCountDetail SCD1 with (NOLOCK),StockCountDetail SCD2 with (NOLOCK) left join Item_Master IM with (NOLOCK) on SCD2.ItemId = IM.ItemId
						where  SCD1.BucketId=5 AND SCD2.BucketId=6 AND
						SCD1.ItemId=  SCD2.ItemId AND SCD1.StockCountSeqNo=SCD2.StockCountSeqNo
						AND SCD1.StockCountSeqNo = '$stockCountSeqNo'";
				
				
				
				
				$stmt = $pdo_object->prepare($sql);
				
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
				$confirmedStockCountData = $vestigeUtil->formatJSONResult(json_encode($results), '');
					
				return $confirmedStockCountData;
			}
			catch(Exception $e)
			{
				$confirmedStockCountData = $vestigeUtil->formatJSONResult('',$e->getMessage());
					
				return $confirmedStockCountData;
			}	
		}
	}
	
	
	/**
	 * function provice currently logined user information.
	 */
	function getLoginUserInfo($locationId)
	{
		return $vestigeUtil->getSessionData("CreatedByName");
	}
	
	function stockCountModuleFuncHandling()
	{
		
	}
}		
?>
