<?php
Class OrderMovement
{
	/*------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function will return distributor information from pickup center id.
	 * @param unknown $selectedPickUpCenter
	 * @return multitype:
	 */
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	function searchOrderHistory( $HistoryStatus,$historyLogNo,$HistoryFromDate,$historyToDate,$historyDistributorNo,$locationId,$loggedInUserId,$isServiceCentreUser)
	{
	
		try{
				
			$pdo_object = POSBusinessClass :: dbConnectionInfo();
				
			$dynamicWhereClause = "AND OL.BOId='$locationId'";
				
			if($isServiceCentreUser == true){
				$dynamicWhereClause = "";
			}
				
			$sql = "select PMS.KeyValue1 AS StatusName,
CO.LogNo AS LogNo,
ISNULL(OL.DistributorId,0)[DistributorId],
ISNULL(LTRIM(RTRIM(DM.DistributorFirstName)),'')+' '+ISNULL(LTRIM(RTRIM(DM.DistributorLastName)) ,'')As [DistributorName],
ISNULL(OL.[CreatedDate],'')[CreatedDate],
( SELECT isnull(sum(COP.PaymentAmount),0) 
       FROM COPayment COP with(nolock) , coheader COH with(nolock) 
       WHERE cop.CustomerOrderNo = COH.CustomerOrderNo AND
        COH.LogNo = CO.LogNo
       AND COH.Status = 3 
       AND COH.BOId = '$locationId'
    ) [OrderAmount] 


            FROM [COHeader] CO with (NOLOCK)
			INNER JOIN [OrderLog] OL with (NOLOCK)
			ON OL.LogNo = CO.LogNo AND OL.logtype=2
			LEFT JOIN DistributorMaster DM with (NOLOCK) On OL.DistributorId=DM.DistributorId
			LEFT JOIN Parameter_Master PMS with (NOLOCK) ON
			PMS.ParameterCode='COLOGSTATUS' AND PMS.KeyCode1=OL.Status
			WHERE OL.logtype=2 AND OL.Status=1 AND
			OL.Logno like '%DIST%' 
			AND  CO.LogNo not in(select CO.LogNo from OrderLog OL inner join COHeader CO
			 on CO.LogNo=OL.LogNo
			  where CO.Status in (1,2,4) and CO.BOId='$locationId') AND
			(ISNULL('$historyDistributorNo',-1)=-1 OR '$historyDistributorNo'=0 OR OL.[DistributorId]='$historyDistributorNo')
			AND (Convert(varchar(10),CAST('$HistoryFromDate' AS DATETIME),112)='19000101' OR Convert(varchar(10),OL.[CreatedDate],112)>=Convert(varchar(10),CAST('$HistoryFromDate' AS DATETIME),112))
			AND (Convert(varchar(10),CAST('$historyToDate' AS DATETIME),112)='19000101' OR CONVERT(varchar(10),OL.[CreatedDate],112)<=Convert(varchar(10),CAST('$historyToDate' AS DATETIME),112))
			AND (ISNULL('$historyLogNo','')='' OR  OL.[LogNo] like '%$historyLogNo%' )
			AND (ISNULL('-1',-1)=-1 OR '-1'=0 OR OL.CreatedBy='-1')"
			.$dynamicWhereClause.
			" group by PMS.KeyValue1,CO.LogNo,OL.DistributorId,OL.[CreatedDate],DM.DistributorFirstName,DM.DistributorLastName
					HAVING sum(CO.paymentAmount)>0
					ORDER BY CO.LogNo,OL.[CreatedDate] desc";
				
				
			
				
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			 
			$historyOrderData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			 
			return $historyOrderData;
	
		}
		catch (PDOException $e) {
				
			$historyOrderData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
	
			return $historyOrderData;
	
		}
	}
	
	
	function historyOrderStatus()
	{
		try
		{
			$connectionString = new DBHelper();
	
			$pdo_object = $connectionString->dbConnection();
	
			$stmt = $pdo_object->prepare("Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with(nolock)
			Where
				parametercode='ORDERSTATUS'
				And isactive=	1
			Order By
				sortorder Asc");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
			$historyOrderStatusData = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
	
			return $historyOrderStatusData;
		}
		catch(Exception $e)
		{
			$historyOrderStatusData = $this->vestigeUtil->formatJSONResult('',$e->getMessage());
	
			return $historyOrderStatusData;
		}
		 
		 
	}
function searchWHLocation($locationId)    
	{
		try
		{
			$connectionString = new DBHelper();

			
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare("Select	lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName,
				CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType, [Name] AS LocationName,
				ISNULL([Address1],'')+' '+ISNULL([Address2],'')+ISNULL([Address3],'')+ISNULL([Address4],'') +  ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'')
				As [Address],cm.CityId, cm.CityName,
				lc.LocationCode,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,IsexportLocation,com.CountryName,IECCode
				From Location_Master lc with (NOLOCK)
				Left join City_Master cm with (NOLOCK)
			
				On cm.CityId = lc.CityId
			
				Left join state_master sm with (NOLOCK)
				On sm.StateId = lc.StateId
			
				Left join Country_Master com with (NOLOCK)
				On com.CountryId = lc.CountryId
				LEFT JOIN DistributorMaster dm  with (NOLOCK)
					ON lc.DistributorId = dm.DistributorId
				WHERE LocationType IN (2, 3)
				AND lc.Status=1 AND lc.LocationId='$locationId'");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			$searchWHLocation = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			
			return $searchWHLocation;
		}
		catch(Exception $e)
		{
			$searchWHLocation = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
			
			return $searchWHLocation;
		}
		
		
	}
	
	function searchDestinationLocation($locationId,$loggedInUserId)
	{
		try
		{
			$connectionString = new DBHelper();
	
				
			$pdo_object = $connectionString->dbConnection();
				
			$stmt = $pdo_object->prepare("
				select  lc.LocationId,[Name] + ' - ' + LocationCode As DisplayName  from location_master lc 
		Left  join state_master sm with (NOLOCK)
		on sm.StateId = lc.StateId and 
		 LocationType IN (3) AND lc.Status=1				
		where sm.StateId=(select StateId from location_master with (NOLOCK) where locationid='$locationId' AND LocationType IN (3) AND status=1);
		");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
			$searchWHLocation = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			return $searchWHLocation;
		}
		catch(Exception $e)
		{
			$searchWHLocation = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
				
			return $searchWHLocation;
		}
	
	
	}
	

	function OrderMove($locationId,$loggedInUserId,$LogNo,$DestinationLocation)
	{
		
		try
		{
			$connectionString = new DBHelper();
		
		
			$pdo_object = $connectionString->dbConnection();
		
		$sql = "{CALL sp_Movement (@inputParam=:SourceLocationId,@inputParam2=:LogNo,@inputParam3=:DestinationLocation,@outParam=:outParam)}"; 
		$stmt = $pdo_object->prepare($sql);
		
		$stmt->bindParam(':SourceLocationId',$locationId, PDO::PARAM_STR);
		$stmt->bindParam(':LogNo',$LogNo,PDO::PARAM_STR);
		$stmt->bindParam(':DestinationLocation',$DestinationLocation, PDO::PARAM_STR);
		
		$stmt->bindParam(':outParam',$outparam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
		
		
			$stmt->execute();
			
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
			
			
			$searchWHLocation = $this->vestigeUtil->formatJSONResult(json_encode($results), '');
		
			return $searchWHLocation;
			
		}
		catch(Exception $e)
		{
		$searchWHLocation = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
		
				return $searchWHLocation;
		}
		
	}
	
}