<?php

//include 'TransferOutInstruction.php';

		
Class LookUpScreenData
{
	function itemSearchLookUpData($itemCode,$itemLookName,$locationId)
	{
		try{
			/* parse_str($itemLookpFormData, $output);
			 *///echo $output['first'];  // value
			//echo $output['arr'][0]; // foo bar
			//echo $output['arr'][1];
			/* $itemLookUpProductCode = $output['ItemLookUpProductCode'];
			$itemLookUpProductName = $output['ItemLookUpProductName'];
			$itemLookUpSubCategoryCode = $output['ItemLookUpSubCategoryCode'];
			$itemLookUpIsComposite = $output['ItemLookUpIsComposite'];
		 */
			$connectionString = new DBHelper();
			
			$pdo_object = $connectionString->dbConnection();
			
			$sql = "SELECT top 51 IL.[ItemId],
					I.ItemCode,
					I.[ItemName],
					I.DistributorPrice,
					CASE WHEN I.PromotionParticipation = 0
					THEN 'NO' ELSE 'YES' end PromotionParticipation,
					CASE I.Status WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END AS StatusName,
					I.IsKit
					FROM [ItemLocation_Link]IL with (NOLOCK)
					JOIN [Item_Master]I ON IL.[ItemId]=I.[ItemId]
					JOIN [MerchHierarchy_Master] MHM with (NOLOCK)
					ON I.MerchHierarchyDetailId = MHM.MerchHierarchyId
					WHERE
					(ISNULL($locationId,0)=-1 OR $locationId=0 OR IL.LocationId=$locationId)
					AND (isnull(nullif('$itemLookName',''),'-1') = '-1' OR I.ItemName like '%$itemLookName%')
					AND (isnull(nullif('$itemCode',''),'-1') = '-1' OR I.ItemCOde like '%$itemCode%')
					AND I.Status='1' AND IL.Status='1'";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			if(sizeof($results) == 0)
			{
				throw new vestigeException("Result not found for item",8001);
			}
			
			
				
		}
		catch (PDOException $e) {
		
			throw new Exception($e);
			
		}
		
		return $results;
		
	}
	
	/**
	 * 
	 * @param unknown $formData - parameter used to hold data of my account form.
	 */
	function myAccountSearchLookUpData($formData,$logNo)
	{
		try{
			
			$POSBusinessClassObject = new POSBusinessClass();
			
			$vestigeUtil = new VestigeUtil();
			
			parse_str($formData, $output);
			 /*///echo $output['first'];  // value
			//echo $output['arr'][0]; // foo bar
			//echo $output['arr'][1];
			/* 
			 * 
			 */
			
			$loggedInUserType = $vestigeUtil->getSessionData("loggedInUserType");
			
			
			if($loggedInUserType == 'D')
			{
				$pucId = -1;
				
				$distributorid = $vestigeUtil->getSessionData("loggedInDistributorId");
			}
			else if($loggedInUserType == 'P')
			{
				
				$pucId =  $vestigeUtil->getSessionData("pickUpCentreId");
				$logNo='';
				//$distributorid = $vestigeUtil->getSessionData("loggedInDistributorId");
			}
			else
			{
				$pucId = -1;
				$logNo='';
				$distributorid = '';
			}
			
			$fromOrderDate = $output['FromOrderDate'];
			$toOrderDate = $output['ToOrderDate'];
			$orderStatus = $output['OrderStatus'];
			
			$locationId = $vestigeUtil->getSessionData("loggedInUserLocation");
			
			$loggedInUserId = $vestigeUtil->getSessionData("loggedInUser");
			
			$connectionString = new DBHelper();

			$pdo_object = $connectionString->dbConnection();
			
			$myAccountOrderSearchData = $POSBusinessClassObject->searchOrderHistory($orderStatus, '', '',$logNo, $fromOrderDate, $toOrderDate, $distributorid, $pucId, $locationId,$loggedInUserId,false,$loggedInUserType);

		}
		catch (PDOException $e) {

			throw new Exception($e);
				
		}

		return $myAccountOrderSearchData;
	}
	
	//search of pincode detail;
	function Seachpindetail($Pincode)
	{
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
	
			$sql = "select top 1 P.PinCode,Cm.CountryId,Cm.CountryName ,Sm.StateName,Sm.StateId,CIm.CityId,Cim.CityName
     ,Sm.ZoneId,O.OrgHierarchyCode ZoneCode,O.Name ZoneName,O2.OrgHierarchyId SubZoneId,
     O2.OrgHierarchyCode SubZoneName,O2.Name AreaName,O2.OrgHierarchyId AreaId
     from [PinCodeLookup] p inner join Country_Master Cm on Cm.CountryId=p.COuntryId
     inner join State_Master Sm On Sm.StateId=p.StateId 
     inner join City_Master CIm on Cim.CityId=P.City
     inner join OrgHierarchy_Master o On Sm.ZoneId=O.OrgHierarchyId
     inner join OrgHierarchy_Master O2 on O2.OrgHierarchyId=P.SubZoneid
     where P.PinCode ='$Pincode'";
	
			$stmt = $pdo_object->prepare($sql);
	
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
			return $results;
			
		}
		catch(PDOException $e){
			//print_r($e->getMessage());
		}
		
	}
	
	
	/**
	 *
	 * @param unknown $formData - parameter used to hold data of my account form.
	 */
	function CODSeachLookUpData($formData)
	{
		try {
			
			$POSBusinessClassObject = new POSBusinessClass ();
			
			$vestigeUtil = new VestigeUtil ();
			
			parse_str ( $formData, $output );
			
			$loggedInUserType = $vestigeUtil->getSessionData ( "loggedInUserType" );
			
			if ($loggedInUserType != 'E') {
				
				throw new vestigeException ( "Option Available for user only" );
			}
			
			$orderNo = $output ['OrderNo'];
			$invoiceNo = $output ['InvoiceNo'];
			$IsPaymentReceived = $output ['IsPaymentReceived'];
			if($IsPaymentReceived == 1){
				$IsPaymentReceived = 1;
			}
			else{
				$IsPaymentReceived = 0;
			}
			
			$locationId = $vestigeUtil->getSessionData ( "loggedInUserLocation" );
			
			$loggedInUserId = $vestigeUtil->getSessionData ( "loggedInUser" );
			
			$connectionString = new DBHelper ();
			
			$pdo_object = $connectionString->dbConnection ();
			
			$sql = "select CODP.CustomerOrderNo,CODP.InvoiceNo,CODP.PaymentAmount,
					UM.UserName PaymentReceivedBy,CIH.InvoiceDate FROM CODPayments CODP with (nolock)
					LEFT JOIN User_Master UM with (nolock) ON CODP.PaymentReceivedBy = UM.UserId
					LEFT JOIN CIHeader CIH with (nolock) ON CODP.InvoiceNo = CIH.InvoiceNo
					WHERE (ISNULL('$orderNo','')='' OR CODP.CustomerOrderNo = '$orderNo')
					AND (ISNULL('$invoiceNo','')='' OR CODP.InvoiceNo = '$invoiceNo')
					AND CODP.IsPaymentReceived = $IsPaymentReceived
					AND CIH.Boid = $locationId AND CODP.CreatedBy = $loggedInUserId";
			
			
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
		}
		catch (PDOException $e) {
	

			throw new Exception($e);
	
		}
	
		return $results;
	}
	
	
	

	function locationSearchLookUpData($locationCode,$stateCode,$locationName,$locationId,$userid)
	{
		try{
			
			$connectionString = new DBHelper();

			$pdo_object = $connectionString->dbConnection();

			$sql = "select lm.LocationCode,LM.LocationId,lm.Name,SM.StateName,SM.StateCode fROM Location_Master lm with (NOLOCK) inner join  State_Master sm with (NOLOCK) on SM.StateId=LM.StateId
 			WHERE LocationCode LIKE'%$locationCode%' anD StateCode LIKE '%$stateCode%' AND lm.Name like '%$locationName%' AND (LM.LocationType=3 OR LM.LocationType=2 ) AND LM.Status=1 AND  LM.locationid in (select distinct(locationid) from UserRoleLoc_link where UserId=$userid)  ";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			if(sizeof($results) == 0)
			{
				throw new vestigeException("No record found");
			}
			
		}
			catch (Exception $e) {
				
				throw new vestigeException($e);
				
		}
		return $results;
	}
	function searchPONumber($PONumber,$VendorCode,$locationId)
	{
		try{
				
			$connectionString = new DBHelper();
	
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "	select POH.PONumber PONo,POH.PODate PODate,POH.POType,POH.VendorCode,
						PM.KeyValue1 POStatus
						 from PO_Header POH  with (NOLOCK)
						 LEFT JOIn Parameter_Master PM with (NOLOCK) ON PM.ParameterCode='POSTATUS' 
						 AND PM.KeyCode1=POH.Status where ( POH.Status=3 or POH.Status=1) AND POH.PONumber LIKE 
						 '%$PONumber%' AND POH.VendorCode LIKE  '%$VendorCode%' AND POH.D_LocationId=$locationId  Order By PODate DESC
						 		
					";
		
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				
			if(sizeof($results) == 0)
			{
				throw new vestigeException("No record found");
			}
				
		}
		catch (Exception $e) {
	
			throw new vestigeException($e);
	
		}
		return $results;
	}
	
	
	/*---------------Search GRN Number for vendor Return ----------*/
	function searchLookUpGRNNumber($GRNNumber,$VendorCode,$locationId)
	{
		try{
	
			$connectionString = new DBHelper();
	
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "select top 51 GH.GRNNo,GH.GRNDate,
                            PM.KeyValue1 Status,GH.ChallanNo,
                             PH.VendorCode						
							from GRN_Header GH
							INNER JOIN Parameter_Master PM
							ON PM.KeyCode1=GH.Status AND PARAMETERCODE LIKE '%GRNSTATUS%'
							INNER JOIN PO_Header PH
							ON GH.PONumber=PH.PONumber
 							where  GRNNo like '%$GRNNumber%' 
 							AND PH.VendorCode like'%$VendorCode%' AND GH.Status=3 order by GH.GRNDate DESC";
		
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
	
			if(sizeof($results) == 0)
			{
			throw new vestigeException("No record found");
			}
	
			}
			catch (Exception $e) {
	
			throw new vestigeException($e);
	
			}
			return $results;
			}
	
	function distributorSearchLookUpData($distributorLookUpFormData,$distributorDOB)
	{
		$vestigeUtil = new VestigeUtil();
		
		$locationId = $vestigeUtil->getSessionData("loggedInUserLocation");
		
		
		try{
			parse_str($distributorLookUpFormData, $output);
			
			$lookUpDistributorFirstName = $output['DistributorFirstName'];
			$lookUpDistributorLastName = $output['DistributorLastName'];
			$lookUpDistributorMobileNo = $output['DistributorMobileNo'];

			$sql = "";
			if(($lookUpDistributorFirstName != null || $lookUpDistributorFirstName != '')){
				$sql = " SELECT top 51 DistributorId,DistributorFirstName,DistributorStatus Status,CM.CityName City,SM.StateName State
					FROM distributorMaster DM with (NOLOCK) left join City_Master CM with (NOLOCK)
					ON DM.DistributorCityCode = CM.CityId left join State_Master SM with (NOLOCK)
					ON DM.DistributorStateCode = SM.StateId
					WHERE
					DM.DistributorFirstName LIKE '$lookUpDistributorFirstName%' AND 
					(ISNULL('$lookUpDistributorLastName','')='' OR DM.DistributorLastName LIKE '$lookUpDistributorLastName%') 
					AND (ISNULL('$lookUpDistributorMobileNo','')='' OR DM.DistributorMobNumber LIKE '%$lookUpDistributorMobileNo%')" ;
			}
			else if($lookUpDistributorMobileNo != null || $lookUpDistributorMobileNo != ''){
				$sql = " SELECT top 51 DistributorId,DistributorFirstName,DistributorStatus Status,CM.CityName City,SM.StateName State
					FROM distributorMaster DM with (NOLOCK) left join City_Master CM with (NOLOCK)
					ON DM.DistributorCityCode = CM.CityId left join State_Master SM with (NOLOCK)
					ON DM.DistributorStateCode = SM.StateId
					WHERE
					 (ISNULL('$lookUpDistributorMobileNo','')='' OR DM.DistributorMobNumber LIKE '%$lookUpDistributorMobileNo%')" ;
			}
			else{
				throw new VestigeException("Please provide firstname or mobile no.");
			}
			/*else{
			}
		
			$sql = " SELECT top 51 DistributorId,DistributorFirstName,DistributorStatus Status,CM.CityName City,SM.StateName State
			FROM distributorMaster DM with (NOLOCK) left join City_Master CM with (NOLOCK)
			ON DM.DistributorCityCode = CM.CityId left join State_Master SM with (NOLOCK)
			ON DM.DistributorStateCode = SM.StateId
			WHERE
			(ISNULL('$lookUpDistributorFirstName','')='' OR DM.DistributorFirstName LIKE '%$lookUpDistributorName') ";*/
		

			//AND (DistributorStatus = 1 || DistributorStatus = 2)
			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			if(sizeof($results) == 0)
			{
				throw new vestigeException("No record found");
			}
			
		}
			catch (Exception $e) {
				
				throw new vestigeException($e);
				
		}
		
		return $results;
		
	}
	
	
	function distributorRTGSLookUpData($distributorRTGSCode)
	{
		$vestigeUtil = new VestigeUtil();
	
		$locationId = $vestigeUtil->getSessionData("loggedInUserLocation");
	
	
		try{
			$connectionString = new DBHelper();
	
			$pdo_object = $connectionString->dbConnection();
	
			$sql = "select top 51 BM.BankBranckCode,BM.BranchName,BM.BankName from BankBranch_Master BM with (NOLOCK)
					 where
					(isnull('$distributorRTGSCode','') = ''	or BankBranckCode like '$distributorRTGSCode%')";
	
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				
			if(sizeof($results) == 0)
			{
				throw new vestigeException("No record found");
			}
				
		}
		catch (Exception $e) {
	
			throw new vestigeException($e);
	
		}
	
		return $results;
	
	}

	function TOISearchLookUpData($TOILookUpFormData,$locationId)
	{
	try{
		$transferOutInstruction = new TransferOutInstruction();
		
		$TOISearchResult = $transferOutInstruction->searchTOI($TOILookUpFormData,-1,-1,-1,$locationId);
		
			
		}
			catch (Exception $e) {
				
				throw new vestigeException($e);
				
		}
		
		return $TOISearchResult;
		
		
	}

	function TOSearchLookUpData($TOLookUpFormData,$locationId)
	{
	try{
		
		$transferOut = new TransferOut();
	
		$TOSearchResult = $transferOut->searchTO($TOLookUpFormData, -1, -1, -1,$locationId);
	}
	catch (Exception $e) {
	
		throw new vestigeException($e);
	
	}
	
		return $TOSearchResult;
	
	}
	
	function TOSearchLookUpData2($TOLookUpFormData,$locationId)
	{
		try{
	
			$transferOut = new TransferOut();
	
			$TOSearchResult = $transferOut->searchTO2($TOLookUpFormData, -1, -1, -1,$locationId);
		}
		catch (Exception $e) {
	
			throw new vestigeException($e);
	
		}
	
		return $TOSearchResult;
	
	}
	
	/*function for composite item look up*/
	
	function CompositeItemSearchLookUpData($CompositeItemLookUpFormData,$locationId)
	{
	
		parse_str($CompositeItemLookUpFormData, $output);
		
		$compositeItemCode=$output['CompositeItemCode'];
		
		$compositeItemName = $output['CompositeItemName'];
		
		$connectionString = new DBHelper();
		
		$sql = "select Distinct(IM.ItemCode),IM.ItemName,IM.ShortName,IM.DisplayName,MM.Name from  Inventory_LocBucketBatch ILB with (NOLOCK)
                LEFT JOIN item_master  IM with (NOLOCK)
		         ON IM.ItemId=ILB.ItemId
		         left join MerchHierarchy_Master MM with (NOLOCK)
				on IM.MerchHierarchyDetailId = MM.MerchHierarchyId 
				 where ILB.LocationId='$locationId' AND ILB.BucketId=5 AND
				IM.IsComposite = 1 and IM.ItemCode like '%$compositeItemCode%' and IM.ItemName like '%$compositeItemName%'";
			
		
		$pdo_object = $connectionString->dbConnection();
		
		$stmt = $pdo_object->prepare($sql);
		
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		
		return $results;
	
		return json_encode($TOSearchResult);
	
	}
	function TIStatus()
	{
		$connectionString = new DBHelper();

		$pdo_object = $connectionString->dbConnection();

		$stmt = $pdo_object->prepare("Select
					-1 'keycode1',
					'Select' 'keyvalue1',
					-1 'keycode2',
					'Select' 'keyvalue2',
					-1 'keycode3',
					'Select' 'keyvalue3',
					1 'isactive',
					-1 'sortorder',
					'' 'ParameterCode',
					'' 'description'
			Union All
			Select
					keycode1,
					keyvalue1,
					ISNULL(keycode2, 0) 'keycode2',
					ISNULL(keyvalue2, '') 'keyvalue2',
					ISNULL(keycode3, 0) 'keycode3',
					ISNULL(keyvalue3, '') 'keyvalue3',
					isactive,
					sortorder,
					ParameterCode,
					ISNULL([description], '') 'description'
			From	Parameter_Master with (NOLOCK)
			Where
				parametercode='TIStatus'
				And isactive=	1
			Order By
				sortorder Asc");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}
		function searchTIItems($TONumber,$sourceId)
			{


			//	$LocationId=10;
			$outParam='';

			$connectionString = new DBHelper();
			$pdo_object = $connectionString->dbConnection();
			try{
				
			$sql = "{CALL usp_TItemSearch (@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
  		$stmt = $pdo_object->prepare($sql);
				
			$stmt->bindParam(':TNumber',$TONumber, PDO::PARAM_STR);
			$stmt->bindParam(':SourceAddressId',$sourceId,PDO::PARAM_INT);
  	
  		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
  			
  		$stmt->execute();
  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
  			
  		return $results;
			}
			catch(PDOException $e){
			//print_r($e->getMessage());
}
}
    function searchTO($TONumber)
				{
					$connectionString = new DBHelper();
					$pdo_object = $connectionString->dbConnection();
					try{
						
					$sql = "Select distinct head.TONumber,Convert(Varchar(20), head.CreationDate, 105) AS 'TOCreationDate', head.TOINumber,th.TOIDate AS 'TOIDate', head.SourceLocationId, head.DestinationLocationId, Convert(Varchar(20),head.CreationDate, 105) CreationDate, head.Status, head.TotalTOQuantity, head.TotalTOAmount, head.TotalTOTaxAmount, head.ModifiedDate,
		lm.Address1 + Char(13) + char(10) + IsNull(lm.Address2,'') + Char(13) + char(10) + IsNull(lm.Address3,'') + Char(13) + char(10) +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As SourceAddress, 
	    lm_1.Address1 + Char(13) + char(10) + IsNull(lm_1.Address2,'') + Char(13) + char(10) + IsNull(lm_1.Address3,'') + Char(13) + char(10) + cm_1.CityName +  ' ' + sm_1.StateName + ' '  + com_1.CountryName As DestinationAddress, 
		head.PackSize, Remarks, RefNumber,head.ShippingWayBillNo,
		prm.KeyValue1 StatusName, head.ShippingDetails, 
		Convert(Varchar(20), head.ShippingDate, 105)	ShipDate, Convert(Varchar(20), head.ExpectedDeliveryDate, 105)	ExpectedDeliveryDate
		--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End As Indentise
		, IsNull(head.GrossWeight,0) GrossWeight,
		sm.StateId, head.CreationDate,head.ModifiedBY, UM.FirstName +' '+ UM.MiddleName +' '+ UM.LastName As ModifiedByName, 
		lm.Phone1 + ', '+ lm.Phone2 As SourcePhone, 
		lm.EmailId1,
		cm.CityName As SourceCity, 
		cm_1.CityName As DestinationCity,
		ISNULL(head.Isexported,'') AS Isexported,lm.IECCode,
		head.ExporterRef, head.OtherRef,head.BuyerOtherthanConsignee,head.PreCarriage,head.PlaceofReceiptbyPreCarrier,com.CountryName AS CountryOfOrigin,
		com_1.CountryName as CountryOfDestination,head.VesselflightNo,head.PortofLoading,head.PortofDischarge,head.PortofDestination,head.TermsofDelivery,
		head.DELIVERY,head.PAYMENT,head.BuyerOrderNo,head.BuyerOrderDate
		--Case When Isnull((head.Isexported,'')='')then '' Else  head.Isexported End AS Isexported
		From TO_Header	head with (NOLOCK)
		Inner Join TO_Detail det with (NOLOCK)
		On head.TONumber = det.TONumber
		Inner Join Location_Master lm  with (NOLOCK)
		On lm.LocationId = head.SourceLocationId
		Inner Join Location_Master lm_1 with (NOLOCK)
		On lm_1.LocationId = head.DestinationLocationId

		LEFT Join TOI_Header th with (NOLOCK)
		On th.TOINumber = head.TOINumber

		Inner Join Parameter_Master  prm  with (NOLOCK)
		On head.Status = prm.KeyCode1
		And prm.ParameterCode = 'TOStatus'
		
		Left join City_Master cm  with (NOLOCK)
		On cm.CityId = lm.CityId
		
		Left join state_master sm  with (NOLOCK)
		On sm.StateId = lm.StateId

		Left join Country_Master com with (NOLOCK)
		On com.CountryId = lm.CountryId

		Left join City_Master cm_1 with (NOLOCK)
		On cm_1.CityId = lm_1.CityId

		Left join state_master sm_1 with (NOLOCK)
		On sm_1.StateId = lm_1.StateId

		Left join Country_Master com_1 with (NOLOCK)
		On com_1.CountryId = lm_1.CountryId

		Left Outer Join User_Master UM with (NOLOCK)
		On UM.UserId = Head.ModifiedBy

		Where	(IsNull('-1','-1')='-1' Or head.DestinationLocationId = '-1')
		And		(IsNull('-1','-1')='-1' Or head.SourceLocationId = '-1')
		And		(IsNull(NullIf('',''),'-1')='-1' Or head.RefNumber Like  '%' + '')
		And		(IsNull(NullIf('',''),'-1')='-1' Or head.TOINumber Like  '%' + '')
		And		(IsNull(NullIf('$TONumber',''),'-1')='-1' Or head.TONumber Like '%' + '$TONumber')
		AND		(IsNull('','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'2099-01-01'),112) >= Convert(varchar(10),CAST('' As DateTime),112))
		AND		(IsNull('2099-12-31T00:00:00','')='' OR Convert(varchar(10),IsNull(head.CreationDate,'1900-01-01'),112) <= Convert(varchar(10),Cast('2099-12-31T00:00:00' As DateTime),112))
		AND		(IsNull('','')='' OR Convert(varchar(10),head.ShippingDate,112) >= Convert(varchar(10),Cast('' As DateTime),112))
		AND		(IsNull('','')='' OR Convert(varchar(10),head.ShippingDate,112) <= Convert(varchar(10),Cast('' As DateTime),112))
		AND		(IsNull('-1','-1')='-1' OR head.Status = '-1')
		AND		(IsNull('0','2')='2' Or IsNull('0','-1')='-1' Or '0' = Case When th.Indentised = 1 Then 1 Else 0 End)
		

		ORDER BY head.TONumber DESC
			
					";
						
					$stmt = $pdo_object->prepare($sql);

					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					return $results;
					}
					catch(PDOException $e){
						//print_r($e->getMessage());
					}
			}
	function ShowControlTaxInfor($locationId)
				{
				$connectionString = new DBHelper();
				$pdo_object = $connectionString->dbConnection();
					try{
						
					$sql = "select phone1,phone2,mobile1,mobile2,tinNo,cstNO,VatNo from location_master with (NOLOCK)
					where  locationid ='$locationId'";
						
					$stmt = $pdo_object->prepare($sql);

					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

					return $results;
}
catch(PDOException $e){
						//print_r($e->getMessage());
}
}
  	function saveTI($jsonForItems,$TONumber,$statusId,$LocationId,$DestinationLocationId,$tnumber){

	$outParam='';
	
	$connectionString = new DBHelper();
	$pdo_object = $connectionString->dbConnection();
	try{

	$outParam='';

	$sql = "{CALL sp_TISave (@jsonForItems=:jsonForItems,@TONumber=:TONumber,@StatusId=:StatusId,
  		@LocationId=:LocationId,@DestinationLocationId=:DestinationLocationId,@tnumber=:tnumber,@outParam=:outParam)}";
  		$stmt = $pdo_object->prepare($sql);
  		$stmt->bindParam(':jsonForItems',$jsonForItems, PDO::PARAM_STR);
		$stmt->bindParam(':TONumber',$TONumber, PDO::PARAM_STR);
		$stmt->bindParam(':StatusId',$statusId, PDO::PARAM_INT);
		$stmt->bindParam(':LocationId',$LocationId, PDO::PARAM_INT);
		$stmt->bindParam(':DestinationLocationId',$DestinationLocationId, PDO::PARAM_INT);
		$stmt->bindParam(':tnumber',$tnumber, PDO::PARAM_STR);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				
			$stmt->execute();
  		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
  			
  			
  		return $results;
	}
	catch(PDOException $e){
	//print_r($e->getMessage());
	}

	}
	function adjustItemAndBatch($SourceAddressId,$TOINumber,$TONumber){
		$outParam='';
		
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
	
	
			$sql = "{CALL usp_TIItemSearch (@TONumber=:TONumber,@TNumber=:TNumber,@SourceAddressId=:SourceAddressId,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':TONumber',$TONumber, PDO::PARAM_STR);
			$stmt->bindParam(':TNumber',$TOINumber, PDO::PARAM_STR);
			$stmt->bindParam(':SourceAddressId',$SourceAddressId,PDO::PARAM_INT);
				
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
				
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			 
			 
			return $results;
		}
		catch(PDOException $e){
			//print_r($e->getMessage());
		}
		 
	}
	
	function blockItemDetailLookup($itemName,$itemCode){
			
		try{
				
			$connectionString = new DBHelper();
				
			$pdo_object = $connectionString->dbConnection();
				
			$sql = "select top 51 ItemCode,ItemName,
			CASE WHEN Status=1 THEN 'ACTIVE'
			ELSE 'NOT ACTIVE'  END Status from item_master
			where ItemCode like '%$itemCode%' and ItemName like '%$itemName%' AND STATUS=1";
				
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
				
			if(sizeof($results) == 0)
			{
				throw new vestigeException("No record found");
			}
				
		}
		catch (Exception $e) {
				
			throw new vestigeException($e);
				
		}
		return $results;
	}
	
	
	function searchTI($TIFormData){
		parse_str($TIFormData, $output);
		
		//$locationId = $output['location_id']; //check name in html form
		
		
			
			
		$sourceLocation=$output['TISourceLocation'];
		$destinationLocation = $output['TIDestinationLocation'];
		
		$FromTODate = $output['fromTIShipDate'];
		
		$ToTODate = $output['toTOShipDate'];
		
		$FromReceiveDate = $output['RecFromDate'];
		$TOReceiveDate  = $output['recToDate'];
		$TONumber  = $output['TONumber'];
		$TIStatus = $output['TIStatus'];
		$TINumber  = $output['TINumber'];
		
		$indentised= $output['indentised'];
		$indentised=2;
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
				
			$sql = "Select distinct head.TOINumber, head.TONumber, head.TINumber,
		 head.SourceLocationId, head.DestinationLocationId, Convert(Varchar(20),
		 TOShippingDate, 105) TOShippingDate, head.Status, TotalTOQuantity, TotalTOAmount,
		  TotalTIQuantity, TotalTIAmount, head.ModifiedDate,
		IsNull(lm.Address1,'') + ' ' + IsNull(lm.Address2,'') + ' ' + IsNull(lm.Address3,'') + ' ' +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As SourceAddress, 
		IsNull(lm_1.Address1,'') + ' ' + IsNull(lm_1.Address2,'') + ' ' + IsNull(lm_1.Address3,'') + ' '+ cm_1.CityName +  ' ' + sm_1.StateName + ' '  + com_1.CountryName As DestinationAddress, 
		head.PackSize, Remarks ,VehicleNo As ShippingWayBillNo,IsNull(head.GrossWeight,0) As GrossWeight, 
		prm.KeyValue1 StatusName, head.ShippingDetails,  Convert(Varchar(20), ReceivedTime, 105)	ReceivedTime,
		Convert(Varchar(20), ReceivedDate, 105)	ReceivedDate--, Case When IsNull(det.IndentNo,'')<>'' Then 1 Else 0 End As Indentise
		,ISNULL(head.Isexported,0) Isexported
		From TIHeader	head(nolock)
		Inner Join TIDetail det(nolock)
		On head.TINumber = det.TINumber
		Inner Join Location_Master lm(nolock)
		On lm.LocationId = head.SourceLocationId
		Inner Join Location_Master lm_1(nolock)
		On lm_1.LocationId = head.DestinationLocationId

--		LEFT Join TOI_Header th(nolock)
--		On th.TOINumber = head.TOINumber
--
		
		Inner Join Parameter_Master  prm(nolock) 
		On head.Status = prm.KeyCode1
		And prm.ParameterCode = 'TIStatus'
		
		Left join City_Master cm(nolock) 
		On cm.CityId = lm.CityId

		Left join state_master sm(nolock) 
		On sm.StateId = lm.StateId

		Left join Country_Master com(nolock) 
		On com.CountryId = lm.CountryId

		Left join City_Master cm_1(nolock) 
		On cm_1.CityId = lm_1.CityId

		Left join state_master sm_1(nolock)
		On sm_1.StateId = lm_1.StateId

		Left join Country_Master com_1(nolock)
		On com_1.CountryId = lm_1.CountryId


		Where	(IsNull('$destinationLocation','-1')='-1' Or head.DestinationLocationId = '$destinationLocation')
		And		(IsNull('$sourceLocation','-1')='-1' Or head.SourceLocationId = '$sourceLocation')
		And		(IsNull(NullIf('$TINumber',''),'-1')='-1' Or head.TINumber Like '%' + '$TINumber')
		And		(IsNull(NullIf('$TONumber',''),'-1')='-1' Or head.TONumber Like '%' + '$TONumber')
		AND		(IsNull('$FromTODate','')='' OR Convert(varchar(10),IsNull(TOShippingDate,'2099-01-01'),101) >= Convert(varchar(10),CAST('$FromTODate' As DateTime),101))
		AND		(IsNull('$ToTODate','')='' OR Convert(varchar(10),IsNull(TOShippingDate,'1900-01-01'),101) <= Convert(varchar(10),Cast('$ToTODate' As DateTime),101))
		AND		(IsNull('$FromReceiveDate','')='' OR (Isnull(ReceivedDate,'') ='' AND Convert(Varchar(10),'$FromReceiveDate',101)='1900-01-01' AND Convert(Varchar(10),'$TOReceiveDate',101)='2099-12-31') OR Convert(varchar(10),ReceivedDate,101) >= Convert(varchar(10),Cast('$FromReceiveDate' As DateTime),101))
		AND		(IsNull('$TOReceiveDate','')=''  OR (Isnull(ReceivedDate,'') ='' AND Convert(Varchar(10),'$FromReceiveDate',101)='1900-01-01' AND Convert(Varchar(10),'$TOReceiveDate',101)='2099-12-31') OR Convert(varchar(10),ReceivedDate,101) <= Convert(varchar(10),Cast('$TOReceiveDate' As DateTime),101))
		AND		(IsNull('$TIStatus','-1')='-1' Or head.Status = '$TIStatus')
--		AND		(IsNull('$indentised','2')='2' Or IsNull('$indentised','-1')='-1' Or '$indentised' = Case When th.Indentised = 1 Then 1 Else 0 End)
		
		ORDER BY head.TINumber ASC	";
		
			$stmt = $pdo_object->prepare($sql);
		
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			return $results;
		}
		catch(PDOException $e){
		//print_r($e->getMessage());
		}
		
	}
	}
?>



