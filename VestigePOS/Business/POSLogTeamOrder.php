<?php



Class LogTeamOrder
{
	/*------------------------------------------------------------------------------------------------------------*/
	/**
	 * Function will return distributor information from pickup center id.
	 * @param unknown $selectedPickUpCenter
	 * @return multitype:
	 */
	var $vestigeUtil;
	function __construct()
	{
		$this->vestigeUtil = new VestigeUtil();
	}
	
	function getCourierDetail($logNo){
		$connectionString = new DBHelper();
		try{
			$pdo_object = $connectionString->dbConnection();
		
			$stmt = $pdo_object->prepare("select TOP 1 CD.CourierId,CD.CourierCompanyName,CD.CourierRefNo,CD.DoketNo,CD.NoOfBoxes,Cd.Weigth from COHeader COH with (NOLOCK)  INNER JOIN CourierDetail 
			CD with (NOLOCK)  ON CD.CourierId=COH.Courierid where  COH.LogNo='$logNo'");
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
		
			return $outputData;
		}
		catch(Exception $e)
		{
			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
		
			return $exception;
		}
	}
	
	function distributorFromPickUpCenter($selectedPickUpCenter)
	{
		$connectionString = new DBHelper();
	try{
		$pdo_object = $connectionString->dbConnection();
		
		$stmt = $pdo_object->prepare("SELECT lm.LocationId,CASE LocationType WHEN 2 THEN 'WH' WHEN 3 THEN 'BO' END LocationType
				,[LocationCode],[Name] AS LocationName,LocationType AS LocationTypeId,
				[Address1],[Address2],[Address3],[Address4]
				,LM.[CityId],LM.[Pincode],LM.[StateId],LM.[CountryId],[Phone1],[Phone2]
				,CM.CityName,SM.StateName,CNM.CountryName
				,[Mobile1],[Mobile2],[Fax1],[Fax2],[EmailId1],[EmailId2]
				,[WebSite],dm.DistributorId,lm.ModifiedDate,[Name] + ' - ' + LocationCode AS [DisplayName]
				,lm.TinNo,isnull (dm.DistributorFirstName,'') + ' ' + isnull (dm.DistributorLastName,'') AS DistributorName,LM.[Pincode]
					
				FROM [Location_Master]LM with (NOLOCK)
				JOIN City_Master CM with (NOLOCK) ON LM.[CityId]=CM.[CityId]
				JOIN State_Master SM with (NOLOCK) ON LM.[StateId]=SM.[StateId]
				JOIN Country_Master [CNM] with (NOLOCK) ON LM.[CountryId]=CNM.[CountryId]
				LEFT JOIN DistributorMaster dm with (NOLOCK) ON lm.DistributorId = dm.DistributorId
				WHERE	LM.[LocationId]=$selectedPickUpCenter");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
	
	/*------------------------------------------------------------------------------------------------------------*/
	function distributorFromDistributorId($distributorId)
	{
		try{
		$distributorInfo = new POSBusinessClass();
		$results = $distributorInfo->distributorInformation($distributorId,'E','','10');
				
			return $results;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
	
	/*------------------------------------------------------------------------------------------------------------*/
	function loadCountries()
	{
	try {
		$countries = new POSBusinessClass(); //later have to make object of distributor registration coz have to seprate that business class.
		$results = $countries->countries();
				
			return $results;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}

	/*------------------------------------------------------------------------------------------------------------*/
	function loadStates($countryId)
	{
	try{
		$states = new POSBusinessClass(); //later have to make object of distributor registration coz have to seprate that business class.
		$results = $states->states($countryId);
				
			return $results;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
	/*------------------------------------------------------------------------------------------------------------*/
	function loadCities($stateId)
	{
		try{
		$cities = new POSBusinessClass(); //later have to make object of distributor registration coz have to seprate that business class.
		$results = $cities->cities($stateId);
					
			return $results;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
	
	function saveLogTeamOrderFormData($LocationId,$logTeamOrderFormData,$zeroLog,$isChangeAddress,$createdBy,$BoId)
	{
		parse_str($logTeamOrderFormData, $output);
		$BoId=$LocationId ;
		$LTOtype='';
		$LTOtype = $output['LTOtype'];
		$logStatus='';
		$logStatus = $output['logStatus'];
		$LTODistributorId='';
		$LTODistributorId = $output['LTODistributorId'];
		$pcId = $output['LTOPickUpCenterId'];
		$LTODistributorName='';
		$LTODistributorName =$output['LTODistributorName'];
		$addressLine1='';
		$addressLine1 = $output['addressLine1'];
		$addressLine2='';
		$addressLine2 = $output['addressLine2'];
		$addressLine3='';
		$addressLine3 = $output['addressLine3'];
		$addressLine4='';
		$addressLine4 = $output['addressLine4'];
		$Country='';
		$Country = $output['Country'];
		$State='';
		$State = $output['State'];
		$City='';
		$City = $output['City'];
		$LTOPinCode='';
		$LTOPinCode = $output['LTOPinCode'];
		$LTOPhone1='';
		$LTOPhone1 = $output['LTOPhone1'];
		
		$LTOPhone2='';
		$LTOPhone2 = $output['LTOPhone2'];
		$LTOMobile1='';
		$LTOMobile1 = $output['LTOMobile1'];
		$LTOMobile2='';
		$LTOMobile2 = $output['LTOMobile2'];
		$LTOEmail1='';
		$LTOEmail1 = $output['LTOEmail1'];
		$LTOEmail2='';
		$LTOEmail2 = $output['LTOEmail2'];
		$LTOFax1='';
		$LTOFax1 = $output['LTOFax1'];
		$LTOFax2='';
		$LTOFax2 = $output['LTOFax2'];
		$LTOWebsite='';
		$LTOWebsite = $output['LTOWebsite'];
		$ABRNo=$output['AWBNo'];
		$logNo = $output['LTONumber'];
		
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
		 
			$sql = "Exec sp_SaveOrdrLog
							'$LTOtype','$LTODistributorId','$BoId','$pcId','$addressLine1','$addressLine2','$addressLine3','$addressLine4',
							'$Country','$State','$City','$LTOPinCode','$LTOPhone1','$LTOPhone2','$LTOMobile1','$LTOMobile2',
							'$LTOEmail1','$LTOEmail2','$LTOFax1','$LTOFax2','$LTOWebsite',1,'$createdBy','','$createdBy','','$zeroLog','$isChangeAddress','$logNo','$ABRNo'				
						";
			
			 $stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	 
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
	
	function generateCourierLog($logTeamOrderFormData,$distributorId,$pcid,$loggedInUser,$orderMode,$loggedInUserType,$locationId){
		parse_str($logTeamOrderFormData, $output);
		
		
		
		//$locationId = $output['location_id']; //check name in html form
		
		$logNo = $output['LTONumber'];
		$status = $output['logStatus'];
		$pcId = $output['LTOPickUpCenterId'];
		$logType = $output['LTOtype'];
		$fromDate = $output['fromDate'];
		//$distributorId  = $output['LTODistributorId'];
		$fromDate  = $output['fromDate'];
		$address1 = $output['addressLine1'];
		$address2 = $output['addressLine2'];
		$address3 = $output['addressLine3'];
		$stateId = $output['State'];
		$countryId = $output['Country'];
		$cityId = $output['City'];
		$emilId = $output['LTOEmail1'];
		//$orderMode = $outputData['OrderMode'];
		$pincode = $output['LTOPinCode'];
		$phoneNo1 = $output['LTOPhone1'];
		$phoneNo2 = $output['Phone2'];
		$mobile1 = $output['LTOMobile1'];
		
		if($loggedInUserType == 'D'){
				$pcid = $locationId;
				$logType = 2;
			}
			else if($loggedInUserType == 'P'){
				$distributorId = 0;
				$logType = 1;
			}
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		
		try{ 
			$outParam = '';
			
			$file = fopen("D://dddddd.txt", "w");
			fwrite($file, $distributorId .','.$pcid.','.$address1.','.$address2.','.$address3.','.$countryId.','.$stateId.','.
					$cityId.','.$pincode.','.$phoneNo1.','.$phoneNo2.','.$mobile1.','.$email1.','.$loggedInUser.','.$orderMode.','.$logType);
					fclose($file);

			$sql = "{CALL sp_generateCourierLog(@distributorId=:distributorId,@pcid=:pcid,@address1=:address1,@address2=:address2,@address3=:address3,@coutryCode=:countryId
					,@stateCode=:stateId,@cityCode=:cityId,@pincode=:pincode,@phone1=:phone1,@phone2=:phone2,@mobile1=:mobile1,@email1=:email1,@createdBy=:createdBy,
					@orderMode=:orderMode,@logType=:logType,@outParam=:outParam)}";
			
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':distributorId',$distributorId, PDO::PARAM_INT);
			$stmt->bindParam(':pcid',$pcid, PDO::PARAM_INT);
			$stmt->bindParam(':address1',$address1, PDO::PARAM_STR);
			$stmt->bindParam(':address2',$address2, PDO::PARAM_STR);
			$stmt->bindParam(':address3',$address3, PDO::PARAM_STR);
			$stmt->bindParam(':countryId',$countryId, PDO::PARAM_INT); 
			$stmt->bindParam(':stateId',$stateId, PDO::PARAM_INT);
			$stmt->bindParam(':cityId',$cityId, PDO::PARAM_INT);
			$stmt->bindParam(':pincode',$pincode,PDO::PARAM_INT);
			$stmt->bindParam(':phone1',$phoneNo1, PDO::PARAM_STR);
			$stmt->bindParam(':phone2',$phoneNo2, PDO::PARAM_STR);
			$stmt->bindParam(':mobile1',$mobile1, PDO::PARAM_STR);
			$stmt->bindParam(':email1',$email1, PDO::PARAM_STR);
			$stmt->bindParam(':createdBy',$loggedInUser, PDO::PARAM_INT);
			$stmt->bindParam(':orderMode',$orderMode, PDO::PARAM_INT);
			$stmt->bindParam(':logType',$logType, PDO::PARAM_INT);
			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
			
			$stmt->execute();
			$courierLogResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}
		return $courierLogResult;
	}
	
	function searchLogTeamOrder($logTeamOrderFormData,$locationId)
	{
		parse_str($logTeamOrderFormData, $output);
		
		//$locationId = $output['location_id']; //check name in html form
		
		$logNo = $output['LTONumber'];
		$status = $output['logStatus'];
		$pcId = $output['LTOPickUpCenterId'];
		$logType = $output['LTOtype'];
		$fromDate = $output['fromDate'];
		$distributorId  = $output['LTODistributorId'];
		$fromDate  = $output['fromDate'];
		if($distributorId==''){
			
			$distributorId=0;
		}
		if($pcId==0){
			
			$pcId=-1;
		}
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try{
				
			$sql = "SELECT CASE WHEN [LogType]=1 AND OL.BOId <> OL.PCId THEN ISNULL(Lm.Name,'') + ' ' + ISNULL([LogNo],'') ELSE ISNULL([LogNo],'') END [LogNo], ISNULL([LogNo],'') LogValue
			,ISNULL([LogType],0)[LogType]
			,ISNULL(OL.[DistributorId],0)[DistributorId]
			,LTRIM(RTRIM(ISNULL(DistributorFirstName,'')))+' '+LTRIM(RTRIM(ISNULL(DistributorLastName,''))) As DistributorName
			,ISNULL(OL.[PCId],0)[PCId]
			,ISNULL(LM.[Name],'')[PCName]
			,ISNULL(OL.[BOId],0)[BOId]
			,ISNULL(OL.[Address1],'')[Address1]
			,ISNULL(OL.[Address2],'')[Address2]
			,ISNULL(OL.[Address3],'')[Address3]
			,ISNULL(OL.[Address4],'')[Address4]
			,ISNULL(OL.[CountryCode],0)[CountryCode]
			,ISNULL(OL.[StateCode],0)[StateCode]
			,ISNULL(OL.[CityCode],0)[CityCode]
			,ISNULL(OL.[PinCode],'')[PinCode]
			,ISNULL(OL.[Phone1],'')[Phone1]
			,ISNULL(OL.[Phone2],'')[Phone2]
			,ISNULL(OL.[Mobile1],'')[Mobile1]
			,ISNULL(OL.[Mobile2],'')[Mobile2]
			,ISNULL(OL.[EmailId1],'')[EmailId1]
			,ISNULL(OL.[EmailId2],'')[EmailId2]
			,ISNULL(OL.[Fax1],'')[Fax1]
			,ISNULL(OL.[Fax2],'')[Fax2]
			,ISNULL(OL.[WebSite],'')[WebSite]
			,ISNULL(OL.[Status],0)[Status]	  
			,OL.[CreatedBy]
			,convert(nvarchar,OL.[CreatedDate],105) CreatedDate
			,OL.[ModifiedBy]
			,OL.[ModifiedDate],
			OL.isChangeAddress,
			OL.isZeroLog
			,Round(ISNULL((SELECT sum(co.OrderAmount + co.TaxAmount) FROM COHeader co with (NOLOCK) WHERE co.LogNo = OL.LogNo AND (ISNULL('$pcId',-1)=-1 OR '$pcId'=0 OR CO.[PCId]='$pcId') AND CO.BOId='$locationId'  AND co.Status IN(3,4)),0),2) AS LogOrderTotal
			,Round(ISNULL((SELECT sum(ci.InvoiceAmount + ci.TaxAmount) FROM CIHeader ci with (NOLOCK) WHERE ci.LogNo = OL.LogNo AND (ISNULL('$pcId',-1)=-1 OR '$pcId'=0 OR CI.[PCId]='$pcId') AND CI.BOId='$locationId' AND ci.Status <> 3),0),2) AS LogInvoiceTotal
			,Round(ISNULL((SELECT ISNULL(sum(CI.TotalWeight),0) FROM CIHeader ci with (NOLOCK) WHERE ci.LogNo = OL.LogNo AND ci.Status <> 3 AND (ISNULL('$pcId',-1)=-1 OR '$pcId'=0 OR CI.[PCId]='$pcId') AND CI.BOId='$locationId'),0),2) AS LogInvoiceWeight
			,(select (SUM(isnull(co.PaymentAmount,0)) - SUM( case when co.PaidOrder = 1 then isnull(co.PaymentAmount,0) else isnull(opay.TransactionAmount,0) + isnull(cop.PaymentAmount,0) end)) from COHeader co
			left join COPayment cop on co.CustomerOrderNo = cop.CustomerOrderNo and cop.TenderType = 5
			left join OnlinePayGOrderTrack opay on co.CustomerOrderNo = opay.TrackId and (opay.TransactionId is not null and  opay.TransactionId != 0)
			where co.LogNo = ol.logno) AS UnpaidAmount
			
		FROM [OrderLog] OL with (NOLOCK)
		LEFT JOIN DistributorMaster DM with (NOLOCK) ON OL.DistributorId=DM.DistributorId
		LEFT JOIN Location_Master LM with (NOLOCK) ON OL.PCId=LM.LocationId
	WHERE (ISNULL('$status',-1)=-1 OR OL.Status='$status')
	AND (ISNULL('$logNo','')='' OR (LM.Name  + OL.[LogNo] LIKE '%' + '$logNo' + '%' AND LogType = 1) OR ( OL.[LogNo] LIKE '%' + '$logNo' + '%' AND LogType = 2)  )
	AND (ISNULL('$logType',-1)=-1 OR '$logType'=0 OR OL.LogType='$logType')
	AND (ISNULL('$distributorId' ,-1)=-1 OR '$distributorId' =0 OR OL.[DistributorId]='$distributorId' )
	AND (ISNULL('$pcId',-1)=-1 OR '$pcId'=0 OR OL.[PCId]='$pcId')
	AND (ISNULL('$locationId',-1)=-1 OR '$locationId'=0 OR OL.[BOId]='$locationId')
	AND (IsNull('$fromDate','')='' OR Convert(varchar(10),OL.CreatedDate,20)>=Convert(varchar(10),'$fromDate',20) )
	ORDER BY OL.CreatedDate ASC ";
			
			$file = fopen("D://logQuery.txt","w");
			fwrite($file, $sql);
			fclose($file) ;
			
			
			
			$stmt = $pdo_object->prepare($sql);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
			$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
	catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
	}
     function searchPaymentsForLogNo($logNo){
     	
     	$connectionString = new DBHelper();
     	$pdo_object = $connectionString->dbConnection();
     	try{
     	
     		$sql = "SELECT CAST(ISNULL([Bank],0) AS NUMERIC(10,2)) AS BANK,CAST(ISNULL([Bonus Check],0) AS NUMERIC(10,2))
     				AS BONUSCHECK,CAST(ISNULL([Cash],0) AS NUMERIC(10,2)) AS CASH,CAST(ISNULL([Cheque],0) AS NUMERIC(10,2)) 
     				AS CHEQUE,CAST(ISNULL([COD],0) AS NUMERIC(10,2)) AS COD,CAST(ISNULL([Credit Card],0) AS NUMERIC(10,2)) AS 
     				CREDITCARD,CAST(ISNULL([Forex],0) AS NUMERIC(10,2)) AS FOREX,CAST(ISNULL([On Account],0) AS NUMERIC(10,2)) 
     				AS ONACCOUNT,CAST(ISNULL(ChangeAmount,0) AS Numeric(10,2)) AS CHANGEAMOUNT 
					from
					(
					SELECT pm.KeyValue1 AS PaymentMode,SUM(cop.PaymentAmount) AS Amount  FROM COHeader  coh with (NOLOCK)
					INNER JOIN COPayment cop with (NOLOCK)
					ON coh.CustomerOrderNo = cop.CustomerOrderNo
					INNER JOIN Parameter_Master pm with (NOLOCK)
					ON PM.KeyCode1 = cop.TenderType
					AND PM.ParameterCode = 'PAYMENTMODE'
					AND coh.LogNo = '$logNo' AND coh.Status in (3, 4) 
					GROUP BY cop.TenderType, pm.KeyValue1

					UNION   

					SELECT 'ChangeAmount' PaymentMode,SUM( coh.ChangeAmount) AS ChangeAmount 
					FROM COHeader coh with (NOLOCK)
					where coh.LogNo = '$logNo' AND coh.Status in (3, 4) 

					) p
					PIVOT
					(
					Sum(Amount) FOR PaymentMode IN ( [Bank],[Bonus Check],[Cash],[Cheque],[COD],[Credit Card],[Forex],[On Account],CHANGEAMOUNT )
					) AS PVT
					
     		";
     		$stmt = $pdo_object->prepare($sql);
     	   
     		$stmt->execute();
     		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
     		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
     		
     		return $outputData;
     		}
     		catch(Exception $e)
     		{
     			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
     		
     			return $exception;
     		}
     }
     function invoice($logNo,$logType)
     {
     	$cities = new POSBusinessClass(); //later have to make object of distributor registration coz have to seprate that business class.
     	$results = $cities->cities($stateId);
     	return $results;
     }
     function searchForLogOrders($logNo){
     
     	$connectionString = new DBHelper();
     	$pdo_object = $connectionString->dbConnection();
     	try{
     
     		$sql = " SELECT COH.CustomerOrderNo,DM.DistributorFirstName DistributorId,isnull(convert(varchar(20),opay.TransactionRefNo),'')
		   OrderRefNo, ISNULL(CIH.InvoiceNo,'') InvoiceNo,COH.PaymentAmount OrderAmount,COh.CreatedDate OrderDate FROM 
		   COHeader COH with (NOLOCK) Left JOIN CIHeader CIH with (NOLOCK) ON COH.CustomerOrderNo=CIH.CustomerOrderNo
		   INNER JOIN Distributormaster Dm with (NOLOCK) on Dm.DistributorId=COH.DistributorId
		   left join OnlinePayGOrderTrack opay on coh.CustomerOrderNo = opay.TrackId
		   where (COH.status=3 or COH.Status=4) and COH.LogNo='$logNo'";
		   
     		$stmt = $pdo_object->prepare($sql);
     
     		$stmt->execute();
     		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
     		$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
     		 
     		return $outputData;
     	}
     		catch(Exception $e)
     		{
     			$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
     			 
     			return $exception;
     	}
     	}
     	function searchOrdersForLog($logNo,$locationId,$generatedBy){

       $connectionString = new DBHelper();
       $pdo_object = $connectionString->dbConnection();
       try{
       	 
       	 
        /*$sql = "
         SELECT '$locationId' LocationId,'$GeneratedBy' GeneratedBy,'$logNo' TeamOrderNo, 'NO Logic' TeamOrderStatus,COP.CustomerOrderNo AllOrders from (SELECT  top 1
         		CustomerOrderNo=STUFF((SELECT ','+ CustomerOrderNo FROM COHeader  t2 where t2.Status=4 and t2.LogNo='$logNo'
         				FOR XML PATH ('')),1,1,'') from COHeader t1 ) COP
        ";*/
       	$sql = "{CALL SP_GetLogOrdersToPrint(@logNo=:logNo, @locationId=:locationId, @generatedBy=:generatedBy,@outParam=:outParam)}";
        $stmt = $pdo_object->prepare($sql);
        $stmt->bindParam(':logNo',$logNo, PDO::PARAM_STR);
        $stmt->bindParam(':locationId',$locationId, PDO::PARAM_INT);
        $stmt->bindParam(':generatedBy',$generatedBy, PDO::PARAM_STR);
        $stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
         
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        
      /*   $file = fopen("D://checkingFile.txt", "w");
				fwrite($file,json_encode($results)); */
			
        
        $results[0]['reportUrl'] = 'birt-viewer';
		
		
         if($locationId=='4488' || $locationId=='4605' || $locationId=='4606' || $locationId=='4638' || $locationId=='4707')
        {
        	$results[0]['reportPath'] = '/getreport/karlogorder';
        }
        else
        {
        	$results[0]['reportPath'] = '/getreport/logorder';
       }
        
        
       
        
        $outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');

        return $outputData;
       }
       catch(Exception $e)
       {
       	$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());

       	return $exception;
       }
     	}
    
     function vallidateLogNo($locationId,$logNo)
     {
     	$outParam='';
     	$connectionString = new DBHelper();
     	$pdo_object = $connectionString->dbConnection();
     	try{
     			
     		$sql = "{CALL usp_ValidateLogInvoice (@logNo=:logNo,@locationId=:locationId,@outParam=:outParam)}";
     		$stmt = $pdo_object->prepare($sql);
     
     		$stmt->bindParam(':logNo',$logNo, PDO::PARAM_STR);
     		$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
     
     		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
     
     		$stmt->execute();
     		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
     
     	$outputData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
						
			return $outputData;
		  }
		catch(Exception $e)
	 			{
						$exception = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
						
						return $exception;
				}
     }

       function multipleInvoices($arrOfOrder,$logNo,$locationId,$modifiedBy,$type){
     	$connectionString = new DBHelper();
     	$outParam=null;
      /*  	if($type==1){
		     		$arrOfOrder= $arrOfOrder;
		     		$logNo='' ;
		     	}
		     	else {
		     		$arrOfOrder="[]" ;
		     		$logNo=$logNo ;
		     	}
		     	$file = fopen("F://wer.txt","w");
		     	fwrite($file,'helo');
		     	fclose($file) ; */
   
     	$pdo_object = $connectionString->dbConnection();
     	try{
     		
     		
     		$sql = "{CALL SP_InvoiceForMultipleOrders (@jsonForOrder=:jsonForOrder,@LogNo=:LogNo
     				,@modifiedBy=:modifiedBy,@locationId=:locationId,@outParam=:outParam)}";
     		$stmt = $pdo_object->prepare($sql);
     		
     		$stmt->bindParam(':jsonForOrder',$arrOfOrder, PDO::PARAM_STR);
     		$stmt->bindParam(':LogNo',$logNo,PDO::PARAM_STR);
     		$stmt->bindParam(':modifiedBy',$modifiedBy,PDO::PARAM_INT);
     		$stmt->bindParam(':locationId',$locationId,PDO::PARAM_INT);
     		$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 50);
     		
     		$stmt->execute();
     		$results =null;
     		//$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
     		$i = 1;
     		do {
     			$rowset = $stmt->fetchAll(PDO::FETCH_ASSOC);
     			if ($rowset) {
     				$results=$rowset ;
     			}
     			
     		} while ($stmt->nextRowset());
     		 
     	 if(sizeof($results[0]['OutParam']) > 0)
			{
				throw new vestigeException($results[0]['OutParam']);
			}
	
			}
			catch(Exception $e){
				throw new Exception($e->getMessage());
				
			}
			return  $results ;
     	}
     	
     function receivePaymentForLog($logNo,$unpaidAmount,$loggedInUserId,$favourableOrderNoForCourier,$withCourierCharges){
     		
     		$connectionString = new DBHelper();
     		$pdo_object = $connectionString->dbConnection();
     		try{
     			$outParam='';
     			 
     			$sql = "{CALL sp_ReceiveLogPayment(@logNo=:logNo, @unpaidAmount=:unpaidAmount,@receivedBy=:receivedBy,@favourableOrderNoForCourier=:favourableOrderNoForCourier,@withCourierCharges=:withCourierCharges,@outParam=:outParam)}";
     			$stmt = $pdo_object->prepare($sql);
     			$stmt->bindParam(':logNo',$logNo, PDO::PARAM_STR);
     			$stmt->bindParam(':unpaidAmount',$unpaidAmount, PDO::PARAM_INT);
     			$stmt->bindParam(':receivedBy',$loggedInUserId, PDO::PARAM_INT);
     			$stmt->bindParam(':favourableOrderNoForCourier',$favourableOrderNoForCourier, PDO::PARAM_STR);
     			$stmt->bindParam(':withCourierCharges',$withCourierCharges, PDO::PARAM_INT);
     			$stmt->bindParam(':outParam',$outParam, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 500);
     			 
     			$stmt->execute();
     			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
     			

     			$file = fopen("D://checkkkkk1.txt", "w");
     			fwrite($file, json_encode($results));
     			fclose($file);
     			
     			if($results[0]['OutParam'] == "INF0010")
     			{
     				throw new vestigeException("Log already invoiced or don't have any unpaid amount",1002);
     			}
     			else if($results[0]['OutParam'] == "NotUpdated")
     			{
     				throw new vestigeException("Server calculated amount ".$results[0]['LogCurrentAmount']." is not equal to passed amount i.e ".$results[0]['UnpaidAmount'] ." for this log",1002);
     			}
     			else if($results[0]['OutParam'] == "INF0011"){
     				throw new vestigeException("No favourable order selected to updated courier charges. Contact branch.",1003);
     			}

     		}
     		catch(Exception $e)
     		{
     			
     			throw new Exception($e->getMessage());
     		}
     		
     		/* $file = fopen("D://checkkkkk.txt", "w");
     		fwrite($file, json_encode($results));
     		fclose($file); */
     		
     		
     		
     		return $results;
     		
     	}

}
  
   


?>
