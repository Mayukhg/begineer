<?php



Class Category
{
	//function return category or merchandising 	
	function getCategory($loggedInUserPOSSelection,$forSkinCareItem)
	{
		
			$sql = "Select MM.MerchHierarchyId 'Id', MM.MerchHierarchyCode 'Code'
					From MerchHierarchy_Master MM(nolock)
					INNER JOIN MerchHierarchy_Config MH(nolock)
					ON MM.MerchConfigId = MH.MerchConfigId
					WHERE MH.Status = 1
					AND MM.Status = 1
					AND MM.IsTradable = 1
					AND MM.ParentHierarchyId > 0
					AND MH.[Level] =4";
					
		$db_helper_object = new DBHelper(); ////DBHelper is class to execute query and return result.
		$result_set = $db_helper_object->executeResultSet($sql);
		
		return $result_set;  // return result set only and json encoding will be done at module file.
	}
	
}


?>
