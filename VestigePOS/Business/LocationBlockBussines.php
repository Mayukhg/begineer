<?php


Class LocationBlockData
{
	var $vestigeUtil;
	function  __construct(){
		$this->vestigeUtil = new VestigeUtil();
	}
	
	function FetchLocForLoginLoc($locationId)
	{
		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
		
		try{
			
			if ($locationId == NULL)
			{
				throw new vestigeException("User Not Logged In ! Please Login again");
			}
			else
			{
				
				
				$sql = "select  lm.LocationCode as LocationCode, lm.LocationType as TypeVal, pm.KeyValue1 as Type,lm.Name as Location, lm.Status, ROW_NUMBER() over (ORDER BY lm.Status DESC) as id 	, '' as action,  lm.isLocationOnline,lm.isOnlineForDistributor ,
				case when lm.LocationType=4 and lm.Status=1 then 'Block'
				  when lm.LocationType=4 and lm.Status=2 then 'Block'
				  when lm.LocationType=4 and lm.Status=0  then 'Unblock'
				  when lm.LocationType=3 and lm.IsLocationOnline=0  then 'Unblock'
				  when lm.LocationType=3 and lm.IsLocationOnline=1  then 'Block' end as CurrentStatus 
				
				From Location_Master as lm
					inner join Parameter_Master as pm on lm.locationtype=pm.keyCode1 and pm.ParameterCode='LOCATIONTYPE'
					where pm.Description='Location Type' and lm.ReplenishmentLocationId='$locationId'";
						$stmt = $pdo_object->prepare($sql);
						$stmt->execute();
						
						$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
						$ReturnData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
					
				
				
			}
		}
		
		
		catch(Exception $e)
   		{
   			$ReturnData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   		
   		return $ReturnData;
		
		
		
		
	}
	function BlockUnblock($locationId,$locationType,$mode)
	{
		$connectionString = new DBHelper();
   		$pdo_object = $connectionString->dbConnection();
		
		try{
			
			if ($mode == 'Block' and $locationType=='3')
			{
				$sql = "Update Location_Master set IsLocationOnline=0  where LocationCode='$locationId'";
						$stmt = $pdo_object->prepare($sql);
						$stmt->execute();
						
						$results = $stmt->rowCount() . " records UPDATED successfully";
						$ReturnData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			}
			
			if ($mode == 'Block' and $locationType=='4')
			{
				
				$sql = "Update Location_Master set Status=0 where LocationCode='$locationId'";
						$stmt = $pdo_object->prepare($sql);
						$stmt->execute();
						
						$results = $stmt->rowCount() . " records UPDATED successfully";
						$ReturnData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			}
			
			
			
			if ($mode == 'Unblock' and $locationType=='3')
			{
				$sql = "Update Location_Master set IsLocationOnline=1  where LocationCode='$locationId'";
						$stmt = $pdo_object->prepare($sql);
						$stmt->execute();
						
						$results = $stmt->rowCount() . " records UPDATED successfully";
						$ReturnData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
			}
	
			 if ($mode == 'Unblock' and $locationType=='4')
			 {
				
				$sql = "Update Location_Master set Status=1 where LocationCode='$locationId'";
						$stmt = $pdo_object->prepare($sql);
						$stmt->execute();
						
						$results = $stmt->rowCount() . " records UPDATED successfully";
						$ReturnData= $this->vestigeUtil->formatJSONResult(json_encode($results), '');
				
			}
		}
		
		
		catch(Exception $e)
   		{
   			$ReturnData = $this->vestigeUtil->formatJSONResult('', $e->getMessage());
   		}
   		
   		return $ReturnData;
		
		
		
		
	}
	
}