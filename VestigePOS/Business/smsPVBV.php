
<?php
class smsPVBV{
	
	function SendSMS($mobileNo,$msgType,$strResponseIPAdd)
	{

		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		try {
			$sql="select 1 as SecureIPStatus from parameter_master where ParameterCode='SMSSecureIPAddress' and KeyValue1 like '%$strResponseIPAdd%'";
			$stmt = $pdo_object->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($results) && $results[0]['SecureIPStatus']==1 && isset($results[0]['SecureIPStatus']))
			{			
				smsPVBV :: SendPV_BV($mobileNo,$msgType);			
			}
			else
			{	
				echo 'Unknown IP Address';
				smsPVBV :: ipMismatch($strResponseIPAdd);			
			}
			
		}
		catch(Exception $e){
			throw new Exception($e->getMessage());
		}
		
	}
	
	
	function ipMismatch($strResponseIPAdd)
	{
		$posBusinessClassObj = new POSBusinessClass();
		
		$incdenceid=time();
		$emailSubject = 'Security Alert [IT'.$incdenceid.'] - Fraud case attempted from unknown IP Address';
		
		$dynamicText = '<p style="margin-bottom: 5px;font-family: CENTURY GOTHIC,tahoma;font-size: 15px;">
		Hi team, <br><br> Please look into the System immediately as someone is trying to acces SMS service from unknown IP address ('.$strResponseIPAdd.')
		</p>';
		
		$posBusinessClassObj->sendEmail($dynamicText,$emailSubject);	
	
	}
	
	function SendPV_BV($mobileNo,$msgType){
		//$msgType='pvbv';
		$response='';
		$connectionString = new DBHelper();
		$pdo_object = $connectionString->dbConnection();
		
		try{
			$outParam = '';
			$sql = "{call sp_SMSPVBV(@mobileNo=:mobileNo,@msgType=:msgType,@outParam=:outParam)}";
			$stmt = $pdo_object->prepare($sql);
			$stmt->bindParam(':mobileNo', $mobileNo,PDO::PARAM_STR);
			$stmt->bindParam(':msgType',$msgType ,PDO::PARAM_STR);
			$stmt->bindParam(':outParam', $outParam,PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,500);
			
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if(isset($results[0]['OutParam'])){
				if($results[0]['OutParam'] == 'WRNGMOB001'){
				$sendMessage = new SendSMS();
				$response=$sendMessage->apicallUnregistered($mobileNo);				
				$smsid1 = $results[0]['smsId'];
				$sql2="update SmsRequests set APIResponse='$response' where smsId=$smsid1";				
				$stmt = $pdo_object->prepare($sql2);
				$stmt->execute();	
				
					//IF mobile number not found then call on that particular number.
					echo "Mobile No Not registered:".$mobileNo."Api response".$response;
				}
				if($results[0]['OutParam'] == 'DUPMOB001'){
					//IF same mobile number exist for more than one distributor.
					echo "More than one distributor registered on this number ".$mobileNo;
				}
				if($results[0]['OutParam'] == 'INVLDMOBNO'){
					//IF same mobile number exist for more than one distributor.
					echo "MOBILE NO IS EMPTY OR NULL ".$mobileNo;
				}
				if($results[0]['OutParam'] == 'NOTTYPE1'){
					//IF transfer detail not available in transaction detail
					echo "Transfer detail not available ".$mobileNo;
				}
				if($results[0]['OutParam'] == 'WRNGTTYPE1'){
					//IF Transfer type in not valid.
					echo "Transfer detail is not valid ".$mobileNo;
				}
				if($results[0]['OutParam'] == 'WRNGMTYPE1'){
					//IF message type is not valid.
					echo "Message type is not valid ".$mobileNo;
				}
				if($results[0]['OutParam'] == 'NOBONUS1'){
					//IF Bonus detail not available for distributor
					echo "Bonus detail not available ".$mobileNo;
				}
				if($results[0]['OutParam'] == 'EXCDLMT1'){
					//No of message exceeded from 3 for a distributor in a day for a category.
					echo "Exceeded permitted limit of message in a day for ".$mobileNo;
				}
				
			}
			else{
					for($i=0;$i<sizeof($results);$i++){
					$sendMessage = new SendSMS();
					$dynamicMsg = $results[0]['Message'];
					$distributorId=$results[0]['DistributorId'];
					$distributorMobNo=$results[0]['DistributorMobNo'];				
					$messageType = $results[0]['MsgType'];
					$smsId=$results[0]['smsId'];
					
					if($messageType=='GROUPPV')
					{
						$SelfPV=$results[0]['SelfPV'];
						$GroupPV = $results[0]['GroupPV'];
						$date = $results[0]['Date'];
						$time = $results[0]['Time'];
						$comPV = $results[0]['ComPV'];
						$bonuspercent = $results[0]['bonuspercent'];
					$response=$sendMessage->apicallGroupPV($distributorId,$distributorMobNo,$SelfPV,$GroupPV,$time,$date,$comPV,$bonuspercent);
					
					}
					
					if($messageType=='PROMOTION')
					{
						$JoinMsgVal=$results[0]['JoinMsgVal'];
						$ReprMsgVal = $results[0]['ReprMsgVal'];
						
					$response=$sendMessage->apicallPromotion($mobileNo,$JoinMsgVal,$ReprMsgVal);
					
					}
					
					if($messageType=='PVBV')
					{
						$pv=$results[0]['PV'];
						$comPV = $results[0]['ComPV'];
						$date = $results[0]['Date'];
						$time = $results[0]['Time'];
					$response=$sendMessage->apicallpvbv($distributorId,$distributorMobNo,$pv,$comPV,$time,$date);					
					}
					if($messageType=='BONUS')
					{	
						$transType = $results[0]['TransferType'];
						$totalBonus = $results[0]['TotalBonus'];
						$netPay = $results[0]['NetPay'];
						$businessMonth = $results[0]['BusinessMonth'];
						$selfBV = $results[0]['SelfBV'];
						$remarks = $results[0]['Remarks'];
						if($transType=='V')
						{
							$voucherno = $results[0]['voucherno'];
							$faststartvoucher = $results[0]['faststartvoucher'];
							$response=$sendMessage->apicallBonusVoucher($distributorId,$distributorMobNo,$transType,$totalBonus,$netPay,$businessMonth,$selfBV,$voucherno,$faststartvoucher);
						}
						else
						{
							$faststartvoucher = $results[0]['faststartvoucher'];
							$response=$sendMessage->apicallBonus($distributorId,$distributorMobNo,$transType,$totalBonus,$netPay,$businessMonth,$selfBV,$remarks,$faststartvoucher);
						}
												
					}		
					if($messageType=='PASSWORD')
					{
						$password = $results[0]['Password'];
						$response=$sendMessage->apicallPassword($distributorId,$distributorMobNo,$password);
					}			
					$sql1="update SmsRequests set APIResponse='$response' where smsId=$smsId ";			
					$stmt = $pdo_object->prepare($sql1);
					$stmt->execute();	
					echo $response;
				}
		
			}
			
		}
		catch(Exception $e){
			throw new Exception($e->getMessage());
		}
		
	}
	
	
}



?>