<?php

class PurchaseOrderScreen
{
	function purchaseOrderHtml()
	{
		return  '<div class="divTransferOutOuter">
				
					<div id="divLookUp">
							
					</div>

					<div id="divTransferOutTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>

					<form id="formSearchTO">
							<div id="DivSearch">
								<div id="divTransferOut">
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">PO Number : </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="TOINumberSearch" type="text"  id="PONumber" name="PONumber">
											
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Status : </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<select class="requiredList" id="POVendorCode" name="POVendorCode">
														
														
												</select>
							
										     </td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Code :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="POVendorCode" name="POVendorCode">
														
														
												</select>
											</td>
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From PO Date : </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<select class="requiredList" id="POVendorCode" name="POVendorCode">
														
														
												</select>
							
										     </td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">To PO Date :</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="POVendorName" name="POVendorName">
														
														
												</select>
											</td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="POVendorCode" name="POVendorCode">
														
												</select>
											</td>
											
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From C Applicable : </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="showCalender2" type="text"  id="fromTODate" name="FromTODate">
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">PO Type : </td>
											<td rowspan="2" class="DISTd" style="text-align:left;width:200px;">
												<input class="showCalender2" type="text"  id="toTODate" name="ToTODate">
							
										     </td>
											
											
										</tr>
										
											
									</table>
								</div>
								<div class="divBtnAddSearch">
								<button type="button" disabled id="btnTSF02Search" style="margin-left:100px;" class="TOCreateActionButtons">Search</button>
								<button type="button" id="btnReset" class="btnAddSearch">Reset</button>
										
										
								</div>
								<div class="searchResultTopic">
									Search Results
								</div>
								
								<div id="DivTOISearchGrid" style="width:1040px;clear:both;">
									
									<table id="TOSearchGrid"></table>
									<div id="PJmap_TOSearchGrid"></div>
										
								</div>
							</div>
					</form>
						<div id="DivCreate">
							<div id="TransferOutCreateDiv">	
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">PO Number : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="TOINumberSearch" type="text"  id="POCreatePONumber" name="POCreatePONumber" placeholder="Press F4 for listing">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Amendment Number : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="POCreateAmendmentNumber" name="TOCreateTOISourceLocation">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">PO Type :*</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="POCreatePOType" name="POCreatePOType">
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Vendor Code :* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="POCreateVendorCode" name="POCreateVendorCode">
														
														
											</select>
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Vendor Name : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="POCreateVendorName" name="POCreateVendorName">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :*</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<select class="requiredList" id="POCreateDestinationLocation" name="POCreateDestinationLocation">
														
														
											</select>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Payment Terms : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTONumber" name="TOCreateTONumber">
											
						
									     </td>
										<td rowspan="2" class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Vendor Address:  </td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											
											<textarea class="distributor_info" id="TOCreateTOSourceLocation" rows="4" cols="20" name="TOCreateTOSourceLocation"></textarea>
										</td>
										
										<td rowspan="2" class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Address :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="TOCreateDestinationAddress" rows="4" cols="20" name="TOICreateDestinationAddress"></textarea>
										</td>
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Created Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="POCreateCreatedDate" name="POCreateCreatedDate" >
										</td>
									
										
									</tr>
									
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">PO Date :* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="POCreatePODate" name="POCreatePODate" >
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Expected Delivery Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="POCreateExpectedDeliveryDate" name="POCreateExpectedDeliveryDate" >
												
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Max. Delivery Date:</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="POCreateMaxDeliveryDate" name="POCreateMaxDeliveryDate" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Status. : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="POCreateStatus" name="POCreateStatus">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From C Applicable.*: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateExpectedDeliveryDate" name="TOCreateExpectedDeliveryDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Remarks. :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRefNo" name="TOCreateRefNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Total Tax Amount : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTONumber" name="TOCreateTONumber">
											
						
									     </td>
										<td rowspan="2" class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Shipping Details:  </td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											
											<textarea class="distributor_info" id="TOCreateTOSourceLocation" rows="4" cols="20" name="TOCreateTOSourceLocation"></textarea>
										</td>
										
										<td rowspan="2" class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Payment Details :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="TOCreateDestinationAddress" rows="4" cols="20" name="TOICreateDestinationAddress"></textarea>
										</td>
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Total PO Amount(Incl tax) : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="POCreateCreatedDate" name="POCreateCreatedDate" >
										</td>
									
										
									</tr>
				
				
				
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div class="DistributorMasterInformation">
								
							</div>
							<div class="DistributorCurrentRanking">
								
							</div>
							<div class="DistributorCurrentPreviousPvBvComputation">
								<table class="DISTable">
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right;">Item Code : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
			                        	<select class="DISTInput" id="TOCreateItemCode" name="TOCreateItemCode"  style="background-color:white;height: 24px;width:100px;">
										
				        
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Name: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateTransferPrice" name="TOCreateTransferPrice" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item UOM: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateUOM" name="TOCreateUOM" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Tax Group Code : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemDescription" name="TOCreateItemDescription" >
									     </td>
				
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Unit Price: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateWeight" name="TOCreateWeight" >
									    </td>
										<td class="DISTd" style="width:100px;text-align:right">Unit Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateRequestedQty" name="TOCreateRequestedQty" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Total Tax: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIIteBucketName" name="TOIIteBucketName" >
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">Amount[Excl.Tax]:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemBatchNo" name="TOCreateItemBatchNo" placeholder="Press f4 for listening" >
									    </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Amount[Incl.Tax]:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemBatchNo" name="TOCreateItemBatchNo" placeholder="Press f4 for listening" >
									    </td>
				
										<td colspan="2" class="DISTd" style="text-align:left;width:100px;">
											<button type="button" id="btnAdd" class="btnAddSearch">Add</button>
											<button type="button" id="clear" class="btnAddSearch">Clear</button>
											
									     </td>
				
									</tr>
									
									
								</table>
								
							</div>
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="TOItemGrid"></table>
								<div id="PJmap_TOItemGrid"></div>
									
							</div>
							<div class="divTOCreateActionButtons">
			  					<button type="button" disabled id="btnTSF02Confirm" class="TOCreateActionButtons">Save</button>
							
								<button type="button"  id="btnCreateReset" class="btnAddSearch">Confirm</button>
								<button type="button" disabled id="btnTSF02Print" class="TOCreateActionButtons">Cancel</button>
								<button type="button" disabled id="btnTSF02Confirm" class="TOCreateActionButtons">Short Closed</button>
							
								<button type="button"  id="btnCreateReset" class="btnAddSearch">Reset</button>
								<button type="button" disabled id="btnTSF02Print" class="TOCreateActionButtons">Print</button>
								<input type="hidden"  id="moduleCode" value="TSF02">
									<input type="hidden" id="actionName" value="">
							</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>
