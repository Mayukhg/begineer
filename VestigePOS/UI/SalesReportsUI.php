<?php

class SalesReportScreen
{
	function SalesReportHtml()
	{
		
			return  '<div class ="divReportOuter">
					
						<input type="hidden" id="moduleCode" value="RCAT02">
						<input type="hidden" id="reportcode" value="0" />
						<div class="divReportTitle" >
							Sales Reports
						</div>
						<div class="divReportSelection" style="margin-left:0;padding-left:0;width:100%;">
							<div style="margin-left:0;float:right;">
									Report Name: <select id="reportTypeSelection" class="reportTypeSelection">
											<option value="0">Select</option>
											<option value="StockistSalesReport">Stockist Sales Report</option>
											<option value="StockistSalesLog">Stockist Sales Log</option>
											<option value="CheckPaymentReport">Cheque Payment Report</option>
											<option value="CreditCardPaymentReport">Credit Card Payment Report</option>
											<option value="CashBookPaymentReport">Cash Book Payment Report</option>
											<option value="PUCAccountSummary">PUC Account Summary</option>
											<option value="BankPaymentReport">Bank Book Payment Report</option>
											<option value="PUCAccountDeposit">PUC Account Deposit</option>
											<option value="DistributorBusinessReport">Distributor Business Report</option>
										</select>
							</div>
						</div>
						<div class="divReportSearchCondition" style="width:100%;height:10%;background-color:#e5f4fb;">
							
						</div>
						<div class="divReportAction" style="width:100%;">
							<button type="button" id="btnRCAT02View" class="reportViewerButton">View</button>
						</div>
						<div class="divReportViewer" style="width:100%;height:74%">
							<iframe id="hidden_frame" name="hidden_frame" style="width:99.7%;height:100%;"></iframe>
						</div>
					</div>'	;
			
			/*
			 * <option value="ConsolidatedSalesCollectionReport">Consolidated Sales Collection Report</option>
			 * <option value="DistributorAddressReport">Distributor Address Report</option>
			 * <option value="GRNRegisterReport">GRN Register Report</option>
			 * <option value="DistributorBusinessReport">Distributor Business Report</option>
			 * <option value="SalesCollectionReport">Sales Collection Report</option>
			 * <option value="MonthwiseItemSalesReport">Monthwise Item Sales Report</option>
			 * <option value="StockAdjustmentSummary">Stock Adjustment Report</option>
			 * */
	}
	function InventoryReportHtml($locationId){
		
		if ($locationId==10)
		{
		return  '<div class ="divReportOuter">
			
						<input type="hidden" id="moduleCode" value="RCAT02" />
						<input type="hidden" id="reportcode" value="1" />
						<div class="divReportTitle" >
							Inventory Reports
						</div>
						<div class="divReportSelection" style="margin-left:0;padding-left:0;width:100%;">
							<div style="margin-left:0;float:right;">
									Report Name: <select id="reportTypeSelection" class="reportTypeSelection">
											<option value="0">Select</option>
											<option value="STNItemWise">STN Item Wise</option>
											<option value="ReceivedItemWise">Received Item Wise</option>
											<option value="ProductWiseSales">Product Wise Sales</option>
											<option value="StockSummary">Stock Summary</option>
											<option value="POItemWise">PO Item Wise</option>
											<option value="StockAdjustmentSummary">Stock Adjustment Report</option>
											<option value="InTransitReport">In Transit Report</option>
											<option value="ConfirmedOrderVsInventoryPos">Confirmed Order Vs Inventory Position</option>
											<option value="BOWISESALES">Bo Wise Sales</option>
											<option value="BOWISESTOCK">Bo Wise Stock Position</option>
											<option value="NODUser">NOD User Report</option>
				                             <option value="GITREPORT">Balance+GIT Report</option>  
                                            <option value="NODLocationWiseReport">NOD Location Wise Report</option>
				                            <option value="GRNReport">GRN Report</option>
				                            <option value="vendorretrun">Vendor Return Report</option>
				                            <option value="TaxReport">Tax Report</option>
				                             <option value="Courier_Report">Courier Report</option>
											</select>
							</div>
						</div>
						<div class="divReportSearchCondition" style="width:100%;height:10%;background-color:#e5f4fb;">
				
						</div>
						<div class="divReportAction" style="width:100%;">
							<button type="button" id="btnRCAT02View" class="reportViewerButton">View</button>
						</div>
						<div class="divReportViewer" style="width:100%;height:74%">
							<iframe id="hidden_frame" name="hidden_frame" style="width:99.7%;height:100%;"></iframe>
						</div>
					</div>'	;
		
		//<option value="GoodsInTransit">Goods In Transit</option>
		}
		else 
		{
			return  '<div class ="divReportOuter">
		
						<input type="hidden" id="moduleCode" value="RCAT02" />
						<input type="hidden" id="reportcode" value="1" />
						<div class="divReportTitle" >
							Inventory Reports
						</div>
						<div class="divReportSelection" style="margin-left:0;padding-left:0;width:100%;">
							<div style="margin-left:0;float:right;">
									Report Name: <select id="reportTypeSelection" class="reportTypeSelection">
											<option value="0">Select</option>
											<option value="STNItemWise">STN Item Wise</option>
											<option value="ReceivedItemWise">Received Item Wise</option>
											<option value="ProductWiseSales">Product Wise Sales</option>
											<option value="StockSummary">Stock Summary</option>
											<option value="POItemWise">PO Item Wise</option>
											<option value="StockAdjustmentSummary">Stock Adjustment Report</option>
											<option value="InTransitReport">In Transit Report</option>
											<option value="ConfirmedOrderVsInventoryPos">Confirmed Order Vs Inventory Position</option>
											<option value="NODReport">NOD Report</option>
											<option value="NODUser">NOD User Report</option>
											<option value="TaxReport">Tax Report</option>
				                             
										</select>
							</div>
						</div>
						<div class="divReportSearchCondition" style="width:100%;height:10%;background-color:#e5f4fb;">
			
						</div>
						<div class="divReportAction" style="width:100%;">
							<button type="button" id="btnRCAT02View" class="reportViewerButton">View</button>
						</div>
						<div class="divReportViewer" style="width:100%;height:74%">
							<iframe id="hidden_frame" name="hidden_frame" style="width:99.7%;height:100%;"></iframe>
						</div>
					</div>'	;
			
			//<option value="GoodsInTransit">Goods In Transit</option>
			
		}
	}
	function FinancialReportHtml(){
		return  '<div class ="divReportOuter">
			
						<input type="hidden" id="moduleCode" value="RCAT02" />
						<input type="hidden" id="reportcode" value="2" />
						<div class="divReportTitle" >
							Financial Reports
						</div>
						<div class="divReportSelection" style="margin-left:0;padding-left:0;width:100%;">
							<div style="margin-left:0;float:right;">
								Report Name: <select id="reportTypeSelection" class="reportTypeSelection">
											<option value="0">Select</option>
											<option value="STNFinance">STN Finance Report</option>
											<option value="DBRPrintingReport">DBR Printing Report</option>
                                            
				
										</select>
							</div>
						</div>
						<div class="divReportSearchCondition" style="width:100%;height:10%;background-color:#e5f4fb;">
				
						</div>
						<div class="divReportAction" style="width:100%;">
							<button type="button" id="btnRCAT02View" class="reportViewerButton">View</button>
						</div>
						<div class="divReportViewer" style="width:100%;height:74%">
							<iframe id="hidden_frame" name="hidden_frame" style="width:99.7%;height:100%;"></iframe>
						</div>
					</div>'	;
	}
	function OtherReportHtml($locationId){
		
		if ($locationId==10)
		{
		return  '<div class ="divReportOuter">
			
						<input type="hidden" id="moduleCode" value="RCAT02" />
						<input type="hidden" id="reportcode" value="3" />
						<div class="divReportTitle" >
							Other Reports
						</div>
						<div class="divReportSelection" style="margin-left:0;padding-left:0;width:100%;">
							<div style="margin-left:0;float:right;">
									Report Name: <select id="reportTypeSelection" class="reportTypeSelection">
											<option value="0">Select</option>
											<option value="DistributorAddressLabel">Distributor Address Label Report</option>
											<option value="DistributorSingleAddress">Distributor Single Address Print Report</option>
											<option value="DistributorPanAndBankEdit">Distributor PAN And Bank Edit Report</option>
											<option value="LocationWiseListOfModifiedDistributor">Location Wise List Of Modified Distributor Report</option>
  											<option value="DistributorJoiningReport">Distributor Joining Report</option> 
 														 <option value="SMSReport">SMS Report</option> 
 														 <option value="ItemDetail">Item Detail</option>
				                                          <option value="Reject">Reject Report</option>  
				                                          <option value="DailyNew">New Pan Bank Status Report</option> 
				                                          <option value="MissingLevel">Missing Level</option>
				                                          <option value="Distributor_Vouchers">Distributor Vouchers</option> 
                                                         <option value="Stock_Transfer_Rate">Location wise Stock Transfer Rate</option>
				                                          
											</select>
							</div>
						</div>
						<div class="divReportSearchCondition" style="width:100%;height:10%;background-color:#e5f4fb;">
				
						</div>
						<div class="divReportAction" style="width:100%;">
							<button type="button" id="btnRCAT02View" class="reportViewerButton">View</button>
						</div>
						<div class="divReportViewer" style="width:100%;height:74%">
							<iframe id="hidden_frame" name="hidden_frame" style="width:99.7%;height:100%;"></iframe>
						</div>
					</div>'	;
		//<option value="DistributorPanAndBankStatus">Distributor PAN And Bank Status Report</option>
		}
		else 
		{
			return  '<div class ="divReportOuter">
		
						<input type="hidden" id="moduleCode" value="RCAT02" />
						<input type="hidden" id="reportcode" value="3" />
						<div class="divReportTitle" >
							Other Reports
						</div>
						<div class="divReportSelection" style="margin-left:0;padding-left:0;width:100%;">
							<div style="margin-left:0;float:right;">
									Report Name: <select id="reportTypeSelection" class="reportTypeSelection">
											<option value="0">Select</option>
											<option value="DistributorAddressLabel">Distributor Address Label Report</option>
											<option value="DistributorSingleAddress">Distributor Single Address Print Report</option>
											<option value="DistributorPanAndBankEdit">Distributor PAN And Bank Edit Report</option>
											<option value="LocationWiseListOfModifiedDistributor">Location Wise List Of Modified Distributor Report</option>
  											<option value="DistributorJoiningReport">Distributor Joining Report</option>
				                             <option value="DistributorJoiningReport">Distributor Joining Report</option>
 														<option value="SMSReport">SMS Report</option>
 														<option value="ItemDetail">Item Detail</option>
				                                        
											</select>
							</div>
						</div>
						<div class="divReportSearchCondition" style="width:100%;height:10%;background-color:#e5f4fb;">
			
						</div>
						<div class="divReportAction" style="width:100%;">
							<button type="button" id="btnRCAT02View" class="reportViewerButton">View</button>
						</div>
						<div class="divReportViewer" style="width:100%;height:74%">
							<iframe id="hidden_frame" name="hidden_frame" style="width:99.7%;height:100%;"></iframe>
						</div>
					</div>'	;
		}
	}
}	
?>