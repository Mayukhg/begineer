<?php

//SC- Stock Count.

class StockCountScreen
{
	function StockCountHtml()
	{
		return  '<div class="divStockCountOuter">

					<div id="divStockCountTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li style="visibility:hidden;"><a href="#DivCreate">Create</a></li>
							
						</ul>
				<form id="stockCountSearchForm">
						<div id="DivSearch">
							<div id="divStockCount">
								<img id="canvasImage" >
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Seq. No : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
				
											<input class="GRNTSearchInput" type="text"  id="SCSearchSeqNo" name="SCSearchSeqNo">
											<input type="hidden" id="moduleCode" value="INV02">
											
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<Select class="requiredList"   id="SCSearchLocation" name="SCSearchLocation">
				
											</select>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="CRLocation" name="SCSearchStatus">
													
													
											</select>
										</td>
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Initiated Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="SCInitiatedDate" name="SCInitiatedDateJSFormatted">
											<input type="hidden"  id="SCInitiatedDateDataBaseFormatted" name="SCInitiatedDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Initiated By :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="" type="text"  id="CRInitiatedBy" name="CRInitiatedBy">
										</td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Confirmed Date :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="" type="text"  id="CRConfirmedDate" name="CRConfirmedDateJSFormatted">
											<input  type="hidden"  id="CRConfirmedDateDatabaseFormatted" name="CRConfirmedDate">
										</td>
										
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Confirmed By: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="" type="text"  id="SCConfirmedBy" name="SCConfirmedBy">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;"></td>
										<td class="DISTd" style="text-align:left;width:150px;">
											
										</td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;"></td>
										<td class="DISTd" style="text-align:left;width:150px;">
											
										</td>
										
										
									</tr>
									
								</table>
						    	<div class="divSCSearchActionButton">
				<h7 style="font-size: smaller;">Select All:</h7>
						<input type="checkbox" name="selectAll" id="selectAll">
				       
				
									<button type="button" id="btnSearchSCReset" class="SCSearchActionButton">Reset</button>
									<button type="button" id="btnINV02Search" class="SCCreateActionButtons">Search</button>
									<button type="button" id="btnINV01Save" class="SCCreateActionButtons">Save</button>
									
								</div>
							</div>
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivSCSearchGrid">
								
								<table id="SCSearchGrid"></table>
								<div id="PJmap_SCSearchGrid"></div>
									
							</div>
						</div>
				</form>
						<div id="DivCreate">
							<div id="StockCountCreateDiv">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location: *</td>
										<td class="DISTd" style="text-align:left;width:150px;">
				
											<select class="requiredList" id="SCCreateLocation" name="SCCreateLocation">
														
														
											</select>
											
										</td>
				
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location Code : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="SCCreateLocationCode" name="SCCreateLocationCode" disabled>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="SCCreateStatus" name="SCCreateStatus" disabled>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Initiated Date :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											
											<input class="showCalender" type="text"  id="SCCreateInitiatedDate" name="SCCreateStockCountDate"> 	
						
									     </td>
										
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Initiated By :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											
											<input class="" type="text"  id="SCCreateInitiatedBy" name="SCCreateInitiatedBy" disabled>
										</td>
				
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Confirmed Date: </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="SCCreateConfirmedDate" name="SCCreateConfirmedDate">
										</td>
									</tr>
				
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Confirmed By:  </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											
											<input class="" type="text"  id="SCCreateConfirmedBy" name="SCCreateConfirmedBy" disabled>
										</td>
				
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Stock Count No: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="SCStcokCountNo" name="SCStcokCountNo" disabled>
						
									     </td>
										
									</tr>
				
								</table>
				
								<div class="divSCCreateActionButtons">
									<button type="button" id="btnINV02Close" class="SCCreateActionButtons">Close</button>					
									
									<button type="button" id="btnReset" class="SCCreateActionButtons">Reset</button>
									<button type="button" id="btnINV02Print" class="SCCreateActionButtons">Print</button>
									<button type="button" id="btnINV02Execute" class="SCCreateActionButtons">Confirm</button>					
									<button type="button" id="btnINV02Create" class="SCCreateActionButtons">Save</button>					
									<button type="button" id="btnINV02Initiate" class="SCCreateActionButtons">Initiate</button>					
									
										
								</div>
				
								<div class="searchResultTopic">
									Item Details
								</div>
								<div class="SCCreateHiddenDiv"> 	
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Code :* </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="DISTSearchInput" type="text"  id="SCCreateItemCode" name="SCCreateItemCode">
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Item Desc.: </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="DISTSearchInput" type="text"  id="SCCreateItemDescription" name="SCCreateItemDescription">
							
										     </td>
											<td class="DISTd" style="text-align:right;width:150px;padding-right:10px;"> Physical Quntity: </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="DISTSearchInput" type="text"  id="SCCreatePhysicalQuantity" name="SCCreatePhysicalQuantity">
							
										     </td>	
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Bucket Name :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="SCCreateBucket" name="SCCreateBucket">
															
															
												</select>
											</td>
											
											
										</tr>
									</table>
									<div class="divSCCreateAddClearButtons">
									
										<button type="button" id="btnSCCreateClear" class="SCCreateActionButtons">Clear</button>
										<button type="button" id="btnSCCreateAdd" class="SCCreateActionButtons">Add</button>
										
									</div>
								</div>
				
								<div id="divSCCreateItemGrid" style="width:1040px;clear:both;">
								
									<table id="SCCreateItemGrid"></table>
									<div id="PJmap_SCCreateItemGrid"></div>
									
								</div>
								
								<div id="batchGridId">
									<div class="searchResultTopic">
										Batch Details
									</div>
										
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Batch No. :* </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="DISTSearchInput" type="text"  id="SCCreateBatchNo" name="SCCreateBatchNo">
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Physical Quantity :* </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="DISTSearchInput" type="text"  id="SCCreateBatchPhysicalQty" name="SCCreateBatchPhysicalQty">
							
										     </td>
											
										</tr>
									</table>
									<div class="divSCCreateBatchAddClearButtons">
									
										<button type="button" id="btnSCCreateBatchClear" class="SCCreateBatchAddClearButtons">Clear</button>
										<button type="button" id="btnSCCreateBatchAdd" class="SCCreateBatchAddClearButtons">Add</button>
										
									</div>
									
									<div id="divSCCreateBatchGrid">
									
										<table id="SCCreateBatchGrid"></table>
										<div id="PJmap_SCCreateBatchGrid"></div>
										
									</div>
								</div>	

							</div>
							
						</div>
						
						
					</div>
				</div>';
	}
}


?>
