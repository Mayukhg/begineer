<?php

class OrderMovementScreen
{
	function ordermovementHtml()
	{
		return  '<div id="historyPopUP" title="History">
							<table class="DISTable">
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Log No : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="bonusChequeFields" type="text"  id="historyLogNo" name="HistoryLogNo">
										</td>
				
				<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Distributor No: </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="historyDistributorNo" type="text"  id="historyDistributorNo" name="HistoryDistributorNo" maxlength="8">
										</td>
				<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Status: </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select  class="CommonSelect" name="HistoryStatus" id="HistoryStatus" style="width:175px;" disabled>
												<option value="-1">confirmed</option>
												<option value="3">Confirmed</option>
												<option value="2">Cancelled</option>
												<option value="4">Invoiced</option>
												
	     	     							</select>
										</td>
										
										<td rowspan="2" class="DISTd" style="text-align:right;width:90px;">
											<button type="button" id="btnSearch" class="historyButtons" style="width:82px;height:31px;margin-right:20px;margin-top:40px;">Search</button>
										</td>
				                        
				                          <td rowspan="2" class="DISTd" style="text-align:right;width:90px;">
											<button type="button" id="btnReset" class="historyButtons" style="width:82px;height:31px;margin-right:20px;margin-top:40px;">Reset</button>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="historyFromDate" name="HistoryFromDate">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">To Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="historyToDate" name="HistoryToDate">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"></td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"></td>
									</tr>									
							</table>
				
							<div id="divHistoryGrid">
								<table id="orderMovementGrid"></table>
								<div id="historyGridPager"></div>
							</div>
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd">Source Location :</td>
											<td class="DISTd"><select class="requiredList" id="SourceLocation" style="background-color:white;height: 28px;"/></td>                                                                                  
				                            <td class="DISTd">Destination Location :</td>
											<td class="DISTd"><select class="requiredList" id="DestinationLocation"style="background-color:white;height: 28px;" /></td>
				<td class="DISTd" style="text-align:left;width:90px;"></td>
										<td rowspan="2" class="DISTd" style="text-align:right;width:90px;">
											<button type="button" id="btnOMV01move" class="historyButtons" style="width:87px;height:31px;margin-right:10px;">Move</button>
										</td>
				                          
				                            <input type="hidden"  id="moduleCode" value="OMV01">
				                            <input type="hidden" id="actionName" value="">
										</tr>	
									</table>
				
							</div>		
						</div>';
	}
}
?>
