<?php

/* 
 * LTO = Log Team Order
 */
class LogTeamOrderScreen
{
	function logTeamOrderHtml()
	{
		return  '<div class="LTOouterdiv">
				<div id="divLookUp"  title="Courier Details">
				
				
				
					<div id="loginScreenPopUp">
					
						<form id="courierDetailsForm">
				
				
						  <table class="CommonTable">
							
				
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:110px;padding-right:10px;">Reference Log :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="referenceLog" name="ReferenceLog" style="width:200px;" disabled>
									
							    </td>	
								
							</tr>
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:110px;padding-right:10px;">Docket No. * :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="docketNo" name="DocketNo" style="width:200px;" maxlength="50">
									
							    </td>	
								
							</tr>

			         
							<tr class="CommonTr" >

									
								<td class="CommonTd" style="text-align:right;width:110px;padding-right:10px;">Courier Company Name *: </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="courierCompanyName" name="CourierCompanyName" style="width:200px;" maxlength="300">
								<input type="hidden"  id="CourierId" name="CourierId" style="width:200px;" maxlength="300">
										
							    </td> 	
								
							</tr>
							
							
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:110px;padding-right:10px;">Number of boxes *: </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="noOfBoxes" name="NoOfBoxes" style="width:200px;">
									
							    </td>
								
									
								
							</tr>
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:110px;padding-right:10px;">Weight (in gram) *: </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="weightInGram" name="WeightInGram" style="width:200px;">
									
							    </td>
				
								
							</tr>
							<tr class="CommonTr">
								<td class="CommonTd" style="text-align:right;width:110px;padding-right:10px;">Items weight : </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="itemsWeight" name="ItemsWeight" style="width:200px;" disabled>
									
							    </td>
							</tr>
				
						</table>
					  </form>	
				
						<div class="divCourierDetailActionButtons">
					
							<button type="button" id="save" class="courierDetailsActionButtons">Save</button>
				
							<button type="button" id="Cancel" class="courierDetailsActionButtons">Cancel</button>
				
						</div>
						
					</div>
				
					
				
				
				
					</div>
					<form id="LogTeamOrderForm">
					<div class="LTOinnerdiv1">
					<table class="CommonTable">
						<tr class="CommonTr">
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Type :* </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								<select  class="CommonSelect" name="LTOtype" id="LTOtype" >
									<option value="1">Log</option>
									<option value="2">Team</option>
	     	     				</select> 
							</td>	
							<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Number :  </td>
							<td class="CommonTd" style="text-align:left;width:200px;">
								<input class="commonTextField" type="text"  id="LTONumber" name="LTONumber" style="width:200px;">
								
						     </td>	
							<td class="CommonTd" style="text-align:right;width:125px;padding-right:10px;">Log Status :</td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								<select  class="CommonSelect" name="logStatus" id="logStatus" >
									<option value="1">Open</option>
									<option value="2">Closed</option>
	     	     				</select> 
								
						     </td>
						</tr>
						<tr class="CommonTr">
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Distributor ID :* </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								 <input class="commonTextField" type="text"  id="LTODistributorId" name="LTODistributorId" style="width:120px;">
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Pick Up Center :* </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								<select  class="CommonSelect" name="LTOPickUpCenterId" id="LTOPickUpCenterId" style="width:200px; height:25px;">
									<option value="-1">Select</option>
									
	     	     				</select> 
							</td>
				<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Date : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								<input class="showCalender" style="width:120px;height:15px;" name="fromDate" type="text" id="fromDate">
							</td>
				
						
						</tr>
					</table>
				
				</div>
				<div class="LTOinnerdiv2">
					<table class="CommonTable">
						
						<tr class="CommonTr">
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Distributor ID : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								 <input class="commonTextField" type="text"  id="LTODistributorID" name="LTODistributorID" style="width:120px;">
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Distributor Name : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								 <input class="commonTextField" type="text"  id="LTODistributorName" name="LTODistributorName">
							</td>
							<td class="CommonTd" style="padding-left: 10px;">
								Zero Log: <input type="checkbox" id="zeroLog" name = "zeroLog" value ="1" > 
							</td>
				
							<td class="CommonTd">
								<select  class="CommonSelect" name="IsChangeAddress" id="isChangeAddress" >
									<option value="-1">Select Mode</option>
									<option value="1">Self</option>
									<option value="2">Courier</option>
	     	     				</select>
								
							</td>
							
							
						</tr>
						<tr class="CommonTr">
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;vertical-align: top;">Address line 1:* </td>
							<td class="CommonTd" style="text-align:left;width:150px;padding:0px;">
								 <textarea class="commonTextArea" id="addressLine1" rows="3" cols="13" name="addressLine1"></textarea>
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;vertical-align: top;">Address line 2:* </td>
							<td class="CommonTd" style="text-align:left;width:150px;padding:0px;">
								 <textarea class="commonTextArea" id="addressLine2" rows="3" cols="13" name="addressLine2"></textarea>
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;vertical-align: top;">Address line 3: </td>
							<td class="CommonTd" style="text-align:left;width:150px;padding:0px;">
								 <textarea class="commonTextArea" id="addressLine3" rows="3" cols="13" name="addressLine3"></textarea>
							</td>
							
						</tr>
						<tr class="CommonTr">
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;vertical-align: bottom;">Address line 4: </td>
							<td rowspan = "2" class="CommonTd" style="text-align:left;width:150px;">
								 <textarea class="commonTextArea" id="addressLine4" rows="3" cols="13" name="addressLine4"></textarea>
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Country :* </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								 <select  class="CommonSelect" name="Country" id="Country" >
									<option value="0">Select</option>
	     	     				</select> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">State :* </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								<select  class="CommonSelect" name="State" id="State" >
									<option value="0">Select</option>
	     	     				</select> 
							</td>
							
							
						</tr>
						<tr class="CommonTr">
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;"></td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">City :* </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								<select  class="CommonSelect" name="City" id="City" >
									<option value="0">Select</option>
	     	     				</select> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Pin Code : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOPinCode" name="LTOPinCode" style="width:110px;"> 
							</td>
							
							
						</tr>
						<tr class="CommonTr">
							
							
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Phone 1 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOPhone1" name="LTOPhone1" style="width:110px;"> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Email 1 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOEmail1" name="LTOEmail1" style="width:110px;"> 
							</td>
				
				
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Mobile 1 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOMobile1" name="LTOMobile1" style="width:110px;"> 
							</td>
							
							
						</tr >
						<tr class="CommonTr" hidden>
							
							
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Mobile 2 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOMobile2" name="LTOMobile2" style="width:110px;"> 
							</td>
							
				<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Phone 2 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOPhone2" name="LTOPhone2" style="width:110px;"> 
							</td>
				
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Email 2 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOEmail2" name="LTOEmail2" style="width:110px;"> 
							</td>
							
							
						</tr>
						<tr class="CommonTr" hidden>
							
							
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Fax 1 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOFax1" name="LTOFax1" style="width:110px;"> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Fax 2 : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOFax2" name="LTOFax2" style="width:110px;"> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;"> Website : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input class="commonTextField" type="text"  id="LTOWebsite" name="LTOWebsite" style="width:110px;"> 
							</td>
							
							
						</tr>
				<tr class="CommonTr" >
							
							
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Courier Company Name : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input disabled class="commonTextField" type="text"  id="courierCompanyName2" name="courierCompanyName2" style="width:110px;"> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;">Weight (in Gram) : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input disabled class="commonTextField" type="text"  id="weightInGram2" name="LTOFax2" style="width:110px;"> 
							</td>
							<td class="CommonTd" style="text-align:right;width:150px;padding-right:10px;"> Docket No : </td>
							<td class="CommonTd" style="text-align:left;width:150px;">
								  <input disabled class="commonTextField" type="text"  id="docketNo2" name="docketNo2" style="width:110px;"> 
							</td>
							
							
						</tr>
					</table>
				
				</div>
				<div class="LogTeamOrderButtonsDiv">
				     <button type="button" id="clear" class="LogTeamOrderButtons2">Clear</button>	

					<button type="button" id="btnPOS02Save" class="LogTeamOrderButtons">Add</button>
					<button type="button" id="btnPOS02Search" class="LogTeamOrderButtons">Search</button>
				<button type="button" id="btnCourierDetails" disabled class="LogTeamOrderButtons">Courier Details</button>
					<button type="button" id="btnPrintLog" disabled class="LogTeamOrderButtons">Print Log</button>
			         	<input type="hidden" id="moduleCode" value="POS02">
									<input type="hidden" id="actionName" value="">	
				</div>
				<div class="logTeamOrderGridDiv">
								
					<table id="LogTeamOrderGrid"></table>
					<div id="pjmap_logTeamOrderGrid"></div>
									
				</div>
				<div class="LTOPaymentDescGridDiv">
								
					<table id="LTOPaymentDescGrid"></table>
					<div id="pjmap_LTOPaymentDescGrid"></div>
									
				</div>
				<div class="LTOPaymentButtonsDiv">
					<button type="button" id="exit" class="LogTeamOrderButtons2">Exit</button>

					<button type="button" id="btnPOS02Invoice" class="LogTeamOrderButtons">Invoice</button>
				
					<div class="">
						<button type="button" id="receiveLogpayment" class="LogTeamOrderButtonsExtra">Receive Payment</button>			    
					</div>
			    
				</div>
				<div id="shortInventorydiv">
					<div class="divOuter">
				 
						<div class="divShortInventoryMessage">
							Short Item in Inventory
						</div>
						<div id="divShortInventoryGrid" style="width:400px;clear:both;">
									
							<table id="ShortInventoryGrid"></table>
							<div id="PJmap_ShortInventoryGrid"></div>
										
						</div>
					   <div id="InvoiceStatusGrid" style="width:640px;clear:both;" title="Invoiced orders">
									
							<table id="LOGInvoiceStatus"></table>
							<div id="PJmap_LOGInvoiceStatus"></div>
										
						</div>
				
				
						<div id="logTeamOrderInvoicePopUp" style="width:600px;clear:both;" title="Invoices for log">
							
							<div style="border:1px solid;">
								<table id="LOGPendingOrders"></table>
								<div id="PJmap_LOGPendingOrders"></div>
							</div>
							
							<div style="border:1px solid">
								<table id="LOGInvoicedOrders"></table>
								<div id="PJmap_LOGInvoicedOrders"></div>
							</div>	
							
				
							<div id="invoicesForAllLog">
				
								<button class="loginvoiceExit" id="logInvoiceExit">Exit</button>
								<button class="loginvoice" id="btnPOS02Invoice2">Invoice</button>
								
							</div>
						</div>
				
						<div id="receivePayPopUp" title="Receive Payment" style="background-color:#0375bb;">
							
							<div id="divLookUpGrid" style="width:560px;clear:both;border:1px solid;">
								<table id="receivePaymentGrid"></table>
								<div id="receivePaymentGridPager"></div>
							</div>
				
							<div class="divLookUpButtons">
	
								<button type="button" class="lookUpActionButtons" id="btnReceivePayClose">Close</button>
								<button type="button" class="lookUpActionButtons" id="btnReceivePayOk">Receive</button>
								<button type="button" class="lookUpActionButtons" id="btnSaveNewPayments" hidden="true">Save New Payments</button>
	
							</div>
				
						</div>
				
				
						<div class="divShortInventoryAction">
							<button type="button" id="ok" class="shortInventoryAction">Ok</button>
							<button type="button" id="cancel" class="shortInventoryAction">Cancel</button>
						</div>
				
				
					</div>
				</div>
				
				
				</form>
				</div>';
	}
}

?>
