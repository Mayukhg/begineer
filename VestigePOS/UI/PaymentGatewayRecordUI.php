
<?php

class PaymentGatewayRecordScreen
{
	function PaymentGatewayRecordHtml()
	{
		return  '<div id="historyPopUP" title="History">
							<table class="DISTable">
									<tr class="DISTr">

										

				<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;font-size:larger;">Distributor No: </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="historyDistributorNo" type="text"  id="historyDistributorNo" name="HistoryDistributorNo" maxlength="8">
										</td>
				
				
			          <td class="DISTd" style="text-align:right;width:100px;padding-right:10px;font-size: larger;">Order No: </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="historyDistributorNo" type="text"  id="historyOrderNo" name="HistoryOrderNo" >
										</td>
				
				
				     <td class="DISTd" style="text-align:right;width:100px;padding-right:10px;font-size: larger;">Log No : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="bonusChequeFields" type="text"  id="historyLogNo" name="HistoryLogNo">
										</td>
										
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:128px;padding-right:10px;font-size: larger;">Transaction From Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="historyFromDate" name="HistoryFromDate">
										</td>
										<td class="DISTd" style="text-align:right;width:119px;padding-right:10px;font-size: larger;"> Transaction To Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="historyToDate" name="HistoryToDate">
										</td>
		
				<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;font-size: larger;"> Status : </td>
										<td class="DISTd" style="text-align:left;width: 152px;">
											<select  class="CommonSelect" name="HistoryStatus" id="PaymentStatus" style="width:152px;height: 34px;">
												<option value="-1">Select</option>
				                                <option value="Captured">Captured</option>
												<option value="Canceled">Cancelled</option>
				                                <option value="Error">Error</option>
											
	     	     							</select>
										</td>
				
									</tr>
				
				                 <tr class="DISTr">
				                      
				                        <td class="DISTd" style="text-align:left;width:90px;"></td>
				                        <td class="DISTd" style="text-align:left;width:90px;"></td>
				                        
				              
				                       <td  class="DISTd" style="text-align:right;width:90px;">
											<button type="button" id="btnPGR01repayment" class="historyButtons" style="width: 131px;height: 31px;margin-right:-102px;visibility: hidden;">Allow Repayment</button>
			                            </td>
				                            <td  class="DISTd" style="text-align:right;width:90px;">
											<button type="button" id="btnPGR01Report" class="historyButtons" style="width:82px;height:31px;margin-right: -102px;">Export</button>
			                            </td>
				 
				
				                         
							            <td  class="DISTd" style="text-align:right;width:90px;">
											<button type="button" id="btnPGR01Search" class="historyButtons" style="width:82px;height:31px;margin-right:-65px;">Search</button>
			                            </td>
				
			                           <td  class="DISTd" style="text-align:right;width:90px;">
				                          <button type="button" id="btnReset" class="historyButtons" style="width:82px;height:31px;margin-right:20px;">Reset</button>
						               </td>
				                 </tr>
							</table>

							<div id="divHistoryGrid">
								<table id="PaymentGatewayRecordGrid"></table>
								<div id="GatewayGridPager"></div>
							</div>
									<table class="DISTable">
										<tr class="DISTr">
										
				                            <input type="hidden"  id="moduleCode" value="PGR01">
				                            <input type="hidden" id="actionName" value="">
										</tr>
									</table>

							</div>
						</div>';
	}
}
?>
