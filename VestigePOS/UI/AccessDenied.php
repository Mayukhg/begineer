<?php



class AccessDeniedScreen
{
	function AccessDeniedHtml()
	{
		return  '<div id="divAccessDeniedOuter" style="text-align:center;">
				
					<div id="divAccessDeniedMessage" style="text-align:center;">
						<h1 style="color:red"> Access Denied </h1>
					</div>
				
					<div id="divAccessDeniedImage">
						<img src="/sites/all/modules/VestigePOS/VestigePOS/images/access_denied.png" height=200 width=400></img>
					</div>
				
					<a href=login>Login</a>
								
				</div>';
	}
}


?>