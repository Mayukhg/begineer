<?php

class ReportViewerScreen
{
	function ReportViewerHtml($isMiniBranch)
	{
		
		
		if ($isMiniBranch==0)
		{
		$dynamicText = '';
			$returnedHtml =  '<div class ="divReportViewerOuter">
						<div class="divButtonDiv" style="background-color:#e5f4fb;height:120px;font:bold; text-align:center;">
							<h1>POS Reports</h1>
						</div>
						<div class="divDynamicDiv">
							<div class="divReportParams">
							</div>
							<div class="divReportAction">
								<button id="view" class="reportViewerButton">View</button>
								<button id="close" class="reportViewerButton">Close</button>
							</div>
						</div>
						<div class="divReportViewerAction">
						 	<button class="reportViewerAction" id="stockSalesReport" value="StockistSalesReport">Stockist Sales Report</button>
							<button class="reportViewerAction" id="salesCollectionReport" value="SalesCollectionReport">Sales Collection Report</button>
							<button class="reportViewerAction" id="stockistSalesLog" value="StockistSalesLog">Stockist Sales Log Report</button>
							<button class="reportViewerAction" id="distributorBusinessReport" value="DistributorBusinessReport">Distributor Business Report</button>
							<button class="reportViewerAction" id="distributorAddressLogReport" value="DistributorAddressLabel">Distributor Address Label Report</button>
							<button class="reportViewerAction" id="distributorSingleAddressReport" value="DistributorSingleAddressReport">Distributor Single Address Report</button>
							<button class="reportViewerAction" id="logStatus" value="LogStatusReport">Log Status</button>
							<button class="reportViewerAction" id="orderPrint" value="OrderPrintReport">Order Print</button>
						</div>
						<div class="divReportDisplayArea">
						 	<iframe id="reportIframe"  src="" style="width:99%;height:99%;"></iframe>
						</div>
						
					</div>'	;
			
			return $returnedHtml;
		}
		else 
		{
			$dynamicText = '';
			$returnedHtml =  '<div class ="divReportViewerOuter">
						<div class="divButtonDiv" style="background-color:#e5f4fb;height:120px;font:bold; text-align:center;">
							<h1>POS Reports</h1>
						</div>
						<div class="divDynamicDiv">
							<div class="divReportParams">
							</div>
							<div class="divReportAction">
								<button id="view" class="reportViewerButton">View</button>
								<button id="close" class="reportViewerButton">Close</button>
							</div>
						</div>
						<div class="divReportViewerAction">
						 	<button class="reportViewerAction" id="stockSalesReport" value="StockistSalesReport">Stockist Sales Report</button>
							<button class="reportViewerAction" id="salesCollectionReport" value="SalesCollectionReport">Sales Collection Report</button>
							<button class="reportViewerAction" id="stockistSalesLog" value="StockistSalesLog">Stockist Sales Log Report</button>

							
							
						</div>
						<div class="divReportDisplayArea">
						 	<iframe id="reportIframe"  src="" style="width:99%;height:99%;"></iframe>
						</div>
			
					</div>'	;
				
			return $returnedHtml;
		}
			
			
	}
}	
?>