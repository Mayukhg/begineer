<?php

class ShortInventoryMessageScreen
{
	function shortInventoryMessageHtml()
	{
		return  '<div class="divOuter">
				 
					<div class="divShortInventoryMessage">
						Short Item in Inventory
					</div>
					<div id="divShortInventoryGrid" style="width:400px;clear:both;">
								
						<table id="ShortInventoryGrid"></table>
						<div id="PJmap_ShortInventoryGrid"></div>
									
					</div>
					<div class="divShortInventoryAction">
						<button type="button" id="ok" class="shortInventoryAction">Ok</button>
						<button type="button" id="cancel" class="shortInventoryAction">Cancel</button>
					</div>
				
				
				</div>';
	}
}		

?>
