<?php

class ReturnToVendorScreen
{
	function returnToVendornHtml()
	{
		return  '<div class="divGoodReceiptNoteOuter">

					<div id="divLookUp" title="Purchase order search">
					</div>
				
					<div id="divGoodReceiptNoteTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
				<form id="formSearchTI">
						<div id="DivSearch">
							<div id="divGoodReceiptNote">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Number : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="GRNTSearchInput" type="text"  id="GRNumber" name="GRNumber">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">PO Number : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="GRNTSearchInput" type="text"  id="GRNPONumber" name="GRNPONumber">
						
									     </td>

					
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Code:</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<select style="width: 150px;height:28px; vertical-align: middle;word-wrap: normal" class="requiredList" id="GRNVendorCode" name="GRNVendorCode" style="background-color:white;height: 28px;">

											
													
											</select>
										</td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Status : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="GRNSearchStatus" name="GRNSearchStatus" style="background-color:white;height: 28px;" disabled>
												
													
											</select>
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">From (PO) Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender2" type="text"  id="GRNFromPODate" name="GRNFromPODate">
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">TO (PO) Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input type="text" class="showCalender2" id="GRNToPODate" name="GRNToPODate">
													
											</select>
										</td>
										
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Received By : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										
											<input class="" type="text"  id="GRNReceivedBy" name="GRNReceivedBy">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From (GRN) Date : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="GRNFromGRNDate" name="GRNFromGRNDate">
										</td>
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To (GRN) Date : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="GRNTOGRNDate" name="GRNTOGRNDate">
										</td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Destination Location : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<select class="requiredList" id="GRNDestinationLocaiton" name="GRNDestinationLocation" style="background-color:white;height: 28px;">
													
													
										</select>
						
											
										
										
									</tr>
										
								</table>
				
								<div class="divGRNSearchResetButtons">
									<button type="button" disabled id="btnRCV01Search" class="GRNCreateActionButtons">Search</button>
									<button type="button" id="btnSearchGRNReset" class="GRNSearchResetButtons">Reset</button>
								</div>
				
							</div>
				
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivGRNSearchGrid" style="width:1040px;clear:both;">
								
								<table id="GRNSearchGrid"></table>
								<div id="PJmap_GRNSearchGrid"></div>
									
							</div>
							
						</div>
				</form>
						<div id="DivCreate" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">PO Number : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  placeHolder="Press f4 for listing"  id="returnToVendorGRNNo" name="GRNCreatePONumber" style="width:160px;">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Amendment No. : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreateAmendmentNo" name="GRNCreateAmendmentNo">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">PO Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreatePODate" name="GRNCreatePODate">
										</td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN No : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											
											<input class="DISTSearchInput" type="text"  id="purchaseOrderNo" name="PurchaseOrderNo" style="width:160px;">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Date:  </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNDate" name="GRNDate">
											
											
										</td>
										
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="GRNStatus" name="GRNStatus">	
										</td>
									</tr>
				
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Challan No. : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreateChallanNo" name="GRNCreateChallanNo">
										</td>
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Challan Date. : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="showCalender3" type="text"  id="GRNCreateChallanDate" name="GRNCreateChallanDate">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Code :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreateVendorCode" name="GRNCreateVendorCode">
										</td>
									</tr>
				
				
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Invoice No. : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreateInvoiceNo" name="GRNCreateInvoiceNo">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Invoice Date. : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="showCalender3" type="text"  id="GRNCreateInvoiceDate" name="GRNCreateInvoiceDate">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Name :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreateVendorName" name="GRNCreateVendorName">
										</td>
									</tr>
				
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Invoice Tax : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="GRNViewInvoiceTax" name="GRNViewInvoiceTax">
											
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Transporter Name:  </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="GRNViewTransporterName" name="GRNViewShippingDetails">
											
						
									     </td>
										
										<td rowspan="2" class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Address :</td>
										<td rowspan="2" class="globalTd" style="text-align:left;width:150px;">
											<textarea class="globalTributor_info" id="GRNViewDestinationLocation" rows="4" cols="20" name="GRNViewDestinationLocation"></textarea>
										</td>
									</tr>
									<tr class="globalTr">
										
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Invoice Amount(incl. Tax) :* </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="GRNViewInvoiceAmtInclTax" name="GRNViewInvoiceAmtInclTax">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Gross Weight * : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="GRNViewGrossWeight" name="GRNViewGrossWeight">
											
						
									     </td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Vehicle No. *: </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNViewVehicleNo" name="GRNViewVehicleNo">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GatePass No : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="GRNGatePassNo" name="GRNViewReceivedBy">
						                 </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">No Of Boxes *:</td>
										<td class="globalTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="GRNViewNoOfBoxes" name="GRNViewNoOfBoxes">
										</td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Docket No. *: </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNTransportNo" name="GRNViewVehicleNo">
										</td>
										
									 
									</tr>	
								
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="GRNViewGrid"></table>
								<div id="PJmap_GRNViewGrid"></div>
									
							</div>
							
							<div class="searchResultTopic">
								Item Batch Details
							</div>
				
							<div id="DivGRNBatchDetailsGrid" style="width:1040px;clear:both;">
								
								<table id="GRNBatchDetailsGrid"></table>
								<div id="PJmap_GRNBatchDetailsGrid"></div>
									
							</div>
				
							<div class="divGRNCreateActionButtons">
								<button type="button" id="btnShowAllbatch" class="GRNSearchResetButtons">Show All Batches</button>
								<button type="button" disabled id="btnRCV01Save" class="GRNCreateActionButtons">Save</button>
								<button type="button"  id="btnReset" class="GRNSearchResetButtons">Reset</button>
								<button type="button" disabled id="btnRCV01Close" class="GRNCreateActionButtons">Confirm</button>
								<button type="button" disabled id="btnRCV01Cancel" class="GRNCreateActionButtons">Cancel</button>
								<button type="button" disabled id="btnRCV01Print" class="GRNCreateActionButtons">Print</button>
									<input type="hidden"  id="moduleCode" value="RCV01">
									<input type="hidden" id="actionName" value="">
									<input type="text" hidden class="showCalender" id="todayDate" value="">
				</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>
