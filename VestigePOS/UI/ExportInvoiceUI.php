<?php

class ExportInvoiceScreen
{
	function exportInvoiceHtml()
	{
		return  '<div class="exportInvoiceOutOuter">
				
					<div id="divLookUp">
							
					</div>

					<div id="divTransferOutTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>

					<form id="formSearchTO">
							<div id="DivSearch">
								<div id="divTransferOut">
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">EOI Number : </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="TOINumberSearch" type="text"  id="TOINumber" name="TOINumber">
											
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Export Invoice Number : </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="DISTSearchInput" type="text"  id="TONumber" name="TONumber">
							
										     </td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="TOStatus" name="TOStatus">
														
												</select>
											</td>
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<select class="requiredList" id="TOSourceLocation" name="TOSourceLocation">
														
														
												</select>
							
										     </td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="TODestinationLocation" name="ToDestinationLocation">
														
														
												</select>
											</td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Indented :</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="checkbox" type="checkbox"   id="TOCreateDestinationCSTNo" name="TOCreateDestinationCSTNo">
									
											</td>
											
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From Ex. Inv. Date : </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="fromTOShipDate" name="FromTODate">
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">To Ex. Inv. Date : </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="showCalender" type="text"  id="toTOShipDate" name="ToTODate">
							
										     </td>
											
											
										</tr>
										
											
									</table>
								</div>
								<div class="divBtnAddSearch">
								<button type="button" disabled id="btnTSF02Search" style="margin-left:100px;" class="TOCreateActionButtons">Search</button>
								<button type="button" id="btnReset" class="btnAddSearch">Reset</button>
										
										
								</div>
								<div class="searchResultTopic">
									Search Results
								</div>
								
								<div id="DivTOISearchGrid" style="width:1040px;clear:both;">
									
									<table id="TOSearchGrid"></table>
									<div id="PJmap_TOSearchGrid"></div>
										
								</div>
							</div>
					</form>
						<div id="DivCreate">
							<div id="TransferOutCreateDiv">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">EOI Number :* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="TOINumberSearch" type="text"  id="TOCreateTOINumber" name="TOCreateTOINumber" placeholder="Press F4 for listing">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTOISourceLocation" name="TOCreateTOISourceLocation">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTOIDestinationLocation" name="TOCreateTOIDestinationLocation">
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Exp. Inv. Number : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTONumber" name="TOCreateTONumber">
											
						
									     </td>
										<td rowspan="2" class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Address:  </td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											
											<textarea class="distributor_info" id="TOCreateTOSourceLocation" rows="4" cols="20" name="TOCreateTOSourceLocation"></textarea>
										</td>
										
										<td rowspan="2" class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Address :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="TOCreateDestinationAddress" rows="4" cols="20" name="TOICreateDestinationAddress"></textarea>
										</td>
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Export Reference : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="reference" name="reference" >
										</td>
									
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Other Reference(s) : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="otherrefrence" name="otherrefrence">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Pre-Carriage By :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="Carriage" name="Carriage">
												<option id="-1", value="-1">Select</option>
												<option id="1", value="1">By Sea</option>
												<option id="2", value="2">By Air</option>		
												<option id="3", value="3">By Road</option>
											</select>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Buyer Other than Consignee. :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="Consigne" name="Consignee" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source TIN No. : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceTINNo" name="TOCreateSourceTINNo" >
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Port of loading. :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="Portofloading" name="Portofloading" >
												
									     </td>
										
										<td  class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Total Amount :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="EOTotalAmount" name="EOTotalAmount" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Vessel/Flight No. : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="FlightNo" name="FlightNo">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Port of Discharge.*: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="PortofDischarge" name="PortofDischarge">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Place of Receipt by Pre-Carrier</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="PreCarrier" name="PreCarrier" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Port of Destination:* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="Destination" name="Destination" >
										</td>
								    	<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Terms of Delivery and Payment :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TermDelivery" name="TermDelivery">
						
									     </td>
									
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Delivery :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="Delivery" name="Delivery" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Buyer\'s Order no:* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="BuyerOrderNo" name="BuyerOrderNo">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Buyer Order Date :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input  type="text"  id="BuyerOrderDate" name="BuyerOrderDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Pack Size :*</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="PackSize" name="PackSize" >
										</td>
									</tr>
									<tr class="DISTr">
										
										<td  class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Payment :* </td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input type="text" class="DISTSearchInput" id="EOPayment" name="EOPayment">
														
										
												
									    </td>
										<td  class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Total net weight :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="EOCreateTotalNetWeight" name="EOCreateTotalNetWeight" >
										</td>
										<td  class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Total Invoice Quantity :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOQuantity" name="TOCreateTotalTOQuantity" >
										</td>
									</tr>
									<tr class="DISTr">
										
										<td  class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Status : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateStatus" name="EOCreateStatus" >
												
									     </td>
										<td  class="DISTd" style="text-align:right;width:125px;padding-right:10px;">IEC No.:*</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="EOIECNumber" name="EOIECNumber" >
										</td>
										
									</tr>
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div class="DistributorMasterInformation">
								
							</div>
							<div class="DistributorCurrentRanking">
								
							</div>
							<div class="DistributorCurrentPreviousPvBvComputation">
								<table class="DISTable">
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right;">Item Code : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
			                        	<select class="DISTInput" id="TOCreateItemCode" name="TOCreateItemCode"  style="background-color:white;height: 24px;width:100px;">
										
				        
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Transfer Price: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateTransferPrice" name="TOCreateTransferPrice" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Description : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemDescription" name="TOCreateItemDescription" >
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">UOM: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateUOM" name="TOCreateUOM" >
									     </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Net Weight: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateWeight" name="TOCreateWeight" >
									    </td>
										<td class="DISTd" style="width:100px;text-align:right">Requested Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateRequestedQty" name="TOCreateRequestedQty" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Bucket Name: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIIteBucketName" name="TOIIteBucketName" >
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">Batch No:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemBatchNo" name="TOCreateItemBatchNo" placeholder="Press f4 for listening" >
									    </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Available Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateAvailableQty" name="TOCreateAvailableQty" >
									    </td>
										<td class="DISTd" style="width:100px;text-align:right">Transfer Qty:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateTransferQty" name="TOCreateTransferQty" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Amount: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemAmount" name="TOCreateItemAmount" >
									     </td>
										<td colspan="2" class="DISTd" style="text-align:left;width:100px;">
											
											
									     </td>
										
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Each Cartoon Qty :* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CarttonQty" name="CarttonQty" >
									    </td>
										<td class="DISTd" style="width:100px;text-align:right">Container No.(From - To) :* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="ContainerNo" name="ContainerNo" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Gross Wt.(in grms) :*</td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="GrossWeight" name="TOCreateItemAmount" >
									     </td>
										<td colspan="2" class="DISTd" style="text-align:left;width:100px;">
											<button type="button" id="btnAdd" class="btnAddSearch">Add</button>
											<button type="button" id="clear" class="btnAddSearch">Clear</button>
											
									     </td>
										
										
									</tr>
									
									
								</table>
								
							</div>
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="TOItemGrid"></table>
								<div id="PJmap_TOItemGrid"></div>
									
							</div>
							<div class="divTOCreateActionButtons">
				<button type="button" disabled id="btnPrint" class="TOCreateActionButtons">Packing List</button>
			  					<button type="button" disabled id="btnTSF02Confirm" class="TOCreateActionButtons">Export</button>
							
								<button type="button"  id="btnCreateReset" class="btnAddSearch">Reset</button>
								<button type="button" disabled id="btnTSF02Print" class="TOCreateActionButtons">Print</button>
								<input type="hidden"  id="moduleCode" value="TSF02">
									<input type="hidden" id="actionName" value="">
							</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>
