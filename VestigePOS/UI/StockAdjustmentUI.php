<?php

class StockAdjustmentScreen
{
	function stockAdjustmentHtml()
	{
		return  '<div class="divStockAdjustmentOuter">

					<input type="hidden" id="moduleCode" value="INV03">
				
					<div id="divLookUp" title="Manufacture Batch">
							
					</div>
				
					<div id="divStockAdjustmentTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
						<div id="DivSearch">
							<div id="divStockAdjustmentAction">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Seq. No : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="seqNo" name="SeqNo">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="location" name="Location" style="background-color:white;height: 28px;">
													
													
											</select>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="status" name="Status" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From (Initial) Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="fromInitialDate" name="FromInitialDate">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">To (Initial) Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="toInitialDate" name="ToInitialDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Approved/Rejected by :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="users" name="users" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
									</tr>
									<tr class="DISTr">
										<td  class="DISTd" style="text-align:right;width:100px;padding-right:10px;"></td>
										<td class="DISTd" style="text-align:left;width:150px;">
											
										</td>
										<td  class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											
						
									     </td>
										
										<td colspan="2" class="DISTd" style="text-align:left;width:150px;">
											<button type="button" id="btnINV03Search" class="StockAdjustmentActionButtons" style="margin-left:150px;">Search</button>
											<button type="button" id="btnReset" class="btnAddSearch">Reset</button>
										</td>
									</tr>
								</table>
							</div>
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivSASearchGrid" style="width:1040px;clear:both;">
								
								<table id="SASearchGrid"></table>
								<div id="PJmap_SASearchGrid"></div>
									
							</div>
						</div>
						<div id="DivCreate">
							<div id="distributorInfoSearchAction">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location :* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="SACreateLocation" name="SACreateLocation" style="background-color:white;height: 28px;">
													<option value="0">Select</option>
													
											</select>
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location Code : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="SACreateLocationCode" name="SACreateLocationCode">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Location Address :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="SACreateLocationAddress" rows="4" cols="20" name="SACreateLocationAddress"></textarea>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Adjustment No : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											
											<input class="DISTSearchInput" type="text"  id="SACreateAdjustmentNo" name="SACreateAdjustmentNo">
									     </td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Initial Date:*  </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="SACreateInitialDate" name="SACreateInitialDate">
										</td>
										
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;"></td>
										
									</tr>
									
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Initiated By : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="SACreateInitiatedBy" name="SACreateInitiatedBy">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Approved/Rejected by : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="SACreateApprovedRejectedBy" name="SACreateApprovedRejectedBy">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="SACreateStatus" name="SACreateStatus" style="width:75px;">
										</td>
									</tr>
			       	<tr class="DISTr">
																<td class="DISTd" style="width:100px;text-align:right">From Bucket:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<select class="requiredList" id="SACreateFromBucket" name="SACreateFromBucket" style="background-color:white;height: 28px ; width: 150px;">
													
													
											</select>
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">To Bucket: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<select class="requiredList" id="SACreateToBucket" name="SACreateToBucket" style="background-color:white;height: 28px;">
													
													
											</select>
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Reason Code:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<select class="requiredList" id="SACreateReasonCode" name="SACreateReasonCode" style="background-color:white;height: 28px;">
													
													
											</select>
									     </td>
				
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Export Item : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input type="checkbox" id="SACreateExportItem"  name="SACreateExportItem">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Batch Adjustment : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input type="checkbox" id="SABatchAdjust"  name="SACreateExportItem">
						
									     </td>
										
									</tr>
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div class="divStockAdjustmentItemDetails">
								<table class="DISTable">
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right;">Item Code*: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateFromItemCode" name="SACreateFromItemCode" placeholder="press f4 for listing">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Desc: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateFromItemDesc" name="SACreateFromItemDesc" style="width:100px;">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Batch No :* </td>
										<td class="DISTd" style="text-align:left;">
											<input class="DISTInput" type="text"  id="SACreateFromBatchNo" name="SACreateFromBatchNo" style="width:100px;" placeholder="press f4 for listing">
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">Weight: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateFromWeight" name="SACreateFromWeight" style="width:100px;">
									     </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right;">To Item Code*: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateToItemCode" name="SACreateToItemCode" placeholder="press f4 for listing">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Desc: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateToItemDesc" name="SACreateToItemDesc" style="width:100px;">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">To Batch No :* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateToBatchNo" name="SACreateToBatchNo " placeholder="press f4 for listing" style="width:100px;">
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">To Batch Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateToWeight" name="SACreateToWeight" style="width:100px;">
									     </td>
										
									</tr>
									<tr class="DISTr">
									<td class="DISTd" style="width:100px;text-align:right">Available Qty:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateAvailableQty" name="SACreateAvailableQty" style="width:100px;">
									     </td>	
				       			  <td class="DISTd" style="width:100px;text-align:right">Allocated Qty:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateAllocatedQty" name="SACreateAllocatedQty" style="width:100px;">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Approved Qty:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateApprovedQty" name="SACreateApprovedQty" style="width:100px;">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Intiated Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateInitiatedQty" name="SACreateInitiatedQty" style="width:100px;">
									     </td>
										
										
									</tr>
									
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Reason Desc.: </td>
										<td colspan="8" class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="SACreateReason" name="SACreateReason" style="width:800px;">
									     </td>
										
				
									</tr>
									
									
								</table>
								<div class="divAddClearButtons">
										<button type="button" id="SACreateAdd" class="btnAddSearch">Add</button>
										<button type="button" id="SACreateClear" class="btnAddSearch">Clear</button>
								</div>
							</div>
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="TOIItemGrid"></table>
								<div id="PJmap_TOIItemGrid"></div>
									
							</div>
							<div class="divStockAdjustmentActionButtons">
								<button type="button" id="btnINV03Initiate" class="StockAdjustmentActionButtons">Initiate</button>
								<button type="button" id="btnINV03Approve" class="StockAdjustmentActionButtons">Approve</button> 
								<button type="button" id="btnINV03Save" class="StockAdjustmentActionButtons">Save</button>
								<button type="button" id="btnCreateReset" class="StockAdjustmentActionButtons">Reset</button>
								<button type="button" id="btnINV03Cancel" class="StockAdjustmentActionButtons" >Cancel</button>
								<button type="button" id="btnINV03Print" class="btnAddSearch">Print</button>
								<button type="button" id="btnINV03Reject" class="StockAdjustmentActionButtons" disabled>Reject</button>
							</div>
						</div>
						
						
					</div>
				</div>';
	}
}		


?>