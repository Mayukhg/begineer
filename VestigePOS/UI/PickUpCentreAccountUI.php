<?php

class PickUpCentreAccountScreen
{
	function PickUpCentreAccountHtml()
	{
		return  '<div class="divPickUpCentreAccOuter">
				
				
						<input type="hidden" value="PUC01" id="moduleCode">
				
						<div class="PUCAccTopic">
							Pick-Up-Centre Accounts
							
						</div>
				 	
						<table class="CommonTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Location Code :* </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="requiredField" type="text"  id="PUCAccLocationCode" name="PUCAccLocationCode" disabled>
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Pick Up Centre :* </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											
											<select class="requiredList" id="PUCAccPickUpCentre" name="PUCAccPickUpCentre" style="vertical-align: middle; word-wrap: normal;">
													<option value="0">Select</option>
													
											</select> 
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Transaction No.:* </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="requiredField" type="text"  id="PUCAccTransactionNo" name="PUCAccTransactionNo">
										</td>
									</tr>
									
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Date :* </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="PUCAccDate" name="PUCAccDate">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Payment Mode :* </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											
											<select class="requiredList" id="PUCAccPaymentMode" name="PUCAccPaymentMode">
													<option value="0">Select</option>
													<option value="1">Cash</option>
													<option value="2">Credit</option>
											</select> 
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Amount :*</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="addRequiredField" type="text"  id="PUCAccAmount" name="PUCAccAmount">
										</td>
									</tr>
										
									
						</table>
						<div class="divPUCAccAddClearButtons">
							<button type="button" class="PUCAccAddClearButtons" id="btnPUCAccClear">Clear</button>
							<button type="button" class="PUCAccountAction" id="btnPUC01Search">Search</button>
							<button type="button" class="PUCAccAddClearButtons" id="btnPUCAccAdd">Add</button>
							
						</div>
						<div class="PUCAccDepositTitle">
							PUC-Account Deposit
						</div>
						<div id="divPUCAccDepositGrid">
								
							<table id="PUCAccDepositGrid"></table>
							<div id="pjmap_PUCAccDepositGrid"></div>
									
						</div>
						<div id="divPUCAccGrid">
								
							<table id="PUCAccGrid"></table>
							<div id="pjmap_PUCAccGrid"></div>
									
						</div>
						<div id="divPUCRemarks" title="Enter reason for reversal">
							
							<textarea class="distributor_info" placeholder="Remarks" id="transactionReverseRemarks" rows="3" cols="20" style="width: 560px; height:100px;margin-top: 5px;" name="ClaimRemarks">
							</textarea>
							<div class="divPUCRemarksButton">
								<button class="btnPUCRemarks" id="btnPUCRemarks">OK</button>
							</div>
				
						</div>
						<div class="divPUCAccountAction">
							<button type="button" class="PUCAccountAction" id="btnPUCAccExit">Exit</button>
							<button type="button" class="PUCAccountAction" id="btnPUCAccReset">Reset</button>
							<button type="button" class="PUCAccountAction" id="btnPUC01Save">Save</button>
							<button type="button" class="PUCAccountAction" id="btnPUC01Reverse" disabled>Reverse</button>
							
						</div>
				
				</div>';
	}
}	
?>