<?php
class AuthenticationScreen
{
	function AuthenticationHtml()
	{
		return  '<div class="outerdiv">
				
					<div id="divLookUp" title="Composite Item Search">
							
					</div>
				
					<div id="loginScreenPopUp" title="Vestige-Login">
					
						<div class="divVestigeLoginImage">
							<img src="/sites/all/modules/VestigePOS/VestigePOS/images/vestige-logo.png" height="80" width="80">
						</div>
						<table class="CommonTable">
							
				
							<tr class="CommonTr trUsername">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Username/Distributor Id :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="username" name="username" style="width:200px;">
									
							    </td>	
								
							</tr>
							<tr class="CommonTr trPassword">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Password :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="password"  id="password" name="password" style="width:200px;">
									
							    </td>	
								
							</tr>
				
							<tr class="CommonTr trLocation" id="locationOption">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Location : </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
				
									
									
									<Select class="requiredList" id="loginLocation" name="LoginLocation" style="width:212px;height:28px;">
				
									</select> 
									
							    </td> 	
								
							</tr>
							
							
							<tr class="CommonTr">
									
								
								<td colspan="2" class="CommonTd" id="errorMsg" style="text-align:center;width:200px;padding-right:10px;"> </td>
									
								
							</tr>
				
						</table>
						<div class="innerdiv2">
					
							<button type="button" id="ok" class="AuthenticationButtons">Login</button>
				
							<button type="button" id="login" class="AuthenticationButtons">Proceed</button>
				
						</div>
						<div class="divTypeOfPOS" hidden="true">
							<input type="radio" class="typeOfPOSSelectionOption" id="normalPOSSelection" name="TypeOfPOS" value="1" style="margin-left:10px;" checked="checked">Normal POS<input type="radio" class="typeOfPOSSelectionOption" id="SF9POSSelection" name="TypeOfPOS" value="2" style="margin-left:10px;">SF9 POS
							
						</div>
						<div class="divForgetPassword" hidden="true">
							<a  href="javascript::void();" id="changePassword">Change password</a> 
							
						</div>
						
					</div>
				
					<div id="divVestigeMLM">
						
						<table style="margin:0px;">
							<tr>
								<td style="color:white;">Welcome :</td><td style="color:white;" id="loggedInUserName"></td>
								<td style="color:white;">Location :</td><td style="color:white;" id="loggedInLocation"></td>
								<td style="color:white;">Date :</td><td style="color:white;" id="loggedInUserTime"></td>
							</tr>
						</table>
				
						
					</div>
				
					<div id="forgetPasswordPopUp" title="Change password" style="background-color:#0375bb;" hidden="true">
							
							<table class="CommonTable">
							
				
							<tr class="CommonTr trUsername">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Distributor/PUC :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="uDistributorId" name="uDistributorId" style="width:200px;">
									
							    </td>	
								
							</tr>
							<tr class="CommonTr trPassword">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">New Password :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="password"  id="newPassword" name="newPassword" style="width:200px;">
									
							    </td>	
								
							</tr>
				
							
				
						</table>
				
							<div class="divLookUpButtons">
	
								<button type="button" class="lookUpActionButtons" id="btnChangePassword">Change Password</button>
									
							</div>
				
					</div>
				
				
				</div>';
	}
}
?>