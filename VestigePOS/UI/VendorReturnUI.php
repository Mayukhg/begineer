<?php

class VendorReturnScreen
{
	function VendorReturnHtml()
	{
		return  '<div class="divGoodReceiptNoteOuter">

					<div id="divLookUp" title="GRN search">
					</div>
				
					<div id="divGoodReceiptNoteTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
				<form id="formSearchTI">
						<div id="DivSearch">
							<div id="divGoodReceiptNote">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Return Number : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="GRNTSearchInput" type="text"  id="VRNumber" name="VRNumber">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Number : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="GRNTSearchInput" type="text"  id="VRGRNNumber" name="VRGRNNumber">
						
									     </td>

					
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Code:</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<select style="width: 150px;height:28px; vertical-align: middle;word-wrap: normal" class="requiredList" id="GRNVendorCode" name="GRNVendorCode" style="background-color:white;height: 28px;">

											
													
											</select>
										</td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Return Status : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="VRSearchStatus" name="VRSearchStatus" style="background-color:white;height: 28px;">
												
													
											</select>
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">From (GRN) Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender2" type="text"  id="VRFromGRNDate" name="VRFromGRNDate">
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">TO (GRN) Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input type="text" class="showCalender2" id="VRToGRNDate" name="VRToGRNDate">
													
											</select>
										</td>
										
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Created By : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										
											<input class="" type="text"  id="VRCreatedBy" name="VRCreatedBy">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From (VR) Date : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="VRFromVRDate" name="VRFromVRDate">
										</td>
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To (VR) Date : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="VRTOVRDate" name="VRTOVRDate">
										</td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Destination Location : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<select class="requiredList" id="GRNDestinationLocaiton" name="GRNDestinationLocation" style="background-color:white;height: 28px;">
													
													
										</select>
						
											
										
										
									</tr>
										
								</table>
				
								<div class="divGRNSearchResetButtons">
									<button type="button" disabled id="btnRET01Search" class="GRNCreateActionButtons">Search</button>
									<button type="button" id="btnSearchGRNReset" class="GRNSearchResetButtons">Reset</button>
								</div>
				
							</div>
				
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivGRNSearchGrid" style="width:1040px;clear:both;">
								
								<table id="GRNSearchGrid"></table>
								<div id="PJmap_GRNSearchGrid"></div>
									
							</div>
							
						</div>
				</form>
						<div id="DivCreate" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Number : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  placeHolder="Press f4 for listing"  id="VrCreateGRNNumber" name="VrCreateGRNNumber" >
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">GRN Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="GRNCreatePODate" name="GRNCreatePODate">
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">GRN Status :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="GRNStatus" name="GRNStatus">	
										</td>
									
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">PO No : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											
											<input class="DISTSearchInput" type="text"  id="PONo" name="PONo" >
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">PO Date:  </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="PODate" name="PODate">
											
											
										</td>
										
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Return Status : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="VrStatus" name="VrStatus">
										</td>
										
									</tr>
				
									<tr class="globalTr">
									
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Return No : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="VrReturnNo" name="VrReturnNo">						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Date : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="VRShipDate" name="VRShipDate">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Code :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="GRNCreateVendorCode" name="GRNCreateVendorCode">
										</td>
									</tr>
				
				
									<tr class="globalTr">
									
									
									     <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Amount(incl. Tax) :* </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="VrTotalAmount" name="VrTotalAmount">						
									     </td>
									    <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Other Handling Charge : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="HandlingCharge" type="text"  id="HandlingCharge" name="HandlingCharge">
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Name :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="GRNCreateVendorName" name="GRNCreateVendorName">
										</td>
									</tr>
				
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Debit Amount*: </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="VRCreateDebitAmt" name="VRCreateDebitAmt">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Returned Quantity : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="VrCreateTotalQty" name="VrCreateTotalQty">
											
						
									     </td>
									
										
										<td rowspan="2" class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Vendor Address :</td>
										<td rowspan="2" class="globalTd" style="text-align:left;width:150px;">
											<textarea class="globalTributor_info" id="GRNViewDestinationLocation" rows="4" cols="20" name="GRNViewDestinationLocation"></textarea>
										</td>
									</tr>
									<tr class="globalTr">	
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Transporter Name*: </td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="VrShipDetail" name="VrShipDetail">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Remarks. *: </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="VrRemarks" name="VrRemarks">
										</td>
										
									</tr>
									<tr class="globalTr">
									<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">No Of Boxes* : </td>
									<td class="globalTd" style="text-align:left;width:150px;">
									<input class="DISTSearchInput" type="text"  id="NoOfBoxes" name="NoOfBoxes">
									</td>	
									<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Gross Weight : </td>
									<td class="globalTd" style="text-align:left;width:150px;">
									<input class="DISTSearchInput" type="text"  id="ProductWeight" name="ProductWeight">
									</td>
										
										
									 
									</tr>	
								
								</table>
								

							</div>							
							
							<div class="searchResultTopic">
								Item Batch Details
							</div>
				
							<div id="DivGRNBatchDetailsGrid" style="width:1040px;clear:both;">
								
								<table id="GRNBatchDetailsGrid"></table>
								<div id="PJmap_GRNBatchDetailsGrid"></div>
									
							</div>
				
							<div class="divGRNCreateActionButtons">
								<button type="button" disabled id="btnRET01Print" class="GRNCreateActionButtons">Debit Note</button>								
								<button type="button"  id="btnReset" class="GRNSearchResetButtons">Reset</button>
								<button type="button" disabled id="btnRET01Close" class="GRNCreateActionButtons">Confirm</button>
								<button type="button" disabled id="btnRET01Cancel" class="GRNCreateActionButtons">Cancel</button>
								<button type="button" disabled id="btnRET01Save" class="GRNCreateActionButtons">Save</button>
									<input type="hidden"  id="moduleCode" value="RET01">
									<input type="hidden" id="actionName" value="">
									<input type="text" hidden class="showCalender" id="todayDate" value="">
				</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>
