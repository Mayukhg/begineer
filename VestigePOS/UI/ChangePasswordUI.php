<?php
class ChangePasswordScreen
{
	function changePasswordHTML()
	{
		return  '<div class="outerdiv">
				
					<div id="divLookUp" title="Composite Item Search">
							
					</div>
				
					<div id="loginScreenPopUp" title="Vestige-Login">
					
						<div class="divVestigeLoginImage" style="text-align:center;">
							<img src="/sites/all/modules/VestigePOS/VestigePOS/images/vestige-logo.png" height="80" width="80">
						</div>
						<table class="CommonTable">
							
				
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">New Password :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="password"  id="txtNewPassword" name="NewPassword" style="width:200px;">
									
							    </td>	
								
							</tr>
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Re-enter Password :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="password"  id="txtReEnterPassword" name="ReEnterPassword" style="width:200px;">
									
							    </td>	
								
							</tr>
				
							
							
							
							<tr class="CommonTr">
									
								
								<td colspan="2" class="CommonTd" id="errorMsg" style="text-align:center;width:200px;padding-right:10px;"> </td>
									
								
							</tr>
				
						</table>
						<div class="innerdiv2">
					
							<button type="button" id="btnChangePassword" class="AuthenticationButtons">Change Password</button>
				
						</div>
						
						
					</div>
				
					<div id="divVestigeMLM">
						
						<table style="margin:0px;">
							<tr>
								<td style="color:white;">Welcome :</td><td style="color:white;" id="loggedInUserName"></td>
								<td style="color:white;">Location :</td><td style="color:white;" id="loggedInLocation"></td>
								<td style="color:white;">Date :</td><td style="color:white;" id="loggedInUserTime"></td>
							</tr>
						</table>
				
						
					</div>
					
				
				
				</div>';
	}
}
?>