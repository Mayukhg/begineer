<?php

class VouchersManagementScreen
{
	function VouchersManagementHTML()
	{ 
		
		return  '<div class="divGoodReceiptNoteOuter">

					<div id="divLookUp" title="Item Search">
					</div>
				
					<div id="divGoodReceiptNoteTab">
						<ul>
							
							<li><a href="#DivCreate">BonusVoucher</a></li>
							<li><a href="#Gift">GiftVoucher</a></li>
							<li><a href="#ActivateJoining">ActivateJoining</a></li>
							<li><a href="#sendmessage">Send Password</a></li>
							
						</ul>
				
						<div id="DivCreate" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Distributor id : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="DistributoridBonus" name="DistributoridBonus" style="width:160px;">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Bonus Voucher No : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="Bonus" name="Bonus">						
									    <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Expiry Date : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="BonusExpiry" name="BonusExpiry" style="width:160px;">
						                </td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Bonus Amount : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="BonusAmount" name="BonusAmount" style="width:160px;">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Created Date : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="BonusCreated" name="BonusCreated" style="width:160px;">
						                </td>
									</tr>
													
								</table>
							</div>
				
								<div class="divGRNCreateActionButtons">
								<button type="button" id="BonusMessage" class="GRNCreateActionButtons Voucher">Send Message</button>
								<button type="button" id="BonusSearch" class="GRNCreateActionButtons Voucher">Search</button>
								<button type="button" id="BonusSave" class="GRNCreateActionButtons Voucher">Save</button>
								<button type="button"  id="btnReset" class="GRNSearchResetButtons Voucher">Reset</button>
								<input type="hidden"  id="moduleCode" value="VOD01">
								<input type="hidden" id="actionName" value="">
								</div>

								<div class="searchResultTopic">
								Vouchers Search Result
							    </div>
				
				                <div id="divHistoryGrid">
								<table id="VouchersManagementGrid"></table>
								<div id="VouchersManagementPager"></div>
							    </div>
				
						</div>
				
				<div id="Gift" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Distributor id : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="DistributoridGift" name="DistributoridGift" style="width:160px;">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Gift Voucher No : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="GiftVoucherNo" name="GiftVoucherNo">						
									    <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Expiry Date : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="GiftExpiry" name="GiftExpiry" style="width:160px;">
						                </td>
									</tr>

									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Min. Buy Amount : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="GiftAmount" name="GiftAmount" style="width:160px;">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Created Date : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="GiftCreated" name="GiftCreated" style="width:160px;">
						                </td>
									</tr>
				
								</table>
							</div>
				
								<div class="divGRNCreateActionButtons">
								<button type="button" id="GiftMessage" class="GRNCreateActionButtons Voucher">Send Message</button>
								<button type="button" id="GiftSearch" class="GRNCreateActionButtons Voucher">Search</button>
								<button type="button" id="GiftSave" class="GRNCreateActionButtons Voucher">Save</button>
								<button type="button"  id="btnResetgift" class="GRNSearchResetButtons Voucher">Reset</button>
								<input type="hidden"  id="moduleCode" value="VOD01">
								<input type="hidden" id="actionName" value="">
								
									</div>
							<div class="searchResultTopic">
								Vouchers Search Result
							    </div>
				
				                <div id="divHistoryGrid">
								<table id="GiftManagementGrid"></table>
								<div id="GiftManagementPager"></div>
							    </div>
				
				
							
						</div>
				
				<div id="ActivateJoining" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Distributor id : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="Distributorid" name="Distributorid" style="width:160px;">
										</td>
				
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Distributor Name : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="DistributorName" name="DistributorName" style="width:160px;">
										</td>
				
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Contact No : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="Contact" name="Contact" style="width:160px;">
										</td>
									</tr>

										<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:150px;padding-right:10px;">Is Eligible For Repurchase : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input  type="checkbox"  id="joining" name="joining">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Date of Joining : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="Joining" name="Joining" style="width:160px;">
										</td>
									</tr>
													
								</table>
							</div>
								
								<div class="divGRNCreateActionButtons">
								<button type="button"  id="OrderActivate" class="GRNSearchResetButtons Voucher">Confirm Orders</button>
								<button type="button" id="DistributorSearch" class="GRNCreateActionButtons Voucher">Search</button>
								<button type="button" id="DistributorSave" class="GRNCreateActionButtons Voucher">Joining Activate</button>
								<button type="button"  id="DistReset" class="GRNSearchResetButtons Voucher">Reset</button>
								<input type="hidden"  id="moduleCode" value="VOD01">
								<input type="hidden" id="actionName" value="">
								</div>
								<div class="searchResultTopic">
								Orders Search Result
							    </div>
				
				                <div id="divHistoryGrid">
								<table id="OnlineOrdersGrid"></table>
								<div id="OnlineOrdersGridPager"></div>
							    </div>
				
						</div>
				
				<div id="sendmessage" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Distributor id : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="Distributoridpass" name="Distributoridpass" style="width:160px;">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Contact No : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="MobNo" name="MobNo" style="width:160px;">
										</td>
									</tr>

				
								</table>
							</div>
				
								<div class="divGRNCreateActionButtons">
								<button type="button" id="sendpass" class="GRNCreateActionButtons Voucher">Send Password</button>
								<button type="button"  id="DistPassReset" class="GRNSearchResetButtons Voucher">Reset</button>
								<input type="hidden"  id="moduleCode" value="VOD01">
								<input type="hidden" id="actionName" value="">
								
									</div>

						</div>
				
						
						
					</div>
				</div>';
	}
}


?>
