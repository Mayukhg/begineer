<?php

class POSReportParamsUI{
	public $SALES_COLLECTION_REPORT = "salesCollectionReport";
	public $STOCKIST_SALES_REPORT = "stockSalesReport";
	public $STOCKIST_SALES_LOG = "stockistSalesLog";
	public $DISTRIBUTOR_BUSINESS_REPORT = "distributorBusinessReport";
	public $DISTRIBUTOR_SINGLE_ADDRESS_REPORT = "distributorSingleAddressReport";
	public $DISTRIBUTOR_ADDRESS_LABEL_REPORT = "distributorAddressLogReport";
	public $ORDER_PRINT_REPORT = "orderPrint";
	public $LOG_ORDERS_STATUS_REPORT = "logStatus";
	
	function getParameterForThis($whichReport){
		$reporParams = "";
		
		switch ($whichReport){
			case $this->DISTRIBUTOR_ADDRESS_LABEL_REPORT:
				$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Distributor ID :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="isDistributor" id="FromDistributorID" name="FromdistributorId" maxlength="8" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="DistributorAddressLabelReport"/>
							</form>
							</div>'	;
				break;
			case $this->DISTRIBUTOR_BUSINESS_REPORT:
				
				$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Distributor ID :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="DistributorId" name="DistributorID" maxlength="8"/>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Downlines From :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="DownlinesFromId" name="FromDate"  />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Downlines To :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DownlinesToId" name="ToDate" />
									     </td>
									</tr>
								</table>
								<input type="hidden" name="WhichReport" value="DistributorBusinessReport"/>
							</form>
							</div>'	;
				break;
			case $this->DISTRIBUTOR_SINGLE_ADDRESS_REPORT:
				$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
									<form class="ReportForm" action="" style="width:100%;height:100%;">
										<table class="ReportTable" style="width:100%;">
											<tr class="DISTr">
											<td class="DISTd" style="width:140px;text-align:right;">
												Distributor ID :
											</td>
											<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="isDistributor" id="DistributorId" name="FromdistributorId" maxlength="8"/>
											</td>
											</tr>
											</table>
										<input type="hidden" name="WhichReport" value="DistributorSingleAddressReport"/>
									</form>
							</div>';
				break;
			case $this->SALES_COLLECTION_REPORT:
				$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>
		
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocationId">
						                    <option id="-1" " value="-1">All</option>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Username :
										</td>
		
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="UsernameId" name="UserId" >
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>
		
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>
		
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="SalesCollectionReport"/>
							</form>
							</div>'	;
				
				break;
			case $this->STOCKIST_SALES_LOG:
				$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocationId">
											<option id="-1" " value="-1">All</option>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="DateFrom" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="DateTo" />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Log No :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="LogNoId" name="LogNo" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="StockistSalesLog"/>
							</form>
							</div>'	;
				break;
			case $this->STOCKIST_SALES_REPORT:
				$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PClocationId">
						                      <option id="-1" " value="-1">All</option>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Log No :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="LogNoId" name="logNo" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="StockistSalesReport"/>
							</form>
							</div>'	;
				break;
				
				case $this->ORDER_PRINT_REPORT:
					$reportParams = '<div class="OrderPrintOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">

										<td class="DISTd" style="width:140px;text-align:right;">
											Business Month :
										</td>
				
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="businessMonth" name="BusinessMonthSelect">
						                    
											</select>
									     </td>
							
										
										<input type="hidden" id="businessMonthValue" name="BusinessMonth"/>
							
									</tr>
									
							</table>
									<input type="hidden" name="WhichReport" value="OrderPrintReport"/>
							</form>
							</div>'	;
					break;
					
					case $this->LOG_ORDERS_STATUS_REPORT:
						$reportParams = '<div class="OrderPrintOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
					
										<td class="DISTd" style="width:140px;text-align:right;">
											Log No. :
										</td>
					
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text"  id="txtLogNo" name="LogNo" />
									     </td>
								
										<td class="DISTd" style="width:140px;text-align:right;">
											Date From :
										</td>
					
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>
					
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" />
									     </td>
									</tr>
					
							</table>
									<input type="hidden" name="WhichReport" value="LogStatusReport"/>
							</form>
							</div>'	;
						break;
		}
		
		return $reportParams;
	}
}
?>