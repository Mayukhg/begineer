<?php
class CourierDetailScreen
{
	function CourierDetailHTML()
	{
		return  '<div class="outerdiv">
				
					<div id="loginScreenPopUp" title="Courier Details">
					
						<form id="courierDetailsForm">
				
				
						  <table class="CommonTable">
							
				
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Reference Log :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="referenceLog" name="ReferenceLog" style="width:200px;" disabled>
									
							    </td>	
								
							</tr>
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Docket No. :  </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="docketNo" name="DocketNo" style="width:200px;" maxlength="50">
									
							    </td>	
								
							</tr>

			         
							<tr class="CommonTr" >

									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Courier company name : </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="courierCompanyName" name="CourierCompanyName" style="width:200px;" maxlength="300">
									
							    </td> 	
								
							</tr>
							
							
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Number of boxes : </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="noOfBoxes" name="NoOfBoxes" style="width:200px;">
									
							    </td>
								
									
								
							</tr>
							<tr class="CommonTr">
									
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Weight in gram : </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="weightInGram" name="WeightInGram" style="width:200px;">
									
							    </td>
				
								
							</tr>
							<tr class="CommonTr">
								<td class="CommonTd" style="text-align:right;width:100px;padding-right:10px;">Items weight : </td>
								<td class="CommonTd" style="text-align:left;width:200px;">
									<input type="text"  id="itemsWeight" name="ItemsWeight" style="width:200px;" disabled>
									
							    </td>
							</tr>
				
						</table>
					  </form>	
				
						<div class="divCourierDetailActionButtons">
					
							<button type="button" id="save" class="courierDetailsActionButtons">Save</button>
				
							<button type="button" id="Cancel" class="courierDetailsActionButtons">Cancel</button>
				
						</div>
						
					</div>
				
					
				
				
				</div>';
	}
}
?>
