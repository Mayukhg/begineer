<?php

class LookUpScreens
{
	function TOILookUpScreenHtml()
	{
		return  '<div class="divTOILookUpOuter">
						<div class="TOILookUpTopic">
						
							
						</div>
				 	
						<form id="TOILookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">TOI Number : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="TOILookUpTOINumber" name="TOINumber" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Indentised : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input type="checkbox"  name=" TOILookUpIndentised" id="TOILookUpIndentised">
											</td>
											
										</tr>
										
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From (Creation) Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="TOILookUpFromCreationDate" name="FromTOICreationDate">
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To (Creation) Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												
												 <input class="showCalender" type="text"  id="TOILookUpToCreationDate" name="ToTOICreationDate">
											</td>
											
										</tr>
							
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From TOI Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="TOILookUpFromTOIDate" name="FromTOIDate" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To TOI Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="TOILookUpToTOIDate" name="ToTOIDate"> 
											</td>
											
										</tr>
											
										
							</table>
						</form>
						<div class="divLookUpButtons">
							<div class="LookUpButtonsWrapDiv">
							<button type="button" class="LookUpButtonsWithTabOrder" id="btnTOILookUpSearch">Search</button>
							<button type="button" class="LookUpButtonsWithTabOrder" id="btnTOILookUpReset">Reset</button>
							</div>
						</div>
						
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divTOISearchGrid" style="width:560px;clear:both;">
									
							<table id="TOISearchGrid"></table>
							<div id="PJmap_TOISearchGrid"></div>
										
						</div>
				
				
				</div>';
	}
	
	/*
	 * function provide TO look up screen.
	 */
	function TOLookUpScreenHtml()
	{
		return  '<div class="divTOLookUpOuter">
						<div class="TOLookUpTopic">
						
						</div>
	
						<form id="TOLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">TO Number : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="TOLookUpTONumber" name="TONumber" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Indentised : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input type="checkbox"  name=" TOLookUpIndentised" id="TOLookUpIndentised">
											</td>
						
										</tr>
	
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From (TO) Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="TOILookUpFromCreationDate" name="FromTODate">
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To (TO) Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
	
												 <input class="showCalender" type="text"  id="TOILookUpToCreationDate" name="ToTODate">
											</td>
						
										</tr>
				
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From Ship Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="TOILookUpFromTOIDate" name="FromTOShipDate" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To Ship Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="TOILookUpToTOIDate" name="ToTOShipDate">
											</td>
						
										</tr>
						
	
							</table>
						</form>
						<div class="divLookUpButtons">
							<div class="LookUpButtonsWrapDiv">
							<button type="button" class="LookUpButtonsWithTabOrder" id="btnTOLookUpSearch">Search</button>
							<button type="button" class="LookUpButtonsWithTabOrder" id="btnTOLookUpReset">Reset</button>
							</div>
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
					
							<table id="TOSearchGrid"></table>
							<div id="PJmap_TOSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	
	/*
	 * function provide Composite Item Code Look screen.
	*/
	function compositeItemLookUpScreenHtml()
	{
		return  '<div class="divTOLookUpOuter">
						<div class="TOLookUpTopic">
							
	
						</div>
	
						<form id="CompositeItemLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Composite Item Code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="compositeItemInfo" type="text"  id="compositeItemCode" name="CompositeItemCode" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Composite Item Name : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="compositeItemInfo" type="text"  id="compositeItemName" name="CompositeItemName" >
											</td>
										
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnCompositeItemLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnCompositeItemLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
			
							<table id="compositeItemSearchGrid"></table>
							<div id="PJmap_compositeItemSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	
	
	function ItemLookUpScreenHtml()
	{
		return  '<div class="divItemLookUpOuter">
						<div class="informationTopic">
							
						</div>
				 		<form id="itemLookUpForm">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Item Name : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="itemLookUpItemName" name="ItemLookUpItemName">
											</td>
								<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Item Code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="itemLookUpItemCode" name="itemLookUpItemCode">
											</td>
										</tr>	
							
							</table>
						</form>
						<div class="divSearchResetButtons">
							
							<button type="button" class="lookUpActionButtons" id="btnItemSearchLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnItemSearchLookUpSearch">Search</button>
							
						</div>
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divItemSearchGrid" clear:both;">
									
							<table id="itemSearchGrid"></table>
							<div id="PJmap_ItemSearchGrid"></div>
										
						</div>
				
				</div>';
	}
	
	
	function locationLookUpScreenHtml()
	{
		return  '<div class="divItemLookUpOuter">
						<div class="informationTopic">
							
						</div>
				 		<form id="itemLookUpForm">
							<table class="DISTable">
										<tr class="globalTr">
									<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Location Name : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="locatioName" name="statecode">
											
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Location Code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="locationCode" name="LocationCode">
											</td>
										</tr>
									<tr class="globalTr">	
										
									<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">State Code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="requiredField" type="text"  id="statecode" name="statecode">
											
											</td>
				<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;"></td>
											<td class="globalTd" style="text-align:left;width:150px;">
											
											</td>
												
										</tr>
	
							</table>
						</form>
						<div class="divSearchResetButtons">
				
							<button type="button" class="lookUpActionButtons" id="btnLocationLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnLocationLookupSearch">Search</button>
				
						</div>
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLocationSearchGrid" style="clear:both;">
					
							<table id="locationSearchGrid"></table>
							<div id="PJmap_LocationSearchGrid"></div>
	
						</div>
	
				</div>';
	}
	
	function manufactureBatchLookupScreen()
	{
		return  '<div class="divManufactureBatchLookUpOuter">
						<div class="TOLookUpTopic">
							
	
						</div>
	
						<form id="manufactureBatchLookUpform">
							<table class="DISTable">
										<tr class="globalTr" hidden>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From mfg Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="fromMFGDatetxt" name="FromMFGDate" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To mfg Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="toMFGDatetxt" name="ToMFGDate" >
											</td>
										
										</tr>
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Manufacturer batch no : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="manufactureBatchNo" name="ManufactureBatchNo" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;"></td>
											<td class="globalTd" style="text-align:left;width:150px;">
												
											</td>
											
										
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnManufactureBatchLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnManufactureBatchLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
			
							<table id="manufactureBatchSearchGrid"></table>
							<div id="PJmap_manufactureBatchSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	function manufactureBatchLookupScreen2()
	{
		return  '<div class="divManufactureBatchLookUpOuter">
						<div class="TOLookUpTopic">
				
	
						</div>
	
						<form id="manufactureBatchLookUpform">
							<table class="DISTable">
										<tr class="globalTr" hidden>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">From mfg Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="fromMFGDatetxt" name="FromMFGDate" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To mfg Date : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="toMFGDatetxt" name="ToMFGDate" >
											</td>
	
										</tr>
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Manufacturer batch no : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="manufactureBatchNo" name="ManufactureBatchNo" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;"></td>
											<td class="globalTd" style="text-align:left;width:150px;">
	
											</td>
						
	
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnManufactureBatchLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnManufactureBatchLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
		
							<table id="manufactureBatchSearchGrid"></table>
							<div id="PJmap_manufactureBatchSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	
	function distributorSearchLookUpScreen()
	{
		return  '<div class="divDistributorSearchLookUpOuter">
						<div class="TOLookUpTopic">
				
		
						</div>
		
						<form id="distributorSearchLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											
											<td class="globalTd" style="text-align:right;width:20%;padding-right:10px;">Firstname : </td>
											<td class="globalTd" style="text-align:left;width:20%;">
												<input class="" type="text"  id="txtDistributorFirstName" name="DistributorFirstName" >
											</td>
											<td class="globalTd" style="text-align:right;width:20%;padding-right:10px;">Lastname : </td>
											<td class="globalTd" style="text-align:left;width:20%;">
												<input class="" type="text"  id="txtDistributorLastName" name="DistributorLastName" >
											</td>
		
										</tr>
										<tr class="globalTr">
											
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Mobile No : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtDistributorMobileNo" name="DistributorMobileNo" >
											</td>
		
										</tr>
							
							</table>
						</form>
						<div class="divLookUpButtons">
		
							<button type="button" class="lookUpActionButtons" id="btnDistributorLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnDistributorLookUpSearch">Search</button>
		
						</div>
		
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
		
							<table id="distributorSearchGrid"></table>
							<div id="PJmap_distributorSearchGrid"></div>
		
						</div>
		
		
				</div>';
	}
	
	function distributorRTGSCodeLookUpScreen()
	{
		return  '<div class="divDistributorRTGSLookUpOuter">
						
						<form id="distributorRTGSLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">RTGS/IFSC Code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtRTGSCode" name="RTGSCode" >
											</td>
											
	
										</tr>
				
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnDistributorLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnDistributorLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
	
							<table id="distributorRTGSSearchGrid"></table>
							<div id="PJmap_distributorRTGSSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	
	
	function locationSearchLookUpScreen()
	{
		return  '<div class="divLocationSearchLookUpOuter">
						
						<form id="locationSearchLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Location Name : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtLocationName" name="LocationName" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">State : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtState" name="State" >
											</td>
	
										</tr>
				
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnLocationLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnLocationLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
	
							<table id="locationsSearchGrid"></table>
							<div id="PJmap_locationsSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	function PurchaseOrderLookUpScreen()
	{
		return  '<div class="divPOSearchLookUpOuter">
	
						<form id="POSearchLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">PO Number : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtPONumber" name="PONumber" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Vender code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtVendorCode" name="VendorCode" >
											</td>
	
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnPOLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnPOLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
	
							<table id="POSearchGrid"></table>
							<div id="PJmap_POSearchGrid"></div>
	
						</div>
	
	
				</div>';
	}
	
	/* function GRNLookUpScreen()
	{
		return  '<div class="divGRNSearchLookUpOuter">
	
						<form id="GRNSearchLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Number : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtGRNNumber" name="GRNNumber" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Vender code : </td>
											<td class="globalTd" style="text-align:left;width:150px;">
												<input class="" type="text"  id="txtVendorCode" name="VendorCode" >
											</td>
	
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnGRNLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnGRNLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
	
							<table id="GRNSearchGridLookup"></table>
							<div id="PJmap_GRNSearchGridLookup"></div>
						</div>
				</div>';
	}
	 */
	
	/**
	 * my account look up screen.
	 * @return string
	 */
	function myAccountLookUpScreenHtml()
	{
		return  '<div class="divMyAccountLookUpOuter">
	
						<form id="myAccountLookUpform">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:200px;padding-right:10px;">From Order Date : </td>
											<td class="globalTd" style="text-align:left;width:100px;">
												<input type="text"  id="txtFromOrderDate" name="FromOrderDate" class="showCalender">
											</td>
											<td class="globalTd" style="text-align:right;width:200px;padding-right:10px;">To Order Date : </td>
											<td class="globalTd" style="text-align:left;width:100px;">
												<input type="text"  id="txtToOrderDate" name="ToOrderDate" class="showCalender" >
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Status : </td>
											<td class="globalTd" style="text-align:left;width:100px;">
											<select  class="CommonSelect" name="OrderStatus" id="HistoryStatus" style="height:25px;">
												<option value="-1">Select</option>
												<option value="3">Confirmed</option>
												<option value="2">Cancelled</option>
												<option value="4">Invoiced</option>
												
	     	     							</select>
											</td>
										
	
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnLookUpReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnLookUpSearch">Search</button>
	
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
	
							<table id="myAccountSearchGrid"></table>
							<div id="PJmap_myAccountSearchGrid"></div>
	
						</div>
				
			<div style="float:left;margin-top:10px;color:white;">
				<b>Note</b> - Press "<b> Order Print </b>" before pressing "<b> Make Payment </b>"
			</div>
				
			<div style="margin-top:10px;">
				<button type="button" class="lookUpActionButtons" id="OrderPrint">OrderPrint</button>
				<button type="button" class="lookUpActionButtons left_margin_class" id="btnOrderPay">Make Payment</button>
			</div>
		</div>';
	}
	
	/**
	 * my account look up screen.
	 * @return string
	 */
	function CODPaymentLookupScreenHTML()
	{
		return  '<div class="divCODPaymentLookUpOuter">
	
						<form id="CODPaymentLookUpForm">
							<table class="DISTable">
										<tr class="globalTr">
											<td class="globalTd" style="text-align:right;width:200px;padding-right:10px;">Order No. : </td>
											<td class="globalTd" style="text-align:left;width:100px;">
												<input type="text"  id="txtOrderNo" class="clsPOPEnterField" name="OrderNo">
											</td>
											<td class="globalTd" style="text-align:right;width:200px;padding-right:10px;">Invoice No. : </td>
											<td class="globalTd" style="text-align:left;width:100px;">
												<input type="text"  id="txtInvoiceNo" class="clsPOPEnterField" name="InvoiceNo">
											</td>
											<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Is Payment Received : </td>
											<td class="globalTd" style="text-align:left;width:100px;">
												<input type="checkbox" class="IsPaymentReceived" value="1" id="IsPaymentReceived" name="IsPaymentReceived">
											</td>
	
	
										</tr>
	
							</table>
						</form>
						<div class="divLookUpButtons">
	
							<button type="button" class="lookUpActionButtons" id="btnCODReset">Reset</button>
							<button type="button" class="lookUpActionButtons" id="btnCODSearch">Search</button>
							
						</div>
	
						<div class="informationTopic">
							Search Results
						</div>
						<div id="divLookUpGrid" style="width:560px;clear:both;">
	
							<table id="CODSearchGrid"></table>
							<div id="PJmap_CODSearchGrid"></div>
	
						</div>
						<div class="divLookUpButtons1">
							<input type="text" class="showCalender" id="txtCODReceivePaymentComment" name="CODReceiveComment" style="width:500px;" placeholder="Insert Comments">
							<button type="button" class="lookUpActionButtons" id="btnReceivePayment" disabled>Receive Payment</button>
						</div>
	
				</div>';
	}

	function GRNLookUpScreen()
	{
		return  '<div class="divPOSearchLookUpOuter">

				<form id="POSearchLookUpform">
				<table class="DISTable">
				<tr class="globalTr">
				<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Number : </td>
				<td class="globalTd" style="text-align:left;width:150px;">
				<input class="" type="text"  id="txtGRNNumber" name="txtGRNNumber" >
				</td>
				<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Vender code : </td>
				<td class="globalTd" style="text-align:left;width:150px;">
				<input class="" type="text"  id="txtVendorCode" name="txtVendorCode" >
				</td>

				</tr>

				</table>
				</form>
				<div class="divLookUpButtons">

				<button type="button" class="lookUpActionButtons" id="btnGRNLookUpReset">Reset</button>
				<button type="button" class="lookUpActionButtons" id="btnGRNLookUpSearch">Search</button>

				</div>

				<div class="informationTopic">
				Search Results:
				</div>
				<div id="divLookUpGrid" style="width:560px;clear:both;">

				<table id="GRNLookupSearchGrid"></table>
				<div id="PJmap_POSearchGrid"></div>

				</div>


				</div>';
	}
	
	function GRNLookUpScreenForVR()
	{
		return  '<div class="divPOSearchLookUpOuter">
	
  <form id="POSearchLookUpform">
  <table class="DISTable">
  <tr class="globalTr">
  <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">GRN Number : </td>
  <td class="globalTd" style="text-align:left;width:150px;">
  <input class="" type="text"  id="txtGRNNumber" name="txtGRNNumber" >
  </td>
  <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Vender code : </td>
  <td class="globalTd" style="text-align:left;width:150px;">
  <input class="" type="text"  id="txtVendorCode" name="txtVendorCode" >
  </td>
	
  </tr>
	
  </table>
  </form>
  <div class="divLookUpButtons">
	
  <button type="button" class="lookUpActionButtons" id="btnGRNLookUpReset">Reset</button>
  <button type="button" class="lookUpActionButtons" id="btnGRNLookUpSearch">Search</button>
	
  </div>
	
  <div class="informationTopic">
  Search Results:
  </div>
  <div id="divLookUpGrid" style="width:560px;clear:both;">
	
  <table id="GRNLookupSearchGrid"></table>
  <div id="PJmap_POSearchGrid"></div>
	
  </div>
	
	
  </div>';
	}
	
	function itemLookUpScreenForBlockItem()
	{
		return  '<div class="divBlockItemSearchLookUpOuter">
	
		<form id="BlockItemSearchLookUpform">
		<table class="DISTable">
		<tr class="globalTr">
		<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Item Code : </td>
		<td class="globalTd" style="text-align:left;width:150px;">
		<input class="" type="text"  id="txtBlockItemCode" name="txtBlockItemCode" >
		</td>
		<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Item Name : </td>
		<td class="globalTd" style="text-align:left;width:150px;">
		<input class="" type="text"  id="txtBlockItemName" name="txtBlockItemName" >
		</td>
	
		</tr>
	
		</table>
		</form>
		<div class="divLookUpButtons">
	
		<button type="button" class="lookUpActionButtons" id="btnBlockItemLookUpReset">Reset</button>
		<button type="button" class="lookUpActionButtons" id="btnBlockItemLookUpSearch">Search</button>
	
		</div>
	
		<div class="informationTopic">
		Search Results:
		</div>
		<div id="divLookUpGrid" style="width:560px;clear:both;">
	
		<table id="BlockItemLookupSearchGrid"></table>
		<div id="PJmap_BlockItemLookupSearchGrid"></div>
	
		</div>
	
	
		</div>';
	}
	
	function BatchLookupScreenHtml()
	{
		return  '';
	}
}


?>
