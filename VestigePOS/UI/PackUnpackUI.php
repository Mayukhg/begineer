<?php



class PackUnpackScreen
{
	function PackUnpackHtml()
	{
		return  '<div class="divPackUnpackOuter">

				
					<input type="hidden" id="moduleCode" value="PUN01">
				
					<div id="divLookUp" title="Composite Item Search">
							
					</div>
				
					<div id="divPackUnpackTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
				<form id="">
						<div id="DivSearch">
							<div id="divPackUnpack">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Pack/Unpack * : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="packUnpackSelect" name="PackUnpackSelect" style="background-color:white;height: 28px;">
													<option name="packUnpackOption" value="-1">Select</option>
													<option name="packUnpackOption" value="0">Pack</option>
													<option name="packUnpackOption" value="1">Unpack</option>
											</select>
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="packUnpackFromDate" name="PackUnpackFromDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">To Date :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="packUnpackToDate" name="PackUnpackToDate">
										</td>
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Code : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="PackUnpackInput" type="text"  id="packUnpackItemCode" name="PackUnpackItemCode">
						
									     </td>
									</tr>
									
									
										
								</table>
							</div>
				
							<div class="divPackUnpackSearchButtons">
								<button type="button" id="btnPUN01Search" class="PackUnpackCreateButtons">Search</button>
								<button type="button" id="btnPackUnpackReset" class="PackUnpackSearchButtons">Reset</button>
								
							</div>

							<div class="searchResultTopic">
								Search Results
							</div>
							<div style="width:1040px; clear:both; height:460px;">
								<div id="DivPackUnPackSearchGrid1">
				
									<table id="PackUnpackGroupItemGrid"></table>
									<div id="PJmap_PackUnpackGroupItemGrid"></div>
									
								</div>
								<div id="DivPackUnPackSearchGrid2">
				
									<table id="PackUnpackItemsInsideGroupGrid"></table>
									<div id="PJmap_PackUnpackItemsInsideGroupGrid"></div>
									
								</div>
							</div>
							
						</div>
				</form>
						<div id="DivCreate">
							<div id="divPackUnpackCreate">
								<form id="packUnpackCreateForm">
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Pack/Unpack : </td>
											<td class="DISTd" style="text-align:left;width:100px;">
												<select id="packUnpackCreateSelect" name="PackUnpackSelect" class="PUFieldFormat" style="background-color:white;height: 28px;">
													
														<option name="packUnpackOption" value="0">Pack</option>
														<option name="packUnpackOption" value="1">Unpack</option>
												</select>
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Code* : </td>
											<td class="DISTd" style="text-align:left;width:100px;">
												<input class="PUFieldFormat" type="text"  id="packUnpackCreateItemCode" name="PackUnpackCreateItemCode" placeholder="Press F4 for listing" >
												<input class="PUFieldFormat" type="hidden"  id="packUnpackCreateItemId" name="CompositeItemId" >
										     </td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Name :</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="PUFieldFormat" type="text"  id="packUnpackCreateItemName" name="PackUnpackCreateItemName">
											</td>
					
											
										</tr>
										<tr class="DISTr">
											
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">MRP:  </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												
												<input class="PUFieldFormat" type="text"  id="packUnpackCreateMRP" name="PackUnpackCreateMRP">
											</td>
											
											<td class="DISTd" id="lbmfgDate" style="text-align:right;width:125px;padding-right:10px;">MFG Date :</td>
											<td class="DISTd" id="txtmfgDate" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="packUnpackCreateMFG" name="PackUnpackCreateMFG">
											</td>
											<td class="DISTd" id="lbexpDate" style="text-align:right;width:125px;padding-right:10px;">Exp. Date :</td>
											<td class="DISTd" id="txtexpDate" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="packUnpackCreateEXP" name="PackUnpackCreateEXP">
												
											</td>
											<td class="DISTd" id="lbtotalQty" style="text-align:right;width:125px;padding-right:10px;">Available Qty :</td>
											<td class="DISTd" id="txttotalQty" style="text-align:left;width:150px;">
												<input class=""  type="text"  id="packunpacktotalQty" name="PackunpacktotalQty">
												
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> </td>
											
											
										</tr>
					
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Remarks : </td>
											<td colspan = "3" class="DISTd" style="text-align:left;width:150px;">
												<input class="PUFieldFormat" type="text"  id="packUnpackCreateRemarks" name="PackUnpackCreateRemarks" style="width:450px;">
											</td>
											
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Pack/Unpack Qty*.: </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="PUFieldFormat" maxlength="4" type="text"  id="packUnpackCreatePackUnpackQty" name="PackUnpackCreatePackUnpackQty">
										    	<input class="PUFieldFormat" hidden type="text"  id="packUnpackNo" name="packUnpackNo">
										     </td>
					
										</tr>
					
					
									</table>
								</form>
								<div class="divPackUnpackCreateButtons">
									<button type="button" id="btnPackUnpackCreateProcess" class="PackUnpackSearchButtons">Process</button>
									<button type="button" id="btnPackUnpackcreateReset2" style="float:Right;" class="PackUnpackSearchButtons">Reset</button>
								 
				               </div>
				
								<div class="searchResultTopic">
									Constituent Item
								</div>
									
								
							</div>
							
							
							<div id="divPankUnpackCreateGrid">
								
								<table id="packUnpackCreateGrid"></table>
								<div id="PJmap_packUnpackCreateGrid"></div>
									
							</div>
				
							<div class="divPackUnpackActionButtons">
								<button type="button" id="btnPackUnpackcreateReset" style="float:Right;" class="PackUnpackSearchButtons">Reset</button>
								<button type="button" id="btnPUN01Save"  style="float:Right;" class="PackUnpackCreateButtons">Save</button>
							    <button type="button" id="btnPUN01Print" style="float:Right;" class="PackUnpackSearchButtons">Print</button>
				          </div>
				
						</div>
						
						
					</div>
				</div>';
	}
}


?>