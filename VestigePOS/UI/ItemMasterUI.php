<?php

class ItemMasterScreen
{
	function itemMasterHtml()
	{
		return  '<div class="divTransferOutOuter">
				
					<div id="divLookUp">
							
					</div>

					<div id="divTransferOutTab"> 
						<ul>
							<li><a href="#ItemMaster">Item Master</a></li>
							<li><a href="#ItemVendor">Item-Vendor</a></li>
							<li><a href="#ItemVendorLocation">Item-Vendor-Location</a></li>
							
						</ul>

					<form id="formSearchTO">
							<div id="ItemMaster">
								<div id="divTransferOut">
									<table class="DISTable">
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item	Code :* </td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="TOINumberSearch" type="text"  id="TOINumber" name="TOINumber">
											
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Barcode : </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="DISTSearchInput" type="text"  id="TONumber" name="TONumber">
							
										     </td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Item Name: *</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="DISTSearchInput" type="text"  id="TORefNumber" name="TORefNumber">
											</td>
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Short Name :* </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<select class="requiredList" id="TOSourceLocation" name="TOSourceLocation">
														
														
												</select>
							
										     </td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Item Print Name :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="TODestinationLocation" name="ToDestinationLocation">
														
														
												</select>
											</td>
											<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Item Display Name :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="TOStatus" name="TOStatus">
														
												</select>
											</td>
											
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Receipt Name :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="showCalender2" type="text"  id="fromTODate" name="FromTODate">
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Sub Category Name :* </td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="showCalender2" type="text"  id="toTODate" name="ToTODate">
							
										     </td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">MRP :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="showCalender" type="text"  id="fromTOShipDate" name="FromTOShipDate">
											</td>
											
										</tr>
										<tr class="DISTr">
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Cost :*</td>
											<td class="DISTd" style="text-align:left;width:200px;">
												<input class="showCalender" type="text"  id="toTOShipDate" name="ToTOShipDate">
							
										     </td>
											<td hidden class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Distributor Price :*</td>
											<td hidden class="DISTd" style="text-align:left;width:150px;">
												<select class="requiredList" id="indentised"   name="indentised">
														<option value="0">Select</option>
	                                                    <option value="1">1</option>
				                                    	<option value="2">2</option>				
												</select>
									
											</td>
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Point Value :*</td>
											<td class="DISTd" style="text-align:left;width:150px;">
												<input class="DISTSearchInput" type="text"  id="TOCreateBranchRemark" name="TOCreateBranchRemark">
											</td>
										
										
											
											
										</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Business Valume :*</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateBranchRemark" name="TOCreateBranchRemark">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Tax Category:*</td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceCSTNo" name="TOCreateSourceCSTNo">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Is Composite.:*</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceVATNo" name="TOCreateSourceVATNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Is Kit :* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceTINNo" name="TOCreateSourceTINNo" >
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Minimum Kit Order Value. : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationCSTNo" name="TOCreateDestinationCSTNo" >
												
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Select Location.:*</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationVATNo" name="TOCreateDestinationVATNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">UOM :*</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationTinNo" name="TOCreateDestinationTinNo">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Width.*: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateExpectedDeliveryDate" name="TOCreateExpectedDeliveryDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Item Height. :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRefNo" name="TOCreateRefNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Length:</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateShippingDetails" name="TOCreateShippingDetails" >
										</td>
								    	<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Weight:* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreatePackSize" name="TOCreatePackSize">
						
									     </td>
									
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Stack Limit :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRemarks" name="TOCreateRemarks" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Bay Number : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOQuantity" name="TOCreateTotalTOQuantity">
										</td>
										<td class="DISTd" disabled style="text-align:right;width:100px;padding-right:10px;">Expiry Duration(in months) *: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateGrossWeight" name="TOCreateGrossWeight">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOAmount" name="TOCreateTotalTOAmount" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Is Available for gift :*</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationTinNo" name="TOCreateDestinationTinNo">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Particaipation in Promotion.*: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateExpectedDeliveryDate" name="TOCreateExpectedDeliveryDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Landed Price. :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRefNo" name="TOCreateRefNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Pack Type:</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateShippingDetails" name="TOCreateShippingDetails" >
										</td>
								    	<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Pack Size:* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreatePackSize" name="TOCreatePackSize">
						
									     </td>
									
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">USD Price :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRemarks" name="TOCreateRemarks" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Product Image : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOQuantity" name="TOCreateTotalTOQuantity">
										</td>
										<td class="DISTd" disabled style="text-align:right;width:100px;padding-right:10px;">Web Description *: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateGrossWeight" name="TOCreateGrossWeight">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Expiry Date Format :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOAmount" name="TOCreateTotalTOAmount" >
										</td>
									</tr>
											
									</table>
								</div>
								<div class="divBtnAddSearch">
								<button type="button" disabled id="btnTSF02Search" style="margin-left:100px;" class="TOCreateActionButtons">Search</button>
								<button type="button" id="btnReset" class="btnAddSearch">Reset</button>
										
										
								</div>
								<div class="searchResultTopic">
									Search Results
								</div>
								
								<div id="DivTOISearchGrid" style="width:1040px;clear:both;">
									
									<table id="TOSearchGrid"></table>
									<div id="PJmap_TOSearchGrid"></div>
										
								</div>
							</div>
					</form>
						<div id="ItemVendor">
							<div id="TransferOutCreateDiv">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">TOI Number : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="TOINumberSearch" type="text"  id="TOCreateTOINumber" name="TOCreateTOINumber" placeholder="Press F4 for listing">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTOISourceLocation" name="TOCreateTOISourceLocation">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTOIDestinationLocation" name="TOCreateTOIDestinationLocation">
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">TO Number : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTONumber" name="TOCreateTONumber">
											
						
									     </td>
										<td rowspan="2" class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Address:  </td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											
											<textarea class="distributor_info" id="TOCreateTOSourceLocation" rows="4" cols="20" name="TOCreateTOSourceLocation"></textarea>
										</td>
										
										<td rowspan="2" class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Address :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="TOCreateDestinationAddress" rows="4" cols="20" name="TOICreateDestinationAddress"></textarea>
										</td>
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Status : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateStatus" name="TOCreateStatus" >
										</td>
									
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Branch Remark : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateBranchRemark" name="TOCreateBranchRemark">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source CST No. : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceCSTNo" name="TOCreateSourceCSTNo">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Source VAT No. :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceVATNo" name="TOCreateSourceVATNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source TIN No. : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateSourceTINNo" name="TOCreateSourceTINNo" >
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Destination CST No. : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationCSTNo" name="TOCreateDestinationCSTNo" >
												
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination VAT No.:</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationVATNo" name="TOCreateDestinationVATNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Destination Tin No. : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateDestinationTinNo" name="TOCreateDestinationTinNo">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Expected Devilery Date.*: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateExpectedDeliveryDate" name="TOCreateExpectedDeliveryDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Gross Weight. :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRefNo" name="TOCreateRefNo" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Transp./Courier CO. Name:* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateShippingDetails" name="TOCreateShippingDetails" >
										</td>
								    	<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">No of Boxes :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreatePackSize" name="TOCreatePackSize">
						
									     </td>
									
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Remarks :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateRemarks" name="TOCreateRemarks" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Total TO Quantity : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOQuantity" name="TOCreateTotalTOQuantity">
										</td>
										<td class="DISTd" disabled style="text-align:right;width:100px;padding-right:10px;">Product Weight *: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateGrossWeight" name="TOCreateGrossWeight">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Total TO Amount :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOCreateTotalTOAmount" name="TOCreateTotalTOAmount" >
										</td>
									</tr>
									<tr class="DISTr">
										
										<td  class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Indentise : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="checkbox" type="checkbox"   id="TOCreateDestinationCSTNo" name="TOCreateDestinationCSTNo">
												
									     </td>
										<td  class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Is Exported :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="checkbox" type="checkbox"    id="TOCreateIsExported" name="TOCreateIsExported">
										</td>
									</tr>
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div class="DistributorMasterInformation">
								
							</div>
							<div class="DistributorCurrentRanking">
								
							</div>
							<div class="DistributorCurrentPreviousPvBvComputation">
								<table class="DISTable">
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right;">Item Code : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
			                        	<select class="DISTInput" id="TOCreateItemCode" name="TOCreateItemCode"  style="background-color:white;height: 24px;width:100px;">
										
				        
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Transfer Price: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateTransferPrice" name="TOCreateTransferPrice" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Description : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemDescription" name="TOCreateItemDescription" >
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">UOM: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateUOM" name="TOCreateUOM" >
									     </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Weight: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateWeight" name="TOCreateWeight" >
									    </td>
										<td class="DISTd" style="width:100px;text-align:right">Requested Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateRequestedQty" name="TOCreateRequestedQty" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Bucket Name: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIIteBucketName" name="TOIIteBucketName" >
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">Batch No:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemBatchNo" name="TOCreateItemBatchNo" placeholder="Press f4 for listening" >
									    </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Available Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateAvailableQty" name="TOCreateAvailableQty" >
									    </td>
										<td class="DISTd" style="width:100px;text-align:right">Transfer Qty:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateTransferQty" name="TOCreateTransferQty" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Amount: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateItemAmount" name="TOCreateItemAmount" >
									     </td>
										<td colspan="2" class="DISTd" style="text-align:left;width:100px;">
											<button type="button" id="btnAdd" class="btnAddSearch">Add</button>
											<button type="button" id="clear" class="btnAddSearch">Clear</button>
											
									     </td>
										
										
									</tr>
				
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Transfer Price: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOCreateAvailableQty" name="TOCreateAvailableQty" >
									    </td>
										
									</tr>
									
									
								</table>
								
							</div>
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="TOItemGrid"></table>
								<div id="PJmap_TOItemGrid"></div>
									
							</div>
							<div class="divTOCreateActionButtons">
			  					<button type="button" disabled id="btnTSF02Confirm" class="TOCreateActionButtons">Transfer Out</button>
							
								<button type="button"  id="btnCreateReset" class="btnAddSearch">Reset</button>
								<button type="button" disabled id="btnTSF02Print" class="TOCreateActionButtons">Print</button>
								<input type="hidden"  id="moduleCode" value="TSF02">
									<input type="hidden" id="actionName" value="">
							</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>
