<?php

//CR - Customer Return

class CustomerReturnScreen
{
	function CustomerReturnHtml()
	{
		return  '<div class="divCustomerReturnOuter">
				
				
					<div id="divLookUp" title="Manufacture Batch">
							
					</div>
				
				
					<div id="itemPromotionDetailPopUp" title="Do you want to proceed ?">
						
						<div id="divItemPromotionDetail">
								<table id="itemPromotionDetailGrid"></table>
								<div id="itemPromotionDetailGridPager"></div>
						</div>
				
						<div id="divItemPromotiondDetailPopUpAction">
						
							<button id="btnPopUpYes" class="itemPromotiondDetailPopUpAction">Yes</button>
							<button id="btnPopUpNO" class="itemPromotiondDetailPopUpAction">NO</button>
							
						</div>
				
				
					</div>

					<div id="divCustomerReturnTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
				<form id="">
						<div id="DivSearch">
							<div id="divCustomerReturn">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Customer Type : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<Select class="requiredList"   id="CRCustomerType" name="CRCustomerType" style="background-color:white;height: 28px;">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Distributor/PC id/Invoice No : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="GRNTSearchInput" type="text"  id="CRDistributorPCIDInvoiceBo" name="CRDistributorPCIDInvoiceBo">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Location :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="CRLocation" name="CRLocation" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
										
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From(Approved) Date   : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="CRFromApprovedDate" name="CRFromApprovedDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">To (Approved) Date :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="CRToApprovedDate" name="CRToApprovedDate">
										</td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="CRStatus" name="CRStatus" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
										
										
									</tr>
									
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Approved by : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
										<select class="requiredList" id="CRApprovedBy" name="CRApprovedBy" style="background-color:white;height: 28px;">
													
													
										</select>
						
									</tr>
				
										
								</table>
						    	<div class="divCRCreateActionButtons">
									<button type="button" id="btnRET02Search" disabled class="CRCreateActionButtons">Search</button>
									<button type="button" id="btnSearchCRReset" class="btnSearchCRResetClass">Reset</button>
									<input type="hidden" id="moduleCode" value="RET02">
									<input type="hidden" id="actionName" value="">
								</div>
							</div>
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivCRSearchGrid" style="width:1040px;clear:both;">
								
								<table id="CRSearchGrid"></table>
								<div id="PJmap_CRSearchGrid"></div>
									
							</div>
						</div>
				</form>
						<div id="DivCreate" style="height:700px;">
							<div id="CustomerReturnCreateDiv">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Customer Type : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<Select class="requiredList"   id="CRCreateCustomerType" name="CRCustomerType" style="background-color:white;height: 28px;">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Distributor/PC Id/Invoice No. : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateDistributorPCIdInvoiceNo" name="CRCreateDistributorPCIdInvoiceNo">
						
									     </td>
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Location : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateLocation" name="CRCreateLocation">
						
									     </td>
					
										
									</tr>
								<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Customer Return No: </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput " type="text"  id="CRCreateReturnNo" name="CRCreateReturnNo">
										</td>
										
											<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Distributoor Id : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput " type="text"  id="CRCreateDistributorId" name="CRCreateTotalAmount">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Distributor Name : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateDistributorName" name="CRCreateDistributorName">
										</td>
									
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Approved Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											
											<input class="showCalender" type="text"  id="CRCreateApprovedDate" name="CRCreateApprovedDate"> 	
						
									     </td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Approved By:  </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											
											<input class="" type="text"  id="CRCreateApprovedBy" name="CRCreateApprovedBy">
										</td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Location Address :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="CRCreateLocationAddress" rows="4" cols="20" name="CRCreateLocationAddress"></textarea>
										</td>
										
									</tr>
				
									<tr class="DISTr">
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Status: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateStatus" name="CRCreateStatus">
						
									     </td>
										
									</tr>
				
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Total Amount : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateTotalAmount" name="CRCreateTotalAmount">
										</td>
									
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Discount Amt : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateDiscountAmt" name="CRCreateDiscountAmt">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Make PV/BV Zero : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input  type="checkbox"  id="CRCreatePBBV" name="CRCreatePBBV">
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Invoice Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="CRCreateInvoiceDate" name="CRCreateTotalAmount">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Invoice Amount : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateInvoiceAmount" name="CRCreateInvoiceAmount">
										</td>
									<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Tax Amount : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateTaxAmount" name="CRCreateTaxAmount">
										</td>
								
										
									</tr>
				
								</table>
				
									<div class="searchResultTopic">
										Item Details
									</div>
									
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Item Code : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="DISTSearchInput"   id="CRCreateItemCode" name="CRCreateItemCode" style="width:150px;vertical-align: middle;word-wrap: normal;">
												<option value="-1">Select</option>
												</select>
				<input class="DISTSearchInput" type="text"  id="CRCreateItemCode2" name="CRCreateItemCode2">
										</td>
				
										
				
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Item Desc: </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateItemDescription" name="CRCreateItemDescription">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Distributor Price :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateDistributorPrice" name="CRCreateDistributorPrice">
										</td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Avail Qty :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateInvoiceQty" name="CRCreateInvoiceQty">
										</td>
										
									</tr>
				
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Mfg Batch No :* </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateMfgBatchNo" name="CRCreateMfgBatchNo" placeholder="Press f4 to search">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Exp Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateExpDate" name="CRCreateExpDate">
										</td>
				
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"> Return Qty :* </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="CRCreateReturnQty" name="CRCreateReturnQty">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Is Promo:</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="checkbox"  id="CRCreateIsPromo" name="CRCreateIsPromo">
										</td>
										
									</tr>
								
								</table>
								<div class="divCRCreateAddClearButtons">
									<button type="button" id="btnCRCreateAdd" class="btnSearchCRResetClass">Add</button>
									<button type="button" id="btnCRCreateClear" class="btnSearchCRResetClass">Clear</button>
								</div>

							</div>
							
							
							<div id="divCRCreateGrid" style="width:1040px;clear:both;">
								
								<table id="CRCreateGrid"></table>
								<div id="PJmap_CRCreateGrid"></div>
									
							</div>
				
						
							<div class="divCRDeductionPayableAmount">
        
					        <table id="tblForApporval" class="DISTable" style=" width: 1040px;background-color: #F3FEFF;">
					         	<tbody style="">
									<tr class="DISTr" style="width: 1500px;">
					          			<td class="DISTd" style="width: 50px;">Deduction Amount : </td>
					          			<td class="DISTd" style="width: 100px;">
					           				<input class="GRNTSearchInput" type="text" id="CreateDedAmt" name="GRNumber" style="background: white;width: 150px;">
					          			</td>
										<td class="DISTd" style="width: 103px;">Payable Amount : </td>
					          			<td class="DISTd" style="width: 100px;">
					           				<input class="GRNTSearchInput" type="text" id="CreatePayable" name="GRNumber" disabled="" style="background: rgb(221, 221, 221);width: 100px;">
					       
					           			</td>
							
					          			
					          			<td class="DISTd" style="width: 122px;float: left;">Pay cash :
					           				<input class="GRNTSearchInput" type="radio" id="radioPayIncash" name="PayMode" value = 2 style="background: rgb(221, 221, 221);">
					       
					              		</td>
				                         <td class="DISTd" style="width: 135px;float: left;">Pay Bonus Cheque :
					           				<input class="GRNTSearchInput" type="radio" id="radioPayBonusCheque" name="PayMode" value = 1 style="background: rgb(221, 221, 221);">
					       
					              		</td>
							
							
					          			<td class="DISTd" style="width: 130px;float: left;">
					           				<input class="GRNTSearchInput" type="text" id="txtBonusChequeNo" name="BonusChequeNo" placeholder="Bonus Cheque No." readonly="" style="width: 180px;">
					       
					              		</td>
								
					         		</tr>
					         		<tr class="DISTr" style="">
					          			<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Remarks :</td>
          								<td class="DISTd" colspan="5" rowspan="2" style="text-align:left;width: 872px;">
           									<textarea class="distributor_info" id="CRCreateRemarks" rows="3" cols="20" style="width: 827px;" name="ClaimRemarks"></textarea>
										</td>
					         		</tr>
									<tr class="DISTr">
									</tr>
						        </tbody>
						</table>
					         
					       </div>
				
							
				
							<div class="divCRCreateActionButtons">
								
								<button type="button" disabled id="btnRET02CreditPrint" class="CRCreateActionButtons">Print Credit Note</button>
								<button type="button" id="btnReset" class="CRCreateActionButtons">Reset</button>
								<button type="button" disabled id="btnRET02Print" class="CRCreateActionButtons">Ack. Print</button>
								<button type="button" disabled id="btnRET02Approve" class="CRCreateActionButtons">Approve</button>
								<button type="button" disabled id="btnRET02Cancel" class="CRCreateActionButtons">Cancel</button>
								<button type="button" disabled id="btnRET02Save" class="CRCreateActionButtons">Save</button>
							</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>