<?php 

class ViewIncidenceScreen{
	
	public function viewIncidenceHTML(){
		
		return '<div class="outer_div">
				
					<div data-role="page" id="pageStartLoading" class="app-content">
	
					<div data-role="header" class="header-basic-properties">
			 		  	<h2 class="header-headings">Incidence Information</h2>
			   		</div>
			
					<div data-role="main" class="ui-content">
						<div id="container" style="width:100%; height:400px;"></div>
					</div>
					</div>
					<div data-role="page" id="pageIncidenceInfo" class="app-content">
			
					<div data-role="header" class="header-basic-properties">
			 		  	<h2 class="header-headings">Incidence Information</h2>
			   		</div>
		
					<div data-role="content" class="ui-content" style="padding-top: 0px;">
			
						<ul id="incidenceList" data-role="listview">
			
						</ul>
					</div>
					</div>
				
				</div>';
	}
}

?>