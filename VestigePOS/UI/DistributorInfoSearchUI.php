	<?php
	
class DistributorInfoSearchScreen
{
	function distributorInfoSearchHtml()
	{
		return  '<div class="DistributorInfoSearchOuterdiv">
				  <div id="divLookUp" title="RTGS/IFSC Code information">
							
						</div>
				<input type="hidden" id="RTGSCodeValidated">

						<input type="hidden" id="bankAccountLength">
				
					<input type="hidden" id="moduleCode" value="DS01">
					<input type="hidden" id="isMiniBranch">
				
					<div id="DistributorPanBankInfoPopUp">
							
							<iframe id="iframeId">
  <p>Your browser does not support iframes.</p>
</iframe>
							
				
					</div>
				
					<div id="DivDistributorInfoSearchTab">
						<ul>
							<li><a href="#DivDistributor">Distributor</a></li>
							<li><a href="#DivDownlines">Downlines</a></li>
							<li><a href="#DivBonus">Bonus</a></li>
							<li><a href="#DivTodayInvoice">Today Invoice</a></li>
							<li><a href="#DivCurrentMonthInvoice">Current Month Invoice</a></li>
							<li><a href="#DivAllInvoice">All Invoice</a></li>
							<li><a href="#DivSuccess">Success</a></li>
						</ul>
						<div id="DivDistributor">
							<form id="distributorInfoSearchForm" method="post" enctype="multipart/form-data">
								<div id="distributorInfoSearchAction">
							  
								<table class="DISTable">
									<tr class="DISTr1">
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Distributor-Id : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="S_DISDistributorId" name="DISDistributorId">
										</td>	
										<td class="DISTd1" style="text-align:right;width:100px;padding-right:10px;">First Name : </td>
										<td class="DISTd1" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="S_DISFirstName" name="DISFirstName">
											
									     </td>	
										<td class="DISTd1" style="text-align:right;width:125px;padding-right:10px;">Last Name :</td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="S_DISLastName" name="DISLastName"> 
										</td>
									</tr>
								</table>
								<div class="DISActionButtonsDiv">
									<button type="button" id="btnDS01InvoicePreview" class="DISActionButtons" disabled>Print</button>
									<button type="button" id="reset" class="DISActionButtons"  disabled>Reset</button>
									<button type="button" id="save" class="DISActionButtons" disabled>Save</button>
									<button type="button" id="btnDS01Update" class="DISActionButtons" disabled>Edit</button>
									<button type="button" id="searchUpline" class="DISActionButtons" disabled>Search Upline</button>
									<button type="button" id="distInfoHistory" class="DISActionButtons" disabled>Distri. Info History</button>
									<button type="button" id="panBankHistory" class="DISActionButtons" disabled>Pan/Bank History</button>
									
									<button type="button" id="btnDS01Search" class="DISActionButtons" disabled>Search</button>
									
								</div>
								
							</div>
							<div class="searchResultTopic">
								Search Result
							</div>
							<div class="DistributorMasterInformation">
								<table class="DISTable">
									<tr class="DISTr1">
										<td class="DISTd1" style="width:140px;"></td>
										<td class="DISTd1" style="width:50px;">Title : </td>
										<td class="DISTd1" style="width:140px;">First Name : </td>
										<td class="DISTd1" style="width:140px;">Last Name : </td>
										<td class="DISTd1" style="width:140px;">Date Of Birth : </td>
										<td class="DISTd1" style="width:140px;">Enrolled On : </td>
										<td class="DISTd1" style="width:140px;">Upline No. : </td>
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" style="width:140px;text-align:center;">
											Distributor:
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:50px;">
											
											<select  class="CommonSelect clsRequiredSelect" name="Title" id="title" style="width:65px;">
												<option value="0">Select</option>
												<option value="1">Mr.</option>
												<option value="2">Ms.</option>
												<option value="3">Dr.</option>	
												<option value="4">M/S.</option>
	     	     							</select>
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput clsRequiredInput" type="text"  id="btnDS01EditDistributorName" name="DISFirstname" style="width:120px;" disabled> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="DISLastname" name="DISLastname" style="width:120px;">
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput clsRequiredInput" type="text"  id="DISDOB" name="DISDOB" style="width:120px;">
											
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="EnrolledOn" name="EnrolledOn" style="width:120px;"> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput clsRequiredInput" type="text"  id="UplineNo" name="UplineNo" style="width:120px;"> 
										</td>
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" style="width:140px;text-align:center;">
											Co-Distributor:
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:50px;">
											
											<select  class="CommonSelect" name="Co_Title" id="co_title" style="width:65px;">
												<option value="0">Select</option>
												<option value="1">Mr.</option>
												<option value="2">Ms.</option>
	     	     							</select>
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="Co_DISFirstname" name="Co_DISFirstname" style="width:120px;"> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="Co_DISLastname" name="Co_DISLastname" style="width:120px;">
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="Co_DISDOB" name="Co_DISDOB" style="width:120px;">
											
									     </td>	
										
										<td class="DISTd1" style="text-align:right;width:140px;">Password : </td>
											
										
										
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="btnDS01Password" name="DISPassword" style="width:120px;">
										</td>
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" id="distributorAddress" style="text-align:center;width:150px;padding-right:10px;">Address : </td>
										<td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<input class="DISTInput clsRequiredInput" type="text"  id="Address1" name="Address1" style="width:250px;">
										</td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Country : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<select  class="CommonSelect clsRequiredSelect" name="Country" id="Country" style="width:150px;">
												<option value="0">Select</option>
	     	     							</select>
									     </td>	
										
										
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Serial No. : </td>
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput clsRequiredInput" type="text"  id="SerialNo" name="DISSerialNo" style="width:120px;">
										</td>	
										
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" style="text-align:center;width:150px;padding-right:10px;"></td>
										<td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<input class="DISTInput" type="text"  id="Address2" name="Address2" style="width:250px;">
										</td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">State : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<select  class="CommonSelect clsRequiredSelect" name="State" id="State" style="width:150px;">
												<option value="0">Select</option>
	     	     							</select>
									     </td>	
										
										
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Zone. : </td>
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="Zone" name="Zone" style="width:120px;">
										</td>	
										
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" style="text-align:center;width:150px;padding-right:10px;"></td>
										<td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<input class="DISTInput" type="text"  id="Address3" name="Address3" style="width:250px;">
										</td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">City : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<select  class="CommonSelect clsRequiredSelect" name="City" id="City" style="width:150px;">
												<option value="0">Select</option>
	     	     							</select>
									     </td>	
										
										
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Pin. : </td>
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput clsRequiredInput" type="text"  id="Pin" name="Pin" style="width:120px;">
										</td>	
										
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" style="text-align:center;width:150px;padding-right:10px;">Phone : </td>
										<td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<input class="DISTInput" type="text"  id="Phone" name="Phone" style="width:200px;">
										</td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Mobile: </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="Mobile" name="Mobile" style="width:120px;">
									     </td>	
										
										
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Fax : </td>
										<td class="DISTd1" style="text-align:left;width:140px;">
											<input class="DISTInput" type="text"  id="Fax" name="Fax" style="width:120px;">
										</td>	
										
									</tr>
									<tr class="DISTr1">
										
										<td class="DISTd1" style="text-align:center;width:150px;padding-right:10px;">Email : </td>
										<td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<input class="DISTInput" type="text"  id="Email" name="Email" style="width:200px;">
										</td>	
				                           	<td class="DISTd1" style="text-align: right;width: 150px;padding-right: 11px;">Is Form Available : </td>
										<td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<input  type="checkbox"  id="IsformAvailable" name="isform" value="0" style="margin-top: 7px;" disabled readonly>
										</td>	
				                          <td colspan="2" class="DISTd1" style="text-align:left;width:250px;">
								 			<Button type="button" id="btn-printagree" name="printagreement"  style="margin-top: 7px;">Print Agreement</button>
										</td>	
										
										
										
										
										
									</tr>
									<tr class="DISTr1" hidden="true">
										
										
										
										
										
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;"></td>
										<td class="DISTd1" style="text-align:left;width:140px;">
											
										</td>	
										
									</tr>
									<tr class="DISTr1">
										
                                        
				
										<td class="DISTd1" style="text-align:center;width:150px;padding-right:4px;">Pan : </td>
				                        
										<td   style="text-align:left;width:0px ;background-color: #e5f4fb;">
											<input id="pan_checkbox1" class="checkbox_status" type="checkbox" name="distributor_pan_information">
				                            
				                           </td>
				                            <td   style="text-align:left;width:180px; background-color: #e5f4fb;">
				                            <input type="file" id="pan_checkbox" class="upload_files" name="distributor_detail[]" disabled>
									     </td>	
				                       
				                     
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Save On: </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="SaveOn" name="SaveOn" style="width:120px;">
									     </td>	
										
										
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Remarks: </td>
										<td class="DISTd1" style="text-align:left;width:140px;">
											<select  class="CommonSelect" name="Remarks id="Remarks" style="width:140px;">
												<option value="0">Select</option>
	     	     							</select>
										</td>	
										
									</tr>
				<tr></tr>
				<tr class="DISTr1">
				<td class="DISTd1" style="text-align:center;width:150px;padding-right:4px;">Cancelled Cheque :</td>
				                        
										<td   style="text-align:left;width:0px ;background-color: #e5f4fb;">
											<input id="cencelled_cheque_checkbox1" class="checkbox_status" type="checkbox" name="distributor_can_cheque_information" value="cancelled_check">
				                            
				                           </td>
				                            <td   style="text-align:left;width:180px;   background-color: #e5f4fb;">
				                            <input type="file" id="cencelled_cheque_checkbox" class="upload_files" name="distributor_detail[] " disabled>
									     </td>	
				<td class="DISTd1" style="text-align:right;width:100px;padding-right:10px;"></td>
				<td class="DISTd1" style="text-align:left;width:150px;">
				<input class="DISTInput" type="text"  id="Pan" name="Pan" style="width:120px;"  placeholder="Pan Number" maxlength="10">
				  
				<select  class="CommonSelect" name="Bank" id="Bank" style="width:150px;">
												<option value="0">Select</option>
	     	     							</select>
				</td>
				<td class="DISTd1" style="text-align:right;width:100px;padding-right:10px;"></td>
				<td class="DISTd1" style="text-align:left;width:150px;">
				<input class="DISTInput" type="text"  id="BankDetail" name="BankDetail" style="width:120px;" placeholder="IFSC Code">
				<input class="DISTInput" type="text"  id="AccountNO" name="AccountNO" style="width:152px;" placeholder="Account Number">
				  
				
				</td>
				</tr>
				
				
								</table>
							</div>
							<div class="DistributorCurrentRanking">
								<table class="DISTable">
									<tr class="DISTr1">
										<td class="DISTd1" style="text-align:right;width:100px;padding-right:10px;">Star : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="Star" name="Star" style="width:150px;">
										</td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Director Group : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="DirectorGroup" name="DirectorGroup" style="width:150px;">
											
									     </td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Record Saved By :</td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="RecordSavedBy" name="RecordSavedBy" style="width:100px;"> 
										</td>
									</tr>
									<tr class="DISTr1">
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Entitled : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="Entitled" name="Entitled" style="width:150px;">
										</td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Director Name : </td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="DirectorName" name="DirectorName" style="width:150px;">
											
									     </td>	
										<td class="DISTd1" style="text-align:right;width:150px;padding-right:10px;">Record Saved On :</td>
										<td class="DISTd1" style="text-align:left;width:150px;">
											<input class="DISTInput" type="text"  id="RecordSavedOn" name="RecordSavedOn" style="width:100px;"> 
										</td>
									</tr>
								</table>
							</div>
							<div class="DistributorCurrentPreviousPvBvComputation">
								<table class="DISTable">
									<tr class="DISTr1">
										<td class="DISTd1" style="width:140px;"></td>
										<td class="DISTd1" style="width:100px;">Prev. Cum. Pv. : </td>
										<td class="DISTd1" style="width:100px;">Excl PV : </td>
										<td class="DISTd1" style="width:100px;">Self BV : </td>
										<td class="DISTd1" style="width:100px;">Group PV : </td>
										<td class="DISTd1" style="width:100px;">Total PV : </td>
										<td class="DISTd1" style="width:50px;">% : </td>
										<td class="DISTd1" style="width:100px;">Total Cum. PV : </td>
										<td class="DISTd1" style="width:10px;">Total BV : </td>
									</tr>
									<tr class="DISTr1">
										<td class="DISTd1" style="width:140px;text-align:center;">
											Current Month :
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentPrevCumPv" name="CurrentPrevCumPv" style="width:100px;">
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentExclPv" name="CurrentExclPv" style="width:100px;"> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentSelfBv" name="CurrentSelfBv" style="width:100px;">
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentGroupPv" name="CurrentGroupPv" style="width:100px;">
											
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentTotalpv" name="CurrentTotalpv" style="width:100px;"> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:50px;">
											<input class="DISTInput" type="text"  id="CurrentPercentage" name="CurrentPercentage" style="width:50px;"> 
										</td>
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentTotalCumPv" name="CurrentTotalCumPv" style="width:100px;">
											
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="CurrentTotalBv" name="CurrentTotalBv" style="width:100px;"> 
										</td>
									</tr>
									<tr class="DISTr1">
										<td class="DISTd1" style="width:140px;text-align:center;">
											Previous Month :
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousPrevCumPv" name="PreviousPrevCumPv" style="width:100px;">
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousExclPv" name="PreviousExclPv" style="width:100px;"> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousSelfBv" name="PreviousSelfBv" style="width:100px;">
										</td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousGroupPv" name="PreviousGroupPv" style="width:100px;">
											
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousTotalpv" name="PreviousTotalpv" style="width:100px;"> 
										</td>
										
										<td class="DISTd1" style="text-align:left;width:50px;">
											<input class="DISTInput" type="text"  id="PerviousPercentage" name="PerviousPercentage" style="width:50px;"> 
										</td>
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousTotalCumPv" name="PreviousTotalCumPv" style="width:100px;">
											
									     </td>	
										
										<td class="DISTd1" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="PreviousTotalBv" name="PreviousTotalBv" style="width:100px;"> 
										</td>
									</tr>
								</table>
							  </div>
							</form>
						</div>
						<div id="DivDownlines">
							<div id="DownlineGrid" style="width:1040px;clear:both;">
								
								<table id="downline_jqgrid"></table>
								<div id="pjmap_downline_grid"></div>
									
							</div>
								
						</div>
				
						<div id="DivBonus">
							
							<div id="divBonusGrid" style="width:1040px;clear:both;">
								
								<table id="bonusGrid"></table>
								<div id="bonusGridPager"></div>
									
							</div>
							
						</div>
				
						<div id="DivTodayInvoice">
							
							<div class="divInvoiceGrid" style="width:1040px;clear:both;">
								
								<table id="todayInvoiceGrid"></table>
								<div id="todayInvoiceGridPager"></div>
									
							</div>
						</div>
						
						
						<div id="DivCurrentMonthInvoice">
							
							<div class="divInvoiceGrid" style="width:1040px;clear:both;">
								
								<table id="currentMonthInvoiceGrid"></table>
								<div id="currentMonthInvoiceGridPager"></div>
									
							</div>
				
						</div>
						<div id="DivAllInvoice">
							
							<div class="divInvoiceGrid" style="width:1040px;clear:both;">
								
								<table id="allInvoiceGrid"></table>
								<div id="allInvoiceGridPager"></div>
									
							</div>
				
						</div>
				
						<div id="DivSuccess">
							
							<div class="divSuccessOuter">
								<div class="divDistributorSuccessHeader">
									<table class="DISTable" style="width:550px;">
										<tr class="DISTr1">
											<td class="DISTd1"></td>
											
											<td class="DISTd1">First Name</td>
											<td class="DISTd1">LastName</td>
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">Distributor :</td>
											
											<td class="DISTd1"><input class="DISTInput" type="text"  id="distributorFirstName" name="DistributorFirstName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="distributorLastName" name="DistributorLastName" style="width:150px;"></td>
										
										</tr>
									</table>
								</div>
								<div class="divDistributorRankOrganizationWise">
				
									<table id="distributorRankOrgGrid"></table>
									<div id="distributorRankOrgGridPager"></div>
				
									<div>
										<table class="DISTable">
											<tr class="DISTr1">
												
												<td class="DISTd1">Actual Title:</td>
												<td class="DISTd1"><input class="DISTInput" type="text"  id="distributorCurrentRankActualtitle" name="DistributorCurrentRankActualtitle" style="width:200px;"></td>
											</tr>
											<tr class="DISTr1">
												
												<td class="DISTd1">Valid Till :</td>
												<td class="DISTd1"><input class="DISTInput" type="text"  id="distributorCurrentRankValidTill" name="DistributorCurrentRankValidTill" style="width:100px;"></td>
											</tr>
										</table>
									</div>
				
								</div>
								<div class="distributorRankMonthWise">
									<table class="DISTable">
										<tr class="DISTr1">
											<td class="DISTd1"></td>
											<td class="DISTd1">Level Name </td>
											<td class="DISTd1">Percent </td>
											<td class="DISTd1">Level </td>
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">12 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist12MonthLevelName" name="Dist12MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist12MonthPecent" name="Dist12MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist12MonthLevel" name="dist12MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">11 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist11MonthLevelName" name="Dist11MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist11MonthPecent" name="Dist11MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist11MonthLevel" name="dist11MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">10 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist10MonthLevelName" name="Dist10MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist10MonthPecent" name="Dist10MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist10MonthLevel" name="dist10MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">9 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist9MonthLevelName" name="Dist9MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist9MonthPecent" name="Dist9MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist9MonthLevel" name="dist9MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">8 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist8MonthLevelName" name="Dist8MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist8MonthPecent" name="Dist8MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist8MonthLevel" name="dist8MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">7 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist7MonthLevelName" name="Dist7MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist7MonthPecent" name="Dist7MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist7MonthLevel" name="dist7MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">6 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist6MonthLevelName" name="Dist6MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist6MonthPecent" name="Dist6MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist6MonthLevel" name="dist6MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">5 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist5MonthLevelName" name="Dist5MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist5MonthPecent" name="Dist5MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist5MonthLevel" name="dist5MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">4 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist4MonthLevelName" name="Dist4MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist4MonthPecent" name="Dist4MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist4MonthLevel" name="dist4MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">3 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist3MonthLevelName" name="Dist3MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist3MonthPecent" name="Dist3MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist3MonthLevel" name="dist3MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">2 months ago :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist2MonthLevelName" name="Dist2MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist2MonthPecent" name="Dist2MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist2MonthLevel" name="dist2MonthLevel" style="width:50px;"></td>
										
										</tr>
										<tr class="DISTr1">
											<td class="DISTd1">Last Month :</td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="Dist1MonthLevelName" name="Dist1MonthLevelName" style="width:150px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist1MonthPecent" name="Dist1MonthPecent" style="width:50px;"></td>
											<td class="DISTd1"><input class="DISTInput" type="text"  id="dist1MonthLevel" name="dist1MonthLevel" style="width:50px;"></td>
										
										</tr>
									</table>
								</div>
				
							</div>
				
						</div>
					</div>
				
					
				
					<div id="distributorHistoryPopUp" title="Distributor History">
							
							<div class="divNavigationControl">
								<button id="firstHistoryResult">|<</button>
								<button id="previousHistoryResult"><</button>
								<button id="nextHistoryResult">></button>
								
								<button id="lastHistoryResult">>|</button>
							</div>
				
				
							<table class="DISTable">
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:100px;"></td>
										<td class="globalTdWithNoPadding" style="width:60px;">Title : </td>
										<td class="globalTdWithNoPadding" style="width:100px;">First Name : </td>
										<td class="globalTdWithNoPadding" style="width:100px;">Last Name : </td>
										<td class="globalTdWithNoPadding" style="width:100px;">Date Of Birth : </td>
										<td class="globalTdWithNoPadding" style="width:100px;">Enrolled On : </td>
										<td class="globalTdWithNoPadding" style="width:100px;">Upline No. : </td>
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right;">
											<b>Distributor : </b> 
										</td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:50px;">
											
											<input class="DISHistInput" type="text"  id="DISHistTitle" name="DISHistTitle" style="width:50px;"> 
									    </td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISHistFirstname" name="DISHistFirstname" style="width:100px;"> 
										</td>
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISHistLastname" name="DISHistLastname" style="width:100px;">
										</td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISHistDOB" name="DISHistDOB" style="width:100px;">
											
									     </td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISHistEnrolledOn" name="DisHistEnrolledOn" style="width:100px;"> 
										</td>
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISHistUplineNo" name="DISHistUplineNo" style="width:100px;"> 
										</td>
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right;">
											<b>Co-Distributor : </b> 
										</td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:50px;">
											
											<input class="DISHistInput" type="text"  id="DISCoHistTitle" name="DISCoHistTitle" style="width:50px;"> 
									    </td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISCoHistFirstname" name="DISCoHistFirstname" style="width:100px;"> 
										</td>
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISCoHistLastname" name="DISCoHistLastname" style="width:100px;">
										</td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISCoHistDOB" name="DISCoHistDOB" style="width:100px;">
											
									     </td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISCoHistEnrolledOn" name="DISCoHistEnrolledOn" style="width:100px;"> 
										</td>
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:100px;">
											<input class="DISHistInput" type="text"  id="DISCoHistUplineNo" name="DISCoHistUplineNo" style="width:100px;"> 
										</td>
									</tr>
									
									
									
									
							</table>
				
							
							<div class="divDISHistAddress">
								<table class="DISTable">
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:110px;text-align:right">
											Address :
										</td>
										<td colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisAddress1" name="DISHisAddress1" style="width:270px;">
										</td>
									</tr>
									<tr class="globalTrWithNoPadding" >
										<td  class="globalTdWithNoPadding" style="width:60px;text-align:right">
											
										</td>
										<td colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisAddress2" name="DISHisAddress2" style="width:270px;">
										</td class="globalTdWithNoPadding">
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:60px;text-align:right">
											
										</td>
										<td colspan="3"  class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisAddress3" name="DISHisAddress3" style="width:270px;">
										</td>
				
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:60px;text-align:right">
											City :
										</td>
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisCity" name="DISHisCity" style="width:100px;">
										</td>
										<td  class="globalTdWithNoPadding" style="width:50px;text-align:right">
											Zone :
										</td>
										<td  class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisZone" name="DISHisZone" style="width:100px;">
										</td>
									</tr>
				
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:60px;text-align:right">
											State :
										</td>
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisState" name="DISHisState" style="width:100px;">
										</td>
										<td  class="globalTdWithNoPadding" style="width:50px;text-align:right">
											Level :
										</td>
										<td  class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisLevel" name="DISHisLevel" style="width:100px;">
										</td>
									</tr>
				
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:60px;text-align:right">
											Pin :
										</td>
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisPin" name="DISHisPin" style="width:100px;">
										</td>
										<td  class="globalTdWithNoPadding" style="width:50px;text-align:right">
											Bank :
										</td>
										<td  class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisBank" name="DISHisBank" style="width:100px;">
										</td>
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:60px;text-align:right">
											
										</td>
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right">
											
										</td>
										<td  class="globalTdWithNoPadding" style="width:50px;text-align:right">
											 Saved By:
										</td>
										<td  class="globalTdWithNoPadding" style="width:100px;text-align:right">
											<input  class="DISHistInput" type="text" id="DISHisSavedBy" name="DISHisSavedBy" style="width:100px;">
										</td>
									</tr>
				
								</table>
							</div>
							
							<div class="divDISHistInfo">
								<table class="DISTable">
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:100px;text-align:right">
											Phone :
										</td>
										<td colspan="3"  class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisPhone" name="DISHisPhone" style="width:250px;">
										</td>
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:70px;text-align:right">
											Mobile : 
										</td>
										<td  colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisMobile" name="DISHisMobile" style="width:250px;">
										</td class="globalTdWithNoPadding">
									</tr>
				
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:70px;text-align:right">
											Email :
										</td>
										<td  colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisEmail" name="DISHisEmail" style="width:250px;">
										</td>
									</tr>
							
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:70px;text-align:right">
											PAN :
										</td>
										<td  colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisPan" name="DISHisPan" style="width:250px;">
										</td>
									</tr>

									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:70px;text-align:right">
											Saved On :
										</td>
										<td  colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisSavedOn" name="DISHisSavedOn" style="width:250px;">
										</td>
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:70px;text-align:right">
											Remarks :
										</td>
										<td  colspan="3" class="globalTdWithNoPadding" style="width:250px;text-align:right">
											<input class="DISHistInput" type="text" id="DISHisRemarks" name="DISHisRemarks" style="width:250px;">
										</td>
									</tr>
								</table>
							</div>
				
								
							
				
							<div>
							</div>
				
							
					</div>
				
					<div id="BankPANAccountPopUp">
						
							
							<div class="divNavigationControl">
								<button id="firstPanBankHistoryResult">|<</button>
								<button id="previousPanBankHistoryResult"><</button>
								<button id="nextPanBankHistoryResult">></button>
								
								<button id="lastPanBankHistoryResult">>|</button>
							</div>
				
				
							<table class="DISTable">
									
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											DistributorId:
										</td>	
										
										<td colspan="2" class="globalTdWithNoPadding" >
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankId" name="DISPanBankId" style="width:290px;"> 
									    </td>	
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											Distributor Name:
										</td>	
										
										<td  class="globalTdWithNoPadding" >
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankFirstName" name="DISPanBankFirstName"> 
									    </td>	
										<td  class="globalTdWithNoPadding" >
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankLastName" name="DISPanBankLastName"> 
									    </td>
									</tr>	
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											Pan Number:
										</td>	
										
										<td colspan="2" class="globalTdWithNoPadding">
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankPan" name="DISPanBankPan" style="width:290px;"> 
									    </td>	
									</tr>	
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											Bank Name:
										</td>	
										
										<td colspan="2" class="globalTdWithNoPadding">
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankBankName" name="DISPanBankBankName" style="width:290px;"> 
									    </td>	
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											Bank Account No.:
										</td>	
										
										<td colspan="2" class="globalTdWithNoPadding">
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankBankAccount" name="DISPanBankBankAccount" style="width:290px;"> 
									    </td>	
									</tr>	
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											RTGS/IFSC Code:
										</td>	
										
										<td colspan="2" class="globalTdWithNoPadding">
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankRTGS" name="DISPanBankRTGS" style="width:290px;"> 
									    </td>	
									</tr>
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											Saved By:
										</td>	
										
										<td class="globalTdWithNoPadding">
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankSavedBy" name="DISPanBankSavedBy" > 
									    </td>	
										
									</tr>	
				
									<tr class="globalTrWithNoPadding">
										<td class="globalTdWithNoPadding" style="width:140px;text-align:right;">
											Saved On:
										</td>	
										
										<td class="globalTdWithNoPadding" style="text-align:left;width:50px;">
											
											<input class="DISPanBankInput" type="text"  id="DISPanBankSavedOn" name="DISPanBankSavedOn"> 
									    </td>	
										
									</tr>
						
							</table>
					</div>
				
				
				</div>';
	}
}
?>