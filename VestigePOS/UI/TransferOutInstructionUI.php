<?php

class TransferOutInstructionScreen
{
	function transferOutInstructionHtml()
	{
		return  '<div class="DistributorInfoSearchOuterdiv">
                
					<div id="divLookUp" title="Item Search">
							
					</div>
				
					<div id="DivDistributorInfoSearchTab">
				
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
						<div id="DivSearch">
			     	 <form id="formSearchTOI">
							<div id="distributorInfoSearchAction">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">TOI Number : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOINumber" name="TOINumber">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="sourceLocation" name="SourceLocation" style="background-color:white;height: 28px;">
												
													
											</select>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="destinationLocation" name="DestinationLocation" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From TOI Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender2" type="text"  id="fromTOIDate" name="FromTOIDate">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">To TOI Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="showCalender2" type="text"  id="toTOIDate" name="ToTOIDate">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Status :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="TOIStatus" name="TOIStatus" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">From (TOI Creation) Date : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="fromTOICreationDate" name="FromTOICreationDate">
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">To (TOI Creation) Date : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="toTOICreationDate" name="ToTOICreationDate">
						
									     </td>
				
							  <td hidden class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Indented :</td>
										<td hidden class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="indentised" name="indentised" style="background-color:white;height: 28px;">
													<option value="0">Select</option>
                                                    <option value="1">1</option>
			                                    	<option value="2">2</option>				
											</select>
										</td>
									</tr>
				                        
				            
				 								
								</table>
							</div>
							<div class="divTOIActionButtons">
								<button type="button"  disabled id="btnTSF01Search" class="TOICreateActionButtons">Search</button>
				            	<button type="button" id="btnReset" class="TOICDefaultActionButtons">Reset</button>
									<input type="hidden" id="moduleCode" value="TSF01">
									<input type="hidden" id="actionName" value="">
							</div>
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivTOISearchGrid" style="width:1040px;clear:both;">
								
								<table id="TOISearchGrid"></table>
								<div id="PJmap_TOISearchGrid"></div>
									
							</div>
			          	 </form >
						</div>
						<div id="DivCreate">
				          <form id="formCreateTOI">
							<div id="distributorInfoSearchAction">
								<table class="DISTable">
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select  class="requiredList" id="createSourceLocation" hidden name="createSourceLocation" style="background-color:white;height: 28px;">
												<option value="0">Select</option>	
													
											</select>
								<Input type="text" class="requiredList" id="createSourceLocation2"  name="createSourceLocation2" style="background-color:white;height: 18px;" placeholder="Press f4 for listing"/>
											
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">TOI Number : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOICreateNumber" name="TOICreateNumber">
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select  class="requiredList" id="createDestinationLocation" hidden name="createDestinationLocation" style="height: 28px;">
												<option value="0">Select</option>
													
											</select>
					     		    <Input type="text" class="requiredList" id="createDestinationLocation2" name="createDestinationLocation2" style="background-color:white;height: 18px;" placeholder="Press f4 for listing"/>
											
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Source Address : </td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:200px;">
											<textarea class="distributor_info" id="createSourceAddress" rows="4" cols="20" name="createSourceAddress"></textarea>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Status:  </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TOICreateStatus" name="TOICreateStatus">
										</td>
										
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Destination Address :</td>
										<td rowspan="2" class="DISTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="createDestinationAddress" rows="4" cols="20" name="createDestinationAddress"></textarea>
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;"></td>
										
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Total TOI Amount : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOICreateTOIAmount" name="TOICreateTOIAmount">
						
									     </td>
										
									</tr>
									<tr class="DISTr">

										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Product Weight (in KG) : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TOICreatePOStatus" name="TOICreatePOStatus">
						
									     </td>
									<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">Total TOI Quantity :</td>
										<td class="DISTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TOICreateTOIQuantity" name="TOICreateTOIQuantity" >
										</td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Price Mode : </td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="pricemode" name="pricemode" style="background-color:white;height: 28px;">
											
													
											</select>
										</td>
										<td class="DISTd" style="text-align:right;width:100px;padding-right:10px;">Percentage : </td>
										<td class="DISTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="Percentage" name="Percentage" style="background-color:white;height: 28px;">
													
													
											</select>
						
									     </td>
										<td class="DISTd" style="text-align:right;width:125px;padding-right:10px;">App Dpp :</td>
										<td class="DISTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="appDpp" name="appDpp" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
									</tr>
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div class="DistributorMasterInformation">
								
							</div>
							<div class="DistributorCurrentRanking">
								
							</div>
							<div class="DistributorCurrentPreviousPvBvComputation">
								<table class="DISTable">
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right;">Item Code*: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="txtItemCode" name="txtItemCode" placeholder="Press f4 for listing">
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item UOM: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="txtUOM" name="txtUOM" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Item Description : </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="txtItemDescription" name="txtItemDescription" >
									     </td>
				
										<td class="DISTd" style="width:100px;text-align:right">Transfer Price: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="txtTransferPrice" name="txtTransferPrice" >
									     </td>
										
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:100px;text-align:right">Bucket*: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<select class="requiredList" id="bucket" name="bucket" style="background-color:white;height: 28px;">
													
													
											</select>
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Available Qty: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIAvailableQty" name="TOIAvailableQty" >
									     </td>
										<td class="DISTd" style="width:100px;text-align:right">Unit Qty:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIUnitQty" name="TOIUnitQty" >
									     </td>
				
				                        <td class="DISTd" style="width:100px;text-align:right">Item Weight:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIItemWeight" name="ItemWeight" >
									     </td>
									</tr>
				
									<tr class="DISTr">
										<td class="DISTd" style="width:100px;text-align:right">Item Amount: </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOIItemAmount" name="TOIItemAmount" >
									     </td>
									<td class="DISTd" style="width:100px;text-align:right">Line Weight:* </td>
										<td class="DISTd" style="text-align:left;width:100px;">
											<input class="DISTInput" type="text"  id="TOILineWeight" name="LineWeight" >
									     </td>
										
									</tr>
									
									
								</table>
								<div class="divAddClearButtons">
										<button type="button" id="btnAdd" class="btnAddSearch">Add</button>
										<button type="button" id="btnClear" class="btnAddSearch">Clear</button>
										
								</div>
							</div>
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="TOIItemGrid"></table>
								<div id="PJmap_TOIItemGrid"></div>
									
							</div>
							<div class="divTOIActionButtons">
								<button type="button" disabled id="btnTSF01Print" class="TOICreateActionButtons">Print</button>
								<button type="button" id="btnCreateReset" class="TOICDefaultActionButtons">Reset</button>
								<button type="button" disabled id="btnTSF01Cancel" class="TOICreateActionButtons">Cancel</button>
								<button type="button" disabled id="btnTSF01Confirm" class="TOICreateActionButtons">Confirm</button>
								<button type="button" disabled id="btnTSF01Save" class="TOICreateActionButtons">Save</button>
								<button type="button" disabled id="btnTSF01Reject" class="TOICreateActionButtons">Reject</button>
								
								
								
							</div>
						</div>
						 </form >
						
					</div>
				
				</div>';
	}
}


?>
