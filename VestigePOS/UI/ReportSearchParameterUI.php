<?php
class ReportSearchParameterUI
{
	/*Sales Reports Keys*/
	var $STOCKIST_SALES_REPORT = "StockistSalesReport";
	var $STOCKIST_SALES_LOG = "StockistSalesLog";
	var $CHEQUE_PAYMENT_REPORT = "CheckPaymentReport";
	var $CASH_PAYMENT_REPORT = "CashBookPaymentReport";
	var $BANK_BOOK_PAYMENT_REPORT = "BankPaymentReport";
	var $CREDIT_CARD_REPORT = "CreditCardPaymentReport";
	var $PUC_ACCOUNT_DEPOSIT = "PUCAccountDeposit";
	var $PUC_ACCOUNT_SUMMARY_REPORT = "PUCAccountSummary";
	var $CONSOLIDATED_SALES_REPORT = "ConsolidatedSalesCollectionReport";
	
	/*Inventory Reports Keys*/
	var $STN_ITEM_WISE_REPORT = "STNItemWise";
	var $RECEIVED_ITEM_WISE_REPORT = "ReceivedItemWise";
	var $PRODUCT_WISE_REPORT = "ProductWiseSales";
	var $STOCK_SUMMARY = "StockSummary";
	var $GOODS_IN_TRANSIT = "GoodsInTransit";
	var $PO_ITEM_WISE = "POItemWise";
	var $STOCK_ADJUSTMENT_SUMMARY_REPORT = "StockAdjustmentSummary";
	var $GIT_REPORT ="GITREPORT";
	var $Bo_Wise_Sales="BOWISESALES";
	var $Bo_Wise_Stock_Position="BOWISESTOCK";
	var $GRN_Report= "GRNReport";
	var $vendorretrun_Report="vendorretrun";
	
	/*Financial Reports Keys*/
	var $STN_FINANCE_REPORT = "STNFinance";
	var $Tax_Report="TaxReport";
	
	/*Other Reports*/
	var $DISTRIBUTOR_ADDRESS_LABEL_REPORT = "DistributorAddressLabel";
	var $DISTRIBUTOR_SINGLE_ADDRESS_REPORT ="DistributorSingleAddress";
	var $DISTRIBUTOR_PAN_AND_BANK_EDIT_REPORT = "DistributorPanAndBankEdit";
	var $DISTRIBUTOR_PAN_AND_BANK_STATUS_REPORT = "DistributorPanAndBankStatus";
	var $LOCATION_WISE_LIST_OF_MODIFIED_DISTRIBUTOR = "LocationWiseListOfModifiedDistributor";
	var $DISTRIBUTOR_JOINING_REPORT = "DistributorJoiningReport";
	var $MISSING_LEVEL = "MissingLevel";
	var $IN_TRANSIT_REPORT = "InTransitReport";
	var $DBR_Printing_Report = "DBRPrintingReport";
	var $Confirmed_Order_Inventory_Position = "ConfirmedOrderVsInventoryPos";
	var $SMS_REPORT = "SMSReport";
	var $NOD_REPORT = "NODReport";
	var $NOD_REPORT_FOR_USER = "NODUser";
	 var $ITEM_DETAIL = "ItemDetail";
	 var $DISTRIBUTOR_VOUCHERS = "Distributor_Vouchers";
	 var $STOCK_TRANSFER_RATE = "Stock_Transfer_Rate";
	 var $REJECT_REPORT ="Reject";
	 var $DailyNew_REPORT ="DailyNew"; //By Vikesh
	 var $DISTRIBUTOR_JOINING_REPORT_PUC = "DistributorJoiningReportForPUC"; //minku
     var $NOD_LOCATIONWISEREPORT= "NODLocationWiseReport";
     Var $CourierReport="Courier_Report";
	
	function reportSearchParameterHtml($moduleCode,$reportType,$locationId,$ifHOUser,$pcid)
	{
		
		$reportParams = "";

		
		if($moduleCode == "RCAT02" || $moduleCode== "PUCR01")
		{
			if($reportType == "GNRRegisterReport")
			{
				return  '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Item Code :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="ItemCodeId" name="ItemCode" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PO No :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="PoNoId" name="PoNo" style="width:140px; text-align:center;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											GRN :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="CommonSelect" name="GRN" id="GRNId">
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											GRN Date From :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="GRNDateFromId" name="ReportFromDate" style="width:140px; text-align:center;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											GRN Date To :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="GRNDateToId" name="ReportToDate" style="width:140px; text-align:center;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect location" name="Location" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="GRNRegisterReport"/>
							</form>
							</div>'	;
			}
			if($reportType == "SalesCollectionReport")
			{
				return  '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="disabled" id="BOLocationId" name="BOLocation" disabled>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocation">
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Username :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="UsernameId" name="Username" >
											</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" />
									     </td>									
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="SalesCollectionReport"/>
							</form>
							</div>'	;
			}
			if($reportType == "MonthwiseItemSalesReport")
			{
				return '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											
											<input type="text" class="showCalender" name="ReportToDate" id="DateToId"  />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											
											<select  class="CommonSelect disabled location" name="Location" id="LocationId" disabled>
	     	     							</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Code :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											
											<input  type="text" class="CommonSelect" name="Code" id="CodeId"  />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="MonthwiseItemSalesReport"/>
							<form>
							</div>';
			}
			
			/*Switch to get the Report Params according to report type passed*/
			switch($reportType)
			{
				case $this->BANK_BOOK_PAYMENT_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportLocation" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="BankPaymentReport"/>
						<form>
						</div>'	;
					break;
					
				case $this->STOCK_ADJUSTMENT_SUMMARY_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											
											<select  class="CommonSelect disabled" name="BOLocation" id="BOLocationId" disabled>
	     	     							</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="FromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Bucket :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											
											<select  class="CommonSelect" name="Bucket" id="BucketId" >
												<option value="-1">All</option>
												<option value="5" selected="selected">On-Hand</option>
												<option value="6">Damaged</option>
												<option value="7">Misplaced</option>
												<option value="9">Expired</option>
												<option value="10">RTV</option>
												<option value="12">Sample</option>
												<option value="13">Special Scheme </option>
												<option value="15">Export</option>
	     	     							</select>
									     </td>
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="StockAdjustmentSummary"/>
							</form>
							<div>';
					break;
				case $this->CASH_PAYMENT_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportLocation" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<input type="hidden" name="WhichReport" value="CashBookPaymentReport"/>
							</table>
						<form>
						</div>'	;
					break;
				case $this->CHEQUE_PAYMENT_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportLocation" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<input type="hidden" name="WhichReport" value="CheckPaymentReport"/>
							</table>
						<form>
						</div>'	;
					break;
					
				case $this->CONSOLIDATED_SALES_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect disabled" "id="BOLocationId" name="BOLocation" disabled>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocation">
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Username :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="UsernameId" name="Username" >
											</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="ConsolidatedSalesCollectionReport"/>
							</form>
							</div>'	;
					break;
				case $this->CREDIT_CARD_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportLocation" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="CreditCardPaymentReport"/>
						<form>
						</div>'	;
					break;
				case $this->DISTRIBUTOR_ADDRESS_LABEL_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Distributor ID :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="isDistributor" id="FromDistributorID" name="FromdistributorId" maxlength="8"/>
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="DistributorAddressLabelReport"/>
							</form>
							</div>'	;
					break;
					
				case $this->DISTRIBUTOR_BUSINESS_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Distributor ID :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="DistributorId" name="DistributorID" maxlength="8" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Downlines From :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="DownlinesFromId" name="FromDate"  />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Downlines To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DownlinesToId" name="ToDate" />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Period :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
												<select class="CommonSelect" name="Period" id="PeriodId">
													<option value="0">Current Month</option>
													<option value="1">Last Month</option>
													<option value="2">Last to Last Month</option>
												</select>
									     </td>								
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="DistributorBusinessReport"/>
							</form>
							</div>'	;
					break;
				case $this->DISTRIBUTOR_PAN_AND_BANK_EDIT_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Distributor ID :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="DistributorId" name="DistributorID" maxlength="8" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="DistributorPanAndBankEdit"/>
							</form>
							</div>'	;
					break;
				case $this->DISTRIBUTOR_PAN_AND_BANK_STATUS_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Distributor ID :
										</td>
					
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="DistributorId" name="DistributorID" maxlength="8"/>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
									</tr>
									<tr>
										<td class="DISTd" style="width:140px;text-align:right;">
											Type :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select name="Type" id="typeId"></select>
										</td>
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="DistributorPANAndBankStatusReport"/>
							</form>
							</div>'	;
					break;
				case $this->DISTRIBUTOR_SINGLE_ADDRESS_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
									<form class="ReportForm" action="" style="width:100%;height:100%;">
										<table class="ReportTable" style="width:100%;">
											<tr class="DISTr">
											<td class="DISTd" style="width:140px;text-align:right;">
												Distributor ID :
											</td>
											<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="isDistributor" id="DistributorId" name="FromdistributorId" maxlength="8" />
											</td>
											</tr>
											</table>
										<input type="hidden" name="WhichReport" value="DistributorSingleAddressReport"/>
									</form>
							</div>';
					break;
					
				case $this->GOODS_IN_TRANSIT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											From :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportFromLocation" id="LocationId"  disabled>
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											To :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="ReportToLocation" id="LocationId">
	     	     							</select>
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="GoodsInTransit"/>
						<form>
						</div>'	;
					break;
				case $this->LOCATION_WISE_LIST_OF_MODIFIED_DISTRIBUTOR:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect disabled" id="BOLocationId" name="ReportLocation" disabled>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="LocationWiseListOfModifiedDistributor"/>
							</form>
							</div>'	;
					break;
					
					case $this->DISTRIBUTOR_JOINING_REPORT_PUC:
					
						//Locationid is 1.
						if($ifHOUser == 1){
							$elementId = "ToLocationId";
						}
						else{
							$elementId = "FromLocationId";
						}
							
						$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
					
										<td class="DISTd" style="width:140px;text-align:right;visibility:hidden;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px; visibility:hidden;">
											<select  class="CommonSelect" name="BoLocationId" id="pcid">
													<option value="'.$pcid.'"></option>
													</select>
									    </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
					
							</table>
							<input type="hidden" name="WhichReport" value="DateOfJoiningForPUC"/>
						<form>
						</div>'	;
					
					
						break;
						
				case $this->DISTRIBUTOR_JOINING_REPORT:
				
					//Locationid is 1.
					if($ifHOUser == 1){
						$elementId = "ToLocationId";
					}
					else{
						$elementId = "FromLocationId";
					}
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="BoLocationId" id="'.$elementId.'">
									    </td>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
									
							</table>
							<input type="hidden" name="WhichReport" value="DateOfJoining"/>
						<form>
						</div>'	;
				
				
				break;	
								
								
				case $this->SMS_REPORT:
				
					//Locationid is 1.
					if($ifHOUser == 1){
						$elementId = "ToLocationId";
					}
					else{
						$elementId = "FromLocationId";
					}
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
									
							</table>
							<input type="hidden" name="WhichReport" value="SMSReport"/>
						<form>
						</div>'	;
				
				
				break;
                // ashwani				
				case $this->MISSING_LEVEL:
				
					//Locationid is 1.
					if($ifHOUser == 1){
						$elementId = "ToLocationId";
					}
					else{
						$elementId = "FromLocationId";
					}
						
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Missing Level Percentage :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="MissingPercentage" class="isBlank" name="MissingPercentage" style="width:140px;" />
									     </td>
										</tr>
					
							</table>
							<input type="hidden" name="WhichReport" value="MissingLevel"/>
						<form>
						</div>'	;
				
				
					break;
				
				
				// ByVikesh
				case $this->REJECT_REPORT:
					//Locationid is 1.
					if($ifHOUser == 1){
						$elementId = "ToLocationId";
					}
					else{
						$elementId = "FromLocationId";
					}
						
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Entry From  :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Entry To :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
		
							</table>
							<input type="hidden" name="WhichReport" value="Reject"/>
						<form>
						</div>'	;
						
						
					break;
				
					// ByVikesh
				case $this->DailyNew_REPORT:
					//Locationid is 1.
					if($ifHOUser == 1){
						$elementId = "ToLocationId";
					}
					else{
						$elementId = "FromLocationId";
					}
						
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Modified From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Modified To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
				
							</table>
							<input type="hidden" name="WhichReport" value="DailyNew"/>
						<form>
						</div>'	;
						
						
					break;
						

									case $this->ITEM_DETAIL:
					
											//Locationid is 1.
											if($ifHOUser == 1){
												$elementId = "ToLocationId";
											}
											else{
													$elementId = "FromLocationId";
												}
						
												$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
 									<form class="ReportForm" action="" style="width:100%;height:100%;">
 									<table class="ReportTable" style="width:100%;">
 												<tr class="DISTr">
 		
 													<td class="DISTd" style="width:140px;text-align:right;">
 														Location :
 													</td>
 												<td class="DISTd" style="text-align:left;width:50px;">
 														<select  class="CommonSelect" name="BoLocationId" id="'.$elementId.'">
 												    </td>
 													<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
 												</tr>
 		
 										</table>
 										<input type="hidden" name="WhichReport" value="ItemDetail"/>
 								<form>
 								</div>'	;
						
						
										break;
										
										case $this->DISTRIBUTOR_VOUCHERS:
										
											//Locationid is 1.
											if($ifHOUser == 1){
												$elementId = "ToLocationId";
											}
											else{
												$elementId = "FromLocationId";
											}
												
											$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
				
							            <td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											DistributorID :
										</td>
										</td>
											<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="isDistributor" id="DistributorId" name="DistributorId" maxlength="8" />
											</td>
										</tr>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
					
							</table>
							<input type="hidden" name="WhichReport" value="Distributor_Vouchers"/>
						<form>
						</div>'	;
										
										
											break;
										
										case $this->STOCK_TRANSFER_RATE:
										
											//Locationid is 1.
											if($ifHOUser == 1){
												$elementId = "ToLocationId";
											}
											else{
												$elementId = "FromLocationId";
											}
												
											$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td></tr>
						
										 <tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Source Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="BoLocationId" id="'.$elementId.'">
									    </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Destination Location :
											</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="to" id="ToLocationId1">
	     	     							</select>
									    </td>
										<input type="hidden" name="ModifyDatepickerDate" value="ModifyDatepickerDate" id="ModifyDatepickerDate"/>
									</tr>
					
							</table>
							<input type="hidden" name="WhichReport" value="Stock_Transfer_Rate"/>
						<form>
						</div>'	;
										
										
											break;
					
				case $this->PO_ITEM_WISE:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PO Number :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="PONumberId" class="isBlank" name="PONumber" style="width:140px;" />
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="POItemWise"/>
						<form>
						</div>'	;
					break;
				case $this->PRODUCT_WISE_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="dtFrom" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="dtTo" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="forLocationId" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Code :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="CodeId" name="ItemCode" style="width:140px;" />
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="ProductWiseSales"/>
						<form>
						</div>'	;
					break;
				case $this->PUC_ACCOUNT_DEPOSIT:
					
					$reportParams =  '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="disabled" id="BOLocationId" name="BOLocation" disabled>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocation">
											</select>
									     </td>
										<td class="DISTd" style="width:140px;">
											Date From :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="DateFromId" name="ReportFromDate"  />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
										Date To :
										</td>
			
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" name="ReportToDate" id="DateToId"   />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="PUCAccountDeposit"/>
							</form>
							</div>'	;
					break;
				case $this->RECEIVED_ITEM_WISE_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="dtFrom" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="dtTo" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportLocation" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
											<td class="DISTd" style="width:140px;text-align:right;">
												Code :
											</td>
											<td class="DISTd" style="width:140px;text-align:left;">
												<input type="text" id="ItemCode" name="ItemCode" style="width:140px;" />
											</td>
											
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="ReceivedItemWiseSales"/>
						<form>
						</div>'	;
					break;
				case $this->PUC_ACCOUNT_SUMMARY_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="disabled" id="BOLocationId" class="location" name="BOLocation" disabled>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocation">
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date From :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="DateFromId" name="ReportFromDate"  />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
										Date To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" name="ReportToDate" id="DateToId" />
													
									     </td>								
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="PUCAccountSummary"/>
							</form>
							</div>'	;
					break;
				case $this->STN_ITEM_WISE_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="dtFrom" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="dtTo" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											From :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="from" id="FromLocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											To :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="to" id="ToLocationId">
	     	     							</select>
									    </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="STNItemWise"/>
						<form>
						</div>'	;
					break;
					
					case $this->GIT_REPORT:
						$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
								</table>
							<input type="hidden" name="WhichReport" value="GITREPORT"/>
						<form>
						</div>'	;
							
							
						break;
						
				case $this->STN_FINANCE_REPORT:
					

					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="ReportFromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ReportToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											From :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="ReportFromLocation" id="FromLocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											To :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="ReportToLocation" id="ToLocationId">
	     	     							</select>
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="STNFinanceReport"/>
						<form>
						</div>'	;
					break;
				case $this->STOCK_SUMMARY:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="dateFrom" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="toDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled location" name="Location" id="LocationId" disabled>
	     	     							</select>
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Bucket :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											
											<select  class="CommonSelect" name="bucketId" id="bucketId" >
												<option value="-1">All</option>
												<option value="5" selected="selected">On-Hand</option>
												<option value="6">Damaged</option>
												<option value="7">Misplaced</option>
												<option value="9">Expired</option>
												<option value="10">RTV</option>
												<option value="12">Sample</option>
												<option value="13">Special Scheme </option>
												<option value="15">Export</option>
	     	     							</select>
									     </td>
									<td class="DISTd" style="width:140px;text-align:right;">
											Batch Wise :
										</td>
							<td class="DISTd" style="text-align:left;width:50px;">
											
									<input type="checkbox" name="batchwises" id="batchwise" value="101"> 		
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="StockSummary"/>
						<form>
						</div>'	;
					break;
					case $this->IN_TRANSIT_REPORT:
							
						$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Branch :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="FromLocationId" id="ToLocationId">
									     </td> 
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											Destination Location :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="ToLocationId" id="FromLocationIdAll">
									     </td>
									</tr>
							</table>
							<input type="hidden" name="WhichReport" value="InTransit"/>
						<form>
						</div>'	;
						break;
						
						
					case $this->DBR_Printing_Report:
												//Locationid is 1.
						if($locationId == 1){
							$elementId = "ToLocationId";
						}
						else{
							$elementId = "FromLocationId";
						}

						$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Branch :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="BOLocationId" id="'.$elementId.'" disabled>
									     </td>
									</tr>
									
							</table>
							<input type="hidden" name="WhichReport" value="DBRPrintReport"/>
						<form>
						</div>';
						
						break;
						
					case $this->Confirmed_Order_Inventory_Position:
						
						//Locationid is 1.
						if($locationId == 1){
							$elementId = "FromLocationId";
						}
						else{
							$elementId = "ToLocationId";
						}

						$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<!--<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>-->
										<td class="DISTd" style="width:140px;text-align:right;">
											Branch :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="BoLocationId" id="'.$elementId.'">
									    </td>
									</tr>
									
							</table>
							<input type="hidden" name="WhichReport" value="ConfirmedOrderVsInventoryPos"/>
						<form>
						</div>';
					
						break;
						
						case $this->GRN_Report:
						
							$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
							<form class="ReportForm" action="" style="width:100%;height:100%;">
							<table class="ReportTable" style="width:100%;">
							<tr class="DISTr">
							<td class="DISTd" style="width:140px;text-align:right;">
							From Date :-
							</td>
							<td class="DISTd" style="text-align:left;width:50px;">
							<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
							</td>
							<td class="DISTd" style="width:140px;text-align:right;">
								
							</td>
							<td class="DISTd" style="text-align:left;width:50px;">
								
							</td>
							<td class="DISTd" style="width:140px;text-align:right;">
							From Date :-
							</td>
							<td class="DISTd" style="text-align:left;width:50px;">
							<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
							</td>
							</tr>
								</table>
							<input type="hidden" name="WhichReport" value="GRNReport"/>
						<form>
						</div>';
						break;	

						case $this->vendorretrun_Report:
						
							$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :-
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="Fromdt" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
						
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
						
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
										 From Date :-
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="Todt" style="width:140px;" />
									    </td>
									</tr>
		
							</table>
							<input type="hidden" name="WhichReport" value="vendorretrun"/>
						<form>
						</div>';
							break;
						
						case $this->NOD_REPORT:
							
							//Locationid is 1.
							if($locationId == 1){
								$elementId = "FromLocationId";
							}
							else{
								$elementId = "ToLocationId";
							}
						
							$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
									
									<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
									
										<td class="DISTd" style="width:140px;text-align:right;">
											State :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="StateId" id="stateId">
									    </td>
										
										<td class="DISTd" style="width:140px;text-align:right;">
											Branch :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="LocationId" id="branchesFromState">
									    </td>
									</tr>
									
									</table>
									
									<input type="hidden" name="WhichReport" value="NODReport"/>
								<form>
								</div>';
								
							break;
							
							case $this->Tax_Report:
							
							//Locationid is 1.
							if($locationId == 1){
								$elementId = "FromLocationId";
							}
							else{
								$elementId = "ToLocationId";
							}
						
							$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
									
									<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
									<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											State :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select multiple class="CommonSelect" name="LocationId" id="stateId">
									    </td>
										
										
									</tr>
									
									</table>
									
									<input type="hidden" name="WhichReport" value="TaxReport"/>
								<form>
								</div>';
								
							break;
						
				case $this->STOCKIST_SALES_LOG:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect disabled" id="BOLocationId" name="BoLocationId" disabled>
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select class="CommonSelect" id="PCLocationId" name="PCLocationId">
											</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Log No :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="LogNoId" name="LogNo" />
									     </td>
									</tr>
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
										Date From :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
												<input type="text" class="showCalender" id="ReportFromDateId" name="DateFrom" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="DateTo" />
									     </td>									
									</tr>
							</table>
								<input type="hidden" name="WhichReport" value="StockistSalesLog"/>
							</form>
							</div>'	;
					break;
					
				case $this->STOCKIST_SALES_REPORT:
					
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
									<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											BO Location :
										</td>	
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect disabled" name="BOLocationId" id="BOLocationId" disabled>
	     	     							</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											PC Location :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="PClocationId" id="PCLocationId">
	     	     							</select>
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Log No :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" id="LogNoId" name="LogNo" />
									     </td>
									</tr>
									<tr class="DISTr">
										
										<td class="DISTd" style="width:140px;text-align:right;">
											Date From :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DateFromId" name="FromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DateToId" name="ToDate" />
									     </td>
									</tr>
							</table>
									<input type="hidden" name="WhichReport" value="StockistSalesReport"/>
							</form>
							</div>';
					break;
                    
                    			case $this->NOD_LOCATIONWISEREPORT:
					
						$reportParams='<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
					
                                   <table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
					
										<td class="DISTd" style="width:140px;text-align:right;">
											State :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select multiple class="CommonSelect" name="stateid" id="stateId">
									    </td>
					
										<td class="DISTd" style="width:140px;text-align:right;">
											Branch :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select multiple class="CommonSelect" name="locationid" id="branchesFromState">
									    </td>
									</tr>
					
									</table>
					
									<input type="hidden" name="WhichReport" value="NODLocationWiseReport"/>
								<form>
								</div>';
					
						break;
					
				case $this->NOD_REPORT_FOR_USER :
					$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
									<input type="hidden" id="UserId" name="UserId" value="" />
									<input type="hidden" name="WhichReport" value="NODUser"/>
							</form>
							</div>';
					break;
					
					case $this->Bo_Wise_Sales :
						$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
									<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
					
										<td class="DISTd" style="width:140px;text-align:right;">
											Date From :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DateFromId" name="FromDate" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Date To :
										</td>	
										
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DateToId" name="ToDate" />
									     </td>
									</tr>
					
									</table>
									<input type="hidden" name="WhichReport" value="BOWISESALES"/>
							</form>
							</div>';
						break;
						
						case $this->Bo_Wise_Stock_Position :
							$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
								<form class="ReportForm" action="" style="width:100%;height:100%;">
									<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
			
										<td class="DISTd" style="width:140px;text-align:right;">
											On Date :
										</td>
						
										<td class="DISTd" style="text-align:left;width:50px;">
											<input class="showCalender" type="text" id="DateToId" name="ToDate" />
									     </td>
									</tr>
			
									</table>
									<input type="hidden" name="WhichReport" value="BOWISESTOCK"/>
							</form>
							</div>';
							break;

							case $this->CourierReport:
									
								$reportParams = '<div class="SalesReportsOuterDiv" style="width:100%;height:100%;">
						<form class="ReportForm" action="" style="width:100%;height:100%;">
						<table class="ReportTable" style="width:100%;">
									<tr class="DISTr">
										<td class="DISTd" style="width:140px;text-align:right;">
											From Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportFromDateId" name="FromDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											To Date :
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<input type="text" class="showCalender" id="ReportToDateId" name="ToDate" style="width:140px;" />
									     </td>
										<td class="DISTd" style="width:140px;text-align:right;">
											Location:
										</td>
										<td class="DISTd" style="text-align:left;width:50px;">
											<select  class="CommonSelect" name="locid" id="ToLocationId">
									     </td>
									</tr>
					
							</table>
							<input type="hidden" name="WhichReport" value="Courier_Report"/>
						<form>
						</div>'	;
								break;
				
			}
			
		}
		return $reportParams;	
	}
}	
?>