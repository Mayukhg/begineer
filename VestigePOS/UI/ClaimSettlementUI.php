<?php

class ClaimSettlementScreen
{
	function claimSettementHtml()
	{
		return  '<div class="divTransferOutOuter">

					<div id="divLookUp">
							
					</div>				
				
					<div id="divTransferInTab">
						<ul>
							<li><a href="#DivSearch">Search</a></li>
							<li><a href="#DivCreate">Create</a></li>
							
						</ul>
				<form id="formSearchTI">
						<div id="DivSearch">
							<div id="divTransferOut">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">TO Number : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="TOSearchInput" type="text"  id="TINumber" name="TONumber">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Claim Number : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TINumber" name="TINumber">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Status:</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="TIStatus" name="TIStatus" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<select class="requiredList" id="TISourceLocation" name="TISourceLocation" style="background-color:white;height: 28px;">
													
													
											</select>
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Destination Location :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<select class="requiredList" id="TIDestinationLocation" name="TIDestinationLocation" style="background-color:white;height: 28px;">
													
													
											</select>
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">From (TO Ship) Date :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input type="text" class="showCalender2" id="fromTIShipDate" name="fromTIShipDate">
													
											</select>
										</td>
										
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">To (TO Ship) Date : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender2" type="text"  id="toTIShipDate" name="toTOShipDate">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Claim. From Date : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="RecFromDate" name="RecFromDate">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Claim. To Date : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="showCalender" type="text"  id="recToDate" name="recToDate">
										</td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;"> </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="checkbox" type="checkbox"  id="TIIndentised" name="TIIndentised" hidden>
												
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;"></td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="checkbox" type="checkbox"  id="TIIsExported" name="TIIsExported" hidden>
										</td>
										
										<td colspan="2" class="globalTd" style="text-align:left;width:150px;">
											<button type="button" id="btnTSF06Search" style="margin-left: 130px;" class="TOCreateActionButtons">Search</button>
											<button type="button" id="btnReset" class="btnAddSearch">Reset</button>
											
											
										</td>
										
									</tr>
										
								</table>
							</div>
							<div class="searchResultTopic">
								Search Results
							</div>
							<div id="DivTOISearchGrid" style="width:1040px;clear:both;">
								
								<table id="TISearchGrid"></table>
								<div id="PJmap_TISearchGrid"></div>
									
							</div>
						</div>
				</form>
						<div id="DivCreate">
							<div id="TransferOutCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">TO Number : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="TOSearchInput" type="text"  id="TICreateTONumber" name="TICreateTONumber" placeholder="Press f4 for listing">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Source Location : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreateTISourceLocation" name="TICreateTOSourceLocation">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Desctination Location :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateTIDestinationLocation" name="TICreateTODestinationLocation">
										</td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Claim Number : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreateTINumber" name="TICreateTINumber">
											
						
									     </td>
										<td rowspan="2" class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Source Location:  </td>
										<td rowspan="2" class="globalTd" style="text-align:left;width:150px;">
											
											<textarea class="distributor_info" id="TICreateSourceAddress" rows="4" cols="20" name="TICreateTISourceLocation"></textarea>
										</td>
										
										<td rowspan="2" class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Destination Address :</td>
										<td rowspan="2" class="globalTd" style="text-align:left;width:150px;">
											<textarea class="distributor_info" id="TICreateDestinationAddress" rows="4" cols="20" name="TOICreateDestinationAddress"></textarea>
										</td>
									</tr>
									<tr class="globalTr">
										
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Pack Size :* </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreatePackSize" name="TICreatePackSize">
						
									     </td>
										
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Status : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateStatus" name="TICreateStatus">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Source CST No. : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreateSourceCSTNo" name="TICreateSourceCSTNo">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Source VAT No. :</td>
										<td class="globalTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TICreateSourceVATNo" name="TICreateSourceVATNo" >
										</td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Source TIN No. : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateSourceTINNo" name="TICreateSourceTINNo" >
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Destination CST No. : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreateDestinationCSTNo" name="TICreateDestinationCSTNo" >
												
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Destination VAT No.:</td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateDestinationVATNo" name="TICreateDestinationVATNo" >
										</td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Destination Tin No. : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateDestinationTinNo" name="TICreateDestinationTinNo">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Claim Date :* </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="showCalender" type="text"  id="claimDate" name="ClaimDate">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Shipping Details: </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateShippingDetails" name="TICreateShippingDetails" >
										</td>
									</tr>
									<tr class="globalTr">
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Shipping Way Bill No: </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreateShippingWayBillNo" name="TICreateShippingWayBillNo" >
												
									     </td>
										<td hidden class="globalTd" style="text-align:right;width:125px;padding-right:10px;">
											Indentised:<input class="checkbox" type="checkbox"  id="TOCreateDestinationCSTNo" name="TOCreateDestinationCSTNo">
										</td>
										<td hidden class="globalTd" style="text-align:left;width:150px;">
											Is Exported:<input class="checkbox" type="checkbox"  id="TOCreateIsExported" name="TOCreateIsExported">
											
										</td>
										<td hidden class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Total TO Quantity : </td>
										<td hidden class="globalTd" style="text-align:left;width:150px;">
											<input class="DISTSearchInput" type="text"  id="TICreateTotalTOQuantity" name="TICreateTotalTOQuantity">
										</td>
										<td hidden class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Gross Weight : </td>
										<td hidden class="globalTd" style="text-align:left;width:200px;">
											<input class="DISTSearchInput" type="text"  id="TICreateGrossWeight" name="TICreateGrossWeight">
						
									     </td>
									</tr>
									<tr class="globalTr">
										
										<td hidden class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Total TI Amount :</td>
										<td hidden class="globalTd" style="text-align:left;width:75px;">
											<input class="DISTSearchInput" type="text"  id="TICreateTotalTOAmount" name="TICreateTotalTOAmount" >
										</td>
									</tr>
									<tr class="globalTr">
										
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">Remarks :</td>
										<td class="globalTd" colspan="5" rowspan="2" style="text-align:left;width:925px;">
											<textarea class="distributor_info" id="claimRemarks" rows="3" cols="20" style="width: 850px;" name="ClaimRemarks">
											</textarea>
											
										</td>
									</tr>
									<tr class="globalTr">
										
									</tr>
								</table>
								

							</div>
							<div class="searchResultTopic">
								Item Details
							</div>
							
							<div class="DistributorMasterInformation">
								
							</div>
							<div class="DistributorCurrentRanking">
								
							</div>
							<div class="DistributorCurrentPreviousPvBvComputation">
												
							</div>
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="TICreateGrid"></table>
								<div id="PJmap_TICreateGrid"></div>
									
							</div>
							<div class="divTOCreateActionButtons">
							
								<button type="button" id="btnTSF06Print" class="TOCreateActionButtons">Print</button>
								<button type="button" id="btnTSF06Save" class="TOCreateActionButtons">Raise Claim</button>
				    		    <button type="button" id="btnTSF06Confirm" class="TOCreateActionButtons">Settle Claim</button>
								<button type="button" id="btnCreateReset" class="btnAddSearch">Reset</button>
				       	<input type="hidden" id="moduleCode" value="TSF06">
									<input type="hidden" id="actionName" value="">
							</div>
						</div>
						
						
					</div>
				</div>';
	}
}


?>
