<?php

class BlockItemScreen
{
	function BlockItemHtml()
	{
		return  '<div class="divGoodReceiptNoteOuter">

					<div id="divLookUp" title="Item Search">
					</div>
				
					<div id="divGoodReceiptNoteTab">
						<ul>
							
							<li><a href="#DivCreate">Search/Create</a></li>
							
						</ul>
				
						<div id="DivCreate" style="padding:0;">
							<div id="GoodReceiptNoteCreateDiv">
								<table class="DISTable">
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Item Code : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
											<input class="BlockItemItemCode" type="text"  placeHolder="Press f4 for listing"  id="BlockItemItemCode" name="BlockItemItemCode" style="width:160px;">
										</td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Item Name. : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="text"  id="BlockItemItemName" name="BlockItemItemName">						
									     </td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">For Level :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<select class="requiredList" id="BlockItemForLevel" name="BlockItemForLevel" style="background-color:white;height: 28px;">
										</td>
									</tr>
									<tr class="globalTr">
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Location Code : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
											
											<input class="BlockItemLocationCode" type="text"  id="BlockItemLocationCode" name="BlockItemLocationCode" style="width:160px;">
						
									     </td>
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Location Name:  </td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="BlockItemLocationName" name="BlockItemLocationName">
											
											
										</td>
											<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">URL :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="text"  id="BlockItemLocationUrl" name="BlockItemLocationUrl">	
										</td>
										
									</tr>
				
									<tr class="globalTr">
									
										
										<td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Unblock : </td>
										<td class="globalTd" style="text-align:left;width:200px;">
										<input class="DISTSearchInput" type="radio"  id="BlockItemIsBlocked" value=0 name="BlockItemIsBlocked">						
									    </td>
									    <td class="globalTd" style="text-align:right;width:100px;padding-right:10px;">Block : </td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<input class="DISTSearchInput" type="radio"  id="BlockItemIsBlocked" value=1 name="BlockItemIsBlocked">
										</td>
										<td class="globalTd" style="text-align:right;width:125px;padding-right:10px;">User Tpye :</td>
										<td class="globalTd" style="text-align:left;width:150px;">
										<Select class="requiredList1" id="UserType"   name="UserType" >
										<option  value="Select">Select</option>
										<option  value="WebUser1">WebUser1</option>
										<option  value="WebUser2">WebUser2</option>
										</select>	
										</td>
									</tr>									
								</table>
							</div>
								<div class="divGRNCreateActionButtons">
								<button type="button" disabled id="btnBIT01Search" class="GRNCreateActionButtons">Search</button>
								<button type="button" disabled id="btnBIT01Save" class="GRNCreateActionButtons">Save</button>
								<button type="button"  id="btnReset" class="GRNSearchResetButtons">Reset</button>
								<button type="button" disabled id="btnBIT01Update" class="GRNCreateActionButtons">Update URL</button>
									<input type="hidden"  id="moduleCode" value="BIT01">
									<input type="hidden" id="actionName" value="">
									<input type="text" hidden class="showCalender" id="todayDate" value="">
									</div>
							<div class="searchResultTopic">
								Item Blocked On Location
							</div>
							
							<div id="DivTOICreateGrid" style="width:1040px;clear:both;">
								
								<table id="BlockItemLocationViewGrid"></table>
								<div id="PJmap_BlockItemLocationViewGrid"></div>
									
							</div>
							
							<div class="searchResultTopic">
								Blocked Items On locations
							</div>
				
							<div id="DivGRNBatchDetailsGrid" style="width:1040px;clear:both;">
								
								<table id="BlockItemItemViewGrid"></table>
								<div id="PJmap_BlockItemItemViewGrid"></div>
									
							</div>
				
							
						</div>
						
						
					</div>
				</div>';
	}
}


?>
