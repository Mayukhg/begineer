<?php
class DistributorRegitrationForm2
{
	
	public function distributorRegitrationFormHtml(){
		
		return  '<div class="outerdiv">
				
						<div id="divLookUp" title="RTGS/IFSC Code information">
							
						</div>
						
						
						<input type="hidden" id="RTGSCodeValidated">

						<input type="hidden" id="bankAccountLength">
				
						<input type="hidden" id="moduleCode" value="POS03">		
				
						<form id="distributor_reg_form" action="/drupal-7.22/sites/all/modules/Distributor_Registration/distributor_registration_data.php" method="post" enctype="multipart/form-data">
						<div class="innerdiv1">
							<table class="CommonTable">
								<tr class="globalTrWithBorder">
									<td class="globalTdWithBorder" style="width:350px;height:60px;padding-left: 10px;">
										
													Distributor No.
													<input id="distributor_no" name="distributor_no" class="most_required_fields" type="text" maxlength="255" required> 
											 
													
											 
										
									</td>
									<td class="globalTdWithBorder" style="width:350px;height:60px;">
													Serial No.
													<input id="serial_no" name="serial_no" class="disable_fields" type="text" maxlength="255" value="" readonly> 
									</td>
									<td class="globalTdWithBorder" style="width:350px;height:60px;">
										
												
												PV Month
												<input id="pv_month" name="pv_month" class="disable_fields" type="text" maxlength="255" value="" readonly> 
									</td>
									
								</tr>
							</table>
						</div>
						<div class="innerdiv2">
							<table class="CommonTable" border="1" style="width:890px;margin:0px;">
								<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="text-align:left;width:100px;padding-right:10px;padding-left:10px;">Upline no :* </td>
										
										<td class="globalTdWithBorder" style="text-align:left;width:100px;padding-right:10px;">First Name : </td>
										
										<td class="globalTdWithBorder" style="text-align:left;width:125px;padding-right:10px;">Last Name :</td>
										
				
										
										
								</tr>
								<tr class="globalTrWithBorder">
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="text-align:left;width:150px;">
				
											<input class="most_required_fields" type="text"  id="upline_no" name="upline_no" style="margin-left:10px;width:200px;">
											
										</td>
										<td class="globalTdWithBorder" style="text-align:left;width:200px;">
											<input class="disable_fields" type="text"  id="firstname" name="firstname" style="width:200px" readonly>
						
									     </td>
										<td class="globalTdWithBorder" style="text-align:left;width:150px;">
											<input class="disable_fields" type="text"  id="lastname" name="lastname" style="width:200px" readonly>
										</td>
										
								</tr>
				
							</table>
						</div>
						<div class="innerdiv3">
							<table class="CommonTable">
								<tr class="globalTrWithBorder">
				
										<td class="globalTdWithBorder" style="width:100px;padding-right:10px;"> </td>
										
				
										<td class="globalTdWithBorder" style="width:100px;padding-right:10px;">Title </td>
										
										<td class="globalTdWithBorder" style="width:125px;padding-right:10px;">First Name : </td>
										<td class="globalTdWithBorder" style="width:125px;padding-right:10px;">Last Name : </td>
										
										<td class="globalTdWithBorder" style="width:100px;padding-right:10px;">DOB(dd/mm/yyyy) :</td>
								</tr>						
				
								<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="text-align:right;width:100px;padding-right:10px;">Distributor :* </td>
										<td class="globalTdWithBorder" style="text-align:left;width:100px;">
											<select class="requiredList" id="distributorTitle" name="title" style="width:100px;background-color:white;height: 22px;">
													
											</select> 
										</td>
										<td class="globalTdWithBorder" style="width:125px;padding-right:10px;">
											<input id="distributor_firstname" name="distributor_firstname" class="required_fields" type="text" value="";style="width: 200px;">
						
									     </td>
										<td class="globalTdWithBorder" style="width:125px;padding-right:10px;">
											<input id="distributor_lastname" name="distributor_lastname" class="not_required_fields" type="text" value="" style="width: 180px;">
										</td>
										<td class="globalTdWithBorder" style="width:100px;padding-right:10px;">
											<input id="distributor_dob" name="distributor_dob" class="required_fields" type="text" value="">
										</td>
				
								</tr>
							
								<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="text-align:right;width:100px;padding-right:10px;">Co-Distributor : </td>
										<td class="globalTdWithBorder" style="text-align:left;width:100px;">
											<select class="" id="co_distributor_title" name="co_title" style="width:100px;background-color:white;height: 22px;">
													
											</select> 
										</td>
										<td class="globalTdWithBorder" style="width:125px;padding-right:10px;">
											<input id="co_distributor_firstname" name="co_distributor_firstname" class="" type="text" value="";width: 200px;">
						
									     </td>
										<td class="globalTdWithBorder" style="width:125px;padding-right:10px;">
											<input id="co_distributor_lastname" name="co_distributor_lastname" class="not_required_fields" type="text" value="" style="width: 180px;">
										</td>
										<td class="globalTdWithBorder" style="width:100px;padding-right:10px;">
											<input id="co_distributor_dob" name="co_distributor_dob" class="" type="text" value="">
										</td>
								</tr>
				
								
							</table>
							<div class="divContactInfo1">
								<table class="CommonTable">
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:100px;height:30px;">Address :*</td>
										<td class="globalTdWithBorder" style="width:300px;height:30px;"><input id="address1" name="address1" class="required_fields" type="text" maxlength="255" style="width:250px;" value="" required></td>
									</tr>
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:100px;height:30px;"></td>
										<td class="globalTdWithBorder" style="width:300px;height:30px;"><input id="address2" name="address2" class="not_required_fields" type="text" maxlength="255" style="width:250px;" value=""></td>
									</tr>
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:100px;height:30px;"></td>
										<td class="globalTdWithBorder" style="width:300px;height:30px;"><input id="address3" name="address3" class="not_required_fields" type="text" maxlength="255" style="width:250px;" value="" ></td>
	
									</tr>
								</table>
							</div>
							<div class="divContactInfo2">
								<table  class="CommonTable">
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:100px;height:30px;text-align:right">Phone :</td>
										<td class="globalTdWithBorder" style="width:200px;height:30px;"><input id="phone" name="phone" class="not_required_fields" type="text" maxlength="255" style="width:200px;" value=""></td>
									</tr>
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:100px;height:30px;text-align:right">Mobile :*</td>
										<td class="globalTdWithBorder" style="width:100px;height:30px;"><input id="mobile" name="mobile" class="most_required_fields" type="text" maxlength="255" style="width:200px;" value="" required></td>
									</tr>
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:100px;height:30px;text-align:right">Email :</td>
										<td class="globalTdWithBorder" style="width:100px;height:30px;"><input id="email" name="email" class="" type="text" maxlength="255" style="width:200px;" value=""  required></td>
	
									</tr>
								</table>
							</div>
							<table class="CommonTable">
								<tr class="globalTrWithBorder">
				                   <td class="globalTdWithBorder" style="width:150px;">Pin :
  										<input id="pin" name="pin" class="most_required_fields" type="text" maxlength="255" value=""placeholder="Hit Enter to validate"> 
  									</td>
				                    <td class="globalTdWithBorder" style="width:160px;">Country :*
  										<select  class="requiredList" id="country" name ="country" style="width:100px;height: 22px;background-color:white;">
  										
  										</select>
									</td>
  									<td class="globalTdWithBorder" style="width:190px;">State :*
  										<select  class="requiredList" id="state" name ="state" style="width:130px;height: 22px;background-color:white;">
  										
  										</select>
									</td>
									<td class="globalTdWithBorder" style="width:150px;">City :*
  										
										<select  class="requiredList" id="city" name ="city" style="width:130px;height: 22px;background-color:white;">
  										
  										</select>
				
  									</td>
  									
										
								</tr>
								<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder" style="width:140px;">Zone :*
  										<select  name="Zone" class="requiredList" id="zone" style="width:100px;height: 22px;background-color:white;">
												
										</select>
									</td>	
								</tr>
								
							</table>
							<div class="uploadDocumentDiv" hidden="true">
								<table class="CommonTable">
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder">Pan:</td>
										<td class="globalTdWithBorder"><input id="pan_checkbox1" class="checkbox_status" type="checkbox" name="distributor_pan_information"><input type="file" id="pan_checkbox" class="upload_files" name="distributor_detail[]"></td>
										<td class="globalTdWithBorder"><input type="text" placeholder="Pan Number" name="Pan" id="panNumber" class="most_required_fields1"></td>
										<td class="globalTdWithBorder"><input type="text" placeholder="IFSC Code, Press F4 for listing" name="IFSCCode" id="IFSCCode" class="most_required_fields1"></td>
										
									</tr>
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder">Cancelled Cheque:</td>
										<td class="globalTdWithBorder"><input id="cencelled_cheque_checkbox1" class="checkbox_status" type="checkbox" name="distributor_can_cheque_information" value="cancelled_check"><input type="file" id="cencelled_cheque_checkbox" class="upload_files" name="distributor_detail[]"></td>
										
										<td class="globalTdWithBorder"><select style="width:150px;height:22px;vertical-align:middle;word-wrap: normal;background-color:white;" name="DistributorBank" id="distributorBank" disabled></td>
										<td class="globalTdWithBorder"><input type="text" placeholder="Account Number" name="AccountNumber" id="AccountNumber" class="most_required_fields1"></td>
											
									</tr>
									<tr class="globalTrWithBorder">
										<td class="globalTdWithBorder">Address:</td>
										<td class="globalTdWithBorder" colspan="2" class="CommonTd"><input id="address_checkbox1" class="checkbox_status" type="checkbox" name="distributor_address_information" value="address"><input type="file" id="address_checkbox" class="upload_files" name="distributor_detail[]"></td>
										
											
									</tr>
									
								</table>
				
								<input type="hidden" id="pan_checkbox11" name="panUploadedDocument">
								<input type="hidden" id="address_checkbox11" name="addressUploadedDocument">
								<input type="hidden" id="cencelled_cheque_checkbox11" name="cancelledChequeUploadedDocument">
				
							</div>
							
											
						</div>
						
						
						
  						
						</form>
							<div class="option_for_dist_reg">
								<canvas id="distributorVarificationCapcha" style="height:40px;float:left;width: 150px;"></canvas>
								<input type="text" id="captchaText" style="float:left;margin-left:100px;" placeholder="Enter text shown">
								<button type="button" class="distributorRegistrationFormOptions" id="btnPOS03Save">Save</button>
  								<button type="button" class="distributorRegistrationFormOptions" id="Reset">Reset</button>
								<button type="button" class="distributorRegistrationFormOptions" id="Close">Close</button>
							</div>
  		
  								
									
					</div>';
	}
}

?>