USE [VestigeIndiaHo]
GO
/****** Object:  StoredProcedure [dbo].[sp_generate_inserts]    Script Date: 02/09/2014 13:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Turn system object marking on
--EXEC master.dbo.sp_MS_upd_sysobj_category 1
--GO

CREATE PROC [dbo].[sp_generate_inserts]
(
	@table_name varchar(776),  		-- The table/view for which the INSERT statements will be generated using the existing data
	@target_table varchar(776) = NULL, 	-- Use this parameter to specify a different table name into which the data will be inserted
	@include_column_list bit = 1,		-- Use this parameter to include/ommit column list in the generated INSERT statement
	@from varchar(800) = NULL, 		-- Use this parameter to filter the rows based on a filter condition (using WHERE)
	@include_timestamp bit = 0, 		-- Specify 1 for this parameter, if you want to include the TIMESTAMP/ROWVERSION column's data in the INSERT statement
	@debug_mode bit = 0,			-- If @debug_mode is set to 1, the SQL statements constructed by this procedure will be printed for later examination
	@owner varchar(64) = NULL,		-- Use this parameter if you are not the owner of the table
	@ommit_images bit = 0,			-- Use this parameter to generate INSERT statements by omitting the 'image' columns
	@ommit_identity bit = 0,		-- Use this parameter to ommit the identity columns
	@top int = NULL,			-- Use this parameter to generate INSERT statements only for the TOP n rows
	@cols_to_include varchar(8000) = NULL,	-- List of columns to be included in the INSERT statement
	@cols_to_exclude varchar(8000) = NULL,	-- List of columns to be excluded from the INSERT statement
	@disable_constraints bit = 0,		-- When 1, disables foreign key constraints and enables them after the INSERT statements
	@ommit_computed_cols bit = 0		-- When 1, computed columns will not be included in the INSERT statement
	
)
AS
BEGIN

/***********************************************************************************************************
Procedure:	sp_generate_inserts  (Build 22) 
		(Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.)
                                          
Purpose:	To generate INSERT statements from existing data. 
		These INSERTS can be executed to regenerate the data at some other location.
		This procedure is also useful to create a database setup, where in you can 
		script your data along with your table definitions.

Written by:	Narayana Vyas Kondreddi
	        http://vyaskn.tripod.com

Acknowledgements:
		Divya Kalra	-- For beta testing
		Mark Charsley	-- For reporting a problem with scripting uniqueidentifier columns with NULL values
		Artur Zeygman	-- For helping me simplify a bit of code for handling non-dbo owned tables
		Joris Laperre   -- For reporting a regression bug in handling text/ntext columns

Tested on: 	SQL Server 7.0 and SQL Server 2000

Date created:	January 17th 2001 21:52 GMT

Date modified:	May 1st 2002 19:50 GMT

Email: 		vyaskn@hotmail.com

NOTE:		This procedure may not work with tables with too many columns.
		Results can be unpredictable with huge text columns or SQL Server 2000's sql_variant data types
		Whenever possible, Use @include_column_list parameter to ommit column list in the INSERT statement, for better results
		IMPORTANT: This procedure is not tested with internation data (Extended characters or Unicode). If needed
		you might want to convert the datatypes of character variables in this procedure to their respective unicode counterparts
		like nchar and nvarchar
		

Example 1:	To generate INSERT statements for table 'titles':
		
		EXEC sp_generate_inserts 'titles'

Example 2: 	To ommit the column list in the INSERT statement: (Column list is included by default)
		IMPORTANT: If you have too many columns, you are advised to ommit column list, as shown below,
		to avoid erroneous results
		
		EXEC sp_generate_inserts 'titles', @include_column_list = 0

Example 3:	To generate INSERT statements for 'titlesCopy' table from 'titles' table:

		EXEC sp_generate_inserts 'titles', 'titlesCopy'

Example 4:	To generate INSERT statements for 'titles' table for only those titles 
		which contain the word 'Computer' in them:
		NOTE: Do not complicate the FROM or WHERE clause here. It's assumed that you are good with T-SQL if you are using this parameter

		EXEC sp_generate_inserts 'titles', @from = "from titles where title like '%Computer%'"

Example 5: 	To specify that you want to include TIMESTAMP column's data as well in the INSERT statement:
		(By default TIMESTAMP column's data is not scripted)

		EXEC sp_generate_inserts 'titles', @include_timestamp = 1

Example 6:	To print the debug information:
  
		EXEC sp_generate_inserts 'titles', @debug_mode = 1

Example 7: 	If you are not the owner of the table, use @owner parameter to specify the owner name
		To use this option, you must have SELECT permissions on that table

		EXEC sp_generate_inserts Nickstable, @owner = 'Nick'

Example 8: 	To generate INSERT statements for the rest of the columns excluding images
		When using this otion, DO NOT set @include_column_list parameter to 0.

		EXEC sp_generate_inserts imgtable, @ommit_images = 1

Example 9: 	To generate INSERT statements excluding (ommiting) IDENTITY columns:
		(By default IDENTITY columns are included in the INSERT statement)

		EXEC sp_generate_inserts mytable, @ommit_identity = 1

Example 10: 	To generate INSERT statements for the TOP 10 rows in the table:
		
		EXEC sp_generate_inserts mytable, @top = 10

Example 11: 	To generate INSERT statements with only those columns you want:
		
		EXEC sp_generate_inserts titles, @cols_to_include = "'title','title_id','au_id'"

Example 12: 	To generate INSERT statements by omitting certain columns:
		
		EXEC sp_generate_inserts titles, @cols_to_exclude = "'title','title_id','au_id'"

Example 13:	To avoid checking the foreign key constraints while loading data with INSERT statements:
		
		EXEC sp_generate_inserts titles, @disable_constraints = 1

Example 14: 	To exclude computed columns from the INSERT statement:
		EXEC sp_generate_inserts MyTable, @ommit_computed_cols = 1
***********************************************************************************************************/

SET NOCOUNT ON

--Making sure user only uses either @cols_to_include or @cols_to_exclude
IF ((@cols_to_include IS NOT NULL) AND (@cols_to_exclude IS NOT NULL))
	BEGIN
		RAISERROR('Use either @cols_to_include or @cols_to_exclude. Do not use both the parameters at once',16,1)
		RETURN -1 --Failure. Reason: Both @cols_to_include and @cols_to_exclude parameters are specified
	END

--Making sure the @cols_to_include and @cols_to_exclude parameters are receiving values in proper format
IF ((@cols_to_include IS NOT NULL) AND (PATINDEX('''%''',@cols_to_include) = 0))
	BEGIN
		RAISERROR('Invalid use of @cols_to_include property',16,1)
		PRINT 'Specify column names surrounded by single quotes and separated by commas'
		PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_include = "''title_id'',''title''"'
		RETURN -1 --Failure. Reason: Invalid use of @cols_to_include property
	END

IF ((@cols_to_exclude IS NOT NULL) AND (PATINDEX('''%''',@cols_to_exclude) = 0))
	BEGIN
		RAISERROR('Invalid use of @cols_to_exclude property',16,1)
		PRINT 'Specify column names surrounded by single quotes and separated by commas'
		PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_exclude = "''title_id'',''title''"'
		RETURN -1 --Failure. Reason: Invalid use of @cols_to_exclude property
	END


--Checking to see if the database name is specified along wih the table name
--Your database context should be local to the table for which you want to generate INSERT statements
--specifying the database name is not allowed
IF (PARSENAME(@table_name,3)) IS NOT NULL
	BEGIN
		RAISERROR('Do not specify the database name. Be in the required database and just specify the table name.',16,1)
		RETURN -1 --Failure. Reason: Database name is specified along with the table name, which is not allowed
	END

--Checking for the existence of 'user table' or 'view'
--This procedure is not written to work on system tables
--To script the data in system tables, just create a view on the system tables and script the view instead

IF @owner IS NULL
	BEGIN
		IF ((OBJECT_ID(@table_name,'U') IS NULL) AND (OBJECT_ID(@table_name,'V') IS NULL)) 
			BEGIN
				RAISERROR('User table or view not found.',16,1)
				PRINT 'You may see this error, if you are not the owner of this table or view. In that case use @owner parameter to specify the owner name.'
				PRINT 'Make sure you have SELECT permission on that table or view.'
				RETURN -1 --Failure. Reason: There is no user table or view with this name
			END
	END
ELSE
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @table_name AND (TABLE_TYPE = 'BASE TABLE' OR TABLE_TYPE = 'VIEW') AND TABLE_SCHEMA = @owner)
			BEGIN
				RAISERROR('User table or view not found.',16,1)
				PRINT 'You may see this error, if you are not the owner of this table. In that case use @owner parameter to specify the owner name.'
				PRINT 'Make sure you have SELECT permission on that table or view.'
				RETURN -1 --Failure. Reason: There is no user table or view with this name		
			END
	END

--Variable declarations
DECLARE		@Column_ID int, 		
		@Column_List varchar(8000), 
		@Column_Name varchar(128), 
		@Start_Insert varchar(786), 
		@Data_Type varchar(128), 
		@Actual_Values varchar(8000),	--This is the string that will be finally executed to generate INSERT statements
		@IDN varchar(128)		--Will contain the IDENTITY column's name in the table

--Variable Initialization
SET @IDN = ''
SET @Column_ID = 0
SET @Column_Name = ''
SET @Column_List = ''
SET @Actual_Values = ''

IF @owner IS NULL 
	BEGIN
		SET @Start_Insert = 'INSERT INTO ' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' 
	END
ELSE
	BEGIN
		SET @Start_Insert = 'INSERT ' + '[' + LTRIM(RTRIM(@owner)) + '].' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' 		
	END


--To get the first column's ID

SELECT	@Column_ID = MIN(ORDINAL_POSITION) 	
FROM	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
WHERE 	TABLE_NAME = @table_name AND
(@owner IS NULL OR TABLE_SCHEMA = @owner)



--Loop through all the columns of the table, to get the column names and their data types
WHILE @Column_ID IS NOT NULL
	BEGIN
		SELECT 	@Column_Name = QUOTENAME(COLUMN_NAME), 
		@Data_Type = DATA_TYPE 
		FROM 	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
		WHERE 	ORDINAL_POSITION = @Column_ID AND 
		TABLE_NAME = @table_name AND
		(@owner IS NULL OR TABLE_SCHEMA = @owner)



		IF @cols_to_include IS NOT NULL --Selecting only user specified columns
		BEGIN
			IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_include) = 0 
			BEGIN
				GOTO SKIP_LOOP
			END
		END

		IF @cols_to_exclude IS NOT NULL --Selecting only user specified columns
		BEGIN
			IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_exclude) <> 0 
			BEGIN
				GOTO SKIP_LOOP
			END
		END

		--Making sure to output SET IDENTITY_INSERT ON/OFF in case the table has an IDENTITY column
		IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsIdentity')) = 1 
		BEGIN
			IF @ommit_identity = 0 --Determing whether to include or exclude the IDENTITY column
				SET @IDN = @Column_Name
			ELSE
				GOTO SKIP_LOOP			
		END
		
		--Making sure whether to output computed columns or not
		IF @ommit_computed_cols = 1
		BEGIN
			IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsComputed')) = 1 
			BEGIN
				GOTO SKIP_LOOP					
			END
		END
		
		--Tables with columns of IMAGE data type are not supported for obvious reasons
		IF(@Data_Type in ('image'))
			BEGIN
				IF (@ommit_images = 0)
					BEGIN
						RAISERROR('Tables with image columns are not supported.',16,1)
						PRINT 'Use @ommit_images = 1 parameter to generate INSERTs for the rest of the columns.'
						PRINT 'DO NOT ommit Column List in the INSERT statements. If you ommit column list using @include_column_list=0, the generated INSERTs will fail.'
						RETURN -1 --Failure. Reason: There is a column with image data type
					END
				ELSE
					BEGIN
					GOTO SKIP_LOOP
					END
			END

		--Determining the data type of the column and depending on the data type, the VALUES part of
		--the INSERT statement is generated. Care is taken to handle columns with NULL values. Also
		--making sure, not to lose any data from flot, real, money, smallmomey, datetime columns
		SET @Actual_Values = @Actual_Values  +
		CASE 
			WHEN @Data_Type IN ('char','varchar','nchar','nvarchar') 
				THEN 
					'COALESCE('''''''' + REPLACE(RTRIM(' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'
			WHEN @Data_Type IN ('datetime','smalldatetime') 
				THEN 
					'COALESCE('''''''' + RTRIM(CONVERT(char,' + @Column_Name + ',109))+'''''''',''NULL'')'
			WHEN @Data_Type IN ('uniqueidentifier') 
				THEN  
					'COALESCE('''''''' + REPLACE(CONVERT(char(255),RTRIM(' + @Column_Name + ')),'''''''','''''''''''')+'''''''',''NULL'')'
			WHEN @Data_Type IN ('text','ntext') 
				THEN  
					'COALESCE('''''''' + REPLACE(CONVERT(char(8000),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'					
			WHEN @Data_Type IN ('binary','varbinary') 
				THEN  
					'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
			WHEN @Data_Type IN ('timestamp','rowversion') 
				THEN  
					CASE 
						WHEN @include_timestamp = 0 
							THEN 
								'''DEFAULT''' 
							ELSE 
								'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
					END
			WHEN @Data_Type IN ('float','real','money','smallmoney')
				THEN
					'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ',2)' + ')),''NULL'')' 
			ELSE 
				'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ')' + ')),''NULL'')' 
		END   + '+' +  ''',''' + ' + '
		
		--Generating the column list for the INSERT statement
		SET @Column_List = @Column_List +  @Column_Name + ','	

		SKIP_LOOP: --The label used in GOTO

		SELECT 	@Column_ID = MIN(ORDINAL_POSITION) 
		FROM 	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
		WHERE 	TABLE_NAME = @table_name AND 
		ORDINAL_POSITION > @Column_ID AND
		(@owner IS NULL OR TABLE_SCHEMA = @owner)


	--Loop ends here!
	END

--To get rid of the extra characters that got concatenated during the last run through the loop
SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)
SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)

IF LTRIM(@Column_List) = '' 
	BEGIN
		RAISERROR('No columns to select. There should at least be one column to generate the output',16,1)
		RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @cols_to_exclude parameter
	END

--Forming the final string that will be executed, to output the INSERT statements
IF (@include_column_list <> 0)
	BEGIN
		SET @Actual_Values = 
			'SELECT ' +  
			CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
			'''' + RTRIM(@Start_Insert) + 
			' ''+' + '''(' + RTRIM(@Column_List) +  '''+' + ''')''' + 
			' +''VALUES(''+ ' +  @Actual_Values  + '+'')''' + ' ' + 
			COALESCE(@from,' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@table_name) + ']' + '(NOLOCK)')
	END
ELSE IF (@include_column_list = 0)
	BEGIN
		SET @Actual_Values = 
			'SELECT ' + 
			CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
			'''' + RTRIM(@Start_Insert) + 
			' '' +''VALUES(''+ ' +  @Actual_Values + '+'')''' + ' ' + 
			COALESCE(@from,' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@table_name) + ']' + '(NOLOCK)')
	END	

--Determining whether to ouput any debug information
IF @debug_mode =1
	BEGIN
		PRINT '/*****START OF DEBUG INFORMATION*****'
		PRINT 'Beginning of the INSERT statement:'
		PRINT @Start_Insert
		PRINT ''
		PRINT 'The column list:'
		PRINT @Column_List
		PRINT ''
		PRINT 'The SELECT statement executed to generate the INSERTs'
		PRINT @Actual_Values
		PRINT ''
		PRINT '*****END OF DEBUG INFORMATION*****/'
		PRINT ''
	END
		
PRINT '--INSERTs generated by ''sp_generate_inserts'' stored procedure written by Vyas'
PRINT '--Build number: 22'
PRINT '--Problems/Suggestions? Contact Vyas @ vyaskn@hotmail.com'
PRINT '--http://vyaskn.tripod.com'
PRINT ''
PRINT 'SET NOCOUNT ON'
PRINT ''


--Determining whether to print IDENTITY_INSERT or not
IF (@IDN <> '')
	BEGIN
		PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@table_name) + ' ON'
		PRINT 'GO'
		PRINT ''
	END


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name, 'U') IS NOT NULL)
	BEGIN
		IF @owner IS NULL
			BEGIN
				SELECT 	'ALTER TABLE ' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
			END
		ELSE
			BEGIN
				SELECT 	'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
			END

		PRINT 'GO'
	END

PRINT ''
PRINT 'PRINT ''Inserting values into ' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' + ''''


--All the hard work pays off here!!! You'll get your INSERT statements, when the next line executes!
EXEC (@Actual_Values)

PRINT 'PRINT ''Done'''
PRINT ''


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name, 'U') IS NOT NULL)
	BEGIN
		IF @owner IS NULL
			BEGIN
				SELECT 	'ALTER TABLE ' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' CHECK CONSTRAINT ALL'  AS '--Code to enable the previously disabled constraints'
			END
		ELSE
			BEGIN
				SELECT 	'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' CHECK CONSTRAINT ALL' AS '--Code to enable the previously disabled constraints'
			END

		PRINT 'GO'
	END

PRINT ''
IF (@IDN <> '')
	BEGIN
		PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@table_name) + ' OFF'
		PRINT 'GO'
	END

PRINT 'SET NOCOUNT OFF'


SET NOCOUNT OFF
RETURN 0 --Success. We are done!
END
GO
/****** Object:  StoredProcedure [dbo].[sp_decrypt_sp]    Script Date: 02/09/2014 13:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[sp_decrypt_sp] (@objectName varchar(50))
 AS
 
 
 
 DECLARE  @OrigSpText1 nvarchar(4000),  @OrigSpText2 nvarchar(4000) , @OrigSpText3 nvarchar(4000), @resultsp nvarchar(4000)
 declare  @i int , @t bigint , @ct nvarchar(max)
 
 --get encrypted data
 SET @OrigSpText1= (SELECT top 1 ctext FROM syscomments  WHERE id = object_id(@objectName) order by colid)
 SET @OrigSpText2='ALTER PROCEDURE '+ @objectName +' WITH ENCRYPTION AS '+REPLICATE('-', 3938)
  EXECUTE (@OrigSpText2)
 print @OrigSpText1
 
 SET @OrigSpText3=(SELECT top 1 ctext FROM syscomments  WHERE id = object_id(@objectName) order by colid)
 SET @OrigSpText2='CREATE PROCEDURE '+ @objectName +' WITH ENCRYPTION AS '+REPLICATE('-', 4000-62)
 
--start counter
 SET @i=1
 --fill temporary variable
 SET @resultsp = replicate(N'A', (datalength(@OrigSpText1) / 2))
 
--loop
 WHILE @i<=datalength(@OrigSpText1)/2
 BEGIN
  --reverse encryption (XOR original+bogus+bogus encrypted)
 SET @resultsp = stuff(@resultsp, @i, 1, NCHAR(UNICODE(substring(@OrigSpText1, @i, 1)) ^
                                 (UNICODE(substring(@OrigSpText2, @i, 1)) ^
                                 UNICODE(substring(@OrigSpText3, @i, 1)))))
 print @resultsp
  SET @i=@i+1
 END
 --drop original SP
 --EXECUTE ('drop PROCEDURE '+ @objectName)
 --remove encryption
 --preserve case
 SET @resultsp=REPLACE((@resultsp),'WITH ENCRYPTION', '')
 SET @resultsp=REPLACE((@resultsp),'With Encryption', '')
 SET @resultsp=REPLACE((@resultsp),'with encryption', '')
 IF CHARINDEX('WITH ENCRYPTION',UPPER(@resultsp) )>0 
   SET @resultsp=REPLACE(UPPER(@resultsp),'WITH ENCRYPTION', '')
 --replace Stored procedure without enryption
 set @ct = (SELECT ctext FROM syscomments WHERE id = object_id(@objectName))
 print @ct
 execute( @resultsp)
GO
/****** Object:  StoredProcedure [dbo].[sp_updateInventoryInStockLedger]    Script Date: 02/09/2014 13:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_updateInventoryInStockLedger]
	(
	@TransTypeId int,
    @InventoryBatchNo Varchar(20),
    @LocationID int, 
	@ItemId int, 
	@BatchNo varchar(20),
	@NetQuantity Numeric(18,4),
	@BucketID int,
	@DiscountValue Numeric(18,4),
	@TransCostValue Numeric(18,4),
	@outParam	VARCHAR(500) OUTPUT	
	)
AS
Begin
	set nocount on ;
	

					
		BEGIN TRY
			-- NEW AND CLOSE STATE
			 DECLARE  @todayDate VARCHAR(10),
			 @Month  VARCHAR(7),
			 @Year VARCHAR(4)
			SELECT @todayDate=CONVERT(VARCHAR(10),GETDATE(),20) ;
			SELECT @Month=CONVERT(VARCHAR(7),GETDATE(),20) ;
	        SELECT @Year=CONVERT(VARCHAR(4),GETDATE(),20) ;
	        SET ROWCOUNT 0 ;
		   				UPDATE Stock_Ledger_Daily 
						SET TransQty = TransQty+@NetQuantity,
						TransDiscountValue = TransDiscountValue+@DiscountValue
						FROM  Stock_Ledger_Daily sld 
						 WHERE
						  sld.LocationId=@LocationID AND
						 sld.BatchNo=@BatchNo AND
						 sld.BucketId=@BucketID AND
						 sld.ItemId=@ItemId AND
						 sld.TransTypeId=@TransTypeId AND
						 sld.HeaderId=@InventoryBatchNo AND
						CONVERT(VARCHAR(10),sld.TransDate,20)=@todayDate;
	
		
				if(@@ROWCOUNT=0)
							BEGIN		
									INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo,
									 LocationId, ItemId, BucketId, TransTypeId, TransQty
									, TransDiscountValue)--, TransCostValue, TransTAXIN, TransTAXOUT, TransMRPValue, TransDesValue)
									SELECT  GETDATE(), @InventoryBatchNo, @BatchNo, @LocationId, @ItemId, @BucketId, @TransTypeId,@NetQuantity
									,@DiscountValue 
							END
				else if (@@ROWCOUNT>1)
				       BEGIN
				           SET @outparam ='1001'  --conflict in stock daily
				           return 
				       END
				  	 SET ROWCOUNT 0 ;
				 DECLARE  @monthlyLastId   INT             
				--UPDATE Stock_Ledger_Monthly
				--		SET Quantity = Quantity+@NetQuantity,
				--		TransDiscountValue = TransDiscountValue+@DiscountValue
				--		FROM  Stock_Ledger_Monthly slm  WHERE
				--		  slm.LocationId=@LocationID AND
				--		 slm.BatchNo=@BatchNo AND
				--		 slm.BucketId=@BucketID AND
				--		 slm.ItemId=@ItemId AND
				--		 slm.TransTypeId=@TransTypeId AND
				--		CONVERT(VARCHAR(7),slm.TransDate,20)=@month;
				
					
					
				--if(@@ROWCOUNT=0)	
				--    BEGIN	
				--		INSERT INTO Stock_Ledger_Monthly(TransDate, BatchNo,
				--		 LocationId, ItemId, BucketId, TransTypeId, Quantity
				--		, TransDiscountValue)--, TransCostValue, TransTAXIN, TransTAXOUT, TransMRPValue, TransDesValue)
				--		SELECT  CONVERT(Datetime,@todayDate,20), @BatchNo, @LocationId, @ItemId, @BucketId, @TransTypeId,@NetQuantity
				--		,@DiscountValue 
				
				--	 END
				--else if (@@ROWCOUNT>1)
				--       BEGIN
				--           SET @outparam ='1002'  --conflict in stock monthly
				--           return 
				--       END	
				--       	 SET ROWCOUNT 0 ;
				--       -- update in yearly
				--       	SET @monthlyLastId=SCOPE_IDENTITY()
				--		UPDATE Stock_Ledger_Yearly
				--		SET Quantity = Quantity+@NetQuantity,
				--		TransDiscountValue = TransDiscountValue+@DiscountValue,
				--		StockLedgerMonthlyId=ISNULL(@monthlyLastId,StockLedgerMonthlyId),
				--		TransDate=CONVERT(VARCHAR(10),GETDATE(),20)
				--		FROM  Stock_Ledger_Yearly slm  WHERE
				--		  slm.LocationId=@LocationID AND
				--		 slm.BatchNo=@BatchNo AND
				--		 slm.BucketId=@BucketID AND
				--		 slm.ItemId=@ItemId AND
				--		 slm.TransTypeId=@TransTypeId AND
				--		 CONVERT(varchar(4),slm.TransDate,20)=@Year;
						 
				--if(@@ROWCOUNT=0)	
				--    BEGIN	
				--		INSERT INTO Stock_Ledger_Yearly(TransDate, BatchNo,
				--		 LocationId, ItemId, BucketId, TransTypeId, Quantity
				--		, TransDiscountValue,StockLedgerMonthlyId)--, TransCostValue, TransTAXIN, TransTAXOUT, TransMRPValue, TransDesValue)
				--		SELECT  CONVERT(Datetime,@todayDate,20), @BatchNo, @LocationId, @ItemId, @BucketId, @TransTypeId,@NetQuantity
				--		,@DiscountValue ,@monthlyLastId
				--    END
				--else if (@@ROWCOUNT>1)		
				--    BEGIN
				--           SET @outparam ='1003'  --conflict in stock yearly
				--           return 
				--       END
				      
						
									
				       	 SET ROWCOUNT 0 ;
				     --update int inventory locationBucket batch
				     UPDATE Inventory_LocBucketBatch_History
						SET Quantity = Quantity+@NetQuantity,
						InventoryDate= CONVERT(varchar(10),InventoryDate,20)
						where
						 BucketId =@BucketID AND
					     LocationId =@LocationID AND
						 ItemId =@ItemId AND
						 BatchNo =@BatchNo AND
						 CONVERT(varchar(10),InventoryDate,20) =@todayDate
						
						if(@@ROWCOUNT=0)
						    BEGIN
									  DECLARE @maxDate varchar(20)
									  SELECT    @maxDate =MAX(ILBB.DocumentDate) from Inventory_LocBucketBatch  ILBB where ( ILBB.BatchNo=@BatchNo AND
									  ILBB.BucketId=@bucketId AND ILBB.LocationId=@LocationId AND ILBB.ItemId=@ItemId )
									  
									INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity,
									 Status, CreatedBy, CreatedDate, ReasonId, ItemName)
									SELECT @todayDate,BatchNo,LocationId,	ItemId,BucketId,Quantity
									,Status,1,GETDATE(),ReasonId,ItemName
									 from Inventory_LocBucketBatch WHERE DocumentDate=@maxDate  AND BatchNo=@BatchNo AND
									  BucketId=@bucketId AND LocationId=@LocationId AND ItemId=@ItemId
						    END
						    
						  
						ELSE if(@@ROWCOUNT>1)
						    BEGIN
						    SET @outParam='1004' ;
						    Return 
						    END
						   	 SET ROWCOUNT 0 ;
						   	 
						   	  
				        SET ROWCOUNT 0 ;
				       Update Inventory_LocBucketBatch set Quantity=Inv.Quantity+@NetQuantity 
				        , DocumentDate=GETDATE()
							FROM Inventory_LocBucketBatch  INV
							WHERE
							 BucketId =@BucketID AND
							 LocationId =@LocationID AND
							 ItemId =@ItemId AND
							 BatchNo =@BatchNo
						
						
						if(@@ROWCOUNT=0)
						    BEGIN
								
								if(@NetQuantity>0)
								  BEGIN	
									INSERT INTO Inventory_LocBucketBatch(DocumentDate, BatchNo, LocationId, 
									ItemId, BucketId, Quantity,
									 Status, CreatedBy, CreatedDate, ReasonId, ItemName)
									SELECT GETDATE(),@BatchNo,@LocationID,@ItemId,@BucketID,@NetQuantity,
									1,1,GETDATE(),1,ItemName  FROM Item_Master where ItemId=@ItemId
						          END
						        ELSE 
						         
						           BEGIN
									SET @outParam='1008' ; -- update quantity in inventory location batch can not negative
									Return 
								          
						          END
						          
						    END
						    
						  
						ELSE if(@@ROWCOUNT>1)
						    BEGIN
						    SET @outParam='1007' ;
						    Return 
						    END
						   	 SET ROWCOUNT 0 ;
						   	 
						
						
						
						   	 
						--UPDATE Stock_Ledger_Daily
						--		SET TransCostValue = im.PrimaryCost, 
						--			TransMRPValue = mrp.MRP
						--		FROM Stock_Ledger_Daily sld
						--		INNER JOIN Item_Master im 
						--		ON sld.ItemId = im.ItemId 
						--		INNER JOIN ItemMRP mrp
						--		ON sld.ItemId = mrp.ItemId
						--	WHERE sld.LocationId = @LocationId
						--	AND  CONVERT(VARCHAR(10), sld.TransDate, 120) = @todayDate
						--	  AND sld.ItemId=@ItemId AND sld.BucketId=@BucketID  AND sld.BatchNo=@BatchNo
						--		 SET ROWCOUNT 0 ;
						--	UPDATE Stock_Ledger_Monthly
						--		SET TransCostValue = im.PrimaryCost, 
						--			TransMRPValue = mrp.MRP
						--		FROM Stock_Ledger_Daily sld
						--		INNER JOIN Item_Master im 
						--		ON sld.ItemId = im.ItemId 
						--		INNER JOIN ItemMRP mrp
						--		ON sld.ItemId = mrp.ItemId
						--	WHERE sld.LocationId = @LocationId
						--	AND  CONVERT(VARCHAR(7), sld.TransDate, 120) = CONVERT(VARCHAR(7),@Month, 120)
      --                       AND sld.ItemId=@ItemId AND sld.BucketId=@BucketID  AND sld.BatchNo=@BatchNo
      --                 UPDATE Stock_Ledger_Yearly
						--		SET TransCostValue = im.PrimaryCost, 
						--			TransMRPValue = mrp.MRP
						--		FROM Stock_Ledger_Daily sld
						--		INNER JOIN Item_Master im 
						--		ON sld.ItemId = im.ItemId 
						--		INNER JOIN ItemMRP mrp
						--		ON sld.ItemId = mrp.ItemId
						--	WHERE sld.LocationId = @LocationId
						--	AND  CONVERT(VARCHAR(4), sld.TransDate, 120) = CONVERT(VARCHAR(4),@Month, 120)
      --                       AND sld.ItemId=@ItemId AND sld.BucketId=@BucketID  AND sld.BatchNo=@BatchNo
        
     			 SET ROWCOUNT 0 ;
					UPDATE InventoryLocation_Master    -- wee can not see this table when inventory update. So their is no 
							--significance of update column ONORDER 
						SET 
							AvailableQuantity	= AvailableQuantity+@NetQuantity,
							Quantity=Quantity+@NetQuantity
						FROM InventoryLocation_Master
						
						WHERE LocationId = @LocationID
						AND ItemId =@ItemId 
					
	
					IF ( @@ROWCOUNT=0 )
					       BEGIN
					          INSERT INTO InventoryLocation_Master (AvailableQuantity,BucketId,ItemId,LocationId,Quantity
					          ,QuantityOnOrder,QuantityOnTransfer)Values(@NetQuantity,@BucketID,@ItemId,@LocationID,@NetQuantity,0,0)
					       END	
					 ELSE IF (@@ROWCOUNT>1)
					  BEGIN
					   SET @outParam='1005' ;  --error in LocationMaster
						    Return 
					  END
						
			
		END TRY
		BEGIN CATCH
			Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
			Return ERROR_NUMBER()
		END CATCH	  
				
							
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateInventoryForClaim]    Script Date: 02/09/2014 13:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_updateInventoryForClaim]
	(
	@TransTypeId int,
    @InventoryBatchNo Varchar(20),
    @LocationID int, 
	@ItemId int, 
	@BatchNo varchar(20),
	@NetQuantity Numeric(18,4),
	@BucketID int,
	@DiscountValue Numeric(18,4),
	@TransCostValue Numeric(18,4),
	@outParam	VARCHAR(500) OUTPUT	
	)
AS
Begin
	set nocount on ;
	

					
		BEGIN TRY
			-- NEW AND CLOSE STATE
			 DECLARE  @todayDate VARCHAR(10),
			 @Month  VARCHAR(7),
			 @Year VARCHAR(4)
			SELECT @todayDate=CONVERT(VARCHAR(10),GETDATE(),20) ;
			SELECT @Month=CONVERT(VARCHAR(7),GETDATE(),20) ;
	        SELECT @Year=CONVERT(VARCHAR(4),GETDATE(),20) ;
	        SET ROWCOUNT 0 ;
		   				UPDATE Stock_Ledger_Daily 
						SET TransQty = TransQty+@NetQuantity,
						TransDiscountValue = TransDiscountValue+@DiscountValue
						FROM  Stock_Ledger_Daily sld 
						 WHERE
						  sld.LocationId=@LocationID AND
						 sld.BatchNo=@BatchNo AND
						 sld.BucketId=@BucketId AND
						 sld.ItemId=@ItemId AND
						 sld.TransTypeId=22 AND
						 sld.HeaderId=@InventoryBatchNo AND
						CONVERT(VARCHAR(10),sld.TransDate,20)=@todayDate;
	
		
				if(@@ROWCOUNT=0)
							BEGIN		
									INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo,
									 LocationId, ItemId, BucketId, TransTypeId, TransQty
									, TransDiscountValue)--, TransCostValue, TransTAXIN, TransTAXOUT, TransMRPValue, TransDesValue)
									SELECT  GETDATE(), @InventoryBatchNo, @BatchNo, @LocationId, @ItemId, @BucketId, 22,@NetQuantity
									,@DiscountValue 
							END
				else if (@@ROWCOUNT>1)
				       BEGIN
				           SET @outparam ='1001'  --conflict in stock daily
				           return 
				       END
				  	 SET ROWCOUNT 0 ;
				 DECLARE  @monthlyLastId   INT             
				--UPDATE Stock_Ledger_Monthly
				--		SET Quantity = Quantity+@NetQuantity,
				--		TransDiscountValue = TransDiscountValue+@DiscountValue
				--		FROM  Stock_Ledger_Monthly slm  WHERE
				--		  slm.LocationId=@LocationID AND
				--		 slm.BatchNo=@BatchNo AND
				--		 slm.BucketId=@BucketID AND
				--		 slm.ItemId=@ItemId AND
				--		 slm.TransTypeId=@TransTypeId AND
				--		CONVERT(VARCHAR(7),slm.TransDate,20)=@month;
				
					
					
				--if(@@ROWCOUNT=0)	
				--    BEGIN	
				--		INSERT INTO Stock_Ledger_Monthly(TransDate, BatchNo,
				--		 LocationId, ItemId, BucketId, TransTypeId, Quantity
				--		, TransDiscountValue)--, TransCostValue, TransTAXIN, TransTAXOUT, TransMRPValue, TransDesValue)
				--		SELECT  CONVERT(Datetime,@todayDate,20), @BatchNo, @LocationId, @ItemId, @BucketId, @TransTypeId,@NetQuantity
				--		,@DiscountValue 
				
				--	 END
				--else if (@@ROWCOUNT>1)
				--       BEGIN
				--           SET @outparam ='1002'  --conflict in stock monthly
				--           return 
				--       END	
				--       	 SET ROWCOUNT 0 ;
				--       -- update in yearly
				--       	SET @monthlyLastId=SCOPE_IDENTITY()
				--		UPDATE Stock_Ledger_Yearly
				--		SET Quantity = Quantity+@NetQuantity,
				--		TransDiscountValue = TransDiscountValue+@DiscountValue,
				--		StockLedgerMonthlyId=ISNULL(@monthlyLastId,StockLedgerMonthlyId),
				--		TransDate=CONVERT(VARCHAR(10),GETDATE(),20)
				--		FROM  Stock_Ledger_Yearly slm  WHERE
				--		  slm.LocationId=@LocationID AND
				--		 slm.BatchNo=@BatchNo AND
				--		 slm.BucketId=@BucketID AND
				--		 slm.ItemId=@ItemId AND
				--		 slm.TransTypeId=@TransTypeId AND
				--		 CONVERT(varchar(4),slm.TransDate,20)=@Year;
						 
				--if(@@ROWCOUNT=0)	
				--    BEGIN	
				--		INSERT INTO Stock_Ledger_Yearly(TransDate, BatchNo,
				--		 LocationId, ItemId, BucketId, TransTypeId, Quantity
				--		, TransDiscountValue,StockLedgerMonthlyId)--, TransCostValue, TransTAXIN, TransTAXOUT, TransMRPValue, TransDesValue)
				--		SELECT  CONVERT(Datetime,@todayDate,20), @BatchNo, @LocationId, @ItemId, @BucketId, @TransTypeId,@NetQuantity
				--		,@DiscountValue ,@monthlyLastId
				--    END
				--else if (@@ROWCOUNT>1)		
				--    BEGIN
				--           SET @outparam ='1003'  --conflict in stock yearly
				--           return 
				--       END
				      
						
									
				       	 SET ROWCOUNT 0 ;
				     --update int inventory locationBucket batch
				     UPDATE Inventory_LocBucketBatch_History
						SET Quantity = Quantity+@NetQuantity,
						InventoryDate= CONVERT(varchar(10),InventoryDate,20)
						where
						 BucketId =16 AND
					     LocationId =@LocationID AND
						 ItemId =@ItemId AND
						 BatchNo =@BatchNo AND
						 CONVERT(varchar(10),InventoryDate,20) =@todayDate
						
						if(@@ROWCOUNT=0)
						    BEGIN
									  DECLARE @maxDate varchar(20)
									  SELECT    @maxDate =MAX(ILBB.DocumentDate) from Inventory_LocBucketBatch  ILBB where ( ILBB.BatchNo=@BatchNo AND
									  ILBB.BucketId=@bucketId AND ILBB.LocationId=@LocationId AND ILBB.ItemId=@ItemId )
									  
									INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity,
									 Status, CreatedBy, CreatedDate, ReasonId, ItemName)
									SELECT @todayDate,BatchNo,LocationId,	ItemId,BucketId,Quantity
									,Status,1,GETDATE(),ReasonId,ItemName
									 from Inventory_LocBucketBatch WHERE DocumentDate=@maxDate  AND BatchNo=@BatchNo AND
									  BucketId=16 AND LocationId=@LocationId AND ItemId=@ItemId
						    END
						    
						  
						ELSE if(@@ROWCOUNT>1)
						    BEGIN
						    SET @outParam='1004' ;
						    Return 
						    END
						   	 SET ROWCOUNT 0 ;
						   	 
						   	  
				        SET ROWCOUNT 0 ;
				       Update Inventory_LocBucketBatch set Quantity=Inv.Quantity+@NetQuantity 
				        , DocumentDate=GETDATE()
							FROM Inventory_LocBucketBatch  INV
							WHERE
							 BucketId =16 AND
							 LocationId =@LocationID AND
							 ItemId =@ItemId AND
							 BatchNo =@BatchNo
						
						
						if(@@ROWCOUNT=0)
						    BEGIN
								
								if(@NetQuantity>0)
								  BEGIN	
									INSERT INTO Inventory_LocBucketBatch(DocumentDate, BatchNo, LocationId, 
									ItemId, BucketId, Quantity,
									 Status, CreatedBy, CreatedDate, ReasonId, ItemName)
									SELECT GETDATE(),@BatchNo,@LocationID,@ItemId,16,@NetQuantity,
									1,1,GETDATE(),1,ItemName  FROM Item_Master where ItemId=@ItemId
						          END
						        ELSE 
						         
						           BEGIN
									SET @outParam='1008' ; -- update quantity in inventory location batch can not negative
									Return 
								          
						          END
						          
						    END
						    
						  
						ELSE if(@@ROWCOUNT>1)
						    BEGIN
						    SET @outParam='1007' ;
						    Return 
						    END
						   	 SET ROWCOUNT 0 ;
						   	 
						
						
						
						   	 
						--UPDATE Stock_Ledger_Daily
						--		SET TransCostValue = im.PrimaryCost, 
						--			TransMRPValue = mrp.MRP
						--		FROM Stock_Ledger_Daily sld
						--		INNER JOIN Item_Master im 
						--		ON sld.ItemId = im.ItemId 
						--		INNER JOIN ItemMRP mrp
						--		ON sld.ItemId = mrp.ItemId
						--	WHERE sld.LocationId = @LocationId
						--	AND  CONVERT(VARCHAR(10), sld.TransDate, 120) = @todayDate
						--	  AND sld.ItemId=@ItemId AND sld.BucketId=@BucketID  AND sld.BatchNo=@BatchNo
						--		 SET ROWCOUNT 0 ;
						--	UPDATE Stock_Ledger_Monthly
						--		SET TransCostValue = im.PrimaryCost, 
						--			TransMRPValue = mrp.MRP
						--		FROM Stock_Ledger_Daily sld
						--		INNER JOIN Item_Master im 
						--		ON sld.ItemId = im.ItemId 
						--		INNER JOIN ItemMRP mrp
						--		ON sld.ItemId = mrp.ItemId
						--	WHERE sld.LocationId = @LocationId
						--	AND  CONVERT(VARCHAR(7), sld.TransDate, 120) = CONVERT(VARCHAR(7),@Month, 120)
      --                       AND sld.ItemId=@ItemId AND sld.BucketId=@BucketID  AND sld.BatchNo=@BatchNo
      --                 UPDATE Stock_Ledger_Yearly
						--		SET TransCostValue = im.PrimaryCost, 
						--			TransMRPValue = mrp.MRP
						--		FROM Stock_Ledger_Daily sld
						--		INNER JOIN Item_Master im 
						--		ON sld.ItemId = im.ItemId 
						--		INNER JOIN ItemMRP mrp
						--		ON sld.ItemId = mrp.ItemId
						--	WHERE sld.LocationId = @LocationId
						--	AND  CONVERT(VARCHAR(4), sld.TransDate, 120) = CONVERT(VARCHAR(4),@Month, 120)
      --                       AND sld.ItemId=@ItemId AND sld.BucketId=@BucketID  AND sld.BatchNo=@BatchNo
        
     			 SET ROWCOUNT 0 ;
					UPDATE InventoryLocation_Master    -- wee can not see this table when inventory update. So their is no 
							--significance of update column ONORDER 
						SET 
							AvailableQuantity	= AvailableQuantity+@NetQuantity,
							Quantity=Quantity+@NetQuantity
						FROM InventoryLocation_Master
						
						WHERE LocationId = @LocationID
						AND ItemId =@ItemId 
						AND BucketId=16
					
	
					IF ( @@ROWCOUNT=0 )
					       BEGIN
					          INSERT INTO InventoryLocation_Master (AvailableQuantity,BucketId,ItemId,LocationId,Quantity
					          ,QuantityOnOrder,QuantityOnTransfer)Values(@NetQuantity,@BucketID,@ItemId,@LocationID,@NetQuantity,0,0)
					       END	
					 ELSE IF (@@ROWCOUNT>1)
					  BEGIN
					   SET @outParam='1005' ;  --error in LocationMaster
						    Return 
					  END
						
			
		END TRY
		BEGIN CATCH
			Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
			Return ERROR_NUMBER()
		END CATCH	  
				
							
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PUCCheckAmount]    Script Date: 02/09/2014 13:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PUCCheckAmount]
    @PCId	INT,
    @BOId	INT,
    @totalPayment MONEY,
    @changeAmount	MONEY,
    @outParam VARCHAR(500) OUTPUT
AS 
    BEGIN
        BEGIN TRY
			DECLARE @LocType INT, 
					@locationId INT,
					@iHnd INT
				
	
				SET @iHnd = 0

				
			SELECT  @locationId = CASE @PCId
                                    WHEN 0 THEN @BOId
                                    ELSE @PCId
                                  END
                   
           
        		
			SET @LocType = 0
    		SELECT @locType = lm.LocationType FROM Location_Master lm WHERE lm.LocationId = @locationId AND lm.Status = 1
    		
    		
    		IF (@locType = 4)
    		BEGIN
    			-- Check for PUC Available amount exits or not
    			IF NOT EXISTS (SELECT 1 FROM PUCAccount p WHERE p.PCId = @locationId)
    			BEGIN
    				SET @outParam = 'INF0146'
    				RETURN
    			END
    			-- Check for PUC Available amount is less than invoice amount or not.
    			DECLARE @UsedAmount MONEY
    			SELECT @UsedAmount = SUM(TransAmount) FROM PUCTransactionDetail WHERE PCId = @locationId                        		
    			IF EXISTS(SELECT 1 FROM PUCAccount p WHERE p.PCId = @locationId AND (p.AvailableAmount - ISNULL(@UsedAmount,0)) < (@totalPayment - @changeAmount))
    			BEGIN
    				SET @outParam = 'INF0145'
    				RETURN
    			END
    		END 
        END TRY
        BEGIN CATCH
         
            SET @outParam = '30001:' + CAST(ERROR_NUMBER() AS VARCHAR(50)) + ', ' + CAST(ERROR_MESSAGE() AS VARCHAR(500)) + 'Error Line: ' + CAST(ERROR_LINE() AS VARCHAR(50)) --+ ', Source: ' + CAST (ERROR_PROCEDURE() as VARCHAR(50))
            RETURN ERROR_NUMBER()
        END CATCH
    END
GO
/****** Object:  StoredProcedure [dbo].[sp_rptStockSummary_Part2]    Script Date: 02/09/2014 13:05:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_rptStockSummary_Part2]   --'20121201','20121220',1858,5,1,''   
 @dtFrom    VARCHAR(10),    
 @dtTo    VARCHAR(10),    
 @location   INT,    
 @bucketId   INT,     
 @reportType   INT,    
 @outParam   VARCHAR(500) OUTPUT    
AS    
BEGIN    
 BEGIN TRY      
    
   DECLARE @Temp  AS TABLE    
   (    
    LocationID INT,    
    ItemId  INT,    
    Quantity NUMERIC(12,4),    
    BucketID INT,    
    TransDate DATETIME    
   )     
    
  --- Variable Declaration    
   DECLARE @FromDate  DATETIME    
   DECLARE @ToDate   DATETIME    
       
   IF(@dtFrom = '')    
    Select @FromDate = Convert(DateTime, Convert(Varchar(10),MIN(TransDate),112)) From dbo.Stock_Ledger_Daily    
   ELSE    
    SET @FromDate = Convert(DateTime, Convert(Varchar(10),@dtFrom,112))         
          
   IF(@dtTo = '')    
    Select @ToDate = GETDATE()         
   ELSE       
    SET @ToDate = Convert(DateTime, (Convert(Varchar(10),@dtTo,112)))         
       
     
   IF NOT EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Monthly_LocationId' )     
            BEGIN    
    CREATE INDEX IDX_Stock_Ledger_Monthly_LocationId ON dbo.Stock_Ledger_Monthly ( LocationId )    
    CREATE INDEX IDX_Stock_Ledger_Monthly_BatchNo ON dbo.Stock_Ledger_Monthly ( BatchNo )    
    CREATE INDEX IDX_Stock_Ledger_Monthly_ItemId ON dbo.Stock_Ledger_Monthly ( ItemId )    
    CREATE INDEX IDX_Stock_Ledger_Monthly_TransTypeId ON dbo.Stock_Ledger_Monthly ( TransTypeId )    
    CREATE INDEX IDX_Stock_Ledger_Monthly_TransDate ON dbo.Stock_Ledger_Monthly ( TransDate )    
    CREATE INDEX IDX_Stock_Ledger_Monthly_BucketId ON dbo.Stock_Ledger_Monthly ( BucketId )    
   END    
    
   IF NOT EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Daily_LocationId' )     
            BEGIN    
    CREATE INDEX IDX_Stock_Ledger_Daily_LocationId ON dbo.Stock_Ledger_Daily ( LocationId )    
    CREATE INDEX IDX_Stock_Ledger_Daily_TransDate ON dbo.Stock_Ledger_Daily ( TransDate )    
   END    
    
    
         
   INSERT INTO @Temp(ItemId,LocationId,BucketId, Quantity,TransDate)    
   SELECT sm.ItemId, sm.LocationId, sm.BucketId, SUM(sm.Quantity) NetQuantity,sm.TransDate    
   FROM Stock_Ledger_Monthly sm    
   INNER JOIN     
   (    
    SELECT BatchNo, LocationId,ItemId, BucketId, MAX(TransDate) TransDate, TransTypeId     
    FROM Stock_Ledger_Monthly    
    WHERE  ((@location = -1) OR (LocationId = @location)) AND    
    ((@FromDate = '' )OR (Convert(VARCHAR(10),TransDate,112)< Convert(VARCHAR(10),@FromDate,112)))    
    GROUP BY BatchNo, LocationId, ItemId, BucketId, TransTypeId    
   ) sm1    
   ON sm.BatchNo = sm1.BatchNo    
   AND sm.LocationId = sm1.LocationId     
   AND sm.BucketId = sm1.BucketId    
   AND sm.ItemId = sm1.ItemId      
   AND sm.TransDate = sm1.TransDate    
   AND sm.TransTypeId = sm1.TransTypeId    
   GROUP BY  sm.ItemId, sm.LocationId,  sm.TransTypeId, sm.BucketId, sm.TransDate    
       
       
    
   Declare @StockTable As table    
   (    
    ItemId INT,    
    ItemCode Varchar(50),    
    ItemName Varchar(50),    
    Bucket Varchar(20),    
    LocationID INT,    
    LocationName Varchar(50),    
    BucketID INT,    
    OpeningQty NUMERIC(12,4)Default 0,    
    TransTypeId VARCHAR(20),    
    Date DATETIME,     
    TIQty NUMERIC(12,4) Default 0,    
    STNQty NUMERIC(12,4)Default 0,    
    PackHeaderQty NUMERIC(12,4)Default 0,    
    PackChildQty NUMERIC(12,4)Default 0,    
    UnPackHeaderQty NUMERIC(12,4)Default 0,    
    UnPackChildQty NUMERIC(12,4)Default 0,    
    GRNQty NUMERIC(12,4)Default 0,    
    AdjQty NUMERIC(12,4)Default 0,    
    SoldQty NUMERIC(12,4)Default 0,    
    RTVQty NUMERIC(12,4)Default 0,    
    BalanceQty Numeric(12,4)Default 0,    
    dtFrom DateTime,    
    dtTo DateTime,    
    CustomerReturnQty  NUMERIC(12,4)Default 0,    
    Exportsub Numeric (12,4)Default 0,    
    ExportAdd Numeric (12,4)Default 0,    
    TOEXP Numeric (12,4)Default 0,    
    TIEXP Numeric (12,4)Default 0 , 
    Claim Numeric (12,4)Default 0    
   )    
    
   DECLARE @InventoryBatch  AS TABLE    
   (    
    TransTypeId VARCHAR(20),    
    TransDate DATETIME,    
    LocationID INT,    
    ItemId INT,    
    NetQuantity NUMERIC(12,4),    
    BucketID INT    
    --,    
--    Exportsub Numeric (12,4),    
--    ExportAdd Numeric (12,4)    
   )     
   -- Insert Data Into Temp Table from Stock ledger Daily table    
   INSERT INTO @StockTable (ItemId, LocationID, BucketID , OpeningQty)       
   SELECT ItemId, LocationID, BucketID , SUM(Quantity) OpeningQty    
   FROM @Temp     
   GROUP BY ItemId, LocationID, BucketID      
       
   INSERT INTO @InventoryBatch(TransTypeId, LocationID, ItemId,  NetQuantity, BucketId)    
   SELECT TransTypeId, LocationID, ItemId,  SUM(TransQty), BucketId FROM Stock_Ledger_Daily     
   WHERE (CONVERT(VARCHAR(10),TransDate,112) >=  CONVERT(VARCHAR(10),@FromDate,112)) AND     
      (CONVERT(VARCHAR(10),TransDate,112) <= CONVERT(VARCHAR(10),@ToDate,112))     
      AND ((@location = -1) OR (LocationId = @location))          
   GROUP BY TransTypeId, LocationID, ItemId, BucketId       
      
   --TI------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET TIQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 4    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, TIQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =4    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(TIQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --TO------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET STNQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 3    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, STNQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =3    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(STNQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --GRN-----------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET GRNQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 2    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, GRNQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =2    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(GRNQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --Adj MINUS FROM BUCKET----------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET AdjQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 8    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =8    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
       
   --Adj Add Qty To BUCKET----------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET AdjQty = ISNULL(AdjQty,0) + a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 7    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =7    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId     
    
   --PackHeaderQty---------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET PackHeaderQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 10    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, PackHeaderQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =10    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(PackHeaderQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
    
   --PackChildQty-----------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET PackChildQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 11    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, PackChildQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =11    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(PackChildQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --UnPackHeaderQty------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET UnPackHeaderQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 12    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, UnPackHeaderQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =12    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(UnPackHeaderQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --UnPackChildQty------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET UnPackChildQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 13    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, UnPackChildQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =13    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(UnPackChildQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --SoldQty---------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET SoldQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 1    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, SoldQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =1    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(SoldQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
       
   --RTVQty---------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET RTVQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 9    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, RTVQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =10    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(RTVQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
       
   --Customer Ret Qty-----------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET CustomerReturnQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 14    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, CustomerReturnQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =14    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(CustomerReturnQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
    
--   Select * From @InventoryBatch    
--      WHERE TransTypeId = 16    
--      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --Stock Adjust(EXP) Exportsub------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET Exportsub = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 16    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, Exportsub)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =16    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(Exportsub) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --Stock Adjust(EXP)-ExportAdd------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET ExportAdd = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 17    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, ExportAdd)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =17    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(ExportAdd) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
    
    
    
   --TI EXP------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET TIEXP = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 19    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, TIEXP)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =19    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(TIEXP) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
   --TO EXP------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET TOEXP = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 18    
      Group By TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, TOEXP)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =18    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(TOEXP) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
    
   --Stock Adj Batch Adjustment (SUB) (MINUS - Internal)FROM BUCKET----------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET AdjQty = a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 20    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =20    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
       
   --Stock Adj  - Batch Adjustment (ADD) - (ADD- Internal) BUCKET----------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET AdjQty = ISNULL(AdjQty,0) + a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 21    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =21  
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId    
    
     --Stock Adj  - Batch Adjustment (ADD) - (ADD- Internal) BUCKET----------------------------------------------------------------------------------------------------    
   UPDATE @StockTable     
    SET Claim = ISNULL(AdjQty,0) + a.NetQty     
   FROM @StockTable t    
   INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch    
      WHERE TransTypeId = 22    
      GROUP BY TransTypeId, ItemId, LocationId, BucketId    
      ) a     
   ON a.LocationId = t.LocationId    
   AND a.Bucketid = t.BucketId     
   AND a.ItemId = t.ItemId    
    
   INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)    
   SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a    
   WHERE TransTypeId =22    
    AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b     
        WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId     
        GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)    
   GROUP BY TransTypeId, ItemId, LocationId, BucketId  
      
   UPDATE @StockTable    
   SET BalanceQty = ISNULL(OpeningQty,0) + ISNULL(TIQty,0) + ISNULL(STNQty,0) + ISNULL(PackHeaderQty,0)+ ISNULL(PackChildQty,0)+    
        ISNULL(UnPackHeaderQty,0)+ISNULL(UnPackChildQty,0)+ISNULL(GRNQty,0)+ISNULL(AdjQty,0)+ISNULL(SoldQty,0)+     
        ISNULL(RTVQty,0) + ISNULL(CustomerReturnQty,0)     
        + ISNULL(TOEXP,0)+ ISNULL(TIEXP,0) + ISNULL(ExportAdd,0) + ISNULL (ExportSub,0)     
        --+ ISNULL(TOEXP,0)+ ISNULL(TIEXP,0)    
   UPDATE A    
   SET        
    A.ItemCode = B.ItemCode,    
    A.ItemName = B.ItemName,    
    A.Bucket = BKT.BucketName,    
    A.dtFrom = @FromDate,    
    A.DtTo = @ToDate,    
       A.LocationName = LM.[Name]    
   FROM Item_Master B JOIN @StockTable A    
   ON A.ItemId = B.ItemId    
   JOIN Bucket BKT    
   ON A.BucketId= BKT.BucketId    
   AND BKT.ParentId <> 'NULL'    
   JOIN Location_Master LM    
   ON A.LocationId= LM.LocationId    
    
   IF(@reportType = 1)    
    BEGIN    
     SELECT Row_Number() OVER (Partition BY a.ItemId ORDER BY a.LocationId,a.BucketId)AS SerialNo,*          
     FROM @StockTable a    
     INNER JOIN Item_Master b    
     ON a.ItemCode = b.ItemCode         
     WHERE ((@bucketId = -1 )OR(a.BucketId = @bucketId))    
     AND b.ItemId NOT IN (471, 472)    
     ORDER BY MerchHierarchyDetailId, a.ItemCode,a.LocationId,a.BucketId        
    END    
   ELSE    
    BEGIN    
     SELECT Row_Number() OVER (ORDER BY ItemId)AS SerialNo,ItemCode,ItemName,BalanceQty,LocationName,dtFrom,dtTo     
     FROM @StockTable    
     WHERE (LocationID in (SELECT LocationId FROM location_Master WHERE LocationType = @reportType AND Status = 1))     
     AND BucketId IN (SELECT BucketId FROM Bucket WHERE Bucket.ParentId IS NOT NULL AND Bucket.Sellable = 1)    
     AND ItemId NOT IN (471, 472)    
     ORDER BY ItemCode,LocationId,BucketId    
    END    
    
    
   IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Monthly_LocationId' )     
            BEGIN    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_LocationId    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BatchNo    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_ItemId    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransTypeId    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransDate    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BucketId    
   END    
    
   IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Daily_LocationId' )     
            BEGIN    
    DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_LocationId    
    DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_TransDate    
   END    
    
 END TRY    
 BEGIN CATCH    
    
   IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Monthly_LocationId' )     
            BEGIN    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_LocationId    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BatchNo    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_ItemId    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransTypeId    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransDate    
    DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BucketId    
   END    
    
   IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Daily_LocationId' )     
            BEGIN    
    DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_LocationId    
    DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_TransDate    
   END    
    
    
  SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()    
  RETURN ERROR_NUMBER()    
 END CATCH    
END
GO
/****** Object:  StoredProcedure [dbo].[sp_rptStockSummary]    Script Date: 02/09/2014 13:05:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_rptStockSummary]	
	@dtFrom				VARCHAR(10),
	@dtTo				VARCHAR(10),
	@location			INT,
	@bucketId			INT,	
	@reportType			INT,
	@outParam			VARCHAR(500) OUTPUT
AS
BEGIN
	BEGIN TRY		
  SET NOCOUNT ON
			DECLARE @Temp	 AS TABLE
			(
				LocationID	INT,
				ItemId		INT,
				Quantity	NUMERIC(12,4),
				BucketID	INT,
				TransDate	DATETIME
			)	

		--- Variable Declaration
			DECLARE @FromDate		DATETIME
			DECLARE @ToDate			DATETIME
			
			IF(@dtFrom = '')
				Select @FromDate = Convert(DateTime, Convert(Varchar(10),MIN(TransDate),112)) From dbo.Stock_Ledger_Daily
			ELSE
				SET @FromDate = Convert(DateTime, Convert(Varchar(10),@dtFrom,112))					
						
			IF(@dtTo = '')
				Select @ToDate = GETDATE()					
			ELSE			
				SET @ToDate = Convert(DateTime, (Convert(Varchar(10),@dtTo,112)))					
			
	
			IF NOT EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Monthly_LocationId' ) 
            BEGIN
				CREATE INDEX IDX_Stock_Ledger_Monthly_LocationId ON dbo.Stock_Ledger_Monthly ( LocationId )
				CREATE INDEX IDX_Stock_Ledger_Monthly_BatchNo ON dbo.Stock_Ledger_Monthly ( BatchNo )
				CREATE INDEX IDX_Stock_Ledger_Monthly_ItemId ON dbo.Stock_Ledger_Monthly ( ItemId )
				CREATE INDEX IDX_Stock_Ledger_Monthly_TransTypeId ON dbo.Stock_Ledger_Monthly ( TransTypeId )
				CREATE INDEX IDX_Stock_Ledger_Monthly_TransDate ON dbo.Stock_Ledger_Monthly ( TransDate )
				CREATE INDEX IDX_Stock_Ledger_Monthly_BucketId ON dbo.Stock_Ledger_Monthly ( BucketId )
			END

			IF NOT EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Daily_LocationId' ) 
            BEGIN
				CREATE INDEX IDX_Stock_Ledger_Daily_LocationId ON dbo.Stock_Ledger_Daily ( LocationId )
				CREATE INDEX IDX_Stock_Ledger_Daily_TransDate ON dbo.Stock_Ledger_Daily ( TransDate )
			END


					
			INSERT INTO @Temp(ItemId,LocationId,BucketId, Quantity,TransDate)
			SELECT sm.ItemId, sm.LocationId, sm.BucketId, SUM(sm.Quantity) NetQuantity,sm.TransDate
			FROM Stock_Ledger_Monthly sm
			INNER JOIN 
			(
				SELECT BatchNo, LocationId,ItemId, BucketId, MAX(TransDate) TransDate, TransTypeId 
				FROM Stock_Ledger_Monthly
				WHERE  ((@location = -1) OR (LocationId = @location)) AND
				((@FromDate = '' )OR (Convert(VARCHAR(10),TransDate,112)< Convert(VARCHAR(10),@FromDate,112)))
				GROUP BY BatchNo, LocationId, ItemId, BucketId, TransTypeId
			) sm1
			ON sm.BatchNo = sm1.BatchNo
			AND sm.LocationId = sm1.LocationId 
			AND sm.BucketId = sm1.BucketId
			AND sm.ItemId = sm1.ItemId 	
			AND sm.TransDate = sm1.TransDate
			AND sm.TransTypeId = sm1.TransTypeId
			GROUP BY  sm.ItemId, sm.LocationId,  sm.TransTypeId, sm.BucketId, sm.TransDate
			
			

			Declare @StockTable As table
			(
				ItemId INT,
				ItemCode Varchar(50),
				ItemName Varchar(50),
				Bucket	Varchar(20),
				LocationID INT,
				LocationName Varchar(50),
				BucketID INT,
				OpeningQty NUMERIC(12,4)Default 0,
				TransTypeId VARCHAR(20),
				Date DATETIME,	
				TIQty NUMERIC(12,4) Default 0,
				STNQty NUMERIC(12,4)Default 0,
				PackHeaderQty NUMERIC(12,4)Default 0,
				PackChildQty NUMERIC(12,4)Default 0,
				UnPackHeaderQty NUMERIC(12,4)Default 0,
				UnPackChildQty NUMERIC(12,4)Default 0,
				GRNQty NUMERIC(12,4)Default 0,
				AdjQty	NUMERIC(12,4)Default 0,
				SoldQty NUMERIC(12,4)Default 0,
				RTVQty NUMERIC(12,4)Default 0,
				BalanceQty Numeric(12,4)Default 0,
				dtFrom DateTime,
				dtTo DateTime,
				CustomerReturnQty  NUMERIC(12,4)Default 0,
				Exportsub Numeric (12,4)Default 0,
				ExportAdd Numeric (12,4)Default 0,
				TOEXP Numeric (12,4)Default 0,
				TIEXP Numeric (12,4)Default 0,
				Claim Numeric (12,4)Default 0
			)

			DECLARE @InventoryBatch	 AS TABLE
			(
				TransTypeId VARCHAR(20),
				TransDate DATETIME,
				LocationID INT,
				ItemId INT,
				NetQuantity	NUMERIC(12,4),
				BucketID INT
				--,
--				Exportsub Numeric (12,4),
--				ExportAdd Numeric (12,4)
			)	
			-- Insert Data Into Temp Table from Stock ledger Daily table
			INSERT INTO @StockTable (ItemId, LocationID, BucketID , OpeningQty)			
			SELECT ItemId, LocationID, BucketID , SUM(Quantity) OpeningQty
			FROM @Temp 
			GROUP BY ItemId, LocationID, BucketID		
			
			INSERT INTO @InventoryBatch(TransTypeId, LocationID, ItemId,  NetQuantity, BucketId)
			SELECT TransTypeId, LocationID, ItemId,  SUM(TransQty), BucketId FROM Stock_Ledger_Daily 
			WHERE (CONVERT(VARCHAR(10),TransDate,112) >=  CONVERT(VARCHAR(10),@FromDate,112)) AND 
				  (CONVERT(VARCHAR(10),TransDate,112) <= CONVERT(VARCHAR(10),@ToDate,112)) 
				  AND ((@location = -1) OR (LocationId = @location))				  
			GROUP BY TransTypeId, LocationID, ItemId, BucketId			
		
			--TI------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET TIQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 4
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, TIQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =4
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(TIQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--TO------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET STNQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 3
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, STNQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =3
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(STNQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--GRN-----------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET GRNQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 2
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, GRNQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =2
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(GRNQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--Adj MINUS FROM BUCKET----------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET AdjQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 8
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =8
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId
			
			--Adj Add Qty To BUCKET----------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET AdjQty = ISNULL(AdjQty,0) + a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 7
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =7
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId	

			--PackHeaderQty---------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET PackHeaderQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 10
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, PackHeaderQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =10
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(PackHeaderQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId


			--PackChildQty-----------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET PackChildQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 11
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, PackChildQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =11
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(PackChildQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--UnPackHeaderQty------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET UnPackHeaderQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 12
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, UnPackHeaderQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =12
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(UnPackHeaderQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--UnPackChildQty------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET UnPackChildQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 13
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, UnPackChildQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =13
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(UnPackChildQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--SoldQty---------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET SoldQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 1
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, SoldQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =1
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(SoldQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId
			
			--RTVQty---------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET RTVQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 9
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, RTVQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =10
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(RTVQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId
			
			--Customer Ret Qty-----------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET CustomerReturnQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 14
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, CustomerReturnQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =14
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(CustomerReturnQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId


--			Select * From @InventoryBatch
--						WHERE TransTypeId = 16
--						GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--Stock Adjust(EXP) Exportsub------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET Exportsub = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 16
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, Exportsub)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =16
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(Exportsub) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--Stock Adjust(EXP)-ExportAdd------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET ExportAdd = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 17
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, ExportAdd)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =17
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(ExportAdd) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId




			--TI EXP------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET TIEXP = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 19
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, TIEXP)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =19
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(TIEXP) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

			--TO EXP------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET TOEXP = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 18
						Group By TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId


			INSERT INTO @StockTable (ItemId, LocationId, BucketId, TOEXP)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =18
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(TOEXP) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId


			--Stock Adj Batch Adjustment (SUB) (MINUS - Internal)FROM BUCKET----------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET AdjQty = a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 20
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =20
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId
			
			--Stock Adj  - Batch Adjustment (ADD) - (ADD- Internal) BUCKET----------------------------------------------------------------------------------------------------
			UPDATE @StockTable 
				SET AdjQty = ISNULL(AdjQty,0) + a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 21
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, AdjQty)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =21
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

    			UPDATE @StockTable 
				SET Claim = ISNULL(AdjQty,0) + a.NetQty 
			FROM @StockTable t
			INNER JOIN (Select ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty From @InventoryBatch
						WHERE TransTypeId = 22
						GROUP BY TransTypeId, ItemId, LocationId, BucketId
						) a 
			ON a.LocationId = t.LocationId
			AND a.Bucketid = t.BucketId 
			AND a.ItemId = t.ItemId

			INSERT INTO @StockTable (ItemId, LocationId, BucketId, Claim)
			SELECT ItemId, LocationId, BucketId, SUM(NetQuantity) NetQty FROM @InventoryBatch a
			WHERE TransTypeId =22
				AND NOT EXISTS (SELECT ItemId, LocationId, BucketId, SUM(AdjQty) NetQty FROM @StockTable b 
								WHERE b.ItemId = a.ItemId AND b.LocationId = a.LocationId AND a.BucketId = b.BucketId 
								GROUP BY b.TransTypeId, b.ItemId, b.LocationId, b.BucketId)
			GROUP BY TransTypeId, ItemId, LocationId, BucketId

		
			UPDATE @StockTable
			SET BalanceQty =	ISNULL(OpeningQty,0) + ISNULL(TIQty,0) + ISNULL(STNQty,0) + ISNULL(PackHeaderQty,0)+ ISNULL(PackChildQty,0)+
								ISNULL(UnPackHeaderQty,0)+ISNULL(UnPackChildQty,0)+ISNULL(GRNQty,0)+ISNULL(AdjQty,0)+ISNULL(SoldQty,0)+ 
								ISNULL(RTVQty,0) + ISNULL(CustomerReturnQty,0) 
								+ ISNULL(TOEXP,0)+ ISNULL(TIEXP,0) + ISNULL(ExportAdd,0) + ISNULL (ExportSub,0)
								--+ ISNULL(TOEXP,0)+ ISNULL(TIEXP,0)
			UPDATE A
			SET				
				A.ItemCode = B.ItemCode,
				A.ItemName = B.ItemName,
				A.Bucket = BKT.BucketName,
				A.dtFrom = @FromDate,
				A.DtTo = @ToDate,
			    A.LocationName = LM.[Name]
			FROM Item_Master B JOIN @StockTable A
			ON A.ItemId = B.ItemId
			JOIN Bucket BKT
			ON A.BucketId= BKT.BucketId
			AND BKT.ParentId <> 'NULL'
			JOIN Location_Master LM
			ON A.LocationId= LM.LocationId

			IF(@reportType = 1)
				BEGIN
					SELECT Row_Number() OVER (Partition BY a.ItemId ORDER BY a.LocationId,a.BucketId)AS SerialNo,* 					
					FROM @StockTable a
					INNER JOIN Item_Master b
					ON a.ItemCode = b.ItemCode					
					WHERE ((@bucketId = -1 )OR(a.BucketId = @bucketId))
					AND b.ItemId NOT IN (471, 472)
					ORDER BY MerchHierarchyDetailId, a.ItemCode,a.LocationId,a.BucketId				
				END
			ELSE
				BEGIN
					SELECT Row_Number() OVER (ORDER BY ItemId)AS SerialNo,ItemCode,ItemName,BalanceQty,LocationName,dtFrom,dtTo 
					FROM @StockTable
					WHERE (LocationID in (SELECT LocationId FROM location_Master WHERE LocationType = @reportType AND Status = 1)) 
					AND BucketId IN (SELECT BucketId FROM Bucket WHERE Bucket.ParentId IS NOT NULL AND Bucket.Sellable = 1)
					AND ItemId NOT IN (471, 472)
					ORDER BY ItemCode,LocationId,BucketId
				END


			IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Monthly_LocationId' ) 
            BEGIN
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_LocationId
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BatchNo
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_ItemId
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransTypeId
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransDate
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BucketId
			END

			IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Daily_LocationId' ) 
            BEGIN
				DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_LocationId
				DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_TransDate
			END

	END TRY
	BEGIN CATCH

			IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Monthly_LocationId' ) 
            BEGIN
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_LocationId
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BatchNo
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_ItemId
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransTypeId
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_TransDate
				DROP INDEX dbo.Stock_Ledger_Monthly.IDX_Stock_Ledger_Monthly_BucketId
			END

			IF EXISTS ( SELECT  1 FROM    sysindexes WHERE   [NAME] = 'IDX_Stock_Ledger_Daily_LocationId' ) 
            BEGIN
				DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_LocationId
				DROP INDEX dbo.Stock_Ledger_Daily.IDX_Stock_Ledger_Daily_TransDate
			END


		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_SendMailToDistributor]    Script Date: 02/09/2014 13:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_SendMailToDistributor]    
as    
begin    
create table #SendMailToAllDistributor(distributorid int ,DistributorEmailID varchar(50),Month_oF_Invoice int)    
insert into #SendMailToAllDistributor    
select distinct dm.distributorid,dm.DistributorEMailID ,DATEDIFF(month,InvoiceConsiderDate, GETDATE()) as Month_oF_Invoice    
from distributormaster(NOLOCK) dm inner join CIHeader(NOLOCK) ICD    
on dm.distributorid=ICD.DistributorId    
Where     
dm .DistributorStatus='1'    
and dm.DistributorEMailID LIKE '%_@__%.__%' and DistributorEMailID!=''    
delete from #SendMailToAllDistributor where DATEDIFF(month,Month_oF_Invoice, GETDATE())<=6     
select distinct distributorid, DistributorEmailID from #SendMailToAllDistributor    
end
GO
/****** Object:  StoredProcedure [dbo].[sp_SearchTOIAvailWQty]    Script Date: 02/09/2014 13:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_SearchTOIAvailWQty]
    
	@ItemCode       varchar(20),
	@locationId     int,
	@bucketId        int,
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	set nocount on ;
	   Declare @isLocationOnline INT 
	      Select @isLocationOnline= IsLocationOnline from Location_Master where LocationId=@locationId
	     if @isLocationOnline=0
	     BEGIN
	      Select 
					LocationId, 
					AvailableQuantity  - ISNULL(QuantityOnTransfer,0) As Quantity, 
					bwi.ItemId, 
					im.ItemCode,
					BucketName ,
					bwi.BucketId
			From InventoryLocation_Master bwi
			Inner Join Bucket buc
			On bwi.BucketId = buc.BucketId And 
			buc.Status = 1 AND buc.ParentId Is Not Null
			AND buc.BucketId<>9 --Intransit

			Inner Join Item_Master im 
			On im.ItemId = bwi.ItemId
			Where im.Status = 1
			and LocationId=@locationId and ItemCode=@ItemCode  AND BWI.BucketId=@bucketId
	     END
	     Else
	        BEGIN
	        Select 
					ILBB.LocationId, 
					ISNULL(SUM(ILBB.Quantity),0) Quantity  ,
					ILBB.ItemId, 
					im.ItemCode,
					B.BucketName ,
					B.BucketId
			From Inventory_LocBucketBatch ILBB
			Inner Join ItemBatch_Detail IBD
			ON ILBB.ItemId=IBD.ItemId AND ILBB.BatchNo=IBD.BatchNo
			INNER JOIN Bucket B On B.BucketId=ILBB.BucketId 
			Inner Join Item_Master im 
			On im.ItemId = IBD.ItemId
			Where im.Status = 1
			and LocationId=@locationId and ItemCode=@ItemCode  AND ILBB.BucketId=@bucketId AND IBD.ExpDate>GETDATE()
			GROUP By ILBB.LocationId,ILBB.ItemId, im.ItemCode,B.BucketName ,B.BucketId
	        END
	     
	END TRY
	BEGIN CATCH
		
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		ROLLBACK 
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InventoryAdjustSearch]    Script Date: 02/09/2014 13:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_InventoryAdjustSearch]
	@LocationId				Int,
			@SeqNo					Varchar(20),			
			@FromInitiateDate		Varchar(20),			@ToInitiateDate		Varchar(20),
			@Status					Int,					
			@ApprovedBy				Int,					@RejectedBy				Int,
			@isexported				Int,					@InternalBatAdj		Int,
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	  set nocount on ;
Declare @tab Table
(
adjustno varchar(20),
reasoncode int
)
Insert into @tab
select Distinct(AdjustmentNo), ReasonCode from InventoryAdj_DetailEntry
Order By AdjustmentNo Desc
		--Select @Status, @SeqNo
		Select DISTINCT iae.AdjustmentNo, Convert(Varchar(10),Date, 120) As InitiatedDate, iae.LocationId, ApprovedBy, Convert(Varchar(10),ApprovedRejectedDate, 120) ApprovedDate, 
		iae.UserId as InitiatedBy, 
		IsNull(um.FirstName + ' ' + um.LastName,'') as InitiatedName, 
		um_1.FirstName + ' ' + um_1.LastName as ApprovedName, 
		lm.Name + '-' + IsNull(lm.Address1,'') + ' ' + IsNull(lm.Address2,'') + ' ' + IsNull(lm.Address3,'') + ' ' + IsNull(cm.CityName,'') +  ' ' + IsNull(sm.StateName,'') + ' '  + IsNull(com.CountryName,'') As LocationName, 
		IsNull(lm.Address1,'') + ' ' + IsNull(lm.Address2,'') + ' ' + IsNull(lm.Address3,'') + ' ' +cm.CityName +  ' ' + sm.StateName + ' '  + com.CountryName As LocationAddress
		,StatusName = Case		When ApprovedBy Is Null AND RejectedBy Is Null  And iae.UserId Is Null  Then 'Created'
								When ApprovedBy Is Null And RejectedBy Is Null  AND iae.UserId Is Not Null  Then 'Initiated'
								When ApprovedBy Is Not Null AND RejectedBy Is Null And iae.UserId Is Not Null  Then 'Approved'
								When ApprovedBy Is Null And RejectedBy Is Not Null And iae.UserId Is Not Null  Then 'Rejected'
						END
		, StatusId = Case		When ApprovedBy Is Null AND RejectedBy Is Null And iae.UserId Is Null  Then 1
								When ApprovedBy Is Null AND RejectedBy Is Null And iae.UserId Is Not Null  Then 2
								When ApprovedBy Is Not Null And RejectedBy Is Null And iae.UserId Is Not Null  Then 3
								When ApprovedBy Is Null  And RejectedBy Is Not Null And iae.UserId Is Not Null  Then 4
						END
		,Exportstatus = Case	When (T.reasoncode = '10' ) Then 1 Else 0 END
		,Interadjstatus = CASE	WHEN iae.IsInternalBatchAdj = 'True' Then 1 Else 0 END
		,iae.ModifiedBy, iae.ModifiedDate
		 From dbo.InventoryAdj_Entry iae
		
		inner Join @tab T
		On iae.AdjustmentNo= T.adjustno
		
		Inner Join Location_Master lm 
		On lm.LocationId = iae.LocationId

		Left Join dbo.User_Master um
		On um.UserId = iae.UserId
		
		Left Join dbo.User_Master um_1
		On (um_1.UserId = iae.ApprovedBy  AND iae.ApprovedBy IS NOT NULL OR um_1.UserId = iae.RejectedBy  AND iae.RejectedBy IS NOT NULL)

		Left join City_Master cm 
		On cm.CityId = lm.CityId

		Left join state_master sm 
		On sm.StateId = lm.StateId

		Left join Country_Master com 
		On com.CountryId = lm.CountryId
		
		--inner join Parameter_Master pm 
		--On pm.KeyCode1 = Ide.ReasonCode

		Where	(IsNull(@LocationId,'-1')='-1' Or iae.LocationId = @LocationId)
		And (@Status = Case		When ApprovedBy Is Null AND RejectedBy Is Null And iae.UserId Is Null  Then 1
								When ApprovedBy Is Null AND RejectedBy Is Null And iae.UserId Is Not Null  Then 2
								When ApprovedBy Is Not Null And RejectedBy Is Null And iae.UserId Is Not Null  Then 3
								When ApprovedBy Is Null  And RejectedBy Is Not Null And iae.UserId Is Not Null  Then 4
						END
			OR IsNull(@Status,-1)=-1)
						
		And		(IsNull(NullIf(@SeqNo,''),'-1')='-1' Or iae.AdjustmentNo Like '%' + @SeqNo)
		AND		(IsNull(@FromInitiateDate,'')='' OR Convert(varchar(10),IsNull(Date,'2099-01-01'),101) >= Convert(varchar(10),CAST(@FromInitiateDate As DateTime),101))
		AND		(IsNull(@ToInitiateDate,'')='' OR Convert(varchar(10),IsNull(Date,'1900-01-01'),101) <= Convert(varchar(10),Cast(@ToInitiateDate As DateTime),101))
		And		(IsNull(NullIf(@ApprovedBy,''),'-1')='-1' Or iae.ApprovedBy = @ApprovedBy Or iae.RejectedBy = @ApprovedBy)
		AND		(IsNull(@Isexported,'2')='2' Or IsNull(@Isexported,'-1')='-1' Or @Isexported = Case When T.reasoncode = '10' Then 1 END  Or @Isexported = Case When T.reasoncode <> '10' Then 0 End)
		AND		(IsNull(@InternalBatAdj,'2')='2' Or IsNull(@InternalBatAdj,'-1')='-1' Or @InternalBatAdj = Case When iae.IsInternalBatchAdj = 'True' Then 1 END  Or @InternalBatAdj = Case When iae.IsInternalBatchAdj = 'False' Then 0 End)
		--AND		(IsNull(@Isexported,'2')='2' Or IsNull(@Isexported,'-1')='-1' Or @Isexported = Case When (Select ReasonCode from InventoryAdj_DetailEntry where ReasonCode = '10')='10' Then 1 END  Or @Isexported = Case When (Select ReasonCode from InventoryAdj_DetailEntry where ReasonCode  <> '10')<>'10' Then 0 End)
		--And		(IsNull(NullIf(@ApprovedBy,''),'-1')='-1' Or iae.RejectedBy = @ApprovedBy)
		ORDER BY iae.AdjustmentNo DESC
	End Try
	Begin Catch
		Set @outParam = '30001:' + CAST(IsNull(Error_Number(),'') AS VarChar(50)) + ', ' + IsNUll(Error_Message(),'') + 'Error Line: ' + CAST(IsNull(Error_Line(),'') As VarChar(50)) + ', Source: ' + IsNull(ERROR_PROCEDURE(),'')
		Return ERROR_NUMBER()
	End Catch
End



---------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[sp_GetPUVoucherSearch]    Script Date: 02/09/2014 13:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetPUVoucherSearch]
	@FromDate varchar(25),@DisplayFromDate varchar(25),@ToDate varchar(25),@DisplayToDate varchar(25)
	,@PU_All int ,@SearchParam varchar(50),@ItemCode varchar(50),@LocationId int,
	@ItemId int,@OutParam	Varchar(500) Output
As
Begin
	set nocount on
	Declare 
		 @iHnd					Int
		,@BucketId_HandOn		Int
	
		,@PUNo					Varchar(50)

	Set @iHnd = 0
	--Exec sp_xml_preparedocument @iHnd Output, @InputParam

	--Read Search values from XML
	/*Select 
		 @FromDate				=	FromDate,
		 @ToDate				=	ToDate,
		 @PU_All				=	PU_All,
		 @SearchParam			=   SearchParam,
		 @ItemCode				=	ItemCode,
		 @LocationId			=	LocationId,
		 @ItemId				=	ItemId,
		 @PUNo					=   PUNo
	From
		OPENXML(@iHnd, 'PUCommon', 2)
	With
		(
			 FromDate			Varchar(25)
			,ToDate			    Varchar(25)
			,PU_All			    int	
			,SearchParam		Varchar(50)
			,ItemCode		    Varchar(50)
			,LocationId			Int
			,ItemId				int
			,PUNo				Varchar(50)
		)
		*/


	--Check search parameters values
	/*
		Print	@FromDate				
		Print	@ToDate					
		Print	@PU_All
		Print	@SearchParam			
	*/			

	--- Begin Execution ---------------------------------------------------------------------------


		IF(@SearchParam='PU')
			BEGIN
				----Header--
					SELECT PUH.PUNo,PUH.CompositeItemId,PUH.Quantity,Convert(Varchar(20), PUH.PUDate, 105) as PUDate,
							ITM.ItemName,PUH.BucketId,PUH.Remarks,PUH.ModifiedDate,
							PUH.LocationId,PUH.PU_Flag,ITM.ItemCode,IBD.ManufactureBatchNo,
							IBD.MRP,IBD.MfgDate,IBD.ExpDate
					FROM PU_Header PUH
						Inner Join Item_Master ITM
							ON PUH.CompositeItemId=ITM.ItemId 
							AND ITM.IsComposite=1
						LEFT OUTER JOIN dbo.ItemBatch_Detail IBD
							ON PUH.BatchNo = IBD.BatchNo
					Where (@FromDate=''  OR Convert(Varchar(10),PUH.PUDate,101)>=Convert(Varchar(10),cast(@FromDate as datetime),101))
						  AND(@ToDate='' OR  Convert(Varchar(10),PUH.PUDate ,101)<=Convert(Varchar(10),cast(@ToDate as datetime),101))
						  AND PU_Flag= CASE @PU_All 
											WHEN -1 THEN PU_Flag
											WHEN 0 THEN 'TRUE'
											WHEN 1 THEN 'FALSE'
										End 
						  AND  PUH.Status <> 2 
						  And ((@LocationId = 1) OR (LocationId=@LocationId ))
						  And
							   ITM.ItemCode=CASE @ItemCode 
												WHEN '' THEN ITM.ItemCode
												Else
												@ItemCode
											End 
							AND ((ISNULL(@PUNo,'')='') OR  PUH.PUNo = @PUNo)
					Order By PUH.PUNo Desc
				
			   ----Detail-----
					SELECT PUD.PUNo,PUD.ItemId,PUD.SeqNo,PUD.Quantity,PUD.ModifiedDate,
							ITM.ItemName,ITM.ItemCode
					FROM PU_Detail PUD
						 Inner Join PU_Header PUH
							ON PUD.PUNo=PUH.PUNo  
						 Inner Join Item_Master ITM
							ON PUD.ItemId=ITM.ItemId AND
							   ITM.IsComposite=0
						Inner Join Item_Master ITM1 
							ON PUH.CompositeItemId=ITM1.ItemId AND
							   ITM1.IsComposite=1
					Where  (@FromDate=''  OR Convert(Varchar(10),PUH.PUDate,101)>=Convert(Varchar(10),cast(@FromDate as datetime),101))
						  AND(@ToDate='' OR  Convert(Varchar(10),PUH.PUDate ,101)<=Convert(Varchar(10),cast(@ToDate as datetime),101))
						   AND PU_Flag= CASE @PU_All 
											WHEN -1 THEN PU_Flag
											WHEN 0 THEN 'TRUE'
											WHEN 1 THEN 'FALSE'
										End 
						   AND	PUH.Status <> 2 
						   And ((@LocationId = 1) OR (LocationId=@LocationId ))
						   And
								ITM1.ItemCode=CASE @ItemCode 
											WHEN '' THEN ITM1.ItemCode
											Else
											@ItemCode
										End 
							AND ((ISNULL(@PUNo,'')='') OR  PUH.PUNo = @PUNo)
					Order By PUH.PUNo,PUD.SeqNo
			END
		ELSE IF(@SearchParam='COMPOSITE')
			BEGIN
			  ----Composite Item---
					SELECT ITI.ItemId,ITI.ItemName,ITI.DistributorPrice,Shortname,ItemCode,ILL.LocationId
					FROM    Item_Master ITI
							Inner Join ItemLocation_Link ILL
									ON  ITI.ItemId=ILL.ItemId And
										ITI.IsComposite=1 And
										ITI.Status=1
					where ILL.LocationId=@LocationId And
						  ITI.ItemCode=CASE @ItemCode 
											WHEN '' THEN ITI.ItemCode
											ELSE @ItemCode
									   End

			
							  SELECT CompositeItemID,COB.ItemId,
									ITI.ItemName,ITI.DistributorPrice,ITI.ShortName,COB.Quantity,
									ITI.ItemCode,Sum(Isnull(ILB.Quantity,0)) As AvailableQty,ILL.LocationId
							FROM  Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId 
										LEFT join Inventory_LocBucketBatch ILB
											On ITI.ItemId=ILB.ItemId AND
				                               ILL.LocationId=ILB.LocationId 
				                                LEFT JOIN  ItemBatch_Detail IBD ON
				                          IBD.BatchNo=ILB.BatchNo	
				                          And IBD.ExpDate>GETDATE()				
								Where  ILL.LocationId=@LocationId  AND
									    CompositeItemID=@ItemId And (BucketId=(Select BucketId 
																    From Bucket 
																	Where Status = 1 AND 
																		  ParentId Is Not Null AND Sellable=1) 
																	or BucketId is null)

										AND (SELECT COUNT(*) FROM CompositeBOM WHERE CompositeItemID = @ItemId) = (SELECT COUNT(*) FROM Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId  WHERE CompositeItemID = @ItemId AND ILL.LocationId=@LocationId)

								Group BY 
										CompositeItemID,COB.ItemId,ITI.ItemName,ITI.ShortName,
										COB.Quantity,ITI.ItemCode,ILL.LocationId,ITI.DistributorPrice
								Order By CompositeItemID,COB.ItemId

			
			
							SELECT  IsNull(Min(expDate),Getdate()) expDate
							FROM  Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId
										INNER JOIN ItemBatch_Detail ITB
				                              ON  ITI.ItemId=ITB.ItemId 
						Where  ILL.LocationId=@LocationId And CompositeItemID=@ItemId AND ITB.ExpDate>GETDATE()
								
								

				
		   End
			----Constituent-----
		ELSE IF(@SearchParam='CONSTITUENT')
			
			Begin  

						---ConstituentAvailable---	

							  SELECT CompositeItemID,COB.ItemId,
									ITI.ItemName,ITI.DistributorPrice,ITI.ShortName,COB.Quantity,
									ITI.ItemCode,Sum(Isnull(ILB.Quantity,0)) As AvailableQty,ILL.LocationId
							FROM  Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId 
										LEFT join Inventory_LocBucketBatch ILB
											On ITI.ItemId=ILB.ItemId AND
				                               ILL.LocationId=ILB.LocationId	
				                         LEFT JOIN ItemBatch_Detail IBD ON IBD.BatchNo=ILB.BatchNo					
								Where  ILL.LocationId=@LocationId And IBD.ExpDate>GETDATE() AND
									    CompositeItemID=@ItemId And (BucketId=(Select BucketId 
																    From Bucket 
																	Where Status = 1 AND 
																		  ParentId Is Not Null AND Sellable=1) 
																	or BucketId is null)

										AND (SELECT COUNT(*) FROM CompositeBOM WHERE CompositeItemID = @ItemId) = (SELECT COUNT(*) FROM Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId  WHERE CompositeItemID = @ItemId AND ILL.LocationId=@LocationId)

								Group BY 
										CompositeItemID,COB.ItemId,ITI.ItemName,ITI.ShortName,
										COB.Quantity,ITI.ItemCode,ILL.LocationId,ITI.DistributorPrice
								Order By CompositeItemID,COB.ItemId

							Select IBD.BatchNo,ManufactureBatchNo,CompositeItemID,COB.ItemId,
									ITI.ItemName,ITI.ShortName,COB.Quantity,
									ITI.ItemCode,Isnull(ILB.Quantity,0) As AvailableQty,ILB.LocationId,
									MfgDate,ExpDate,cast(round(MRP,2)As numeric(12,2)) As MRP

							FROM  CompositeBOM COB
									Inner Join ItemLocation_Link ILL
										ON  COB.ItemId=ILL.ItemId And
											ILL.LocationId=@LocationId
									Inner Join Item_Master ITI
										ON  ITI.ItemId=ILL.ItemId And
											ITI.IsComposite=0
									Inner Join  ItemBatch_Detail IBD
										ON COB.ItemId=IBD.ItemId
									LEFT join Inventory_LocBucketBatch ILB
										On COB.ItemId=ILB.ItemId AND
										   IBD.BatchNo=ILB.BatchNo And
										   ILB.LocationId=ILL.LocationId
							where CompositeItemID=@ItemId  And (BucketId=(Select BucketId 
																    From Bucket 
																	Where Status = 1 AND 
																		  ParentId Is Not Null AND Sellable=1) 
																	or BucketId is null)
							Order By CompositeItemID,COB.ItemId


							SELECT  IsNull(Min(expDate),Getdate())
							FROM  Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId
										INNER JOIN ItemBatch_Detail ITB
				                              ON  ITI.ItemId=ITB.ItemId 
						Where  ILL.LocationId=@LocationId And CompositeItemID=@ItemId AND ITB.ExpDate>GETDATE()
								
								


			END
		ELSE
		BEGIN
			SELECT 1
		END


	
	
	--- End Execution -----------------------------------------------------------------------------	
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetDistributorAddress]    Script Date: 02/09/2014 13:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sp_GetDistributorAddress]
(
@distributorid varchar(8)
)
as
begin

select dm.distributorfirstname + ' '+ dm.distributorlastname as DistributorName ,' '+dm.DistributorAddress1+ ' '+dm.DistributorAddress2+' '+dm.DistributorAddress3 as Address  
,dm.distributormobnumber as DistributorContact,cm.cityname as City,sm.statename as State
from distributormaster dm join  
state_master sm
on dm.distributorstatecode=sm.stateid join
city_master cm on dm.distributorcitycode =cm.cityid  where dm.distributorid=@distributorid
end
GO
/****** Object:  StoredProcedure [dbo].[sp_GetBonus]    Script Date: 02/09/2014 13:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
procedure [dbo].[sp_GetBonus]   
(  
@DistributorId   
varchar(15),   
@distributormobnumber   
varchar(15)   
)  
AS  
begin  
create  
table #IncomingSMSForBonus(distributorid varchar(15),distributormobnumber varchar(15),Bonus numeric(18,4))   
insert  
into #IncomingSMSForBonus(distributorid,distributormobnumber,Bonus)   
select  
distinct d.DistributorId,d.DistributorMobNumber,sum(dc.EARNEDAMOUNT) as Bonus   
from  
distributormaster d   
inner  
join   
DISTRIBUTORBUSINESS_MONTHLYDETAIL dc   
on  
d.DistributorId=dc.DistributorId   
where  
((  
d.distributormobnumber like '[9,7,8,5][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')   
or  
(d.distributormobnumber like '0[9,7,8,5][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))   
group  
by d.DistributorId,d.DistributorMobNumber   
select  
*from #IncomingSMSForBonus where DistributorId=@DistributorId and distributormobnumber=@distributormobnumber   
end  
--exec sp_GetBonus '11000010','9319836783'
GO
/****** Object:  StoredProcedure [dbo].[Sp_Get_Template]    Script Date: 02/09/2014 13:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Author,Manikant Thakur>          
-- Create date: <Create Date,20-November-2011>          
-- Description: <Description,This is to select the branch detail,>          
-- =============================================          
CREATE PROCEDURE [dbo].[Sp_Get_Template]          
 @distributorid varchar(8)           
AS        
BEGIN          

create
table #MainTemplate(SelfPV numeric(18,4),TotalPV numeric(18,4),Qualified bit,BusinessMonth datetime,TDSAmount numeric(18,4))

--Two month BackData
insert into #MainTemplate
select (selfpv) as 'SelfPV',(totalpv) as 'TotalPV',IsQualifiedForPayout as 'Qualified',(businessmonth) as 'Month',(tdsamount) 'TDSAmount' from distributorbusiness_summary a
inner
join DistributorMaster b
on a.distributorid=b.distributorid
where b.UplineDistributorId=@distributorid and b.DistributorStatus=2 and a.IsQualifiedForPayout='1' and businessmonth between DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-2,0)) and getdate()

--Current Month Data
insert into #MainTemplate
select (a.selfpv) as 'SelfPV',(a.totalpv) as 'TotalPV',a.IsQualifiedForPayout as 'Qualified',(a.documentdate) as 'Month',(a.tdsamount) 'TDSAmount' from distributorbusiness_current a
inner join DistributorMaster b
on a.distributorid=b.distributorid
where b.UplineDistributorId=@distributorid and b.DistributorStatus=2 and  documentdate between   DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-2,0)) and getdate()

select
'Period'=
case DATEPART(m, DATEADD(m, 0, BusinessMonth))
WHEN '1' THEN 'January' +' '+cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '2' THEN 'February'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '3' THEN 'March'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '4' THEN 'April'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '5' THEN 'May'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '6' THEN 'June'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '7' THEN 'July'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '8' THEN 'August'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '9' THEN 'September'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '10' THEN 'October'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '11' THEN 'November'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))

WHEN '12' THEN 'December'+' ' +cast(DATEPART(yyyy, DATEADD(m, -1, BusinessMonth)) as varchar(10))
end
,sum(selfpv) as 'SelfPV',sum(totalpv) as 'TotalPV',sum(tdsamount) as 'TDSAmount',sum(cast(qualified as int)) as 'TotalQualified' from #MainTemplate group by BusinessMonth order by BusinessMonth desc
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetDstPwd]    Script Date: 02/09/2014 13:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sp_GetDstPwd]     
--Umesh
(      

  @DstID varchar(8),  
  @DstMob_no varchar(10)         
)      

as      

begin 
select DistributorMobNumber as mob_no, DistributorId as login_id, Password from DistributorMaster where DistributorId=@DstID and DistributorMobNumber=@DstMob_no
end
GO
/****** Object:  StoredProcedure [db_datareader].[Sp_GetDstPwd]    Script Date: 02/09/2014 13:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [db_datareader].[Sp_GetDstPwd]

(

@DistributorId varchar(10)   

)

as

begin

select distributorid,password from distributormaster where distributorid=@DistributorId

end
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateOrderLog]    Script Date: 02/09/2014 13:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_UpdateOrderLog]
	@jsonForOrder	Nvarchar(max),
	@LogNo     varchar(50),
	@modifiedBy int ,
	@outParam	VARCHAR(500) OUTPUT	
AS
Begin
	set nocount on ;
	

           DECLARE @MyHierarchy JSONHierarchy ,@XMLItems xml
           INSERT INTO @myHierarchy SELECT * FROM parseJSON(@jsonForOrder)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)


                            select  b.value('@OrderNo', 'varchar(50)') as OrderNo  
							
								into #Orders
							  FROM @XMLItems.nodes('/root/item') a(b)
					BEGIN TRANSACTION 
		BEGIN TRY
			-- NEW AND CLOSE STATE
   
		--update OrderLog set Status=2 where LogNo in  (	select LogNo from COHeader C LEFT  JOIN 
		--	#temp t ON t.OrderNo=c.CustomerOrderNo where C.Status!=4 )
			
		--	UPDATE COHeader SET LogNo=@LogNo,ModifiedDate=GETDATE()
		--	FROM COHeader C,#temp T 
		--	WHERE C.CustomerOrderNo=T.OrderNo
		-- Interface Audit Entry  
			DECLARE @TempTable Table (
		    logNo Varchar(20),
		    allcatedLogNo varchar(20)
		) 
	DECLARE @temLogNo varchar(30)
	DECLARE @prevtemLogNo varchar(30)

	DECLARE @lastChar  vARCHAR(30),
		 @OrderNo varchar(30),
	 @lastNumber int ,@isNumber int ,
	 @allocatedLogNo varchar(30)
	 ,@countForSameLogNo int,@parentLog varchar(30),@count int
	 SET @countForSameLogNo=0 ;
	 

    BEGIN
					DECLARE OrderCur CURSOR FOR SELECT  ISNULL(COH.LogNo,''),O.OrderNo from COHeader COH INNER JOIN #Orders O ON
					  O.OrderNo=COH.CustomerOrderNo Order By O.OrderNo
							
														OPEN OrderCur
														FETCH NEXT FROM OrderCur INTO @temLogNo,@OrderNo
										
														WHILE @@FETCH_STATUS = 0
															
														  BEGIN
														     IF(@temLogNo='')
														            BEGIN
														              UPDATE COHeader set LogNo=@LogNo where CustomerOrderNo=@OrderNo 
														                 Goto Cont
														            END
															    select @lastChar=RIGHT(@temLogNo,1)
                                                                 select @isNumber=case when @lastChar not like '%[^0-9]%' then 1 else 0 end
                                                                 
                                                           Select @count=Count(*) from @TempTable where logNo=@temLogNo
                                                           IF(@count>0)
                                                             BEGIN
                                                             Select @allocatedLogNo=allcatedLogNo from @TempTable where logNo=@temLogNo
                                                             UPDATE COHeader set LogNo=@allocatedLogNo where CustomerOrderNo=@OrderNo  
															 Goto Cont ;
                                                             END
                                                               IF(@isNumber=1)  --if last is number only a is added.
                                                                           BEGIN
                                                                             SET  @allocatedLogNo=@temLogNo+'A' ;
                                                                              Select @count=COUNT(*) from OrderLog where LogNo=@allocatedLogNo
																     IF(@count=0)
                                                                                BEGIN
																						 INSERT INTO OrderLog ([LogNo] ,[LogType],[DistributorId],[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							 ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,[Status] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate] ,[isZeroLog] ,[isChangeAddress])
																						  SELECT  @allocatedLogNo ,[LogType],[DistributorId] ,[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							  ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,1 ,@modifiedBy ,GETDATE() ,@modifiedBy ,GETDATE() ,[isZeroLog] ,[isChangeAddress]
																							  FROM [OrderLog] where LogNo=@temLogNo
																							  insert into @TempTable SELECT @temLogNo,@allocatedLogNo
																						UPDATE COHeader set LogNo=@allocatedLogNo where CustomerOrderNo=@OrderNo  
																						       Goto Cont ;
                                                                                END
                                                                                
                                                                             
                                                                               BEGIN
                                                                                SET @parentLog=@temLogNo
																					 SELECT @countForSameLogNo =COUNT(*) from OrderLog where LogNo Like @parentLog+'%' AND LogType=1
																					  SET @countForSameLogNo=@countForSameLogNo+63
																					   select  @lastChar=CHAR(@countForSameLogNo)
																					   SET @countForSameLogNo=0 ;
																				    	 SET  @allocatedLogNo=@parentLog+@lastChar ;
                                                                               IF  (@allocatedLogNo=@temLogNo )  -- all LOGNO  of this Category is close 
																		              BEGIN
																			           SET @countForSameLogNo=@countForSameLogNo+64
																					   select  @lastChar=CHAR(@countForSameLogNo)
																				    	 SET  @allocatedLogNo=@parentLog+@lastChar ;
                                
																						INSERT INTO OrderLog ([LogNo] ,[LogType],[DistributorId],[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							 ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,[Status] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate] ,[isZeroLog] ,[isChangeAddress])
																						  SELECT  @allocatedLogNo ,[LogType],[DistributorId] ,[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							  ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,1 ,@modifiedBy ,GETDATE() ,@modifiedBy ,GETDATE() ,[isZeroLog] ,[isChangeAddress]
																							  FROM [OrderLog] where LogNo=@temLogNo
																							   insert into @TempTable SELECT @temLogNo,@allocatedLogNo
																						UPDATE COHeader set LogNo=@allocatedLogNo where CustomerOrderNo=@OrderNo  
																				     	 SET  @countForSameLogNo=0 ;
																						   Goto Cont 
																																	
																						END
																					  ELSE      -- GET MINIMUM CREATED DATE LOg NO which is not closed
																						    BEGIN  
																							  SELECT  @allocatedLogNo= LogNo from OrderLog where LogNo Like @parentLog+'%' AND LogType=1 And CreatedDate=
																							  (
																							 SELECT   MAX(CreatedDate) from OrderLog where LogNo Like @parentLog+'%' AND LogType=1
																						     )
																							UPDATE COHeader set LogNo=@allocatedLogNo where CustomerOrderNo=@OrderNo  
																						
																							   Goto Cont  
																									      END
                                                                                                END
															    
                                                                             
                                                                            END       
                                                                 
                                                              ELSE 
                                                                  
                                                                               BEGIN
                                                                                select  @parentLog= substring(@temLogNo, 1, (len(@temLogNo) - 1))
																					 SELECT @countForSameLogNo =COUNT(*) from OrderLog where LogNo Like @parentLog+'%' AND LogType=1
																					  SET @countForSameLogNo=@countForSameLogNo+63
																					   select  @lastChar=CHAR(@countForSameLogNo)
																					   Select @countForSameLogNo=COUNT(*) from OrderLog where LogNo Like @parentLog+'%' AND LogType=1 ;
																				    	 SET  @allocatedLogNo=@parentLog+@lastChar ;
                                                                               IF  (@allocatedLogNo=@temLogNo )  -- all LOGNO  of this Category is close 
																		              BEGIN
																			           SET @countForSameLogNo=@countForSameLogNo+64
																					   select  @lastChar=CHAR(@countForSameLogNo)
																				    	 SET  @allocatedLogNo=@parentLog+@lastChar ;
                                
																						INSERT INTO OrderLog ([LogNo] ,[LogType],[DistributorId],[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							 ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,[Status] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate] ,[isZeroLog] ,[isChangeAddress])
																						  SELECT  @allocatedLogNo ,[LogType],[DistributorId] ,[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							  ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,1 ,@modifiedBy ,GETDATE() ,@modifiedBy ,GETDATE() ,[isZeroLog] ,[isChangeAddress]
																							  FROM [OrderLog] where LogNo=@temLogNo
																							 insert into @TempTable SELECT @temLogNo,@allocatedLogNo
																						UPDATE COHeader set LogNo=@allocatedLogNo where CustomerOrderNo=@OrderNo  
																				     	 SET  @countForSameLogNo=0 ;
																						   Goto Cont 
																																	
																						END
																					  ELSE      -- GET MINIMUM CREATED DATE LOg NO which is not closed
																						    BEGIN  
																							  SELECT  @allocatedLogNo LogNo from OrderLog where LogNo Like @parentLog+'%' AND LogType=1 And CreatedDate=
																							  (
																							 SELECT   MAX(CreatedDate) from OrderLog where LogNo Like @parentLog+'%' AND LogType=1
																						     )
																							UPDATE COHeader set LogNo=@allocatedLogNo where CustomerOrderNo=@OrderNo  
																						
																							   Goto Cont  
																									      END
                                                                                                END
                                                                                                
                                                             --  FOR CLOSE LOG
							                          
							                             
															Cont:
															 Select @count= COUNT(*) from COHeader where LogNo=@temLogNo AND Status=3
							                            IF @count=0
							                                 UPDATE OrderLog set Status=2 where LogNo=@temLogNo 
							                               
																FETCH NEXT FROM OrderCur INTO @temLogNo,@OrderNo
															END
												Drop Table #Orders
														CLOSE OrderCur 
														DEALLOCATE OrderCur 
                            
	END	   
       
       
         Select 'Success' OutParam 
		Commit TRANSACTION 	
		END TRY
		BEGIN CATCH
			Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
				Rollback 
			Return ERROR_NUMBER()
		
		END CATCH	  
					
							
END
GO
/****** Object:  StoredProcedure [dbo].[spGetDistributorPwd]    Script Date: 02/09/2014 13:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[spGetDistributorPwd]
(
@DistributorId varchar(10)
)
as
begin
if exists(select Password from DistributorMaster where
DistributorId=@DistributorId)
	begin
	SET NOCOUNT ON
	select Password,'Valid' as Result from DistributorMaster where DistributorId=@DistributorId
	SET NOCOUNT OFF
	end
else
begin
select '' as Password,'NoExists' as Result 
end 
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DistributorAccountHistory]    Script Date: 02/09/2014 13:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_DistributorAccountHistory] 
 @distributorId  varchar(20),    
 @outParam   Varchar(500) OutPut     
AS    
BEGIN    

 BEGIN TRY  
 
 set nocount on  
	  
SELECT DISTINCT DADH.DistributorBankName BankName,DADH.DistributorBankBranch,DADH.ModifiedDate,
DADH.DistributorBankAccNumber ,DM.DistributorId distributorid,DM.DistributorFirstName,DM.DistributorLastName,UM.UserName
FROM 
DistributorAccountDetails_History DADH 
LEFT JOIN DistributorMaster DM ON
DADH.DistributorId = DM.DistributorId 
LEFT JOIN User_Master UM ON 
DADH.ModifiedBy = UM.UserId
WHERE 
DADH.distributorid = @distributorId 
GROUP BY 
DADH.ModifiedDate,DADH.DistributorBankName,DADH.distributorbankbranch,DADH.DistributorBankAccNumber,
DM.DistributorId,DM.DistributorFirstName,DM.DistributorLastName,UM.UserName




 End Try    
 Begin Catch    
  Set @outParam= CAST(ISNULL(ERROR_NUMBER(),'') As Varchar(50)) + ',' + ISNULL(ERROR_MESSAGE(),'') + 'Error Line: ' + CAST(ISNULL(ERROR_LINE(),'') As Varchar(50)) + 'Source: ' + ISNULL(ERROR_PROCEDURE(),'')    
  Return ERROR_NUMBER()    
 End Catch    
End
GO
/****** Object:  StoredProcedure [dbo].[sp_TOAdjustItemSearch]    Script Date: 02/09/2014 13:05:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_TOAdjustItemSearch]
	@TNumber	Varchar(20), 
	@SourceAddressId Int, 
	@outParam		Varchar(500) OUTPUT
AS
Begin
	Begin Try
		set nocount on;
	DECLARE @TotalQuantity As Numeric(12,4)
	DECLARE @BucketId				Int,	@BucketName	Varchar(50),		@UOMId	Int,				@SourceLocationId		Int,	
			@ItemDescription Varchar(100),	@ManufactureBatchNo Varchar(20),@TOINumber Varchar(20),		@RowNo INT,						
			@ItemId INT,					@ItemCode Varchar(20),			@ItemQuantity Numeric(12,4),@BatchNo Varchar(20),			
			@BuketQuantity Numeric(12,4),	@Weight	Numeric(12,4)
	DECLARE @ItemBatchId VARCHAR(20)
	SET @TotalQuantity = 0 

	Declare @TO_Detail Table 
	(
		ItemBatchId Varchar(20), 
		RowNo Int,
		ItemId Int,
		ItemCode Varchar(20),
		AvailableQty Numeric(12,4),
		RequestQty	Numeric(12,4),
		TransferPrice Numeric(12,4),
		TotalAmount Money,
		BucketId Int,
		BatchNo Varchar(20),
		AfterAdjustQty Numeric(12,4),
		BucketName Varchar(50),
		ItemDescription Varchar(100),
		ManufactureBatchNo Varchar(20),
		UOMId Int,
		Weight	Numeric(12,4)
	)
	
	
	
	
	IF EXISTS (SELECT 1 FROM TOI_Detail td WHERE ItemId NOT IN (SELECT ItemId FROM Item_Master im WHERE im.Status=1) AND td.TOINumber = @TNumber)
	BEGIN 
		PRINT 	'Item does not exists at current location'			
		SET @outParam = 'INF0041'
		SELECT  @outParam OutParam
		Return
	END

	IF EXISTS (SELECT 1 FROM TOI_Detail td INNER JOIN TOI_Header th ON th.TOINumber = td.TOINumber AND td.TOINumber = @TNumber
	           WHERE NOT EXISTS (SELECT 1 FROM ItemLocation_Link ill WHERE ill.LocationId = th.SourceLocationID AND ill.ItemId = td.ItemId AND Status =1)
	           )
	BEGIN 
		PRINT 	'Item is not mapped to current location'			
		SET @outParam = 'INF0042 '
		SELECT @outParam AS OutParam
		Return
	END
	

--Select * From parameter_Master 

	DECLARE TOICur CURSOR FOR 
		Select	td.TOINumber, RowNo, td.UOMId, td.ItemId, td.ItemCode, td.Quantity As ItemQuantity, td.BucketId, buk.BucketName, ItemDescription, im.Weight
		From TOI_Detail td
		Inner Join Bucket buk 
		On buk.BucketId = td.BucketId And buk.Status = 1
		Inner Join Item_Master im 
		On im.ItemId = td.ItemId
		Inner Join TOI_Header th 
		On th.TOINumber = td.TOINumber
		And th.Status = 2 -- 2 Confirmed
		And td.TOINumber NOT IN (Select TOINumber From TO_Header Where Status = 2)
		--And td.ItemId = 5
		Where td.TOINumber = @TNumber 


		
		--And td.ItemId = 3

		Order By TOINumber, td.ROwNo
		
		OPEN TOICur
		FETCH NEXT FROM TOICur
			INTO @TOINumber, @RowNo,  @UOMId, @ItemId, @ItemCode, @ItemQuantity, @BucketId, @BucketName, @ItemDescription, @Weight
		
		WHILE @@FETCH_STATUS = 0
		BEGIN 
			
			--SELECT @TOINumber, @RowNo, @ItemId, @ItemCode, @ItemQuantity, @BucketId

			Select @TotalQuantity = SUM(Quantity) From ItemBatch_Detail ib Inner Join Inventory_LocBucketBatch bwi
			On bwi.ItemId = ib.ItemId And  bwi.BatchNo = ib.BatchNo Where bwi.LocationId = @SourceAddressId
			And bwi.BucketId = @BucketId And bwi.ItemId = @ItemId And bwi.Status = 1 And ib.Status = 1
			
			--SELECT @TotalQuantity TOTAL, @ItemQuantity ITEM 
			IF ISNULL(@TotalQuantity,0) < @ItemQuantity
			BEGIN
				--Print  CAST(@TotalQuantity As Varchar)+ ', ' +  CAST(@ItemQuantity AS Varchar)
				Print 'Total quantity in TOI item(s) is greater than item(s) quantity at current location.'
				SET @outParam = 'VAL0027'
				SELECT  @outParam as OutParam
				CLOSE TOICur
				DEALLOCATE TOICur
				Return
			END

			DECLARE @ConsumedQuantity AS Numeric(12,4)
			SET @ConsumedQuantity = 0 
			DECLARE @Inserted BIT
			DECLARE @First BIT
			SET @First = 0 
			SET @Inserted = 0

			DECLARE TOIItem CURSOR FOR 
				SELECT	ItemBatchId, ib.BatchNo, Quantity, ManufactureBatchNo
				FROM ItemBatch_Detail ib Inner Join Inventory_LocBucketBatch bwi
				ON   bwi.BatchNo = ib.BatchNo WHERE bwi.LocationId = @SourceAddressId
				AND bwi.ItemId = @ItemId And bwi.Status = 1 And ib.Status = 1 And bwi.BucketId = @BucketId
				AND Quantity > 0 
				ORDER BY  MfgDate--, ib.ItemId,ib.BatchNo
				
				OPEN TOIItem
				FETCH NEXT FROM TOIItem
					INTO @ItemBatchId, @BatchNo, @BuketQuantity, @ManufactureBatchNo
				
				WHILE @@FETCH_STATUS = 0
				BEGIN 	
					--SELECT 'Test', @ItemBatchId, @RowNo, @UOMId, @ItemId, @ItemCode, @BuketQuantity bucketQty, @BucketId, @BatchNo, @ItemQuantity, @BuketQuantity, @BucketName, @ItemDescription, @ManufactureBatchNo, @Weight					

					SET @ConsumedQuantity = @ConsumedQuantity + @BuketQuantity
					If (@ConsumedQuantity>@ItemQuantity AND @Inserted = 0)
					BEGIN

						SET @Inserted = 1 

						Declare @AdjustQty As Numeric (12,4) 
						SET @AdjustQty = 0 
						
						--SELECT 'Test',@BuketQuantity, @ItemQuantity,  * FROM @TO_Detail

						SELECT @AdjustQty = SUM(AvailableQty) FROM @TO_Detail WHERE ItemBatchId <> @ItemBatchId And ItemId = @ItemId AND ItemBatchID In (Select ItemBatchId From @TO_Detail)
						
						IF @AdjustQty <0 
							SET @AdjustQty = 0 
	
						DECLARE @Qty  AS NUMERIC(12,4)
						SET @Qty = 0 
						IF @First = 0 
							SET @Qty = 	@ItemQuantity 
						ELSE IF @First = 1 --AND @BuketQuantity > @ItemQuantity 
							SET @Qty = 	@ItemQuantity - @AdjustQty 
						--ELSE IF @First = 1 AND @BuketQuantity < @ItemQuantity 
						--	SET @Qty = 	@ItemQuantity - @AdjustQty 

						--Select 'T1' , @BuketQuantity, @ConsumedQuantity, @ItemQuantity, @Qty
						-- CASE WHEN @BuketQuantity > @ItemQuantity THEN @ItemQuantity ELSE @ItemQuantity - @AdjustQty END
						INSERT INTO @TO_Detail (ItemBatchID, RowNo,		UOMId, ItemId, ItemCode,	BucketId,	BatchNo, RequestQty,	AfterAdjustQty, BucketName, ItemDescription, ManufactureBatchNo, Weight) 
						SELECT					ItemBatchID, @RowNo,	@UOMId, @ItemId, @ItemCode, @BucketId,	BatchNo, @ItemQuantity, @Qty,			@BucketName, @ItemDescription, @ManufactureBatchNo, @Weight  FROM ItemBatch_Detail WHERE ItemBatchId = @ItemBatchId And ItemId = @ItemId
						--AND ItemBatchID Not In (Select ItemBatchId From @TO_Detail)
					END
					ELSE If (@ConsumedQuantity<=@ItemQuantity)
					BEGIN
						--Select 'T' , @ConsumedQuantity, @ItemQuantity
						SET @First = 1 
						INSERT INTO @TO_Detail (ItemBatchId, RowNo, UOMId, ItemId, ItemCode, AvailableQty, BucketId, BatchNo, RequestQty, AfterAdjustQty, BucketName, ItemDescription, ManufactureBatchNo, Weight) 
						VALUES (@ItemBatchId, @RowNo, @UOMId, @ItemId, @ItemCode, @BuketQuantity, @BucketId, @BatchNo, @ItemQuantity, @BuketQuantity, @BucketName, @ItemDescription, @ManufactureBatchNo, @Weight)
					END

				FETCH NEXT FROM TOIItem
				INTO @ItemBatchId, @BatchNo, @BuketQuantity, @ManufactureBatchNo
				END
				CLOSE TOIItem
				DEALLOCATE TOIItem

			FETCH NEXT FROM TOICur
			INTO @TOINumber, @RowNo, @UOMId, @ItemId, @ItemCode, @ItemQuantity, @BucketId, @BucketName, @ItemDescription, @Weight
		END -- While End
		CLOSE TOICur
		DEALLOCATE TOICur

--		Select 'TT', * From @TO_Detail
--
-- Select * From @TO_Detail a Inner Join Inventory_LocBucketBatch b
--		ON a.ItemId = b.ItemId and a.BatchNo = b.BatchNo And a.BucketId = b.BucketId--and b.TOINumber = @TNumber
--		WHERE a.AvailableQty IS NULL



		UPDATE @TO_Detail SET AvailableQty = b.Quantity
		FROM @TO_Detail a
		INNER JOIN Inventory_LocBucketBatch b
		ON a.ItemId = b.ItemId and a.BatchNo = b.BatchNo And a.BucketId = b.BucketId And LocationId = @SourceAddressId--and b.TOINumber = @TNumber
		WHERE a.AvailableQty IS NULL

		UPDATE @TO_Detail SET TransferPrice = b.TransferPrice, TotalAmount = AfterAdjustQty * b.TransferPrice
		FROM @TO_Detail a
		INNER JOIN TOI_Detail b
		ON a.ItemId = b.ItemId and a.RowNo = b.RowNo and b.TOINumber = @TNumber


		SELECT tod.*, MfgDate, ExpDate, MRP, UOMName FROM @TO_Detail tod
		Inner Join ItemBatch_Detail ibd
		ON tod.ItemId = ibd.ItemId
		And tod.BatchNo = ibd.BatchNo
		Inner Join UOM_Master um 
		On um.UOMId = tod.UOMId
		WHERE AfterAdjustQty >0
		ORDER BY RowNo, MfgDate

	End Try
	Begin Catch
		Set @outParam = '30001:' + CAST(Error_Number() AS Varchar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As Varchar(50)) + ', Source: ' + ERROR_PROCEDURE()
		SELECT  @outParam OutParam
		Return ERROR_NUMBER()
	End Catch
End


-------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[sp_TIItemSearch]    Script Date: 02/09/2014 13:05:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_TIItemSearch]
	@TONumber Varchar(20),
	@TNumber	Varchar(20), 
	@SourceAddressId Int, 
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	set nocount on;
		IF IsNull(@TONumber,'')= ''
		BEGIN

					SELECT DISTINCT
							 tbd.RowNo, td.ItemId, td.ItemCode, td.ItemDescription, td.Quantity As RequestQty,
							  IsNull(td.UOMId,'') As UOMId, td.TransferPrice, 
							 CAST((tbd.Quantity) AS NUMERIC(12,2)) * CAST(td.TransferPrice AS NUMERIC(12,2)) As TotalAmount	
							, tbd.RcvSubBucketId As BucketId,  th.TOINumber, th.TONumber, th.TINumber,	UOMName,	buk.BucketName
							,tbd.Quantity AfterAdjustQty,ISNULL(CBD.Quantity,0) ClaimQty, tbd.BatchNo, Convert(Varchar(20),tbd.MfgDate,120)As MfgDate, 
							Convert(Varchar(20),tbd.ExpDate,120) ExpDate, tbd.ManufactureBatchNo, tbd.MRP, 
							tbd.Quantity AvailableQty, IsNull(im.Weight,0) Weight
					FROM  TIHeader TIH
					LEFT JOIN TIDetail td ON 
					td.TINumber=TIH.TINumber
					INNER JOIN TIBatchDetail tbd
					ON tbd.ItemId = td.ItemId
					AND td.TINumber = tbd.TINumber
					AND td.BucketId = tbd.RcvSubBucketId
					LEFT JOIN CSHeader CSH ON 
					CSH.TONumber=TIH.TONumber 
					LEFT JOIN CSBatchDetail CBD ON
					CBD.CSNumber=CSH.CSNumber
					AND CBD.ItemId=tbd.ItemId
					AND CBD.BatchNo=tbd.BatchNo
					Inner Join TIHeader th 
					ON th.TINumber = td.TINumber 
					INNER JOIN Item_Master im
					ON im.ItemId = td.ItemId
					Inner Join ItemUOM_Link  iul
					ON td.ItemId = iul.ItemId
					And iul.TypeOfMeasure = 1
					Inner Join UOM_Master um
					On um.UOMId = iul.UOMId
					Inner Join Bucket buk
					ON buk.BucketId = tbd.RcvSubBucketId
					And buk.Status = 1
					Where th.TINumber = @TNumber
					AND th.SourceLocationId  =@SourceAddressId
					Order By TOINumber, ROwNo
				END
		ELSE 
		BEGIN
				SELECT DISTINCT
							 tbd.RowNo, td.ItemId, td.ItemCode, td.ItemDescription, td.Quantity As RequestQty, td.UOM UOMId, td.TransferPrice, 
							(tbd.Quantity-ISNULL(CSBD.Quantity,0)) * td.TransferPrice As TotalAmount, td.BucketId,  th.TOINumber,UOMName, buk.BucketName
							,tbd.Quantity-ISNULL(CSBD.Quantity,0) AfterAdjustQty,ISNULL(CSBD.Quantity,0) ClaimQty, tbd.BatchNo, Convert(Varchar(20),tbd.MfgDate,120)As MfgDate, Convert(Varchar(20),tbd.ExpDate,120) ExpDate, tbd.ManufactureBatchNo, tbd.MRP, FromSubBucketId as BucketId,
						tbd.Quantity AvailableQty, IsNull(im.Weight,0) Weight
				FROM TO_Header TOH LEFT JOIN
				 TO_Detail td  ON
				 TOH.TONumber=td.TONumber
				INNER JOIN TOBatchDetail tbd
				ON tbd.ItemId = td.ItemId
				AND td.TONumber = tbd.TONumber
				AND td.BucketId = tbd.FromSubBucketId
                LEFT JOIN CSHeader CSH ON 
                CSH.TONumber =TOH.TONumber
                LEFT JOIN  CSBatchDetail CSBD ON
                CSBD.CSNumber=CSH.CSNumber AND 
                  tbd.ItemId=CSBD.ItemId AND
                  tbd.BatchNo=CSBD.BatchNo
				Inner Join TO_Header th 
				ON th.TONumber = td.TONumber 
				INNER JOIN Item_Master im
				ON im.ItemId = td.ItemId
				Inner Join ItemUOM_Link  iul
				ON td.ItemId = iul.ItemId
				And iul.TypeOfMeasure = 1
				Inner Join UOM_Master um
				On um.UOMId = iul.UOMId

				Inner Join Bucket buk
				ON 
				buk.BucketId = td.BucketId 
				And buk.Status = 1
				Where th.TONumber = @TONumber
			
				Order By TOINumber, ROwNo
				

		END

		
	End Try
	Begin Catch
		Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
		Return ERROR_NUMBER()
	End Catch
End
GO
/****** Object:  StoredProcedure [dbo].[SP_TOISave]    Script Date: 02/09/2014 13:05:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_TOISave]
	@jsonForItems	nvarchar(max),
	@jsonRemoveItems    nvarchar(max),
	@iHnd   int, 
	@BucketId				Int,		
			@SourceLocationId		Int,
			@DestinationLocationId	Int,
			@TOIDate				Varchar(20),
			@CreationDate			Varchar(20),
			@StatusId				Int,
			@ModifiedBy				Int,					
			@ModifiedDate			Varchar(20),
			--@IndentNo				Varchar(20),
			@TNumber				Varchar(20),
			@TotalTOIQuantity		float, 
			@TotalTOIAmount			float, 
			@CreatedDate			Varchar(20) ,
			@Indentised				int,
			@Isexported				int,
			@SystemDate				DateTime,
			@LocationId				Int,
			@IndexSeqNo				Int,
			@LocationCode			VARCHAR(20),
			@PriceMode				int, 
			@Percentage				int, 
			@AppDep	                 int,
	 @outParam		VARCHAR(500) OUTPUT
			
AS
Begin 
  	 set nocount on 
	Begin Try
     BEGIN  TRANSACTION 
			Declare @TempCnt As int 
			Declare @Cnt As int		
            DECLARE @MyHierarchy JSONHierarchy,@items JSONHierarchy
			Declare @OldQuantity As Numeric(12,4)
			Declare @NewQuantity As Numeric(12,4)
			DECLARE @RecCnt AS VARCHAR(2000)
            declare @XMLItems	 xml;
            declare @XMLRemoveItems				xml;
             DECLARE @IslocationOnline int
			Set @SystemDate = getdate()
			Declare @RowNo Int, @Row Int, @ItemId Int, @ItemCode Varchar(20), @ItemName Varchar(100), @UnitQty Numeric(12,4), @UOMId Int, @ItemUnitPrice Money, @ItemTotalAmount Money
			Declare @SeqNo Varchar(200)
			Set @iHnd = 0

		
  
			INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)


	
		SELECT 
						
							  b.value('@ItemId', 'int') as ItemId,
							  b.value('@ItemCode', 'varchar(50)') as ItemCode,
							  b.value('@ItemName', 'varchar(100)') as ItemDescription,
							  b.value('@UnitQty', 'Numeric(12,4)') as Quantity,
							  b.value('@UOMId', 'int') as UOMId,
							  b.value('@UnitPrice', 'Numeric(12,4)') as TransferPrice,
							  b.value('@TotalAmount', 'Money') as TotalAmount,
							  b.value('@Bucketid', 'int') as BucketId,
							  b.value('@TOINumber','varchar(50)') as TOINumber
								into #TOI_Detail 
							  FROM @XMLItems.nodes('/root/item') a(b)
							  
							  alter table  #TOI_Detail add  RowNo Int Identity(1,1)  ;
							  
							   
     if (@TNumber='')
           BEGIN
--		Insert Into @TOI_Header (TOINumber,SourceLocationID, DestinationLocationID, TOIDate, Status, 
--								 CreationDate, TotalTOIQuantity, TotalTOIAmount, Indentised, [LocationId],[IndexSeqNo] ,CreatedBy,
--		
			Select @LocationCode = LocationCode,@IslocationOnline =IsLocationOnline From Location_Master Where LocationId = @SourceLocationId

					If (IsNull(NULLIF(@IndexSeqNo,''),-1)<0)
					Begin
		
						IF @Isexported = 0 
							Execute usp_GetSeqNo 'TOI', @LocationId, @SeqNo Out
						ELSE 
							Execute usp_GetSeqNo 'TOIEX', @LocationId, @SeqNo Out--, 1, @DestinationLocationId
							
						Set @TNumber=@SeqNo

						Insert Into TOI_Header ( TOINumber,SourceLocationID, DestinationLocationID, TOIDate, Status,
						 CreationDate, TotalTOIQuantity, TotalTOIAmount, Indentised, CreatedBy,CreatedDate, ModifiedBy, 
						 ModifiedDate,Isexported, PriceMode, Percentage, AppDep)
							Values (@SeqNo, @SourceLocationId, @DestinationLocationId, @SystemDate,
							 @StatusId,  @SystemDate , @TotalTOIQuantity, @TotalTOIAmount, @Indentised,
							  @ModifiedBy, @SystemDate, @ModifiedBy, @SystemDate,@Isexported, @PriceMode, @Percentage, @AppDep)

						Insert Into TOI_Detail (	TOINumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOMId, TransferPrice, TotalAmount, --IndentNo, 
																	BucketId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
										Select @TNumber, RowNo, ItemId, ItemCode, ItemDescription,Quantity , UOMId, TransferPrice, TotalAmount, --@IndentNo, 
																	BucketId, @ModifiedBy, @SystemDate, @ModifiedBy, @SystemDate from #TOI_Detail	
							
--						Exec [usp_Interface_Audit] '', 'TRNORDINS', @LocationCode, 'TOI_Header', @SeqNo, NULL, NULL, NULL, NULL, 'I', @ModifiedBy

						--DBCC CHECKIDENT (#TOI_Detail , reseed, 0)
					
				if(@IslocationOnline=0)
				   BEGIN
						If Exists (SELECT TValue From (Select Case When AvailableQuantity < td.Quantity Then 0 Else 1 End TValue From InventoryLocation_Master ilm
								Inner Join TOI_Detail td On td.BucketId = ilm.BucketId 
								And td.ItemId = ilm.ItemId Where ilm.LocationId = @SourceLocationId And td.TOINumber = @TNumber) result 
								Where TValue = 0)
						Begin
							Set @outParam = 'VAL0022'  --TOI Quantity is greater then location Qty
							select @outParam as OutParam
							 ROLLBACK
							Return
						End
                   END
               
                    

					End
					Else -- If TOI Is Already Exists
					Begin
						--Select 'Test2'
						-- Check Concurrency, Get DBDate
						Declare @DBDate DateTime
						Select @DBDate = (Select ModifiedDate = (Case When ModifiedBy Is Not Null Then
						 ModifiedDate Else CreatedDate End) From TOI_Header Where  TOINumber = @TNumber)
						
						If @ModifiedDate <> Convert(Varchar(20),@DBDate,120)
						Begin
							Set @outParam = 'INF0022'  --For Concurrency
							select @outParam as OutParam
							ROLLBACK
							Return
						End

						Declare @OldStatus As Int
						Select @OldStatus = Status From TOI_Header Where TOINumber = @TNumber
						
						--Select 'Test1'
						If @OldStatus<>@StatusId And  @StatusId = 2 -- In (Select KeyCode1 From Parameter_Master Where ParameterCode = 'TOIStatus' And KeyValue1='Confirmed')
						Begin
							--Check AvailableQuanity And Buket Quantity also 
							Update TOI_Header Set SourceLocationID = @SourceLocationID, TotalTOIQuantity = @TotalTOIQuantity, DestinationLocationId = @DestinationLocationId, Status = @StatusId, TotalTOIAmount= @TotalTOIAmount,  TOIDate = @SystemDate , 
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
						End
						Else If @OldStatus<>@StatusId 
							Update TOI_Header Set SourceLocationID = @SourceLocationID, TotalTOIQuantity = @TotalTOIQuantity, DestinationLocationId = @DestinationLocationId, Status = @StatusId, TotalTOIAmount= @TotalTOIAmount, 
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
						Else If @OldStatus=@StatusId 
							Update TOI_Header Set SourceLocationID = @SourceLocationID, TotalTOIQuantity = @TotalTOIQuantity, DestinationLocationId = @DestinationLocationId,  TotalTOIAmount= @TotalTOIAmount, 
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
						
						--------------------------------------------------------------------------------------------------
						-- INTERFACE
						---------------------------------------------------------------------------------------------------
				
											
						---------------------------------------------------------------------------------------------------

						-- If Status Is Created TODo: Can we add/Update Item At Confirmed Status
						If  @StatusId Not In (Select KeyCode1 From Parameter_Master Where ParameterCode = 'TOIStatus' And (KeyValue1='Created' Or KeyValue1='Confirmed' ))
						Begin
								
								Select @Cnt = COUNT(*) From TOI_Detail Where TOINumber = @TNumber
								Select @TempCnt = COUNT(*) From #TOI_Detail
								--Select @TempCnt, @Cnt
								If (@TempCnt<>@Cnt)
								Begin
									Set @outparam = 'INF0036'
									SELECT @outparam OutParam -- User Can not Add or Edit Item
							        ROLLBACK
									Return 
								End
							-- Can not edit item in Cancel, Confirm, Reject TOI Status

								Select TOINumber TNumber, Convert(Varchar(20), ModifiedDate , 120) ModifiedDate, 1 as IndexSeqNo
								From TOI_Header 
								Where TOINumber = @TNumber
							Return 
						End


						If  @StatusId = 1 Or @StatusId = 2 -- In (Select * From Parameter_Master Where ParameterCode = 'TOIStatus' And (KeyValue1='Created' Or KeyValue1='Confirmed' ))
						Begin
							
					    	Insert Into TOI_Detail (	TOINumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOMId, TransferPrice, TotalAmount, --IndentNo, 
																	BucketId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
										Select @TNumber, RowNo, ItemId, ItemCode, ItemDescription,Quantity , UOMId, TransferPrice, TotalAmount, --@IndentNo, 
																	BucketId, @ModifiedBy, @SystemDate, @ModifiedBy, @SystemDate from #TOI_Detail	
								
														End -- If @StatusId End
					End

						-- Update Quantity, Is Status IS Confirmed
						If @StatusId = 2 -- In (Select KeyCode1 From Parameter_Master Where ParameterCode = 'TOIStatus' And KeyValue1='Confirmed')
						Begin
							If Exists (SELECT TValue From (Select Case When AvailableQuantity <  td.Quantity Then 0 Else 1 End TValue From InventoryLocation_Master ilm
								Inner Join TOI_Detail td On td.BucketId = ilm.BucketId 
								And td.ItemId = ilm.ItemId Where ilm.LocationId = @SourceLocationId And td.TOINumber = @TNumber) result 
								Where TValue = 0)
							Begin
								Set @outParam = 'VAL0022'
								select @outParam as OutParam
								ROLLBACK
								Return
							End

							--Select 'Test11'
							Update InventoryLocation_Master Set AvailableQuantity = AvailableQuantity - td.Quantity
							From InventoryLocation_Master ilm
							Inner Join TOI_Detail td On td.BucketId = ilm.BucketId 
							And td.ItemId = ilm.ItemId Where ilm.LocationId = @SourceLocationId And td.TOINumber = @TNumber 
						End
         END
         
     else if @StatusId=1 AND @TNumber!=''
           BEGIN 
           delete from TOI_Detail where TOINumber=@TNumber;
          
          	Begin
						--Select 'Test2'
						-- Check Concurrency, Get DBDate
					
						Select @DBDate = (Select ModifiedDate = (Case When ModifiedBy Is Not Null Then ModifiedDate Else CreatedDate End) From TOI_Header Where  TOINumber = @TNumber)
						Select @OldStatus = Status From TOI_Header Where TOINumber = @TNumber
						Insert Into TOI_Detail (	TOINumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOMId, TransferPrice, TotalAmount, --IndentNo, 
																	BucketId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
										Select @TNumber, RowNo, ItemId, ItemCode, ItemDescription,Quantity , UOMId, TransferPrice, TotalAmount, --@IndentNo, 
																	BucketId, @ModifiedBy, @SystemDate, @ModifiedBy, @SystemDate from #TOI_Detail
							Update TOI_Header Set SourceLocationID = @SourceLocationID, TotalTOIQuantity = @TotalTOIQuantity, DestinationLocationId = @DestinationLocationId, Status = @StatusId, TotalTOIAmount= @TotalTOIAmount,  TOIDate = @SystemDate , 
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
						
	     	End
           END
     else if @StatusId = 2  --confirmed 
          BEGIN
						  Update TOI_Header Set  Status = @StatusId , IsProcessed=0,
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
							Update InventoryLocation_Master Set AvailableQuantity = AvailableQuantity -  td.Quantity
							From InventoryLocation_Master ilm
							Inner Join TOI_Detail td On td.BucketId = ilm.BucketId 
							And td.ItemId = ilm.ItemId Where ilm.LocationId = @SourceLocationId And td.TOINumber = @TNumber 
				Exec [usp_Interface_Audit] '', 'TRNORDINS', @LocationCode, 'TOI_Header', @TNumber, NULL, NULL, NULL, NULL, 'I', @ModifiedBy, @RecCnt OUTPUT
							
							
          END
      
         else if  @StatusId = 4 --  In (Select * From Parameter_Master Where ParameterCode = 'TOIStatus' And KeyValue1='Rejected')
						Begin
						
					    	Update TOI_Header Set  Status = @StatusId , IsProcessed = 0,
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
							Update InventoryLocation_Master Set AvailableQuantity = AvailableQuantity +  td.Quantity
							From InventoryLocation_Master ilm
							Inner Join TOI_Detail td On td.BucketId = ilm.BucketId 
							And td.ItemId = ilm.ItemId Where ilm.LocationId = @SourceLocationId And td.TOINumber = @TNumber 
						
							Exec [usp_Interface_Audit] '', 'TRNORDINS', @LocationCode, 'TOI_Header', @TNumber, NULL, NULL, NULL, NULL, 'U', @ModifiedBy, @RecCnt OUTPUT
							
							
						End
		 else if  @StatusId = 3 --  In (Select * From Parameter_Master Where ParameterCode = 'TOIStatus' And KeyValue1='Rejected')
						Begin
						
					    	Update TOI_Header Set  Status = @StatusId , 
							ModifiedBy= @ModifiedBy, ModifiedDate= @SystemDate
							Where TOINumber = @TNumber
						End
   
		Select TOINumber TNumber,
					Convert(Varchar(20), ModifiedDate , 120) ModifiedDate, 
					1 as IndexSeqNo,@StatusId as statusId
			From TOI_Header 
			Where TOINumber = @TNumber
		Commit
	End Try
	Begin Catch
		Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
			Select @outParam OutParam
			ROLLBACK
				Return ERROR_NUMBER()

	End Catch
End



-----------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[sp_StockCountSave]    Script Date: 02/09/2014 13:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: ~vs25D0.sql|0|0|C:\Users\Deepak\AppData\Local\Temp\~vs25D0.sql

CREATE Procedure [dbo].[sp_StockCountSave]
	@jsonForItems nvarchar(max),
	@jsonForItemBatch nvarchar(max), 
	@SeqNo nvarchar(max),
	@LocationId int,
	@InitiatedDate DateTime,
	@createdBy int,
	@StatusId int,
	@Remarks varchar(100),
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
SET NOCOUNT ON
	Begin Try
		Declare
			@new varchar(20),
			@StockCountBy		INT,					@UserId			INT,										
			@ModifiedBy			INT,					@CreatedDate		datetime,
			@ModifiedDate		datetime,			@SystemDate			DATETIME, @StockCountStatus int

			Set @SystemDate = getdate()
			Declare @RowNo INT, @Row INT, @ItemId INT, @ItemCode VARCHAR(20), @ItemName VARCHAR(100), @Quantity Numeric(12,4), @UOMId INT, 
					@BucketId Int, @BatchNo VARCHAR(20), @ManufactureBatchNo VARCHAR(20)
			Declare @RetSeqNo VARCHAR(200)
			Declare @JsonItemHeirarchy JSONHierarchy,@JsonItemBatchHeirarchy JSONHierarchy,@xml xml
		
		
			IF @jsonForItems!=''
			
			BEGIN
				INSERT INTO @JsonItemHeirarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@JsonItemHeirarchy)
		       
		    
							SELECT 
						
							  b.value('@ItemCode', 'varchar(50)') as ItemCode,
							  b.value('@ItemName', 'varchar(100)') as ItemName,
							  b.value('@ItemId', 'int') as ItemId,
							  b.value('@OnHandBucket', 'int') as OnHandQuantity,
							  b.value('@DamagedBucket', 'int') as DamagedQuantity,
							  b.value('@OnHandBucketSys', 'int') as OnHandBucketSys,
							  b.value('@DamagedBucketSys', 'int') as DamagedBucketSys,
							  b.value('@Varience', 'int') as Varience
							 
								into #ItemsInformation
							  FROM @xml.nodes('/root/item') a(b)
							  
				alter table  #ItemsInformation add RowNo Int Identity(1,1);
						--select  * from 	  #ItemsInformation ;
				Declare @ItemDetail Table 
				(
					Ident INT Identity(1,2),
					RowNo INT,
					ItemId INT,
					ItemName varchar(50),
					ItemCode VARCHAR(20),
					OnHandQuantity int,
					DamagedQuantity int,
					OnHandBucketSys int,
					DamagedBucketSys int,
					Varience int
				)
				
				Insert INTO @ItemDetail(ItemId,	ItemCode,ItemName,OnHandQuantity
					,DamagedQuantity,OnHandBucketSys,DamagedBucketSys,Varience)
				Select  ItemId,	ItemCode,ItemName,OnHandQuantity,DamagedQuantity,OnHandBucketSys,
				DamagedBucketSys,Varience From #ItemsInformation
			END
			
			IF @jsonForItemBatch !=''
			
			BEGIN
				INSERT INTO @JsonItemBatchHeirarchy
				SELECT * FROM parseJSON(@jsonForItemBatch)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@JsonItemBatchHeirarchy)
		       
		    
							SELECT 
						
							  b.value('@BatchNo', 'varchar(20)') as BatchNo,
							
							  b.value('@PhysicalQty', 'varchar(50)') as PhysicalQty,
							  
							  b.value('@ManufactureBatchNo', 'varchar(50)') as ManufactureBatchNo
							 
								into #ItemsBatchInformation
							  FROM @xml.nodes('/root/item') a(b)				  
							  alter table  #ItemsBatchInformation add RowNo Int Identity(1,1);
							  
				Declare @ItemBatchDetail Table 
				(
					Ident INT Identity(1,1),
					RowNo INT,
					ItemRowNo INT,
					BatchNo VARCHAR(20),
					ManufactureBatchNo VARCHAR(20),
					PhysicalQuantity	Numeric(12,4)
				)

				Insert INTO @ItemBatchDetail(BatchNo,	ManufactureBatchNo,		PhysicalQuantity)
				Select 	BatchNo,	ManufactureBatchNo,		PhysicalQty
				From #ItemsBatchInformation			  
							  
			END
		--Select * From @ItemDetail

		set @ModifiedBy = @createdBy -- need to change later when sending modified by flag.	
		
		set @StockCountStatus = 0;
	
		IF ISNULL(NULLIF(@SeqNo,''),'-1')='-1'
		BEGIN
			declare @initiateOrNot int;
			declare @savedOrNot int;
			select @initiateOrNot = COUNT(*) from StockCountHeader where LocationId = @LocationId and Status = 3 ;
			
			select @savedOrNot = COUNT(*) from StockCountHeader where LocationId = @LocationId and Status = 1 ;
			if @initiateOrNot > 0
			begin
				select 'SC0001' as outParam;
				return;
			end
			if @savedOrNot > 0
			begin
				select 'SC0002' as outParam;
				return;
			end
			else
			begin
				EXECUTE usp_GetSeqNo 'SC', @LocationId, @RetSeqNo Out
				SET @SeqNo=@RetSeqNo
			
				INSERT INTO dbo.StockCountHeader	(StockCountSeqNo,	LocationId,	Status, CreatedBy, InitiatedDate,CreatedDate ,ModifiedBy, ModifiedDate,  InitiatedBy,Remarks)
				VALUES	(@SeqNo,	@LocationId,	@StatusId,	@ModifiedBy, @InitiatedDate,GETDATE(),	@ModifiedBy,GETDATE(),CASE WHEN @StatusId = 3 THEN @ModifiedBy ELSE NULL END, @Remarks)
			end
			
		END
		ELSE
		BEGIN
			-- Check Concurrency, Get DBDate
			DECLARE @DBDate DATETIME
			--SELECT @DBDate = (SELECT ModifiedDate = (CASE WHEN ModifiedBy Is Not Null THEN ModifiedDate ELSE CreatedDate END) FROM StockCountHeader WHERE  StockCountSeqNo = @SeqNo)
			select @DBDate = (select InitiatedDate FROM StockCountHeader WHERE  StockCountSeqNo = @SeqNo)
			--modified date
			if @StatusId = 6
			begin
				set @StatusId = @StatusId;
			end

			else
			begin
				IF Convert(varchar(20),CAST(GETDATE() As DateTime),105) <> Convert(varchar(20),CAST(@DBDate As DateTime),105)      -- CONVERT(VARCHAR(20),@DBDate,120)
			--select @new = Convert(varchar(20),CAST(@DBDate As DateTime),105)
			
				BEGIN
					SET @outParam = 'INF0022'
					Return
				END
			end
			
			
			
			--Select @StatusId
			-- When Updated On HO 
				
				
			
				UPDATE StockCountHeader
				SET	
					ModifiedBy			=	@ModifiedBy,		
					ModifiedDate		=	GETDATE(),
					InitiatedDate		=   CASE WHEN @StatusId = 3 THEN GETDATE() ELSE InitiatedDate END,
					InitiatedBy			=   CASE WHEN @StatusId = 3 THEN @ModifiedBy ELSE InitiatedBy END,
					Status				=	CASE WHEN @StatusId = 6 THEN Status ELSE @StatusId END,-- Status for closed and cancelled will be updated later.
					--ExecutedDate		=	CASE WHEN @StatusId = 5 THEN GETDATE() ELSE ExecutedDate END,
					--ExecutedBy		=	CASE WHEN @StatusId = 5 THEN @ModifiedBy ELSE ExecutedBy END,
					ExecutedDate		=	CASE WHEN @StatusId = 5 THEN GETDATE() ELSE ExecutedDate END,
					ExecutedBy			=	CASE WHEN @StatusId = 5 THEN @ModifiedBy ELSE ExecutedBy END,
					Remarks				=	CASE WHEN @StatusId = 6 THEN @Remarks ELSE Remarks END
				WHERE StockCountSeqNo = @SeqNo
		END


		--------------------------------------------------------------------------------------------------
		-- INTERFACE
		---------------------------------------------------------------------------------------------------
		DECLARE @LocationCode AS VARCHAR(20)
		DECLARE @RecCnt AS VARCHAR(2000)
		IF @StatusId = 3 -- Initiated From HO
		BEGIN
			
			SELECT @LocationCode = LocationCode FROM Location_Master WHERE LocationId = @LocationId
			Exec [usp_Interface_Audit] '', 'STKCNT', @LocationCode, 'StockCountHeader', @SeqNo, NULL, NULL, NULL, NULL, 'I', @ModifiedBy, @RecCnt OUTPUT	
		END
		ELSE IF @StatusId = 6 --CLOSED From HO
		BEGIN
			SELECT @LocationCode = LocationCode FROM Location_Master WHERE LocationId = @LocationId
			Exec [usp_Interface_Audit] '', 'STKCNT', @LocationCode, 'StockCountHeader', @SeqNo, NULL, NULL, NULL, NULL, 'U', @ModifiedBy, @RecCnt OUTPUT	
		END
		ELSE IF @StatusId = 5 -- Executed From BO/WH
		BEGIN
			SELECT @LocationCode = LocationCode FROM Location_Master WHERE LocationType = 1
			Exec [usp_Interface_Audit] '', 'STKCNT', @LocationCode, 'StockCountHeader', @SeqNo, NULL, NULL, NULL, NULL, 'U', @ModifiedBy, @RecCnt OUTPUT	
		END
		
		Declare @checkInitiatedOrNot int;
		---------------------------------------------------------------------------------------------------

		IF @StatusId = 3 -- 1 For Created, 3 For Initiated  . 1 removed from there.
		BEGIN
		  select @checkInitiatedOrNot=COUNT(*)    from StockCountDetail where 
									StockCountSeqNo = @SeqNo 
			
			if @checkInitiatedOrNot = 0
			BEGIN
				DECLARE @initiatedTable TABLE(
				seqNo varchar(50),
				itemId int,
				rno int identity(1,1)
				)
				insert into @initiatedTable
				select @SeqNo ,IM.ItemId 'itemid' 
				FROM MerchHierarchy_Master MM ,Item_Master IM
				INNER JOIN ITEMLOCATION_LINK ILL
				ON IM.ItemId = ILL.ItemId
				INNER JOIN ItemMRP IMRP
				ON IM.ItemId = IMRP.ItemId
				INNER JOIN ItemUOM_Link IUOM
				ON IM.ItemId = IUOM.ItemId
				AND IUOM.TypeOfMeasure = 2
				LEFT OUTER JOIN 
					(
						SELECT SUM(Quantity) Quantity, Ilbb.ItemId, LocationId FROM dbo.Inventory_LocBucketBatch ilbb LEFT JOIN ItemBatch_Detail IBD
						ON IBD.BatchNo=ILBB.BatchNo
						WHERE ilbb.BucketId = (SELECT BucketId FROM dbo.Bucket WHERE ParentId IS NOT NULL AND sellable = 1) AND IBD.ExpDate>GETDATE()
						GROUP BY ilbb.ItemId, LocationId
					) a
				ON IM.ItemId = a.ItemId
				AND ILL.LocationId = a.LocationId
				WHERE 
				MM.MerchHierarchyId=IM.MerchHierarchyDetailId 
				AND ILL.LocationId = @LocationId
				AND IM.MerchHierarchyDetailId in (Select MerchHierarchyId From MerchHierarchy_Master where Status = 1 AND IsTradable = 1)
				AND IM.STATUS = 1 
				AND ILL.Status = 1 
				--AND ((@isKitOrder = 1 AND IM.IsKit = 1) OR(@isKitOrder = 0 AND 1=1))
					--AND IM.ItemId IN (361, 362)
				AND ((ISNULL(IM.IsForRegistrationPurpose,0) = ISNULL(0,0) AND 1 = 2)  
					OR (1 = 0  AND ISNULL(IM.IsForRegistrationPurpose,0) <> 1)--AND IM.IsKit <> 1

					OR (1 = 1 AND ( ISNULL(IM.IsForRegistrationPurpose,0) = 0))--IM.IsKit = 1 OR
					)
					ORDER BY IM.DisplayName 
				
					insert into StockCountDetail(StockCountSeqNo,RowNo,itemid,BucketId) SELECT  seqNo,rno,itemId,'' from @initiatedTable
			END
		
		END
		
		--ELSE IF @StatusId = 4 OR @StatusId = 5 -- 4 For Processed, 5 For Executed
		--BEGIN
		--	DELETE FROM StockCountDetail WHERE StockCountSeqNo = @SeqNo
		--	INSERT INTO StockCountDetail (StockCountSeqNo, RowNo, ItemId, BucketId, SystemQuantity)
		--	SELECT @SeqNo, Ident, ItemId, BucketId, SystemQuantity FROM @ItemDetail
		--END
		
		ELSE IF @StatusId = 1 -- 1 for save assumed.
		BEGIN
		
			DELETE FROM StockCountDetail WHERE StockCountSeqNo = @SeqNo
			INSERT INTO StockCountDetail (StockCountSeqNo, RowNo,ItemId, BucketId, PhysicalQuantity)
			SELECT @SeqNo, Ident, ItemId, 5,OnHandQuantity FROM @ItemDetail 
			UNION ALL
			SELECT @SeqNo, Ident+1, ItemId, 6,DamagedQuantity FROM @ItemDetail 
			
			set @StockCountStatus = 1;
			
			select 1 as 'saved';
			
			
		END
		
		
		ELSE IF @StatusId = 5 -- 5 for confirm assumed.
		BEGIN
		
			DELETE FROM StockCountDetail WHERE StockCountSeqNo = @SeqNo
			INSERT INTO StockCountDetail (StockCountSeqNo, RowNo,ItemId, BucketId, PhysicalQuantity)
			SELECT @SeqNo, Ident, ItemId, 5,OnHandQuantity FROM @ItemDetail 
			UNION ALL
			SELECT @SeqNo, Ident+1, ItemId, 6,DamagedQuantity FROM @ItemDetail 
					
					
					
			update StockCountDetail set SystemQuantity = (select isNull(sum(Quantity),0) 
			from Inventory_LocBucketBatch ILBB where ILBB.ItemId=StockCountDetail.ItemId 
			and ILBB.BucketId=StockCountDetail.BucketId )
			
		select IM.ItemCode, IM.ItemName,SCD1.ItemId ,SCD1.systemQuantity OnHandBucketSYS ,SCD1.physicalQuantity as OnHandBucket 
		,SCD2.SystemQuantity DamagedBucketSYS ,SCD2.physicalQuantity 
		as DamagedBucket,((SCD1.SystemQuantity+SCD2.SystemQuantity)-(SCD1.physicalQuantity + SCD2.physicalQuantity)) as Varience	
		,((SCD1.SystemQuantity+SCD2.SystemQuantity)-(SCD1.physicalQuantity + SCD2.physicalQuantity))*IM.DistributorPrice 
		as VariancePrice from StockCountDetail SCD1 ,   StockCountDetail SCD2 left join Item_Master IM on 
		 IM.ItemId =  SCD2.ItemId 
		where  SCD1.BucketId=5 AND SCD2.BucketId=6 AND
		SCD1.ItemId=  SCD2.ItemId AND SCD1.StockCountSeqNo=SCD2.StockCountSeqNo
		 AND SCD1.StockCountSeqNo = @SeqNo
			
			
			
			
			
		END
		
		ELSE IF @StatusId = 6 -- 1 for close assumed.
		BEGIN
		
			declare @CloseDecisionFlag int;
			
			select @CloseDecisionFlag = status from StockCountHeader where StockCountSeqNo = @SeqNo;
			
			
			
			
			if @CloseDecisionFlag = 3
			begin
				update StockCountHeader set Status = 2 where StockCountSeqNo = @SeqNo;
			end
			
			if @CloseDecisionFlag = 1
			begin
				update StockCountHeader set Status = 6 where StockCountSeqNo = @SeqNo;
			end
			
			else if @CloseDecisionFlag = 5
			begin
				update StockCountHeader set Status = 6 where StockCountSeqNo = @SeqNo;
			end
		
			select StockCountSeqNo, status from StockCountHeader where StockCountSeqNo = @SeqNo; 	
			
		END
		
		SELECT StockCountSeqNo SeqNo,
				Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate,Convert(VARCHAR(20), InitiatedDate , 120) InitiatedDate
		FROM StockCountHeader 
		WHERE StockCountSeqNo = @SeqNo

	END TRY
	BEGIN CATCH
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DistributorUpdate]    Script Date: 02/09/2014 13:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*exec [usp_DistributorUpdate] '<Distributor  > 
 <BankType>B</BankType>  
 <DistributorId>11000005</DistributorId>  
 <UplineDistributorId>11000001</UplineDistributorId>  
 <DistributorTitleId>1</DistributorTitleId>  <DistributorFirstName>PUSHPLATA</DistributorFirstName>  <DistributorLastName />  <DistributorDOB>3/1/1947 12:00:00 AM</DistributorDOB>  <CoDistributorTitleId>1</CoDistributorTitleId>  <CoDistributorFirstName />  <CoDistributorLastName />  <CoDistributorDOB>1/1/1950 12:00:00 AM</CoDistributorDOB>  <DistributorAddress1>542 SECTOR - 4</DistributorAddress1>  <DistributorAddress2>VAISHALI Vihar</DistributorAddress2>  <DistributorAddress3>DIST - GAZIABAD</DistributorAddress3>  <DistributorCityCode>8292</DistributorCityCode>  <DistributorStateCode>21</DistributorStateCode>  <DistributorCountryCode>1</DistributorCountryCode>  <SubZoneCode>0</SubZoneCode>  <AreaCode>0</AreaCode>  <DistributorPinCode />  <DistributorTeleNumber>9211790068</DistributorTeleNumber>  <DistributorMobNumber>7838333272</DistributorMobNumber>  <DistributorFaxNumber /> 
<DistributorEMailID>ajayvats07@yahoo.com</DistributorEMailID>  <DistributorRegistrationDate>0001-01-01T00:00:00</DistributorRegistrationDate>  <DistributorActivationDate>0001-01-01T00:00:00</DistributorActivationDate>  <MinFirstPurchaseAmount>0</MinFirstPurchaseAmount>  <MinimumSaleAmount>0</MinimumSaleAmount>  <LocationId>0</LocationId>  <CreatedById>0</CreatedById>  <ModifiedById>1</ModifiedById>  <AccountNumber>-1</AccountNumber>  <BankBranchCode>-1</BankBranchCode>  <DistributorPANNumber />  <FirstOrderTaken>false</FirstOrderTaken>  <ZoneCode>0</ZoneCode>  <BankCode>0</BankCode>  <Curr_PrevCumPV>0</Curr_PrevCumPV>  <AllInvoiceAmountSum>0</AllInvoiceAmountSum>  <CurrentLocationId>1</CurrentLocationId>  <DisplayCurr_PrevCumPV>0</DisplayCurr_PrevCumPV>  <Curr_ExclPV>0</Curr_ExclPV>  <DisplayCurr_ExclPV>0</DisplayCurr_ExclPV>  <Curr_SelfPV>0</Curr_SelfPV>  <DisplayCurr_SelfPV>0</DisplayCurr_SelfPV>  <Curr_GroupPV>0</Curr_GroupPV>  <DisplayCurr_GroupPV>0</DisplayCurr_GroupPV>  <Curr_TotalPV>0</Curr_TotalPV>  
<DisplayCurr_TotalPV>0</DisplayCurr_TotalPV>  <Curr_BonusPercent>0</Curr_BonusPercent>  <DisplayCurr_BonusPercent>0</DisplayCurr_BonusPercent>  <Curr_TotalCumPV>0</Curr_TotalCumPV>  <DisplayCurr_TotalCumPV>0</DisplayCurr_TotalCumPV>  <Curr_TotalBV>0</Curr_TotalBV>  <DisplayCurr_TotalBV>0</DisplayCurr_TotalBV>  <Prev_ExclPV>0</Prev_ExclPV>  <DisplayPrev_ExclPV>0</DisplayPrev_ExclPV>  <Prev_SelfPV>0</Prev_SelfPV>  <DisplayPrev_SelfPV>0</DisplayPrev_SelfPV>  <Prev_GroupPV>0</Prev_GroupPV>  <DisplayPrev_GroupPV>0</DisplayPrev_GroupPV>  <Prev_TotalPV>0</Prev_TotalPV>  <DisplayPrev_TotalPV>0</DisplayPrev_TotalPV>  <Prev_BonusPercent>0</Prev_BonusPercent>  <DisplayPrev_BonusPercent>0</DisplayPrev_BonusPercent>  <Prev_TotalBV>0</Prev_TotalBV>  <CurrentPrevCummulativePV>0</CurrentPrevCummulativePV>  <CurrentTotalCummulativePV>0</CurrentTotalCummulativePV>  <DisplayPrev_TotalBV>0</DisplayPrev_TotalBV>  <UserName />  <Password /></Distributor>',''
*/

CREATE Procedure [dbo].[sp_DistributorUpdate]   
 @PanType varchar(10), @BankType varchar(10),@distributorId numeric,@UplineDistributorId numeric,@DistributorTitleId int,@DistributorFirstName varchar(50),
 @DistributorLastName varchar(40),@DistributorDOB varchar(40),@CoDistributorTitleId int,@CoDistributorFirstName varchar(40),
 @CoDistributorLastName varchar(40),
 @CoDistributorDOB varchar(40),@DistributorAddress1 varchar(50),@DistributorAddress2 varchar(50),
 @DistributorAddress3 varchar(50),@DistributorCityCode numeric,@DistributorStateCode int,@DistributorCountryCode int,@SubZoneCode int,
 @AreaCode int,@DistributorPinCode numeric,
 @DistributorTeleNumber varchar(30),@DistributorMobNumber varchar(50),@DistributorFaxNumber varchar(30),
 @DistributorEMailID varchar(50),@DistributorRegistrationDate varchar(30),@MinimumSaleAmount int,
 @LocationId int,@CreatedById int,@ModifiedById int,
 @AccountNumber varchar(50),@BankBranchCode VARCHAR(50),@DistributorPANNumber varchar(50),
 @FirstOrderTaken varchar(10),@ZoneCode int,@BankCode int,@Curr_PrevCumPV int,@AllInvoiceAmountSum int,
 @CurrentLocationId int,
 @outParam VARCHAR(500) OUTPUT  
AS  
Begin  
 Begin Try  
   set nocount on
 DECLARE    
  @iHnd INT,  
  @date DATETIME,  
 
  
  @DistributorAccountNo VARCHAr(50),
 
  @ModifiedBy INT,  
  @iChange INT,  
  @reccnt VARCHAR(2000)  
    
  SET @date = GetDate()  
  SET @iHnd = 0  
  SET @iChange = 0  
  set @ModifiedBy = @ModifiedById
  Declare @DistributorInfo Table 
		(
			distributorId numeric,
			DistributorTitleId int,
			DistributorFirstName varchar(50),
			DistributorLastName VARCHAR(40),
			DistributorDOB	varchar(40),
			CoDistributorTitleId int,
			CoDistributorFirstName varchar(40),
			CoDistributorLastName varchar(40),
			CoDistributorDOB varchar(40),
			--IndentNo VARCHAR(20),
			DistributorAddress1 varchar(50),
			DistributorAddress2 VARCHAR(50),
			DistributorAddress3	varchar(50),
			DistributorAddress4 varchar(50),
			DistributorCityCode numeric,
			DistributorStateCode int,
			DistributorCountryCode	int,
			DistributorPinCode numeric,
			DistributorTeleNumber varchar(30),
			DistributorMobNumber varchar(20),
			DistributorFaxNumber varchar(50),
			DistributorEMailID varchar(50),
			DistributorPANNumber varchar(20),
			ModifiedBy int,
			BankCode int,
			
			
			AccountNumber varchar(50),
			BankBranchCode varchar(50),
			UplineDistributorId	numeric,
			PANType	VARCHAR(10),
			BankType VARCHAR(10)
		)
  
  Insert INTo @DistributorInfo(	distributorId, DistributorTitleId, DistributorFirstName, DistributorLastName, 
								DistributorDOB, CoDistributorTitleId, CoDistributorFirstName, 
								CoDistributorLastName,CoDistributorDOB,DistributorAddress1, DistributorAddress2,
								DistributorAddress3, DistributorAddress4,DistributorCityCode, DistributorStateCode,
								DistributorCountryCode, DistributorPinCode,DistributorTeleNumber, DistributorMobNumber,
								DistributorFaxNumber, DistributorEMailID,DistributorPANNumber, ModifiedBy,
								BankCode, AccountNumber,BankBranchCode, UplineDistributorId,
								PANType, BankType )
		Select  @distributorId,	@DistributorTitleId,@DistributorFirstName,@DistributorLastName,
				@DistributorDOB,@CoDistributorTitleId,@CoDistributorFirstName,@CoDistributorLastName,
				@CoDistributorDOB, @DistributorAddress1,@DistributorAddress2,
				@DistributorAddress3,'',@DistributorCityCode,@DistributorStateCode,@DistributorCountryCode,
				@DistributorPinCode,@DistributorTeleNumber,@DistributorMobNumber,@DistributorFaxNumber,
				@DistributorEMailID,@DistributorPANNumber,@ModifiedBy,@BankCode,@AccountNumber,
				@BankBranchCode,@UplineDistributorId,@PanType,@BankType
				
  
  
  Select      
   @DistributorAccountNo = @AccountNumber,  

   @ModifiedBy = @ModifiedById  
 
      
  IF NOT EXISTS (SELECT 1 FROM DistributorMaster DM Where DM.DistributorId = @distributorId)  
   BEGIN  
    SET @outParam = '40018'  
    RETURN  
   END  
  ELSE  
  BEGIN  
    PRINT 'GET DATA IN TEMP TABLE'  
      
    --SELECT * FROM DistributorMaster dm WHERE dm.DistributorId = @distributorId  
         
     
      
      
   
     --select * from #DistributorInfo 

	 if exists(select * from @DistributorInfo where banktype='B' and Pantype ='P')
	 begin
		insert into DistributorBankPanUpdate(distributorid,BankUpdate,PanUpdate)
		select distributorid,GETDATE(),GETDATE() 
		from @DistributorInfo
		where distributorid not in (
										select distributorid 
										from DistributorBankPanUpdate
										where distributorid =@distributorId  
								   )  	 
	 
		update dp set bankupdate=GETDATE(),
					  panupdate =GETDATE() 
		from DistributorBankPanUpdate dp 
		inner join 
			 @DistributorInfo di
		  on dp.distributorid=di.distributorid	 
			 
	 end
	 if exists(select * from @DistributorInfo where banktype='B' and Pantype is null or pantype='')
	 begin
		insert into DistributorBankPanUpdate(distributorid,BankUpdate)
		select distributorid,GETDATE()
		from @DistributorInfo
		where distributorid not in (
										select distributorid 
										from DistributorBankPanUpdate
										where distributorid =@distributorId  
								   )  	 
	 
		update dp set bankupdate=GETDATE()
		from DistributorBankPanUpdate dp 
		inner join 
			 @DistributorInfo di
		  on dp.distributorid=di.distributorid	 
	 end
	 
	 if Exists(select * from @DistributorInfo where (banktype='' or banktype is null) and Pantype ='P')
	 begin
		insert into DistributorBankPanUpdate(distributorid,PANUpdate)
		select distributorid,GETDATE()
		from @DistributorInfo
		where distributorid not in (
										select distributorid 
										from DistributorBankPanUpdate
										where distributorid =@distributorId  
								   )  	 
	 
		update dp set Panupdate=GETDATE()
		from DistributorBankPanUpdate dp 
		inner join 
			 @DistributorInfo di
		  on dp.distributorid=di.distributorid	 
	 end

  
--   PRINT 'CHECK IF UPDATE REQD.'  
      
--    SELECT 1  FROM DistributorMaster dm INNER JOIN #DistributorInfo tdi  ON dm.DistributorId = @distributorId   
--       AND dm.DistributorTitle = tdi.DistributorTitleId AND dm.DistributorFirstName = tdi.DistributorFirstName AND   
--       dm.DistributorLastName = tdi.DistributorLastName AND dm.DistributorDOB = tdi.DistributorDOB AND dm.Co_DistributorTitle= tdi.CoDistributorTitleId  
--       AND dm.Co_DistributorFirstName = tdi.CoDistributorFirstName AND dm.Co_DistributorLastName = tdi.CoDistributorLastName AND dm.Co_DistributorDOB = tdi.CoDistributorDOB  
--       AND dm.DistributorAddress1 = tdi.DistributorAddress1 AND dm.DistributorAddress2 = tdi.DistributorAddress2 AND dm.DistributorAddress3 = tdi.DistributorAddress3 AND  
--       dm.DistributorCityCode = tdi.DistributorCityCode AND dm.DistributorStateCode = tdi.DistributorStateCode AND dm.DistributorCountryCode = tdi.DistributorCountryCode AND  
--       Dm.DistributorPinCode = tdi.DistributorPinCode AND dm.DistributorTeleNumber = tdi.DistributorTeleNumber AND dm.DistributorMobNumber = tdi.DistributorMobNumber AND  
--       DM.DistributorFaxNumber = tdi.DistributorFaxNumber AND dm.DistributorEMailID = tdi.DistributorEMailID AND   
--       Dm.DistributorPANNumber = tdi.DistributorPANNumber  
--         
--         
--      
--       SELECT 1  FROM DistributorAccountDetails dad ON dad.DistributorId = @distributorId   
--       AND (@DistributorAccountNo = -1 OR tdi.AccountNumber = dad.DistributorBankAccNumber )  
--       AND (@BankCode= -1 OR  tdi.BankCode = dad.BankID )  

	 if not exists( select  1 
					from DistributorMaster dm inner join
						 @DistributorInfo di
						on dm.DistributorId =di.distributorid
					    and  dm.DistributorId =@distributorId
						and dm.DistributorFirstName =di.DistributorFirstName
						and dm.DistributorLastName =di.DistributorLastName)
		Begin
			
			INSERT INTO DistributorAccountDetails_History(DistributorId,  
			DistributorBankName, DistributorBankBranch,  
			DistributorBankAccNumber, BankID, CreatedBy,  
			CreatedDate, ModifiedBy, ModifiedDate, IsPrimary)  
			
			SELECT DISTINCT dad.DistributorId, dad.DistributorBankName,  
			 dad.DistributorBankBranch, dad.DistributorBankAccNumber,  
			 dad.BankID, dad.CreatedBy, dad.CreatedDate, dad.ModifiedBy,  
			 dad.ModifiedDate, dad.IsPrimary  
			FROM DistributorAccountDetails dad  
			WHERE dad.DistributorId = @distributorId 
			
			Delete from  DistributorAccountDetails where DistributorId = @distributorId 
		End   
		
	 if (@DistributorAccountNo = '-1' and  @BankCode = '-1'  )
	 begin
			INSERT INTO DistributorAccountDetails_History(DistributorId,  
			DistributorBankName, DistributorBankBranch,  
			DistributorBankAccNumber, BankID, CreatedBy,  
			CreatedDate, ModifiedBy, ModifiedDate, IsPrimary)  
			
			SELECT DISTINCT dad.DistributorId, dad.DistributorBankName,  
			 dad.DistributorBankBranch, dad.DistributorBankAccNumber,  
			 dad.BankID, dad.CreatedBy, dad.CreatedDate, dad.ModifiedBy,  
			 dad.ModifiedDate, dad.IsPrimary  
			FROM DistributorAccountDetails dad  
			WHERE dad.DistributorId = @distributorId 
			
			Delete from  DistributorAccountDetails where DistributorId = @distributorId 
	 end
	 	      
     IF NOT EXISTS (SELECT 1  FROM DistributorMaster dm INNER JOIN @DistributorInfo tdi  ON dm.DistributorId = @distributorId   
     AND dm.DistributorTitle = tdi.DistributorTitleId AND dm.DistributorFirstName = tdi.DistributorFirstName AND   
     dm.DistributorLastName = tdi.DistributorLastName AND dm.DistributorDOB = tdi.DistributorDOB AND dm.Co_DistributorTitle= tdi.CoDistributorTitleId  
     AND dm.Co_DistributorFirstName = tdi.CoDistributorFirstName AND dm.Co_DistributorLastName = tdi.CoDistributorLastName AND dm.Co_DistributorDOB = tdi.CoDistributorDOB  
     AND dm.DistributorAddress1 = tdi.DistributorAddress1 AND dm.DistributorAddress2 = tdi.DistributorAddress2 AND dm.DistributorAddress3 = tdi.DistributorAddress3 AND  
     dm.DistributorCityCode = tdi.DistributorCityCode AND dm.DistributorStateCode = tdi.DistributorStateCode AND dm.DistributorCountryCode = tdi.DistributorCountryCode AND  
     Dm.DistributorPinCode = tdi.DistributorPinCode AND dm.DistributorTeleNumber = tdi.DistributorTeleNumber AND dm.DistributorMobNumber = tdi.DistributorMobNumber AND  
     DM.DistributorFaxNumber = tdi.DistributorFaxNumber AND dm.DistributorEMailID = tdi.DistributorEMailID AND   
     Dm.DistributorPANNumber = tdi.DistributorPANNumber AND dm.BankID = tdi.BankCode AND dm.UplineDistributorId = tdi.UplineDistributorId )   
     BEGIN   
       INSERT INTO DistributorMaster_History(DistributorId,  
                UplineDistributorId, DistributorTitle,  
                DistributorFirstName, DistributorLastName,  
                DistributorDOB, Co_DistributorTitle,  
                Co_DistributorFirstName, Co_DistributorLastName,  
                Co_DistributorDOB, DistributorAddress1,  
                DistributorAddress2, DistributorAddress3,  
                DistributorAddress4, DistributorCityCode,  
                DistributorStateCode, DistributorCountryCode,  
                DistributorPinCode, DistributorTeleNumber,  
                DistributorMobNumber, DistributorFaxNumber,  
                DistributorEMailID, DistributorRegistrationDate,  
                DistributorActivationDate, MinFirstPurchaseAmount,  
                DistributorStatus, LocationId, DistributorPANNumber,  
                CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
                KitOrderNo, KitInvoiceNo, FirstOrderTaken, SubZone,  
                Area, Remarks, Zone, BankID, HierarchyLevel, username,password,SerialNo)  
       SELECT dm.DistributorId, dm.UplineDistributorId,  
           dm.DistributorTitle, dm.DistributorFirstName,  
           dm.DistributorLastName, dm.DistributorDOB,  
           dm.Co_DistributorTitle, dm.Co_DistributorFirstName,  
           dm.Co_DistributorLastName, dm.Co_DistributorDOB,  
           dm.DistributorAddress1, dm.DistributorAddress2,  
           dm.DistributorAddress3, dm.DistributorAddress4,  
           dm.DistributorCityCode, dm.DistributorStateCode,  
           dm.DistributorCountryCode, dm.DistributorPinCode,  
           dm.DistributorTeleNumber, dm.DistributorMobNumber,  
           dm.DistributorFaxNumber, dm.DistributorEMailID,  
           dm.DistributorRegistrationDate,  
           dm.DistributorActivationDate, dm.MinFirstPurchaseAmount,  
           dm.DistributorStatus, dm.LocationId,  
           dm.DistributorPANNumber, dm.CreatedBy, dm.CreatedDate,  
           dm.ModifiedBy, dm.ModifiedDate, dm.KitOrderNo,  
           dm.KitInvoiceNo, dm.FirstOrderTaken, dm.SubZone,  
           dm.Area, dm.Remarks, dm.Zone, dm.BankID,  
           dm.HierarchyLevel,username,password,SerialNo  
         FROM DistributorMaster dm  
         WHERE dm.DistributorId = @distributorId  
                    
      UPDATE dm  
      SET  
          dm.UplineDistributorId = tdi.UplineDistributorId,  
       dm.DistributorTitle = tdi.DistributorTitleId,  
       dm.DistributorFirstName = tdi.DistributorFirstName,  
       dm.DistributorLastName = tdi.DistributorLastName,  
       dm.DistributorDOB = tdi.DistributorDOB,  
       dm.Co_DistributorTitle = tdi.CoDistributorTitleId,  
       dm.Co_DistributorFirstName = tdi.CoDistributorFirstName,  
       dm.Co_DistributorLastName = tdi.CoDistributorLastName,  
       dm.Co_DistributorDOB = tdi.CoDistributorDOB,  
       dm.DistributorAddress1 = tdi.DistributorAddress1,  
       dm.DistributorAddress2 = tdi.DistributorAddress2,  
       dm.DistributorAddress3 = tdi.DistributorAddress3,  
       dm.DistributorAddress4 = tdi.DistributorAddress4,  
       dm.DistributorCityCode = tdi.DistributorCityCode,  
       dm.DistributorStateCode = tdi.DistributorStateCode,  
       dm.DistributorCountryCode = tdi.DistributorCountryCode,  
       dm.DistributorPinCode = tdi.DistributorPinCode,  
       dm.DistributorTeleNumber = tdi.DistributorTeleNumber,  
       dm.DistributorMobNumber = tdi.DistributorMobNumber,  
       dm.DistributorFaxNumber = tdi.DistributorFaxNumber,  
       dm.DistributorEMailID = tdi.DistributorEMailID,  
       dm.DistributorPANNumber = tdi.DistributorPANNumber,  
       dm.ModifiedBy = tdi.ModifiedBy,  
       dm.ModifiedDate = getdate(),  
       dm.BankID = tdi.BankCode  
      FROM DistributorMaster dm  
      INNER JOIN @DistributorInfo tdi  
      ON dm.DistributorId = tdi.DistributorId  
                    
      SET @iChange = 1  
      PRINT 'UPDATED DISTRIBUTOR MASTER'  
     END   

PRINT @DistributorAccountNo
PRINT @BankCode
PRINT @BankBranchCode
	if exists(select 1 
				from @DistributorInfo 
				where DistributorCountryCode='4')
	begin
		INSERT INTO DistributorAccountDetails_History(DistributorId,  
			DistributorBankName, DistributorBankBranch,  
			DistributorBankAccNumber, BankID, CreatedBy,  
			CreatedDate, ModifiedBy, ModifiedDate, IsPrimary)  
		 SELECT DISTINCT dad.DistributorId, dad.DistributorBankName,  
			 dad.DistributorBankBranch, dad.DistributorBankAccNumber,  
			 dad.BankID, dad.CreatedBy, dad.CreatedDate, dad.ModifiedBy,  
			 dad.ModifiedDate, dad.IsPrimary  
		 FROM DistributorAccountDetails dad  
		 WHERE dad.DistributorId = @distributorId 
	
		if(@BankCode<>-1)
		 Begin
		   Print '2'
			 UPDATE dad  
			 SET       
			  DistributorBankName = pm.KeyValue1,  
			  DistributorBankBranch = tdi.BankBranchCode,  
			  DistributorBankAccNumber = tdi.AccountNumber,  
			  BankID = tdi.BankCode,  
			  ModifiedBy = tdi.ModifiedBy,  
			  ModifiedDate = getdate()  
		  
			 FROM DistributorAccountDetails dad  
			 INNER JOIN @DistributorInfo tdi  
			 ON dad.DistributorId = tdi.DistributorId  
			 AND dad.IsPrimary = 1  
			 INNER JOIN Parameter_Master pm  
			 ON pm.ParameterCode = 'BANKID'  
			 AND pm.KeyCode1 = tdi.BankCode
			 
				  --Insert account details  
	       
			 INSERT INTO DistributorAccountDetails  
			 (  
			  DistributorId,  
			  DistributorBankName,  
			  DistributorBankBranch,  
			  DistributorBankAccNumber,  
			  BankId,  
			  CreatedBy,  
			  CreatedDate,  
			  ModifiedBy,  
			  ModifiedDate,  
			  IsPrimary  
			 )  
			 SELECT  tdi.DistributorId,  
			   pm.KeyValue1,   
			   tdi.BankBranchCode,  
			   tdi.AccountNumber,  
			   tdi.BankCode,  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   1  
			 FROM @DistributorInfo tdi  
			 INNER JOIN Parameter_Master pm  
			 ON pm.ParameterCode = 'BANKID'  
			 AND pm.KeyCode1 = tdi.BankCode  
			 AND tdi.AccountNumber <> '-1'  
			 WHERE tdi.DistributorId NOT IN (SELECT d.DistributorId FROM DistributorAccountDetails d)  
	       
		 end
		 else
			begin 
			UPDATE dad  
			 SET       
			  DistributorBankName ='' ,  
			  DistributorBankBranch = tdi.BankBranchCode,  
			  DistributorBankAccNumber = tdi.AccountNumber,  
			  BankID = tdi.BankCode,  
			  ModifiedBy = tdi.ModifiedBy,  
			  ModifiedDate = getdate()  
		  
			 FROM DistributorAccountDetails dad  
			 INNER JOIN @DistributorInfo tdi  
			 ON dad.DistributorId = tdi.DistributorId  
			 AND dad.IsPrimary = 1  
	     
				 --Insert account details  
	       
			 INSERT INTO DistributorAccountDetails  
			 (  
			  DistributorId,  
			  DistributorBankName,  
			  DistributorBankBranch,  
			  DistributorBankAccNumber,  
			  BankId,  
			  CreatedBy,  
			  CreatedDate,  
			  ModifiedBy,  
			  ModifiedDate,  
			  IsPrimary  
			 )  
			 SELECT  tdi.DistributorId,  
			   '',   
			   tdi.BankBranchCode,  
			   tdi.AccountNumber,  
			   tdi.BankCode,  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   1  
			 FROM @DistributorInfo tdi  
			 --INNER JOIN Parameter_Master pm  
			 --ON pm.ParameterCode = 'BANKID'  
			 --AND pm.KeyCode1 = tdi.BankCode  
			 WHERE tdi.DistributorId NOT IN (SELECT d.DistributorId FROM DistributorAccountDetails d) 
		end
	end

    --IF ((@DistributorAccountNo <> '-1' AND @BankCode <> -1)   
    IF ((@DistributorAccountNo <> '-1' AND @BankBranchCode <> '-1')   
     AND NOT EXISTS   
     (  
      SELECT 1  FROM DistributorAccountDetails dad WHERE dad.DistributorId = @distributorId
      AND dad.BankID = @BankCode AND dad.DistributorBankBranch = @BankBranchCode
      AND dad.DistributorBankAccNumber = @DistributorAccountNo
     ))  
    BEGIN  
		PRINT'yes'
		 INSERT INTO DistributorAccountDetails_History(DistributorId,  
			DistributorBankName, DistributorBankBranch,  
			DistributorBankAccNumber, BankID, CreatedBy,  
			CreatedDate, ModifiedBy, ModifiedDate, IsPrimary)  
		 SELECT DISTINCT dad.DistributorId, dad.DistributorBankName,  
			 dad.DistributorBankBranch, dad.DistributorBankAccNumber,  
			 dad.BankID, dad.CreatedBy, dad.CreatedDate, dad.ModifiedBy,  
			 dad.ModifiedDate, dad.IsPrimary  
		 FROM DistributorAccountDetails dad  
		 WHERE dad.DistributorId = @distributorId  
	       
		 Print '1' 
	     
		 if(@BankCode<>-1)
		 Begin
		   Print '2'
			 UPDATE dad  
			 SET       
			  DistributorBankName = pm.KeyValue1,  
			  DistributorBankBranch = tdi.BankBranchCode,  
			  DistributorBankAccNumber = tdi.AccountNumber,  
			  BankID = tdi.BankCode,  
			  ModifiedBy = tdi.ModifiedBy,  
			  ModifiedDate = getdate()  
		  
			 FROM DistributorAccountDetails dad  
			 INNER JOIN @DistributorInfo tdi  
			 ON dad.DistributorId = tdi.DistributorId  
			 AND dad.IsPrimary = 1  
			 INNER JOIN Parameter_Master pm  
			 ON pm.ParameterCode = 'BANKID'  
			 AND pm.KeyCode1 = tdi.BankCode
			 
				  --Insert account details  
	       
			 INSERT INTO DistributorAccountDetails  
			 (  
			  DistributorId,  
			  DistributorBankName,  
			  DistributorBankBranch,  
			  DistributorBankAccNumber,  
			  BankId,  
			  CreatedBy,  
			  CreatedDate,  
			  ModifiedBy,  
			  ModifiedDate,  
			  IsPrimary  
			 )  
			 SELECT  tdi.DistributorId,  
			   pm.KeyValue1,   
			   tdi.BankBranchCode,  
			   tdi.AccountNumber,  
			   tdi.BankCode,  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   1  
			 FROM @DistributorInfo tdi  
			 INNER JOIN Parameter_Master pm  
			 ON pm.ParameterCode = 'BANKID'  
			 AND pm.KeyCode1 = tdi.BankCode  
			 AND tdi.AccountNumber <> '-1'  
			 WHERE tdi.DistributorId NOT IN (SELECT d.DistributorId FROM DistributorAccountDetails d)  
	       
		 end
		 else
		 begin
		   print '3'
			UPDATE dad  
			 SET       
			  DistributorBankName ='' ,  
			  DistributorBankBranch = tdi.BankBranchCode,  
			  DistributorBankAccNumber = tdi.AccountNumber,  
			  BankID = tdi.BankCode,  
			  ModifiedBy = tdi.ModifiedBy,  
			  ModifiedDate = getdate()  
		  
			 FROM DistributorAccountDetails dad  
			 INNER JOIN @DistributorInfo tdi  
			 ON dad.DistributorId = tdi.DistributorId  
			 AND dad.IsPrimary = 1  
	     
				 --Insert account details  
	       
			 INSERT INTO DistributorAccountDetails  
			 (  
			  DistributorId,  
			  DistributorBankName,  
			  DistributorBankBranch,  
			  DistributorBankAccNumber,  
			  BankId,  
			  CreatedBy,  
			  CreatedDate,  
			  ModifiedBy,  
			  ModifiedDate,  
			  IsPrimary  
			 )  
			 SELECT  tdi.DistributorId,  
			   '',   
			   tdi.BankBranchCode,  
			   tdi.AccountNumber,  
			   tdi.BankCode,  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   1  
			 FROM @DistributorInfo tdi  
			 --INNER JOIN Parameter_Master pm  
			 --ON pm.ParameterCode = 'BANKID'  
			 --AND pm.KeyCode1 = tdi.BankCode  
			 WHERE tdi.DistributorId NOT IN (SELECT d.DistributorId FROM DistributorAccountDetails d)  
					and tdi.AccountNumber <> '-1' and tdi.DistributorCountryCode <>'4'
					
		   print '4'
			 INSERT INTO DistributorAccountDetails  
			 (  
			  DistributorId,  
			  DistributorBankName,  
			  DistributorBankBranch,  
			  DistributorBankAccNumber,  
			  BankId,  
			  CreatedBy,  
			  CreatedDate,  
			  ModifiedBy,  
			  ModifiedDate,  
			  IsPrimary  
			 )  
			 SELECT  tdi.DistributorId,  
			   '',   
			   tdi.BankBranchCode,  
			   tdi.AccountNumber,  
			   tdi.BankCode,  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   tdi.ModifiedBy,  
			   GetDate(),  
			   1  
			 FROM @DistributorInfo tdi  
			 --INNER JOIN Parameter_Master pm  
			 --ON pm.ParameterCode = 'BANKID'  
			 --AND pm.KeyCode1 = tdi.BankCode  
			 WHERE tdi.DistributorId NOT IN (SELECT d.DistributorId FROM DistributorAccountDetails d)  
					and tdi.AccountNumber <> '-1' and tdi.DistributorCountryCode ='4'

		     
	     
		 End 
	       
	  
		 SET @iChange = 1  
		 PRINT 'UPDATED DISTRIBUTOR ACC DET'  
       
   END   
   
        
    --RETURN  
      
--    IF NOT EXISTS (SELECT 1  FROM DistributorMaster dm INNER JOIN #DistributorInfo tdi  ON dm.DistributorId = @distributorId   
--       AND dm.DistributorTitle = tdi.DistributorTitleId AND dm.DistributorFirstName = tdi.DistributorFirstName AND   
--       dm.DistributorLastName = tdi.DistributorLastName AND dm.DistributorDOB = tdi.DistributorDOB AND dm.Co_DistributorTitle= tdi.CoDistributorTitleId  
--       AND dm.Co_DistributorFirstName = tdi.CoDistributorFirstName AND dm.Co_DistributorLastName = tdi.CoDistributorLastName AND dm.Co_DistributorDOB = tdi.CoDistributorDOB  
--       AND dm.DistributorAddress1 = tdi.DistributorAddress1 AND dm.DistributorAddress2 = tdi.DistributorAddress2 AND dm.DistributorAddress3 = tdi.DistributorAddress3 AND  
--       dm.DistributorCityCode = tdi.DistributorCityCode AND dm.DistributorStateCode = tdi.DistributorStateCode AND dm.DistributorCountryCode = tdi.DistributorCountryCode AND  
--       Dm.DistributorPinCode = tdi.DistributorPinCode AND dm.DistributorTeleNumber = tdi.DistributorTeleNumber AND dm.DistributorMobNumber = tdi.DistributorMobNumber AND  
--       DM.DistributorFaxNumber = tdi.DistributorFaxNumber AND dm.DistributorEMailID = tdi.DistributorEMailID AND   
--       Dm.DistributorPANNumber = tdi.DistributorPANNumber AND dm.BankID = tdi.BankCode )   
--       OR   
--       NOT EXISTS(SELECT 1  FROM #DistributorInfo tdi LEFT JOIN DistributorAccountDetails dad ON dad.DistributorId = @distributorId AND tdi.AccountNumber = dad.DistributorBankAccNumber AND tdi.AccountNumber <> -1 AND tdi.BankCode = dad.BankID AND  tdi.BankCode <> -1)  
--    BEGIN  
      
  
      
      
      
    --SELECT * FROM DistributorMaster dm WHERE dm.DistributorId = @distributorId;  
      
    --SELECT * FROM DistributorAccountDetails dad WHERE dad.DistributorId = @distributorId  
      
       
   --SELECT * FROM DistributorAccountDetails dad WHERE dad.DistributorId = @distributorId  
   PRINT 'UPDATE FINISHED'  
     
   IF(@iChange = 1)  
    BEGIN  
     EXEC usp_Interface_Audit '','DISTMST','','DistributorMaster',@distributorId,NULL,NULL,NULL,NULL,'U',@ModifiedBy,@reccnt OUTPUT  
     PRINT 'INSERTED IN INTERFACE'  
    END  
     
   set @outParam = 'S00DIS';
   select @outParam 'OutParam' 
      
      
  END   
  
  
 End Try  
 Begin Catch  
  Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()  
  Return ERROR_NUMBER()  
 End Catch  
End
GO
/****** Object:  StoredProcedure [dbo].[SP_DistributorSummaryReport]    Script Date: 02/09/2014 13:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_DistributorSummaryReport]    
@UPL varchar(8)    
AS         
BEGIN        
DECLARE @ARN VARCHAR(8)      
DECLARE @DRN VARCHAR(8)      
DECLARE @PCENT INT      
DECLARE @COUNT INT      
DECLARE @COUNT1 INT      
DECLARE @DSTARN VARCHAR(8)      
DECLARE @DSTUPL VARCHAR(8)      
CREATE TABLE #TEMP1(Dst_No varchar(8),Pcent int)      
CREATE TABLE #TEMPBELOW20(Dst_No varchar(8), changed bit, upl varchar(8))      
CREATE TABLE #TEMPMAIN(Dst_No varchar(8), changed bit, upl varchar(8))      
CREATE TABLE #TEMP(Arn varchar(8))      
CREATE TABLE #DSTTEMP(SNo int,Dst_No varchar(8),Dst_Name varchar(60),Pcent int,Cumbp numeric(16,2),Ebp numeric(10,2),Sbp numeric(10,2),Gbp numeric(10,2),Bp numeric(10,2),Bv int,upl varchar(8),levelid int)      
--Insert Section      
INSERT INTO #TEMP SELECT DISTRIBUTORID from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID=@UPL and DISTRIBUTORSTATUS=2      
SET NOCOUNT ON;      
insert into #TEMP1(Dst_No,Pcent)      
select DISTRIBUTORID,BONUSPERCENT from DISTRIBUTORBUSINESS_CURRENT where DISTRIBUTORID in(select Arn from #TEMP)      
DECLARE C1 CURSOR FOR      
SELECT Dst_No,Pcent FROM #TEMP1      
OPEN C1      
FETCH NEXT FROM C1 INTO @ARN,@PCENT      
WHILE @@FETCH_STATUS=0      
BEGIN      
INSERT INTO #TEMPBELOW20(Dst_No, changed, upl)VALUES(@ARN,0, @upl)      
      
FETCH NEXT FROM C1 INTO @ARN,@PCENT      
      
END      
      
CLOSE C1      
      
DEALLOCATE C1      
      
declare c1 cursor for      
      
select dst_no from #tempbelow20 where changed = 0      
      
open c1      
      
fetch next from c1 into @drn      
      
while @@fetch_status = 0      
      
begin      
      
insert into #tempbelow20(dst_no, changed, upl) select DISTRIBUTORID as dst_no, 0 as changed, @drn      
      
from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID =@drn and DISTRIBUTORSTATUS = 2      
      
update #tempbelow20 set changed = 1 where dst_no = @drn      
      
fetch next from c1 into @drn      
      
end      
      
CLOSE C1      
      
DEALLOCATE C1      
      
INSERT INTO #TEMPMAIN(Dst_No, changed, upl)      
      
Select @UPL as Dst_No,0,dbo.fnGetUpline(@UPL)      
      
union      
      
select Dst_No,Changed,UPL from #TEMPBELOW20      
      
DECLARE      
C1 CURSOR FOR      
      
SELECT      
Dst_No,UPL FROM #TEMPMAIN      
      
OPEN C1      
      
FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL      
      
WHILE @@FETCH_STATUS=0      
      
BEGIN      
      
insert into #DSTTEMP(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl,levelid)      
      
select @COUNT,@DSTARN,isnull(dbo.fnGetDistributorName(@DSTARN),'N/A'),BONUSPERCENT,PrevCumPV,ExclPV,SelfPV,GroupPV,TotalPV,TotalBV,@DSTUPL,levelid from DISTRIBUTORBUSINESS_CURRENT where DISTRIBUTORID =(select Dst_No from #TEMPMAIN where #TEMPMAIN.Dst_NO=@DSTARN)     
     
      
SET @COUNT=@COUNT+1      
      
FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL      
      
END      
      
CLOSE C1      
      
DEALLOCATE C1      
select row_number() over(order by tm.Dst_No asc) as 'SNo.',tm.Dst_No,tm.Dst_Name,tm.Pcent,tm.Cumbp,tm.Ebp,tm.Sbp,tm.Gbp,tm.Bp,tm.Bv,tm.upl,dst.distributoremailid,dst.distributorregistrationdate ,dl.levelname from #dsttemp tm    
inner join distributormaster dst    
on  tm.Dst_No=dst.DistributorId     
inner join  distributorlevels dl    
on tm.levelid=dl.levelid where  dst.distributorregistrationdate between DATEADD(yyyy, -1, GETDATE())  and getdate()  
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_Distributor_SummaryReportNew]    Script Date: 02/09/2014 13:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,Manikant>      
-- Create date: <Create Date,10 Dec 2011>      
-- Description: <Description,Get Downlines>      
-- =============================================      
CREATE PROCEDURE [dbo].[Sp_Distributor_SummaryReportNew]       
 -- Add the parameters for the stored procedure here      
@UPL varchar(8) ,  
@levelid int   
AS      
BEGIN      
      
-- local declaration section      
DECLARE @ARN VARCHAR(8)      
DECLARE @DRN VARCHAR(8)      
DECLARE @PCENT INT      
DECLARE @COUNT INT      
DECLARE @COUNT1 INT      
DECLARE @DSTARN VARCHAR(8)      
DECLARE @DSTUPL VARCHAR(8)      
set @COUNT=1      
set @COUNT1=1      
      
-- tbl for get all distributor no and percent against upl      
CREATE TABLE #TEMP1(Dst_No varchar(8),Pcent int)      
-- tbl for get distributor no having 20 percent      
CREATE TABLE #TEMP20(Dst_No varchar(8), changed bit, upl varchar(8))      
-- tbl for get distributor no having below 20 percent      
CREATE TABLE #TEMPBELOW20(Dst_No varchar(8), changed bit, upl varchar(8))      
--tbl for get final result all distributor no       
CREATE TABLE #TEMPMAIN(Dst_No varchar(8), changed bit, upl varchar(8))      
-- tbl for get distributor no against upl      
CREATE TABLE #TEMP(Arn varchar(8))      
-- tbl for all merged record with points detail      
CREATE TABLE #DSTTEMP(SNo int,Dst_No varchar(8),Dst_Name varchar(60),Pcent int,Cumbp numeric(16,2),Ebp numeric(10,2),Sbp numeric(10,2),Gbp numeric(10,2),Bp numeric(10,2),Bv int,upl varchar(8))      
-- tbl for all merged record with point detail and to get next level and estimate point short      
CREATE TABLE #DSTPOINT(SNo int,Dst_No varchar(8),Dst_Name varchar(60),Pcent int,Cumbp numeric(16,2),Ebp numeric(10,2),Sbp numeric(10,2),Gbp numeric(10,2),Bp numeric(10,2),Bv int,upl varchar(8),sumbp numeric(16,2),NxtLvl int,PntShort numeric(10,2))      
      
--Insert Section      
    INSERT INTO #TEMP SELECT DISTRIBUTORID from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID=@UPL and DISTRIBUTORSTATUS=2      
 SET NOCOUNT ON;      
      insert into #TEMP1(Dst_No,Pcent)      
           select DISTRIBUTORID,BONUSPERCENT from DISTRIBUTORBUSINESS_CURRENT where DISTRIBUTORID in(select Arn from #TEMP)      
-- End of Insert Section      
      
--Loop for break Record to get distributor number having 20 pcent and below than 20 pcent        
      
       
  DECLARE C1 CURSOR FOR      
   SELECT Dst_No,Pcent FROM #TEMP1      
 OPEN C1      
 FETCH NEXT FROM C1 INTO @ARN,@PCENT          
 WHILE @@FETCH_STATUS=0      
 BEGIN      
   IF(@PCENT=20)          
     BEGIN      
       INSERT INTO #TEMP20(Dst_No,changed , upl)VALUES(@ARN,0,@UPL)      
     END      
   IF(@PCENT<20)        
     BEGIN      
      INSERT INTO #TEMPBELOW20(Dst_No, changed, upl)VALUES(@ARN,0, @upl)      
     END      
 FETCH NEXT FROM C1 INTO @ARN,@PCENT       
 END      
    CLOSE C1      
    DEALLOCATE C1       
-- end of loop           
      
-- loop to get whole downline of distribtor number having below 20 percent       
      
      
 declare c1 cursor for       
 select dst_no from #tempbelow20 where changed = 0      
  open c1      
 fetch next from c1 into @drn      
 while @@fetch_status = 0      
 begin      
  insert into #tempbelow20(dst_no, changed, upl) select DISTRIBUTORID as dst_no, 0 as changed, @drn       
  from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID =@drn and DISTRIBUTORSTATUS = 2      
  update #tempbelow20 set changed = 1 where dst_no = @drn      
  fetch next from c1 into @drn      
 end      
 CLOSE C1      
    DEALLOCATE C1       
-- end of loop      
      
-- insert section to merged all records       
  INSERT INTO #TEMPMAIN(Dst_No, changed, upl)      
   Select @UPL as Dst_No,0,dbo.fnGetUpline(@UPL)      
    union      
    select Dst_No,Changed,UPL from #TEMP20      
    union      
    select Dst_No,Changed,UPL from #TEMPBELOW20      
-- end of insert section      
      
-- loop to get points and cumbp,cumbv of all merged records      
      
DECLARE C1 CURSOR FOR      
SELECT Dst_No,UPL FROM #TEMPMAIN      
 OPEN C1      
 FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL          
 WHILE @@FETCH_STATUS=0      
 BEGIN      
     insert into #DSTTEMP(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl)      
           select @COUNT,@DSTARN,isnull(dbo.fnGetDistributorName(@DSTARN),'N/A'),BONUSPERCENT,PrevCumPV,ExclPV,SelfPV,GroupPV,TotalPV,TotalBV,@DSTUPL  from DISTRIBUTORBUSINESS_CURRENT where DISTRIBUTORID =(select Dst_No from #TEMPMAIN where #TEMPMAIN.Dst_NO=@DSTARN)      
       SET @COUNT=@COUNT+1       
   FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL       
 END      
    CLOSE C1      
    DEALLOCATE C1       
--END OF LOOP      
-- loop to get nextlevel and point short      
      
Declare @SNo int,      
@Dst_No varchar(8),      
@Dst_Name varchar(60),      
@fPcent int,      
@Cumbp numeric(16,2),      
@Ebp numeric(10,2),      
@Sbp numeric(10,2),      
@Gbp numeric(10,2),      
@Bp numeric(10,2),      
@Bv int,      
@fupl varchar(8),      
@Sumbp numeric(16,2),      
@Pntshort numeric(16,2),      
@NxtLvl int      
set @NxtLvl=0      
set @Pntshort=0.0      
DECLARE C1 CURSOR FOR      
SELECT SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl FROM #DSTTEMP      
 OPEN C1      
 FETCH NEXT FROM C1 INTO @SNo,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@Sbp,@Gbp,@Bp,@Bv,@fupl          
 WHILE @@FETCH_STATUS=0      
 BEGIN      
     set @Sumbp=@Cumbp+@Bp      
           
     -- FORMULA TO CALCULATE NEXT LEVEL           
  if(@Sumbp<=0)      
     begin      
       set @Pntshort=16-@Sumbp      
       set @NxtLvl=5      
     end      
  ELSE       
  begin      
   if(@Sumbp>0 and @Sumbp<=500)      
   begin      
    set @Pntshort=501-@Sumbp      
    set @NxtLvl=8      
   end      
   ELSE       
   begin      
    if(@Sumbp>=501 and @Sumbp<=2000)      
    begin      
     set @Pntshort=2001-@Sumbp      
     set @NxtLvl=11      
    end      
    ELSE      
    begin       
     if(@Sumbp>=2001 and @Sumbp<=4500)      
     begin      
      set @Pntshort=4501-@Sumbp      
      set @NxtLvl=14      
     end      
     ELSE       
     begin      
      if(@Sumbp>=4501 and @Sumbp<=7500)      
      begin      
        set @Pntshort=7501-@Sumbp      
        set @NxtLvl=17      
      end      
      ELSE        
      begin      
       if(@Sumbp>=7501 and @Sumbp<=10000)      
       begin      
         set @Pntshort=10001-@Sumbp      
         set @NxtLvl=20      
       end      
       ELSE       
       begin      
        if(@Sumbp>=10001)      
        begin      
         set @nxtlvl=0      
         set @Pntshort=0.0      
        end      
       end      
      end      
     end      
    end      
   end      
     end      
     -- END OF FORMULA      
           
     insert into #DSTPOINT(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,sumbp,upl,NxtLvl,PntShort)      
           select @COUNT1,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@Sbp,@Gbp,@Bp,@Bv,@sumbp,@fupl,@NxtLvl,@PntShort      
      SET @COUNT1=@COUNT1+1      
   FETCH NEXT FROM C1 INTO @SNo,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@Sbp,@Gbp,@Bp,@Bv,@fupl         
 END      
    CLOSE C1      
    DEALLOCATE C1       
--end of loop          
--select Sno,Dst_No,upl,Dst_Name,cumbp,Ebp,Sbp,Gbp,Bp,Bv,Sumbp,Pcent,NxtLvl,PntShort from #DSTPOINT ORDER BY upl  
select row_number() over(order by tm.Dst_No asc) as 'SNo.',tm.Dst_No,tm.Dst_Name,tm.upl,tm.cumbp,tm.Ebp,tm.Sbp,tm.Gbp,tm.Bp,tm.bv,tm.Sumbp,dst.distributoremailid,dst.distributorregistrationdate ,dl.levelname from #DSTPOINT tm      
inner join distributormaster dst      
on  tm.Dst_No=dst.DistributorId       
inner join  distributorlevels dl      
on tm.levelid=dl.levelid where  dst.distributorregistrationdate between DATEADD(yyyy, -1, GETDATE())  and getdate()  and  dl.levelid=@levelid     
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClaimSave]    Script Date: 02/09/2014 13:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_ClaimSave]
    @jsonForItems nvarchar(max),
    @TONumber varchar(20),
    @StatusId int,
    @LocationId int,

    @tnumber varchar(20),
   @Remarks varchar(500),
   @createdBy int,
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	set nocount on ;
		Declare
		
			@iHnd					INT,					@BucketId				INT,			@SourceLocationId		INT,					@PackSize				INT,			@GrossWeight			Numeric(12,4),
			@TODate					VARCHAR(20),			@CreationDate			VARCHAR(20),	@ReceivedDate			VARCHAR(20),
								@ModifiedBy				INT,			@ModifiedDate			VARCHAR(20),
			--@IndentNo				VARCHAR(20), 
						    @DestinationLocationId int,@TotalTOIAmount Numeric(18,4),@TotalTOIQuantity Numeric(18,2),
			@TotalTOQuantity		Numeric(12,4),			@TotalTOAmount			Money,			@CreatedDate			VARCHAR(20) ,
			@SystemDate				DateTime,				
			@IndexSeqNo				INT,					@ShippingBillNo			VARCHAR(20),	@ShippingDetails		VARCHAR(20),
			@ReceivedTime			VARCHAR(20),			@RefNumber				VARCHAR(100),
			@TOINumber				VARCHAR(20),			@ShippingDate			VARCHAR(20),	@RecCnt					VARCHAR(2000),
			@Exportvalue			int,@InputParam varchar(500)

			Set @SystemDate = getdate()
			Declare @RowNo INT, @Row INT, @ItemId INT, @ItemCode VARCHAR(20), @ItemName VARCHAR(100), @UnitQty Numeric(12,4), @UOMId INT, @ItemUnitPrice Money, @ItemTotalAmount Money, @BatchNo VARCHAR(20), @ManufactureBatchNo VARCHAR(20)
			Declare @SeqNo VARCHAR(200)
			Set @iHnd = 0


		Exec sp_xml_preparedocument @iHnd OutPut, @inputParam

	DECLARE @MyHierarchy JSONHierarchy ,@XMLItems xml
           INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)


                            select  b.value('@ItemId', 'int') as ItemId,  
							  b.value('@Items', 'varchar(20)') as ItemCode,
							  b.value('@ItemName', 'varchar(100)') as ItemDescription,
							  b.value('@BatchNo', 'varchar(20)') as BatchNo,
							  b.value('@UnitPrice', 'Numeric(12,4)') as TransferPrice,
							  b.value('@TotalAmount', 'Money') as TotalAmount,
							  b.value('@BucketId', 'int') as BucketId,
							  b.value('@TOINo','varchar(50)') as TOINumber,
							  b.value('@RequestQty','float') as RequestQty,
							  b.value('@Weight','float') as Weight,
							  b.value('@MfgBatchNo','varchar(50)') as ManufactureBatchNo,
							  b.value('@ClaimQty','float') as ClaimQty,
							  b.value('@MRP','Numeric(12,4)') as MRP,
							  b.value('@MfgDate','varchar(50)') as MfgDate,
							  b.value('@ExpDate','varchar(50)') as ExpDate,
							  b.value('@Weight','float') as GrossWeightItem,
							  b.value('@UOMId','int') as UOMId
								into #TI_DetailMaster 
							  FROM @XMLItems.nodes('/root/item') a(b)
							  
							  alter table  #TI_DetailMaster   add  RowNo Int Identity(1,1)  ;
							
                        


	
		--Select * From #TIDetail


		SELECT  @TONumber			= TONumber,			@SourceLocationId	= SourceLocationId,		 
				@ReceivedDate		= GETDATE(),	@DestinationLocationId=	DestinationLocationId,			
				@TotalTOQuantity	= TotalTOQuantity,	@TotalTOAmount		= TotalTOAmount,		 		
				@CreatedDate		= CreatedDate,		@ModifiedBy			= ModifiedBy,			@ModifiedDate			= ModifiedDate,
				@TOINumber			= TOINumber,			@PackSize			= PackSize,				@GrossWeight			= GrossWeight,
				@ShippingBillNo		= ShippingWayBillNo,	@ReceivedTime		= GETDATE(),			@ShippingDetails	= ShippingDetails,
				@Exportvalue = Isexported	
		FROM
			 TO_Header where TONumber=@TONumber

 select @GrossWeight=SUM(TID.Weight*TID.ClaimQty) ,@TotalTOIQuantity=SUM(TID.ClaimQty) 
 ,@TotalTOIAmount =SUM(TID.MRP*TID.ClaimQty) from #TI_DetailMaster TID

--		SELECT @ReceivedDate, @ReceivedTime
		IF Convert(VARCHAR(10),@ReceivedDate,120)='1900-01-01'
			SET @ReceivedDate = NULL
			
		SET @ReceivedTime = GetDate()
--		IF Convert(VARCHAR(10),@ReceivedTime,120)='1900-01-01'
--			SET @ReceivedTime = NULL
--		SELECT @ReceivedDate, @ReceivedTime


		--SELECT @outParam =[dbo].[ufn_GetStockCountStatus](@DestinationLocationId)
		--IF @outParam<>''
		--	RETURN

		DECLARE @LocationCode AS VARCHAR(20)
		SELECT @LocationCode = LocationCode From Location_Master Where LocationType = @DestinationLocationId

		IF @TNumber=''
		BEGIN
			SELECT @TOINumber = TOINumber, @ShippingDate = ShippingDate FROM TO_Header WHERE TONumber = @TONumber

			EXECUTE usp_GetSeqNo 'C', @LocationId, @SeqNo Out
			SET @TNumber=@SeqNo

			INSERT INTO CSHeader	(CSNumber,	TONumber,	TOINumber,	TOShippingDate, SourceLocationID,	
			DestinationLocationID,	ShippingDetails,	VehicleNo,			ReceivedBy , 	ReceivedDate,	ReceivedTime,
				TotalTOQuantity,	TotalTOAmount,	TotalTIQuantity,	TotalTIAmount,	PackSize,	GrossWeight,	
				Remarks,	Status,	CreatedBy,		CreatedDate,	ModifiedBy,	ModifiedDate,Isexported)
				VALUES				(@TNumber,	@TONumber,	@TOINumber, @ShippingDate,	@SourceLocationId,
					@DestinationLocationId, @ShippingDetails,	@ShippingBillNo,	@createdBy,	@ReceivedDate,	
					@ReceivedTime,			@TotalTOQuantity,	@TotalTOAmount,	@TotalTOIQuantity,	@TotalTOIAmount, 
					@PackSize,	@GrossWeight,	@Remarks,	@StatusId,		@ModifiedBy,	@SystemDate,
						@ModifiedBy,@SystemDate, @Exportvalue)

		--	Exec [usp_Interface_Audit] '', 'TRNIN', @LocationCode, 'TIHeader', @TNumber, NULL, NULL, NULL, NULL, 'I', @ModifiedBy	,  @RecCnt	OUTPUT	
		END
		ELSE
		BEGIN
			---- Check Concurrency, Get DBDate
			--Declare @DBDate DateTime
			--Select @DBDate = (Select ModifiedDate = (Case When ModifiedBy Is Not Null Then ModifiedDate Else CreatedDate End) From TO_Header Where  TONumber = @TNumber)
			
			--If @ModifiedDate <> Convert(VARCHAR(20),@DBDate,120)
			--BEGIN
			--	SET @outParam = 'INF0022'
			--	Return
			--END

			-- Update In TO_Header
			UPDATE CSHeader
			SET VehicleNo			=	@ShippingBillNo,		ShippingDetails		=	@ShippingDetails,
				GrossWeight			=	@GrossWeight,			PackSize			=	@PackSize,
				TotalTOQuantity		=	@TotalTOQuantity,		TotalTOAmount		=	@TotalTOAmount,
				ModifiedBy			=	@ModifiedBy,			ModifiedDate		=	@SystemDate	,
				Status				=	@StatusId, 
				ReceivedDate		=   CASE WHEN @StatusId = 2 THEN @ReceivedDate ELSE NULL END,
				ReceivedTime		=   CASE WHEN @StatusId = 2 THEN GetDate() ELSE NULL END,
				Remarks				=	@Remarks
			WHERE CSNumber = @TNumber

			--If @StatusId = 2 
			--BEGIN
			--	Exec [usp_Interface_Audit] '', 'TRNIN', @LocationCode, 'TIHeader', @TNumber, NULL, NULL, NULL, NULL, 'U', @ModifiedBy	,  @RecCnt	OUTPUT	
			--END

		END

		-- If Confirm, Update the Quantity in InventoryLocation_Master and Inventory_LocBucketBatch Tabled
		IF @StatusId = 1
		BEGIN
	
				 DECLARE @retItemId int ,
					  @retQuantity numeric(18,2),@retBatchNo varchar(20),@retManufactureBatchNo varchar(20),@retBucketId int

					DECLARE retCus CURSOR FOR  select ItemId,ClaimQty,BatchNo,ManufactureBatchNo,BucketId from #TI_DetailMaster 
							
								OPEN retCus
					FETCH NEXT FROM retCus
						INTO @retItemId,@retQuantity,@retBatchNo,@retManufactureBatchNo,@retBucketId

					WHILE @@FETCH_STATUS = 0
						
						BEGIN 
										  
							UPDATE Inventory_LocBucketBatch SET Quantity = Quantity + @retQuantity
							FROM Inventory_LocBucketBatch 
							WHERE ItemId=@retItemId AND LocationId=@locationId AND BatchNo=@retBatchNo
							AND BucketId=@retBucketId
							 IF @@ROWCOUNT=0
							  BEGIN
							     INSERT INTO Inventory_LocBucketBatch (BatchNo,BucketId,CreatedBy,
							     CreatedDate,DocumentDate,ItemId,
									ItemName,LocationId,Quantity,ReasonId,Status) SELECT 
									@retBatchNo,@retBucketId,@createdBy,GETDATE(),GETDATE(),@retItemId,ItemName
									,@locationId,@retQuantity,0,1
									 FROM Item_Master where ItemId=@retItemId
							  END
						
						
						FETCH NEXT FROM retCus
						INTO @retItemId,@retQuantity,@retBatchNo,@retManufactureBatchNo,@retBucketId

						END
						CLOSE retCus
						DEALLOCATE retCus

--				If @StatusId = 2 
--					BEGIN
--						SELECT @LocationCode = LocationCode From Location_Master Where LocationType = @SourceLocationId
--						Exec [usp_Interface_Audit] '', 'TRNOUT', @LocationCode, 'TO_Header', @TONumber, NULL, NULL, NULL, NULL, 'U', @ModifiedBy,  @RecCnt	OUTPUT		
--					END

				--GROUP BY ItemId, AfterAdjustQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId, ItemDescription
					DELETE FROM CSDetail WHERE CSNumber = @TNumber
			DELETE FROM CSBatchDetail WHERE CSNumber = @TNumber


			INSERT INTO CSDetail (	CSNumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOMId, TransferPrice, TotalAmount, --IndentNo, 
									CreatedBy, CreatedDate, BucketId)
			SELECT @tnumber,ROW_NUMBER() OVER (Order By ItemId) as RowNo,ItemId, ItemCode, ItemDescription, SUM(ClaimQty) AS AfterAdjustQty, UOMId, TransferPrice, SUM(TotalAmount) As TotalAmount, --IndentNo, 
											@createdBy,GETDATE(),BucketId
			FROM #TI_DetailMaster GROUP BY ItemId, ItemCode, ItemDescription, UOMId, TransferPrice, BucketId

			INSERT INTO CSBatchDetail (CSNumber, RowNo, ItemId, Quantity, BatchNo,  MfgDate, ExpDate, ManufactureBatchNo, MRP, RcvSubBucketId, CreatedBy, CreatedDate)
			SELECT @TNumber, ROW_NUMBER() OVER (Order By ItemId) as RowNo, ItemId, ClaimQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId, @ModifiedBy, @SystemDate
			FROM #TI_DetailMaster 
			GROUP BY ItemId, RowNo, ClaimQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId
	
		END

		


	SELECT CSNumber CSNumber,
			Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate, 
			1 as IndexSeqNo
	FROM CSHeader 
	WHERE CSNumber = @TNumber


	END TRY
	BEGIN CATCH
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		  Select @outParam OutParam
		RETURN ERROR_NUMBER()
	END CATCH
END

--------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[SP_CancelOrder]    Script Date: 02/09/2014 13:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_CancelOrder]
(
@orderId varchar(30),
@statusParam int ,
@distributorId int,
@SourceLocationId int,
@createdBy int,
@outParam varchar(500) OUTPUT
)

AS
BEGIN

 SET NOCOUNT ON; -- set this flag for PDO .don't remove it
 BEGIN TRY 
   DECLARE  @tempStatus int,@tempOrderType int
   DECLARE @ChequeNo VARCHAR(30),@reccnt Varchar(100)
		
   
									SELECT  @tempStatus = Status,
                                            @tempOrderType = OrderType
                                    FROM    CoHeader
                                    WHERE   CustomerOrderNo = @orderid 
									
    -- Insert statements for procedure here
      
                                            
                             IF @tempStatus = 3
                                       BEGIN
										   UPDATE  COHeader
												SET     Status = @statusParam,
														ModifiedDate = GETDATE(),
														ModifiedBy = @createdBy
												WHERE   CustomerOrderNo = @orderId                                
									
									
														UPDATE GDL    SET AvailedDate = NULL 
														,Availed =0 ,ModifiedBy = @createdBy,
														ModifiedDate = GETDATE()
														FROM GiftVoucher_Distributor_Link GDL
														JOIN CODetail c ON GDL.GiftVoucherCode = c.GiftVoucherCode
														AND c.VoucherSrNo = GDL.VoucherSrNo 
														AND c.CustomerOrderNo = @orderId
												
														DECLARE @GiftVoucherNum AS VARCHAR(200)
														DECLARE @VoucherSrNo as varchar(40)
														DECLARE GiftVoucherCursor CURSOR FORWARD_ONLY STATIC
															FOR SELECT  GDL.GiftVoucherCode,
																		GDL.VoucherSrNo
																FROM    GiftVoucher_Distributor_Link GDL JOIN CODetail c 
																ON GDL.GiftVoucherCode = c.GiftVoucherCode AND c.VoucherSrNo = GDL.VoucherSrNo 
																AND c.CustomerOrderNo = @orderId
																WHERE   ISNULL(GDL.VoucherSrNo, '') <> ''

														OPEN GiftVoucherCursor
														FETCH NEXT FROM GiftVoucherCursor INTO @GiftVoucherNum,
															@VoucherSrNo
														WHILE @@FETCH_STATUS = 0
															BEGIN
																EXEC usp_interface_audit '', 'TRGFTVCHR', '', 'Gift Voucher Distributor Link', @GiftVoucherNum, @VoucherSrNo, NULL,	NULL, NULL, 'U', @createdBy, @reccnt OUTPUT    	
																
																FETCH NEXT FROM GiftVoucherCursor INTO @GiftVoucherNum, @VoucherSrNo
															END
															
														CLOSE GiftVoucherCursor 
														DEALLOCATE GiftVoucherCursor 
                            
							
										-- - ----------------------------
										-- If Unused --------------------							
									SELECT    ChequeNo
										INTO #tempTable2
										FROM      DistributorBonus_ChequeLink 
										WHERE OrderNo = @orderId
			                                   
			                            
									   -- DECLARE @PaymentAmount AS VARCHAR(200)
															
														DECLARE DistributorBonusCurForCancel CURSOR FORWARD_ONLY STATIC
															FOR SELECT  ChequeNo
							                                           
																FROM    #tempTable2 lm
																WHERE   ISNULL(ChequeNo, '') <> ''

														OPEN DistributorBonusCurForCancel
														FETCH NEXT FROM DistributorBonusCurForCancel INTO @ChequeNo
							                               
														WHILE @@FETCH_STATUS = 0
															BEGIN
																EXEC usp_interface_audit '', 'DISBONUSCHEQUE',
																	'', 'DistributorBonus_ChequeLink',
																	@ChequeNo, NULL, NULL,
																	NULL, NULL, 'U', @createdBy, @reccnt OUTPUT    	
																
							--									EXEC usp_interface_audit '','GFTVCHR','','Gift Voucher',@GiftVoucherNumber,NULL,NULL,NULL,NULL,'U',@user,@reccnt OUTPUT    	
																
																FETCH NEXT FROM DistributorBonusCurForCancel INTO @ChequeNo
							                                       
															END
															Deallocate DistributorBonusCurForCancel ;
														DROP TABLE #tempTable2
                                    --   PUC ACCOUNT ROLLBACK
                             --          DECLARE @tempAmt Numeric(18,4)
                             --          SET @tempAmt=0;
                             --        SELECT @tempAmt=TransAmount from PUCTransactionDetail where OrderNo=@orderId
                             --      IF(@tempAmt!=0 )
                             --      BEGIN
                             --         --UPDATE PUCAccount set AvailableAmount
                             --         -- =AvailableAmount+@tempAmt
                             --         --Delete from PUCTransactionDetail where OrderNo=@orderId
                             --          INSERT INTO PUCTransactionDetail(OrderNo,PCId,TransAmount,TransDate) select OrderNo,PCId,-TransAmount,GETDATE()  from PUCTransactionDetail where OrderNo=@orderId
		                           --END   
									-- bonus check rollback						
									
                                                    UPDATE  dbo.DistributorBonus_ChequeLink
                                                    SET     Status = 1,
                                                            UsedAmount = 0,
                                                            BalanceAmount = Amount,
                                                            CanBeUsedAgain = 1,
                                                            OrderNo = ''
                                                    WHERE   OrderNo = @orderId
									
									
                                                    DECLARE @tempBonusChqPayment MONEY,
                                                        @tempChequeNo VARCHAR(20)
									
                                                    SELECT  @tempBonusChqPayment = cop.PaymentAmount,
                                                            @tempChequeNo = cop.Reference
                                                    FROM    COPayment cop
                                                    WHERE   cop.CustomerOrderNo = @orderId
                                                            AND cop.TenderType = 5
									
                                                    UPDATE  dbcl
                                                    SET     Status = CASE WHEN BalanceAmount + @tempBonusChqPayment = Amount THEN 1
                                                                          ELSE 2
                                                                     END,
                                                            UsedAmount = usedAmount
                                                            - @tempBonusChqPayment,
                                                            BalanceAmount = BalanceAmount
                                                            + @tempBonusChqPayment,
                                                            CanBeUsedAgain = 1,
                                                            dbcl.TeamOrderNo = ''
                                                    FROM    dbo.DistributorBonus_ChequeLink dbcl
                                                            INNER JOIN COHeader coh ON dbcl.TeamOrderNo = coh.LogNo
                                                                                       AND coh.CustomerOrderNo = @orderId
                                                                                       AND dbcl.ChequeNo = @tempChequeNo
										
									
									-- first order rollback	
									
                                                    IF @tempOrderType = 1 
                                                        BEGIN
                                                            UPDATE  dbo.DistributorMaster
                                                            SET     FirstOrderTaken = 0
                                                            WHERE   DistributorId = @distributorId
                                                        END
									
									-- Interface Audit Entry   
                                                    IF @tempStatus = 3
                                                        EXEC usp_interface_audit '',
                                                            'CUSTORD', '',
                                                            'COHeader',
                                                            @orderId, NULL,
                                                            NULL, NULL, NULL,
                                                            'U', @createdBy,
                                                            @reccnt OUTPUT 
       IF(@statusParam<>2)  --FOR MODIFIED CASE
       BEGIN
         
          DELETE FROM COPayment where CustomerOrderNo=@orderId
          DELETE FROM CODetailDiscount where CustomerOrderNo=@orderId
          DELETE FROM CODetailTax where CustomerOrderNo=@orderId
            DELETE FROM CODetail where CustomerOrderNo=@orderId
            DELETE FROM COHeader where CustomerOrderNo=@orderId ;
       END	
								
                                                END
--                                EXEC usp_interface_audit '','CUSTORD','','COHeader',@orderId,NULL,NULL,NULL,NULL,'U',@user,@reccnt OUTPUT
--                                PRINT 'INTERFACE AUDIT UPDTAE DONE on status 2'
                              ELSE 
                                BEGIN
                                    if(@statusParam=2 and @tempStatus!=3)
                                        BEGIN
                                          SET  @outParam= 'NOCAN'   --return for when order is not in confirm state.we can not cancelle order
                                           SELECT @outParam OutParam
                                            Return
                                        END
                                    else if (@tempStatus!=3)
                                        BEGIN
                                            SET @outParam= 'NOMOD'
                                               
                                             RETURN
                                        END
                                END    
		if(@statusParam=2)    --for  cancel order 
			 BEGIN
					DECLARE @logNo varchar(30),@LogOrderAmount INT
					SELECT TOP 1 @logNo = LOGNo FROM COHeader where CustomerOrderNo = @orderId;
					IF @logNo = '' OR @logNo = null
					BEGIN
						SET @LogOrderAmount = 0;
					END
					ELSE
					BEGIN
						--Select @LogOrderAmount = SUM(COH.PaymentAmount) FROM COHeader COH  
						--where COH.LogNo=@LogNo AND COH.Status = 3 group by COH.LogNo
						select @LogOrderAmount = sum(COP.PaymentAmount) from COPayment COP where cop.CustomerOrderNo IN
						(select COH.customerOrderNo from coheader COH where COH.LogNo = @LogNo
						 AND COH.Status = 3) AND
						COP.TenderType = 6
					END
						Select '' as InvoiceNo, CustomerOrderNo, UserName as CreatedBy,CO.Date 'CreatedDate', case CO.Status when 2 then 'Cancelled' else '' end as
						   Status,@logNo LogNo,@LogOrderAmount LogOrderAmount,2 StatusId  from COHeader CO left join  User_Master UM on um.UserId=co.ModifiedBy where CustomerOrderNo=@orderId; 
				END
	
	END TRY
	  BEGIN CATCH
       
            SET @outParam = '30001:' + CAST(ERROR_NUMBER() AS VARCHAR(50))
                + ', ' + CAST(ERROR_MESSAGE() AS VARCHAR(500))
                + 'Error Line: ' + CAST(ERROR_LINE() AS VARCHAR(50)) --+ ', Source: ' + CAST (ERROR_PROCEDURE() as VARCHAR(50))
           select @outParam OutParam 
           
        END CATCH 
               
END
GO
/****** Object:  StoredProcedure [dbo].[spGetBranchAddressProd]    Script Date: 02/09/2014 13:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spGetBranchAddressProd]      
(      
@LocationType int,      
@stateid int,      
@IsMiniBranch int      
)      
as      
begin      
if(@LocationType=3)--DLCP/Branch      
begin      
 select lm.LocationCode, [Name] PName, FirstName+' ' +LastName as [Name],(lm.Phone1+','+ lm.Mobile1) Contact_No,                       
 lm.Address1 + ',' + lm.Address2 + ',' + lm.Address3 + ',' + CityName + ','+ StateName + ','+lm.Pincode as Address                  
 from Location_Master lm  
 inner join LocationContact lc
 on lm.LocationId = lc.LocationId     
 left join City_Master cm                      
 on lm.CityId =cm.CityId                       
 left join state_master sm                      
 on lm.StateId = sm.StateId                       
 left join DistributorMaster dm                      
 on lm.DistributorId= dm.DistributorId                      
 where LocationType= @LocationType  and lm.status = 1 and lm.stateid=@stateid and lm.IsMiniBranch=@IsMiniBranch order by PName       
 end      
else if(@LocationType=2)--DCC      
begin      
select lm.LocationCode, [Name] PName, FirstName+' ' +LastName as [Name],(lm.Phone1+','+ lm.Mobile1) Contact_No,                       
 lm.Address1 + ',' + lm.Address2 + ',' + lm.Address3 + ',' + CityName + ','+ StateName + ','+lm.Pincode as Address                  
 from Location_Master lm  
 inner join LocationContact lc
 on lm.LocationId = lc.LocationId      
left join City_Master cm                      
on lm.CityId =cm.CityId                       
left join state_master sm                      
on lm.StateId = sm.StateId                       
left join DistributorMaster dm                      
on lm.DistributorId= dm.DistributorId                      
where LocationType= @LocationType  and lm.status = 1 and lm.stateid=@stateid order by PName       
end      
      
end
GO
/****** Object:  StoredProcedure [dbo].[spGetBranchAddress]    Script Date: 02/09/2014 13:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spGetBranchAddress]      
(      
@LocationType int,      
@stateid int,      
@IsMiniBranch int      
)      
as      
begin      
	if(@LocationType=3)--DLCP/Branch      
	begin      
	 select lm.LocationCode, [Name] PName, FirstName+' ' +LastName as [Name],(lm.Phone1+','+ lm.Mobile1) Contact_No,                         
	 lm.Address1 + ',' + lm.Address2 + ',' + lm.Address3 + ',' + CityName + ','+ StateName + ','+lm.Pincode as Address                    
	 from Location_Master lm    
	 inner join LocationContact lc  
	 on lm.LocationId = lc.LocationId       
	 left join City_Master cm                        
	 on lm.CityId =cm.CityId                         
	 left join state_master sm                        
	 on lm.StateId = sm.StateId                         
	 left join DistributorMaster dm                        
	 on lm.DistributorId= dm.DistributorId                        
	 where LocationType= @LocationType  and lm.status = 1 and lm.stateid=@stateid and lm.IsMiniBranch=@IsMiniBranch order by PName         
	end      
else if(@LocationType=4)--DCC      
	begin      
	select LocationCode Id, [Name] PName, DistributorFirstName+' '  
	+DistributorLastName as [Name],case phone1 when '0' then mobile1 when '' then mobile1 when null then Mobile1 else (Phone1+','+ Mobile1) end Contact_No,  
	Address1 + ',' + Address2 + ',' + Address3 + ',' + CityName + ','+ StateName  
	+ ','+Pincode as Address                  
	from Location_Master lm       
	left join City_Master cm                      
	on lm.CityId =cm.CityId                       
	left join state_master sm                      
	on lm.StateId = sm.StateId                       
	left join DistributorMaster dm                      
	on lm.DistributorId= dm.DistributorId                      
	where LocationType= @LocationType  and lm.status = 1 and lm.stateid=@stateid  
	order by PName       
	end      
end
GO
/****** Object:  StoredProcedure [dbo].[sp_UnpackSave]    Script Date: 02/09/2014 13:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Procedure [dbo].[sp_UnpackSave]
		@compositeItemId int,
		@Quantity Numeric(18,2),
		@CreatedBy int	,
		@locationId int,
		@BucketId int,
		@Remarks varchar(100),
				@outParam	Varchar(50) Output
AS
Begin
SET NOCOUNT ON
	Begin Try

	Declare @iHnd int,@TotalBuketQty Numeric(18,2),@RecCnt varchar(500),@diff Numeric(18,2),
	@effectedComBatch varchar(100)
	 SET @effectedComBatch ='' ;
	           	--Check Unpack Quantity in available or not		
			Select @TotalBuketQty=isNull(sum(Quantity),0)    from Inventory_LocBucketBatch where
			 ItemId=@compositeItemId And BucketId=@BucketId And LocationId=@locationId
					
							 
								 if(@Quantity>@TotalBuketQty)
								Begin
								        select 'VAL0058' as OutParam
										SET @outParam = 'VAL0058'
										ROLLBACK
										Return
								End
					
      -- @TCompositeItem table holds batch details of composite items from which deduct quantity according 
      --  to least exp dste first
      
     Begin Transaction
		 Declare @ComItemId int,@ComItemName varchar(20),@COMQuantity numeric(18,2),@COMMfgDate DateTime,
		 @COMExpDate DateTime,@COMBatchNo varchar(20),@COMManufactureBatchNo varchar(20)                         
       Declare @TCompositeItem Table   
		(
			ItemId Int,
			ItemName Varchar(100),
			Quantity Float,
			MfgDate DateTime,
			ExpDate DateTime,
			LocationId int,
			BatchNo varchar(20),
			mfgNo varchar(20)
		)
						
		DECLARE @RetSeqNo Varchar(20)
		DECLARE @LocationCode VARCHAR(20)
		DECLARE @SeqNo Varchar(20)
				
		EXECUTE usp_GetSeqNo 'PU', @LocationId, @RetSeqNo Out

		SELECT @LocationCode = LocationCode FROM LOCATION_MASTER WHERE LocationID = @LocationID

		SET @SeqNo=@LocationCode + '/' + 'U' + '/' + @RetSeqNo

		-- select items which we want to unpack
						   Declare CompositeBatchDetail  cursor For  select ILLB.ItemId,ILLB.ItemName,
						    ILLB.Quantity,IBD.MfgDate,IBD.ExpDate ,ILLB.LocationId,ILLB.BatchNo,IBD.ManufactureBatchNo
						   from ItemBatch_Detail IBD
						  LEFT JOIN 
						Inventory_LocBucketBatch ILLB   ON
						  IBD.BatchNo=ILLB.BatchNo
						  where ILLB.ItemId=@compositeItemId AND ILLB.LocationId=@locationId AND ILLB.BucketId=5 AND Quantity>0 
						   Order By IBD.MfgDate DESC  
		
		 OPEN CompositeBatchDetail  
     FETCH NEXT FROM CompositeBatchDetail  
      INTO @ComItemId,@ComItemName,@COMQuantity,@COMMfgDate,@COMExpDate,@locationId,@COMBatchNo,@COMManufactureBatchNo
     Declare @tempQuantity numeric(18,2),@diffQuantity numeric(18,2);
     set @tempQuantity=0;
    
     WHILE @@FETCH_STATUS = 0 
     
       BEGIN
        set  @tempQuantity=@tempQuantity+@COMQuantity;
       set  @diffQuantity=@tempQuantity-@Quantity ;
           if @diffQuantity <0     -- when quantity is not greater Total quainty Then all Quantity if that batch should be zero
				   BEGIN
				    insert into @TCompositeItem select @ComItemId,@ComItemName,@COMQuantity,@COMMfgDate,
				    @COMExpDate,@locationId,@COMBatchNo,@COMManufactureBatchNo   
				    --update Inventory_LocBucketBatch
				    --   set Quantity =Quantity-@COMQuantity where ItemId=@compositeItemId 
				    --   AND BatchNo=@COMBatchNo AND LocationId=@locationId AND BucketId=5
				    --   set @effectedComBatch=  @effectedComBatch +@COMBatchNo+','
				     set  @COMQuantity=-@COMQuantity ;
				        EXEC sp_updateInventoryInStockLedger 12,@SeqNo,@locationId,@compositeItemId,@COMBatchNo,@COMQuantity,5,
                           0 ,0,@outParam OUT
				       
				   END
           ELSE
					BEGIN
                     insert into @TCompositeItem select @ComItemId,@ComItemName
                     ,@COMQuantity-(@tempQuantity-@Quantity),@COMMfgDate,@COMExpDate,@locationId,@COMBatchNo,@COMManufactureBatchNo
					 --update Inventory_LocBucketBatch
				  --     set Quantity =Quantity-(@COMQuantity-(@tempQuantity-@Quantity)) where ItemId=@compositeItemId 
				  --     AND BatchNo=@COMBatchNo AND LocationId=@locationId
				       set @COMQuantity=-(@COMQuantity-(@tempQuantity-@Quantity)) ;
				       EXEC sp_updateInventoryInStockLedger 12,@SeqNo,@locationId,@compositeItemId,@COMBatchNo,@COMQuantity,5,
                           0 ,0,@outParam OUT
				    set @tempQuantity=0.00;
                    SET  @diffQuantity=0.00;
                    set @effectedComBatch=@effectedComBatch +@COMBatchNo
                    break ;
					END
		 FETCH NEXT FROM CompositeBatchDetail  
      INTO @ComItemId,@ComItemName,@COMQuantity,@COMMfgDate,@COMExpDate,@locationId,@COMBatchNo,@COMManufactureBatchNo	
		  
        END 
			CLOSE CompositeBatchDetail
			DEALLOCATE CompositeBatchDetail	
     SET @tempQuantity=0.00;
	 SET  @diffQuantity=0.00;
				
				--select * from @TCompositeItem		    
		
------------------------------------------------------------------------------------------------------------------------------------------------------		
		--@TCompositeBOM hold Quaintity ,ItemId of consituent elements which we shuld increase qty.
		DECLARE @TCBItemId int,@TCBTotalQty numeric(18,4)					
		Declare  @TCompositeBOM Table
		( 
		     RowNo int identity(1,1),
			ItemId Int,
			TotalQty numeric(18,4)
			
		)
 
						insert into @TCompositeBOM select  CBOM.ItemId,
						   CBOM.Quantity * @Quantity from
						 CompositeBOM CBOM where CompositeItemID=@CompositeItemId
		   -- select  * from @TCompositeBOM		
			--------------------------------------------------------------------------------------------------------------------------------
		
		 
		 --@TEMPPUTBatchDetails hold batch detail for consitutent element on bases of PU number which come on bases of manufacture no 
		 -- of Composite item 
		      DECLARE @TPBDBatchNo Varchar(20),
					@TPBDQuantity Numeric(18,2) ,
					@TPBDItemId Int,
					@TPBDExpDate varchar(20)
		      Declare  @TEMPPUTBatchDetails Table
				(
				   
					BatchNo Varchar(20),
					Quaintity Numeric(18,2) ,
					ItemId Int,
					ExpDate varchar(20)
				)
				
				
			INSERT INTO @TEMPPUTBatchDetails SELECT PUBD.BatchNo,PUBD.Quantity,PUBD.ItemId,IBD.ExpDate FROM   PU_Header PUH LEFT JOIN 
		PU_BatchDetail PUBD ON PUH.PUNo = PUBD.PUNo
		LEFT JOIN  ItemBatch_Detail IBD
		ON IBD.BatchNo=PUBD.BatchNo
		 WHERE PUH.ManufactureBatchNo in (SELECT TCI.mfgNo FROM @TCompositeItem  TCI) AND IBD.ExpDate>getdate() ORDER BY PUBD.ItemId,IBD.ExpDate
		 	    
		   if  not exists (select * from @TEMPPUTBatchDetails)  --corrosponding trasation not avail
		          BEGIN
                     SELECT 'VAL0060' as OutParam ;
                     rollback  transaction ;
					 return;
                    END
		   
		   
		   
		------------------------------------------------------------------------------------------------
				Declare  @PUTBatchDetails Table
				(
				    RowNo int identity(1,1),
					BatchNo Varchar(20),
					Quaintity Numeric(18,2) ,
					ItemId Int,
					ExpDate varchar(20)
				)
				
			DECLARE @PUNo varchar(20),@PUQuantity float,@ExpDate varchar(20)
			
			 DECLARE TCompositeBOMDetail CURSOR FOR SELECT ItemId,TotalQty from @TCompositeBOM 
       
		  
		   OPEN TCompositeBOMDetail  
     FETCH NEXT FROM TCompositeBOMDetail  
      INTO @TCBItemId,@TCBTotalQty
      
        WHILE @@FETCH_STATUS = 0 
        BEGIN
                  
                    DECLARE TPBDDetail CURSOR FOR SELECT * from @TEMPPUTBatchDetails TPBD where TPBD.ItemId=@TCBItemId
						   OPEN TPBDDetail  
					 FETCH NEXT FROM TPBDDetail  
					  INTO @TPBDBatchNo,@TPBDQuantity,@TPBDItemId,@TPBDExpDate
				               WHILE @@FETCH_STATUS = 0 
								   BEGIN  
							                                     
								  set  @tempQuantity=@tempQuantity+@TPBDQuantity;
								   set  @diffQuantity=@tempQuantity-@TCBTotalQty ;
									   if @diffQuantity <0
											   BEGIN
												insert into @PUTBatchDetails select @TPBDBatchNo,@TPBDQuantity,@TPBDItemId,@TPBDExpDate
												  
											
											   END
									   ELSE
												BEGIN
												 insert into @PUTBatchDetails select @TPBDBatchNo,
												 @TPBDQuantity-(@tempQuantity-@TCBTotalQty),@TPBDItemId,@TPBDExpDate
												
											set @tempQuantity=0.00;
										   SET  @diffQuantity=0.00;
											       break;
												END
               
               
									
									FETCH NEXT FROM TPBDDetail  
							  INTO @TPBDBatchNo,@TPBDQuantity,@TPBDItemId,@TPBDExpDate
							  END
								 CLOSE TPBDDetail
								DEALLOCATE TPBDDetail	
	  
           FETCH NEXT FROM TCompositeBOMDetail  
      INTO @TCBItemId,@TCBTotalQty   
        END
        
             CLOSE TCompositeBOMDetail
			DEALLOCATE TCompositeBOMDetail

			DECLARE puDetail CURSOR FOR SELECT ItemId,SUM(Quaintity) from @PUTBatchDetails group by ItemId 
		  
		   OPEN puDetail  
     FETCH NEXT FROM puDetail  
      INTO @TCBItemId,@TCBTotalQty
      
        WHILE @@FETCH_STATUS = 0
       
                 BEGIN 
                  select @diff=TotalQty-@TCBTotalQty  from @TCompositeBOM BOM where BOM.ItemId=@TCBItemId
                 if @diff!=0.00
                    BEGIN
                     SELECT 'VAL0059' as OutParam ;
                     rollback  transaction ;
					 return;
                    END
                 
			
				 FETCH NEXT FROM puDetail  
				  INTO @TCBItemId,@TCBTotalQty
			      
                 END
		 CLOSE puDetail
			DEALLOCATE puDetail	
  
       
				
					--UPDATE Inventory_LocBucketBatch 
					--SET Quantity =ILB.Quantity + BD.Quaintity
					--FROM Inventory_LocBucketBatch ILB 
					--	INNER JOIN @PUTBatchDetails BD ON
					--		BD.ItemId = ILB.ItemId AND 
					--		BD.BatchNo = ILB.BatchNo AND 
					--		ILB.LocationId = @LocationId
					--   INNER Join Bucket buc ON
					--		ILB.BucketId = buc.BucketId AND 
					--		buc.Status = 1 AND 
					--		buc.ParentId Is Not Null AND
					--		Sellable=1
				 --DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity2 Numeric(18,4)
            
          
             DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity2 Numeric(18,4)
            
           DECLARE INVLOCBUCKBATCH CURSOR FOR   SELECT BD.BatchNo,5,BD.ItemId,BD.Quaintity FROM @PUTBatchDetails BD  	  	
                       
                        OPEN INVLOCBUCKBATCH  
            FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity2  
                                 
                            WHILE @@FETCH_STATUS = 0  
                              BEGIN  
                            
                            EXEC sp_updateInventoryInStockLedger 11,@SeqNo,@locationId,@tempItemID,@tempBatchNo,@tempQuantity2,@tempBucketId,  
                            0.00 ,0,@outParam OUT  
                              
                            if @outParam!=''  
                                   BEGIN  
                                   SELECT @outParam OutParam  
                                     CLOSE INVLOCBUCKBATCH  
                       
                                     DEAllOCATE INVLOCBUCKBATCH  
                                   ROLLBACK  
                                   RETURN  
                                   END  
                            FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity2  
                              END  
                                
                              CLOSE INVLOCBUCKBATCH  
                                
                              DEAllOCATE INVLOCBUCKBATCH  
			
							


		
		INSERT INTO PU_Header
			(PUNo,PUDate,CompositeItemId,Quantity,
			 BucketId,BatchNo,PU_Flag,Remarks,LocationId,
			 Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select @SeqNo,GETDATE(),@compositeItemId,@Quantity,
			   @BucketId,'',0,@Remarks,@locationId,
			   1,@CreatedBy,GETDATE(),@CreatedBy,GETDATE()
	
			
		INSERT INTO PU_Detail
			(PUNo,ItemId,SeqNo,Quantity,
			 Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select @SeqNo,ItemId,RowNo,TotalQty,
			   1,@CreatedBy,GETDATE(),@CreatedBy,GETDATE()
		FROM @TCompositeBOM

		Insert Into PU_BatchDetail
			(PUNo,RowNo,ItemId,BucketId,BatchNo,
			 Quantity,Status,CreatedBy,CreatedDate)
		Select @SeqNo,RowNo,ItemId,@BucketId,BatchNo,
			   Quaintity,1,@CreatedBy,GETDATE()
		FROM @PUTBatchDetails 
          select @SeqNo PUNo ;
         
		  Commit Transaction    
	--- End Execution -----------------------------------------------------------------------------		
	End Try

	Begin Catch
		If @iHnd <> 0
			Execute sp_xml_removedocument @iHnd
		Set @outParam = '30001: ' + ' - ' +
						'Error Number: ' + Cast(Error_Number() As Varchar(50)) + ', ' +
						'Error Message: ' + Error_Message() + ', ' +
						'Error Line: ' + Cast(Error_Line() As Varchar(50)) + ', ' +
						'Source: ' + Error_Procedure()
		ROLLBACK
		Return Error_Number()
	End Catch
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetDownline_Live_New_BackUP_11FEB]    Script Date: 02/09/2014 13:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Alter by:Manikant Thakur
--Alter Date:6 -Nov-2012
CREATE  PROCEDURE [dbo].[Sp_GetDownline_Live_New_BackUP_11FEB]         
@distributorid         
varchar(8)         
AS        
BEGIN        
--Old One         
--select row_number() over(order by a.DistributorId asc) as 'SNo.',dbo.fnGetDistributorName(@distributorid) as Upline, a.DistributorId,a.uplineid,Bonuspercent,a.PrevCumPV ,a.exclpv ,a.selfpv ,a.grouppv ,         
--a.totalpv ,a.totalBv,LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as DistributorName         
--from DistributorBusiness_Current a         
--inner join DistributorMaster b on a.distributorid=b.distributorid         
--where a.UplineId=@distributorid and b.DistributorStatus=2 order by 'SNo.'         
select        
row_number() over(order by a.DistributorId asc) as 'SNo.',dbo.fnGetDistributorName(@distributorid) as Upline, a.DistributorId,a.uplineid,b.DistributorStatus,round(convert(int,Bonuspercent),0) 'Bonuspercent',round(cast(a.PrevCumPV as varchar(15)),2) as PrevCumPV ,round(cast(a.exclpv as varchar(15)),2) as 'exclpv' ,round(cast(a.selfBv as varchar(15)),2) as 'selfBV' ,round(cast(a.grouppv as varchar(15)),2) as 'grouppv' ,         
round(cast(a.totalpv as varchar(15)),2) as 'totalpv' ,round(cast(a.totalBv as varchar(15)),2) 'totalBv',LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as DistributorName         
from        
DistributorBusiness_Current_WebSite(NOLOCK) a         
inner        
join DistributorMaster(NOLOCK) b on a.distributorid=b.distributorid         
where        
a.UplineId=@distributorid and     
(b.DistributorStatus=2 or b.DistributorStatus=1) order by 'SNo.'         
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetDownline_Live_New]    Script Date: 02/09/2014 13:04:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_GetDownline_Live_New]                 
@distributorid                 
varchar(8)                 
AS                
BEGIN                          
select                
row_number() over(order by a.DistributorId asc) as 'SNo.',dbo.fnGetDistributorName(@distributorid) as Upline, a.DistributorId,
dbo.funGetDistributorLevel(a.DistributorId)as Designation,a.uplineid,b.DistributorStatus,
round(convert(int,Bonuspercent),0) 'Bonuspercent',round(cast(a.PrevCumPV as varchar(15)),2) as PrevCumPV ,
round(cast(a.exclpv as varchar(15)),2) as 'exclpv' ,round(cast(a.selfBv as varchar(15)),2) as 'selfBV' ,
round(cast(a.grouppv as varchar(15)),2) as 'grouppv' , round(cast(a.totalpv as varchar(15)),2) as 'totalpv', 
round(cast(a.totalBv as varchar(15)),2) 'totalBv', LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as DistributorName                 
from                
DistributorBusiness_Current_WebSite(NOLOCK) a                
inner join DistributorMaster(NOLOCK) b on a.distributorid=b.distributorid             
where                
a.UplineId=@distributorid and             
(b.DistributorStatus=2 or b.DistributorStatus=1 or b.DistributorStatus=4 ) order by 'SNo.'                 
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetDownline_Live]    Script Date: 02/09/2014 13:04:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[Sp_GetDownline_Live]   
@distributorid   
varchar(8)   
AS  
BEGIN  
--Old One   
--select row_number() over(order by a.DistributorId asc) as 'SNo.',dbo.fnGetDistributorName(@distributorid) as Upline, a.DistributorId,a.uplineid,Bonuspercent,a.PrevCumPV ,a.exclpv ,a.selfpv ,a.grouppv ,   
--a.totalpv ,a.totalBv,LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as DistributorName   
--from DistributorBusiness_Current a   
--inner join DistributorMaster b on a.distributorid=b.distributorid   
--where a.UplineId=@distributorid and b.DistributorStatus=2 order by 'SNo.'   
select  
row_number() over(order by a.DistributorId asc) as 'SNo.',dbo.fnGetDistributorName(@distributorid) as Upline, a.DistributorId,a.uplineid,round(convert(int,Bonuspercent),0) 'Bonuspercent',round(cast(a.PrevCumPV as varchar(15)),2) as PrevCumPV ,round(cast(a.exclpv as varchar(15)),2) as 'exclpv' ,round(cast(a.selfpv as varchar(15)),2) as 'selfpv' ,round(cast(a.grouppv as varchar(15)),2) as 'grouppv' ,   
round  
(cast(a.totalpv as varchar(15)),2) as 'totalpv' ,round(cast(a.totalBv as varchar(15)),2) 'totalBv',LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as DistributorName   
from  
DistributorBusiness_Current(NOLOCK) a   
inner  
join DistributorMaster(NOLOCK) b on a.distributorid=b.distributorid   
where  
a.UplineId=@distributorid and b.DistributorStatus=2 order by 'SNo.'   
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_FRONTLINE_EXCEL_Live]    Script Date: 02/09/2014 13:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  <Manikant>                  
-- Alter date: <6-Nov-2012>                  
-- Description: <To Get Immediate Upline>                  
-- =============================================     
CREATE PROC [dbo].[SP_GET_FRONTLINE_EXCEL_Live]      
@DSTID VARCHAR(8)      
AS      
BEGIN      
IF OBJECT_ID ('#TEMP') IS NOT NULL BEGIN DROP TABLE VESTIGEINDIAHO_NEW.DBO.#TEMP END      
CREATE TABLE #TEMP(SNo int ,Upline varchar(30),DistributorId varchar(10),uplineid varchar(10),Bonuspercent numeric(16,2),prevcumBV numeric (16,2),exclpv numeric(16,2),selfbv numeric(16,2),grouppv numeric(16,2),totalpv numeric(16,2),totalBv numeric(16,2), 
Distributor_Name varchar(50))      
insert into #temp      
select row_number() over(order by a.DistributorId ASC) as 'SNo.',dbo.fnGetDistributorName(@DSTID) as Upline,  a.DistributorId,a.uplineid,Bonuspercent,a.prevcumBV ,a.exclpv ,a.selfbv ,a.grouppv ,      
a.totalpv ,a.totalBv,LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as distributor_Name      
from DistributorBusiness_Current(NOLOCK) a       
inner join DistributorMaster(NOLOCK) b on a.distributorid=b.distributorid      
where a.UplineId=@DstID and b.DistributorStatus=2 order by LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname))       
      
Declare       
@SNo int,        
@Dst_No varchar(8),        
@Dst_Name varchar(60),        
@fPcent int,        
@Cumbp numeric(16,2),        
@Ebp numeric(10,2),        
@Sbp numeric(10,2),        
@Gbp numeric(10,2),        
@Bp numeric(10,2),        
@Bv int,        
@fupl varchar(8),        
@Sumbp numeric(16,2),        
@Pntshort numeric(16,2),        
@NxtLvl int        
--select *from #temp        
DECLARE C1 CURSOR FOR        
select SNo,distributorid,upline,distributor_name,bonuspercent,prevcumBV,exclpv,selfbv,grouppv,totalpv,totalBV from #temp      
 OPEN C1        
 FETCH NEXT FROM C1 INTO @SNo,@Dst_No,@fupl,@Dst_Name,@fPcent,@Cumbp,@Ebp,@Sbp,@Gbp,@Bp,@Bv        
 IF OBJECT_ID('#DSTPOINT')    IS NOT NULL BEGIN DROP TABLE VESTIGEINDIAHO_NEW.DBO.#DSTPOINT END      
 create table #DSTPOINT(SNo int,Dst_No varchar(20),Dst_Name varchar(60),Pcent numeric(16,2),Cumbp numeric(16,2),Ebp numeric(16,2),Sbp numeric(16,2),Gbp numeric(16,2),Bp numeric(16,2),Bv numeric(16,2),upl VARCHAR(8),NxtLvl int,PntShort numeric(16,2))      
 WHILE @@FETCH_STATUS=0        
 BEGIN        
     set @Sumbp=@Cumbp+@Bp        
             
     -- FORMULA TO CALCULATE NEXT LEVEL             
  if(@fpcent=0)        
     begin        
       set @Pntshort=16-@Sumbp        
       set @NxtLvl=5        
     end        
  ELSE         
  begin        
   if(@fpcent = 5)        
   begin        
    set @Pntshort=501-@Sumbp        
    set @NxtLvl=8        
   end        
   ELSE         
   begin        
    if(@fpcent = 8)        
    begin        
     set @Pntshort=2001-@Sumbp        
     set @NxtLvl=11        
    end        
    ELSE        
    begin         
     if(@fpcent = 11)        
     begin        
      set @Pntshort=4501-@Sumbp        
      set @NxtLvl=14        
     end        
     ELSE         
     begin        
      if(@fpcent = 14)        
      begin        
        set @Pntshort=7501-@Sumbp        
        set @NxtLvl=17        
      end        
      ELSE          
      begin        
       if(@fpcent = 17)        
       begin        
         set @Pntshort=10001-@Sumbp        
         set @NxtLvl=20        
       end        
       ELSE         
       begin        
        if(@fpcent = 20)        
        begin        
         set @nxtlvl=0        
         set @Pntshort=0.0        
        end        
       end        
      end        
     end        
    end        
   end        
     end        
         
       
insert into #DSTPOINT(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl,NxtLvl,PntShort)        
select @SNo,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@Sbp,@Gbp,@Bp,@Bv,@fupl,@NxtLvl,@PntShort        
FETCH NEXT FROM C1 INTO @SNo,@Dst_No,@fupl,@Dst_Name,@fPcent,@Cumbp,@Ebp,@Sbp,@Gbp,@Bp,@Bv          
 END        
  CLOSE C1        
  DEALLOCATE C1         
--end of loop            
 select SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl,NxtLvl,PntShort from #DSTPOINT            
ORDER BY SNO      
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GenerateLogNo]    Script Date: 02/09/2014 13:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GenerateLogNo]
	@PCId	int,
	@LogType    int,
	@OrderMode    int,
	@CreatedBy int ,
	@DistributorId int,
	@BOId int,
	@outParam	VARCHAR(500) OUTPUT	
AS
Begin
        
  DECLARE @LogNo varchar(30),@LogOrderAmount int,@LogAddress varchar(100)
  Set @LogNo=''
	set nocount on ;
	 BEGIN TRY
      if(@LogType=1)
        BEGIN
        
           Select TOP(1) @LogNo=LogNo from OrderLog where Status= 1 and PCId=@PCId AND isChangeAddress=@OrderMode AND LogType=@LogType And Convert(varchar(10),CreatedDate,20)>=Convert(varchar(10),GETDATE(),20)
         if @LogNo= ''
          BEGIN
		
				Execute usp_GetSeqNo 'WLOG', 10, @LogNo Out --Paramter taken WEB for web order instead COLOG for distributor and puc view and locaiton id taken as 10.
		
              INSERT INTO OrderLog ([LogNo] ,[LogType],[DistributorId],[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							 ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,[Status] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate] ,[isZeroLog] ,[isChangeAddress])
																						  SELECT  @LogNo ,1,@DistributorId ,@BOId ,@PCId ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,CountryId ,StateId ,CityId ,[PinCode] ,[Phone1] ,[Phone2]
																							  ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,1 ,@CreatedBy ,GETDATE() ,@CreatedBy ,GETDATE() ,0 ,@OrderMode
																							  FROM Location_Master where LocationId=@PCId
																							  
			 SET @LogOrderAmount = 0;																			  
																							  
		
	       END
		   ELSE
			   BEGIN
					select @LogOrderAmount = sum(COP.PaymentAmount) from COPayment COP where cop.CustomerOrderNo IN
						(select COH.customerOrderNo from coheader COH where COH.LogNo = @LogNo
						 AND COH.Status = 3) AND
						COP.TenderType = 6 --for bank tender type.
				END
				
				
					SELECT @LogAddress =  OL.Address1+' '+OL.Address2+' '+OL.Address3 + ' '
	       			+ CM.CityName +' '+ SM.StateName  FROM OrderLog OL left join CIty_master CM 
	       			on CM.CityId = OL.CityCode 
	       			LEFT join State_Master SM ON
	       			SM.StateId = OL.StateCode WHERE OL.LogNo = @LogNo
	
		END
	  if(@LogType=2)
		         BEGIN
		          Select TOP(1) @LogNo=LogNo from OrderLog where Status= 1 AND DistributorId=@DistributorId AND LogType=2  AND isChangeAddress=@OrderMode And Convert(varchar(10),CreatedDate,20)>=Convert(varchar(10),GETDATE(),20)
         if  @LogNo= ''
          BEGIN
  			
				Execute usp_GetSeqNo 'WLOG', 10, @LogNo Out----Paramter taken WLOG for web order instead COLOG for distributor and puc view and locaiton taken as 10.
			
              INSERT INTO OrderLog ([LogNo] ,[LogType],[DistributorId],[BOId] ,[PCId] ,[Address1] ,[Address2] ,[Address3] ,[Address4] ,[CountryCode] ,[StateCode] ,[CityCode] ,[PinCode] ,[Phone1] ,[Phone2]
																							 ,[Mobile1] ,[Mobile2] ,[EmailId1] ,[EmailId2] ,[Fax1] ,[Fax2] ,[WebSite] ,[Status] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate] ,[isZeroLog] ,[isChangeAddress])
																						  SELECT  @LogNo ,2,@DistributorId ,@BOId ,@PCId ,DistributorAddress1 ,DistributorAddress2 ,DistributorAddress3 ,DistributorAddress4 , DM.DistributorCountryCode,DM.DistributorStateCode ,DM.DistributorCityCode ,DM.DistributorPinCode,DM.DistributorMobNumber ,DM.DistributorMobNumber 
																							  ,DM.DistributorMobNumber  ,'' ,DM.DistributorEMailID ,'' ,DM.DistributorFaxNumber ,'' ,'' ,1 ,@CreatedBy ,GETDATE() ,@CreatedBy ,GETDATE() ,0 ,@OrderMode
																							  FROM DistributorMaster DM where DistributorId=@DistributorId
		
	       	END
	       	ELSE
	       	BEGIN
	       		select @LogOrderAmount = sum(COP.PaymentAmount) from COPayment COP where cop.CustomerOrderNo IN
						(select COH.customerOrderNo from coheader COH where COH.LogNo = @LogNo
						 AND COH.Status = 3) AND --for confirmed orders.
						COP.TenderType = 6 --tender type for bank.
	       	
	       	END
	       	
	       			SELECT @LogAddress =  OL.Address1+' '+OL.Address2+' '+OL.Address3 + ' '
	       			+ CM.CityName +' '+ SM.StateName  FROM OrderLog OL left join CIty_master CM 
	       			on CM.CityId = OL.CityCode 
	       			LEFT join State_Master SM ON
	       			SM.StateId = OL.StateCode WHERE OL.LogNo = @LogNo	
	     
		END
		  	Select @LogNo LogNo,@LogOrderAmount LogOrderAmount,@LogAddress LogAddress,@OrderMode OrderMode,@LogType LogType
		END TRY
		BEGIN CATCH
			Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
				Rollback 
			Return ERROR_NUMBER()
		
		END CATCH	  
					
							
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetGroupDownline_Excel_Live]    Script Date: 02/09/2014 13:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetGroupDownline_Excel_Live]             
 -- Add the parameters for the stored procedure here            
 @UPL varchar(8)            
AS            
BEGIN            
            
-- local declaration section            
DECLARE @ARN VARCHAR(8)            
DECLARE @DRN VARCHAR(8)            
DECLARE @PCENT INT            
DECLARE @COUNT INT            
DECLARE @COUNT1 INT            
DECLARE @DSTARN VARCHAR(8)            
DECLARE @DSTUPL VARCHAR(8)            
set @COUNT=1            
set @COUNT1=1            
            
-- tbl for get all distributor no and percent against upl            
CREATE TABLE #TEMP1(Dst_No varchar(8),Pcent int)            
-- tbl for get distributor no having 20 percent            
CREATE TABLE #TEMP20(Dst_No varchar(8), changed bit, upl varchar(8))            
-- tbl for get distributor no having below 20 percent            
CREATE TABLE #TEMPBELOW20(Dst_No varchar(8), changed bit, upl varchar(8))            
--tbl for get final result all distributor no             
CREATE TABLE #TEMPMAIN(Dst_No varchar(8), changed bit, upl varchar(8))            
-- tbl for get distributor no against upl            
CREATE TABLE #TEMP(Arn varchar(8))            
-- tbl for all merged record with points detail            
CREATE TABLE #DSTTEMP(SNo int,Dst_No varchar(8),Dst_Name varchar(60),Pcent int,Cumbp numeric(16,2),Ebp numeric(10,2),SBV numeric(10,2),Gbp numeric(10,2),Bp numeric(10,2),Bv int,upl varchar(8))            
-- tbl for all merged record with point detail and to get next level and estimate point short            
CREATE TABLE #DSTPOINT(SNo int,Dst_No varchar(8),Dst_Name varchar(60),Pcent int,Cumbp numeric(16,2),Ebp numeric(10,2),SBV numeric(10,2),Gbp numeric(10,2),Bp numeric(10,2),Bv int,upl varchar(8),sumbp numeric(16,2),NxtLvl int,PntShort numeric(10,2))        
  
    
            
--Insert Section            
    INSERT INTO #TEMP SELECT DISTRIBUTORID from DISTRIBUTORMASTER(NOLOCK) where UPLINEDISTRIBUTORID=@UPL and DISTRIBUTORSTATUS=2            
 SET NOCOUNT ON;            
      insert into #TEMP1(Dst_No,Pcent)            
  select DISTRIBUTORID,BONUSPERCENT from DISTRIBUTORBUSINESS_CURRENT(NOLOCK) where DISTRIBUTORID in(select Arn from #TEMP)            
-- End of Insert Section            
            
--Loop for break Record to get distributor number having 20 pcent and below than 20 pcent              
            
             
  DECLARE C1 CURSOR FOR            
   SELECT Dst_No,Pcent FROM #TEMP1            
 OPEN C1            
 FETCH NEXT FROM C1 INTO @ARN,@PCENT                
 WHILE @@FETCH_STATUS=0            
 BEGIN            
   IF(@PCENT=20)                
     BEGIN            
       INSERT INTO #TEMP20(Dst_No,changed , upl)VALUES(@ARN,0,@UPL)            
     END            
   IF(@PCENT<20)              
     BEGIN            
      INSERT INTO #TEMPBELOW20(Dst_No, changed, upl)VALUES(@ARN,0, @upl)            
     END            
 FETCH NEXT FROM C1 INTO @ARN,@PCENT             
 END            
    CLOSE C1            
    DEALLOCATE C1             
-- end of loop                 
            
-- loop to get whole downline of distribtor number having below 20 percent             
            
            
 declare c1 cursor for             
 select dst_no from #tempbelow20 where changed = 0            
  open c1            
 fetch next from c1 into @drn            
 while @@fetch_status = 0            
 begin            
  insert into #tempbelow20(dst_no, changed, upl) select DISTRIBUTORID as dst_no, 0 as changed, @drn             
  from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID =@drn and DISTRIBUTORSTATUS in (1,2)
  update #tempbelow20 set changed = 1 where dst_no = @drn            
  fetch next from c1 into @drn            
 end            
 CLOSE C1            
    DEALLOCATE C1             
-- end of loop            
            
-- insert section to merged all records             
  INSERT INTO #TEMPMAIN(Dst_No, changed, upl)            
   Select @UPL as Dst_No,0,dbo.fnGetUpline(@UPL)            
    union          
    select Dst_No,Changed,UPL from #TEMP20            
    union            
    select Dst_No,Changed,UPL from #TEMPBELOW20            
-- end of insert section                      
-- loop to get points and cumbp,cumbv of all merged records            
            
DECLARE C1 CURSOR FOR            
SELECT Dst_No,UPL FROM #TEMPMAIN            
 OPEN C1       
 FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL                
 WHILE @@FETCH_STATUS=0            
 BEGIN            
     --insert into #DSTTEMP(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl)        
         insert into #DSTTEMP(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,SBV,Gbp,Bp,Bv,upl)          
           select @COUNT,@DSTARN,isnull(dbo.fnGetDistributorName(@DSTARN),'N/A'),BONUSPERCENT,PrevCumPV,ExclPV,SelfBV,GroupPV,TotalPV,TotalBV,@DSTUPL  from DISTRIBUTORBUSINESS_CURRENT(NOLOCK) 
           where DISTRIBUTORID =(select Dst_No from #TEMPMAIN where #TEMPMAIN.Dst_NO=@DSTARN)            
       SET @COUNT=@COUNT+1             
   FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL             
 END            
    CLOSE C1            
    DEALLOCATE C1             
--END OF LOOP            
-- loop to get nextlevel and point short            
            
Declare @SNo int,            
@Dst_No varchar(8),            
@Dst_Name varchar(60),            
@fPcent int,            
@Cumbp numeric(16,2),            
@Ebp numeric(10,2),            
@SBV numeric(10,2),            
@Gbp numeric(10,2),            
@Bp numeric(10,2),            
@Bv int,            
@fupl varchar(8),            
@Sumbp numeric(16,2),            
@Pntshort numeric(16,2),            
@NxtLvl int            
set @NxtLvl=0            
set @Pntshort=0.0            
DECLARE C1 CURSOR FOR            
SELECT SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,SBV,Gbp,Bp,Bv,upl FROM #DSTTEMP            
 OPEN C1            
 FETCH NEXT FROM C1 INTO @SNo,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@SBV,@Gbp,@Bp,@Bv,@fupl                
 WHILE @@FETCH_STATUS=0            
 BEGIN            
     set @Sumbp=@Cumbp+@Bp            
if(@fpcent=0)            
     begin            
       set @Pntshort=16-@Sumbp            
       set @NxtLvl=5            
     end            
  ELSE             
  begin            
   if(@fpcent = 5)            
   begin            
    set @Pntshort=501-@Sumbp            
    set @NxtLvl=8            
   end            
   ELSE             
   begin            
    if(@fpcent = 8)            
    begin            
     set @Pntshort=2001-@Sumbp            
     set @NxtLvl=11            
    end            
    ELSE            
    begin             
     if(@fpcent = 11)            
     begin            
      set @Pntshort=4501-@Sumbp            
      set @NxtLvl=14            
     end            
     ELSE             
     begin            
      if(@fpcent = 14)            
      begin            
        set @Pntshort=7501-@Sumbp            
        set @NxtLvl=17            
      end            
      ELSE              
      begin            
       if(@fpcent = 17)            
       begin            
         set @Pntshort=10001-@Sumbp            
         set @NxtLvl=20            
       end            
       ELSE             
       begin            
        if(@fpcent = 20)            
        begin            
         set @nxtlvl=0            
         set @Pntshort=0.0            
        end            
       end            
      end            
     end            
    end            
   end            
     end            
     -- END OF FORMULA            
     insert into #DSTPOINT(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,SBV,Gbp,Bp,Bv,sumbp,upl,NxtLvl,PntShort)            
           select @COUNT1,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@SBV,@Gbp,@Bp,@Bv,@sumbp,@fupl,@NxtLvl,@PntShort            
      SET @COUNT1=@COUNT1+1            
   FETCH NEXT FROM C1 INTO @SNo,@Dst_No,@Dst_Name,@fPcent,@Cumbp,@Ebp,@SBV,@Gbp,@Bp,@Bv,@fupl               
 END            
    CLOSE C1            
    DEALLOCATE C1             
--end of loop                
select Sno,Dst_No,upl,Dst_Name,cumbp,Ebp,SBV,Gbp,Bp,Bv,Sumbp,Pcent,NxtLvl,PntShort from #DSTPOINT ORDER BY upl            
END
GO
/****** Object:  StoredProcedure [dbo].[sp_HistroryOrder]    Script Date: 02/09/2014 13:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_HistroryOrder]
 
@orderNo varchar(20)
 
AS
BEGIN
  SET NOCOUNT ON;
  declare @IS int,@logOrderAmount int,@logNo varchar(30)
  set @IS=0;
  
  SELECT @logNo = LogNo FROM COHeader WHERE CustomerOrderNo =  @orderNo;
  
  IF @logNo!=null or @logNo!=''
  BEGIN
	select @logOrderAmount = sum(COP.PaymentAmount) from COPayment COP where cop.CustomerOrderNo IN
						(select COH.customerOrderNo from coheader COH where COH.LogNo = @LogNo
						 AND COH.Status = 3) AND
						COP.TenderType = 6
  END
  ELSE
  BEGIN
	SET @logOrderAmount = 0;
  END
  
  
  
  
  
  SELECT ISNULL(CO.CustomerOrderNo,'')[CustomerOrderNo]
      ,ISNULL([Date],'')[Date]
      ,@logOrderAmount LogOrderAmount
      , OL.LogNo AS LogValue,ol.isZeroLog
      , CASE WHEN OL.LogType=1 AND OL.BOId <> OL.PCId THEN ISNULL(Lm.Name,'') + ' ' + ISNULL(OL.LogNo,'') ELSE ISNULL(OL.LogNo,'') END [LogNo]
      --,ISNULL(CO.LogNo,'')[LogNo]
      ,ISNULL(CO.DistributorId,0)[DistributorId]
   ,ISNULL(LTRIM(RTRIM(DM.DistributorFirstName)),'')+' '+ISNULL(LTRIM(RTRIM(DM.DistributorLastName)) ,'')As [DistributorName]
      ,ISNULL(CO.DeliverTo,0)[DeliverTo]
      ,ISNULL(CO.DeliverToAddressLine1,'')[DeliverToAddressLine1]
      ,ISNULL(CO.DeliverToAddressLine2,'')[DeliverToAddressLine2]
      ,ISNULL(CO.DeliverToAddressLine3,'')[DeliverToAddressLine3]
      ,ISNULL(CO.DeliverToAddressLine4,'')[DeliverToAddressLine4]
      ,ISNULL(CO.DeliverToCityId,0)[DeliverToCityId]
      ,ISNULL(CO.DeliverToPincode,'')[DeliverToPincode]
      ,ISNULL(CO.DeliverToStateId,0)[DeliverToStateId]
      ,ISNULL(CO.DeliverToCountryId,0)[DeliverToCountryId]
      ,ISNULL(CO.DeliverToTelephone,0)[DeliverToTelephone]
      ,ISNULL(CO.DeliverToMobile,0)[DeliverToMobile]
      ,PMA.KeyValue1 Status
      ,ISNULL(CO.TotalUnits,0)[TotalUnits]
      ,ISNULL(CO.TotalWeight,0)[TotalWeight]
      ,ISNULL(CO.OrderAmount,0)[OrderAmount]
      ,ISNULL(CO.DiscountAmount,0)[DiscountAmount]
      ,ISNULL(CO.TaxAmount,0)[TaxAmount]
      ,ISNULL(CO.PaymentAmount,0)[PaymentAmount]
      ,ISNULL(CO.ChangeAmount,0)[ChangeAmount]
      ,ISNULL(CO.CreatedBy,0)[CreatedBy]
   ,ISNULL(UM.[UserName],0)[CreatedByName]
      ,ISNULL(CO.[CreatedDate],'')[CreatedDate]
      ,ISNULL(CO.[ModifiedBy],0)[ModifiedBy]
      ,ISNULL(CO.[ModifiedDate],'')[ModifiedDate]
      ,ISNULL([OrderType],0)[OrderType]
      ,ISNULL(CO.PCId,0)[PCId]
      ,ISNULL(lm.LocationCode,0)PCCode
      ,ISNULL(CO.BOId,0)[BOId]
      ,ISNULL(CO.DeliverFromAddress1,'')[DeliverFromAddress1]
      ,ISNULL(CO.DelverFromAddress2,'')[DelverFromAddress2]
      ,ISNULL(CO.DeliverFromAddress3,'')[DeliverFromAddress3]
      ,ISNULL(CO.DeliverFromAddress4,'')[DeliverFromAddress4]
      ,ISNULL(CO.DeliverFromCityId,0)[DeliverFromCityId]
      ,ISNULL(CO.DeliverFromPincode,'')[DeliverFromPincode]
      ,ISNULL(CO.DeliverFromStateId,0)[DeliverFromStateId]
      ,ISNULL(CO.DeliverFromCountryId,0)[DeliverFromCountryId]
      ,ISNULL(CO.DeliverFromTelephone,'')[DeliverFromTelephone]
      ,ISNULL(CO.DeliverFromMobile,'')[DeliverFromMobile]
      ,ISNULL(CO.OrderMode,0)[OrderMode]
      ,ISNULL([UsedForRegistration],'')[UsedForRegistration]
   ,ISNULL(CO.TerminalCode,'')TerminalCode
   ,ISNULL(CO.TotalBV,0)TotalBV
   ,ISNULL(CO.TotalPV,0)TotalPV,
    ISNULL(CO.OrderRemarks,0)Remarks,
    ISNULL(CO.OrderShippingCharges,0)CourierCharges
   ,ISNULL(CO.DistributorAddress, '') DistributorAddress
   ,ISNULL(CM.CityName,'')DeliverToCityName
   ,ISNULL(SM.StateName,'')DeliverToStateName
   ,ISNULL(CNM.CountryName,'')DeliverToCountryName
   ,ISNULL(CM2.CityName,'')DeliverFromCityName
   ,ISNULL(SM2.StateName,'')DeliverFromStateName
   ,ISNULL(CNM2.CountryName,'')DeliverFromCountryName
   ,ISNULL(c.InvoiceNo,'') AS InvoiceNo
   ,ISNULL(pm.KeyValue1, 'true') AS IsPrintAllowed
   ,ISNULL(pm2.KeyValue1, '0') AS ValidReportPrintDays
   , ISNULL(c.InvoiceDate,'') AS InvoiceDate
   
  -- ,ISNULL(dm.SerialNo ,'') AS  SerialNo
    FROM [COHeader] CO
    LEFT JOIN [OrderLog] OL
    ON OL.LogNo = CO.LogNo
 LEFT JOIN DistributorMaster DM On CO.DistributorId=DM.DistributorId
 LEFT JOIN User_Master  UM On CO.CreatedBy=UM.UserId
 LEFT JOIN Location_Master lm ON CO.PCID=lm.LocationId
 LEFT JOIN City_Master CM ON CO.[DeliverToCityId]=CM.[CityId]
 LEFT JOIN State_Master SM ON CO.[DeliverToStateId]=SM.[StateId]
 LEFT JOIN Country_Master [CNM] ON CO.[DeliverToCountryId]=CNM.[CountryId] 
 LEFT JOIN City_Master CM2 ON CO.[DeliverFromCityId]=CM2.[CityId]
 LEFT JOIN State_Master SM2 ON CO.[DeliverFromStateId]=SM2.[StateId]
 LEFT JOIN Country_Master CNM2 ON CO.[DeliverFromCountryId]=CNM2.[CountryId]
 LEFT JOIN CIHeader c ON c.CustomerOrderNo = CO.CustomerOrderNo
 LEFT JOIN Parameter_Master PMA ON PMA.ParameterCode='ORDERSTATUS' AND PMA.KeyCode1=CO.Status 
 left JOIN (SELECT * FROM  Parameter_master WHERE ParameterCode = 'DISALLOWINVOICEPRINT' )pm ON pm.KeyCode1 = co.OrderType 
 left JOIN (SELECT * FROM  Parameter_master WHERE ParameterCode = 'OLDINVOICEPRINT' )pm2 ON pm.KeyCode1 = co.OrderType 
 WHERE CO.CustomerOrderNo=@orderNo
 
 
     SELECT 
         ISNULL(CDD.DiscountAmount,'') DiscountAmount,
       ISNULL(CDD.DiscountPercent,'') DiscountPercent,
       ISNULL(CDD.PromotionId,0) PromotionId,
       ISNULL(CDD.Description,'') as PromoDescription,
       ISNULL(PM.PromotionCategory,0) PromotionType,
        CO.[RecordNo]
       ,CO.ItemId  RowID
       ,CO.ItemName as Name
       ,CO.ShortName
       ,CO.PrintName
       ,CO.ReceiptName
       ,CO.DisplayName
       ,CO.ItemCode
       ,[UOM]
       ,CO.PV * CO.Qty PV
       ,CO.BV * CO.Qty BV
       ,[DP] as DistributorPrice
       ,[UnitPrice]
       ,[MRP]
       ,[Qty]
       ,[Discount]
       ,[TaxAmount]
       ,[TotalWeight]
       ,[Amount]
       ,CO.IsKit
       ,CO.IsComposite
       ,CO.Weight
       ,CO.Length
       ,CO.Height
       ,CO.Width
       ,CO.IsPromo
       ,CO.PrimaryCost
       ,CO.TaxCategoryId
       ,[GiftVoucherCode] as GiftVoucherNumber
       ,[VoucherSrNo]
       ,IM.PromotionParticipation
       ,case when IsPromo=0 then Qty*DP else Qty*1.00 end as Price,
       
       case when IsPromo = 1 then  2 else 1 end as 'IS',
          IM.MerchHierarchyDetailId as MerchHierarchyDetailId
      FROM [CODetail] CO
      left join CODetailDiscount CDD 
      on
      CDD.CustomerOrderNo=CO.CustomerOrderNo AND CDD.RecordNo=CO.RecordNo
      left join 
      Item_Master IM on
      CO.ItemId=IM.ItemId
        LEFT JOIN Promotion_Master PM 
      ON PM.PromotionId=CDD.PromotionId
       where CO.CustomerOrderNo=@orderNo;
      
      
     select [CustomerOrderNo]
       ,[RecordNo]
       ,[TenderType]
       ,[PaymentAmount]
       ,[Date]
       ,[Remark]
       ,[BankName]
       ,[ChqIssueDate]
       ,[ChqExpiryDate]
       ,[ForexAmount]
       ,[Reference]
       ,[CreditCardNumber]
       ,[CardHolderName]
       ,[CardExpiryDate]
       ,[CurrencyCode]
       ,[ExchangeRate]
       ,[CardType]
       ,[ReceiptDisplay] PaymentDescription,
       ReceiptDisplay,
       CASE  WHEN TenderType=5 THEN  DCL.ChequeNo ELSE '' END AS 'VoucherId'
        from COPayment COP LEFT JOIN 
        DistributorBonus_ChequeLink DCL  ON 
         cop.CustomerOrderNo= DCL.OrderNo 
       WHERE CustomerOrderNo=@orderNo AND COP.TenderType!=5
       UNION 
         select [CustomerOrderNo]
       ,[RecordNo]
       ,[TenderType]
       ,[PaymentAmount]
       ,[Date]
       ,[Remark]
       ,[BankName]
       ,[ChqIssueDate]
       ,[ChqExpiryDate]
       ,[ForexAmount]
       ,[Reference]
       ,[CreditCardNumber]
       ,[CardHolderName]
       ,[CardExpiryDate]
       ,[CurrencyCode]
       ,[ExchangeRate]
       ,[CardType]
       ,[ReceiptDisplay] PaymentDescription,
       ReceiptDisplay,
       CASE  WHEN TenderType=5 THEN  DCL.ChequeNo ELSE '' END AS 'VoucherId'
        from   DistributorBonus_ChequeLink DCL LEFT JOIN 
       COPayment COP ON 
         cop.CustomerOrderNo= DCL.OrderNo 
       WHERE CustomerOrderNo=@orderNo AND COP.TenderType=5  AND COP.Reference=DCL.ChequeNo
       
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GroupTest]    Script Date: 02/09/2014 13:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GroupTest]  
  
@UPL varchar(8),
@levelid int  
  
AS     
    
BEGIN    
               
DECLARE @ARN VARCHAR(8)  
  
DECLARE @DRN VARCHAR(8)  
  
DECLARE @PCENT INT  
  
DECLARE @COUNT INT  
  
DECLARE @COUNT1 INT  
  
DECLARE @DSTARN VARCHAR(8)  
  
DECLARE @DSTUPL VARCHAR(8)  
  
CREATE TABLE #TEMP1(Dst_No varchar(8),Pcent int)  
  
CREATE TABLE #TEMPBELOW20(Dst_No varchar(8), changed bit, upl varchar(8))  
  
  
CREATE TABLE #TEMPMAIN(Dst_No varchar(8), changed bit, upl varchar(8))  
  
CREATE TABLE #TEMP(Arn varchar(8))  
  
  
CREATE TABLE #DSTTEMP(SNo int,Dst_No varchar(8),Dst_Name varchar(60),Pcent int,Cumbp numeric(16,2),Ebp numeric(10,2),Sbp numeric(10,2),Gbp numeric(10,2),Bp numeric(10,2),Bv int,upl varchar(8),levelid int)  
  
--Insert Section  
  
INSERT INTO #TEMP SELECT DISTRIBUTORID from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID=@UPL and DISTRIBUTORSTATUS=2  
  
SET NOCOUNT ON;  
  
insert into #TEMP1(Dst_No,Pcent)  
  
select DISTRIBUTORID,BONUSPERCENT from DISTRIBUTORBUSINESS_CURRENT where DISTRIBUTORID in(select Arn from #TEMP)  
  
DECLARE C1 CURSOR FOR  
  
SELECT Dst_No,Pcent FROM #TEMP1  
  
OPEN C1  
  
FETCH NEXT FROM C1 INTO @ARN,@PCENT  
  
WHILE @@FETCH_STATUS=0  
  
BEGIN  
  
INSERT INTO #TEMPBELOW20(Dst_No, changed, upl)VALUES(@ARN,0, @upl)  
  
FETCH NEXT FROM C1 INTO @ARN,@PCENT  
  
END  
  
CLOSE C1  
  
DEALLOCATE C1  
  
declare c1 cursor for  
  
select dst_no from #tempbelow20 where changed = 0  
  
open c1  
  
fetch next from c1 into @drn  
  
while @@fetch_status = 0  
  
begin  
  
insert into #tempbelow20(dst_no, changed, upl) select DISTRIBUTORID as dst_no, 0 as changed, @drn  
  
from DISTRIBUTORMASTER where UPLINEDISTRIBUTORID =@drn and DISTRIBUTORSTATUS = 2  
  
update #tempbelow20 set changed = 1 where dst_no = @drn  
  
fetch next from c1 into @drn  
  
end  
  
CLOSE C1  
  
DEALLOCATE C1  
  
INSERT INTO #TEMPMAIN(Dst_No, changed, upl)  
  
Select @UPL as Dst_No,0,dbo.fnGetUpline(@UPL)  
  
union  
  
select Dst_No,Changed,UPL from #TEMPBELOW20  
  
DECLARE  
C1 CURSOR FOR  
  
SELECT  
Dst_No,UPL FROM #TEMPMAIN  
  
OPEN C1  
  
FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL  
  
WHILE @@FETCH_STATUS=0  
  
BEGIN  
  
insert into #DSTTEMP(SNo,Dst_No,Dst_Name,Pcent,Cumbp,Ebp,Sbp,Gbp,Bp,Bv,upl,levelid)  
  
select @COUNT,@DSTARN,isnull(dbo.fnGetDistributorName(@DSTARN),'N/A'),BONUSPERCENT,PrevCumPV,ExclPV,SelfPV,GroupPV,TotalPV,TotalBV,@DSTUPL,levelid from DISTRIBUTORBUSINESS_CURRENT where DISTRIBUTORID =(select Dst_No from #TEMPMAIN where #TEMPMAIN.Dst_NO=@DSTARN) 
 
  
SET @COUNT=@COUNT+1  
  
FETCH NEXT FROM C1 INTO @DSTARN,@DSTUPL  
  
END  
  
CLOSE C1  
  
DEALLOCATE C1  
  
--select * from #dsttemp  
select row_number() over(order by tm.Dst_No asc) as 'SNo.',tm.Dst_No,tm.Dst_Name,tm.Pcent,tm.Cumbp,tm.Ebp,tm.Sbp,tm.Gbp,tm.Bp,tm.Bv,tm.upl,dst.distributoremailid,dst.distributorregistrationdate, 
dl.levelid,dl.levelname from #dsttemp tm
inner join distributormaster dst
on  tm.Dst_No=dst.DistributorId 
inner join  distributorlevels dl
on tm.levelid=dl.levelid where dl.levelid=@levelid
end
GO
/****** Object:  StoredProcedure [dbo].[sp_SavePUCInfo]    Script Date: 02/09/2014 13:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SavePUCInfo] 
	    @PUCAccDepositJson nvarchar(max), 
	    @PCId				INT ,
		@AVAILABLEAMOUNT	NUMERIC(18,4),
		@CurrentLocationId	INT,
		@LocationCodeId		INT,
		@CREATEDBY			INT,
		@MODIFIEDBY			INT,
		@MODIFIEDDATE		DATETIME,
		@DBAvailAmt         float,
		@ReverseTransactionStatus INT,
		@ReverseTransactionRemarks varchar(200),
	    @OutParam VARCHAR(500) OUTPUT
AS
BEGIN
	BEGIN TRY
	  set nocount on ;
	 set  @MODIFIEDDATE=getdate();
	
	
	
	
	  
	BEGIN TRAN
	
			
			
	
			DECLARE @MyHierarchy JSONHierarchy
			DECLARE  @RECCNT VARCHAR(500)
			declare @XMLItems xml;
			
			INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@PUCAccDepositJson)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)


	
		SELECT 
						
							  b.value('@DepositDate', 'DateTime') as DepositDate,
							  b.value('@DepositModeId', 'varchar(50)') as DepositModeId,
							  b.value('@TransactionNo', 'varchar(100)') as TransactionNo,
							  b.value('@DepositAmount', 'Numeric(12,4)') as DepositAmount
							
								into #PUCAcnt 
							  FROM @XMLItems.nodes('/root/item') a(b)
							  
							  alter table  #PUCAcnt add  RowNo Int Identity(1,1)  ;
					
			
		
	DECLARE 
		@ModifyingUserId		INT,
	    @RECORDNO				varchar(50),
		@RECORDNO_PREFIX		VARCHAR(1000),
		@RECORDNO_INTPART		VARCHAR(50),
		@TEMPINTPART			VARCHAR(50),
		@RECORDNO_INTLENGTH		INT,
		@STARTSEQNO				INT,
		@BEGINLIMIT				INT,
		@ENDLIMIT				INT,
		@TOTALAMOUNT			NUMERIC(18,4)
		
	DECLARE @LOC1 VARCHAR(20)
	DECLARE @LOC2 VARCHAR(20)
		
	SELECT  @RECORDNO =	''

	DECLARE @PUCACCOUNT TABLE
	(
		PCId				INT NOT NULL,
		AVAILABLEAMOUNT		NUMERIC(18,4),
		CurrentLocationId	INT,
		LocationCodeId		INT,
		CREATEDBY			INT,
		MODIFIEDBY			INT,
		MODIFIEDDATE		DATETIME		
	)

	DECLARE @PUCDEPOSIT_NEWRECORDS TABLE
	(
		Id					INT IDENTITY(1,1),
		PCId				INT NOT NULL,
		RECORDNO			VARCHAR(500),
		PAYMENTMODE			INT,
		TRANSACTIONNO		VARCHAR(200),
		AMOUNT				NUMERIC(18,4),
		DATE				DATETIME,
		CREATEDBY			INT,
		MODIFIEDBY			INT
	)

	DECLARE	@iHnd			INT, 
			@RECCNT_NEWRECS	INT

	SELECT	@iHnd = 0, 
			@RECCNT_NEWRECS	= 0


		--STORE PC-ID AND AVAILABLE AMOUNT IN LOCAL VARIABLES

		--STORE PUC-ACCOUNT RECORD IN TABLE VARIABLE
		INSERT INTO	@PUCACCOUNT (PCId, AVAILABLEAMOUNT, CurrentLocationId, CREATEDBY, MODIFIEDBY, MODIFIEDDATE)
		SELECT @PCId, @DBAvailAmt, @CurrentLocationId, @CreatedBy, @ModifiedBy, @ModifiedDate

		--STORE NEW DEPOSIT-RECORD(S) IN A TABLE VARIABLE
		INSERT INTO @PUCDEPOSIT_NEWRECORDS(PCId, PAYMENTMODE, TRANSACTIONNO, AMOUNT, DATE, CREATEDBY, MODIFIEDBY)
		SELECT @PCId, DepositModeId, TransactionNo, ISNULL(DepositAmount, 0.0000), GETDATE(), @CREATEDBY,@MODIFIEDBY
		from #PUCAcnt
		
		SELECT @RECCNT_NEWRECS	= COUNT(*) FROM @PUCDEPOSIT_NEWRECORDS

			
		
		    IF(@ReverseTransactionStatus = 2)
			BEGIN
				DECLARE @SavedRecordNO varchar(50);
				
				DECLARE @TransactionNo varchar(100);
					
					
				select @TransactionNo = TransactionNo from @PUCDEPOSIT_NEWRECORDS;
				
				
				select top 1 @SavedRecordNO = RecordNo from PUCDeposit where TransactionNo = @TransactionNo;
			
				IF EXISTS(select RecordNo from PUCDeposit where ReverseId = @SavedRecordNO)
				BEGIN
					
					SELECT 'PUCEXP003' as outParam
						ROLLBACK
						return;
					
				END
				ELSE BEGIN
					DECLARE @ReverseTransactionAmount int;
				
				
			
				DECLARE @totalAmountAvailable int; --variable will store diff. bet. saved and used amount.
				
				select @ReverseTransactionAmount = -sum(Amount) from PUCDeposit where TransactionNo = @TransactionNo;
				
				select top 1 @SavedRecordNO = RecordNo from PUCDeposit where TransactionNo = @TransactionNo;
				
				select @totalAmountAvailable = (PA.AvailableAmount-sum(PTD.TransAmount)) from 
					PUCAccount PA, PUCTransactionDetail PTD where PA.PCId = @PCId
					and PTD.PCId = @PCId group by PA.AvailableAmount
					
					
				IF(@totalAmountAvailable >= 0)
				BEGIN
					IF(@totalAmountAvailable >= @AVAILABLEAMOUNT) --@AVAILABLEAMOUNT is transaction amount.
					BEGIN
					
						DECLARE @count int;
				
						SET @count = 1; --Becasue only one transaction to be reversed at one time.Hence fetched only one row from temp storage named as @PUCDEPOSIT_NEWRECORDS 
				
						EXEC usp_GetSeqNo 'PUCD', @CurrentLocationId, @RECORDNO OUT
				
						INSERT INTO PUCDEPOSIT (PCId, RECORDNO, PAYMENTMODE, TRANSACTIONNO, AMOUNT, DATE, CREATEDBY, CREATEDDATE, MODIFIEDBY, MODIFIEDDATE,Remarks,REVERSEID)
						SELECT PCId, @RECORDNO, PAYMENTMODE, TRANSACTIONNO, @ReverseTransactionAmount AS AMOUNT, DATE, CREATEDBY, GETDATE(), MODIFIEDBY, GETDATE(),@ReverseTransactionRemarks,@SavedRecordNO--GetDate()
						FROM
							@PUCDEPOSIT_NEWRECORDS
						Where 
							Id = @count
							
							
						UPDATE PUCDeposit set REVERSEID = @RECORDNO,Remarks = @ReverseTransactionRemarks where RecordNo = @SavedRecordNO;
						
						--update PUCDeposit set Remarks = @ReverseTransactionRemarks where 
						
						update PUCAccount set AvailableAmount =  AvailableAmount+@ReverseTransactionAmount
						 where PCId = @PCId
						 
						 
						 select 1 outParam
					
						
					
					END
					ELSE
					BEGIN
					
						SELECT 'PUCEXP001' as outParam
						ROLLBACK
						return;
					END	
				
				END	
				ELSE
				BEGIN
					SELECT 'PUCEXP002' as outParam
					ROLLBACK
					return;
				END	
				END
			END
			ELSE
			BEGIN
				
				--
--SELECT TransactionNo FROM @PUCDEPOSIT_NEWRECORDS GROUP BY TransactionNo HAVING COUNT(1)>1
--
		IF EXISTS (SELECT TransactionNo FROM @PUCDEPOSIT_NEWRECORDS GROUP BY TransactionNo HAVING COUNT(1)>1)
		BEGIN
			SET @OutParam = 'INF0228'
			ROLLBACK 
			RETURN 
		END
	
	IF (@PCID <> -1)
		BEGIN
	
			SELECT
				@RECORDNO_INTLENGTH = 11,
				@STARTSEQNO = -1,
				@BEGINLIMIT = 1,
				@ENDLIMIT = 1

			SET @RECORDNO_INTPART = REPLICATE('0',@RECORDNO_INTLENGTH)

			SELECT @ModifyingUserId  = PUCT.MODIFIEDBY FROM @PUCACCOUNT PUCT


			--CHECK WHETHER PC-ID ALREADY EXISTS AS A PUCACCOUNT
			
			IF NOT EXISTS ( SELECT 1 FROM PUCACCOUNT PUC INNER JOIN @PUCACCOUNT PUCT ON PUC.PCId = PUCT.PCId )	
			INSERT INTO PUCACCOUNT(PCId, AvailableAmount, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
			SELECT @PCId, @TOTALAMOUNT, CreatedBy, GetDate(), ModifiedBy,GetDate()--GetDate()
			FROM @PUCACCOUNT

				--CHECK FOR CONCURRENCY;
				IF EXISTS ( SELECT 1 FROM PUCACCOUNT PUC INNER JOIN @PUCACCOUNT PUCT ON PUC.PCId = PUCT.PCId AND PUC.MODIFIEDDATE <> PUCT.MODIFIEDDATE )
					BEGIN
						IF (@RECCNT_NEWRECS = 0)
							BEGIN
								SET @OutParam = 'INF0022'
								ROLLBACK
								RETURN
							END
					END
					
					
--					SELECT * FROM PUCDeposit pucd INNER JOIN @PUCDEPOSIT_NEWRECORDS tpnr ON pucd.TransactionNo = tpnr.TRANSACTIONNO
					
				-- Check for Duplicate Transection No.
				IF EXISTS (SELECT 1 FROM PUCDeposit pucd INNER JOIN @PUCDEPOSIT_NEWRECORDS tpnr ON pucd.TransactionNo = tpnr.TRANSACTIONNO)
					BEGIN
						select 'INF0144' as outParam;
						--SET @OutParam = 'INF0144'
						ROLLBACK 
						RETURN 
					END

				 
				SET @MODIFIEDDATE = ( SELECT MODIFIEDDATE FROM @PUCACCOUNT PUC )				
			
			
					
--				EXEC usp_GetSeqNo 'PUCD', @CurrentLocationId, @RECORDNO OUT, @RECCNT_NEWRECS

--				SET @RECORDNO_PREFIX = SUBSTRING(@RECORDNO, 1, 9)
--				SET @TEMPINTPART = SUBSTRING(@RECORDNO, 10, LEN(@RECORDNO) - 9)
--				SET @STARTSEQNO = CAST(@TEMPINTPART AS INT)
--				SET @ENDLIMIT = @RECCNT_NEWRECS --+ 1
				
				DECLARE @depositRecordsCount INT, @iCount INT
				
				SELECT @depositRecordsCount = COUNT(1) FROM @PUCDEPOSIT_NEWRECORDS
				SET @iCount = 1
				SELECT @currentLocationId = LocationId FROM dbo.Location_Master WHERE locationcode = (SELECT TOP 1 LocationId FROM dbo.Interface_Control)
				WHILE(@iCount <= @depositRecordsCount)
				BEGIN
					
					EXEC usp_GetSeqNo 'PUCD', @CurrentLocationId, @RECORDNO OUT
				
					INSERT INTO PUCDEPOSIT (PCId, RECORDNO, PAYMENTMODE, TRANSACTIONNO, AMOUNT, DATE, CREATEDBY, CREATEDDATE, MODIFIEDBY, MODIFIEDDATE)
					SELECT PCId, @RECORDNO, PAYMENTMODE, TRANSACTIONNO, ISNULL(AMOUNT,0.0000) AS AMOUNT, DATE, CREATEDBY, GETDATE(), MODIFIEDBY, GETDATE()--GetDate()
					FROM
						@PUCDEPOSIT_NEWRECORDS
					Where 
						Id = @iCount
						
					SET @iCount = @iCount + 1


--					SELECT @STARTSEQNO = @STARTSEQNO + 1, @BEGINLIMIT = @BEGINLIMIT + 1
		
				--SELECT @LocationCodeId , @RecNumber
				--INTERFACE-AUDIT ENTRY
				SELECT @LOC1 = lm.LocationCode FROM Location_Master lm WHERE lm.LocationId = @LocationCodeId	
				
				
--				IF EXISTS (SELECT 1 FROM Location_Master lm WHERE lm.LocationId = @CurrentLocationId AND lm.LocationType = 1)
--					BEGIN				
--						SELECT '2' 
--						--SELECT @LOC2 = lm.LocationCode FROM Location_Master lm WHERE lm.LocationId = @PCID	
--						EXEC [usp_Interface_Audit] '','PUC',@LOC1,'PUCACCOUNT',@PCID,@RecNumber,NULL,NULL,NULL,'I',@ModifyingUserId,@RECCNT OUTPUT
--						--EXEC [usp_Interface_Audit] '','PUC',@LOC2,'PUCACCOUNT',@PCID,@RecNumber,NULL,NULL,NULL,'I',@ModifyingUserId,@RECCNT OUTPUT
--					END	
--				ELSE
--				BEGIN
						EXEC [usp_Interface_Audit] '','PUC',@LOC1,'PUCACCOUNT',@RECORDNO,NULL,NULL,NULL,NULL,'I',@ModifyingUserId,@RECCNT OUTPUT
--					END	
				END

				SET @TOTALAMOUNT = (SELECT SUM(ISNULL(PUCD.AMOUNT,0.0000)) 
				                    FROM PUCACCOUNT PUC 
				                    INNER JOIN @PUCACCOUNT PUCT 
				                    ON PUC.PCId = PUCT.PCId 
				                    INNER JOIN PUCDEPOSIT PUCD 
				                    ON PUC.PCId = PUCD.PCId )
				
				--2. UPDATE(in this case, INCREASE) AVAILABLE AMOUNT AGAINST THE EXISTING PC-ID RECORD
				
				UPDATE PUC
				SET
					PUC.AvailableAmount	=	ISNULL(@TOTALAMOUNT,0),--ISNULL(PUC.AVAILABLEAMOUNT, 0.0000) + SUM(ISNULL(PUCDT.AMOUNT,0.0000)),
					PUC.MODIFIEDBY		=	@ModifyingUserId,
					PUC.MODIFIEDDATE	=	GetDate()--GetDate()
				FROM
					PUCAccount	PUC
				Where 
					PUC.PCId = @PCID	
					
					
				Select 1 outParam	;
								
			END



		--CHECK WHETHER PC-ID IS VALID
			
			END


		
	 COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK
		SET @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
		SELECT @outParam 
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SaveOrdrLog]    Script Date: 02/09/2014 13:05:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SaveOrdrLog]
 

@LogType int
,@DistributorId int
,@BOId int
,@PCId int
,@Address1 varchar(50)
,@Address2 varchar(50)
,@Address3 varchar(50)
,@Address4 varchar(50),
@CountryCode int,@StateCode  int,@CityCode varchar(20),@PinCode varchar(20),@Phone1 varchar(20),@Phone2 varchar(20),@Mobile1 varchar(20),@Mobile2 varchar(20)
						,@EmailId1 varchar(20),@EmailId2 varchar(20)
						,@Fax1 varchar(20),@Fax2 varchar(20)
						,@WebSite varchar(20)
						,@Status int,@CreatedBy int,@CreatedDate varchar(30),@ModifiedBy int
						,@ModifiedDate varchar(50),@isZeroLog varchar(20),@isChangeAddress varchar(20),@LogNo varchar(25),@AWBNo varchar(40)

AS
BEGIN
SET NOCOUNT ON
DECLARE  @SeqNo varchar(30) ;

 if (@LogNo='')BEGIN
   EXEC usp_GetSeqNo 'COLOG',@BOId,@SeqNo Out
    insert into [OrderLog]
						(
						 [LogNo],[LogType],[DistributorId],[BOId],[PCId],[Address1],[Address2],[Address3],[Address4]
						,[CountryCode],[StateCode],[CityCode],[PinCode],[Phone1],[Phone2],[Mobile1] ,[Mobile2]
						,[EmailId1],[EmailId2],[Fax1],[Fax2],[WebSite],[Status],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],isZeroLog,isChangeAddress		
						)					
					SELECT
						@SeqNo,@LogType,@DistributorId,@BOId,@PCId,@Address1,@Address2,@Address3,@Address4
						,@CountryCode,@StateCode,@CityCode,@PinCode,@Phone1,@Phone2,@Mobile1 ,@Mobile2
						,@EmailId1,@EmailId2,@Fax1,@Fax2,@WebSite,@Status,@CreatedBy,GETDATE(),@ModifiedBy,GETDATE(),@isZeroLog,@isChangeAddress		
						
						
			IF @LogType!=2
			BEGIN
			       SELECT  lm.Name,ol.LogNo      
			             FROM OrderLog ol 
			               INNER JOIN Location_Master lm
			               ON ol.PCId = lm.LocationId
			              WHERE ol.LogNo = @SeqNo
			END
			else
			 BEGIN
			SELECT LogNo      
			             FROM OrderLog 
			               WHERE LogNo = @SeqNo
			END
	END
	ELSE 
	  BEGIN
	    UPDate OrderLog Set AWBNo=@AWBNo Where LogNo=@LogNo
	    SELECT LogNo      
			             FROM OrderLog 
			               WHERE LogNo = @LogNo
	  END		
	END
GO
/****** Object:  StoredProcedure [dbo].[sp_RegisterDistributor]    Script Date: 02/09/2014 13:05:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_RegisterDistributor]        
					 @DistributorId INT,@UplineDistributorId INT,@DistributorTitle INT
					,@DistributorFirstName VARCHAR(30),@DistributorLastName VARCHAR(30),@DistributorDOB VARCHAR(30),
					@Co_DistributorTitle INT,@Co_DistributorFirstName VARCHAR(30),@Co_DistributorLastName VARCHAR(30),
					@Co_DistributorDOB VARCHAR(20),@DistributorAddress1 VARCHAR(100),@DistributorAddress2 VARCHAR(100),
					@DistributorAddress3 VARCHAR(100),@DistributorCityCode INT,@DistributorStateCode INT,
					@DistributorCountryCode INT,
					@DistributorPinCode varchar(20),@DistributorTeleNumber varchar(20),@DistributorMobNumber varchar(20),
					@DistributorEMailID VARCHAR(50),@DistributorRegistrationDate VARCHAR(30),@MinFirstPurchaseAmount NUMERIC(18,4),
					@DistributorStatus VARCHAR(10),@LocationId INT,@DistributorPANNumber VARCHAR(10),@CreatedBy varchar(20),
					@CreatedDate VARCHAR(30),@ModifiedBy INT,@ModifiedDate VARCHAR(30),@SubZone INT,
					@Area INT,@Zone INT,@bankId int,@IFSCCode VARCHAR(30),@accountNumber varchar(30),@ForSkinCareItem int,
					@distributorPassword varchar(20),
					@uploadedPan VARCHAR(400),@uploadedAddress VARCHAR(400),@uploadedCancelledCheque VARCHAR(400),        
					@outParam VARCHAR(500) OUTPUT       
       
AS        
Begin        
   -- SET NOCOUNT ON added to prevent extra result sets         
 SET NOCOUNT ON;    
    
 BEGIN TRANSACTION    
        
 BEGIN TRY  
       
       DECLARE @distributorBankId int;
       DECLARE @distributorAccountLength int;
       DECLARE @RTGSCodeValidation int;
       
       
       
 
		IF EXISTS(select DistributorId from DistributorMaster where DistributorId = @DistributorId)
		BEGIN
					
					SELECT 'INV001' as OutParam
						ROLLBACK
						return;
					
		END
		IF @DistributorPANNumber !='' AND @DistributorPANNumber is not null
		BEGIN
			IF EXISTS(select DistributorPANNumber from DistributorMaster where DistributorPANNumber = @DistributorPANNumber AND (DistributorPANNumber != '' OR DistributorPANNumber IS NOT NULL))
			BEGIN
				SELECT 'INV002' as OutParam --Pan number already exist.
						ROLLBACK
						return;
			END
		
		END
		
		IF ((@IFSCCode != '' AND @IFSCCode IS NOT NULL) AND (@accountNumber !='' AND @accountNumber IS NOT NULL))
		BEGIN
			IF EXISTS(select 1 from DistributorAccountDetails where DistributorBankBranch = @IFSCCode AND DistributorBankAccNumber = @accountNumber)
			BEGIN
			SELECT 'INV003' as OutParam --Account info already exist.
						ROLLBACK
						return;
			END
		
		END
		
		IF @RTGSCodeValidation != '' AND @RTGSCodeValidation IS NOT NULL
			BEGIN
		
			SELECT @RTGSCodeValidation = COUNT(BankBranckCode)  from BankBranch_Master where BankBranckCode = @IFSCCode
       
			IF @RTGSCodeValidation = 0
			BEGIN
				SELECT 'INV004' as OutParam --Wrong rtgs code.
							ROLLBACK
							return;
			END
		
			IF EXISTS(select 1 from BankBranch_Master where BankBranckCode = @IFSCCode)
			BEGIN
				SELECT @distributorBankId = BankId from  BankBranch_Master WHERE BankBranckCode = @IFSCCode
				
				IF @distributorBankId != @bankId
				BEGIN
					SELECT 'INV005' as OutParam --wrong rtgs code entered.
								ROLLBACK
							return;
				END
			
			
				SELECT @distributorAccountLength = FixedLength from BankAccountLength where BankId = @distributorBankId
				
				IF @distributorAccountLength IS NOT NULL
				BEGIN
				   IF LEN(@distributorAccountLength) != LEN(@accountNumber)
				   BEGIN
						SELECT 'INV006' as OutParam --Wrong account number entered.
							ROLLBACK
							return;
				END
				END

			END
		
		END
		
		ELSE
		BEGIN
						
					 DECLARE  
	   @distSeqNo VARCHAR(30),
	   @HierarchyLevel INT,
	   @seqNumber VARCHAR(30)
	   
	   set @distSeqNo = '';
	   
	   EXECUTE usp_GetSeqNo 'DSN', @LocationId, @distSeqNo Out
	   
	   set @seqNumber = @distSeqNo;
	   
	   SELECT @HierarchyLevel = HierarchyLevel
		FROM DistributorMaster WHERE DistributorId = @UplineDistributorId
		
		set @HierarchyLevel = @HierarchyLevel +1;
		
		
  insert into DistributorMaster(DistributorId,UplineDistributorId,DistributorTitle
					,DistributorFirstName,DistributorLastName,DistributorDOB,Co_DistributorTitle,Co_DistributorFirstName,Co_DistributorLastName,
					Co_DistributorDOB,DistributorAddress1,DistributorAddress2,DistributorAddress3,DistributorCityCode,DistributorStateCode,DistributorCountryCode,
					DistributorPinCode,DistributorTeleNumber,DistributorMobNumber,DistributorEMailID,DistributorRegistrationDate,DistributorActivationDate,MinFirstPurchaseAmount,
					DistributorStatus,LocationId,DistributorPANNumber,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,SubZone,Area,Zone,BankID,HierarchyLevel,SerialNo,
					ForSkinCareItem,Password,uploadedPan,uploadedAddress,uploadedCancelledCheque) 
					values (@DistributorId, @UplineDistributorId, @DistributorTitle 
					,@DistributorFirstName,@DistributorLastName,@DistributorDOB,@Co_DistributorTitle,@Co_DistributorFirstName,@Co_DistributorLastName
					,@Co_DistributorDOB,@DistributorAddress1,@DistributorAddress2,@DistributorAddress3,@DistributorCityCode, @DistributorStateCode,@DistributorCountryCode,
					 @DistributorPinCode, @DistributorTeleNumber, @DistributorMobNumber,@DistributorEMailID,GETDATE(),GETDATE(),@MinFirstPurchaseAmount,
					@DistributorStatus,@LocationId,@DistributorPANNumber,@CreatedBy,GETDATE(),@ModifiedBy,GETDATE(),@SubZone,@Area,@Zone,@bankId,@HierarchyLevel,@seqNumber
					,@ForSkinCareItem,@distributorPassword,@uploadedPan,@uploadedAddress,@uploadedCancelledCheque )  
					
					
	IF @IFSCCode!= '' AND @accountNumber != '' AND @bankId!=''
	BEGIN
		insert into DistributorAccountDetails values(@DistributorId,'',@IFSCCode,@accountNumber,@bankId,@CreatedBy,GETDATE(),@ModifiedBy,GETDATE(),1)-- one taken as primary for new distributor registration.				  
	
	END
	
					
	
    
        
--  SELECT @minAmount = im.MinKitValue FROM Item_Master im        
--  INNER JOIN CODetail c3        
--  ON im.ItemId = c3.ItemId        
--  AND c3.CustomerOrderNo = @kitOrderNo        
	--SELECT @minAmount = 0  
            
       -- SELECT @kitInvoiceNo = 0  
      
          
               
--    UPDATE COHeader SET UsedForRegistration = 1  ,ModifiedBy=@ModifiedBy ,ModifiedDate=Getdate()        
--    WHERE CustomerOrderNo=@kitOrderNo        
--            
            
   /*         
    --Insert account details        
    IF(@BankId  > 0 AND LEN(@AccountNo) > 0)        
    BEGIN        
     INSERT INTO DistributorAccountDetails        
     (        
      DistributorId,        
      DistributorBankName,        
      DistributorBankBranch,        
      DistributorBankAccNumber,        
      BankId,        
      CreatedBy,        
      CreatedDate,        
      ModifiedBy,        
      ModifiedDate,        
      IsPrimary        
     )        
     SELECT        
      DistributorId,        
      (        
        SELECT         
         KEYVALUE1        
        FROM         
         PARAMETER_MASTER        
        WHERE         
         PARAMETERCODE = 'BANKID'        
        AND        
         KEYCODE1 = BankCode        
      ) As BankName,        
      '',        
      AccountNumber,        
      BankCode,        
      CreatedById,        
      GetDate(),        
      ModifiedById,        
      GetDate(),        
      1        
     FROM        
      OPENXML(@iHnd, 'Distributor', 2)        
     WITH        
     (        
      DistributorId INT,        
      AccountNumber VARCHAR(50),        
      BankCode  INT,        
      CreatedById  INT,        
      ModifiedById INT        
     )        
    END        
      
    ---Distributor Id proof--Uploaded file-------
      INSERT INTO DistributorWebExtensions(DistributorId,ImageName,IsValid)
      VALUES(@distributorId,@UploadedFile,0)
          
    ------Maintain Registration History-----------------------  
    INSERT INTO DistributorRegistration_History (DistributorId,RegisteredDistributorId,CreateDate)     
    VALUES(@distributorId,@uplineDistributorId,@date)  
    -----------------------End--------------------------------  
      
      
 */     
      
   -- Interface Audit Insert        
   -- 1. Insert for Distributor Master Entry        
   -- 2. Update Co Header for Kit Order        
   -- 3. Insert for Distributor Accounts Details -- NOT REQD.   
  
  declare @reccnt varchar(10);
  
  set @reccnt = '';      
            
  EXEC usp_Interface_Audit '','DISTMST','','DistributorMaster',@DistributorId,NULL,NULL,NULL,NULL,'I',@ModifiedBy,@reccnt OUTPUT        
   --EXEC usp_Interface_Audit '','CUSTORD','','COHeader',0,NULL,NULL,NULL,NULL,'U',@ModifiedBy,@reccnt OUTPUT        
     
     
           
   SET @outParam = @seqNumber
   
   select @outParam 'DistributorSeqNo'      
            
          
         
  -- Check if the distributor number is already registered, then return the distributor details        
-- Else Check if upline exists, if no return message        
  -- if no Check if the Order Number mentioned is a kit order, if no then return error message        
  -- If yes, then check if that kit order is already used if yes then return message        
  -- If no, then register the distributor and return the distributor details         
         
 -- Exec sp_xml_removedocument @iHnd        
     
   COMMIT TRANSACTION   
				
				END
 
    
          
 END TRY  
 BEGIN CATCH      
    ROLLBACK TRANSACTION  
  SELECT @outParam= 'Database Error:-ProcName: SC_RegisterDistributor ErrorState :' + CAST(ERROR_STATE() AS VARCHAR) + '  ErrorNumber :'+ CAST(ERROR_NUMBER() AS VARCHAR) + '   ErrorLine:'+ CAST(ERROR_LINE() AS VARCHAR) + '  Error_Msg:  '+ ERROR_MESSAGE()        
    SELECT @outParam OutParam
 END CATCH       
End
GO
/****** Object:  StoredProcedure [dbo].[sp_InventorySave2]    Script Date: 02/09/2014 13:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_InventorySave2]
			@jsonForItems nvarchar(max),
			@ApprovedBy		INT,			
			@SeqNo				VARCHAR(20),			@ApprovedDate	VARCHAR(20),
			@LocationId			INT,					@UserId			INT,
			@InitiatedDate		VARCHAR(20),			
			@StatusId			INT  ,					
			@ModifiedBy			INT,		
			@CreatedDate		VARCHAR(20),
			@ModifiedDate		VARCHAR(20),
			@SourceLocationId	INT,
			@tempIsInternalBatchAdj int	,
			@outParam    varchar(200) output		
			
			
AS
Begin
  set nocount on
	Begin Try
	   BEGIN TRANSACTION
			Declare @RetSeqNo VARCHAR(200)
			,@iHnd int,@RecCnt			VARCHAr(2000), @ReasonCode varchar(10),@LocationCode varchar(10),@LocationType varchar(10)	
			Set @iHnd = 0

		
	--Added for storing stock adjustment initiation
		Declare @InitiateStockAdjustment	VARCHAR(10),@MyHierarchy JSONHierarchy,
			@xml XML ,@inputParam varchar(500)

		Exec sp_xml_preparedocument @iHnd OutPut, @inputParam
        
			           INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@MyHierarchy)
		                  
		
		
		                  
							SELECT 
						
								
							  b.value('@ItemId', 'int') as ItemId,
							  b.value('@ItemCode', 'varchar(20)') as ItemCode,
							  b.value('@ItemName', 'varchar(50)') as ItemDescription,
							  b.value('@FromBucketId', 'varchar(30)') as FromBucketId,
							  b.value('@ToBucketId', 'varchar(30)') as ToBucketId,
							  b.value('@Quantity', 'int') as Quantity,
							  b.value('@ReasonCode', 'varchar(50)') as ReasonDescription,
							  b.value('@ApprovedQty', 'int') as ApprovedQty,
							  b.value('@BatchNo', 'Varchar(20)') as ManufactureBatchNo,
							  b.value('@UOMId', 'int') as UOMId,
							  b.value('@ReasonId', 'int') as ReasonCode,
							  b.value('@MfgDate', 'Varchar(20)') as MfgDate,
							  b.value('@ExpDate', 'Varchar(20)') as ExpDate,
							  b.value('@BatchId', 'varchar(20)') as BatchNo,
							  b.value('@ToItemid', 'int') as ToItemid,
							  b.value('@ToBatchNo', 'Varchar(20)') as ToBatchNo,
								b.value('@ToManufactureBatchNo', 'Varchar(20)') as ToManufactureBatchNo,
								b.value('@ToItemName', 'Varchar(50)') as ToItemName,
									b.value('@ToItemCode', 'Varchar(50)') as ToItemCode
								into #ItemDetail 
							  FROM @xml.nodes('/root/item') a(b)                                 
		               
		               alter table  #ItemDetail add  RowId Int Identity(1,1)  ; 
			           
			           
			           UPDATE #ItemDetail set ToItemid =IM.ItemId,MfgDate=IBD.MfgDate,ExpDate=IBD.ExpDate from #ItemDetail ID  INNER JOIN  
			           Item_Master IM On IM.ItemCode=ID.ToItemCode
			           INNER JOIN ItemBatch_Detail IBD ON IBD.BatchNo=ID.BatchNo
	

	
	
--Select '1', * From #ItemDetail
		select @ReasonCode = ReasonCode from #ItemDetail 

		
		--SELECT @InitiateStockAdjustment = Value from dbo.ufn_StringListSplit(@seqNo,'/') where value = 'HO'
		SELECT @InitiateStockAdjustment = Value from dbo.ufn_StringListSplit(@seqNo,'/') where value in (select top 2 value from dbo.ufn_StringListSplit(@seqNo,'/'))
		IF ISNULL(NULLIF(@SeqNo,''),'-1')='-1'
		BEGIN
			EXECUTE usp_GetSeqNo 'SA', @LocationId, @RetSeqNo Out
			SET @SeqNo=@RetSeqNo
			
			INSERT INTO dbo.InventoryAdj_Entry	(AdjustmentNo,	LocationId,	CreatedBy,		CreatedDate,	ModifiedBy,	ModifiedDate,IsInternalBatchAdj)
				VALUES				(@SeqNo,	@SourceLocationId,	@ModifiedBy,	GETDATE(),	@ModifiedBy,GETDATE(),@tempIsInternalBatchAdj)		
			set @StatusId=1	
		END						
		ELSE 
		BEGIN
			-- Check Concurrency, Get DBDate
			DECLARE @DBDate DATETIME
			SELECT @DBDate = (SELECT ModifiedDate = (CASE WHEN ModifiedBy Is Not Null THEN ModifiedDate ELSE CreatedDate END) FROM InventoryAdj_Entry WHERE  AdjustmentNo = @SeqNo)

			DECLARE @PreInitiatedDate AS DATETIME 
			SELECT @PreInitiatedDate = [Date] FROM InventoryAdj_Entry WHERE AdjustmentNo = @SeqNo

			DECLARE @SourceLocationCode AS VARCHAR(20)
			SELECT @SourceLocationCode = LocationCode FROM Location_Master lm WHERE lm.LocationId = @SourceLocationId
			--Initiated, Approved Or Rejected
		
			--Select @StatusId
			
end
-----------------------------------------------------------------------------
IF @StatusId = 1 OR @StatusId = 2
			BEGIN
				UPDATE InventoryAdj_Entry
				SET	
					ModifiedBy			=	@ModifiedBy,		
					ModifiedDate		=	getDate()	,
					Date				=   @InitiatedDate ,
					UserId				=   CASE WHEN @StatusId = 2 THEN @ModifiedBy ELSE NULL END
				WHERE AdjustmentNo = @SeqNo
					DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				INSERT INTO InventoryAdj_DetailEntry (AdjustmentNo, RowNo, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemid,ToBatchNo)
				SELECT  @SeqNo, RowId, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemId,ToBatchNo FROM #ItemDetail
	
		    END
			ELSE IF @StatusId = 3 OR @StatusId = 4
			BEGIN
				
				UPDATE InventoryAdj_Entry
				SET	
					ModifiedBy				=	@ModifiedBy,		
					ModifiedDate			=	GETDATE()	,
					ApprovedRejectedDate	=   CASE WHEN @StatusId = 3 OR @StatusId = 4 THEN GETDATE() ELSE NULL END,
					ApprovedBy				=   CASE WHEN @StatusId = 3 THEN @ModifiedBy ELSE NULL END,
					RejectedBy				=   CASE WHEN @StatusId = 4 THEN @ModifiedBy ELSE NULL END,
					UserId					=   CASE WHEN @StatusId = 3 THEN @ModifiedBy ELSE UserId END
					
				WHERE AdjustmentNo = @SeqNo
--				SELECT @StatusId
                  	DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				INSERT INTO InventoryAdj_DetailEntry (AdjustmentNo, RowNo, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemid,ToBatchNo)
				SELECT  @SeqNo, RowId, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemId,ToBatchNo FROM #ItemDetail
	
			END
			ELSE IF @StatusId = 5 -- Cancel
			BEGIN
				SET @outParam = '8004'
		
				-- As per discussion with Lalit, 01-09-2009, We'll remove the code from Table
				DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				DELETE FROM InventoryAdj_Entry WHERE AdjustmentNo = @SeqNo
				Select @SeqNo SeqNo
				ROLLBACK
				RETURN 
			END

--				SELECT 'tEST', 'uPLODATE lOCATION1', @StatusId
				
			IF (@StatusId = 3 )
			BEGIN 
					DECLARE @Item AS INT ,@ToItem AS INT,@ToBatch AS VARCHAR(20), @ToMfgBatchNo AS VARCHAR(20),@Reason AS VARCHAR(10),@MfdDate AS VARCHAR(50),@ExdDate AS VARCHAR(50)
					DECLARE @Count AS INT
					DECLARE @mfgDate AS DATETIME
					DECLARE @ExpDate AS DATETIME
					DECLARE @MRP AS MONEY
					DECLARE @Batch_New	AS VARCHAR(20)
					DECLARE @BatchTest INT

					DECLARE Entry_Cursor  CURSOR FOR SELECT ItemId,ToItemId,ToBatchNo,ToManufactureBatchNo,ReasonCode,MfgDate,ExpDate FROM  #ItemDetail	
					OPEN Entry_Cursor 
					FETCH next from Entry_Cursor INTO @Item,@ToItem,@ToBatch,@ToMfgBatchNo,@ReasonCode, @MfdDate,@ExdDate
					While @@FETCH_STATUS =0
					Begin
							
							BEGIN
							
								Select @BatchTest = count(1) from itembatch_detail where BatchNo=@ToBatch AND  itemid = @ToItem 
													AND (datediff(dy,convert(varchar(30),MfgDate,102),convert(varchar(30),@MfdDate,110))='0') 
													AND (datediff(dy,convert(varchar(30),ExpDate,102),convert(varchar(30),@ExdDate,110))='0')
							
								select @MRP = MRP from ITEMMRP where itemid = @Item
						 
								IF(@BatchTest=0)
								BEGIN
--									SELECT '3'
										DECLARE @LocationCode1 varchar(20)
										SET @LocationCode1 =( select LocationCode From Location_Master Where LocationID = @LocationId)
										Declare @SeqNo1 Varchar(20)  
										Execute usp_GetSeqNo 'BAT', @locationID, @SeqNo1 Out
										Declare @p AS INT  
										SET @p = CAST(@SeqNo1 AS INT) 
																
										Set @Batch_New ='BAT/'+@LocationCode1+'/'+REPLICATE('0',6 - LEN(@p))+cast(@p as Varchar) 
                                           --SELECT @Batch_New+' '+convert (varchar(20),@ToItem,20)  OutParam 
                                           --ROLLBACK
                                           --RETURN
										--IF NOT EXISTS (SELECT 1 FROM ItemBatch_Detail ibd WHERE ibd.BatchNo = @Batch_New)
										INSERT INTO ItemBatch_Detail VALUES(@Batch_New,@ToItem,@Batch_New,@MfdDate,@ExdDate,@MRP,@ToMfgBatchNo,'1', @ModifiedBy,GETDATE(),@ModifiedBy,getdate())
										UPDATE #ItemDetail set ToBatchNo = @Batch_New where toitemid = @toItem
									
--										SELECT 'TEST', * FROM #ItemDetail
									END
									
							END
					Fetch Next from Entry_Cursor into @Item,@ToItem,@ToBatch,@ToMfgBatchNo,@ReasonCode , @MfdDate,@ExdDate
				END
					CLOSE Entry_Cursor
					DEALLOCATE Entry_Cursor
			END
			
			
			IF ( @StatusId = 2 )-- StatusId = 2 For Initiate at BO, StatusId = 3, Approved At HO
			BEGIN


				IF EXISTS (SELECT ResultValue From (SELECT CASE WHEN id.Quantity > ilb.Quantity THEN 1 ELSE 0 END ResultValue FROM (SELECT SUM(Quantity) Quantity, LocationId, ItemId, BucketId, BatchNo FROM Inventory_LocBucketBatch ilb
											GROUP BY LocationId, ItemId, BucketId, BatchNo) ilb 
				INNER JOIN (SELECT SUM(Quantity) Quantity, ItemId, FromBucketId, BatchNo FROM #ItemDetail id
				GROUP BY ItemId, FromBucketId, BatchNo) id 
				ON id.FromBucketId = ilb.BucketId AND id.BatchNo = ilb.BatchNo AND ilb.LocationId = @LocationId
				AND id.ItemId = ilb.ItemId) AS RESULT WHERE ResultValue = 1)
				BEGIN
					SET @outParam = 'VAL0022'
					Select @outParam OutParam
					ROLLBACK
					RETURN 
				END
           END
				-- Move Quantity from FromBucket To ToQuantity
				-- Remove Quantity from - FROM BUCKET
				-----------------------------------------------------------------------------------------------
		
						
				If (@StatusId = 3)
				BEGIN
				
			                   DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempFromBucketId INT 
			                   ,@tempQuantity Numeric(18,4),@tempToBucketId INT,@ToItemid varchar(50),@tempToBatchNo varchar(20)
            
           DECLARE INVLOCBUCKBATCH CURSOR FOR 		   SELECT BatchNo,ItemId, Quantity, FromBucketId, ToBucketId,ToItemid,ToBatchNo
						                      FROM #ItemDetail 	
                       
                        OPEN INVLOCBUCKBATCH  
            FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempItemID,@tempQuantity  ,@tempFromBucketId,@tempToBucketId,@ToItemid,@tempToBatchNo
                                 
                            WHILE @@FETCH_STATUS = 0  
                              BEGIN 
                                EXEC sp_updateInventoryInStockLedger 17,@SeqNo,@LocationId,@ToItemid,@tempToBatchNo,@tempQuantity,@tempFromBucketId,  
                            0.00 ,0,@outParam OUT  
                               if @outParam!=''  
                                   BEGIN  
                                   SELECT @outParam+','+@ToItemid+' ,'+ convert (varchar(20),@tempQuantity,20) OutParam 
                                   
                                     CLOSE INVLOCBUCKBATCH  
                       
                                     DEAllOCATE INVLOCBUCKBATCH  
                                   ROLLBACK  
                                   RETURN  
                                   END  
                            Set @tempQuantity=-@tempQuantity;
                          
                                EXEC sp_updateInventoryInStockLedger 16,@SeqNo,@LocationId,@tempItemID,@tempBatchNo,@tempQuantity,@tempToBucketId,  
                            0.00 ,0,@outParam OUT 
                            if @outParam!=''  
                                   BEGIN  
                                   SELECT @outParam+','+@ToItemid+' ,'+ convert (varchar(20),@tempQuantity,20) OutParam 
                                   
                                     CLOSE INVLOCBUCKBATCH  
                       
                                     DEAllOCATE INVLOCBUCKBATCH  
                                   ROLLBACK  
                                   RETURN  
                                   END  
                             FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempItemID,@tempQuantity  ,@tempFromBucketId,@tempToBucketId,@ToItemid,@tempToBatchNo
                              END  
                                
                              CLOSE INVLOCBUCKBATCH  
                                
                              DEAllOCATE INVLOCBUCKBATCH  
           
         
					--UPDATE Inventory_LocBucketBatch SET Quantity = ilm.Quantity - tod.Quantity
					--FROM Inventory_LocBucketBatch ilm
					--INNER JOIN (
					--			SELECT ItemId, Quantity, BatchNo, ManufactureBatchNo, FromBucketId, ToBucketId
					--			FROM #ItemDetail
					--			GROUP BY ItemId, Quantity, BatchNo, ManufactureBatchNo, FromBucketId, ToBucketId
					--			) tod
					--ON ilm.ItemId = tod.ItemId
					--AND ilm.LocationId = @SourceLocationId
					--AND ilm.BucketId = tod.FromBucketId
					--AND ilm.BatchNo = tod.BatchNo
					--WHERE ilm.ItemId = tod.ItemId
					--UPDATE InventoryLocation_Master SET Quantity = ilm.Quantity - tod.Quantity, AvailableQuantity = ilm.AvailableQuantity - tod.Quantity 
					--FROM InventoryLocation_Master ilm
					--INNER JOIN (
					--			SELECT ItemId, SUM(Quantity) Quantity, FromBucketId
					--			FROM #ItemDetail
					--			GROUP BY ItemId, Quantity, FromBucketId
					--			) tod
					--ON ilm.ItemId = tod.ItemId
					--AND ilm.LocationId = @SourceLocationId
					--AND ilm.BucketId = tod.FromBucketId
					--WHERE ilm.ItemId = tod.ItemId
					
					
				END
			
			
--					If(@StatusId=3 AND @ReasonCode  NOT IN ('10', '11'))
--					BEGIN
----						SELECT '1'
--						UPDATE InventoryLocation_Master 
--						SET Quantity = ilm.Quantity + tod.Quantity, AvailableQuantity = ilm.AvailableQuantity + tod.Quantity
--						FROM InventoryLocation_Master ilm					
--						INNER JOIN (
--									SELECT Itemid, SUM(Quantity) Quantity, ToBucketId
--									FROM #ItemDetail
--									GROUP BY Itemid, Quantity, ToBucketId
--									) tod
--						ON ilm.ItemId = tod.Itemid
--						AND ilm.LocationId = @SourceLocationId
--						AND ilm.BucketId = tod.ToBucketId
						
						
--						INSERT INTO InventoryLocation_Master 
--							(LocationId, ItemId, BucketId, Quantity, AvailableQuantity)
--						SELECT  @SourceLocationId, ItemId, ToBucketId, Quantity, Quantity FROM #ItemDetail tod
--						WHERE NOT EXISTS (SELECT 1 FROM InventoryLocation_Master b where b.ItemId = tod.ItemId AND b.BucketId = tod.ToBucketId AND b.LocationId = @SourceLocationId)


--					END
					
				-- ADD Quantity into - TO BUCKET
				-- If Already Exists Then Update
				DECLARE @ReasonId  as INT
				--SELECT @ReasonId  = KeyCode1 FROM Parameter_Master WHERE ParameterCode='REASONID'
				select @ReasonId = ReasonCode from #ItemDetail
				
	
				
			declare @cnt int
			select @cnt=  Count(1) FROM Inventory_LocBucketBatch ilm inner join #ItemDetail tod on  ilm.ItemId = tod.ItemId AND ilm.LocationId = @SourceLocationId AND ilm.BucketId = tod.ToBucketId AND ilm.BatchNo = tod.BatchNo

		IF @StatusId = 3 
			BEGIN
		
--				SELECT @StatusId
                  	DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				INSERT INTO InventoryAdj_DetailEntry (AdjustmentNo, RowNo, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemid,ToBatchNo)
				SELECT  @SeqNo, RowId, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemId,ToBatchNo FROM #ItemDetail
	
			END

		SELECT AdjustmentNo SeqNo,
				Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate
		FROM InventoryAdj_Entry 
		WHERE AdjustmentNo = @SeqNo
    COMMIT 
	END TRY
	BEGIN CATCH
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		Select @outParam OutParam
		ROLLBACK
		RETURN ERROR_NUMBER()
	END CATCH
END




-----------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[sp_InventorySave]    Script Date: 02/09/2014 13:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_InventorySave]
			@jsonForItems nvarchar(max),
			@ApprovedBy		INT,			
			@SeqNo				VARCHAR(20),			@ApprovedDate	VARCHAR(20),
			@LocationId			INT,					@UserId			INT,
			@InitiatedDate		VARCHAR(20),			
			@StatusId			INT  ,					
			@ModifiedBy			INT,		
			@CreatedDate		VARCHAR(20),
			@ModifiedDate		VARCHAR(20),
			@SourceLocationId	INT,
			@tempIsInternalBatchAdj int	,
			@outParam    varchar(200) output		
			
			
AS
Begin
  set nocount on
	Begin Try
	      Begin Transaction
			Declare @RetSeqNo VARCHAR(200)
			,@iHnd int,@RecCnt			VARCHAr(2000), @ReasonCode varchar(10),@LocationCode varchar(10),@LocationType varchar(10)	
			Set @iHnd = 0

		
	--Added for storing stock adjustment initiation
		Declare @InitiateStockAdjustment	VARCHAR(10),@MyHierarchy JSONHierarchy,
			@xml XML ,@inputParam varchar(500)

		Exec sp_xml_preparedocument @iHnd OutPut, @inputParam
        
			           INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@MyHierarchy)
		                  
		
		
		                  
							SELECT 
						
								
							  b.value('@ItemId', 'int') as ItemId,
							  b.value('@ItemCode', 'varchar(20)') as ItemCode,
							  b.value('@ItemName', 'varchar(50)') as ItemDescription,
						
							  b.value('@FromBucketId', 'varchar(30)') as FromBucketId,
							  b.value('@ToBucketId', 'varchar(30)') as ToBucketId,
							  b.value('@Quantity', 'int') as Quantity,
							  b.value('@ReasonCode', 'varchar(50)') as ReasonDescription,
							  b.value('@ApprovedQty', 'int') as ApprovedQty,
							  b.value('@BatchNo', 'Varchar(20)') as ManufactureBatchNo,
							  b.value('@UOMId', 'int') as UOMId,
							  b.value('@ReasonId', 'int') as ReasonCode,
							  b.value('@MfgDate', 'Varchar(20)') as MfgDate,
							  b.value('@ExpDate', 'Varchar(20)') as ExpDate,
							  b.value('@BatchId', 'varchar(20)') as BatchNo,
							  b.value('@ToItemid', 'int') as ToItemid,
							  b.value('@ToBatchNo', 'Varchar(20)') as ToBatchNo,
								b.value('@ToManufactureBatchNo', 'Varchar(20)') as ToManufactureBatchNo,
								b.value('@ToItemName', 'Varchar(50)') as ToItemName
								
								into #ItemDetail 
							  FROM @xml.nodes('/root/item') a(b)                                 
		               
		               alter table  #ItemDetail add  RowId Int Identity(1,1)  ; 
			           
			           
	

	
	
--Select '1', * From #ItemDetail
		select @ReasonCode = ReasonCode from #ItemDetail 

		
		--SELECT @InitiateStockAdjustment = Value from dbo.ufn_StringListSplit(@seqNo,'/') where value = 'HO'
		SELECT @InitiateStockAdjustment = Value from dbo.ufn_StringListSplit(@seqNo,'/') where value in (select top 2 value from dbo.ufn_StringListSplit(@seqNo,'/'))
		IF ISNULL(NULLIF(@SeqNo,''),'-1')='-1'
		BEGIN
			EXECUTE usp_GetSeqNo 'SA', @LocationId, @RetSeqNo Out
			SET @SeqNo=@RetSeqNo
			
			INSERT INTO dbo.InventoryAdj_Entry	(AdjustmentNo,	LocationId,	CreatedBy,		CreatedDate,	ModifiedBy,	ModifiedDate,IsInternalBatchAdj)
				VALUES				(@SeqNo,	@SourceLocationId,	@ModifiedBy,	GETDATE(),	@ModifiedBy,GETDATE(),@tempIsInternalBatchAdj)		
			set @StatusId=1	
		END						
		ELSE 
		BEGIN
			-- Check Concurrency, Get DBDate
			DECLARE @DBDate DATETIME
			SELECT @DBDate = (SELECT ModifiedDate = (CASE WHEN ModifiedBy Is Not Null THEN ModifiedDate ELSE CreatedDate END) FROM InventoryAdj_Entry WHERE  AdjustmentNo = @SeqNo)

			DECLARE @PreInitiatedDate AS DATETIME 
			SELECT @PreInitiatedDate = [Date] FROM InventoryAdj_Entry WHERE AdjustmentNo = @SeqNo

			DECLARE @SourceLocationCode AS VARCHAR(20)
			SELECT @SourceLocationCode = LocationCode FROM Location_Master lm WHERE lm.LocationId = @SourceLocationId
			--Initiated, Approved Or Rejected
		
			--Select @StatusId
			
end
-----------------------------------------------------------------------------
IF @StatusId = 1 OR @StatusId = 2
			BEGIN
				UPDATE InventoryAdj_Entry
				SET	
					ModifiedBy			=	@ModifiedBy,		
					ModifiedDate		=	getDate()	,
					Date				=   CASE WHEN @StatusId = 2 THEN @InitiatedDate ELSE NULL END ,
					UserId				=   CASE WHEN @StatusId = 2 THEN @ModifiedBy ELSE NULL END
				WHERE AdjustmentNo = @SeqNo
					DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				INSERT INTO InventoryAdj_DetailEntry (AdjustmentNo, RowNo, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemid,ToBatchNo)
				SELECT  @SeqNo, RowId, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemId,ToBatchNo FROM #ItemDetail
	
		    END
			ELSE IF @StatusId = 3 OR @StatusId = 4
			BEGIN
				
				UPDATE InventoryAdj_Entry
				SET	
					ModifiedBy				=	@ModifiedBy,		
					ModifiedDate			=	GETDATE()	,
					ApprovedRejectedDate	=   CASE WHEN @StatusId = 3 OR @StatusId = 4 THEN GETDATE() ELSE NULL END,
					ApprovedBy				=   CASE WHEN @StatusId = 3 THEN @ModifiedBy ELSE NULL END,
					RejectedBy				=   CASE WHEN @StatusId = 4 THEN @ModifiedBy ELSE NULL END,
					UserId					=   CASE WHEN @StatusId = 3 THEN @ModifiedBy ELSE UserId END
					
				WHERE AdjustmentNo = @SeqNo
--				SELECT @StatusId
                  	DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				INSERT INTO InventoryAdj_DetailEntry (AdjustmentNo, RowNo, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemid,ToBatchNo)
				SELECT  @SeqNo, RowId, ItemId, FromBucketId, ToBucketId, BatchNo, Quantity, ReasonCode, ApprovedQty, ReasonDescription,ToItemId,ToBatchNo FROM #ItemDetail
	
			END
			ELSE IF @StatusId = 5 -- Cancel
			BEGIN
				SET @outParam = '8004'
		
				-- As per discussion with Lalit, 01-09-2009, We'll remove the code from Table
				DELETE FROM InventoryAdj_DetailEntry WHERE AdjustmentNo = @SeqNo
				DELETE FROM InventoryAdj_Entry WHERE AdjustmentNo = @SeqNo
				Select @SeqNo SeqNo
				 
				ROLLBACK
				RETURN 
			END

--				SELECT 'tEST', 'uPLODATE lOCATION1', @StatusId
				
			IF (@StatusId = 1 )
			BEGIN 
					DECLARE @Item AS INT ,@ToItem AS INT,@ToBatch AS VARCHAR(20), @ToMfgBatchNo AS VARCHAR(20),@Reason AS VARCHAR(10),@MfdDate AS VARCHAR(50),@ExdDate AS VARCHAR(50)
					DECLARE @Count AS INT
					DECLARE @mfgDate AS DATETIME
					DECLARE @ExpDate AS DATETIME
					DECLARE @MRP AS MONEY
					DECLARE @Batch_New	AS VARCHAR(20)
					DECLARE @BatchTest INT

					DECLARE Entry_Cursor  CURSOR FOR SELECT ItemId,ToItemId,ToBatchNo,ToManufactureBatchNo,ReasonCode,MfgDate,ExpDate FROM  #ItemDetail	
					OPEN Entry_Cursor 
					FETCH next from Entry_Cursor INTO @Item,@ToItem,@ToBatch,@ToMfgBatchNo,@ReasonCode, @MfdDate,@ExdDate
					While @@FETCH_STATUS =0
					Begin
							if(@MfdDate IS NOT NULL AND @ExdDate IS NOT NULL) AND @ReasonCode  IN ('10', '11') 
							BEGIN
							
								Select @BatchTest = count(1) from itembatch_detail where BatchNo=@ToBatch AND  itemid = @ToItem 
													AND (datediff(dy,convert(varchar(30),MfgDate,102),convert(varchar(30),@MfdDate,110))='0') 
													AND (datediff(dy,convert(varchar(30),ExpDate,102),convert(varchar(30),@ExdDate,110))='0')
							
								select @MRP = MRP from ITEMMRP where itemid = @Item
						 
								IF(@BatchTest=0)
								BEGIN
--									SELECT '3'
										DECLARE @LocationCode1 varchar(20)
										SET @LocationCode1 =( select LocationCode From Location_Master Where LocationID = @LocationId)
										Declare @SeqNo1 Varchar(20)  
										Execute usp_GetSeqNo 'BAT', @locationID, @SeqNo1 Out
										Declare @p AS INT  
										SET @p = CAST(@SeqNo1 AS INT) 
																
										Set @Batch_New ='BAT/'+@LocationCode1+'/'+REPLICATE('0',6 - LEN(@p))+cast(@p as Varchar) 

										--IF NOT EXISTS (SELECT 1 FROM ItemBatch_Detail ibd WHERE ibd.BatchNo = @Batch_New)
										INSERT INTO ItemBatch_Detail VALUES(@Batch_New,@ToItem,@Batch_New,@MfdDate,@ExdDate,@MRP,@ToMfgBatchNo,'1', @ModifiedBy,GETDATE(),@ModifiedBy,getdate())
										UPDATE #ItemDetail set ToBatchNo = @Batch_New where toitemid = @toItem
						
--										SELECT 'TEST', * FROM #ItemDetail
									END
									
							END
					Fetch Next from Entry_Cursor into @Item,@ToItem,@ToBatch,@ToMfgBatchNo,@ReasonCode , @MfdDate,@ExdDate
				END
					CLOSE Entry_Cursor
					DEALLOCATE Entry_Cursor
			END
			
			
			IF ( @StatusId = 2 )-- StatusId = 2 For Initiate at BO, StatusId = 3, Approved At HO
			BEGIN


				IF EXISTS (SELECT ResultValue From (SELECT CASE WHEN id.Quantity > ilb.Quantity THEN 1 ELSE 0 END ResultValue FROM (SELECT SUM(Quantity) Quantity, LocationId, ItemId, BucketId, BatchNo FROM Inventory_LocBucketBatch ilb
											GROUP BY LocationId, ItemId, BucketId, BatchNo) ilb 
				INNER JOIN (SELECT SUM(Quantity) Quantity, ItemId, FromBucketId, BatchNo FROM #ItemDetail id
				GROUP BY ItemId, FromBucketId, BatchNo) id 
				ON id.FromBucketId = ilb.BucketId AND id.BatchNo = ilb.BatchNo AND ilb.LocationId = @LocationId
				AND id.ItemId = ilb.ItemId) AS RESULT WHERE ResultValue = 1)
				BEGIN
					SET @outParam = 'VAL0022'
					Select @outParam OutParam
					rollback
					RETURN 
				END
           END
				-- Move Quantity from FromBucket To ToQuantity
				-- Remove Quantity from - FROM BUCKET
				-----------------------------------------------------------------------------------------------
			    	
						
				If (@StatusId = 3)
				BEGIN
					
                   DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempFromBucketId INT ,@tempQuantity Numeric(18,4),@tempToBucketId INT
            
           DECLARE INVLOCBUCKBATCH CURSOR FOR SELECT BatchNo,ItemId, Quantity, FromBucketId, ToBucketId
						                      FROM #ItemDetail
                       
                        OPEN INVLOCBUCKBATCH  
            FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempItemID,@tempQuantity  ,@tempFromBucketId,@tempToBucketId
                                 
                            WHILE @@FETCH_STATUS = 0  
                              BEGIN  
                                EXEC sp_updateInventoryInStockLedger 7,@SeqNo,@LocationId,@tempItemID,@tempBatchNo,@tempQuantity,@tempToBucketId,  
                            0.00 ,0,@outParam OUT 
                            Set @tempQuantity=-@tempQuantity;
                            EXEC sp_updateInventoryInStockLedger 8,@SeqNo,@LocationId,@tempItemID,@tempBatchNo,@tempQuantity,@tempFromBucketId,  
                            0.00 ,0,@outParam OUT  
                              
                            if @outParam!=''  
                                   BEGIN  
                                   SELECT @outParam OutParam  
                                     CLOSE INVLOCBUCKBATCH  
                       
                                     DEAllOCATE INVLOCBUCKBATCH  
                                   ROLLBACK  
                                   RETURN  
                                   END  
                             FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempItemID,@tempQuantity  ,@tempFromBucketId,@tempToBucketId
                              END  
                                
                              CLOSE INVLOCBUCKBATCH  
                                
                              DEAllOCATE INVLOCBUCKBATCH  
         
					--UPDATE Inventory_LocBucketBatch SET Quantity = ilm.Quantity - tod.Quantity
					--FROM Inventory_LocBucketBatch ilm
					--INNER JOIN (
					--			SELECT ItemId, Quantity, BatchNo, ManufactureBatchNo, FromBucketId, ToBucketId
					--			FROM #ItemDetail
					--			GROUP BY ItemId, Quantity, BatchNo, ManufactureBatchNo, FromBucketId, ToBucketId
					--			) tod
					--ON ilm.ItemId = tod.ItemId
					--AND ilm.LocationId = @SourceLocationId
					--AND ilm.BucketId = tod.FromBucketId
					--AND ilm.BatchNo = tod.BatchNo
					--WHERE ilm.ItemId = tod.ItemId
					--UPDATE InventoryLocation_Master SET Quantity = ilm.Quantity - tod.Quantity, AvailableQuantity = ilm.AvailableQuantity - tod.Quantity 
					--FROM InventoryLocation_Master ilm
					--INNER JOIN (
					--			SELECT ItemId, SUM(Quantity) Quantity, FromBucketId
					--			FROM #ItemDetail
					--			GROUP BY ItemId, Quantity, FromBucketId
					--			) tod
					--ON ilm.ItemId = tod.ItemId
					--AND ilm.LocationId = @SourceLocationId
					--AND ilm.BucketId = tod.FromBucketId
					--WHERE ilm.ItemId = tod.ItemId
					
					
				END
			
			
--					If(@StatusId=3 AND @ReasonCode  NOT IN ('10', '11'))
--					BEGIN
----						SELECT '1'
--						UPDATE InventoryLocation_Master 
--						SET Quantity = ilm.Quantity + tod.Quantity, AvailableQuantity = ilm.AvailableQuantity + tod.Quantity
--						FROM InventoryLocation_Master ilm					
--						INNER JOIN (
--									SELECT Itemid, SUM(Quantity) Quantity, ToBucketId
--									FROM #ItemDetail
--									GROUP BY Itemid, Quantity, ToBucketId
--									) tod
--						ON ilm.ItemId = tod.Itemid
--						AND ilm.LocationId = @SourceLocationId
--						AND ilm.BucketId = tod.ToBucketId
						
						
--						INSERT INTO InventoryLocation_Master 
--							(LocationId, ItemId, BucketId, Quantity, AvailableQuantity)
--						SELECT  @SourceLocationId, ItemId, ToBucketId, Quantity, Quantity FROM #ItemDetail tod
--						WHERE NOT EXISTS (SELECT 1 FROM InventoryLocation_Master b where b.ItemId = tod.ItemId AND b.BucketId = tod.ToBucketId AND b.LocationId = @SourceLocationId)


--					END
					
				-- ADD Quantity into - TO BUCKET
				-- If Already Exists Then Update
				DECLARE @ReasonId  as INT
				--SELECT @ReasonId  = KeyCode1 FROM Parameter_Master WHERE ParameterCode='REASONID'
				select @ReasonId = ReasonCode from #ItemDetail
				
	
				
			declare @cnt int
			select @cnt=  Count(1) FROM Inventory_LocBucketBatch ilm inner join #ItemDetail tod on  ilm.ItemId = tod.ItemId AND ilm.LocationId = @SourceLocationId AND ilm.BucketId = tod.ToBucketId AND ilm.BatchNo = tod.BatchNo

		

		SELECT AdjustmentNo SeqNo,
				Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate
		FROM InventoryAdj_Entry 
		WHERE AdjustmentNo = @SeqNo
        commit
	END TRY
	BEGIN CATCH
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		Select @outParam OutParam
		rollback
		RETURN ERROR_NUMBER()
	END CATCH
END




-----------------------------------------------------------------------------
select * from TOI_Detail where TOINumber='I/HO/13/04251'
GO
/****** Object:  StoredProcedure [dbo].[Sp_NewDistributorCustomize_Report]    Script Date: 02/09/2014 13:05:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_NewDistributorCustomize_Report]              
 @distributorid varchar(8),  
 @levelid int             
AS            
BEGIN              
select   a.DistributorId, LTRIM(RTRIM(b.distributorfirstname))+' '+LTRIM(RTRIM(b.distributorlastname)) as DistributorName   , dbo.fnGetDistributorName(@distributorid) as Upline,a.uplineid,distributormobnumber,distributoremailid ,distributorregistrationdate as 'JoiningDate',Bonuspercent,a.PrevCumPV ,a.exclpv ,a.selfpv ,a.grouppv  ,a.levelid     
,'Designation'=    
case a.levelid    
      when '0' THEN 'NON QUALIFIED DIRECTOR'    
      when '1' THEN 'DISTRIBUTOR'    
      when '2' THEN 'SENIOR DISTRIBUTOR'    
      when '3' THEN 'ASST. DIRECTOR'    
      WHEN '4' THEN 'DIRECTOR'    
      WHEN '5' THEN 'SILVER DIRECTOR'    
      WHEN '6' THEN 'GOLD DIRECTOR'    
      WHEN '7' THEN 'STAR DIRECTOR'    
      WHEN '8' THEN 'DIAMOND DIRECTOR'    
      WHEN '9' THEN 'CROWN DIRECTOR'    
      WHEN '10' THEN 'UNIVERSAL CROWN DIRECTOR'    
      WHEN '11' THEN 'DOUBLE CROWN'    
      WHEN '12' THEN 'DOUBLE UNIVERSAL CROWN'    
END    
from DistributorBusiness_Current(NOLOCK) a inner join DistributorMaster(NOLOCK) b    
on a.distributorid=b.distributorid            
where a.UplineId=@distributorid and b.DistributorStatus=2 and a.levelid between 0 and @levelid       
end
GO
/****** Object:  StoredProcedure [dbo].[SP_MerchdisingHirarachyForPromotion]    Script Date: 02/09/2014 13:05:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Procedure [dbo].[SP_MerchdisingHirarachyForPromotion]
	@locationId int,
	@outParam	VARCHAR(500) OUTPUT	
AS
Begin
	set nocount on ;
	

 DECLARE @TempTable  TABLE (
		 ItemId varchar(20),
		 PromotionID varchar(1000) 
		 )
       DECLARE @promotionId Int ;
       Declare @ConditionCode int
		BEGIN TRY
		
          
				DECLARE ORDERCUROR CURSOR FOR SELECT pll.PromotionId,
             pc.ConditionCode   
					   FROM promotionLocation_Link pll 
					 LEFT JOIN promotion_master PM
					   ON pll.promotionId   = pm.promotionId 
					  LEFT JOIN promotion_condition pc
					 ON pll.promotionId   = pc.promotionId 
					  LEFT JOIN Promotion_Tier pt 
					 ON 
					  pt.PromotionId=pll.PromotionId and pt.Status=1 
      
					  WHERE pll.locationId  = @locationId--(SELECT LocationId FROM Location_Master WHERE LocationCode = @iLocationCode)
					  AND pll.status   = 1  AND pm.status = 1 ANd pc.Status=1 
  
						AND GETDATE() BETWEEN pm.StartDate AND pm.EndDate 
     
				     GROUP BY pll.PromotionId, pc.ConditionCode  			
		 
		  
		    OPEN ORDERCUROR
				FETCH NEXT FROM ORDERCUROR INTO @promotionId,@ConditionCode
										
					WHILE @@FETCH_STATUS = 0
		      
						BEGIN
								
							INSERT INTO @TempTable SELECT *,@promotionId   from [dbo].ReturnQtyPromoItems(@ConditionCode)	
							FETCH NEXT FROM ORDERCUROR INTO  @promotionId,@ConditionCode
				
						END
						
			CLOSE ORDERCUROR 
			DEALLOCATE ORDERCUROR
		

						SELECT PromotionID,
						ItemId=STUFF((SELECT ','+ ItemId FROM @TempTable  t2 where t1.PromotionID=t2.PromotionID  FOR XML PATH ('')),1,1,'')
						 from @TempTable t1 GROUP BY PromotionID
		END TRY
		BEGIN CATCH
			CLOSE ORDERCUROR 
			DEALLOCATE ORDERCUROR
			Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
			Return ERROR_NUMBER()
			Rollback 
		END CATCH	  
					
							
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PackSave]    Script Date: 02/09/2014 13:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery41.sql|7|0|C:\Users\m\AppData\Local\Temp\~vsDC37.sql

CREATE  Procedure [dbo].[sp_PackSave]
		@CompositeItemId int,
		@DBQuantity numeric(18,4),
		@Remarks Varchar(100),
		@CreatedBy int	,
		@locationId int,
		@BucketId int,
				@outParam	Varchar(500) Output
As
Begin
SET NOCOUNT ON
	Begin Try
--Global Varibale----

		DECLARE @iHnd int,@SysDate DateTime,@RecCnt varchar(500),@MinDate DateTime;
			Declare @BatchNo Varchar(20)
			Declare @IsBatchExist Int
			Declare @IsPackExist Int
			Declare @ReasonId Int
			DECLARE @RetSeqNo Varchar(20)
			DECLARE @LocationCode VARCHAR(20)
			DECLARE @SeqNo Varchar(20),@PUNo varchar(200)
		
       --------------Query execute for Expiry date of item to be unpack
	SELECT @MinDate=IsNull(Min(expDate),Getdate())
							FROM  Item_Master ITI
										Inner Join ItemLocation_Link ILL
											ON  ITI.ItemId=ILL.ItemId And
												ITI.IsComposite=0
										Inner Join CompositeBOM COB
											 ON  ITI.ItemId=COB.ItemId
										INNER JOIN ItemBatch_Detail ITB
				                              ON  ITI.ItemId=ITB.ItemId 
						Where  ITB.ExpDate>GETDATE()   And CompositeItemID=@CompositeItemId
    
		Set @SysDate = GetDate()		
	
	
		Set @iHnd = 0


         EXECUTE usp_GetSeqNo 'BAT1', @LocationId, @RetSeqNo Out
				    SET @BatchNo= @RetSeqNo 
		Declare @TCompositeItem Table   
		(
			ItemId Int,
			ItemName Varchar(100),
			PackUnpackQty Float,
			MRP Float,
			MfgDate varchar(30),
			ExpDate varchar(30),
			LocationId int,
			MfgNo Varchar(20)
		)
   INSERT INTO @TCompositeItem SELECT @CompositeItemId,ItemName 
   ,@DBQuantity,DistributorPrice,GETDATE(),@MinDate,@locationId,@BatchNo
   FROM Item_Master where ItemId=@CompositeItemId
   
    
    
    select @CompositeItemId =ItemId ,@DBQuantity =PackUnpackQty from @TCompositeItem
		Declare  @TCompositeBOM Table
		(
		    SeqNo    INT identity(1,1), 
			ItemId Int,
			CompositeItemID int,
			TotalQty numeric(18,4),
			LocationId int
		)
    ---------- Here multiply BOM items To DBQuantity For Batch Item Deduction.........
    
				insert into @TCompositeBOM select  CBOM.ItemId,CBOM.CompositeItemID,CBOM.Quantity * @DBQuantity,@locationId from
				 CompositeBOM CBOM where CompositeItemID=@CompositeItemId

		Declare @After_Adjustment Table 
		(
			ItemBatchId varchar(20),
			RowNo   INT identity(1,1), 
			ItemId Int,
			AvailableQty Numeric(12,4),
			RequestQty	Numeric(12,4),
			BatchNo Varchar(20),
			AfterAdjustQty Numeric(12,4)
		)
   
				
		Declare  @PUHeader Table
		(
				PUNo Varchar(20),
				CompositeItemID int,
				Quantity numeric(18,4),
				BucketId  int,
				Remarks Varchar(100),
				IsPackVoucher bit,
				LocationId	int,
				PUDate   Varchar(30),
				CreatedBy int		
		)
 

	    INSERT INTO @PUHeader
        Select 	@PUNo,@CompositeItemId,@DBQuantity,
				@BucketId,@Remarks,0,@LocationId,GETDATE(),@CreatedBy    
		
		
		
		
	---Variable used in Cursor--

         Declare @TotalAvailableQty numeric(18,4)
         DEClare @OutCompositeItemId int
		 DEClare @OutItemId int
		 DEClare @OutTotalQty numeric(18,4)
         DEClare @OutLocationId int

		 DECLARE CurOutComBOM CURSOR FOR 
		 Select CompositeItemId,ItemId,TotalQty,LocationId 
		 From @TCompositeBOM

		 OPEN CurOutComBOM
		 FETCH NEXT FROM CurOutComBOM
			 INTO @OutCompositeItemId,@OutItemId, @OutTotalQty, @OutLocationId
							
		 WHILE @@FETCH_STATUS = 0
			BEGIN
				Select @TotalAvailableQty = ISNULL(SUM(Quantity),0)
				From ItemBatch_Detail ib 
					INNER Join Inventory_LocBucketBatch bwi
						On bwi.ItemId = ib.ItemId AND  
						bwi.BatchNo = ib.BatchNo
					INNER Join Bucket buc
						On bwi.BucketId = buc.BucketId AND 
						   buc.Status = 1 AND 
						   buc.ParentId Is Not Null AND
						   Sellable=1 
			   WHERE bwi.LocationId = @OutLocationId AND 
					 bwi.ItemId = @OutItemId AND 
					 bwi.Status = 1 AND 
					 ib.Status = 1 AND ib.ExpDate>GETDATE()
				AND bwi.Quantity>0
				
					--SELECT @TotalQuantity TOTAL, @ItemQuantity ITEM 
				IF ISNULL(@TotalAvailableQty,0) < @OutTotalQty
					BEGIN
						--Print  CAST(@TotalQuantity As Varchar)+ ', ' +  CAST(@ItemQuantity AS Varchar)
								
								--select Min(a.Quantity) availQuantity  From	(	Select IsNULL (SUM(Quantity),0) Quantity ,BWi.ItemId ItemId 
								--From ItemBatch_Detail ib 
								--	INNER Join Inventory_LocBucketBatch bwi
								--		On bwi.ItemId = ib.ItemId AND  
								--		bwi.BatchNo = ib.BatchNo
								--	INNER Join Bucket buc
								--		On bwi.BucketId = buc.BucketId AND 
								--		   buc.Status = 1 AND 
								--		   buc.ParentId Is Not Null AND
								--		   Sellable=1 
							 --  WHERE bwi.LocationId = @locationId AND 
								--	 bwi.ItemId in ( select ItemId from @TCompositeBOM) AND 
								--	 bwi.Status = 1 AND 
								--	 ib.Status = 1
								--AND bwi.Quantity>0 Group By bwi.ItemId )  a
				
				
				CLOSE CurOutComBOM
						DEALLOCATE CurOutComBOM

						select 'VAL0057' as OutParam
						SET @outParam = 'VAL0057'
						Return
					END

				DECLARE @ConsumedQuantity AS Numeric(12,4)
				DECLARE @Inserted BIT
				DECLARE @First BIT
				DECLARE @InBuketQuantity numeric(12,4)
				Declare @InBatchNo Varchar(20)			
				DECLARE @InitemBatchId Varchar(20)
				Declare @InManufactureBatchNo Varchar(20)

				SET @ConsumedQuantity = 0
				SET @First = 0 
				SET @Inserted = 0
				SET @InBuketQuantity=0

					DECLARE CurINComBOM CURSOR FOR 
						Select ItemBatchId, ib.BatchNo, Quantity, ManufactureBatchNo
						From ItemBatch_Detail ib 
							INNER Join Inventory_LocBucketBatch bwi
								On bwi.ItemId = ib.ItemId AND  
								bwi.BatchNo = ib.BatchNo
							INNER Join Bucket buc
								On bwi.BucketId = buc.BucketId AND 
								   buc.Status = 1 AND 
								   buc.ParentId Is Not Null AND
								   Sellable=1 
					   WHERE bwi.LocationId = @OutLocationId AND 
							 bwi.ItemId = @OutItemId AND 
							 bwi.Status = 1 AND 
							 ib.Status = 1
							 AND
							 ib.ExpDate>GETDATE()
							 AND bwi.Quantity>0
							 
					   ORDER BY  
							   MfgDate Asc
				
					OPEN CurINComBOM
					FETCH NEXT FROM CurINComBOM
						INTO @InitemBatchId,@InBatchNo,@InBuketQuantity,@InManufactureBatchNo

					WHILE @@FETCH_STATUS = 0
						BEGIN 	
							SET @ConsumedQuantity = @ConsumedQuantity + @InBuketQuantity

							If (@ConsumedQuantity>@OutTotalQty AND @Inserted = 0)
								BEGIN
									SET @Inserted = 1 
									Declare @AdjustQty As Numeric (12,4) 
									SET @AdjustQty = 0 

									SELECT @AdjustQty = SUM(AvailableQty) 
									FROM @After_Adjustment 
									WHERE ItemBatchId <> @INItemBatchId AND 
											ItemId = @OutItemId AND 
											ItemBatchID In (Select ItemBatchId 
															From @After_Adjustment)

									DECLARE @Qty  AS NUMERIC(12,4)
									SET @Qty = 0 

									IF @First = 0 
										SET @Qty = 	@OutTotalQty 
									ELSE IF @First = 1 
										SET @Qty = 	@OutTotalQty - @AdjustQty 
						
									INSERT INTO @After_Adjustment 
											   (ItemBatchID,ItemId,BatchNo,RequestQty,AfterAdjustQty) 
									SELECT	ItemBatchID, @OutItemId,BatchNo,@OutTotalQty,@Qty
									FROM ItemBatch_Detail 
									WHERE ItemBatchId = @INItemBatchId AND 
										  ItemId = @OutItemId

								END
							ELSE If (@ConsumedQuantity<=@OutTotalQty)
								BEGIN
									SET @First = 1 
									INSERT INTO @After_Adjustment
											  (ItemBatchId, ItemId,AvailableQty,
											   BatchNo,RequestQty,AfterAdjustQty) 
									VALUES (@InItemBatchId, @OutItemId,@InBuketQuantity, 
											@InBatchNo, @OutTotalQty,@InBuketQuantity)
								END
						
						FETCH NEXT FROM CurINComBOM
							INTO @InItemBatchId,@InBatchNo,@InBuketQuantity,@InManufactureBatchNo
					END
					CLOSE CurINComBOM
					DEALLOCATE CurINComBOM

			    FETCH NEXT FROM CurOutComBOM
					INTO @OutCompositeItemId,@OutItemId, @OutTotalQty, @OutLocationId
	
			END
			CLOSE CurOutComBOM
			DEALLOCATE CurOutComBOM	
 BEGIN Transaction 
			UPDATE @After_Adjustment 
			SET AvailableQty = ILB.Quantity
			FROM @After_Adjustment AA
				INNER JOIN Inventory_LocBucketBatch ILB ON
					AA.ItemId = ILB.ItemId AND 
					AA.BatchNo = ILB.BatchNo AND 
					LocationId = @LocationId
				INNER Join Bucket buc ON
					ILB.BucketId = buc.BucketId AND 
					buc.Status = 1 AND 
					buc.ParentId Is Not Null AND
					Sellable=1
			WHERE AA.AvailableQty IS NULL

---select * from @After_Adjustment

---Update Inventory_LocBucketBatch WITH FIFO Values
			--UPDATE Inventory_LocBucketBatch 
			--SET Quantity =ILB.Quantity-AA.AfterAdjustQty
			--FROM Inventory_LocBucketBatch ILB 
			--	INNER JOIN @After_Adjustment AA ON
			--		AA.ItemId = ILB.ItemId AND 
			--		AA.BatchNo = ILB.BatchNo AND 
			--		LocationId = @LocationId
			--INNER Join Bucket buc ON
			--		ILB.BucketId = buc.BucketId AND 
			--		buc.Status = 1 AND 
			--		buc.ParentId Is Not Null AND
			--		Sellable=1
					
				
					

----Block For Updation of Case Pack--
				

	SET @SeqNo=''
		SET @RetSeqNo=''
		EXECUTE usp_GetSeqNo 'PU', @LocationId, @RetSeqNo Out
		
		SELECT @LocationCode = LocationCode 
		FROM LOCATION_MASTER 
		WHERE LocationID = @LocationID
		
		DECLARE @ManBat VARCHAR(50)
		DECLARE @MfgDate Varchar(20)
		DECLARE @ExpDate Varchar(20)
		DECLARE @MRP NUMERIC(10,4)
		SELECT TOP 1 @MfgDate = MfgDate,@ExpDate = ExpDate, @ManBat = MfgNo,@MRP = MRP FROM @TCompositeItem
				
		SET @SeqNo=@LocationCode + '/' + 'P' + '/' + @RetSeqNo	
		
		 DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity Numeric(18,4)
            
           DECLARE INVLOCBUCKBATCH CURSOR FOR SELECT BD.BatchNo,5,BD.ItemId,BD.AfterAdjustQty FROM @After_Adjustment BD  	
                       
                        OPEN INVLOCBUCKBATCH  
            FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity  
                                 
                            WHILE @@FETCH_STATUS = 0  
                              BEGIN  
                            Set @tempQuantity=-@tempQuantity;
                            EXEC sp_updateInventoryInStockLedger 11,@SeqNo,@locationId,@tempItemID,@tempBatchNo,@tempQuantity,@tempBucketId,  
                            0.00 ,0,@outParam OUT  
                              
                            if @outParam!=''  
                                   BEGIN  
                                   SELECT @outParam OutParam  
                                     CLOSE INVLOCBUCKBATCH  
                       
                                     DEAllOCATE INVLOCBUCKBATCH  
                                   ROLLBACK  
                                   RETURN  
                                   END  
                            FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity  
                              END  
                                
                              CLOSE INVLOCBUCKBATCH  
                                
                              DEAllOCATE INVLOCBUCKBATCH  
         
		
		INSERT INTO ItemBatch_Detail
						   (ItemBatchId,ItemId,BatchNo,MfgDate,ExpDate,MRP
						   ,ManufactureBatchNo,Status,CreatedBy,CreatedDate
						   ,ModifiedBy,ModifiedDate)
					 Select @BatchNo,ItemId,@BatchNo,MfgDate,ExpDate,MRP
						   ,@BatchNo,1,@CreatedBy,@Sysdate,@CreatedBy,@Sysdate
					 FROM @TCompositeItem
		
      EXEC sp_updateInventoryInStockLedger 10,@SeqNo,@locationId,@CompositeItemId,@BatchNo,@DBQuantity,5,  
                            0.00 ,0,@outParam OUT			
						
	
		

		INSERT INTO PU_Header
			(PUNo,PUDate,CompositeItemId,Quantity,
			 BucketId,BatchNo,PU_Flag,Remarks,LocationId,
			 Status,ManufactureBatchNo, MRP,MfgDate,ExpDate, CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select @SeqNo,Convert(datetime,PUDate),CompositeItemID,Quantity,
			   BucketId,@BatchNo,1,Remarks,LocationId,
			   1,@BatchNo,@MRP , @MfgDate ,@ExpDate , @CreatedBy,@SysDate,@CreatedBy,@SysDate
		FROM @PUHeader

		
			
		INSERT INTO PU_Detail
			(PUNo,ItemId,SeqNo,Quantity,
			 Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select @SeqNo,ItemId,SeqNo,TotalQty,
			   1,@CreatedBy,@SysDate,@CreatedBy,@SysDate
		FROM @TCompositeBOM

				   
		Insert Into PU_BatchDetail
			(PUNo,RowNo,ItemId,BucketId,BatchNo,
			 Quantity,Status,CreatedBy,CreatedDate)
		Select @SeqNo,ROW_NUMBER() OVER(ORDER BY ItemId)As SerialNo,ItemId,@BucketId,BatchNo,
			   AfterAdjustQty,1,@CreatedBy,@SysDate 
		FROM @After_Adjustment
		WHERE AfterAdjustQty > 0
		select @SeqNo AS PUNo
		EXECUTE usp_Interface_Audit '','PACKUNPACK',@LocationCode,'PU_Header',@SeqNo,null,null,null,null,'I',@CreatedBy,@RecCnt Out
		Exec [usp_Interface_Audit] '', 'ITMBATCH', NULL, 'ITEMBATCH_DETAIL', @BatchNo, NULL, NULL, NULL, NULL, 'I',@CreatedBy, @reccnt OUTPUT					
	--- End Execution -----------------------------------------------------------------------------		
	
	    Commit Transaction 
	End Try

	Begin Catch
		If @iHnd <> 0
			Execute sp_xml_removedocument @iHnd
		Set @outParam = '30001: ' + ' - ' +
						'Error Number: ' + Cast(Error_Number() As Varchar(50)) + ', ' +
						'Error Message: ' + Error_Message() + ', ' +
						'Error Line: ' + Cast(Error_Line() As Varchar(50)) + ', ' +
						'Source: ' + Error_Procedure()
				Select @outParam OutParam
						ROLLBACK
		Return Error_Number()
	End Catch
End
GO
/****** Object:  StoredProcedure [dbo].[SP_orderSave]    Script Date: 02/09/2014 13:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_orderSave]
				( @jsonForItems nvarchar(max),
					@jsonForPayment nvarchar(max),   
				  @courierCharges int, @locationId int,    @stockpoint int, @distributorId int, 
				     @TotalUnit float,
				  @TotalWeight NUMERIC (18,4), @OrderAmount NUMERIC (18,4), @DiscountAmount float,@TaxAmount float,
				  @PaymentAmount NUMERIC (18,4),@ChangeAmount NUMERIC (18,4),
				  @PCId int,@BOId int,@OrderType int,@OrderMode int,@TerminalCode int,@TotalBV Numeric (18,4),
				  @TotalPV NUMERIC (18,4) ,
				  @SourceLocationId int,@TaxJurisdictionId int,@LogNO varchar(20),@createBy int,
				  @addressID int,@orderDispatchMode int,
						@orderId varchar(50),@Remarks varchar(100),@TypeOfUserLoggedIn varchar(2) ,@outParam  varchar(500) output
				)
		   AS
	 
  BEGIN
		BEGIN TRAN
        BEGIN TRY
		  set nocount on --done for PDO compatibilty .Do not remove
		  
		   DECLARE @MyHierarchy JSONHierarchy,
			@xml XML,@payment JSONHierarchy ,
		
			@AddressLine1 varchar(50),
			@AddressLine2 Varchar(50),
			@AddressLine3 varchar(50),
			@AddressLine4 Varchar(50),
			@AddressLine5 varchar(200),
		     @reccnt   VARCHAR(500),
			@FaxNo1 varchar(20),
			@FaxNo2 varchar(20),
			@MobileNo1 varchar(20),
			@MobileNO2 varchar(20),
			@PhoneNo1 varchar(20),
			@PhoneNo2 varchar(20),
			@Email1 varchar(50),
			@Email2 Varchar(50),
			@contryCode int,
			@StateCode int,
			@PinCode int,
			@cityCode int,
			@tempOrderId varchar(20),
			@isFirstOrder int,
			@minimumOrderAmount NUMERIC(18,4)
					  set @AddressLine1='';
					  set @AddressLine5='';
					  set @AddressLine2='';
						set @AddressLine3='';
						  set @AddressLine4='';
		   
		   
		  -----------------------------------------for first order and address of distributor    
		            
			 select  @AddressLine1=isnull(DistributorAddress1,''),@AddressLine2=isnull(DistributorAddress2,''),
				 @AddressLine3=isnull(DistributorAddress3,' '),@AddressLine4=(isnull(DistributorAddress4 ,' '))+' '+ISNULL(CM.CityName,'')+','+ISNULL(SM.StateName,'')+' - '+isnull(Dm.DistributorPinCode,'')   
				 , @isFirstOrder =  FirstOrderTaken
                                     ,
                      @minimumOrderAmount = CASE FirstOrderTaken
                                            WHEN 1 THEN 0
                                            ELSE MinFirstPurchaseAmount
                                          END              
				  from DistributorMaster DM
				  LEFT JOIN City_Master CM ON CM.CityId=DM.DistributorCityCode
				  LEFT JOIN State_Master SM ON SM.StateId=Dm.DistributorStateCode
				   where DistributorId=@distributorId ;
						 set @AddressLine5=@AddressLine1+' '+@AddressLine2+' '+@AddressLine3+' '+@AddressLine4 ;
		  
					  if @OrderMode=2
					   BEGIN
							  If @orderDispatchMode=3   --courier Address for OTHER
							  BEGIN
								 select @AddressLine1=Address1,@AddressLine2=Address2,@AddressLine3=Address3,@AddressLine4=Address4 ,
								@cityCode= CityCode,@pincode=Pincode, @StateCode=StateCode , @contryCode=CountryCode,@PhoneNo1=Phone1,
								@MobileNo1=Mobile1,@MobileNO2=Mobile2 from DistributorAddress where AddressId=@addressID  
							  END
						  else if  @orderDispatchMode=2 -- courier Address for stock point
								BEGIN
								   SELECT 
									  @AddressLine1=isnull([Address1],''),@AddressLine2=isnull([Address2],''),@AddressLine3=isnull([Address3],''),@AddressLine4=isnull([Address4],'')
								   ,@cityCode=LM.[CityId],@pincode=LM.[Pincode],@StateCode=LM.[StateId],@contryCode=LM.[CountryId],
									 @PhoneNo1=[Phone1],@PhoneNo2= [Phone2],@MobileNo1=
									   [Mobile1],@MobileNo2=[Mobile2],@FaxNo1=[Fax1],@FaxNo2= [Fax2],@Email1=[EmailId1],@Email2=[EmailId2]
								 FROM [Location_Master]LM
									  JOIN City_Master CM ON LM.[CityId]=CM.[CityId]
									  JOIN State_Master SM ON LM.[StateId]=SM.[StateId]
									   JOIN Country_Master [CNM] ON LM.[CountryId]=CNM.[CountryId]
									WHERE locationid = @PCId
									
								  AND LM.Status =1 
								END
						  else If @orderDispatchMode=1 or @orderDispatchMode=0-- courier on distributor address
							   BEGIN
							   select   @AddressLine1=dm.DistributorAddress1,@AddressLine2=dm.DistributorAddress2,@AddressLine3=isnull(dm.DistributorAddress3,''),@AddressLine4=isnull(dm.DistributorAddress4,'')
								   ,@cityCode=dM.DistributorCityCode,@pincode=dM.DistributorPinCode,@StateCode=dm.DistributorStateCode,@contryCode=dm.DistributorCountryCode,
									 @PhoneNo1=dm.DistributorTeleNumber,@MobileNo1=dm.DistributorMobNumber,
									   @FaxNo1=dm.DistributorFaxNumber,@Email1=dm.DistributorEMailID    
										from DistributorMaster dm where dm.DistributorId=@distributorId
								END
						END  
				  else if @OrderMode=1 
				   --self
				   BEGIN
										 select   @AddressLine1=isnull([Address1],''),@AddressLine2=isnull([Address2],''),@AddressLine3=isnull([Address3],''),@AddressLine4=isnull([Address4],'')
								   ,@cityCode=LM.[CityId],@pincode=LM.[Pincode],@StateCode=LM.[StateId],@contryCode=LM.[CountryId],
									 @PhoneNo1=[Phone1],@PhoneNo2= [Phone2],@MobileNo1=
									   [Mobile1],@MobileNo2=[Mobile2],@FaxNo1=[Fax1],@FaxNo2= [Fax2],@Email1=[EmailId1],@Email2=[EmailId2]
									  FROM [Location_Master]LM
									  JOIN City_Master CM ON LM.[CityId]=CM.[CityId]
										  JOIN State_Master SM ON LM.[StateId]=SM.[StateId]
											JOIN Country_Master [CNM] ON LM.[CountryId]=CNM.[CountryId]
											  WHERE LM.LocationId=@locationId 
											 AND LM.Status =1 
		           END             
		      
		    if @jsonForItems='[]'
		         BEGIN
		           ROLLBACK
	                  RETURN
		         END                
		  if( @orderId=''  )
            BEGIN
            
            IF @TypeOfUserLoggedIn != 'E'
            BEGIN
				EXECUTE usp_GetSeqNo 'WEB', 10, @orderId OUT --  Location id for all other web based order taken as 10 for generating seq number for order.
            END 
            ELSE 
			BEGIN
				EXECUTE usp_GetSeqNo 'CO', @locationId, @orderId OUT
			
            END
            
			
			 
		    END
	     ELSE     --Case for Modification
	     
	        BEGIN
	  
 	         --PROCEDURE FOR BOTH CANCEL AND MODIFIY
	           EXECUTE  SP_CancelOrder @orderId,3,@distributorId,@SourceLocationId,@createBy,@OrderId out
	        
	             if(@outParam!='')
	                 BEGIN
	                    ROLLBACK
	                  RETURN
	                 END
	        END 
	      
	      
	    if(@LogNo!=null OR @LogNo!='')
	            BEGIN
	              if Exists(Select * From OrderLog where LogNo=@LogNO AND isChangeAddress=2)
	               BEGIN
	                   select @AddressLine1=Address1,@AddressLine2=Address2,@AddressLine3=Address3,@AddressLine4=Address4 ,
								@cityCode= CityCode,@pincode=Pincode, @StateCode=StateCode , @contryCode=CountryCode,@PhoneNo1=Phone1,
								@MobileNo1=Mobile1,@MobileNO2=Mobile2 from OrderLog where LogNo
								=@LogNO  
	               END
	                if Exists(Select * From OrderLog where LogNo=@LogNO AND Status=2)
	               BEGIN
	                  Select 'LOG001' OutParam
	                  RollBack
	                  Return 
	               END
	            END
		   INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@MyHierarchy)
		                  
		                  
							SELECT 
						
							  b.value('@RowID', 'int') as itemID,
							  b.value('@PV', 'float') as PV,
							  b.value('@PromotionParticipation', 'int') as PromotionParticipation,
							  b.value('@MerchHierarchyDetailId', 'int') as MerchHierarchyDetailId,
							  b.value('@GroupItemId', 'int') as GroupItemId,
							  b.value('@BV', 'float') as BV,
							  b.value('@IS', 'varchar(50)') as ISA,
							  b.value('@Qty', 'float') as Qty,
							  b.value('@Name', 'varchar(200)') as Name,
							  b.value('@Price', 'float') as Price,
							  b.value('@IsPromo', 'int') as IsPromo,
							  b.value('@DistributorPrice', 'float') as DistributorPrice,
							  b.value('@PromotionId', 'int')     as  PromotionId,
							  b.value('@PromoDescription', 'varchar(100)') as PromoDescription,
							  b.value('@DiscountPercent', 'float') as DiscountPercent,
								b.value('@DiscountAmount', 'float') as DiscountAmount,
								 b.value('@GiftVoucherNumber', 'varchar(100)') as GiftVoucherNumber,
								b.value('@VoucherSrNo', 'varchar(100)') as VoucherSrNo
								into #temptable 
							  FROM @xml.nodes('/root/item') a(b)                                 
		               
		               alter table  #temptable add  recordNo Int Identity(1,1)  ;
		        declare   @countVouchers varchar(50);
		        select @countVouchers=count(GiftVoucherNumber) from #temptable  where GiftVoucherNumber !='';  
		         if (@countVouchers>0)
		           BEGIN     
		          IF  not EXISTS (SELECT 1 FROM #temptable GDL WHERE GDL.VoucherSrNo !='' and GDL.GiftVoucherNumber 
		           IN (SELECT GiftVoucherNumber FROM GiftVoucher_Distributor_Link im WHERE im.Availed=0 and IssuedTo=@distributorId 
		           and GETDATE()<=ApplicableTo) )
	                    BEGIN 
	                    	PRINT 	'VoucherSrNo already used '	;	
	                    	select 'INF0041' OutParam	
	                		SET @outParam = 'INF0041'
	                	  ROLLBACK 
		                 Return
	                     END
	 
		            END
		            
		                 
						INSERT  INTO [COHeader]  --(Table tells for Custumer order detail fo addresses ,)
										(
				 [CustomerOrderNo], [Date],[LogNo],[DistributorId],   [DeliverToAddressLine1], [DeliverToAddressLine2],[DeliverToAddressLine3],
				 [DeliverToAddressLine4],[DeliverToCityId],  [DeliverToPincode],    [DeliverToStateId],[DeliverToCountryId],[DeliverToTelephone],
						[DeliverToMobile],     [Status],   [TotalUnits],   [TotalWeight],      [OrderAmount], [DiscountAmount], [TaxAmount],
				   [PaymentAmount], [ChangeAmount],   [CreatedBy],  [CreatedDate],     [ModifiedBy],     [ModifiedDate],    [OrderType], 
					   [PCId],   [BOId],     [DeliverFromAddress1], [DelverFromAddress2],    [DeliverFromAddress3],
					   [DeliverFromAddress4],   [DeliverFromCityId], [DeliverFromPincode], [DeliverFromStateId],  [DeliverFromCountryId],
					   [DeliverFromTelephone],   [DeliverFromMobile], [OrderMode], [TerminalCode],  DistributorAddress,
					   TotalBV,   TotalPV,   SourceLocationId,    TaxJurisdictionId  ,AddressId,OrderDispatchMode,OrderRemarks,OrderShippingCharges                                                            
										)
										SELECT  @orderId, getDate (),@LogNO,@distributorId,@AddressLine1,@AddressLine2,@AddressLine3
										  ,@AddressLine4,@cityCode,@PinCode,@StateCode,@contryCode,@PhoneNo1
										  ,@MobileNo1,'3',@TotalUnit,@TotalWeight,@OrderAmount,@DiscountAmount,@TaxAmount,@PaymentAmount,@ChangeAmount
										   ,@createBy,GETDATE(),
										  @createBy,GETDATE(),Case when @isFirstOrder=0 then 1 else 2 end,CASE WHEN @PCId=0 Then @BOId Else @PCId END,@BOId,lm.Address1,lm.Address2,lm.Address3,lm.Address4
										  ,lm.CityId,lm.Pincode,lm.StateId,lm.CountryId,
										  lm.Phone1,lm.Mobile1,@OrderMode,@TerminalCode,@AddressLine5,@TotalBV,@TotalPV,@SourceLocationId,lm.TaxJurisdiction,@addressID,@orderDispatchMode,
										  @Remarks,@courierCharges
											 FROM    [Location_Master]LM  
											  LEFT   JOIN City_Master CM ON LM.[CityId]=CM.[CityId]
											  LEFT   JOIN State_Master SM ON LM.[StateId]=SM.[StateId]
											  LEFT     JOIN Country_Master [CNM] ON LM.[CountryId]=CNM.[CountryId] 
											 WHERE lm.LocationId= @locationId 
		                 	
						INSERT  INTO CODetail
										(  
										  [CustomerOrderNo],
										  [RecordNo],
										    GiftVoucherCode,
                                              VoucherSrNo,
										  [ItemId],
										  [UnitPrice],
										  [Qty],
										  [IsPromo],
										  [ItemName],
										  [ShortName],
										  [PrintName],
										  [ReceiptName],
										  [DisplayName],
										  [ItemCode],
										  [PV],
										  [BV],
										  [DP],
										  [TaxAmount],
										  [TotalWeight],
										  [Amount],
										  [IsKit],
										  [IsComposite],
										  [Weight],
										  [Length],
										  [Height],
										  [Width],                                 
										  [PrimaryCost],
										  [UOM],
										  [MRP],
										  [TaxCategoryId]
										)
										SELECT  InnerTable.*,
												IM.ItemName,
												IM.ShortName,
												IM.PrintName,
												IM.ReceiptName,
												IM.DisplayName,
												IM.ItemCode,
												IM.PointValue,
												IM.BusinessVolume,
												IM.DistributorPrice,
												0 'TaxAmount',
												IM.Weight * InnerTable.Qty,
												IM.DistributorPrice * INNERTABLE.Qty,
												IM.IsKit,
												IM.IsComposite,
												IM.Weight,
												IM.Length,
												IM.Height,
												IM.Width,
												IM.PrimaryCost,
												IUOM.UOMId,
												IMrp.MRP,
												IM.TaxCategoryId
										FROM    ( SELECT    @orderId 'CustomerOrderNo',
														    recordNo,  
														      GiftVoucherNumber,
                                                             VoucherSrNo,
															 [ItemId],
															[Price],
															[Qty],
														   [IsPromo]
															FROM     #temptable
												) InnerTable
												INNER JOIN Item_Master IM ON InnerTable.ItemId = IM.ItemId
												INNER JOIN ItemMRP IMRP ON IM.ItemId = IMRP.ItemId
		--						AND IMRP.PrimaryPrice = 1
												INNER JOIN ItemUOM_Link IUOM ON IM.ItemId = IUOM.ItemId
																				AND IUOM.IsPrimary = 1 
																				AND IUOM.TypeOfMeasure = 2    
					 --select * from CODetail where CustomerOrderNo=@orderId  

				  INSERT  INTO [CODetailDiscount]
												   ([CustomerOrderNo] ,
													  [RecordNo],
													  [PromotionId],
													  [Description],
													  [DiscountPercent],
													  [DiscountAmount]									
													)
														SELECT    @orderId OrderId,
																		recordNo,
																		PromotionId,
																		PromoDescription,
																		DiscountPercent,
																		DiscountAmount
															  FROM      #temptable   where IsPromo=1   
									  INSERT INTO @payment     
									   SELECT * FROM parseJSON(@jsonForPayment)
									   
			DECLARE @PromotionCount INT 
			
			select @PromotionCount=COUNT(*) From  #temptable   where IsPromo=1 
			if @PromotionCount>0
			   BEGIN
			     select @PromotionCount =COUNT(*) from Promotion_Master PM INNER JOIN #temptable TM ON tm.PromotionId
			     =PM.PromotionId Where PM.EndDate<GETDATE()
			   If @PromotionCount>0
			     BEGIN
			        select 'EXP001' OutParam --Promotion expired.
			         ROLLBACK
			          RETURN
			     END
			   if(@isFirstOrder=1)  
			     BEGIN
			          select @PromotionCount =COUNT(*) from Promotion_Master PM INNER JOIN #temptable TM ON tm.PromotionId
			     =PM.PromotionId Where PM.Applicability=1
			        If @PromotionCount>0
			     BEGIN
			        select 'EXP002' OutParam --Promotion Given wrong.
			         ROLLBACK
			          RETURN
			     END
			     END
			     
			   END
                            
                            
                       ---Check for Inventory
                        create table #BatchDetail  (
							 InvoiceNo   Varchar(20), 
							ItemId    INT,  
							BucketId   INT,  
							BatchNo    VARCHAR(50),  
							ItemCode   Varchar(20),  
							TotalQty  NUMERIC(12,4),  
							Quantity   NUMERIC(12,4),  
							ManufacturingBatchNo Varchar(20),  
							MRP    MONEY,  
							MfgDate Varchar(30),  
							ExpDate   Varchar(30),  
							AvailableQty  NUMERIC(12,4),
							RecordNo   INT  
								)
			     declare @countItems int;
			   set @countItems=0;
				 insert #BatchDetail Exec  usp_DefaultInvoiceBatch  @orderId, @locationId  ,''   ;
	
				 select @countItems=count(BD.ItemId) from #BatchDetail BD LEFT JOIN 
				( SELECT TotalQty,ItemId,SUM(Quantity) Quantity FROm #BatchDetail GROUP BY ItemId,TotalQty )
				 TOD ON TOD.ItemId=BD.ItemId
 				  where TOD.TotalQty>TOD.Quantity  ;		   
							-- CALCULATE TAXES ON GIFTVOUCHER ITEMS
						IF(@countItems>0)
						   BEGIN
						   SET @outParam='INV001' --short inventory                 
								SELECT @outParam OutParam,IM.ItemCode,im.ItemName,BD.ItemId,TOD.TotalQty RequiredQty,Tod.Quantity AvailableQty
								 from #BatchDetail BD LEFT JOIN 
									( SELECT TotalQty,ItemId,SUM(Quantity) Quantity FROm #BatchDetail GROUP BY ItemId,TotalQty )
									 TOD ON TOD.ItemId=BD.ItemId
 				 
								inner join Item_Master IM
								on BD.ItemId = IM.ItemId
								 where TOD.TotalQty>TOD.Quantity  GROUP By IM.ItemCode,Im.ItemName,BD.ItemId,TOD.TotalQty,Tod.Quantity
								rollback
								Return
						   END	
							
							declare   @locationCode varchar(50),@isLocationOnline int, @deliverFromStateId int ,@deliverToStateId int, @validationMessage varchar(200) ;
                                    SELECT  @locationCode = LocationCode ,@isLocationOnline=IsLocationOnline ,@TaxJurisdictionId=TaxJurisdiction 
                                    FROM    Location_Master
                                    WHERE   LocationId = @locationId
							if @isLocationOnline=0   --case when order comes from web and location is offline
							   BEGIN
								 EXEC usp_interface_audit '', 'CUSTORD', '',
                                'COHeader', @orderId, NULL, NULL, NULL, NULL,
                                'I', @createBy, @reccnt OUTPUT    
								END
                                    SELECT  @deliverFromStateId = o.TaxJurisdictionId, -- o.DeliverFromStateId,
                                            @deliverToStateId = O.DeliverToStateId
                                    FROM    COHeader O
                                    WHERE   O.CustomerOrderNo = @orderId
		--					EXEC usp_PromotionEngine 2,'',-1, @locationCode, -1, @orderId, @deliverFromStateId, @deliverToStateId, 'SOTAX', '', 0, @outParam OUTPUT, @validationMessage OUTPUT, @isFirstOrder
                                    DECLARE @iCount INT,
                                        @iItemId INT,
                                        @recordNo INT,
                                        @giftVoucherNo VARCHAR(20),
                                        @voucherSrNo VARCHAR(50)
                                    SET @iCount = 0
                                    SET @iItemId = -1

                                    SELECT  @iCount = MAX(RecordNo)
                                    FROM    CoDetail
                                    WHERE   CustomerOrderNo = @orderId
                                            AND ISNULL(GiftVoucherCode, '') <> ''
							
                                    WHILE @iCount > 0
                                        BEGIN
                                            SELECT  @iItemId = ItemId,
                                                    @recordNo = RecordNo,
                                                    @giftVoucherNo = GiftVoucherCode,
                                                    @voucherSrNo = VoucherSrNo
                                            FROM    CODetail
                                            WHERE   CustomerOrderNo = @orderId
                                                    AND ISNULL(GiftVoucherCode, '') <> ''
                                                    AND RecordNo = @iCount
									
                                            IF ( @iItemId <> -1 ) 
                                                BEGIN
                                                    EXEC [usp_GetTaxGroupDetail] @iItemId,
                                                        '',
                                                        @deliverFromStateId,
                                                        @deliverToStateId,
                                                        'SOTAX', '', 0,
                                                        @outParam OUTPUT,
                                                        @validationMessage OUTPUT,
                                                        1, @orderId, @recordNo
                                                    SET @iCount = @iCount - 1
											
											EXEC usp_interface_audit '', 'TRGFTVCHR',
                                        '', 'Gift Voucher Distributor Link',
                                        @giftVoucherNo, @VoucherSrNo, NULL,
                                        NULL, NULL, 'U', @createBy, @reccnt OUTPUT    	
									
											
                                                    UPDATE  giftvoucher_distributor_link
                                                    SET     Availed = 1,
                                                            AvailedDate = GETDATE()
                                                    WHERE   GiftVoucherCode = @giftVoucherNo
                                                            AND VoucherSrNo = @voucherSrNo
										
                                                    SET @iItemId = -1
                                                END
                                            ELSE 
                                                BEGIN
                                                    SET @iCount = 0
                                                END
                                        END
                                        
                                
									   
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@payment)
							SELECT 
							
							   b.value('@PaymentAmount', 'float') as paymentAmount,
							 b.value('@TenderType', 'int') as TenderType ,
							  b.value('@PaymentDescription', 'varchar(20)') as ReceiptDisplay,
							   b.value('@VoucherId', 'varchar(20)') as VoucherId
								into #PaymentTemptable 
							
							  FROM @xml.nodes('/root/item') a(b) 
							  alter table  #PaymentTemptable add  RecordNo Int Identity(1,1)  ; 
						
			   INSERT  INTO [COPayment]
											(
											  [CustomerOrderNo],
											  [RecordNo],
											  [TenderType],
											  [PaymentAmount],
											  [Date],
											  [Remark],
											  [CurrencyCode],
											  [ForexAmount],
											  [ReceiptDisplay],
											  [CardType],
											  [Reference]
											)
											SELECT  @orderId,
													[RecordNo],
													[tenderType],
													[PaymentAmount],
													 getDate(),
													 '',
													 'INR',
													 '0.00',
												   ReceiptDisplay ,0,VoucherId  from #PaymentTemptable  
								  
								   declare @LogType int;
								
								   DECLARE @ChequeNo AS VARCHAR(20);
								   DECLARE @totalPayment Numeric(18,4),@locType int
					SELECT @PaymentAmount =SUM(paymentAmount) from #PaymentTemptable ;
					
					DECLARE @LogOrderAmount INT;
					
					if(@LogNo!=null OR @LogNo!='')
					BEGIN
						select @LogOrderAmount = sum(COP.PaymentAmount) from COPayment COP where cop.CustomerOrderNo IN
						(select COH.customerOrderNo from coheader COH where COH.LogNo = @LogNo
						 AND COH.Status = 3) AND
						COP.TenderType = 6
					
						--Select @LogOrderAmount = SUM(COH.PaymentAmount) FROM COHeader COH  
					--where COH.LogNo=@LogNo AND COH.Status = 3 group by COH.LogNo
					END
					ELSE
					BEGIN
						SET @LogOrderAmount = 0;
					END
					
					
					
					UPDATE COHeader set PaymentAmount=@PaymentAmount where CustomerOrderNo=@orderId		
					SELECT @totalPayment= ISNULL(SUM(PaymentAmount),0)
					  FROM #PaymentTemptable 
					if(@totalPayment!=@OrderAmount) 
					  BEGIN
					      SET @outParam='AMT001'
					      Select 'payment Amount not equal to orderamount ' OutParam
					        RoLLBACK
					        Return
					  END
			
												
						SET @iCount = 0
						SELECT @iCount = COUNT(1) FROM CoDetail WHERE CustomerOrderNo = @orderId
						
						                            UPDATE  DistributorBonus_ChequeLink
                            SET     Status = CASE WHEN ISNULL(UsedAmount, 0)
                                                       + ISNULL(P.PaymentAmount, 0) = Amount
                                                  THEN 3
                                                  ELSE 2
                                             END,
                                    UsedAmount = ISNULL(UsedAmount, 0)
                                    + ISNULL(P.PaymentAmount, 0),
                                    BalanceAmount = ISNULL(Amount, 0)
                                    - ( ISNULL(UsedAmount, 0)
                                        + ISNULL(P.PaymentAmount, 0) ),
                                    CanBeUsedAgain = ( CASE WHEN @LogType = 2
                                                                 AND ISNULL(UsedAmount, 0) + ISNULL(P.PaymentAmount, 0) < ISNULL(Amount, 0)
                                                            THEN 1
                                                            ELSE 0
                                                       END ),
                                    OrderNo = ( CASE WHEN @LogType = 2 THEN ''
                                                     ELSE @orderId
                                                END ),
                                    TeamOrderNo = ( CASE WHEN @LogType = 2
                                                         THEN @LogNo
                                                         ELSE ''
                                                    END )
----------------------------------------------------------------------------------------------------
                            FROM    DistributorBonus_ChequeLink DB,
                                    ( SELECT    VoucherId,
                                                paymentAmount
                                      FROM      #PaymentTemptable 
                                    ) P
                            WHERE   DB.ChequeNo = P.VoucherId
                            
     --select * from #PaymentTemptable where TenderType=5
							SELECT    VoucherId,
									ISNULL(PaymentAmount, 0) AS PaymentAmount
							INTO #tempTable1
							FROM      #PaymentTemptable where TenderType=5
           

                            
                           -- DECLARE @PaymentAmount AS VARCHAR(200)
								
                            DECLARE DistributorBonusCur CURSOR FORWARD_ONLY STATIC
                                FOR SELECT  VoucherId
                                           
                                    FROM    #tempTable1 lm
                                    WHERE   ISNULL(lm.VoucherId, '') <> ''

                            OPEN DistributorBonusCur
                            FETCH NEXT FROM DistributorBonusCur INTO @ChequeNo
                               
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    EXEC usp_interface_audit '', 'DISBONUSCHEQUE',
                                        '', 'DistributorBonus_ChequeLink',
                                        @ChequeNo, NULL, NULL,
                                        NULL, NULL, 'U', @createBy, @reccnt OUTPUT    	
									 
--									EXEC usp_interface_audit '','GFTVCHR','','Gift Voucher',@GiftVoucherNumber,NULL,NULL,NULL,NULL,'U',@user,@reccnt OUTPUT    	
									
                                    FETCH NEXT FROM DistributorBonusCur INTO @ChequeNo
                                       
                                END
                          CLOSE DistributorBonusCur
						  DEALLOCATE DistributorBonusCur
																			UPDATE cd
						SET cd.Discount = a.Discount,
						cd.Amount = ((cd.Dp*cd.Qty) - a.Discount),
						Cd.UnitPrice = ((cd.Dp*cd.Qty) - a.Discount)/cd.Qty
						FROM CoDetail cd INNER JOIN
						(SELECT CustomerOrderNo, RecordNo, SUM(DiscountAmount) Discount FROM CoDetailDiscount WHERE CustomerOrderNo = @orderId GROUP BY CustomerOrderNo, RecordNo) a
						ON cd.CustomerOrderNo = a.CustomerOrderNo
						AND cd.RecordNo = a.RecordNo


										UPDATE CoDetail
										SET Amount = Dp*Qty,
											UNITPrice = Dp,
											Discount = 0
										WHERE CustomerOrderNo = @orderId
										AND RecordNo Not In (Select RecordNo FROM CoDetailDiscount WHERE CustomerOrderNo = @orderId)	


											
								WHILE @iCount >0
											BEGIN
												
												SELECT @iItemId= ItemId--,
														--@TaxTypeCode = TaxCategoryId
												FROM CoDetail
												WHERE CustomerOrderNo = @orderId
												AND RecordNo = @iCount

												--Select @iItemId,'',@fromStateID,@toStateID,@TaxTypeCode,'',@IsFormC, @vCustomerOrder
												-- Calculate All Taxes for a customer Order
												exec [usp_GetTaxGroupDetail] @iItemId,'',@TaxJurisdictionId,@StateCode,'SOTAX','',0,@outParam output,@validationMessage OUTPUT,1,@orderId, @iCount


											SET @iCount = @iCount-1
											END

											--Select * from CoDetailTax
											UPDATE cd SET cd.TaxAmount  = a.TaxAmount,
											Cd.UnitPrice = Cd.UnitPrice - (a.TaxAmount)/cd.Qty,
											cd.Amount = cd.Amount - a.TaxAmount
											FROM CoDetail cd INNER JOIN 
											(SELECT cdt1.CustomerOrderNo, cdt1.RecordNo, SUM(cdt1.TaxAmount) TaxAmount FROM CoDetailTax cdt1
											WHERE cdt1.CustomerOrderNo = @orderId GROUP BY cdt1.CustomerOrderNo, cdt1.RecordNo) a
											ON cd.CustomerOrderNo = a.CustomerOrderNo
											AND cd.RecordNo =  a.RecordNo

											UPDATE COHeader
											SET 
												TaxAmount = (SELECT SUM(TaxAmount) FROM CoDetail WHERE CustomerOrderNo = @orderId),
												DiscountAmount = (SELECT SUM(Discount) FROM CoDetail WHERE CustomerOrderNo = @orderId) ,
												OrderAmount =	(SELECT SUM(unitPrice*Qty) FROM CoDetail WHERE CustomerOrderNo = @orderId),
											TotalWeight=(SELECT SUM(Weight*Qty) FROM CoDetail WHERE CustomerOrderNo = @orderId)
											WHERE CustomerOrderNo = @orderId

                      DECLARE  @zeroLogFlag INT
                        Select @zeroLogFlag=isZeroLog from OrderLog where LogNo=@LogNO 
                        if(@zeroLogFlag=1)
                           BEGIN 
                             UPDATE CODetail Set PV=0,BV=0 where CustomerOrderNo=@orderId
                           END
		                                 
										  drop table #PaymentTemptable;
										  drop table #temptable;
		      
		   select '' as InvoiceNo, CustomerOrderNo, UserName as CreatedBy,TaxAmount,DiscountAmount ,@LogOrderAmount LogOrderAmount,
                   @LogNO LogNo, co.Date  CreatedDate, case CO.Status when 3 then 'Confirmed' else '' end as
   Status ,CO.Status StatusId from COHeader CO left join  User_Master UM on um.UserId=co.ModifiedBy where CustomerOrderNo=@orderId;
		    
		      COMMIT
           
        END TRY
        BEGIN CATCH
            ROLLBACK
            SET @outParam = '30001:' + CAST(ERROR_NUMBER() AS VARCHAR(50))
                + ', ' + CAST(ERROR_MESSAGE() AS VARCHAR(500))
                + 'Error Line: ' + CAST(ERROR_LINE() AS VARCHAR(50)) --+ ', Source: ' + CAST (ERROR_PROCEDURE() as VARCHAR(50))
           select @outParam OutParam 
            RETURN ERROR_NUMBER()
        END CATCH        
		                                                    
		end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateInventroyOnInvoice]    Script Date: 02/09/2014 13:05:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UpdateInventroyOnInvoice]            
@InvoiceNo varchar(20) ,          
@outParam VARCHAR(500) OUTPUT              
AS            
BEGIN       
     
 DECLARE @ProcessDate DATETIME                          
          
  DECLARE @Status int,                      
   @ModifiedBy int,                      
   @INTransitBucketId int,                      
   @LocationId int,                      
   @ItemId int,                      
   @NetQuatity int,                      
   @SellableBucketId int ,              
   @TOBatchDetail  varchar(20)                           
                        
                       
  DECLARE @InventoryBatch  AS TABLE                            
   (                            
   TransType VARCHAR(20),                            
   TransTypeId VARCHAR(20),                            
   Date DATETIME,                            
   InventoryBatchNo VARCHAR(20),                            
   LocationID INT,                            
   ItemId INT,                            
   BatchNo VARCHAR(20),                            
   NetQuantity NUMERIC(12,4),                            
   BucketID INT,                            
   DiscountValue NUMERIC(12,4)                            
   )                             
                       
  SELECT Top 1 @SellableBucketId  = BucketId FROM Bucket WHERE ParentId Is Not Null AND Sellable = 1                                  
  SET @ProcessDate=(SELECT c.InvoiceDate            
                      FROM CIHeader c WHERE c.InvoiceNo=@InvoiceNo)            
                                  
  SET @LocationId=(SELECT c.BOId            
                      FROM CIHeader c WHERE c.InvoiceNo=@InvoiceNo)            
             
  PRINT @ProcessDate                                              
  SET @Status=1                          
  SET @ModifiedBy=1                           
  SET @INTransitBucketId=9           
      
   PRINT 'I m here - 1 ' + Convert(Varchar(20), GETDATE(), 20)                          
  --get invoice detail                  
   BEGIN Try                            
    
        
  -- IF EXISTS (SELECT TOP 1 *  FROM Inventory_real_time_history irth WHERE referencekey=@InvoiceNo)      
  --BEGIN      
  -- ROLLBACK      
  -- return      
  --END      
      
      
    INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID, DiscountValue)              
      SELECT 'CUSTOMER', 1, InvoiceDate, ch.InvoiceNo,   BOId LocationId,cd.ItemId, cbd.BatchNo, -1 * SUM(ISNULL(cbd.Quantity,0)) As NetQuantity, cbd.BucketId As BucketId,              
        dbo.[ufn_GetStockLedgerInvoiceDiscount] (ch.InvoiceNo,BOId,cd.ItemId,cbd.BatchNo,Discount)              
      FROM dbo.CIBatchDetail cbd              
      INNER JOIN dbo.CIDetail cd  ON cd.InvoiceNo = cbd.InvoiceNo  AND cd.ItemId = cbd.ItemId  --AND cbd.RecordNo = cd.RecordNo              
      INNER JOIN CIHeader ch ON   cbd.InvoiceNo = ch.InvoiceNo              
      WHERE cd.InvoiceNo=@InvoiceNo           
      GROUP BY InvoiceDate, ch.InvoiceNo, BOId, cd.ItemId , cbd.BatchNo, cbd.BucketId, Discount    
          
          
          
        -- Insert invoice detail into Stock_Ledger_Daily                          
      INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo, LocationId, ItemId, BucketId, TransTypeId, TransQty, TransDiscountValue)                          
      SELECT date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId, SUM(ISNULL(NetQuantity,0)) NetQuantity, SUM(ISNULL(DiscountValue,0)) FROM @InventoryBatch ib                                
      GROUP BY date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId         
          
          
      IF EXISTS (SELECT 1 FROM Inventory_LocBucketBatch sl       
      INNER JOIN @InventoryBatch ib  on sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId  
      INNER JOIN CIBatchDetail cd ON cd.ItemId = ib.ItemId AND cd.BatchNo = ib.BatchNo AND cd.BucketID = ib.BucketID  
                 WHERE cd.InvoiceNo=@InvoiceNo  
      )      
      begin      
        PRINT '1'
			UPDATE Inventory_LocBucketBatch                            
			SET               
		   --select                 
			Quantity = ilbb.Quantity + ibt.NetQuantity             
			FROM @InventoryBatch ibt  
			Inner JOIN Inventory_LocBucketBatch ilbb                            
			ON ilbb.BucketId = ibt.BucketId                            
			AND ilbb.LocationId = ibt.LocationId                            
			AND ilbb.ItemId = ibt.ItemId   
			AND ilbb.BatchNo = ibt.BatchNo  
			INNER JOIN CIBatchDetail cd ON cd.ItemId = ilbb.ItemId AND cd.ItemId = ilbb.ItemId AND cd.BucketId = ilbb.BucketId  
				 WHERE cd.InvoiceNo=@InvoiceNo     
                   
      END    
      ELSE      
   begin     
     PRINT '2'  
				 INSERT INTO Inventory_LocBucketBatch(BatchNo, LocationId, ItemId, BucketId, Quantity, Status, CreatedBy, CreatedDate, ReasonId, ItemName, DocumentDate)           
			 SELECT ib.BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, @Status, @ModifiedBy, @ProcessDate, 1, im.ItemName, @ProcessDate         
			 FROM @InventoryBatch ib                            
			  INNER JOIN Item_Master im                             
			  ON im.ItemId = ib.ItemId        
			  INNER JOIN InventoryLocation_Master ilm ON ilm.ItemId = ib.ItemId AND  ilm.BucketId = ib.BucketId   AND ilm.LocationId=ib.LocationID  
			  INNER JOIN CIBatchDetail cd ON cd.ItemId = ilm.ItemId AND cd.ItemId = ilm.ItemId AND cd.BucketId = ilm.BucketId           
			  WHERE (ib.LocationID = @LocationId and ib.BucketID=@INTransitBucketId AND  cd.InvoiceNo=@InvoiceNo )                            
			  AND NOT EXISTS  (SELECT 1 FROM Inventory_LocBucketBatch sl        
			   WHERE sl.ItemId = ib.ItemId AND sl.BatchNo = ib.BatchNo        
			 AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                            
			GROUP BY ib.BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, im.ItemName,ib.BatchNo    
   end      
        
        
             
      --Insert invoice detail into Inventory_LocBucketBatch_History                        
       INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity, Status,CreatedBy, CreatedDate, ReasonId, ItemName)                            
       SELECT            @ProcessDate,BatchNo, LocationId,im.ItemId, BucketId, SUM(ISNULL(NetQuantity,0)) NetQuantity,@Status , @ModifiedBy,@ProcessDate,1,im.ItemName        
       FROM @InventoryBatch ib         
       INNER JOIN Item_Master im ON im.ItemId = ib.ItemId                               
       GROUP BY date, InventoryBatchNo, BatchNo, LocationId, im.ItemId, BucketId, TransTypeId , im.ItemName        
           
           
           
           
        IF exists(SELECT 1 FROM InventoryLocation_Master sl      
   INNER JOIN @InventoryBatch ib ON sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId  
         INNER JOIN CIBatchDetail cd ON cd.ItemId = sl.ItemId AND cd.ItemId = sl.ItemId AND cd.BucketId = sl.BucketId  
         WHERE cd.InvoiceNo=@InvoiceNo   )      
   BEGIN   
      PRINT '3'  
			   UPDATE InventoryLocation_Master                          
			   SET             
			  --select               
			  Quantity =ilm.Quantity+ibt.NetQuantity ,      
			  AvailableQuantity =ilm.AvailableQuantity+ibt.NetQuantity       
			  FROM @InventoryBatch ibt                         
			  Inner JOIN InventoryLocation_Master ilm                          
			  ON ilm.BucketId = ibt.BucketId                          
			  AND ilm.LocationId = ibt.LocationId                          
			  AND ilm.ItemId = ibt.ItemId   
			  INNER JOIN CIBatchDetail cd ON cd.ItemId = ilm.ItemId AND cd.ItemId = ilm.ItemId AND cd.BucketId = ilm.BucketId  
				 WHERE cd.InvoiceNo=@InvoiceNo                    
      --GROUP BY ibt.LocationId, ibt.ItemId, ibt.BucketId      
          
    END                
           ELSE      
     BEGIN     
        PRINT '4'  
				 INSERT INTO InventoryLocation_Master (LocationId, ItemId, BucketId, Quantity, AvailableQuantity, QuantityOnOrder, QuantityOnTransfer)                            
				  SELECT LocationId, ItemId, BucketId,                             
					SUM(ISNULL(Quantity,0)) NetQuantity,                             
					SUM(ISNULL(Quantity,0)),               
					CASE WHEN ib.BucketId = @SellableBucketId THEN dbo.[ufn_GetStockLedgerPO_GRNQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId) ELSE 0 END,        
					0        
					--dbo.[ufn_GetStockLedgerTOIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId) ,        
					-- dbo.[ufn_GetStockLedgerTIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId)                             
					FROM Inventory_LocBucketBatch ib                            
				  WHERE (LocationID = @LocationId)               
				  --AND ib.BatchNo=@TOBatchDetail                           
				  AND NOT EXISTS (SELECT 1 FROM InventoryLocation_Master sl WHERE sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId )                            
				  GROUP BY LocationId, ItemId, BucketId    
	           
     END                 
           
     --INSERT INTO Inventory_real_time_history       
     --SELECT ''Customer'',ib.LocationID,@InvoiceNo,ib.ItemId,ib.NetQuantity,ib.Date      
     --FROM   @InventoryBatch ib      
         
           
                 
 END  Try                                       
 Begin Catch                                 
    Set @outParam = '90001:' + CAST(Error_Number() AS VarChar(50)) +  '' + Error_Message()                             
    SELECT Error_Message()                 
    Return ERROR_NUMBER()                           
            
                             
                       
 END catch                        
                           
END
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateInventroyOn_TI]    Script Date: 02/09/2014 13:05:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UpdateInventroyOn_TI]                    
@TINumber varchar(20) ,                    
@outParam VARCHAR(500) OUTPUT                        
AS                      
BEGIN                
               
  DECLARE @ProcessDate DATETIME                      
      
  DECLARE @Status int,                  
   @ModifiedBy int,                  
   @INTransitBucketId int,                  
   @LocationId int,                  
   @ItemId int,                  
   @NetQuatity int,                  
   @SellableBucketId int                      
                    
                   
  DECLARE @InventoryBatch  AS TABLE                        
   (                        
   TransType VARCHAR(20),                        
   TransTypeId VARCHAR(20),                        
   Date DATETIME,                        
   InventoryBatchNo VARCHAR(20),                        
   LocationID INT,                        
   ItemId INT,                        
   BatchNo VARCHAR(20),                        
   NetQuantity NUMERIC(12,4),                        
   BucketID INT,                        
   DiscountValue NUMERIC(12,4)                        
   )                         
                   
  SELECT Top 1 @SellableBucketId  = BucketId FROM Bucket WHERE ParentId Is Not Null AND Sellable = 1                              
  SET @ProcessDate=(SELECT th.ReceivedTime                  
                    FROM TIHeader  th   WHERE th.TINumber=@TINumber)                                                
  SET @LocationId=(SELECT th.DestinationLocationID                  
                     FROM TIHeader th   WHERE th.TINumber=@TINumber)    
      
  PRINT @ProcessDate                                          
  SET @Status=1                      
  SET @ModifiedBy=1                       
  SET @INTransitBucketId=9                      
                    
                    
  PRINT 'I m here - 1 ' + Convert(Varchar(20), GETDATE(), 20)                      
  --get invoice detail        
  --BEGIN TRANSACTION                  
   BEGIN Try                        
             
     
   
 DECLARE @midVar AS varchar(2)    
 SET @midVar=(SELECT substring(@TINumber,1,2)  )    
 
 
       IF(@midVar<>'EX')    
      Begin   
           
     INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID)                    
      SELECT 'TI', 4, CAST(Convert(VARCHAR(10),ReceivedDate,120) + ' ' + Convert(VARCHAR(10),ReceivedTime,108) AS DATETIME),    
       th.TINumber, DestinationLocationID LocationId, td.ItemId, tbd.BatchNo, SUM(ISNULL(tbd.Quantity,0)) Quantity, td.BucketId              
      FROM TIBatchDetail tbd                       
      INNER JOIN TIDetail td  ON tbd.TINumber = td.TINumber  AND tbd.ItemId = td.ItemId  AND td.BucketId = tbd.RcvSubBucketId                      
      INNER JOIN TIHeader th  ON tbd.TINumber = th.TINumber  AND th.TINumber = td.TINumber                      
      INNER JOIN Location_Master lm   ON lm.LocationId = th.DestinationLocationID                        
      WHERE th.TINumber=@TINumber               
    AND ReceivedDate IS NOT NULL                         
    AND th.Status = 2-- Confirmed                      
    AND th.IsProcessed = 0                       
   --AND CAST(Convert(VARCHAR(10),ReceivedDate,120) + ' ' + Convert(VARCHAR(10),ReceivedTime,108) AS DATETIME) BETWEEN @ProcessDate AND DATEADD (second , -1, @LastProcessBusinessDate)                          
    AND ISNULL(td.BucketId,'')<>''                      
    AND DestinationLocationID = @LocationId                    
  GROUP BY  CAST(Convert(VARCHAR(10),ReceivedDate,120) + ' ' + Convert(VARCHAR(10),ReceivedTime,108) AS DATETIME),            
   th.TINumber, DestinationLocationID, td.ItemId, tbd.BatchNo, td.BucketId, tbd.BatchNo                      
                             
         END    
      ELSE    
      BEGIN
      
      INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID)                    
      SELECT 'TIEXP', 19, CAST(Convert(VARCHAR(10),ReceivedDate,120) + ' ' + Convert(VARCHAR(10),ReceivedTime,108) AS DATETIME),    
       th.TINumber, DestinationLocationID LocationId, td.ItemId, tbd.BatchNo, SUM(ISNULL(tbd.Quantity,0)) Quantity, td.BucketId              
      FROM TIBatchDetail tbd                       
      INNER JOIN TIDetail td  ON tbd.TINumber = td.TINumber  AND tbd.ItemId = td.ItemId  AND td.BucketId = tbd.RcvSubBucketId                      
      INNER JOIN TIHeader th  ON tbd.TINumber = th.TINumber  AND th.TINumber = td.TINumber                      
      INNER JOIN Location_Master lm   ON lm.LocationId = th.DestinationLocationID                        
      WHERE th.TINumber=@TINumber               
    AND ReceivedDate IS NOT NULL                         
    AND th.Status = 2-- Confirmed                      
    AND th.IsProcessed = 0                       
   --AND CAST(Convert(VARCHAR(10),ReceivedDate,120) + ' ' + Convert(VARCHAR(10),ReceivedTime,108) AS DATETIME) BETWEEN @ProcessDate AND DATEADD (second , -1, @LastProcessBusinessDate)                          
    AND ISNULL(td.BucketId,'')<>''                      
    AND DestinationLocationID = @LocationId                    
    GROUP BY  CAST(Convert(VARCHAR(10),ReceivedDate,120) + ' ' + Convert(VARCHAR(10),ReceivedTime,108) AS DATETIME),            
    th.TINumber, DestinationLocationID, td.ItemId, tbd.BatchNo, td.BucketId, tbd.BatchNo    
       END            
                              
        --Insert invoice detail into Stock_Ledger_Daily                      
      INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo, LocationId, ItemId, BucketId, TransTypeId, TransQty, TransDiscountValue)                      
      SELECT  date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId, SUM(ISNULL(NetQuantity,0)) NetQuantity, SUM(ISNULL(DiscountValue,0)) FROM @InventoryBatch ib                            
      GROUP BY date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId                    
          
           PRINT 'test4'  
      IF exists(SELECT 1 FROM InventoryLocation_Master sl     
     INNER JOIN @InventoryBatch ib on sl.ItemId = ib.ItemId AND    
      sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)    
 BEGIN     
     
   UPDATE InventoryLocation_Master                        
    SET           
      --select             
      Quantity =Quantity+ibt.NetQuantity ,    
      AvailableQuantity =AvailableQuantity+ibt.NetQuantity     
      FROM @InventoryBatch ibt                       
      Inner JOIN InventoryLocation_Master ilm                        
      ON ilm.BucketId = ibt.BucketId                        
      AND ilm.LocationId = ibt.LocationId                        
      AND ilm.ItemId = ibt.ItemId             
      --GROUP BY ibt.LocationId, ibt.ItemId, ibt.BucketId    
    
 END    
 ELSE    
  BEGIN    
       
      
       INSERT INTO InventoryLocation_Master (LocationId, ItemId, BucketId, Quantity, AvailableQuantity, QuantityOnOrder, QuantityOnTransfer)                        
      SELECT LocationId, ItemId, BucketId,                         
        SUM(ISNULL(Quantity,0)) NetQuantity,                         
        SUM(ISNULL(Quantity,0)),           
        CASE WHEN ib.BucketId = @SellableBucketId THEN dbo.[ufn_GetStockLedgerPO_GRNQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId) ELSE 0 END,    
        0    
        --dbo.[ufn_GetStockLedgerTOIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId) ,    
        -- dbo.[ufn_GetStockLedgerTIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId)                         
   FROM Inventory_LocBucketBatch ib                        
    WHERE (LocationID = @LocationId)           
    --AND ib.BatchNo=@TOBatchDetail                       
    AND NOT EXISTS (SELECT 1 FROM InventoryLocation_Master sl WHERE sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                        
    GROUP BY LocationId, ItemId, BucketId      
           
      END    
    
    
   PRINT 'test5'  
      
          
      --Insert invoice detail into Stock_Ledger_Daily                      
       INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity, Status,CreatedBy, CreatedDate, ReasonId, ItemName)                        
       SELECT            @ProcessDate,BatchNo, LocationId,im.ItemId, BucketId, SUM(ISNULL(NetQuantity,0)) NetQuantity,@Status , @ModifiedBy,@ProcessDate,1,im.ItemName    
       FROM @InventoryBatch ib     
       INNER JOIN Item_Master im ON im.ItemId = ib.ItemId                           
       GROUP BY date, InventoryBatchNo, BatchNo, LocationId, im.ItemId, BucketId, TransTypeId , im.ItemName    
         
         
           
       IF EXISTS (SELECT 1 FROM Inventory_LocBucketBatch sl    
    INNER join @InventoryBatch ib ON     
    sl.ItemId = ib.ItemId AND sl.BatchNo = ib.BatchNo    
      AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)    
       begin    
           
      UPDATE Inventory_LocBucketBatch                        
      SET           
     --select             
      Quantity =Quantity+ibt.NetQuantity           
      FROM @InventoryBatch ibt                       
      Inner JOIN Inventory_LocBucketBatch ilbb                        
      ON ilbb.BucketId = ibt.BucketId                        
      AND ilbb.LocationId = ibt.LocationId                        
      AND ilbb.ItemId = ibt.ItemId      
       AND ilbb.BatchNo=ibt.BatchNo     
       END    
       ELSE    
        BEGIN    
             
            
       INSERT INTO Inventory_LocBucketBatch(BatchNo, LocationId, ItemId, BucketId, Quantity, Status, CreatedBy, CreatedDate, ReasonId, ItemName, DocumentDate)                        
        SELECT BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, @Status, @ModifiedBy, @ProcessDate, 1, im.ItemName, @ProcessDate     
        FROM @InventoryBatch ib                        
    INNER JOIN Item_Master im                         
    ON im.ItemId = ib.ItemId    
    INNER JOIN InventoryLocation_Master ilm ON ilm.ItemId = ib.ItemId AND  ilm.BucketId = ib.BucketId   AND ilm.LocationId=ib.LocationID                     
    WHERE (ib.LocationID = @LocationId and ib.BucketID=@SellableBucketId )                        
    AND NOT EXISTS  (SELECT 1 FROM Inventory_LocBucketBatch sl    
     WHERE sl.ItemId = ib.ItemId AND sl.BatchNo = ib.BatchNo    
      AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                        
    GROUP BY ib.BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, im.ItemName    
        
  END        
   --      IF(@midVar<>'EX')    
		 --BEGIN   
		 --  INSERT INTO Inventory_real_time_history     
		 --  SELECT 'TI',ib.LocationID,@TINumber,ib.ItemId,ib.NetQuantity,ib.Date    
		 --  FROM   @InventoryBatch ib    
		 --  PRINT 'test6'   
   --       END    
   --   ELSE    
   --    BEGIN 
		 --  INSERT INTO Inventory_real_time_history     
		 --  SELECT 'TIEXP',ib.LocationID,@TINumber,ib.ItemId,ib.NetQuantity,ib.Date    
		 --  FROM   @InventoryBatch ib    
		 --  PRINT 'test6' 
   --    END
    COMMIT transaction                  
 END  Try                                   
 Begin Catch                             
    Set @outParam = '90001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()                        
    Rollback transaction            
    SELECT Error_Message()  
    Return ERROR_NUMBER()   
 END catch                  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateInventroyOn_Return]    Script Date: 02/09/2014 13:05:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[SP_UpdateInventroyOn_Return]                  
@ReturnNo varchar(20) ,                
@outParam VARCHAR(500) OUTPUT                    
AS                  
BEGIN             
           
 DECLARE @ProcessDate DATETIME                                
                
  DECLARE @Status int,                            
   @ModifiedBy int,                            
   @INTransitBucketId int,                            
   @LocationId int,                            
   @ItemId int,                            
   @NetQuatity int,                            
   @SellableBucketId int                                                
                              
                             
  DECLARE @InventoryBatch  AS TABLE                                  
   (                                  
   TransType VARCHAR(20),                                  
   TransTypeId VARCHAR(20),                                  
   Date DATETIME,                                  
   InventoryBatchNo VARCHAR(20),                                  
   LocationID INT,                                  
   ItemId INT,                                  
   BatchNo VARCHAR(20),                                  
   NetQuantity NUMERIC(12,4),                                  
   BucketID INT,                                  
   DiscountValue NUMERIC(12,4)                                  
   )                                   
                             
  SELECT Top 1 @SellableBucketId  = BucketId FROM Bucket WHERE ParentId Is Not Null AND Sellable = 1      
                                        
  SET @ProcessDate=(SELECT RCH.ApprovedDate                  
                      FROM RetCustomer_Header RCH WHERE RCH.ReturnNo=@ReturnNo)                  
                                        
  SET @LocationId=(SELECT rch.LocationId                  
                      FROM RetCustomer_Header RCH WHERE RCH.ReturnNo=@ReturnNo)                  
                   
  PRINT @ProcessDate                                                    
  SET @Status=1                                
  SET @ModifiedBy=1                                 
  SET @INTransitBucketId=9                 
            
   PRINT 'I m here - 1 ' + Convert(Varchar(20), GETDATE(), 20)                                
  --get invoice detail                        
   BEGIN Try                                  
  BEGIN TRANSACTION  ToMain             
              
  -- IF EXISTS (SELECT TOP 1 *  FROM Inventory_real_time_history irth WHERE referencekey=@ReturnNo)            
  --BEGIN            
  -- ROLLBACK            
  -- return            
             
            
 /*-----------------CUSTOMER RETURN Quantity  -------------------*/      
      INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID)      
      SELECT  'CUSTOMERRETURN', 14, ApprovedDate, ph.ReturnNo, ph.LocationId, pbd.ItemId, pbd.BatchNo, SUM(pbd.Quantity), pbd.BucketId      
      FROM  RetCustomer_Header ph      
      INNER JOIN dbo.RetCustomer_Detail pd      
      ON ph.ReturnNo = pd.ReturnNo      
      INNER JOIN RetCustomerBatch_Detail pbd      
      ON pbd.ReturnNo = pd.ReturnNo       
      AND pbd.ItemId = pd.ItemId      
      AND pbd.RowNo = pd.RowNo      
      WHERE ApprovedDate IS NOT NULL       
      AND ph.IsProcessed = 0       
      AND ApprovedDate =@ProcessDate    
      AND LocationID = @LocationID      
      GROUP BY ApprovedDate, ph.ReturnNo, ph.LocationId, pbd.ItemId, pbd.BatchNo, pbd.BucketId      
      /*-----------------CUSTOMER RETURN Quantity  -------------------*/      
            
                
                
                
        -- Insert invoice detail into Stock_Ledger_Daily                                
    INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo, LocationId, ItemId, BucketId, TransTypeId, TransQty, TransDiscountValue)                                
      SELECT date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId, SUM(ISNULL(NetQuantity,0)) NetQuantity, SUM(ISNULL(DiscountValue,0)) FROM @InventoryBatch ib            
      GROUP BY date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId               
                
                
 IF EXISTS (SELECT 1 FROM Inventory_LocBucketBatch sl             
 INNER JOIN @InventoryBatch ib  on sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId        
      INNER JOIN RetCustomerBatch_Detail rcbd  ON rcbd.ItemId = ib.ItemId AND rcbd.BatchNo = rcbd.BatchNo AND rcbd.BucketID = ib.BucketID        
                 WHERE rcbd.ReturnNo=@ReturnNo        
      )            
      begin            
   PRINT '1'        
     UPDATE Inventory_LocBucketBatch                                  
     SET                     
    --select                       
     Quantity = ilbb.Quantity + ibt.NetQuantity                   
     FROM @InventoryBatch ibt        
     Inner JOIN Inventory_LocBucketBatch ilbb                                  
     ON ilbb.BucketId = ibt.BucketId                                  
     AND ilbb.LocationId = ibt.LocationId                                  
     AND ilbb.ItemId = ibt.ItemId         
     AND ilbb.BatchNo = ibt.BatchNo        
    INNER JOIN RetCustomerBatch_Detail rcbd  ON rcbd.ItemId = ibt.ItemId AND rcbd.BatchNo = ibt.BatchNo AND rcbd.BucketID = ibt.BucketID        
                 WHERE rcbd.ReturnNo=@ReturnNo           
                           
      END          
 ELSE            
     begin           
    PRINT '2'         
    INSERT INTO Inventory_LocBucketBatch(BatchNo, LocationId, ItemId, BucketId, Quantity, Status, CreatedBy, CreatedDate, ReasonId, ItemName, DocumentDate)                 
   SELECT ib.BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, @Status, @ModifiedBy, @ProcessDate, 1, im.ItemName, @ProcessDate               
   FROM @InventoryBatch ib                                  
    INNER JOIN Item_Master im                                   
    ON im.ItemId = ib.ItemId              
    INNER JOIN InventoryLocation_Master ilm ON ilm.ItemId = ib.ItemId AND  ilm.BucketId = ib.BucketId   AND ilm.LocationId=ib.LocationID        
    INNER JOIN RetCustomerBatch_Detail rcbd  ON rcbd.ItemId = ib.ItemId AND rcbd.BatchNo = rcbd.BatchNo AND rcbd.BucketID = ib.BucketID        
                               
    WHERE (ib.LocationID = @LocationId and ib.BucketID=@INTransitBucketId AND  rcbd.ReturnNo=@ReturnNo )                                  
    AND NOT EXISTS  (SELECT 1 FROM Inventory_LocBucketBatch sl              
     WHERE sl.ItemId = ib.ItemId AND sl.BatchNo = ib.BatchNo              
   AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                                  
     GROUP BY ib.BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, im.ItemName,ib.BatchNo          
     end            
              
              
                   
      --Insert invoice detail into Inventory_LocBucketBatch_History                              
       INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity, Status,CreatedBy, CreatedDate, ReasonId, ItemName)                                  
       SELECT            @ProcessDate,BatchNo, LocationId,im.ItemId, BucketId, SUM(ISNULL(NetQuantity,0)) NetQuantity,@Status , @ModifiedBy,@ProcessDate,1,im.ItemName              
       FROM @InventoryBatch ib               
       INNER JOIN Item_Master im ON im.ItemId = ib.ItemId                                     
       GROUP BY date, InventoryBatchNo, BatchNo, LocationId, im.ItemId, BucketId, TransTypeId , im.ItemName              
                 
                 
                 
                 
        IF exists(SELECT 1 FROM InventoryLocation_Master sl            
   INNER JOIN @InventoryBatch ib ON sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId        
     INNER JOIN RetCustomerBatch_Detail rcbd  ON rcbd.ItemId = ib.ItemId AND rcbd.BatchNo = rcbd.BatchNo AND rcbd.BucketID = ib.BucketID         
         WHERE rcbd.ReturnNo=@ReturnNo   )            
    BEGIN         
    PRINT '3'        
   UPDATE InventoryLocation_Master                                
    SET                   
   --select                     
   Quantity =ilm.Quantity+ibt.NetQuantity ,            
   AvailableQuantity =ilm.AvailableQuantity+ibt.NetQuantity             
   FROM @InventoryBatch ibt                               
   Inner JOIN InventoryLocation_Master ilm                                
   ON ilm.BucketId = ibt.BucketId                                
   AND ilm.LocationId = ibt.LocationId                                
   AND ilm.ItemId = ibt.ItemId         
  INNER JOIN RetCustomerBatch_Detail rcbd  ON rcbd.ItemId = ibt.ItemId AND rcbd.BatchNo = ibt.BatchNo AND rcbd.BucketID = ibt.BucketID         
         WHERE rcbd.ReturnNo=@ReturnNo                          
    --GROUP BY ibt.LocationId, ibt.ItemId, ibt.BucketId            
                 
  END                      
           ELSE            
   BEGIN           
   PRINT '4'         
    INSERT INTO InventoryLocation_Master (LocationId, ItemId, BucketId, Quantity, AvailableQuantity, QuantityOnOrder, QuantityOnTransfer)                                  
     SELECT LocationId, ItemId, BucketId,                                   
    SUM(ISNULL(Quantity,0)) NetQuantity,                                   
    SUM(ISNULL(Quantity,0)),                     
    CASE WHEN ib.BucketId = @SellableBucketId THEN dbo.[ufn_GetStockLedgerPO_GRNQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId) ELSE 0 END,              
    0              
    --dbo.[ufn_GetStockLedgerTOIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId) ,              
    -- dbo.[ufn_GetStockLedgerTIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId)                                   
    FROM Inventory_LocBucketBatch ib                                  
     WHERE (LocationID = @LocationId)                     
     --AND ib.BatchNo=@TOBatchDetail                                 
     AND NOT EXISTS (SELECT 1 FROM InventoryLocation_Master sl WHERE sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId )                                  
     GROUP BY LocationId, ItemId, BucketId          
                   
   END                       
                 
   --INSERT INTO Inventory_real_time_history             
   --SELECT 'CUSTRTN',ib.LocationID,@ReturnNo,ib.ItemId,ib.NetQuantity,ib.Date            
   --FROM   @InventoryBatch ib            
               
                 
   COMMIT transaction                            
 END  Try                                             
 Begin Catch                                       
    Set @outParam = '90001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()                                  
    SELECT Error_Message()                       
    Return ERROR_NUMBER()                                 
    Rollback transaction                    
                             
 END catch                              
                                 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateInventroyOn_GRN]    Script Date: 02/09/2014 13:05:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UpdateInventroyOn_GRN]                                
@GRNNumber varchar(20) ,                                
@outParam VARCHAR(500) OUTPUT                                    
AS                                  
BEGIN                            
                           
  DECLARE @ProcessDate DATETIME                                  
                  
  DECLARE @Status int,                              
   @ModifiedBy int,                              
   @INTransitBucketId int,                              
   @LocationId int,                              
   @ItemId int,                              
   @NetQuatity int,                              
   @SellableBucketId int ,                         
   @CurrentDate DATETIME                                 
                                
                               
  DECLARE @InventoryBatch  AS TABLE                                    
   (                                    
   TransType VARCHAR(20),                                    
   TransTypeId VARCHAR(20),                                    
   Date DATETIME,                                    
   InventoryBatchNo VARCHAR(20),                                    
   LocationID INT,                                    
   ItemId INT,                                    
   BatchNo VARCHAR(20),                                    
   NetQuantity NUMERIC(12,4),                                    
   BucketID INT,                                    
   DiscountValue NUMERIC(12,4)                                    
   )                                     
                    
 -- This Table is used for Updating the Item Master Cost              
  DECLARE @COST_PO_GRN_FOR_ITEM_MASTER AS TABLE              
  (              
   Date DATETIME,              
   GRNNo VARCHAR(20),              
   LocationID INT,              
   ItemId INT,              
   PrimaryCost NUMERIC(12,4), -- Unit Price,              
   Row_Number INT              
  )             
                         
  SELECT Top 1 @SellableBucketId  = BucketId FROM Bucket WHERE ParentId Is Not Null AND Sellable = 1                                          
  SET @ProcessDate=(SELECT gh.GRNDate   FROM GRN_Header gh   WHERE gh.GRNNo=@GRNNumber)                                                            
  SET @LocationId=(SELECT ph.D_LocationId   FROM GRN_Header gh              
                      Inner Join PO_Header ph   ON ph.PONumber = gh.PoNumber   AND ph.PODate = gh.PODate             
                   WHERE gh.GRNNo=@GRNNumber)                                
                                                   
  SET @Status=1                                  
  SET @ModifiedBy=1                                   
  SET @INTransitBucketId=9                
  SET @CurrentDate =getdate()                              
                                
                                
  PRINT 'I m here - 1 ' + Convert(Varchar(20), GETDATE(), 20)                                  
  --get invoice detail                          
   BEGIN Try                                    
  BEGIN TRANSACTION                 
--                
  -- IF EXISTS (SELECT TOP 1 *  FROM Inventory_real_time_history irth WHERE referencekey=@GRNNumber)              
  --BEGIN              
  -- ROLLBACK              
  -- return              
  --END              
                                             
                                
  INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID)              
  SELECT 'GRN', 2, GRNDate, gbd.GRNNo, ph.D_LocationId LocationId, gbd.ItemId, gbd.BatchNumber, SUM(ISNULL(gbd.ReceivedQty,0)) ReceivedQty,  @SellableBucketId              
  FROM GRNBatch_Detail gbd              
    INNER JOIN GRN_Detail gd  ON gd.GRNNo = gbd.GRNNo    AND gbd.ItemId = gd.ItemId              
    INNER JOIN GRN_Header gh ON     gbd.GRNNo = gh.GRNNo              
    Inner Join PO_Header ph     ON ph.PONumber = gh.PoNumber     AND ph.PODate = gh.PODate              
    WHERE GRNDate IS NOT NULL           
    AND gh.Status = 3 -- 3 is Close              
   AND gh.IsProcessed = 0               
    AND GRNDate =@ProcessDate             
    AND ph.D_LocationId = @LocationID  
 AND gbd.GRNNo=@GRNNumber              
    GROUP BY GRNDate, gbd.GRNNo, ph.D_LocationId, gbd.ItemId, gbd.BatchNumber, gbd.ReceivedQty               
                                    
                                    
      INSERT INTO @COST_PO_GRN_FOR_ITEM_MASTER(Date, GRNNo, LocationID, ItemId,PrimaryCost)              
      SELECT Date, InventoryBatchNo, LocationID, ib.ItemId, pd.UnitPrice FROM @InventoryBatch ib              
      INNER JOIN GRN_Header gh              
      ON gh.GRNNo = ib.InventoryBatchNo              
      INNER JOIN PO_Detail pd              
      ON pd.ItemId = ib.ItemId              
      AND pd.PONumber = gh.PONumber              
      WHERE ib.TransTypeId = 2              
      AND gh.IsProcessed = 0             
      AND gh.GRNNo=@GRNNumber              
      GROUP BY Date, InventoryBatchNo, LocationID, ib.ItemId, pd.UnitPrice                              
                 
                                          
        --Insert invoice detail into Stock_Ledger_Daily                                  
     INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo, LocationId, ItemId, BucketId, TransTypeId, TransQty, TransDiscountValue)                                  
      SELECT date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId, SUM(ISNULL(NetQuantity,0)) NetQuantity, SUM(ISNULL(DiscountValue,0)) FROM @InventoryBatch ib                                        
      GROUP BY date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId                                
                      
                      
         IF exists(SELECT 1 FROM InventoryLocation_Master sl              
     -- INNER JOIN @InventoryBatch ib ON sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId          
     INNER JOIN GRNBatch_Detail gd ON gd.ItemId = sl.ItemId        
               WHERE gd.GRNNo=@GRNNumber)              
   BEGIN              
--                 
       UPDATE InventoryLocation_Master                                  
       SET                     
--     select                       
      Quantity =Quantity+ibt.NetQuantity ,              
      AvailableQuantity =AvailableQuantity+ibt.NetQuantity               
      FROM @InventoryBatch ibt                                 
      Inner JOIN InventoryLocation_Master ilm                                  
      ON ilm.BucketId = ibt.BucketId                                  
      AND ilm.LocationId = ibt.LocationId                                  
      AND ilm.ItemId = ibt.ItemId          
      INNER JOIN GRNBatch_Detail gd ON gd.ItemId = ibt.ItemId AND gd.BatchNumber=ibt.BatchNo           
               WHERE gd.GRNNo=@GRNNumber                       
      --GROUP BY ibt.LocationId, ibt.ItemId, ibt.BucketId              
    END            
   ELSE              
   BEGIN              
       INSERT INTO InventoryLocation_Master (LocationId, ItemId, BucketId, Quantity, AvailableQuantity, QuantityOnOrder, QuantityOnTransfer)                                    
     SELECT LocationId, ItemId, BucketId,                                     
       SUM(ISNULL(Quantity,0)) NetQuantity,                                     
       SUM(ISNULL(Quantity,0)),                       
       CASE WHEN ib.BucketId = @SellableBucketId THEN dbo.[ufn_GetStockLedgerPO_GRNQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId) ELSE 0 END,                
       0                
       --dbo.[ufn_GetStockLedgerTOIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId) ,                
       -- dbo.[ufn_GetStockLedgerTIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId)    
       FROM Inventory_LocBucketBatch ib                                    
     WHERE (LocationID = @LocationId)                       
     --AND ib.BatchNo=@TOBatchDetail                                   
     AND NOT EXISTS (SELECT 1 FROM InventoryLocation_Master sl WHERE sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                                    
     GROUP BY LocationId, ItemId, BucketId                  
   END              
           
    --Insert invoice detail into Stock_Ledger_Daily                                  
    INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity, Status,CreatedBy, CreatedDate, ReasonId, ItemName)                                    
     SELECT            @ProcessDate,gd.BatchNumber, LocationId,im.ItemId, BucketId, SUM(ISNULL(NetQuantity,0)) NetQuantity,@Status , @ModifiedBy,@ProcessDate,1,im.ItemName                
     FROM @InventoryBatch ib                 
     INNER JOIN Item_Master im ON im.ItemId = ib.ItemId     
     INNER JOIN GRNBatch_Detail gd ON gd.ItemId = ib.ItemId AND gd.BatchNumber=ib.BatchNo AND gd.ItemId=ib.ItemId  
     WHERE    gd.GRNNo=@GRNNumber                              
     GROUP BY date, InventoryBatchNo, gd.BatchNumber, LocationId, im.ItemId, BucketId, TransTypeId , im.ItemName                
                        
                       
                     
    IF EXISTS (SELECT 1 FROM Inventory_LocBucketBatch sl                   
   -- INNER JOIN @InventoryBatch ib  on sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId          
    INNER JOIN GRNBatch_Detail gd ON gd.ItemId = sl.ItemId AND gd.BatchNumber=sl.BatchNo           
               WHERE gd.GRNNo=@GRNNumber)              
    begin              
       UPDATE Inventory_LocBucketBatch                                    
      SET                       
     -- select                         
      Quantity =Quantity+ibt.NetQuantity          
      FROM @InventoryBatch ibt                                   
      Inner JOIN Inventory_LocBucketBatch ilbb                                    
      ON ilbb.BucketId = ibt.BucketId                                    
      AND ilbb.LocationId = ibt.LocationId                                    
      AND ilbb.ItemId = ibt.ItemId           
      INNER JOIN GRNBatch_Detail gd ON gd.ItemId = ibt.ItemId AND gd.ItemId=ibt.ItemId  AND gd.BatchNumber=ilbb.BatchNo      
               WHERE gd.GRNNo=@GRNNumber                         
    END              
       ELSE              
       BEGIN              
   INSERT INTO Inventory_LocBucketBatch(BatchNo, LocationId, ItemId, BucketId, Quantity, Status, CreatedBy, CreatedDate, ReasonId, ItemName, DocumentDate)                   
   SELECT gd.BatchNumber, ib.LocationId, ib.ItemId, ib.BucketId, netQuantity, @Status, @ModifiedBy, @ProcessDate, 1, im.ItemName, @ProcessDate                 
     FROM @InventoryBatch ib                                    
   INNER JOIN Item_Master im                                     
    ON im.ItemId = ib.ItemId                    
      INNER JOIN GRNBatch_Detail gd ON gd.ItemId = ib.ItemId  AND gd.ItemId=ib.ItemId  AND gd.BatchNumber=ib.BatchNo      
    where gd.GRNNo=@GRNNumber     and  
     NOT EXISTS  (SELECT 1 FROM Inventory_LocBucketBatch sl                
     WHERE sl.ItemId = ib.ItemId AND sl.BatchNo = ib.BatchNo                
      AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                                    
   GROUP BY gd.BatchNumber, ib.LocationId, ib.ItemId, ib.BucketId, netquantity, im.ItemName                 
                      
       END              
                      
    --INSERT INTO Inventory_real_time_history               
    --SELECT 'GRN',ib.LocationID,@GRNNumber,ib.ItemId,ib.NetQuantity,ib.Date              
    --FROM   @InventoryBatch ib              
                 
                   
        --------------------------------------------------------------------------------------------------              
      -- START UPDATE ITEM_MASTER PRIMARY COST               
      --------------------------------------------------------------------------------------------------              
      IF EXISTS (SELECT 1 FROM @COST_PO_GRN_FOR_ITEM_MASTER)              
      BEGIN              
       DECLARE @COST_PO_GRN_FOR_ITEM_MASTER_LOOP AS TABLE              
       (              
    RowNumber Int IDENTITY(1,1),              
    ItemId VARCHAR(20),              
    PrimaryCost NUMERIC(12,4)              
      )              
                     
                     
       INSERT INTO @COST_PO_GRN_FOR_ITEM_MASTER_LOOP (ItemId, PrimaryCost)              
       SELECT  C1.ItemId, Max(C1.PrimaryCost) PrimaryCost FROM @COST_PO_GRN_FOR_ITEM_MASTER C1              
       INNER JOIN                          
     (SELECT Max(cim.Date) Date, ItemId FROM @COST_PO_GRN_FOR_ITEM_MASTER cim              
     GROUP BY ItemId) C2              
       ON C1.Date = C2.Date              
       AND C1.ItemId = C2.ItemId              
       GROUP BY C1.ItemId          
                     
       UPDATE Item_Master              
       SET           
       --SELECT              
     PrimaryCost = c.PrimaryCost,              
     ModifiedBy = @ModifiedBy,              
     ModifiedDate = @CurrentDate          
       FROM Item_Master im              
       INNER JOIN @COST_PO_GRN_FOR_ITEM_MASTER_LOOP c              
       ON c.ItemId = im.ItemId              
                  
                  
       DECLARE @Item AS INT                
       DECLARE ItemBatchCur CURSOR  FORWARD_ONLY STATIC  FOR               
       SELECT @Item FROM @COST_PO_GRN_FOR_ITEM_MASTER_LOOP              
                     
       OPEN ItemBatchCur FETCH NEXT FROM ItemBatchCur INTO @Item              
       WHILE @@FETCH_STATUS = 0              
       BEGIN                
     --INTERFACE-AUDIT ENTRY              
     EXEC usp_Interface_Audit '','ITMMST',NULL,'ITEM_MASTER',@Item,NULL,NULL,NULL,NULL,'U',@ModifiedBy,''              
                  
     FETCH NEXT FROM ItemBatchCur INTO @ItemId              
       END              
                     
       CLOSE ItemBatchCur              
       DEALLOCATE ItemBatchCur              
                  
      END               
                  
      -------------------------------------------              
      --END UPDATE ITEM_MASTER PRIMARY COST               
      -------------------------------------------           
                
      PRINT 'END'             
              
   COMMIT transaction                              
 END  Try                                               
 Begin Catch                                         
    Set @outParam = '90001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()                                    
    SELECT Error_Message()                         
    Return ERROR_NUMBER()                                   
    Rollback transaction                        
                                     
                               
 END catch                        
                                   
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateInventroyOn_TO]    Script Date: 02/09/2014 13:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateInventroyOn_TO]                      
@TONumber varchar(20) ,                      
@outParam VARCHAR(500) OUTPUT                          
AS                        
BEGIN                  
                 
  DECLARE @ProcessDate DATETIME                        
        
  DECLARE @Status int,                    
   @ModifiedBy int,                    
   @INTransitBucketId int,                    
   @LocationId int,                    
   @ItemId int,                    
   @NetQuatity int,                    
   @SellableBucketId int ,            
   @TOBatchDetail  varchar(20)                         
                      
                     
  DECLARE @InventoryBatch  AS TABLE                          
   (                          
   TransType VARCHAR(20),                          
   TransTypeId VARCHAR(20),                          
   Date DATETIME,                          
   InventoryBatchNo VARCHAR(20),                          
   LocationID INT,                          
   ItemId INT,                          
   BatchNo VARCHAR(20),                          
   NetQuantity NUMERIC(12,4),                          
   BucketID INT,                          
   DiscountValue NUMERIC(12,4)                          
   )                           
                     
  SELECT Top 1 @SellableBucketId  = BucketId FROM Bucket WHERE ParentId Is Not Null AND Sellable = 1                                
  SET @ProcessDate=(SELECT th.CreationDate                    
                     FROM TO_Header th   WHERE th.TONumber=@TONumber)                                                  
  SET @LocationId=(SELECT th.SourceLocationID                    
                     FROM TO_Header th   WHERE th.TONumber=@TONumber)      
                  
  PRINT @ProcessDate                                            
  SET @Status=1                        
  SET @ModifiedBy=1                         
  SET @INTransitBucketId=9                        
                      
                      
  PRINT 'I m here - 1 ' + Convert(Varchar(20), GETDATE(), 20)                        
  --get invoice detail                
   BEGIN Try                          
  BEGIN TRANSACTION  ToMain     
      
--   IF EXISTS (SELECT TOP 1 *  FROM Inventory_real_time_history irth WHERE referencekey=@TONumber)    
--  BEGIN    
--   ROLLBACK    
--   return    
--  END    
        
       
	DECLARE @midVar AS varchar(2)
	SET @midVar=(SELECT substring(@TONumber,1,2)	 )
                            
      IF(@midVar<>'EX')
      Begin
                     
			 INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID)                      
				  SELECT 'TO', 3, ShippingDate, th.TONumber, SourceLocationId LocationId, td.ItemId, tbd.BatchNo, -1 * SUM(ISNULL(tbd.Quantity,0)) NetQuantity,      
				  td.BucketId                      
				  FROM TOBatchDetail tbd                      
				  INNER JOIN  TO_Detail td  ON tbd.TONumber = td.TONumber AND tbd.ItemId = td.ItemId  AND td.BucketId = tbd.FromSubBucketId                              
				  INNER JOIN TO_Header th  ON tbd.TONumber = th.TONumber                        
				  INNER JOIN Location_Master lm   ON lm.LocationId = th.SourceLocationId  AND ISNULL(th.Isexported,0) = 0                      
				  WHERE  ISNULL(td.BucketId,'')<>''                     
				  AND tbd.TONumber=@TONumber               
				  AND ShippingDate IS NOT NULL                     
				  AND th.Status IN(2,3)                      
				  AND th.IsProcessed = 0                       
				  --AND ShippingDate BETWEEN @ProcessDate AND DATEADD (second , -1, @LastProcessBusinessDate)                      
				  AND SourceLocationId = @LocationId                          
				   GROUP BY ShippingDate, th.TONumber,SourceLocationId, td.ItemId, tbd.BatchNo, td.BucketId                      
                          
      END
      ELSE
      BEGIN
      			INSERT INTO @InventoryBatch (TransType, TransTypeId, Date, InventoryBatchNo, LocationID, ItemId, BatchNo, NetQuantity, BucketID)                      
				  SELECT 'EO', 3, ShippingDate, th.TONumber, SourceLocationId LocationId, td.ItemId, tbd.BatchNo, -1 * SUM(ISNULL(tbd.Quantity,0)) NetQuantity,      
				  td.BucketId                      
				  FROM TOBatchDetail tbd                      
				  INNER JOIN  TO_Detail td  ON tbd.TONumber = td.TONumber AND tbd.ItemId = td.ItemId  AND td.BucketId = tbd.FromSubBucketId                              
				  INNER JOIN TO_Header th  ON tbd.TONumber = th.TONumber                        
				  INNER JOIN Location_Master lm   ON lm.LocationId = th.SourceLocationId  AND ISNULL(th.Isexported,0) = 1                      
				  WHERE  ISNULL(td.BucketId,'')<>''                     
				  AND tbd.TONumber=@TONumber               
				  AND ShippingDate IS NOT NULL                     
				  AND th.Status IN(2,3)                      
				  AND th.IsProcessed = 0                       
				  --AND ShippingDate BETWEEN @ProcessDate AND DATEADD (second , -1, @LastProcessBusinessDate)                      
				  AND SourceLocationId = @LocationId                          
				   GROUP BY ShippingDate, th.TONumber,SourceLocationId, td.ItemId, tbd.BatchNo, td.BucketId 
      		End
      	              
                                
        --Insert invoice detail into Stock_Ledger_Daily                        
		INSERT INTO Stock_Ledger_Daily (TransDate, HeaderId, BatchNo, LocationId, ItemId, BucketId, TransTypeId, TransQty, TransDiscountValue)                        
      SELECT date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId, SUM(ISNULL(NetQuantity,0)) NetQuantity, SUM(ISNULL(DiscountValue,0)) FROM @InventoryBatch ib                              
      GROUP BY date, InventoryBatchNo, BatchNo, LocationId, ItemId, BucketId, TransTypeId                      
            
            
         IF exists(SELECT 1 FROM InventoryLocation_Master sl    
     INNER JOIN @InventoryBatch ib ON sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)    
	BEGIN    
     
   UPDATE InventoryLocation_Master                        
   SET           
    -- select             
     Quantity =Quantity+ibt.NetQuantity ,    
     AvailableQuantity =AvailableQuantity+ibt.NetQuantity     
     FROM @InventoryBatch ibt                       
     Inner JOIN InventoryLocation_Master ilm                        
     ON ilm.BucketId = ibt.BucketId                        
     AND ilm.LocationId = ibt.LocationId                        
     AND ilm.ItemId = ibt.ItemId             
     --GROUP BY ibt.LocationId, ibt.ItemId, ibt.BucketId    
    
  END    
     
  ELSE    
  begin    
  INSERT INTO InventoryLocation_Master (LocationId, ItemId, BucketId, Quantity, AvailableQuantity, QuantityOnOrder, QuantityOnTransfer)                          
    SELECT LocationId, ItemId, BucketId,                           
   SUM(ISNULL(Quantity,0)) NetQuantity,                           
   SUM(ISNULL(Quantity,0)),             
   CASE WHEN ib.BucketId = @SellableBucketId THEN dbo.[ufn_GetStockLedgerPO_GRNQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId) ELSE 0 END,      
   0      
   --dbo.[ufn_GetStockLedgerTOIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId) ,      
   -- dbo.[ufn_GetStockLedgerTIQuantity](@ProcessDate, @ProcessDate,@LocationId,  ib.ItemId, ib.BucketId)                           
   FROM Inventory_LocBucketBatch ib                          
    WHERE (LocationID = @LocationId)             
    --AND ib.BatchNo=@TOBatchDetail                         
    AND NOT EXISTS (SELECT 1 FROM InventoryLocation_Master sl WHERE sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                          
    GROUP BY LocationId, ItemId, BucketId        
  end    
      
      
        
      
        
            
      --Insert invoice detail into Stock_Ledger_Daily                        
       INSERT INTO Inventory_LocBucketBatch_History(InventoryDate, BatchNo, LocationId, ItemId, BucketId, Quantity, Status,CreatedBy, CreatedDate, ReasonId, ItemName)                          
       SELECT            @ProcessDate,BatchNo, LocationId,im.ItemId, BucketId, SUM(ISNULL(NetQuantity,0)) NetQuantity,@Status , @ModifiedBy,@ProcessDate,1,im.ItemName      
       FROM @InventoryBatch ib       
       INNER JOIN Item_Master im ON im.ItemId = ib.ItemId                             
       GROUP BY date, InventoryBatchNo, BatchNo, LocationId, im.ItemId, BucketId, TransTypeId , im.ItemName      
             
             
           
      IF EXISTS (SELECT 1 FROM Inventory_LocBucketBatch sl     
      INNER JOIN @InventoryBatch ib  on sl.ItemId = ib.ItemId AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)    
      begin    
   UPDATE Inventory_LocBucketBatch                          
     SET             
     --select               
     Quantity =ilbb.Quantity+ibt.NetQuantity  
     FROM @InventoryBatch ibt                         
     Inner JOIN Inventory_LocBucketBatch ilbb                          
     ON ilbb.BucketId = ibt.BucketId                          
     AND ilbb.LocationId = ibt.LocationId                          
     AND ilbb.ItemId = ibt.ItemId   
     AND ilbb.BatchNo = ibt.BatchNo       
     --GROUP BY ibt.LocationId, ibt.ItemId, ibt.BucketId                     
             END    
      ELSE    
       BEGIN    
       INSERT INTO Inventory_LocBucketBatch(BatchNo, LocationId, ItemId, BucketId, Quantity, Status, CreatedBy, CreatedDate, ReasonId, ItemName, DocumentDate)         
    SELECT BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, @Status, @ModifiedBy, @ProcessDate, 1, im.ItemName, @ProcessDate       
    FROM @InventoryBatch ib                          
     INNER JOIN Item_Master im                           
     ON im.ItemId = ib.ItemId      
     INNER JOIN InventoryLocation_Master ilm ON ilm.ItemId = ib.ItemId AND  ilm.BucketId = ib.BucketId   AND ilm.LocationId=ib.LocationID                       
     WHERE (ib.LocationID = @LocationId and ib.BucketID=@INTransitBucketId )                          
     AND NOT EXISTS  (SELECT 1 FROM Inventory_LocBucketBatch sl      
      WHERE sl.ItemId = ib.ItemId AND sl.BatchNo = ib.BatchNo      
       AND sl.BucketId = ib.BucketId AND sl.LocationId = ib.LocationId)                          
    GROUP BY ib.BatchNo, ib.LocationId, ib.ItemId, ib.BucketId, ilm.AvailableQuantity, im.ItemName      
            
       END    
        
    --    IF(@midVar<>'EX')
    --  BEGIN
      	    
			 -- INSERT INTO Inventory_real_time_history     
			 --SELECT 'TO',ib.LocationID,@TONumber,ib.ItemId,ib.NetQuantity,ib.Date    
			 --FROM   @InventoryBatch ib    
    --  END
    --  ELSE
    --  	BEGIN
    --  						INSERT INTO Inventory_real_time_history     
				-- SELECT 'EO',ib.LocationID,@TONumber,ib.ItemId,ib.NetQuantity,ib.Date    
				-- FROM   @InventoryBatch ib    
    --  	END
         
   COMMIT transaction                    
 END  Try                                     
 Begin Catch                               
    Set @outParam = '90001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()                          
    SELECT Error_Message()               
    Return ERROR_NUMBER()                         
    Rollback transaction              
                           
                     
 END catch                      
                         
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TISave]    Script Date: 02/09/2014 13:05:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_TISave]
    @jsonForItems nvarchar(max),
    @TONumber varchar(20),
    @StatusId int,
    @LocationId int,
    @createdBy int,
    @tnumber varchar(20),
    @SWNo varchar(50),
    @Sdetail varchar(50),
    @Remark Varchar(50),
    
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	   BEGIN Transaction
	set nocount on ;
		Declare
		
			@iHnd					INT,					@BucketId				INT,			@SourceLocationId		INT,					@PackSize				INT,			@GrossWeight			Numeric(12,4),
			@TODate					VARCHAR(20),			@CreationDate			VARCHAR(20),	@ReceivedDate			VARCHAR(20),
								@ModifiedDate			VARCHAR(20),
			--@IndentNo				VARCHAR(20), 
			@DestinationLocationId  int,			
			@TotalTOQuantity		Numeric(12,4),			@TotalTOAmount			Money,			@CreatedDate			VARCHAR(20) ,
			@SystemDate				DateTime,				
			@IndexSeqNo				INT,					@ShippingBillNo			VARCHAR(20),	@ShippingDetails		VARCHAR(20),
			@ReceivedTime			VARCHAR(20),			@RefNumber				VARCHAR(100),	@Remarks				VARCHAR(100),
			@TOINumber				VARCHAR(20),			@ShippingDate			VARCHAR(20),	@RecCnt					VARCHAR(2000),
			@Exportvalue			int,@InputParam varchar(500),@TotalTOIQuantity NUMERIC(18,2),@TotalTOIAmount NUMERIC(18,4)

			Set @SystemDate = getdate()
			Declare @RowNo INT, @Row INT, @ItemId INT, @ItemCode VARCHAR(20), @ItemName VARCHAR(100), @UnitQty Numeric(12,4), @UOMId INT, @ItemUnitPrice Money, @ItemTotalAmount Money, @BatchNo VARCHAR(20), @ManufactureBatchNo VARCHAR(20)
			Declare @SeqNo VARCHAR(200)
			Set @iHnd = 0


		Exec sp_xml_preparedocument @iHnd OutPut, @inputParam

	DECLARE @MyHierarchy JSONHierarchy ,@XMLItems xml
           INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)
							
                            select  b.value('@ItemId', 'int') as ItemId,  
							  b.value('@Items', 'varchar(20)') as ItemCode,
							  b.value('@ItemName', 'varchar(100)') as ItemDescription,
							  b.value('@BatchNo', 'varchar(20)') as BatchNo,
							  b.value('@UnitPrice', 'Numeric(12,4)') as TransferPrice,
							  b.value('@TotalAmount', 'Money') as TotalAmount,
							  b.value('@BucketId', 'int') as BucketId,
							  b.value('@TOINo','varchar(50)') as TOINumber,
							  b.value('@RequestQty','float') as RequestQty,
							  b.value('@Weight','float') as Weight,
							  b.value('@MfgBatchNo','varchar(50)') as ManufactureBatchNo,
							  b.value('@Adjust','float') as AfterAdjustQty,
							  b.value('@MRP','Numeric(12,4)') as MRP,
							  b.value('@MfgDate','varchar(50)') as MfgDate,
							  b.value('@ExpDate','varchar(50)') as ExpDate,
							  	b.value('@Weight','float') as GrossWeightItem,
							  	  	b.value('@UOMId','int') as UOMId
								into #TI_Detail 
							  FROM @XMLItems.nodes('/root/item') a(b)
							  
							  alter table  #TI_Detail   add  RowNo Int Identity(1,1)  ;	
	

	      
	
		--Select * From #TIDetail


		SELECT  @TONumber			= TONumber,			@SourceLocationId	= SourceLocationId,	@DestinationLocationId=DestinationLocationID,	 
				@ReceivedDate		= GETDATE(),					
				@TotalTOQuantity	= TotalTOQuantity,	      @TotalTOAmount		= TotalTOAmount,		 		
				@CreatedDate		= CreatedDate,				@ModifiedDate			= ModifiedDate,
				@TOINumber			= TOINumber,			@PackSize			= PackSize,				@GrossWeight			= GrossWeight,
		    	@ReceivedTime		= GETDATE(),			
						@Exportvalue = Isexported	
		FROM
			 TO_Header where TONumber=@TONumber
       select @GrossWeight=SUM(TID.Weight*TID.AfterAdjustQty) ,@TotalTOIQuantity=SUM(TID.AfterAdjustQty) ,@TotalTOIAmount =SUM(TID.MRP*TID.AfterAdjustQty) from #TI_Detail TID

--		SELECT @ReceivedDate, @ReceivedTime
		IF Convert(VARCHAR(10),@ReceivedDate,120)='1900-01-01'
			SET @ReceivedDate = NULL
			
		SET @ReceivedTime = GetDate()

		DECLARE @LocationCode AS VARCHAR(20)
		SELECT @LocationCode = LocationCode From Location_Master Where LocationType = @DestinationLocationId

		IF @TNumber=''
		BEGIN
			SELECT @TOINumber = TOINumber, @ShippingDate = ShippingDate FROM TO_Header WHERE TONumber = @TONumber

			EXECUTE usp_GetSeqNo 'TI', @DestinationLocationId, @SeqNo Out
			SET @TNumber=@SeqNo

			INSERT INTO TIHeader	(TINumber,	TONumber,	TOINumber,	TOShippingDate, SourceLocationID,	DestinationLocationID,	ShippingDetails,
				VehicleNo,			ReceivedBy , 	ReceivedDate,	ReceivedTime,	TotalTOQuantity,	TotalTOAmount,	TotalTIQuantity,	TotalTIAmount,
					PackSize,	GrossWeight,	Remarks,	Status,	CreatedBy,		CreatedDate,	ModifiedBy,	ModifiedDate,Isexported)
				VALUES				(@TNumber,	@TONumber,	@TOINumber, @ShippingDate,	
				@SourceLocationId,	@DestinationLocationId, @SWNo,	@Sdetail,
					@createdBy,	@ReceivedDate,	@ReceivedTime,			@TotalTOQuantity,
						@TotalTOAmount,	@TotalTOIQuantity,	@TotalTOIAmount, @PackSize,	
						@GrossWeight,	@Remark,	@StatusId,		@createdBy,
							@SystemDate,	@createdBy,@SystemDate, @Exportvalue)

			Exec [usp_Interface_Audit] '', 'TRNIN', @LocationCode, 'TIHeader', @TNumber, NULL, NULL, NULL, NULL, 'I', @createdBy	,  @RecCnt	OUTPUT	
		END
		ELSE
		BEGIN
			-- Check Concurrency, Get DBDate
			Declare @DBDate DateTime
			Select @DBDate = (Select ModifiedDate = (Case When ModifiedBy Is Not Null Then ModifiedDate Else CreatedDate End) From TO_Header Where  TONumber = @TNumber)
			
			If @ModifiedDate <> Convert(VARCHAR(20),@DBDate,120)
			BEGIN
				SET @outParam = 'INF0022'
			 	select @outParam OutParam
			      ROLLBACK
				Return
			END

			DECLARE @OldStatus AS INT
			SELECT @OldStatus = Status FROM TO_Header WHERE TONumber = @TNumber
			-- Update In TO_Header
			UPDATE TIHeader
			SET VehicleNo			=	@ShippingBillNo,		ShippingDetails		=	@ShippingDetails,
				GrossWeight			=	@GrossWeight,			PackSize			=	@PackSize,
				TotalTOQuantity		=	@TotalTOQuantity,		TotalTOAmount		=	@TotalTOAmount,
				ModifiedBy			=	@createdBy,			ModifiedDate		=	@SystemDate	,
				Status				=	@StatusId, 
				ReceivedDate		=   CASE WHEN @StatusId = 2 THEN @ReceivedDate ELSE NULL END,
				ReceivedTime		=   CASE WHEN @StatusId = 2 THEN GetDate() ELSE NULL END,
				Remarks				=	@Remarks
			WHERE TINumber = @TNumber

			If @StatusId = 2 
			BEGIN
				Exec [usp_Interface_Audit] '', 'TRNIN', @LocationCode, 'TIHeader', @TNumber, NULL, NULL, NULL, NULL, 'U', @createdBy	,  @RecCnt	OUTPUT	
			END

		END

		-- If Confirm, Update the Quantity in InventoryLocation_Master and Inventory_LocBucketBatch Tabled
		IF @StatusId = 2
		BEGIN
	


				-- Close TO, If Confirm Received
				Update TO_Header Set Status =  3 WHERE TONumber = @TONumber
		END

		IF @StatusId = 2 OR @StatusId = 1
		BEGIN
			DELETE FROM TIDetail WHERE TINumber = @TNumber
			DELETE FROM TIBatchDetail WHERE TINumber = @TNumber


		
			INSERT INTO TIDetail (	TINumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOMId, TransferPrice, TotalAmount, --IndentNo, 
									CreatedBy, CreatedDate, BucketId)
			SELECT @TNumber,ROW_NUMBER() OVER (Order By ItemId) as RowNo,ItemId, ItemCode, ItemDescription, SUM(AfterAdjustQty) AS AfterAdjustQty, UOMId, TransferPrice, SUM(TotalAmount) As TotalAmount, --IndentNo, 
								@createdBy,GETDATE(),BucketId			
			FROM #TI_Detail 
			GROUP BY ItemId, ItemCode, ItemDescription, UOMId, TransferPrice, BucketId

			INSERT INTO TIBatchDetail (TINumber, RowNo, ItemId, Quantity, BatchNo,  MfgDate, ExpDate, ManufactureBatchNo, MRP, RcvSubBucketId, CreatedBy, CreatedDate)
			SELECT @TNumber, ROW_NUMBER() OVER (Order By ItemId) as RowNo, ItemId, AfterAdjustQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId, @createdBy
			, @SystemDate
			FROM #TI_Detail 
			
		END


	SELECT TINumber TNumber,
			Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate, 
			1 as IndexSeqNo
	FROM TIHeader 
	WHERE TINumber = @TNumber
	If(@StatusId=2)
	       BEGIN
				EXEC SP_UpdateInventroyOn_TI @tnumber ,@outParam OUT
				IF(@outParam!='')
				   BEGIN
					 Select @outParam OutParam
					 ROLLBack
					 Return
			         
			         
				   END

           END
       COMMIT Transaction
	END TRY
	BEGIN CATCH
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		Select @outParam OutParam
		 ROLLBACK
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CustomerReturnSave]    Script Date: 02/09/2014 13:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_CustomerReturnSave]
	@InvoiceNO	varchar(20),
	@ReturnNo	varchar(20), 
	@customerType int,
	@jsonForItems nvarchar(max),
	@isPVBVZero int,
	@Remarks varchar(20),
	@Status int,
	@locationId int,
	@createdBy int,
	@PayableAmt Numeric(18,4),
	@DeductionAmt Numeric(18,4),
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
    SET NOCOUNT ON 
 
	Begin Try
	  BEGIN Transaction
	 
		Declare
			@iHnd					INT,					
			@BucketId				INT,			
		
			@DistributorPCId		VARCHAR(50),		
			@ApprovedDate			DateTime,			
			
			@ModifiedDate			VARCHAR(20),
	
			@Indentised				INT,					
			@SystemDate				DATETIME,		
			
			@RecCnt					VARCHAR(2000),
			@TotalQuantity			NUMERIC(12,4),
			@TotalAmount			NUMERIC(12,4),
			@LocationCode			VARCHAR(20),
			@AmountPayable			NUMERIC(12,4),
		
			@SourceLocationId		INT,
			@SourceLocationCode		VARCHAR(50),
			--Added by Kaushik for Customer Return Save in case of Invoice Return
			@TaxAmount				NUMERIC(12,4),
			@InvoiceAmount			NUMERIC(12,4),
			@InvoiceDate			DateTime,
			@DistributorId			VARCHAR(20),
			@isReturnOnAnotherLoc   int,
			   @BOId int ,
			@inputParam VARCHAR(20)
   
					Set @SystemDate = getdate()
					SET @TotalAmount=0.00;
					Declare @RowNo INT, @Row INT, @ItemId INT, @ItemCode VARCHAR(20), @ItemName VARCHAR(100), @Quantity Numeric(12,4), @BatchNo VARCHAR(20), @ManufactureBatchNo VARCHAR(20)
					Declare @SeqNo VARCHAR(200)
					Set @iHnd = 0
					  select @LocationCode=LocationCode from Location_Master where LocationId=@locationId
						Exec sp_xml_preparedocument @iHnd OutPut, @inputParam
						SET @DistributorPCId	= @InvoiceNO ;	
						
						select @isReturnOnAnotherLoc=KeyCode1 from Parameter_Master where ParameterCode='ISRETURNONANOTHERLOC'
				
		Declare @ReturnCustomer Table 
		(
		
			RowNo INT Identity(1,1),
			ItemId INT,
			ItemCode VARCHAR(20),
			ItemName VARCHAR(100),
			BatchNo VARCHAR(20),
			BucketId INT,
			ManufactureBatchNo VARCHAR(20),
			Quantity	NUMERIC(12,4)
		)
		
		--@BOId it is for location iD on BO
		IF (@CustomerType = 3 OR @CustomerType = 4) 
			BEGIN 
			  
			  select @BOId=BOId from CIHeader where InvoiceNo=@InvoiceNO
			  if @isReturnOnAnotherLoc =0 
			     BEGIN 
			        --invoice can not return on another location
			        if @BOId!=@locationId  
			         BEGIN
							set @outParam='L001'
							select @outParam as OutParam
							RollBack 
							Return 
					 END  
			     END
			     
			 END
		
		--in case of invoice return header ,detail fill on base of CI tables
		
			--in case of invoice return header ,detail fill on base of grid
			--by default value for @jsonForItems is [] ;
    if ( @jsonForItems!='[]')
    BEGIN 
            DECLARE @myHierarchy JSONHierarchy,
			@xml XML ;
			 INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@MyHierarchy)
				
--confusion when itemId,ManufacturingNo same in ItemBatch database One row extra added

      							SELECT 
							  b.value('@ItemCode', 'varchar(20)') as ItemCode,
							  b.value('@ReturnQty', 'float') as ReturnQty,
							  b.value('@DistributorPrice', 'float') as DistributorPrice,
							  b.value('@ItemDescription', 'varchar(20)') as ItemDescription,
							  b.value('@ManufacturerBatchNo', 'varchar(20)') as ManufacturerBatchNo,
							  b.value('@BatchNo', 'varchar(20)') as BatchNo
								into #BatchDetail 
							  FROM @xml.nodes('/root/item') a(b)  
				Insert INTO @ReturnCustomer(	ItemId, ItemCode, ItemName, BatchNo, BucketId,	
				  ManufactureBatchNo, Quantity)
			select IM.ItemId,BD.ItemCode,IM.ItemName,IBD.BatchNo,5,BD.ManufacturerBatchNo,BD.ReturnQty from #BatchDetail BD LEFT JOIN 
			Item_Master IM ON IM.ItemCode=BD.ItemCode LEFT JOIN ItemBatch_Detail IBD
			ON IBD.ManufactureBatchNo=BD.ManufacturerBatchNo AND IM.ItemCode=BD.ItemCode AND  IBD.BatchNo=BD.BatchNo
      if @customerType=4   --price accoring to invoice
          BEGIN	
			select 					
				@TotalAmount		= SUM(CID.DP*RC.Quantity),	
				
		     
				@TaxAmount			= SUM((CID.DP-CID.UnitPrice)*RC.Quantity) -- for Customer Return Save in case of Invoice Return
								from CIDetail CID LEFT JOIN  @ReturnCustomer RC ON
				CID.ItemId=RC.ItemId
				 where InvoiceNo =@InvoiceNO
		
			select 	@SourceLocationId=@locationId,@InvoiceAmount		= InvoiceAmount,		@InvoiceDate        = InvoiceDate,
				@DistributorId		= DistributorId,	@AmountPayable=0 from CIHeader where InvoiceNo=@InvoiceNO
	      END
	  else If  @customerType=3
	            BEGIN 
	            select @TotalAmount =CIH.InvoiceAmount ,
	            @TaxAmount=CIH.TaxAmount 
	            
	             from CIHeader CIH where InvoiceNo=@InvoiceNO
	            END 
	  else
	  	BEGIN 
	  		SELECT	@TotalAmount		= SUM(IM.DistributorPrice*RC.Quantity)	
	
							from Item_Master IM LEFT JOIN  @ReturnCustomer RC ON
				IM.ItemId=RC.ItemId
				 
	  	END

           
    END
  
  DECLARE @SumOfInvoice Numeric(18,0)
   if @customerType=3
     BEGIN
   --  select * from @ReturnCustomer
       select @SumOfInvoice=SUM(CID.Quantity-iSNULL(RC.Quantity,0))  from  CIDetail 
       CID LEFT JOIN @ReturnCustomer RC On RC.ItemId=CID.ItemId 
       where CID.InvoiceNo=@InvoiceNO 
       --  select @SumOfInvoice=SUM(CID.Quantity-RC.Quantity)  from  CIDetail 
       --CID LEFT JOIN 
       --(select Quantity ,ItemId from  CIDetail 
       --where InvoiceNo='I/B0010/11/038640' ) RC On RC.ItemId=CID.ItemId 
       --where CID.InvoiceNo='I/B0010/11/038641' 
        update CIHeader set Status=3 where InvoiceNo=@InvoiceNO
       IF @SumOfInvoice!=0
       BEGIN
           SELECT 'INV001' AS OutParam
           set @outParam='INV001'
           RollBack
           return 
       END
     END
		SELECT @SourceLocationCode = LocationCode FROM Location_Master WHERE LocationId = @locationId
		IF EXISTS (
					SELECT 1 FROM ItemBatch_Detail ibd 
					INNER JOIN @ReturnCustomer rc 
					ON ibd.ItemId = rc.ItemId
					AND ibd.BatchNo = rc.BatchNo
					AND CONVERT(Varchar(10),ibd.ExpDate,112) <= CONVERT(Varchar(10),GetDate(),112)
				  )
			BEGIN
                -- exp batch can not return 
			select 'INF0207'  OutParam
				SET @outParam = 'INF0207'
				ROLLBACK
				RETURN
			END	

			if @customerType!=4
			
			  BEGIN 
					IF EXISTS (SELECT 1 FROM RetCustomer_Header WHERE DistributorPCId = @DistributorPCId And StatusId=3)
					BEGIN 
					--already return
					select 'INF0235'  OutParam
						SET @outParam = 'INF0235'
						ROLLBACK
						RETURN
					END
			  END

		IF ISNULL(NULLIF(@ReturnNo,''),'-1')='-1'
		BEGIN
		
		
			EXECUTE usp_GetSeqNo 'CR', @LocationId, @SeqNo Out
			SET @ReturnNo=@SeqNo
	
			INSERT INTO RetCustomer_Header (ReturnNo, CustomerType,DistributorPCId, LocationID,  StatusId, CreatedBy, CreatedDate, 
			ModifiedBy, ModifiedDate, TotalAmount, DeductionAmount, AmountPayable, TaxAmount, InvoiceAmount,MakePVBVZero)
				VALUES (@ReturnNo, @CustomerType, @DistributorPCId, @locationId, @Status, @createdBy,
				 @SystemDate, @createdBy, @SystemDate, @TotalAmount, @DeductionAmt, @PayableAmt, @TaxAmount, @InvoiceAmount,@isPVBVZero)
			--INTERFACE AUDIT ENTRY FOR NEW INSERT
			
		END
		ELSE
		BEGIN
			
			-- Check Concurrency, Get DBDate
			DECLARE @DBDate DATETIME
			SELECT @DBDate = (SELECT ModifiedDate = (CASE WHEN ModifiedBy IS NOT NULL THEN ModifiedDate
			 ELSE CreatedDate END) FROM dbo.RetCustomer_Header WHERE  ReturnNo = @ReturnNo)
			
			If @ModifiedDate <> Convert(VARCHAR(20),@DBDate,120)
			BEGIN
		        SELECT 'INF0022' as OutParam
				SET @outParam = 'INF0022'
				SELECT @outParam OutParam
				 ROLLBACK
				Return
			END

			-- Update In RetCustomer_Header
			UPDATE RetCustomer_Header
			SET TotalAmount			=	@TotalAmount,
				DeductionAmount		=	@DeductionAmt,
				AmountPayable		=	@PayableAmt,
				ModifiedBy			=	@createdBy,			
				ModifiedDate		=	@SystemDate	,
				StatusId			=	@Status , 
				ApprovedBy			=	CASE WHEN @Status = 3 THEN @createdBy ELSE NULL END,		
				ApprovedDate		=   CASE WHEN @Status = 3 THEN @SystemDate ELSE NULL END,
				Remarks				=	@Remarks
			WHERE ReturnNo = @ReturnNo	

			--INTERFACE AUDIT ENTRY FOR UPDATE
			
		END

		-- If Confirm, Update the Quantity in InventoryLocation_Master and Inventory_LocBucketBatch Tabled
		IF @Status = 3
	     BEGIN
					-- DECLARE @retItemId int ,
					--  @retQuantity numeric(18,2),@retBatchNo varchar(20),@retManufactureBatchNo varchar(20);

					--DECLARE retCus CURSOR FOR  SELECT ItemId, Quantity , BatchNo, ManufactureBatchNo
					--				FROM @ReturnCustomer
							
							
					--			OPEN retCus
					--FETCH NEXT FROM retCus
					--	INTO @retItemId,@retQuantity,@retBatchNo,@retManufactureBatchNo

					--WHILE @@FETCH_STATUS = 0
						
					--	BEGIN 
										  
					--		UPDATE Inventory_LocBucketBatch SET Quantity = Quantity + @retQuantity
					--		FROM Inventory_LocBucketBatch 
					--		WHERE ItemId=@retItemId AND LocationId=@locationId AND BatchNo=@retBatchNo
					--		AND BucketId=5
					--		 IF @@ROWCOUNT=0
					--		  BEGIN
					--		     INSERT INTO Inventory_LocBucketBatch (BatchNo,BucketId,CreatedBy,
					--		     CreatedDate,DocumentDate,ItemId,
					--				ItemName,LocationId,Quantity,ReasonId,Status) SELECT 
					--				@retBatchNo,5,@createdBy,GETDATE(),GETDATE(),@retItemId,ItemName
					--				,@locationId,@retQuantity,0,1
					--				 FROM Item_Master where ItemId=@retItemId
					--		  END
						
						
					--	  FETCH NEXT FROM retCus
					--	INTO @retItemId,@retQuantity,@retBatchNo,@retManufactureBatchNo
							  
					--	END
					--	CLOSE retCus
					--	DEALLOCATE retCus

			-- Distributor Inactive And 
			-- PC Location In active 
			IF @Status = 3
				BEGIN
					IF  @CustomerType = 1 -- Distributor Type
					BEGIN

						DECLARE @CurDistributorId AS INT
						DECLARE CurDistributor CURSOR LOCAL FOR
							SELECT DistributorId FROM DistributorMaster WHERE UplineDistributorId = CAST(@DistributorPCId AS INT)
						OPEN CurDistributor
						FETCH NEXT FROM CurDistributor INTO @CurDistributorId

						WHILE (@@FETCH_STATUS = 0)
						BEGIN

							EXEC usp_Interface_Audit '','DISTMST','','DistributorMaster',@CurDistributorId,NULL,NULL,NULL,NULL,'U',@createdBy,@reccnt OUTPUT

							FETCH NEXT FROM CurDistributor INTO @CurDistributorId

						END	---cursor while end
						
						CLOSE CurDistributor;
						DEALLOCATE CurDistributor;
						
						DECLARE @UplineDistributorId AS INT
						SELECT @UplineDistributorId = UplineDistributorId FROM DistributorMaster WHERE DistributorId = CAST(@DistributorPCId AS INT)

						UPDATE DistributorMaster SET UplineDistributorId = @UplineDistributorId WHERE UplineDistributorId = CAST(@DistributorPCId AS INT)

						UPDATE DistributorMaster SET DistributorStatus = 3 -- Resigned 
						WHERE DistributorId = CAST(@DistributorPCId AS INT)
						
						EXEC usp_Interface_Audit '','DISTMST','','DistributorMaster',@DistributorPCId,NULL,NULL,NULL,NULL,'U',@createdBy,@reccnt OUTPUT
					END
			ELSE IF @CustomerType = 2
					BEGIN
						UPDATE Location_Master Set Status = 0 WHERE LocationCode = @DistributorPCId
						-- Added Code For Interface Audit, 29 Jan 2010
						EXEC [usp_Interface_Audit] '', 'LOCMST', '', 'Location_Master', @DistributorPCId, NULL, NULL, NULL, NULL, 'U', @createdBy, @RecCnt	OUTPUT
					END

				END
		END

		-- Approved, Created
		IF @Status = 3 OR @Status = 1
		BEGIN

			DELETE FROM RetCustomerBatch_Detail WHERE ReturnNo = @ReturnNo
			DELETE FROM RetCustomer_Detail WHERE ReturnNo = @ReturnNo

           
			IF (@CustomerType = 3 OR @CustomerType = 4) 
			BEGIN 
	
			     
				INSERT INTO RetCustomer_Detail (ReturnNo, RowNo, ItemId, ItemDescription, ReturnQty)
				SELECT @ReturnNo, ROW_NUMBER() OVER (Order By ItemId) as RowNo, ItemId, ItemName, SUM(Quantity) AS Qty
				FROM @ReturnCustomer   GROUP BY ItemId,ItemName 
				
				INSERT INTO RetCustomerBatch_Detail(ReturnNo, RowNo, ItemId, BatchNo, BucketId, Quantity)
				SELECT @ReturnNo, rc.RowNo,  rc.ItemId, rc.BatchNo, rc.BucketId, rc.Quantity From @ReturnCustomer rc
			END 
			ELSE
			BEGIN 
				INSERT INTO RetCustomer_Detail (ReturnNo, RowNo, ItemId, ItemDescription, ReturnQty)
				SELECT @ReturnNo, ROW_NUMBER() OVER (order by ItemId)as RowNo, ItemId, ItemName, SUM(Quantity) AS Qty
				FROM @ReturnCustomer
				GROUP BY ItemId, ItemName

				INSERT INTO RetCustomerBatch_Detail(ReturnNo, RowNo, ItemId, BatchNo, BucketId, Quantity)
				SELECT @ReturnNo, rcd.RowNo,  rcd.ItemId, rc.BatchNo, rc.BucketId, rc.Quantity From @ReturnCustomer rc
				INNER JOIN RetCustomer_Detail rcd
				ON  rc.ItemId = rcd.ItemId 
				AND rcd.ReturnNo = @ReturnNo
			END

			IF (@isPVBVZero=1 AND @Status=3) 
			BEGIN
			   If(@customerType =3 OR @customerType =4 )
						BEGIN
						UPDATE CIheader set TotalBV = 0, TotalPV = 0 where InvoiceNo = @DistributorPCId
						UPDATE CIDetail  Set LineBV=0 ,LinePV =0 where InvoiceNo = @DistributorPCId AND ItemId
						 in(select ItemId from @ReturnCustomer) 
						END
			END
			
			

		END
          if(@Status=3)
                BEGIN
                  EXEC SP_UpdateInventroyOn_Return @ReturnNo,  @outParam OUT
                    if(@outParam !='')
                      BEGIN
                          SELECT  @outParam OutParam
                          ROllBACK
                          Return
                      END
                
               END
	SELECT ReturnNo,@TotalAmount TotalAmount,
			Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate
		FROM RetCustomer_Header 
	WHERE ReturnNo = @ReturnNo
     COMMIT Transaction
	END TRY
	BEGIN CATCH
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		Select @outParam OutParam
		ROLLBack
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TOSave]    Script Date: 02/09/2014 13:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_TOSave]
    
	@jsonForItems	nvarchar(max),
	@TOINumber  varchar(20),
	@statusId int,
	@LocationId int, 
	@CreatedBy int,
		@PackSize				INT,
		@ExpectedDeliveryDate	VARCHAR(20),
		@ShippingBillNo			VARCHAR(20),
    	@ShippingDetails		VARCHAR(20),
    	@RefNumber				VARCHAR(100),
		@Remarks				VARCHAR(100),
		@Grossweight            Numeric(12,4),
	@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	 	set nocount on ;
	 
	   BEGIN Transaction
		Declare
			@iHnd					INT,					@BucketId				INT,			@SourceLocationId		INT,
			@DestinationLocationId	INT,				
			@TODate					VARCHAR(20),			@CreationDate			VARCHAR(20),	@ShippingDate			VARCHAR(20),
								@ModifiedBy				INT,			@ModifiedDate			VARCHAR(20),
			--@IndentNo				VARCHAR(20),			
			@TNumber				VARCHAR(20),
			@TotalTOQuantity		Numeric(12,4),			@TotalTOAmount			Money,			@CreatedDate			VARCHAR(20) ,
			@Indentised				Int,					@SystemDate				DateTime,	
			@IndexSeqNo				INT,					
			@Isexported				Bit,
					
			@LocationCode			VARCHAR(20),			@RecCnt					VARCHAR(2000),
			@ExporterRef			varchar(200),			@OtherRef				varchar(200),	@BuyerOtherthanConsignee varchar(200),
			@PreCarriage			Varchar(200),			@PlaceofReceiptbyPreCarrier varchar(200),@VesselflightNo			varchar(200),
			@PortofLoading			varchar(200),			@PortofDischarge		varchar(200),	@PortofDestination		varchar(200),
			@TermsofDelivery		Varchar(200),			@DELIVERY				varchar(200),	@PAYMENT				varchar(200),
			@BuyerOrderNo			Varchar(200),			@BuyerOrderDate			varchar(20)

			Set @SystemDate = getdate()
			
			Declare @SeqNo VARCHAR(200)
			Set @iHnd = 0


           DECLARE @MyHierarchy JSONHierarchy ,@XMLItems xml
           INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)


                          

                            select  b.value('@ItemId', 'int') as ItemId,  
							  b.value('@Items', 'varchar(20)') as ItemCode,
							  b.value('@ItemName', 'varchar(100)') as ItemDescription,
							  b.value('@BatchNo', 'varchar(20)') as BatchNo,
							  b.value('@UnitPrice', 'Numeric(12,4)') as TransferPrice,
							  b.value('@TotalAmount', 'Money') as TotalAmount,
							  b.value('@BucketId', 'int') as BucketId,
							  b.value('@TOINo','varchar(50)') as TOINumber,
							  b.value('@RequestQty','float') as RequestQty,
							  b.value('@Weight','float') as Weight,
							  b.value('@MfgBatchNo','varchar(50)') as ManufactureBatchNo,
							  b.value('@Adjust','float') as AfterAdjustQty,
							  b.value('@MRP','Numeric(12,4)') as MRP,
							  b.value('@MfgDate','varchar(50)') as MfgDate,
							  b.value('@ExpDate','varchar(50)') as ExpDate,
							   b.value('@EachCartonQty','float') as EachCartonQty,
							  b.value('@TOINo','varchar(50)') as ContainerNOFromTo,
							  	b.value('@Weight','float') as GrossWeightItem,
							  	  	b.value('@UOMId','int') as UOMId
								into #TO_Detail 
							  FROM @XMLItems.nodes('/root/item') a(b)
							  
							  alter table  #TO_Detail   add  RowNo Int Identity(1,1)  ;	
	

		
			DECLARE @ItemCodeDuplicate as VARCHAR(800), @ItemCodeDuplicateModified as VARCHAR(800)
			SET @ItemCodeDuplicate =''
			SET @ItemCodeDuplicateModified = ''
			SELECT @ItemCodeDuplicate = @ItemCodeDuplicate + ',' + im.ItemCode From Inventory_LocBucketBatch ilb
			INNER JOIN Item_Master im 
			ON im.ItemId = ilb.ItemId
			WHERE ilb.ItemId IN (SELECT ItemId FROM #TO_Detail) AND Ilb.LocationId=@LocationId
			Group By im.ItemCode, ilb.ItemId, BatchNo, LocationId, BucketId 
			Having Count(1)>1
			
			--SELECT @ItemCodeDuplicate 
			-- Remove Comma from @ItemCodeDuplicate, 
			-- If length >0 
			-- SET @outParam = 'INF0226' + @ItemCodeDuplicate
			SELECT @ItemCodeDuplicateModified = REPLACE(@ItemCodeDuplicate, ',', '') 
			
			--SELECT @ItemCodeDuplicateModified 

			If LEN(@ItemCodeDuplicateModified) > 0
			BEGIN
				SET @outParam = 'INF0229' 
				SELECT @outParam OutParam 
				ROLLBACK
				RETURN 
			END


		SELECT  @TOINumber	= head.TOINumber,@SourceLocationId=SourceLocationId,@DestinationLocationId = DestinationLocationId, 
				@CreationDate= GETDATE(), 
				@TotalTOQuantity	= head.TotalTOIQuantity,@TotalTOAmount= head.TotalTOIAmount,@Indentised= Indentised, 
				@CreatedDate	= GETDATE(),			@ModifiedBy			= @CreatedBy
				,			@ModifiedDate			= GETDATE(),
			   @Isexported = Isexported				
				
		FROM
			   TOI_Header	head
					Inner Join TOI_Detail det
					On head.TOINumber = det.TOINumber
					Where	
					 head.TOINumber=@TOINumber
			
		SELECT @LocationCode = LocationCode FROM Location_Master WHERE LocationId = @SourceLocationId
		--Select @GrossWeight

--		SELECT @TNumber,'ddd'
		Declare @cnt int
		set @cnt = (Select count(TONumber) from TO_Header where TOINumber = @TOINumber)
		--IF (@cnt='0')
		--BEGIN
		IF IsNull(@TNumber,'-1')='-1'
		BEGIN
		IF (@cnt='0' AND @StatusId >='1')
		BEGIN
			
				EXECUTE usp_GetSeqNo 'TO', @SourceLocationId, @SeqNo Out
		
			SET @TNumber=@SeqNo

			INSERT INTO TO_Header (TONumber, TOINumber,	SourceLocationID, DestinationLocationID, ShippingWayBillNo, ShippingDetails, 
				 GrossWeight, PackSize, Status, RefNumber, Remarks, CreationDate, TotalTOQuantity, TotalTOAmount, Indentised, ShippingDate, 
				  ExpectedDeliveryDate,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,Isexported)
				VALUES (@TNumber, @TOINumber, @SourceLocationId, @DestinationLocationId, @ShippingBillNo, @ShippingDetails,
				  @GrossWeight, @PackSize, @StatusId, @RefNumber, @Remarks, @SystemDate, @TotalTOQuantity, @TotalTOAmount,
				   @Indentised, @SystemDate, NULLIF(@ExpectedDeliveryDate, '1900-01-01 00:00:00'), @ModifiedBy, @SystemDate,
				   
				    @ModifiedBy, @SystemDate,@Isexported
						)
		END	

		ELSE IF(@cnt<>'0' AND @StatusId<>'1')
		BEGIN
		SET @outParam = 'INF0226' 
		  ROLLBACK
		 SELECT 'INF0226' OutParam
		   Return
		END
		 IF (@StatusId = 2)
            
			Exec [usp_Interface_Audit] '', 'TRNOUT', @LocationCode, 'TO_Header', @TNumber, NULL, NULL, NULL, NULL, 'I', @ModifiedBy, @RecCnt OUTPUT
--			SELECT @TNumber, 'dd'

		END

		ELSE
		BEGIN
			
			-- Check Concurrency, Get DBDate
			Declare @DBDate DateTime
			Select @DBDate = (Select ModifiedDate = (Case When ModifiedBy Is Not Null Then ModifiedDate Else CreatedDate End) From TO_Header Where  TONumber = @TNumber)
			
			If @ModifiedDate <> Convert(VARCHAR(20),@DBDate,120)
			BEGIN
				SET @outParam = 'INF0022' 
				SELECT @outParam OutParam
				ROLLBACK
				Return
			END

			DECLARE @OldStatus AS INT
			SELECT @OldStatus = Status FROM TO_Header WHERE TONumber = @TNumber
			-- Update In TO_Header
			UPDATE TO_Header
			SET ShippingWayBillNo	=	@ShippingBillNo,		ShippingDetails		=	@ShippingDetails,
				GrossWeight			=	@GrossWeight,			PackSize			=	@PackSize,
				TotalTOQuantity		=	@TotalTOQuantity,		TotalTOAmount		=	@TotalTOAmount,
				ModifiedBy			=	@ModifiedBy,			ModifiedDate		=	@SystemDate	,
				Status				=	@StatusId, 
				ShippingDate		=   CASE WHEN @StatusId = 2 THEN @SystemDate ELSE NULL END,
				RefNumber			=	@RefNumber,				Remarks				=	@Remarks,
				ExpectedDeliveryDate=	NULLIF(@ExpectedDeliveryDate, '1900-01-01 00:00:00'),
				ExporterRef			= @ExporterRef,			OtherRef					= @OtherRef,					BuyerOtherthanConsignee = @BuyerOtherthanConsignee,
				PreCarriage			= @PreCarriage,			PlaceofReceiptbyPreCarrier	= @PlaceofReceiptbyPreCarrier,	VesselflightNo			= @VesselflightNo,
				PortofLoading		= @PortofLoading,		PortofDischarge				= @PortofDischarge,				PortofDestination		= @PortofDestination,
				TermsofDelivery		= @TermsofDelivery,		DELIVERY					= @DELIVERY ,					PAYMENT					= @PAYMENT,
				BuyerOrderNo		= @BuyerOrderNo,		BuyerOrderDate				= @BuyerOrderDate
			WHERE TONumber = @TNumber
			--Update TOI Status To Closed
			
		END
--END
--ELSE
--BEGIN
--SET @outParam = 'INF0226' 
--END
		-- If Confirm, Update the Quantity in InventoryLocation_Master and Inventory_LocBucketBatch Tabled
		IF @StatusId = 2
		BEGIN
             
			
			--DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity Numeric(18,4),@DiscountValue Numeric(18,4),
			--							@tempDiscount Numeric(18,4)
										
			--								DECLARE INVLOCBUCKBATCH CURSOR FOR SELECT BD.BatchNo,BD.BucketId,BD.ItemId,BD.AfterAdjustQty  FROM #TO_Detail BD
											          
			--								             OPEN INVLOCBUCKBATCH
			--								 FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity
                               
   --                         WHILE @@FETCH_STATUS = 0
   --                           BEGIN
   --                           set @tempQuantity=-@tempQuantity ;
   --                        SELECT @tempDiscount =0.00 ;
   --                         SET  @DiscountValue=0.00 ;
   --                         EXEC sp_updateInventoryInStockLedger 3,@TNumber,@SourceLocationId,@tempItemID,@tempBatchNo,@tempQuantity,@tempBucketId,
   --                         @DiscountValue ,0,@outParam OUT
                            
   --                         if @outParam!=''
   --                                BEGIN
   --                                SELECT @outParam OutParam
   --                                  CLOSE INVLOCBUCKBATCH
                     
   --                                  DEAllOCATE INVLOCBUCKBATCH
   --                                ROLLBACK
   --                                RETURN
   --                                END
   --                         FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity
   --                           END
                              
   --                           CLOSE INVLOCBUCKBATCH
                              
   --                           DEAllOCATE INVLOCBUCKBATCH
							

			Update TOI_Header Set Status =  5 WHERE TOINumber = @TOINumber
			--Exec [usp_Interface_Audit] '', 'TRNORDINS', @LocationCode, 'TOI_Header', @TOINumber, NULL, NULL, NULL, NULL, 'U', @ModifiedBy,  @RecCnt	OUTPUT	

			Exec [usp_Interface_Audit] '', 'TRNOUT', @LocationCode, 'TO_Header', @TNumber, NULL, NULL, NULL, NULL, 'I', @ModifiedBy	, @RecCnt	OUTPUT

		END
--Select @StatusId
		IF @StatusId = 2 OR @StatusId = 1
		BEGIN

			--SELECT 'T', ItemId, AfterAdjustQty As Quantity, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId FROM @TO_Detail
			--Select 'TT2', * From dbo.Inventory_LocBucketBatch Where ItemId in (2,4) And LocationId = 2

			DELETE FROM TO_Detail WHERE TONumber = @TNumber
			DELETE FROM TOBatchDetail WHERE TONumber = @TNumber

			--INSERT INTO TO_Detail (TONumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOM, TransferPrice, TotalAmount, IndentNo, CreatedBy, CreatedDate)
		
			INSERT INTO TO_Detail (	TONumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOM, TransferPrice, TotalAmount, --IndentNo, 
									CreatedBy, CreatedDate, BucketId,EachCartonQty, ContainerNOFromTo ,GrossWeightItem)
			SELECT @TNumber,ROW_NUMBER() OVER (Order By ItemId) as RowNo,ItemId, ItemCode, ItemDescription, SUM(AfterAdjustQty) AS AfterAdjustQty, UOMId, TransferPrice, SUM(TotalAmount) As TotalAmount, --IndentNo, 
										@CreatedBy,GETDATE(),	BucketId,EachCartonQty, ContainerNOFromTo ,GrossWeightItem
			FROM #TO_Detail
			GROUP BY ItemId, ItemCode, ItemDescription, UOMId, TransferPrice, BucketId,EachCartonQty, ContainerNOFromTo ,GrossWeightItem


			INSERT INTO TOBatchDetail (TONumber, RowNo, ItemId, Quantity, BatchNo,  MfgDate, ExpDate, ManufactureBatchNo, MRP, FromSubBucketId, CreatedBy, CreatedDate)
			SELECT @TNumber,  RowNo, ItemId, AfterAdjustQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId, @ModifiedBy, @SystemDate
			FROM #TO_Detail
			GROUP BY ItemId, RowNo, AfterAdjustQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId
		END
       EXEC UpdateInventroyOn_TO @TNumber ,@outParam OUt
      IF(@outParam!='')
         
       BEGIN
            Select @outParam OutParam
            RollBack
            Return 
       END


	SELECT TONumber TNumber,
			Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate, 
			1 as IndexSeqNo
	FROM TO_Header 
	WHERE TONumber = @TNumber
	  COMMIT Transaction
	END TRY
	BEGIN CATCH
		
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		select @outParam OutParam
		ROLLBACK 
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GRNSave]    Script Date: 02/09/2014 13:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Procedure [dbo].[sp_GRNSave]
    @jsonForBatchDetail nvarchar(max),
	@PONumber	Varchar(20),
	@ambendMentNO varchar(20),
	@Status int,
	@GRNNo      varchar(20),
	@challanNo varchar(20),
	@challanDate varchar(20),
	@invoiveNo varchar(20),
	@invoiceDate varchar(20),
	@invoiceTax float,
	@invoiceAmt float,
	@vehicleNo varchar(20),
	@receiveBy varchar(20),
	@noOfBox   int,
	@locationId int,
	@createdBy int,
	@TransporterName varchar(20),
	@TransporterNo varchar(20),
	@GatepassNo varchar(20),
	@jsonForViewDetail nvarchar(max),
	--@GrossWeight Numeric(18,4),
	@outParam	VARCHAR(500) OUTPUT	
AS
BEGIN
	BEGIN TRY
                SET NOCOUNT ON ;
		DECLARE
			@iHnd						Int
		SET @iHnd = 0
		Declare @LocationCode VARCHAR(20
		)
		DECLARE @reccnt VARCHAR(2000)
		,@inputParam varchar(500)
		EXEC sp_xml_preparedocument @iHnd OUTPUT, @inputParam
		
		DECLARE @GRN Table (
		GRNNo Varchar(25),				
				PONumber Varchar(20),				
				AmendmentNo Int,
				ChallanNo varchar(20),
				ChallanDate DATETIME,
				GrossWeight varchar(20),
				NoOfBoxes INT,
				InvoiceNo varchar(20),
				InvoiceDate DATETIME,
				DBInvoiceTaxAmount decimal,
				DBInvoiceAmount decimal,
				ShippingDetails varchar(100),
				VehicleNo varchar(20),
				ReceivedBy varchar(20),
				Status INT,
				CreatedBy INT,				
				ModifiedBy INT,
				ModifiedDate DATETIME,
				DestLocationID INT
		)
		insert into @GRN
		select  
			@GRNNo,
			@PONUmber,
			@ambendmentNO,
		    @challanNo,
		    @challanDate,
		    '',
		    @noOfBox,
		     @invoiveNo,
		     @invoiceDate,
		       @invoiceTax,
		     @invoiceAmt,
		     '',
		   @vehicleNo,
		   @receiveBy,
		   @status,
		   @createdBy,
		   @createdBy,
		   getDate(),
		    @locationId
		      
			
					
					
		
		--Select * from @GRN
		DECLARE @GRNDETAIL TABLE
		(
		        SerialNo Int identity(1,1),
				GRNNo Varchar(25),			
				PONumber Varchar(20),			
				AmendmentNo Int,
				ItemId INT,
				ItemCode varchar(20),
				PurchaseUOM varchar(20),
				ItemDescription varchar(100),
				DBPOQty decimal,
				DBAlreadyReceivedQty decimal,
				AlreadyInvoicedQty decimal,
				DBBalanceQty decimal,
				DBChallanQty decimal,
				DBInvoiceQty decimal,		
				DBReceivedQty decimal
			) 
			INSERT INTO @GRNDETAIL
		SELECT	@GRNNo,
					PD.[PONumber],
					PD.[AmendmentNo],
					PD.[ItemId],
					I.[ItemCode],
					ISNULL(UM.[UOMName],'')[PurchaseUOM],
					ISNULL(PD.[ItemDescription],'')[ItemDescription],
					ISNULL([UnitQty],0)[POQty],
					ISNULL(GRN.[AlreadyReceivedQty],0)[AlreadyReceivedQty],
					ISNULL(GRN.[AlreadyInvoicedQty],0)[AlreadyInvoicedQty],
					ISNULL([UnitQty],0)-ISNULL(GRN.[AlreadyReceivedQty],0)[BalanceQty],
					0 [ChallanQty],
					0 [InvoiceQty],
					0 [ReceivedQty]
			FROM [PO_Detail]PD
			JOIN [PO_Header] PH1 ON PD.[PONumber]=PH1.[PONumber] AND PD.[AmendmentNo]=PH1.[AmendmentNo]
			JOIN [Item_Master]I ON PD.[ItemId]=I.[ItemId]
			JOIN [UOM_Master] UM ON PD.[PurchaseUOM]=UM.UOMId			
			LEFT JOIN
			(
				SELECT sum(GD1.ReceivedQty) AlreadyReceivedQty,sum(GD1.InvoiceQty) AlreadyInvoicedQty, GD1.PONumber, GD1.ItemId
				FROM [GRN_Detail]  GD1
				INNER JOIN GRN_Header gh
				ON gh.GRNNo = GD1.GRNNo
				where GD1.[PONumber]=@PONumber AND GH.Status!=2 and	GH.Status!=1
				GROUP BY GD1.PONumber, GD1.ItemId
					
			)GRN
			ON PD.[PONumber]=GRN.[PONumber] 
			--AND PD.[AmendmentNo]=GRN.[AmendmentNo] 
			AND PD.[ItemId]=GRN.[ItemId]

			
			WHERE PD.[PONumber]=@PONumber AND PD.AmendmentNo=(Select MAX([AmendmentNo]) from [PO_Header]PH where PD.PONumber=PH.PONumber )
			AND PH1.Status in (1,3)
			--AND ISNULL(GRN.[AlreadyReceivedQty],0)<ISNULL([UnitQty],0) Order By PD.ItemId

		
       DECLARE @myHierarchy JSONHierarchy,
			@xml XML ;
			 INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForBatchDetail)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XML=dbo.ToXML(@MyHierarchy)
				  
--[{"":"11045","":"200","":"123","":"sd-10",
--"":"24-Oct-2013","":"29-Oct-2013"}
							SELECT 
							  b.value('@ItemCode', 'varchar(20)') as ItemCode,
							  b.value('@ReceivedQty', 'float') as ReceivedQty,
							  b.value('@MRP', 'float') as MRP,
							  b.value('@ManufacturerBatch', 'varchar(20)') as ManufacturerBatchNumber,
							  b.value('@ManufacturingDate', 'varchar(20)') as ManufacturingDate,
							  b.value('@ExpiryDate', 'varchar(20)') as ExpiryDate
								into #BatchDetail 
							  FROM @xml.nodes('/root/item') a(b)  
							       
			    DECLARE @myHierarchy2 JSONHierarchy,
			@xml2 XML ;
			
			 INSERT INTO @myHierarchy2
				SELECT * FROM parseJSON(@jsonForViewDetail)
				--SELECT dbo.ToXML(@MyHierarchy)
			--	 select 'Here' OutParam  
				SELECT @xml2=dbo.ToXML(@MyHierarchy2)
				
--[{"":"11045","":"200","":"123","":"sd-10",
--"":"24-Oct-2013","":"29-Oct-2013"}

							SELECT 
							  b.value('@ItemId', 'int') as ItemId,
							  
							  b.value('@InvoiceQty', 'int') as InvoiceQty
								into #itemDetail 
							  FROM @xml2.nodes('/root/item') a(b)  
							          
		                                     
		 DECLARE   @GRNBatchDetail TABLE
		 ( 
		       GRNNo Varchar(25),			
				SerialNo	 int,
				ItemId INT,
				BatchNumber varchar(20),
				RowNo int,
				ManufacturingDate datetime,
				ManufacturerBatchNumber varchar(20),
				ExpiryDate datetime,
				MRP decimal,
				ReceivedQty decimal	
				)
				insert into @GRNBatchDetail select @GRNNo,'',IM.ItemId,'',''
				, GBD.ManufacturingDate,
				  GBD.ManufacturerBatchNumber,
			      GBD.ExpiryDate

			     ,GBD.MRP,GBD.ReceivedQty
				 from #BatchDetail GBD Left Join Item_Master
				IM on IM.ItemCode= GBD.ItemCode
				
				DECLARE @tempItemId INT,@MfgBatchNo varchar(20)   , @RowNo int,@SerialNo int,@prevItemId int
			DEClARE batchDetail CURSOR for select ItemId,ManufacturerBatchNumber from @GRNBatchDetail order By ItemId
			
		 OPEN batchDetail  
     FETCH NEXT FROM batchDetail  
      INTO @tempItemId,@MfgBatchNo
   
     SET @RowNo=1

    SET @prevItemId=@tempItemId
     WHILE @@FETCH_STATUS = 0 
     
       BEGIN
             IF @prevItemId=@tempItemId
                BEGIN
                  update @GRNBatchDetail set RowNo=@RowNo,SerialNo=(select SerialNo from @GRNDETAIL where ItemId=@tempItemId) where ItemId=@tempItemId and ManufacturerBatchNumber=@MfgBatchNo
				   SET   @RowNo=  @RowNo+1 ;
                END
             ELSE 
                BEGIN 
        
                SET  @RowNo = 1;
                  update @GRNBatchDetail set RowNo=@RowNo,SerialNo=(select SerialNo from @GRNDETAIL where ItemId=@tempItemId) where ItemId=@tempItemId and ManufacturerBatchNumber=@MfgBatchNo
                    SET   @RowNo=  @RowNo+1 ;
                  
                END
       
          SET @prevItemId=@tempItemId ;
       
          
		   FETCH NEXT FROM batchDetail  
			INTO @tempItemId,@MfgBatchNo
			
		END			
				CLOSE batchDetail
			DEALLOCATE batchDetail	
			
			
			Declare @ReceivedQty int
			DEClARE batchDetail CURSOR for select ItemId,SUM(ReceivedQty) from @GRNBatchDetail GROUP By ItemId
			
		 OPEN batchDetail  
     FETCH NEXT FROM batchDetail  
      INTO @tempItemId,@ReceivedQty
   
   
     WHILE @@FETCH_STATUS = 0 
     
       BEGIN
             
         update @GRNDETAIL set DBReceivedQty=@ReceivedQty,DBChallanQty =@ReceivedQty,DBInvoiceQty=(select InvoiceQty from #itemDetail where ItemId=@tempItemId),DBBalanceQty=DBBalanceQty-@ReceivedQty where ItemId=
         @tempItemId
		   FETCH NEXT FROM batchDetail  
			INTO @tempItemId,@ReceivedQty
			
		END			
				CLOSE batchDetail
			DEALLOCATE batchDetail	

				
				
			Declare @ModifiedDate DATETIME
			set @ModifiedDate=getdate();
			SELECT @LocationCode=LocationCode FROm Location_Master WHERE LocationId=@locationID
			
			IF(ISNULL(@GRNNo,'')='')
			BEGIN
				--VALIDATION FOR OPEN GRN
				if(Exists(SELECT 1 FROM [GRN_Header] WHERE [PONumber]=@PONumber AND [Status]=1))
					BEGIN
										
						SET @outParam='INF0066'	
						select @outParam OutParam
						ROLLBACK
						return 							
					END
			END
			--ELSE
			--	BEGIN
			--		-- VALIDATION FOR RECORD MODIFY
			--		if EXISTS(SELECT 1 FROM [GRN_Header] WHERE GRNNo=@GRNNo AND [ModifiedDate]<>@ModifiedDate)
			--			BEGIN
			--			Set @outParam='INF0044'
			--			return
			--			END			
			--	END

				BEGIN TRANSACTION 			
				BEGIN TRY
				-- NEW AND CLOSE STATE
				IF(@status=1 OR @status=3)
				BEGIN				
					IF(ISNULL(@GRNNo,'')='')
						BEGIN					
							Declare @SeqNo Varchar(20)			
							Execute usp_GetSeqNo 'POGRN', @locationID, @SeqNo Out
							SET @GRNNo=@SeqNo
							UPDATE  @GRN set GRNNo=@GRNNo, ModifiedDate=GETDATE()
							UPDATE @GRNDetail set GRNNo=@GRNNo
							UPDATE @GRNBatchDetail set GRNNo=@GRNNo	
							--select * from @GRN ;
							--select * from @GRNDetail;
							--  select * from @GRNBatchDetail
							--UPDATE @GRNBatchTaxDetail set GRNNo=@GRNNo	
							
					--			print 'GRN HEader Insert'
								INSERT INTO [GRN_Header] ([GRNNo],[GRNDate],[PONumber],[AmendmentNo],[PODate]
													,[ChallanNo],[ChallanDate],[GrossWeight],[NoOfBoxes]
													,[InvoiceNo],[InvoiceDate],InvoiceTaxAmount,InvoiceAmount,[ShippingDetails],[VehicleNo]
													,[ReceivedBy],[Status],[CreatedBy],[CreatedDate],[ModifiedBy]
													,[ModifiedDate],GatePassNo,TransportNo,TransportName)
								SELECT GRNNo, GETDATE() ,
										G.PONumber,G.AmendmentNo,PH.PODate,
										ChallanNo,ChallanDate,GrossWeight,NoOfBoxes,
										InvoiceNo,InvoiceDate,DBInvoiceTaxAmount,DBInvoiceAmount,
										PH.ShippingDetails,VehicleNo,
										ReceivedBy,G.Status,G.CreatedBy,GETDATE(),G.ModifiedBy,GETDATE()[ModifiedDate],@GatepassNo,
										@TransporterNo,@TransporterName
								FROM @GRN G
								JOIN PO_Header PH ON G.[PONumber]=PH.[PONumber] 
								AND PH.[AmendmentNo]=G.AmendmentNo
														
								--'Interface Audit Entry'							
								
	                        
						END
					ELSE
						BEGIN				
							if EXISTS(SELECT 1 FROM [GRN_Header] WHERE GRNNo=@GRNNo AND STATUS=3)
							--UPDATE ONLY INVOICE DETAILS
							BEGIN				
								UPDATE [GRN_Header]
									SET [InvoiceNo]=G.InvoiceNo,[InvoiceDate]=G.InvoiceDate,InvoiceTaxAmount=G.DBInvoiceTaxAmount,InvoiceAmount=G.DBInvoiceAmount
									,[ModifiedBy]=G.ModifiedBy,[ModifiedDate]= GetDATE()													
									FROM [GRN_Header] GH, @GRN G WHERE GH.GRNNo	=G.GRNNo
								
						
								if(@Status=3)
							
								UPDATE [GRN_Detail] SET [InvoiceQty]=GD.[DBInvoiceQty]
								FROM [GRN_Detail] G,@GRNDetail GD
									WHERE G.[GRNNo]=GD.[GRNNo] AND G.[ItemId]=GD.[ItemId]							
				
				
								 ROLLBACK
								RETURN							
							END
							ELSE
								BEGIN
								-- GRN Exists But in Created Status
						   -- 'Update Header'--
								UPDATE [GRN_Header]
								SET [GRNDate]=GetDATE(),[ChallanNo]=G.ChallanNo,[ChallanDate]=G.ChallanDate,[GrossWeight]=G.GrossWeight,[NoOfBoxes]=G.NoOfBoxes
									,[InvoiceNo]=G.InvoiceNo,[InvoiceDate]=G.InvoiceDate,InvoiceTaxAmount=G.DBInvoiceTaxAmount,InvoiceAmount=G.DBInvoiceAmount
									,[ShippingDetails]=G.ShippingDetails,[VehicleNo]=G.VehicleNo
									,[ReceivedBy]=G.ReceivedBy,[ModifiedBy]=G.ModifiedBy,Status=G.Status
									,[ModifiedDate]= GetDATE()													
								FROM [GRN_Header] GH, @GRN G WHERE GH.GRNNo	=G.GRNNo	
								
								--INTERFACE AUDIT							
								if(@Status=3)
								exec usp_Interface_Audit '','GRN',@LocationCode,'GRN_Header',@GRNNo,NULL,NULL,NULL,NULL,'I',@createdBy,@reccnt OUTPUT			 		
								
								END
						END								
						
						DELETE FROM [GRNBatch_Detail] WHERE GRNNo=@GRNNo				
						
						DELETE FROM [GRN_Detail] WHERE GRNNo=@GRNNo
						
						
						
						INSERT INTO [GRN_Detail]([GRNNo],[PONumber],[AmendmentNo],[SerialNo],[ItemId],[ItemCode],[PurchaseUOM],[ItemDescription]
													,[POQty],[AlreadyReceivedQty],[AlreadyInvoicedQty],[BalanceQty],[ChallanQty],[InvoiceQty],[ReceivedQty])
								SELECT GRNNo,PONumber,AmendmentNo,SerialNo,ItemId,ItemCode,PurchaseUOM,ItemDescription,
						DBPOQty,DBAlreadyReceivedQty,DBAlreadyReceivedQty,DBBalanceQty,DBChallanQty,DBInvoiceQty,DBReceivedQty			 	
								FROM @GRNDETAIL  WHERE DBReceivedQty>0			
							
							--select * from @GRNDETAIL
						 
						INSERT INTO [GRNBatch_Detail]([GRNNo],[SerialNO],[ItemId],[BatchNumber],[RowNo],
						  [ManufacturingDate],[ManufacturerBatchNo],[ExpiryDate],[MRP],[ReceivedQty])
							SELECT  GRNNo,SerialNo,ItemId,ISNULL([BatchNumber],''),RowNo,ManufacturingDate,ManufacturerBatchNumber,ExpiryDate,MRP,ReceivedQty
							FROM @GRNBatchDetail 				
						
						--INSERT INTO [GRNBatchTax_Detail]([GRNNo],[BatchNo],[ItemId],[SerialNo],[RowNo],[TaxCode],[TaxPercentage],[TaxGroup],[TaxAmount],[GroupOrder])
						--	SELECT GRNNo,ISNULL(BatchNumber,''),ItemId,SerialNo,RowNo,TaxCode,TaxPercentage,TaxGroup,TaxAmount,GroupOrder 
						--	FROM @GRNBatchTaxDetail
						
					-- FOR CLOSE STATE ONLY--
					IF(@Status=3)
					BEGIN							
							
							Declare @batchstart VARCHAR(20)
							DECLARE @record INT						
							
							update @GRNBatchDetail SET BatchNumber=I.BatchNo
							FROM @GRNBatchDetail G,[ItemBatch_Detail] I WHERE 
							G.[ItemId]=I.ItemId AND
							Convert(varchar(10),G.ManufacturingDate,101)=Convert(varchar(10),I.[MfgDate],101)
							AND G.MRP=I.[MRP] AND  Convert(varchar(10),G.ExpiryDate,101)=Convert(varchar(10),I.[ExpDate],101)
							AND G.ManufacturerBatchNumber=I.[ManufactureBatchNo]						
						
							SELECT @record= count(*) from @GRNBatchDetail WHERE ISNULL(BatchNumber,'')=''
							-- TOTAL RECORDS IN BATCHDETAIL
							
							Declare @SeqNo1 Varchar(20)
							Execute usp_GetSeqNo 'BAT', @locationID, @SeqNo1 Out,@record

							Declare @p AS INT
							SET @p = CAST(@SeqNo1 AS INT) 						

							CREATE TABLE  #TEMP 
							(
								seqno INT Identity(1,1),
								GRNNo Varchar(25),			
								SerialNo	 int,
								ItemId INT,
								BatchNumber varchar(20),
								ManufacturingDate datetime,
								ManufacturerBatchNumber varchar(20),
								ExpiryDate datetime,
								MRP decimal							
							)
								
							DBCC checkident(#TEMP, RESEED,  @p);

							INSERT INTO #TEMP (GRNNo ,SerialNo,ItemId,BatchNumber,ManufacturingDate,ManufacturerBatchNumber,ExpiryDate ,MRP)
							SELECT  GRNNo,
								SerialNo,
								ItemId,
								BatchNumber,
								ManufacturingDate,
								ManufacturerBatchNumber,
								ExpiryDate,
								MRP						
							FROM @GRNBatchDetail G
							WHERE ISNULL(BatchNumber,'')=''

							IF (IsNull(@locationID,-1) != -1)
								SELECT @LocationCode = LocationCode FROM LOCATION_MASTER WHERE LocationID = @locationID
							ELSE
								SELECT @LocationCode = '-1'
			
							UPDATE #TEMP 
							SET BatchNumber='BAT/'+@LocationCode+'/'+REPLICATE('0', 5 - LEN(seqno)) + CONVERT(VARCHAR(5),seqno) 

							INSERT INTO [ItemBatch_Detail]([ItemBatchId],[ItemId],[BatchNo],[MfgDate],[ExpDate],[MRP],[ManufactureBatchNo],[Status],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
							SELECT [BatchNumber],ItemId,[BatchNumber],	ManufacturingDate,ExpiryDate,MRP,ManufacturerBatchNumber,1,@createdBy,GETDATE(),@createdBy,GETDATE() 
							FROM #TEMP 
							
							SELECT *,ROW_NUMBER() Over (Order By ih.[BatchNumber]) 'RowNumber'
							INTO #ITEMBATCH 
							FROM #TEMP ih 
								
							DECLARE @ICount INT,@M INT
							DECLARE @ILocCode VARCHAR(20),@INO VARCHAR(50)
							SET @M=1
							SELECT @ICount=Count(*) FROM #ITEMBATCH
										
							WHILE (@M<=@ICount)
							BEGIN	
								PRINT @M					
								SELECT @INO=I.[BatchNumber]
								FROM #ITEMBATCH I	 WHERE RowNumber=@M																
								Exec [usp_Interface_Audit] '', 'ITMBATCH', @LocationCode, 'ITEMBATCH_DETAIL', @INO, NULL, NULL, NULL, NULL, 'I', @createdBy, @reccnt OUTPUT					
								SET @M=@M+1
							END							
					
							---updated here--
							update @GRNBatchDetail SET BatchNumber=T.BatchNumber
							FROM @GRNBatchDetail G,#TEMP T WHERE 
							G.ItemId=T.ItemId AND
							Convert(varchar(10),G.ManufacturingDate,101)=Convert(varchar(10),T.ManufacturingDate,101)
							 AND  Convert(varchar(10),G.ExpiryDate,101)=Convert(varchar(10),T.ExpiryDate,101)
							AND G.ManufacturerBatchNumber=T.ManufacturerBatchNumber AND ISNULL(G.BatchNumber,'')=''
							
							--UPDATE @GRNBatchTaxDetail SET BatchNumber=T.BatchNumber
							--FROM @GRNBatchTaxDetail G,@GRNBatchDetail T
							--WHERE  G.ItemId=T.ItemId AND G.SerialNo=T.SerialNo AND G.RowNo=T.RowNo					
							
							DROP TABLE #TEMP			
															
							DELETE FROM [GRNBatchTax_Detail] WHERE GRNNo=@GRNNo
													
							DELETE FROM [GRNBatch_Detail] WHERE GRNNo=@GRNNo
					
							INSERT INTO [GRNBatch_Detail]([GRNNo],[SerialNO],[ItemId],[BatchNumber],[RowNo],[ManufacturingDate],
							[ManufacturerBatchNo],[ExpiryDate],[MRP],[ReceivedQty])
							SELECT  GRNNo,SerialNo,ItemId,[BatchNumber],[RowNo],ManufacturingDate,ManufacturerBatchNumber
							,ExpiryDate,MRP,ReceivedQty
							FROM @GRNBatchDetail
						
							--INSERT INTO [GRNBatchTax_Detail]([GRNNo],[BatchNo],[ItemId],[SerialNo],[RowNo],[TaxCode],[TaxPercentage],[TaxGroup],[TaxAmount],[GroupOrder])
							--SELECT GRNNo,BatchNumber,ItemId,[SerialNo],RowNo,TaxCode,TaxPercentage,TaxGroup,TaxAmount,GroupOrder 
							--FROM @GRNBatchTaxDetail	
															
							Declare @sellablebucketId  int	
							SET @sellablebucketId=(SELECT [BucketId] FROM [Bucket] where [ParentId] IS NOT NULL AND [Sellable]=1)
                             EXEC SP_UpdateInventroyOn_GRN @GRNNo,@outparam
                             if(@outparam!='')
                                BEGIN
                                Select @outParam OutParam
                                  Rollback
                                  RETURN
                                 END
                            
							--update [Inventory_LocBucketBatch] SET [Quantity]=[Quantity]+T.ReceivedQty
							--FROM [Inventory_LocBucketBatch] I ,@GRNBatchDetail T
							--WHERE I.[BucketId]=@sellablebucketId AND [LocationId]=@locationID AND 
							--I.[ItemId]=T.ItemId AND I.[BatchNo]=T.[BatchNumber]
							
							--INSERT INTO [Inventory_LocBucketBatch] ([LocationId],[ItemId],[BucketId],[BatchNo],[ItemName],[Quantity],[DocumentDate],[ReasonId],[Status],[CreatedBy],[CreatedDate])
							--SELECT @locationID,T.ItemId,@sellablebucketId,T.[BatchNumber],G.[ItemDescription],sum(T.ReceivedQty)as ReceivedQty,Getdate(),	0,1,@createdBy,GETDATE()
							--FROM @GRNBatchDetail T JOIN GRN_DETAIL G ON T.GRNNo=G.GRNNo AND T.[ItemId]=G.[ItemId]
							--WHERE NOT EXISTS
							--(
							--	SELECT 1 FROM [Inventory_LocBucketBatch] I WHERE I.[BucketId]=@sellablebucketId AND [LocationId]=@locationID AND [ItemId]=T.ItemId AND I.[BatchNo]=T.[BatchNumber]
							--)		
							--group by T.ItemId,T.[BatchNumber],G.[ItemDescription]
							
							    
			--DECLARE @tempItemID2 INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity Numeric(18,4),@DiscountValue Numeric(18,4),
			--							@tempDiscount Numeric(18,4)
							
			--								DECLARE INVLOCBUCKBATCH CURSOR FOR SELECT BD.BatchNumber,5,BD.ItemId,BD.ReceivedQty
			--								  FROM @GRNBatchDetail BD
											          
			--								             OPEN INVLOCBUCKBATCH
			--								 FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID2,@tempQuantity
                               
   --                         WHILE @@FETCH_STATUS = 0
   --                           BEGIN
   --                        SELECT @tempDiscount =0.00 ;
   --                         SET  @DiscountValue=0.00 ;
   --                         EXEC sp_updateInventoryInStockLedger 2,@GRNNo,@locationId,@tempItemID2,@tempBatchNo,@tempQuantity,@tempBucketId,
   --                         @DiscountValue ,0,@outParam OUT
                            
   --                         if @outParam!=''
   --                                BEGIN
   --                                SELECT @outParam OutParam
   --                                  CLOSE INVLOCBUCKBATCH
                     
   --                                  DEAllOCATE INVLOCBUCKBATCH
   --                                ROLLBACK
   --                                RETURN
   --                                END
   --                         FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID2,@tempQuantity
   --                           END
                              
   --                           CLOSE INVLOCBUCKBATCH
                              
   --                           DEAllOCATE INVLOCBUCKBATCH

							
							
							

							Declare @AmendmentNo int
							SELECT @AmendmentNo= Max(AmendmentNo) from PO_Header PH where PH.[PONumber]=@PONumber

							IF NOT EXISTS
							(
								SELECT 1 from PO_Detail PD WHERE ItemId NOT in 
								(
									SELECT ItemId from [GRN_Detail] GD
									JOIN GRN_Header GH
									ON GD.[GRNNo]=GH.[GRNNo]
									WHERE GD.[PONumber]=@PONumber AND  GD.[BalanceQty]= 0 AND GH.Status=3
								)
								AND [PONumber]=@PONumber AND AmendmentNo=(Select Max(AmendmentNo) from PO_Header PH where PH.[PONumber]=PD.[PONumber])
							)
							BEGIN 
							
								UPDATE PO_Header SET Status=5, [ModifiedDate]=Getdate() WHERE PONumber=@PONumber
								--Interface Audit Entry								
								--EXEC usp_Interface_Audit '','PO',@LocationCode,'PO_Header',@PONumber,@AmendmentNo,NULL,NULL,NULL,'U',@createdBy,@reccnt OUTPUT			
							END		
							ELSE 
								IF EXISTS(SELECT 1 FROM PO_Header where PONumber=@PONumber AND Status=1)
									BEGIN
										UPDATE PO_Header SET Status=3, [ModifiedDate]=Getdate() WHERE PONumber=@PONumber
										--Interface Audit entry							
										EXEC usp_Interface_Audit '','PO',@LocationCode,'PO_Header',@PONumber,@AmendmentNo,NULL,NULL,NULL,'U',@createdBy,@reccnt OUTPUT			
									END	
						END				 					

				END	
				ELSE 
			IF(@status=2)
					BEGIN					
						UPDATE [GRN_Header] 
						SET [Status]=@status ,[ModifiedDate]=GetDATE() ,[ModifiedBy]=@createdBy 							
						where GRNNo=@GRNNo
					END
				SELECT GRNNo As GRNNo FROM @GRN				
				
		Commit TRANSACTION 	
			
			END TRY
			BEGIN CATCH
				Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
			      Select @outParam OutParam
				Rollback 
				Return ERROR_NUMBER()
				
			END CATCH		
		
	End Try
	Begin Catch
		Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
		select @outParam OutParam
		Return ERROR_NUMBER()
	End Catch
End
GO
/****** Object:  StoredProcedure [dbo].[sp_ExportSave]    Script Date: 02/09/2014 13:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_ExportSave]
    
	@jsonForItems	nvarchar(max),
	@TOINumber  varchar(20),
	@statusId int,
	@LocationId int, 
	@CreatedBy int,
		@PackSize				INT,
		@ExpectedDeliveryDate	VARCHAR(20),
		@ShippingBillNo			VARCHAR(20),
    	@ShippingDetails		VARCHAR(20),
    	@RefNumber				VARCHAR(100),
		@Remarks				VARCHAR(100),
		@Grossweight            Numeric(12,4),
		@ExporterRef			varchar(200),
		@OtherRef				varchar(200),
		@BuyerOtherthanConsignee varchar(200),
		@PreCarriage			Varchar(200),
		@PlaceofReceiptbyPreCarrier varchar(200),
		@VesselflightNo			varchar(200),
		@PortofLoading			varchar(200),	
		@PortofDischarge		varchar(200),
		@PortofDestination		varchar(200),
		@TermsofDelivery		Varchar(200),		
		@DELIVERY				varchar(200),
		@PAYMENT				varchar(200),
		@BuyerOrderNo			Varchar(200),	
		@BuyerOrderDate			varchar(20),
		@EOIECNumber            varchar(50),
		@outParam		VARCHAR(500) OUTPUT
AS
Begin
	Begin Try
	
	set nocount on ;
	   BEGIN Transaction
		Declare
			@iHnd					INT,					@BucketId				INT,			@SourceLocationId		INT,
			@DestinationLocationId	INT,				
			@TODate					VARCHAR(20),			@CreationDate			VARCHAR(20),	@ShippingDate			VARCHAR(20),
			@ModifiedBy				INT,			@ModifiedDate			VARCHAR(20),			
			@TNumber				VARCHAR(20),
			@TotalTOQuantity		Numeric(12,4),			@TotalTOAmount			Money,			@CreatedDate			VARCHAR(20) ,
			@Indentised				Int,					@SystemDate				DateTime,	
			@IndexSeqNo				INT,						
			@LocationCode			VARCHAR(20),			@RecCnt					VARCHAR(2000)	
			
			

			Set @SystemDate = getdate()
			
			Declare @SeqNo VARCHAR(200)
			Set @iHnd = 0 ;


           DECLARE @MyHierarchy JSONHierarchy ,@XMLItems xml
           INSERT INTO @myHierarchy
				SELECT * FROM parseJSON(@jsonForItems)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)


                          

                            select  b.value('@ItemId', 'int') as ItemId,  
							  b.value('@Items', 'varchar(20)') as ItemCode,
							  b.value('@ItemName', 'varchar(100)') as ItemDescription,
							  b.value('@BatchNo', 'varchar(20)') as BatchNo,
							  b.value('@UnitPrice', 'Numeric(12,4)') as TransferPrice,
							  b.value('@TotalAmount', 'Money') as TotalAmount,
							  b.value('@BucketId', 'int') as BucketId,
							  b.value('@TOINo','varchar(50)') as TOINumber,
							  b.value('@RequestQty','float') as RequestQty,
							  b.value('@Weight','float') as Weight,
							  b.value('@MfgBatchNo','varchar(50)') as ManufactureBatchNo,
							  b.value('@Adjust','float') as AfterAdjustQty,
							  b.value('@MRP','Numeric(12,4)') as MRP,
							  b.value('@MfgDate','varchar(50)') as MfgDate,
							  b.value('@ExpDate','varchar(50)') as ExpDate,
							   b.value('@CartonQty','float') as EachCartonQty,
							  b.value('@ContainerNo','varchar(50)') as ContainerNOFromTo,
							  	b.value('@Weight','float') as GrossWeightItem,
							  	  	b.value('@UOMId','int') as UOMId
								into #TO_Detail 
							  FROM @XMLItems.nodes('/root/item') a(b)
							  
							  alter table  #TO_Detail   add  RowNo Int Identity(1,1)  ;	
	

		
			DECLARE @ItemCodeDuplicate as VARCHAR(800), @ItemCodeDuplicateModified as VARCHAR(800)
			SET @ItemCodeDuplicate =''
			SET @ItemCodeDuplicateModified = ''
			SELECT @ItemCodeDuplicate = @ItemCodeDuplicate + ',' + im.ItemCode From Inventory_LocBucketBatch ilb
			INNER JOIN Item_Master im 
			ON im.ItemId = ilb.ItemId
			WHERE ilb.ItemId IN (SELECT ItemId FROM #TO_Detail) AND Ilb.LocationId=@LocationId
			Group By im.ItemCode, ilb.ItemId, BatchNo, LocationId, BucketId 
			Having Count(1)>1
			
			--SELECT @ItemCodeDuplicate 
			-- Remove Comma from @ItemCodeDuplicate, 
			-- If length >0 
			-- SET @outParam = 'INF0226' + @ItemCodeDuplicate
			SELECT @ItemCodeDuplicateModified = REPLACE(@ItemCodeDuplicate, ',', '') 
			
			--SELECT @ItemCodeDuplicateModified 

			If LEN(@ItemCodeDuplicateModified) > 0
			BEGIN
				SET @outParam = 'INF0229' 
				SELECT @outParam OutParam 
				ROLLBACK
				RETURN 
			END


		SELECT  @TOINumber	= head.TOINumber,@SourceLocationId=SourceLocationId,@DestinationLocationId = DestinationLocationId, 
				@CreationDate= GETDATE(), 
				@TotalTOQuantity	= head.TotalTOIQuantity,@TotalTOAmount= head.TotalTOIAmount,@Indentised= Indentised, 
				@CreatedDate	= GETDATE(),			@ModifiedBy			= @CreatedBy
				,			@ModifiedDate			= GETDATE()
			 			
				
		FROM
			   TOI_Header	head
					Inner Join TOI_Detail det
					On head.TOINumber = det.TOINumber
					Where	
					 head.TOINumber=@TOINumber
			
		SELECT @LocationCode = LocationCode FROM Location_Master WHERE LocationId = @SourceLocationId
		--Select @GrossWeight

--		SELECT @TNumber,'ddd'
		Declare @cnt int
		set @cnt = (Select count(TONumber) from TO_Header where TOINumber = @TOINumber)
		--IF (@cnt='0')
		--BEGIN
		IF IsNull(@TNumber,'-1')='-1'
		BEGIN
		IF (@cnt='0' AND @StatusId >='1')
		BEGIN
			
				   Execute usp_GetSeqNo 'TOEX', @SourceLocationId, @SeqNo Out, 1, @DestinationLocationId  
      
   SET @TNumber=@SeqNo  
  
   INSERT INTO TO_Header (TONumber, TOINumber, SourceLocationID, DestinationLocationID, ShippingWayBillNo, ShippingDetails,   GrossWeight, PackSize, Status, RefNumber, Remarks, CreationDate, TotalTOQuantity, TotalTOAmount, Indentised, ShippingDate,  ExpectedDeliveryDate,  CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,Isexported,  
   ExporterRef,OtherRef,BuyerOtherthanConsignee,PreCarriage,PlaceofReceiptbyPreCarrier,VesselflightNo,  
   PortofLoading,PortofDischarge,PortofDestination,TermsofDelivery,DELIVERY,PAYMENT,BuyerOrderNo,BuyerOrderDate)  
    VALUES (@TNumber, @TOINumber, @SourceLocationId, @DestinationLocationId, @ShippingBillNo, @ShippingDetails,  @GrossWeight, @PackSize, @StatusId, @RefNumber, @Remarks, @SystemDate, @TotalTOQuantity, @TotalTOAmount, @Indentised, @SystemDate, NULLIF(@ExpectedDeliveryDate, '1900-01-01 00:00:00'), @ModifiedBy, @SystemDate, @ModifiedBy, @SystemDate,1,  
      @ExporterRef,@OtherRef,@BuyerOtherthanConsignee,@PreCarriage,@PlaceofReceiptbyPreCarrier,@VesselflightNo,  
      @PortofLoading,@PortofDischarge,@PortofDestination,@TermsofDelivery,@DELIVERY,@PAYMENT,@BuyerOrderNo,@BuyerOrderDate)  
  
		END	

		ELSE IF(@cnt<>'0' AND @StatusId<>'1')
		BEGIN
		SET @outParam = 'INF0226' 
		  ROLLBACK
		 SELECT 'INF0226' OutParam
		   Return
		END
		 IF (@StatusId = 2)
            
			Exec [usp_Interface_Audit] '', 'TRNOUT', @LocationCode, 'TO_Header', @TNumber, NULL, NULL, NULL, NULL, 'I', @ModifiedBy, @RecCnt OUTPUT
--			SELECT @TNumber, 'dd'

		END

		ELSE
		BEGIN
			
			-- Check Concurrency, Get DBDate
			Declare @DBDate DateTime
			Select @DBDate = (Select ModifiedDate = (Case When ModifiedBy Is Not Null Then ModifiedDate Else CreatedDate End) From TO_Header Where  TONumber = @TNumber)
			
			If @ModifiedDate <> Convert(VARCHAR(20),@DBDate,120)
			BEGIN
				SET @outParam = 'INF0022' 
				SELECT @outParam OutParam
				ROLLBACK
				Return
			END

			DECLARE @OldStatus AS INT
			SELECT @OldStatus = Status FROM TO_Header WHERE TONumber = @TNumber
			-- Update In TO_Header
			UPDATE TO_Header
			SET ShippingWayBillNo	=	@ShippingBillNo,		ShippingDetails		=	@ShippingDetails,
				GrossWeight			=	@GrossWeight,			PackSize			=	@PackSize,
				TotalTOQuantity		=	@TotalTOQuantity,		TotalTOAmount		=	@TotalTOAmount,
				ModifiedBy			=	@ModifiedBy,			ModifiedDate		=	@SystemDate	,
				Status				=	@StatusId, 
				ShippingDate		=   CASE WHEN @StatusId = 2 THEN @SystemDate ELSE NULL END,
				RefNumber			=	@RefNumber,				Remarks				=	@Remarks,
				ExpectedDeliveryDate=	NULLIF(@ExpectedDeliveryDate, '1900-01-01 00:00:00'),
				ExporterRef			= @ExporterRef,			OtherRef					= @OtherRef,					BuyerOtherthanConsignee = @BuyerOtherthanConsignee,
				PreCarriage			= @PreCarriage,			PlaceofReceiptbyPreCarrier	= @PlaceofReceiptbyPreCarrier,	VesselflightNo			= @VesselflightNo,
				PortofLoading		= @PortofLoading,		PortofDischarge				= @PortofDischarge,				PortofDestination		= @PortofDestination,
				TermsofDelivery		= @TermsofDelivery,		DELIVERY					= @DELIVERY ,					PAYMENT					= @PAYMENT,
				BuyerOrderNo		= @BuyerOrderNo,		BuyerOrderDate				= @BuyerOrderDate
			WHERE TONumber = @TNumber
			--Update TOI Status To Closed
			
		END
--END
--ELSE
--BEGIN
--SET @outParam = 'INF0226' 
--END
		-- If Confirm, Update the Quantity in InventoryLocation_Master and Inventory_LocBucketBatch Tabled
		IF @StatusId = 2
		BEGIN
             
			
			--DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity Numeric(18,4),@DiscountValue Numeric(18,4),
			--							@tempDiscount Numeric(18,4)
										
			--								DECLARE INVLOCBUCKBATCH CURSOR FOR SELECT BD.BatchNo,BD.BucketId,BD.ItemId,BD.AfterAdjustQty  FROM #TO_Detail BD
											          
			--								             OPEN INVLOCBUCKBATCH
			--								 FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity
                               
   --                         WHILE @@FETCH_STATUS = 0
   --                           BEGIN
   --                           set @tempQuantity=-@tempQuantity ;
   --                        SELECT @tempDiscount =0.00 ;
   --                         SET  @DiscountValue=0.00 ;
   --                         EXEC sp_updateInventoryInStockLedger 3,@TNumber,@SourceLocationId,@tempItemID,@tempBatchNo,@tempQuantity,@tempBucketId,
   --                         @DiscountValue ,0,@outParam OUT
                            
   --                         if @outParam!=''
   --                                BEGIN
   --                                SELECT @outParam OutParam
   --                                  CLOSE INVLOCBUCKBATCH
                     
   --                                  DEAllOCATE INVLOCBUCKBATCH
   --                                ROLLBACK
   --                                RETURN
   --                                END
   --                         FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity
   --                           END
                              
   --                           CLOSE INVLOCBUCKBATCH
                              
   --                           DEAllOCATE INVLOCBUCKBATCH
							

			Update TOI_Header Set Status =  5 WHERE TOINumber = @TOINumber
			--Exec [usp_Interface_Audit] '', 'TRNORDINS', @LocationCode, 'TOI_Header', @TOINumber, NULL, NULL, NULL, NULL, 'U', @ModifiedBy,  @RecCnt	OUTPUT	

			Exec [usp_Interface_Audit] '', 'TRNOUT', @LocationCode, 'TO_Header', @TNumber, NULL, NULL, NULL, NULL, 'I', @ModifiedBy	, @RecCnt	OUTPUT

		END
--Select @StatusId
		IF @StatusId = 2 OR @StatusId = 1
		BEGIN

			--SELECT 'T', ItemId, AfterAdjustQty As Quantity, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId FROM @TO_Detail
			--Select 'TT2', * From dbo.Inventory_LocBucketBatch Where ItemId in (2,4) And LocationId = 2

			DELETE FROM TO_Detail WHERE TONumber = @TNumber
			DELETE FROM TOBatchDetail WHERE TONumber = @TNumber

			--INSERT INTO TO_Detail (TONumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOM, TransferPrice, TotalAmount, IndentNo, CreatedBy, CreatedDate)
		
			INSERT INTO TO_Detail (	TONumber, RowNo, ItemId, ItemCode, ItemDescription, Quantity, UOM, TransferPrice, TotalAmount, --IndentNo, 
									CreatedBy, CreatedDate, BucketId,EachCartonQty, ContainerNOFromTo ,GrossWeightItem)
			SELECT @TNumber,ROW_NUMBER() OVER (Order By ItemId) as RowNo,ItemId, ItemCode, ItemDescription, SUM(AfterAdjustQty) AS AfterAdjustQty, UOMId, TransferPrice, SUM(TotalAmount) As TotalAmount, --IndentNo, 
										@CreatedBy,GETDATE(),	BucketId,EachCartonQty, ContainerNOFromTo ,GrossWeightItem
			FROM #TO_Detail
			GROUP BY ItemId, ItemCode, ItemDescription, UOMId, TransferPrice, BucketId,EachCartonQty, ContainerNOFromTo ,GrossWeightItem


			INSERT INTO TOBatchDetail (TONumber, RowNo, ItemId, Quantity, BatchNo,  MfgDate, ExpDate, ManufactureBatchNo, MRP, FromSubBucketId, CreatedBy, CreatedDate)
			SELECT @TNumber,  RowNo, ItemId, AfterAdjustQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId, @ModifiedBy, @SystemDate
			FROM #TO_Detail
			GROUP BY ItemId, RowNo, AfterAdjustQty, BatchNo, MfgDate, ExpDate, ManufactureBatchNo, MRP, BucketId
		END
       EXEC UpdateInventroyOn_TO @TNumber ,@outParam OUt
      IF(@outParam!='')
         
       BEGIN
            Select @outParam OutParam
            RollBack
            Return 
       END


	SELECT TONumber TNumber,
			Convert(VARCHAR(20), ModifiedDate , 120) ModifiedDate, 
			1 as IndexSeqNo
	FROM TO_Header 
	WHERE TONumber = @TNumber
	  COMMIT Transaction
	END TRY
	BEGIN CATCH
		
		SET @outParam = '30001:' + CAST(Error_Number() AS VARCHAR(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() AS VARCHAR(50)) + ', Source: ' + ERROR_PROCEDURE()
		ROLLBACK 
		RETURN ERROR_NUMBER()
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InvoiceSave]    Script Date: 02/09/2014 13:05:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_InvoiceSave]  
   (  
   @orderId varchar(30),
   @locationId  int  ,  
   @outParam varchar(500) OUTPUT,  
   @ModifiedBy int  
   )  
  AS  
  BEGIN  
  BEGIN TRY    
       BEGIN Transaction  
   SET NOCOUNT ON   
     
      
    create table #BatchDetail  (  
    InvoiceNo   Varchar(20),   
   ItemId    INT,    
   BucketId   INT,    
   BatchNo    VARCHAR(50),    
   ItemCode   Varchar(20),    
   TotalQty  NUMERIC(12,4),    
   Quantity   NUMERIC(12,4),    
   ManufacturingBatchNo Varchar(20),    
   MRP    MONEY,    
   MfgDate Varchar(30),    
   ExpDate   Varchar(30),    
   AvailableQty  NUMERIC(12,4),  
   RecordNo   INT    
   )  
   declare @countItems int;  
      set @countItems=0;  
     insert #BatchDetail Exec  usp_DefaultInvoiceBatch  @orderId, @locationID  ,''   ;  
    
      select @countItems=count(BD.ItemId) from #BatchDetail BD LEFT JOIN   
    ( SELECT TotalQty,ItemId,SUM(Quantity) Quantity FROm #BatchDetail GROUP BY ItemId,TotalQty )  
     TOD ON TOD.ItemId=BD.ItemId  
       where TOD.TotalQty>TOD.Quantity  ;  
      -- Select @countItems
    if @countItems =0  
            
     BEGIN   
        DECLARE    
         @iHnd      Int,    
         @PCId      Int,    
         @BOId      INT,     
         @InvoiceNo varchar(30),  
         @LocType     INT    
      
          DECLARE @CountryId AS INT    
          DECLARE @totalPayment MONEY,    
          @changeAmount MONEY    
        
         DECLARE @totalSaleOfDistributor numeric (18, 4)    
         DECLARE @countryIdForLocId AS INT  ,@isLocationOnline INT  ,@DistributorID int
        DECLARE @logNo Varchar(24) ;  
      Create Table #INVOICE (  
       InvoiceNo Varchar(20),    
        TINNo Varchar(20),        
       CustomerOrderNo Varchar(20),    
       BOId INT,    
         LogNo varchar(30),        
         Status INT,        
         CreatedBy INT,        
         ModifiedBy INT ,    
         DistributorId INT,     
         PCId INT                
      )  
      insert INTO #INVOICE    
         SELECT '' as InvoiceNo,LM.TinNo,CustomerOrderNo,BOId,LogNo,    
       CO.Status,CO.CreatedBy,CO.ModifiedBy,CO.DistributorId, PCId    
                         
          FROM    
         COHeader CO left join Location_Master LM on LM.LocationId=CO.BOId   
      where LM.LocationId=@locationId and CO.CustomerOrderNo=@orderId    
        
         SELECT @orderId=CustomerOrderNo,@BOID=BOId,@CountryId =  
          DeliverFromCountryId,@DistributorID=DistributorId  
          , @PCId = PCId,@logNo=ISNULL(LogNo,'')    
                      from  COHeader CH where CH.[CustomerOrderNo]=@orderId    
          
                             
        SELECT     
       @PCId    = I.PCId,    
       @BOId    = I.BOId    
        FROM #INVOICE I Join COPayment COP     
        ON I.[CustomerOrderNo]=COP.[CustomerOrderNo]            
           
        
        SELECT 
        @isLocationOnline=IsLocationOnline   
        FROM Location_Master lm WHERE lm.LocationId   
        = @locationId AND lm.Status = 1    
        
 
      BEGIN  
        declare @orderType int   
         DECLARE @reccnt VARCHAR(2000)   
       set @orderType=-1;         
        select @orderType=orderType from COHeader where CustomerOrderNo=@orderId         
        IF EXISTS    
        (    
           SELECT 1 from Inventory_LocBucketBatch INV     
         JOIN #BatchDetail BD    
         ON INV.BatchNo=BD.BatchNo AND INV.ItemId=BD.ItemId AND INV.LocationId=@BOID    
         AND INV.BucketID=(SELECT [BucketId] FROM [Bucket] where [ParentId] IS NOT NULL AND [Sellable]=1 )    
         AND INV.Quantity<BD.Quantity    
         )    
         BEGIN    
        --PRINT 'Qty Not available'          
        SET @outParam='40008'  
        SELECT @outParam OutParam  
        rollback    
        return    
         END    
        -- select @orderType as orderType  
        DECLARE @SeqNo varchar(30)  
              Execute usp_GetSeqNo 'TOGRN', @locationId, @SeqNo Out   
             -- In Case of nepal, SeqType will be same for kit invoice no. and invoice    
               
             --PRINT ' GET NEW INVOICE NO'    
                   
            SET @InvoiceNo=@SeqNo    
              -- SELECT @InvoiceNo as InvoiceNo    
            Update #INVOICE Set InvoiceNo=@InvoiceNo    
            Update #BatchDetail SET InvoiceNo=@InvoiceNo    
             
            --   SELECT  @InvoiceNo InvoiceNo  
            --PRINT 'Insert CI HEader Details'   
              if Exists(SELECT 1 from CoHeader WHERE CustomerOrderNo=@orderId AND status = 4)    
                BEGIN      
              --PRINT 'Order Not Exists'    
              --select * from #BatchDetail     
              SET @outParam='40013'    
                     
                
                   SELECT @outParam OutParam  
               rollback   
                return    
               END     
                            
                    INSERT INTO [CIHeader]    
                   ([InvoiceNo],[InvoiceDate],[CustomerOrderNo],[LogNo]    
                   ,[PCId],[BOId],[TINNo],[DistributorId],[TaxAmount]    
                   ,[InvoiceAmount],[DiscountAmount],[PaymentAmount],[ChangeAmount]    
                   ,[TotalBV],[TotalPV],[Status],[DeliveryMode],[IsProcessed]    
                   ,[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[StaffId]    
                   ,[DeliveryToId],[DeliverToAddressLine1],[DeliverToAddressLine2],[DeliverToAddressLine3],[DeliverToAddressLine4]    
                   ,[DeliverToCityId],[DeliverToPincode],[DeliverToStateId],[DeliverToCountryId],[DeliverToTelephone],[DeliverToMobile]    
                   ,[DeliverfromId],[DeliverFromAddress1],[DelverFromAddress2],[DeliverFromAddress3],[DeliverFromAddress4]    
                   ,[DeliverFromCityId],[DeliverFromPincode],[DeliverFromStateId],[DeliverFromCountryId],[DeliverFromTelephone],[DeliverFromMobile]    
                   ,[TotalUnits],[TotalWeight],[InvoicePRINTed],[OrderMode],[TerminalCode],[InvoiceConsiderDate], TaxJurisdictionId)    
                SELECT I.InvoiceNo,GetDate(),I.[CustomerOrderNo],CH.[LogNo]    
                ,CH.[PCId],CH.[BOId],I.[TINNo],CH.[DistributorId],CH.[TaxAmount]    
                ,CH.[OrderAmount],CH.[DiscountAmount],LTRIM(RTRIM(CH.[PaymentAmount])),CH.[ChangeAmount]    
                ,CH.[TotalBV],CH.[TotalPV],1,CH.[DeliveryMode],0    
                ,I.[CreatedBy],GetDate(),I.[ModifiedBy],GetDate(),I.[CreatedBy]    
                ,CH.[DeliverTo],CH.[DeliverToAddressLine1],CH.[DeliverToAddressLine2],CH.[DeliverToAddressLine3],CH.[DeliverToAddressLine4]    
                ,CH.[DeliverToCityId],CH.[DeliverToPincode],CH.[DeliverToStateId],CH.[DeliverToCountryId],CH.[DeliverToTelephone],CH.[DeliverToMobile]    
                ,CH.[BOId],CH.[DeliverFromAddress1],CH.[DelverFromAddress2],CH.[DeliverFromAddress3],CH.[DeliverFromAddress4]    
                ,CH.[DeliverFromCityId],CH.[DeliverFromPincode],CH.[DeliverFromStateId],CH.[DeliverFromCountryId],CH.[DeliverFromTelephone],CH.[DeliverFromMobile]    
                ,CH.[TotalUnits],CH.[TotalWeight],0,CH.[OrderMode],CH.[TerminalCode],(select MonthEndDate from BusinessMonth where Status=1
 ), TaxJurisdictionId    
                FROM #INVOICE I Join COHeader CH ON I.[CustomerOrderNo]=CH.[CustomerOrderNo]    
                     
     --            --PRINT 'INSERT INTO CI DETAIL'    
                INSERT INTO CIDetail ([InvoiceNo],[RecordNo],[ItemId],[ItemCode]    
                ,[ItemName],[ShortName],[PRINTName],[ReceiptName],[DisplayName]    
                   ,[TotalWeight],[Quantity],[DP],[UnitPrice],[MRP],[UOM],[Discount]    
                ,[TaxAmount],[LineAmount],[LineBV],[LinePV],[IsKit],[IsComposite]    
                ,[Weight],[Length],[Height],[Width],[IsPromo],[PrimaryCost],[TaxCategoryId],[GiftVoucherCode],[VoucherSrNo])    
                SELECT @InvoiceNo,[RecordNo],[ItemId],[ItemCode]    
                ,[ItemName],[ShortName],[PRINTName],[ReceiptName],[DisplayName]    
                   ,[TotalWeight],[Qty],[DP],[UnitPrice],case when IsPromo=0 then [MRP] else 0 end,[UOM],[Discount]    
                ,[TaxAmount],[Amount],[BV],[PV],[IsKit],[IsComposite]    
                ,[Weight],[Length],[Height],[Width],[IsPromo],[PrimaryCost],[TaxCategoryId],[GiftVoucherCode],[VoucherSrNo]    
                FROM CODetail CD WHERE CustomerOrderNo=@orderId  
                      
     --              --PRINT ' INSERT INTO CIDetailDiscount'    
                INSERT INTO  [CIDetailDiscount]     
                (    
                 [InvoiceNo],[RecordNo],[PromotionId],[Description],[DiscountPercent]    
                 ,[DiscountAmount]    
                )    
                SELECT @InvoiceNo,[RecordNo],[PromotionId],[Description],[DiscountPercent]    
                 ,[DiscountAmount]    
                FROM [CODetailDiscount] WHERE CustomerOrderNo=@orderId  
                     
                   INSERT INTO [CIPayment]    
                (    
                 [InvoiceNo],[TenderType],[PaymentAmount],[RecordNo]    
                 ,[Date],[Remark],[BankName],[ChqIssueDate],[ChqExpiryDate]    
                 ,[ForexAmount],[Reference],[CreditCardNumber],[CardHolderName]    
                 ,[CardExpiryDate],[CurrencyCode],[ExchangeRate],[CardType]    
                )    
                SELECT @InvoiceNo,[TenderType],[PaymentAmount],[RecordNo]    
                  ,[Date],[Remark],[BankName],[ChqIssueDate],[ChqExpiryDate]    
                  ,[ForexAmount],[Reference],[CreditCardNumber],[CardHolderName]    
                  ,[CardExpiryDate],[CurrencyCode],[ExchangeRate],[CardType]    
                FROM [COPayment]  WHERE CustomerOrderNo=@orderId    
                       
                --PRINT 'INSERT INTO CIBATCHDETAILS'    
                     
                INSERT INTO [CIBatchDetail]    
                (    
                 [InvoiceNo],[RecordNo],[ItemId],[BucketId],[BatchNo]    
                 ,[Quantity],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate]    
                )    
                SELECT InvoiceNo,RecordNo,ItemId,BucketId,BatchNo,    
                 Quantity,@ModifiedBy,@ModifiedBy,GETDATE(),GETDATE()    
                FROM #BatchDetail    
                       
                 		
						 
				  SELECT @locType = lm.LocationType 
					   FROM Location_Master lm WHERE lm.LocationId 
					   = @PCId AND lm.Status = 1
			IF(	@locType=4)
			   BEGIN   
			                     
					SELECT @totalPayment= ISNULL(COH.[PaymentAmount],0),    
				   @changeAmount = COh.ChangeAmount,    
				   @PCId    = COH.PCId,    
				   @BOId    = COH.BOId    
					FROM COHeader    COH
					Where COH.[CustomerOrderNo]   =@orderId 
					EXEC [sp_PUCCheckAmount] @PCId, @BOId, @totalPayment, @changeAmount, @outparam OUTPUT  
					
							  IF @outParam !=''  
								BEGIN   
									select @outParam OutParam
								 rollback
								  RETURN
								END 
				        	INSERT INTO PUCTransactionDetail  
									VALUES(@PCId,@InvoiceNo,(@totalPayment),GetDate())                             	 	   
		       END	 
		     else
		       BEGIN
		                      
					SELECT @totalPayment= ISNULL(SUM(COP.PaymentAmount),0),    
				   @changeAmount = 0.00  
					FROM COPayment    COP
					Where COP.[CustomerOrderNo]   =@orderId ANd TenderType=6
							if @totalPayment!=0
								  BEGIN
									EXEC [sp_PUCCheckAmount] @PCId, @BOId, @totalPayment, @changeAmount, @outparam OUTPUT  
									
											  IF @outParam !=''  
												BEGIN   
												select @outParam OutParam
												 rollback
												  RETURN
												END 
									
				        					INSERT INTO PUCTransactionDetail  
													VALUES(@PCId,@InvoiceNo,(@totalPayment),GetDate())
									END
		       END
                    
                INSERT INTO [CIBatchDetailTax]    
                (    
                 [InvoiceNo],[RecordNo],[TaxRecordNo],[ItemId]    
                 ,[BatchNo],[TaxCode],[TaxPercent],[TaxGroupCode]    
                 ,[TaxAmount]    
                )    
                SELECT BD.InvoiceNo,BD.RecordNo,CT.TaxRecordNo    
                 ,BD.ItemId,BD.BatchNo,CT.[TaxCode],CT.[TaxPercent],CT.[TaxGroupCode]    
                 ,Cast(CT.[TaxAmount]*BD.Quantity/BD.TotalQty as NUMERIC(12,4))         
                FROM #BatchDetail BD     
                JOIN [CODetailTax]CT    
                       
                On BD.ItemId=CT.ItemId AND BD.RecordNo=CT.RecordNo           
                WHERE CT.[CustomerOrderNo]=@orderId        
             if Exists(SELECT 1 FROM ciheader WHERE CustomerOrderNo=@orderId GROUP BY CustomerOrderNo HAVING count(1)>1)    
                BEGIN      
                 --PRINT 'Order Not Exists'       
                 SET @outParam='40013'   
                 SELECT @outParam OutParam  
               rollback   
            
                END    
              --PRINT 'DEDUCT BATCH QTY'    
              --Update Inventory_LocBucketBatch set Quantity=Inv.Quantity-B.Quantity    
              --FROM Inventory_LocBucketBatch Inv,    
              --(    
              -- SELECT Sum(Quantity)Quantity,ItemId,[BatchNo] FROM #BatchDetail      
              -- GROUP BY ItemId,[BatchNo]    
              --) B    
              --WHERE Inv.[BatchNo]=B.[BatchNo] AND Inv.ItemId=B.ItemId     
              --AND Inv.BucketID=(SELECT [BucketId] FROM [Bucket] where [ParentId] IS NOT NULL AND [Sellable]=1 )  
          --DECLARE @tempItemID INT,@tempBatchNo Varchar(20),@tempBucketId INT ,@tempQuantity Numeric(18,4),@DiscountValue Numeric(18,4),  
          --@tempDiscount Numeric(18,4)  
            
          -- DECLARE INVLOCBUCKBATCH CURSOR FOR SELECT BD.BatchNo,BD.BucketId,BD.ItemId,BD.Quantity FROM #BatchDetail BD  
                       
          --              OPEN INVLOCBUCKBATCH  
          --  FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity  
                                 
          --                  WHILE @@FETCH_STATUS = 0  
          --                    BEGIN  
          --                    set @tempQuantity=-@tempQuantity ;  
          --                 SELECT @tempDiscount =Discount FROM CIDetail where InvoiceNo=@InvoiceNo AND ItemId=@tempItemID   
          --                  SET  @DiscountValue=dbo.[ufn_GetStockLedgerInvoiceDiscount] (@InvoiceNo,@BOId,@tempItemID,@tempBatchNo,@tempDiscount)  
          --                  EXEC sp_updateInventoryInStockLedger 1,@InvoiceNo,@BOId,@tempItemID,@tempBatchNo,@tempQuantity,@tempBucketId,  
          --                  @DiscountValue ,0,@outParam OUT  
                              
          --                  if @outParam!=''  
          --                         BEGIN  
          --                         SELECT @outParam OutParam  
          --                           CLOSE INVLOCBUCKBATCH  
                       
          --                           DEAllOCATE INVLOCBUCKBATCH  
          --                         ROLLBACK  
          --                         RETURN  
          --                         END  
          --                  FETCH NEXT FROM INVLOCBUCKBATCH INTO @tempBatchNo,@tempBucketId,@tempItemID,@tempQuantity  
          --                    END  
                                
          --                    CLOSE INVLOCBUCKBATCH  
                                
          --                    DEAllOCATE INVLOCBUCKBATCH  
                           
                            
               
             DECLARE @tempDistributorBusiness_Current TABLE    
             (    
              DistributorId INT,    
              DownLineId INT,    
              TotalBV NUMERIC(18, 4),    
              TotalPV NUMERIC(18, 4),    
              IsSelf BIT,    
              PersonalSale NUMERIC(18, 4),    
              DownLinePercent NUMERIC(12, 4),    
              AlreadyExist BIT    
             )    
                   
             INSERT INTO @tempDistributorBusiness_Current    
             SELECT @DistributorId, -1, cih.TotalBV,cih.TotalPV,1, (cih.InvoiceAmount+ cih.TaxAmount), 0, 0    
             FROM dbo.CIHeader cih    
             WHERE cih.InvoiceNo = @invoiceno    
             UNION ALL    
             SELECT a.UplineId, a.DistributorId, ci.TotalBV,ci.TotalPV, 0, 0, 0, 0    
             FROM dbo.ufn_GetDistributorUpline(@DistributorID) a    
             JOIN dbo.CIHeader ci    
             ON Ci.InvoiceNo = @InvoiceNo    
                    
                    
             UPDATE x    
             SET x.DownLinePercent = ISNULL(y.BonusPercent, 5)    
             FROM @tempDistributorBusiness_Current x    
             LEFT OUTER JOIN dbo.DistributorBusiness_Current y    
             ON x.DownLineId = y.DistributorId    
                    
             UPDATE a    
             SET a.AlreadyExist = CASE WHEN b.DistributorId IS NULL THEN 0 ELSE 1 END    
             FROM @tempDistributorBusiness_Current a    
             LEFT Outer JOIN dbo.DistributorBusiness_Current b    
             ON a.DistributorId = b.DistributorId    
                    
             UPDATE dbc    
             SET dbc.TotalBV = dbc.TotalBV + tbc.TotalBV,    
              dbc.TotalPV = dbc.TotalPV + tbc.TotalPV,    
              dbc.TotalCumBV = dbc.TotalCumBV + tbc.TotalBV,    
              dbc.TotalCumPV = dbc.TotalCumPV + tbc.TotalPV,    
              dbc.SelfBV = dbc.SelfBV + CASE WHEN tbc.IsSelf = 1 THEN tbc.TotalBV ELSE 0 END,    
              dbc.SelfPV = dbc.SelfPV + CASE WHEN tbc.IsSelf = 1 THEN tbc.TotalPV ELSE 0 END,    
              dbc.GroupBV = dbc.GroupBV + CASE WHEN tbc.IsSelf = 1 THEN 0 ELSE tbc.TotalBV END,    
              dbc.GroupPV = dbc.GroupPV + CASE WHEN tbc.IsSelf = 1 THEN 0 ELSE tbc.TotalPV END,    
              dbc.ExclBV = dbc.ExclBV + CASE WHEN tbc.ISSelf = 1 THEN tbc.TotalBV ELSE CASE WHEN tbc.DownlinePercent = 20 THEN 0 ELSE tbc.TotalBV END END,    
              dbc.ExclPV = dbc.ExclPV + CASE WHEN tbc.ISSelf = 1 THEN tbc.TotalPV ELSE CASE WHEN tbc.DownlinePercent = 20 THEN 0 ELSE tbc.TotalPV END END    
             FROM dbo.DistributorBusiness_Current dbc    
             INNER JOIN @tempDistributorBusiness_Current tbc    
             ON dbc.DistributorId = tbc.DistributorId    
             AND tbc.AlreadyExist = 1    
                    
             INSERT INTO DistributorBusiness_Current (DistributorId, TotalBV, TotalPV, TotalCUmBV, TotalCUMPV, SelfBV, SelfPV, GroupBV, GroupPV, ExclBV, ExclPV)    
             SELECT DistributorId, TotalBV, TotalPV, TotalBV, TotalPV, CASE WHEN IsSelf = 1 THEN TotalBV ELSE 0 END, CASE WHEN IsSelf = 1 THEN TotalPV ELSE 0 END,    
             CASE WHEN IsSelf = 1 THEN 0 ELSE TotalBV END,    
             CASE WHEN IsSelf = 1 THEN 0 ELSE TotalPV END,    
             CASE WHEN ISSelf = 1 THEN TotalBV ELSE CASE WHEN DownlinePercent = 20 THEN 0 ELSE TotalBV END END,    
             CASE WHEN ISSelf = 1 THEN TotalPV ELSE CASE WHEN DownlinePercent = 20 THEN 0 ELSE TotalPV END END    
             FROM @tempDistributorBusiness_Current    
             WHERE AlreadyExist = 0    
                   
               --PRINT 'UPDATE COHEADER STATUS'    
              --Update CO header status to invoiced    
              Update COHeader set status=4,ModifiedDate=GETDATE(),ModifiedBy=@ModifiedBy where [CustomerOrderNo]=@orderId     
              --PRINT 'orderType'+Cast(@OrderType AS varchar(2))    
               --KIt Order    
               IF (@OrderType=3)    
                BEGIN    
                 --PRINT 'Update Distributor Matser kit order'    
                 UPDATE DistributorMaster SET KitInvoiceNo=@InvoiceNo, ModifiedBy =@ModifiedBy ,ModifiedDate = Getdate()     
                 WHERE KitOrderNo=@orderId     
                END    
                       
               --First Order Activate Distributor    
               ELSE IF (@OrderType = 1 OR @OrderType=2 OR @OrderType=0)    
               BEGIN    
                      
               DECLARE @MinimumSaleAmount numeric (18, 4) , @CurrentSaleAmount numeric (18, 4)       
               SELECT @CurrentSaleAmount = convert(decimal(18,4), SUM(IsNull(( (C.BV)*(C.Qty)), 0) ))     
                       FROM #INVOICE I Join COHeader CH ON I.[CustomerOrderNo]=CH.[CustomerOrderNo]       
                       INNER JOIN CODetail c ON     
                       c.CustomerOrderNo = ch.CustomerOrderNo    
                       INNER JOIN ITEM_MASTER im ON c.itemid = im.itemid     
                       AND im.MerchHierarchyDetailId NOT IN (28,26,23) --AND im.iskit<>1 and  --im.MerchHierarchyDetailId IN (10,17,29)    
                          AND im.itemname NOT LIKE '%Starter%'    
                             
               DECLARE @PrevDistributorStatus AS INT    
                 
                  SELECT @MinimumSaleAmount = convert(decimal(18,4),IsNull( pm.KeyValue1, 0))    
                  FROM Parameter_Master pm     
                  INNER JOIN DistributorMaster b     
                  ON b.DistributorCountryCode = pm.KeyCode1    
                  WHERE pm.ParameterCode = 'MINIMUMSALEAMOUNT'     
                  AND b.DistributorId = @DistributorID    
                      
                SELECT @totalSaleOfDistributor =   IsNull(AllInvoiceAmountSum, 0),
                @PrevDistributorStatus = DistributorStatus  FROM DistributorMaster dm WHERE dm.DistributorId=@DistributorID     
             --          
             --   SELECT @totalSaleOfDistributor ,@CurrentSaleAmount,@MinimumSaleAmount,@DistributorID    
             --      SELECT DistributorStatus,AllInvoiceAmountSum, FirstOrderTaken,* FROM DistributorMaster WHERE DistributorId = '31313131'    
                 
                       
               BEGIN    
                --PRINT 'Update Distributor Matser First order'    
             --      UPDATE DistributorMaster SET DistributorStatus = 2,DistributorActivationDate = Getdate(), ModifiedBy =@ModifiedBy ,ModifiedDate = Getdate()     
             --      WHERE DistributorId=@DistributorID     
                -----------------Alok---------------    
                UPDATE DistributorMaster    
                SET DistributorStatus =   CASE WHEN ((IsNull(@totalSaleOfDistributor, 0) + IsNull(@CurrentSaleAmount, 0))
                  >= IsNull(@MinimumSaleAmount, 0)) THEN  2                              
                      ELSE 1    
                       END ,   
                         FirstOrderTaken =  1      ,                
                       AllInvoiceAmountSum = IsNull(@totalSaleOfDistributor, 0) + convert(decimal(18,2), IsNull(@CurrentSaleAmount, 0))    
                ,DistributorActivationDate = Getdate(), ModifiedBy =@ModifiedBy ,ModifiedDate = Getdate()--,LastInvoiceDate = GetDate()     
                WHERE DistributorId=@DistributorID     
                  
               
                ---------------Alok--------------    
                       
                IF EXISTS (SELECT 1 FROM DistributorMaster WHERE AllInvoiceAmountSum <>@totalSaleOfDistributor OR DistributorStatus<>@PrevDistributorStatus)    
                 EXEC usp_interface_audit '','DISTMST','','DistributorMaster',@DistributorID,NULL,NULL,NULL,NULL,'U',@ModifiedBy,@reccnt OUTPUT      
                        
               END    
               END   
          if @isLocationOnline=0         
            BEGIN  
              EXEC usp_interface_audit '','CUSTINV','','CIHeader',@InvoiceNo,NULL,NULL,NULL,NULL,'I',@ModifiedBy,@reccnt OUTPUT        
            END  
               
     --     /*******************************************************    
     --     START INTERFACE AUDIT FOR PUC ACCOUNT     
     --     *******************************************************/    
              
           --SELECT '1', @locType, @BOId, @PCId    
               
           -- Deduct Available amount for this PUC from PUC Accounts    
         
           --IF (@locType = 4)    
           --BEGIN     
           -- UPDATE PUCTransactionDetail    
           -- SET OrderNo=@InvoiceNo where OrderNo=@orderId                               
           --END    
                            
                            
          --  Interface Audit Entry        
                EXEC usp_interface_audit '','CUSTORD','','COHeader',@orderId,NULL,NULL,NULL,NULL,'I',@ModifiedBy,@reccnt OUTPUT        
         --  IF (@PCId !=@ )    
         --  BEGIN    
         --DECLARE @TopRecordId VARCHAR(25)    
         --SELECT @TopRecordId = p.RecordNo FROM PUCDeposit p WHERE p.PCId = @PCId    
         --EXEC usp_interface_audit '','PUC','','PUCAccount',@TopRecordId,@InvoiceNo,NULL, NULL,NULL,'U',@ModifiedBy,@reccnt OUTPUT               
         --  END    
               
          /*******************************************************    
          END INTERFACE AUDIT FOR PUC ACCOUNT     
          *******************************************************/    
   DECLARE @countForLogOrder int;  
    DECLARE @isLogClosed int ;
    Set @isLogClosed=0 ;
   IF @logNo !=''  
       BEGIN  
            SELECT @countForLogOrder =COUNT(CustomerOrderNo) from COHeader where (Status=3 OR Status=1) AND LogNo=@logNo  
            IF @countForLogOrder=0  
              BEGIN  
                 UPDATE OrderLog set Status=2 where LogNo=@logNo  
                 SET @isLogClosed =1
              END  
    END    
    EXEC SP_UpdateInventroyOnInvoice @InvoiceNo ,@outParam  OUT
            if(@outParam!='')
               BEGIN
                SELECT @outParam OutParam
                    ROLLBACK  
                     Return   
                END     
       select @isLogClosed isLogClosed, @InvoiceNo as InvoiceNo, CustomerOrderNo, UserName as CreatedBy,co.Date 'CreatedDate', case CO.Status when 4 then 'Invoiced' else '' end as  
     Status , CO.Status StatusId  from COHeader CO left join  User_Master UM on um.UserId=co.ModifiedBy where CustomerOrderNo=@orderId;  
          
                
        END  
            
      END  
                       
    else if @countItems>0  
    BEGIN      
                 
     SET @outParam='INV001' --short inventory                   
        SELECT @outParam OutParam,IM.ItemCode,im.ItemName,BD.ItemId,TOD.TotalQty RequiredQty,Tod.Quantity AvailableQty  
         from #BatchDetail BD LEFT JOIN   
         ( SELECT TotalQty,ItemId,SUM(Quantity) Quantity FROm #BatchDetail GROUP BY ItemId,TotalQty )  
          TOD ON TOD.ItemId=BD.ItemId  
        
        inner join Item_Master IM  
        on BD.ItemId = IM.ItemId  
         where TOD.TotalQty>TOD.Quantity  GROUP By IM.ItemCode,Im.ItemName,BD.ItemId,TOD.TotalQty,Tod.Quantity  
       
    rollback  
    RETURN   
                     
       --   select * from #BatchDetail;  
    END                  
       drop TABLE #BatchDetail;           
      drop TABLE #INVOICE;                  
             
       Commit Transaction           
                         
        END try  
        Begin Catch    
    Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()    
     SELECT @outParam OutParam
     ROLLBACK  
    
    Return    
       
   End Catch    
   END
GO
/****** Object:  StoredProcedure [dbo].[SP_InvoiceForMultipleOrders]    Script Date: 02/09/2014 13:05:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Procedure [dbo].[SP_InvoiceForMultipleOrders]
	@jsonForOrder	Nvarchar(max),
	@LogNo     varchar(50),
	@modifiedBy int ,
	@locationId int,
	@outParam	VARCHAR(500) OUTPUT	
AS
Begin
	set nocount on ;
	
DECLARE @OrderId varchar(20), @DistributorId INT
 DECLARE @TempTable  TABLE (
		 orderNo varchar(20),
		 InvDescription varchar(1000),
		 Status int,
		 locationId int,
		 modifiedBy int
		 )
           DECLARE @MyHierarchy JSONHierarchy ,@XMLItems xml,@finalOutPut int
         
           SET @finalOutPut=0;
           SET @outParam ='';
           INSERT INTO @myHierarchy SELECT * FROM parseJSON(@jsonForOrder)
				--SELECT dbo.ToXML(@MyHierarchy)
				SELECT @XMLItems=dbo.ToXML(@MyHierarchy)
		

                            select  b.value('@OrderNo', 'varchar(50)') as OrderNo  
							
								into #Orders
							  FROM @XMLItems.nodes('/root/item') a(b)
		BEGIN TRY
		IF(@LogNo='')
         BEGIN 
				DECLARE ORDERCUROR CURSOR FOR SELECT COH.CustomerOrderNo,COH.DistributorId from #Orders INNER
				 JOIN COHeader COH ON COH.CustomerOrderNo=#Orders.OrderNo 
				  And COH.Status=3
		 END
		 ELSE
		  BEGIN
		     DECLARE ORDERCUROR CURSOR FOR SELECT COH.CustomerOrderNo,COH.DistributorId From  COHeader COH where LogNo=@LogNo  And COH.Status=3
		  END    
		    OPEN ORDERCUROR
				FETCH NEXT FROM ORDERCUROR INTO @OrderId,@DistributorId
										
					WHILE @@FETCH_STATUS = 0
		      
						BEGIN
						IF(@OrderId!='')
								BEGIN
									EXEC SP_InvoiceSave  @OrderId,@locationId,@outParam out,@ModifiedBy
									if(@outParam='40008') --order not exist
									 BEGIN
									   SET @finalOutPut=1
									   INSERT INTO @TempTable values(@orderId,'order not exist',0,@locationId,@modifiedBy)
									 END
									 ELSE if(@outParam='40013')--order for invoice not exist
									 BEGIN
									   SET @finalOutPut=1
									 INSERT INTO @TempTable values(@orderId,'order for invoice not exist',0,@locationId,@modifiedBy)
									 END
									 
									 ELSE if(@outParam='INV001') --short inventory
									 BEGIN
									       SET @finalOutPut=1
										 INSERT INTO @TempTable values(@orderId,'short inventory for order',0,@locationId,@modifiedBy)
								     END
								      ELSE if(@outParam='INF0145') --short inventory
									 BEGIN
									       SET @finalOutPut=1
										 INSERT INTO @TempTable values(@orderId,'PUC have no sufficient balance',0,@locationId,@modifiedBy)
								     END
								     ELSE if(@outParam='')
								     BEGIN
								     insert into @TempTable values(@orderId,'Invoiced',1,@locationId,@modifiedBy)
								     END
								     else 
								        BEGIN
								         insert into @TempTable values(@orderId,@outParam,0,@locationId,@modifiedBy ) 
								        END
									 
									         
								END
								  SET @outParam =''  ;
						
							FETCH NEXT FROM ORDERCUROR INTO @OrderId,@DistributorId
				
						END
						
			CLOSE ORDERCUROR 
			DEALLOCATE ORDERCUROR
						SELECT * from @TempTable
							if NOT  Exists (select * from COHeader where LogNo=@LogNo and Status=3 )		
				  BEGIN
				     update OrderLog set Status=2 where LogNo=@LogNo
				  END
		END TRY
		BEGIN CATCH
			CLOSE ORDERCUROR 
			DEALLOCATE ORDERCUROR
			Set @outParam = '30001:' + CAST(Error_Number() AS VarChar(50)) + ', ' + Error_Message() + 'Error Line: ' + CAST(Error_Line() As VarChar(50)) + ', Source: ' + ERROR_PROCEDURE()
			Return ERROR_NUMBER()
			Rollback 
		END CATCH	  
					
							
END
GO
